
<div class="cls-content-sm panel">
    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h1 class="h3">Регистрация компании</h1>
        </div>
        <form action="/api/v1/companies/auth/registration" method="post">

            <div class="form-group">
                <input type="text" class="form-control" placeholder="email" name="email" autofocus>
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="phone" name="phone" autofocus>
            </div>

            <div class="form-group">
                <input type="password" class="form-control" placeholder="пароль" name="password">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="имя" name="firstname">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="фамилия" name="lastname">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="название компании" name="company_name">
            </div>

            <button class="btn btn-primary btn-lg btn-block" type="submit">Регистрация</button>
        </form>
    </div>

</div>
