<!-- LOGIN FORM -->
<!--===================================================-->
<div class="cls-content">
    <div class="cls-content-sm panel">
        <div class="panel-body">
            <div class="mar-ver pad-btm">
                <h1 class="h3">Сброс пароля в личный кабинет поставщика</h1>
            </div>
            <form action="/api/v1/companies/auth/reset-password" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="login" name="login" autofocus>
                </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit">Вход</button>
            </form>
        </div>

        <div class="pad-all">
            <a href="/passreminder" class="btn-link mar-rgt">Забыли пароль ?</a>

            <div class="media pad-top bord-top"></div>
        </div>
    </div>
</div>
