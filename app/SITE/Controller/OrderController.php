<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class OrderController extends AppController
{
    public $uses = array(
        'City',
        'Offer',
        'Order',
        'Order_Comment',
        'Orderer',
        'Order_Product',
        'Product',
        'Region',
        'Shop',
        'Shop_Product',
    );

    public $components = array(
        'Session',
        'Breadcrumbs',
        'Flash',
        'Uploader',
        'ShopOwnerCom',
        'ProductCom',
        'OrderCom',
        'Activity'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(_("Заказы"), Router::url(array('plugin' => false, 'controller' => 'order', 'action' => 'index')));
        parent::beforeFilter();
    }

//    public function partner_redirect()
//    {
//        $id = $this->request->param('id');
//
//
//        $url = "http://mail.ru";
//        return $this->redirect($url);
//    }

    public function index()
    {
        if ($this->ShopOwnerCom->shop_list() == null) {
            $this->set('shops', null);
            return;
        }

        $city_id = $this->request->query['city_id'] ?? null;
        $orderer_id = $this->request->query['orderer_id'] ?? null;
        $shop_id = $this->request->query['shop_id'] ?? null;
        $status = $this->request->query['status'] ?? null;

        if ($city_id == null OR $city_id == "0") {
            $city_ids_array = [];
        } else {
            $city_ids_array = ['City.id' => $city_id];
        }

        if ($orderer_id == null OR $orderer_id == "0") {
            $orderer_ids_array = [];
        } else {
            $orderer_ids_array = ['Order.orderer_id' => $orderer_id];
        }

        if ($shop_id == null OR $shop_id == "0") {
            $shop_ids_array = [];
        } else {
            $shop_ids_array = ['Order.shop_id' => $shop_id];
        }
        if ($status == null OR $status == "" OR !in_array($status, array_keys($this->OrderCom->valid_order_status_list))) {
            $status_array = [];
        } else {
            $status_array = ['Order.status' => $status];
        }

        $page = $this->request->query['page'] ?? 1;
        $c_count = 5;

        $show_count = $this->request->query['count'] ?? $c_count;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        $total_orders_count = $this->Order->find("count",
            array('conditions' =>
                array(
                    'Shop.id' => $this->ShopOwnerCom->shop_list(),
                    $city_ids_array,
                    $orderer_ids_array,
                    $shop_ids_array,
                    $status_array
                ),
                'joins' => array(
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop.id = Order.shop_id'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Order.order_city = City.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Order.*',
                    'Shop.*',
                ),
            )
        );

        $page_orders = $this->Order->find("all",
            array('conditions' =>
                array(
                    'Shop.id' => $this->ShopOwnerCom->shop_list(),
                    $city_ids_array,
                    $orderer_ids_array,
                    $shop_ids_array,
                    $status_array
                ),
                'joins' => array(
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop.id = Order.shop_id'
                        )
                    ),
                    array(
                        'table' => 'orderers',
                        'alias' => 'Orderer',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Order.orderer_id = Orderer.id'
                        )
                    ),
                    // город заказа
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'City.id = Order.order_city'
                        )
                    ),
                ),
                'fields' => array(
                    'City.*',
                    'Order.*',
                    'Shop.*',
                    'Orderer.*',
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array('order.id DESC')
            )
        );

        foreach ($page_orders as &$order) {
            $order_id = $order['Order']['id'];
            $order['product_count'] = $this->OrderCom->shop_order_amount($order_id);
            $order['order_total_cost'] = $this->OrderCom->shop_order_sum($order_id);
        }
        $form_data = [];
        $form_data['active_only'] = "on";
        $form_data['orderer_id'] = $orderer_id;
        $form_data['shop_id'] = $shop_id;
        $form_data['city_id'] = $city_id;
        $form_data['status'] = $status;


        $this->set('form_data', $form_data);
        $this->set('orders', $page_orders);
        $this->set('shop_list', $this->ShopOwnerCom->shop_list_full());
        $this->set('orderers_list', $this->OrderCom->shop_orderers_list());

        $this->set('orderers_city_list', $this->OrderCom->shop_orderers_city_list());

        $pages = ceil($total_orders_count / $show_count);
        $this->set('page', $page);
        $this->set("pages", $pages);

        $this->set("order_status_list", $this->OrderCom->valid_order_status_list);

        $this->set('title', "Управление заказами");
        $this->set('content_title', "Управление заказами");
    }

    public function edit()
    {
        $id = $this->request->param('id');

        //проверка владения магазином
        //TODO сделать проверку на наличие прав для редактирования заказа
        /*
        if(!$this->ShopOwnerCom->has_owner_rights($id)){
            $this->Flash->set(__('У вас нет права на доступ к данным этого магазина!'));
            return $this->redirect(['action' => 'index']);
        }
        */

        if ($this->request->is('post')) {
            $order_email = $this->request->data('order_email');
            $order_phone = $this->request->data('order_phone') ?? 0;
            $order_firstname = $this->request->data('order_firstname') ?? 0;
            $order_lastname = $this->request->data('order_lastname') ?? 0;
            $order_city = $this->request->data('order_city') ?? 0;
            $order_street = $this->request->data('order_street') ?? 0;
            $order_building = $this->request->data('order_building') ?? 0;
            $status = $this->request->data('status') ?? 0;
            if ($city_id == null) {
                return $this->redirect($this->referer());
            }
            $saved_data = array(
                'order_email' => $order_email,
                'order_phone' => $order_phone,
                'status' => "$status",
                'order_city' => $order_city,
                'order_firstname' => $order_firstname,
                'order_lastname' => $order_lastname,
                'order_street' => $order_street,
                'order_building' => $order_building,
            );
            $this->Order->id = $id;

            // сохранение связанных данных
            if ($this->Order->save($saved_data)) {
                $this->Flash->set(__('заказ успешно сохранен'));
                $this->Activity->addOrdererActivity("edit_order", "order", $this->ShopOwnerCom->shop_owner_id(), "Заказ №" . $this->Order->id);
                return $this->redirect(['action' => 'edit', 'id' => $id]);
            }

        }

        $shops = $this->Shop->find("all",
            array(
                'conditions' =>
                    array(//'id' => $id
                    ),
                'order' => 'shop_name ASC'
            )
        );
        $this->set('shops', $shops);
        $order = $this->Order->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $id
                    ),
            )
        );
        $this->set('order', $order);

        $order_comments = $this->Order_Comment->find("all",
            array(
                'conditions' =>
                    array(
                        'order_id' => $id
                    ),
                'order' => array('id' => 'DESC'),
            )
        );
        // russian regions
        $russian_country_id = 218;
        $regions = $this->Region->find("all",
            array(
                'conditions' =>
                    array(
                        'country_id' => $russian_country_id
                    ),
                'order' => 'name ASC'
            )
        );

        $regions_list = [];
        foreach ($regions as $region) {
            $region = $region['Region']['id'];
            if (!in_array($region, $regions_list)) {
                $regions_list[] = $region;
            }
        }

        $cities = $this->City->find("all",
            array(
                'conditions' =>
                    array(
                        'region_id' => $regions_list
                    ),
                'order' => 'name ASC'
            )
        );
        $this->set('cities', $cities);

        // список товаров в заказе
        $order_products_list = $this->OrderCom->shop_order_products($id);
        $order_product_results = [];
        foreach ($order_products_list as $product) {
            $order_product_result = [];
            $p_id = $product['Order_Product']['product_id'];
            $product_data = $this->Product->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $p_id
                        ),
                )
            )['Product'];
            $category_id = $product_data['category_id'];
            $category_name = $this->ProductCom->buildProductNameByCategories($category_id);
            $order_product_result['product_name'] = $category_name['name'] . " / " . $product_data['product_name'];
            $order_product_result['amount'] = $product['Order_Product']['amount'];
            $order_product_result['order_price'] = $product['Order_Product']['order_price'];
            $order_product_result['product_id'] = $p_id;
            //получение базовой цены
            $product_shop = $this->Shop_Product->find("first",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $p_id
                        ),
                )
            )['Shop_Product'];
            $order_product_result['base_price'] = $product_shop['base_price'];
            $order_product_results[] = $order_product_result;
        }

        $this->set('order_products_list', $order_product_results);
        $this->set('title', "редактирование заказа №$id от " . lang_calendar($order['Order']['created']));
        $this->set('content_title', "Редактирование заказа <b>№$id</b> от <b>" . lang_calendar($order['Order']['created']) . " </b>");

        $this->set('order_comments', $order_comments);
        $this->set('order_id', $id);
    }

    // TODO доделать вывод детальной страницы заказа
    public function view()
    {
        $id = $this->request->param('id');

        //проверка владения магазином
        //TODO сделать проверку на наличие прав для редактирования заказа
        /*
        if(!$this->ShopOwnerCom->has_owner_rights($id)){
            $this->Flash->set(__('У вас нет права на доступ к данным этого магазина!'));
            return $this->redirect(['action' => 'index']);
        }
        */

        $shops = $this->Shop->find("all",
            array(
                'conditions' =>
                    array(//'id' => $id
                    ),
                'order' => 'shop_name ASC'
            )
        );
        $this->set('shops', $shops);
        $order = $this->Order->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $id
                    ),
            )
        );
        $this->set('order', $order);

        $order_comments = $this->Order_Comment->find("all",
            array(
                'conditions' =>
                    array(
                        'order_id' => $id
                    ),
                'order' => array('id' => 'DESC'),
            )
        );
        // russian regions
        $russian_country_id = 218;
        $regions = $this->Region->find("all",
            array(
                'conditions' =>
                    array(
                        'country_id' => $russian_country_id
                    ),
                'order' => 'name ASC'
            )
        );

        $regions_list = [];
        foreach ($regions as $region) {
            $region = $region['Region']['id'];
            if (!in_array($region, $regions_list)) {
                $regions_list[] = $region;
            }
        }

        $cities = $this->City->find("all",
            array(
                'conditions' =>
                    array(
                        'region_id' => $regions_list
                    ),
                'order' => 'name ASC'
            )
        );
        $this->set('cities', $cities);

        // список товаров в заказе
        $order_products_list = $this->OrderCom->shop_order_products($id);
        $order_product_results = [];
        foreach ($order_products_list as $product) {
            $order_product_result = [];
            $p_id = $product['Order_Product']['product_id'];
            $product_data = $this->Product->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $p_id
                        ),
                )
            )['Product'];
            $category_id = $product_data['category_id'];
            $category_name = $this->ProductCom->buildProductNameByCategories($category_id);
            $order_product_result['product_name'] = $category_name['name'] . " / " . $product_data['product_name'];
            $order_product_result['amount'] = $product['Order_Product']['amount'];
            $order_product_result['order_price'] = $product['Order_Product']['order_price'];
            $order_product_result['product_id'] = $p_id;
            //получение базовой цены
            $product_shop = $this->Shop_Product->find("first",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $p_id
                        ),
                )
            )['Shop_Product'];
            $order_product_result['base_price'] = $product_shop['base_price'];
            $order_product_results[] = $order_product_result;
        }

        $this->set('order_products_list', $order_product_results);
        $action_title =  "Заказ №$id от " . lang_calendar($order['Order']['created']);
        $action_title_html = "Заказ <b>№$id</b> от <b>" . lang_calendar($order['Order']['created']) . " </b>";
        $this->set('title', $action_title);

        $this->Breadcrumbs->add($action_title_html, Router::url(array('plugin' => false, 'controller' => 'order', 'action' => 'view', 'id' => $id)));

        $this->set('order_comments', $order_comments);
        $this->set('order_id', $id);
    }

    // добавление комменитария к заказу
    public function add_comment()
    {
        $order_id = $this->request->param('id');
        if ($this->request->is('post')) {
            $order = $this->Order->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $order_id
                        ),
                )
            );
            $shop_id = $order['Order']['shop_id'];
            $comment = $this->request->data('comment');
            $saved_data = array(
                'order_id' => $order_id,
                'shop_id' => $shop_id,
                'comment' => $comment,
            );
            if ($this->Order_Comment->save($saved_data)) {
                $id = $this->Order_Comment->id;
                $this->Flash->set(__('Комментарий к заказу добавлен'));
                $this->Activity->addOrdererActivity("add_order_comment", "order", $this->ShopOwnerCom->shop_owner_id(), "Добавлен комментарий #$id к заказу №" . $order_id);
                return $this->redirect(['controller' => 'order', 'action' => 'comments/' . $order_id]);
            }
        }
        $this->set("order_id", $order_id);
    }

    // удаление комменитария из заказу
    public function delete_comment()
    {
        $comment_id = $this->request->param('comment_id');
        $order_id = $this->request->param('order_id');
        if ($comment_id <= 0) {
            die("Передан некорректный идентификатор комментария");
        }
        // проверка наличия товара
        $order_comment = $this->Order_Comment->find("first",
            array('conditions' =>
                array(
                    'id' => $comment_id
                )
            )
        );
        if (count($order_comment) <= 0) {
            die("Комментарий не найден");
        }
        $this->Order_Comment->id = $comment_id;
        $this->Order_Comment->delete();
        $this->Flash->set(__('Комментарий успешно удален'));
        return $this->redirect(['controller' => 'order', 'action' => 'comments/' . $order_id]);
    }

    // добавление комменитария к заказу
    public function comments()
    {
        $order_id = $this->request->param('id');

        $this->set('title', "Комментарии к заказу №$order_id");
        $this->set('content_title', "Комментарии к заказу №$order_id");
        $order_comments = $this->Order_Comment->find("all",
            array(
                'conditions' =>
                    array(
                        'order_id' => $order_id
                    ),
                'order' => array('id' => 'DESC'),
            )
        );

        $this->set('order_comments', $order_comments);
        $this->set("order_id", $order_id);
    }

    /*
    public function index()
    {
        $offer_id = $this->request->param('id');
        $offer = $this->Offer->find("first",
            array('conditions' =>
                array(
                        'id' => $offer_id
                ),
                'joins' => array(
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Category.id = Product.category_id'
                        )
                    )
                ),
                'fields' => array(
                    'Category.*',
                    'Product.*',
                    //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Map.created) AS active_time',
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
            )
        );


        $url = "http://mail.ru";
        return $this->redirect($url);
    }
    */

    public function stat()
    {
        $this->set('stat_last_orders', $this->stat_last_orders());
        $this->render('/Statistic/orders');
    }

    public function stat_last_orders()
    {
        $dates = [];
        for ($x = 0; $x <= 60; $x++) {
            $dates[] = date('Y-m-d', strtotime('-' . $x . ' days', strtotime('now')));
        }
        $dates = array_reverse($dates);
        $product_data_array = [];
        foreach ($dates as $date) {
            /*
            $views = $this->Product_View->find("count",
                array('conditions' =>
                    array(
                        'DATE(created) - DATE("' . $date . ' 00:00:00")' => 0
                    ),
                )
            );*/

            $orders = $this->Order->find("count",
                array('conditions' =>
                    array(
                        'DATE(created) - DATE("' . $date . ' 00:00:00")' => 0
                    ),
                )
            );

            $product_data_array[] = array(
                'period' => $date,
                //'views' => $views,
                'orders' => $orders
            );
        }
        return json_encode($product_data_array);
    }

}