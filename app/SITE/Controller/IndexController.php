<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class IndexController extends AppController
{
    public  $uses       = [
        'Company',
        'Company_Agent',
        'Complaint',
        'Product',
        'Product_Param',
        'ProductCategory',
        'Ticket',
        'Ticket_Message',
    ];

    public  $components = [
        'CompanyCom',
        'Session',
    ];

    public  $layout     = "default";

    private $use_memcached;

    private $memcache_host;

    private $memcache_port;

    private $memcache_time_seconds;

    public function beforeFilter()
    {
        $auth_error      = (isset($this->request->query['auth_error'])) ? $this->request->query['auth_error'] : null;
        $auth_error_text = (isset($this->request->query['auth_error_text'])) ? $this->request->query['auth_error_text'] : null;
        $this->set('auth_error', $auth_error);
        $this->set('auth_error_text', $auth_error_text);
        $show_login_form = true;
        $this->set('show_login_form', $show_login_form);
        $this->set('show_login_form', $show_login_form);
        if (!function_exists("memcache_connect")) {
            $this->use_memcached = false;
        } else {
            $this->use_memcached         = true;
            $this->memcache_host         = 'localhost';
            $this->memcache_port         = 11211;
            $this->memcache_time_seconds = 3600;
        }
        parent::beforeFilter();
    }

    public function index()
    {
        $this->Api->response_api(null, "success");
        exit;
    }
}