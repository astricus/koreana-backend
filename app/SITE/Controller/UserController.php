<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class UserController extends AppController
{
    public $uses = array(
        'Country',
        'Shop_Product',
        'Shop',
        'City',
        'Region',
        'Company',
        'Orderer',
        'Token',

    );

    private $valid_hosts = [
        'terpikka.ru',
        'terpikka.local',
    ];

    public $components = array(
        'Activity',
        'CityCom',
        'Cookie',
        'CompanyCom',
        'EmailCom',
        'Flash',
        'OrderCom',
        'ProductCom',
        'Session',
        'Uploader',
        'UserCom',
        'Validator',
        'ShortLink',
        'Sms',
    );

    public $layout = "default";

    public $spb_city_id = 174;

    private $MIN_PASS_LENGTH = 8;

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Cookie->name = 'token';
        $this->Cookie->key = null;
        //$this->Cookie->domain = 'terpikka.ru';
    }

    // ПОЛЬЗОВАТЕЛЬСКИЕ ДАННЫЕ

    public function user_data()
    {
        $this->UserCom->auth_requires();
        $user_id = $this->UserCom->getUserId();
        if($user_id==null){
            response_api(["error" => UserComponent::AUTH_REQUIRES_MESSAGE], "error");
            exit;
        }

        $user_data = $this->UserCom->getUserData($user_id);

        $subscription = $this->UserCom->getUserSubscriptions($user_id);

        $compared_product = $this->UserCom->getCompareProductList($user_id);

        $offer_count = $this->UserCom->getCartOffersCount($user_id);

        $favourite = [];
        if ($this->UserCom->getFavouriteList($user_id)) {
            foreach ($this->UserCom->getFavouriteList($user_id) as $v) {
                $favourite[] = $v['content_id'];
            }
        }


        $user_avatar = null;
        if (!empty($user_data['Orderer']['avatar'])) {
            $user_avatar = $this->UserCom->getUserImagesDir($this->UserCom->getUserId(), "relative") . DS . $user_data['Orderer']['avatar'];
        }

        if($user_data['Orderer']['middlename']!=="" OR $user_data['Orderer']['middlename']!=="null") {
            $middlename = $user_data['Orderer']['middlename'];
        }  else {
            $middlename = null;
        }
        if($user_data['Orderer']['lastname']!==""  OR $user_data['Orderer']['lastname']!=="null") {
            $lastname = $user_data['Orderer']['lastname'];
        }  else {
            $lastname = null;
        }
        $arr = [
            'firstname' => $user_data['Orderer']['firstname'],
            'lastname' => $lastname,
            'middlename' => $middlename,
            'sex' => $user_data['Orderer']['sex'],
            'birthday' => $user_data['Orderer']['birthday'],
            'phone_number' => prepare_phone($user_data['Orderer']['phone_number']),
            'email' => $user_data['Orderer']['email'],
            'avatar' => $user_avatar,
            'subscriptions' => $subscription,
            'favourite_products_ids' => $favourite,
            'compare_products_ids' => $compared_product,
            'offer_count' => $offer_count,
        ];

        response_api($arr, "success");
        exit();
    }

    private function checkEmailExists($email)
    {

        $check = $this->Orderer->find("count",
            array(
                'conditions' =>
                    array(
                        'email' => $email
                    ),
            )
        );

        return $check;
    }

    // АВТОРИЗАЦИЯ API
    public function login()
    {
        if ($this->request->is('post')) {
            //почта
            $mail = $this->request->data('login');
            //пароль
            $password = $this->request->data('password');
            $hashed_pass = get_hash(Configure::read('USER_AUTH_SALT'), $password);

            if (!empty($mail)) {

                $check_user = $this->Orderer->find('first', array('conditions' => array('password' => $hashed_pass, 'email' => $mail)));
                if (count($check_user) > 0) {
                    //удачная авторизация
                    $this->Session->write('User', $mail);
                    $user_data = $this->Orderer->find('first', array('conditions' => array('email' => $mail)));
                    $user_id = $user_data['Orderer']['id'];
                    // отдавать данные в процессе авторизации

                    $token = $this->generateToken($user_id);
                    $this->Session->write('token', $token);
                    $user_data_arr = [];
                    $user_data_arr['id'] = $user_data['Orderer']['id'];
                    $user_data_arr['firstname'] = $user_data['Orderer']['firstname'];
                    $user_data_arr['lastname'] = $user_data['Orderer']['lastname'];
                    $user_data_arr['email'] = $user_data['Orderer']['email'];
                    $user_data_arr['city_id'] = $user_data['Orderer']['city_id'] ?? $this->spb_city_id;
                    $user_data_arr['phone_number'] = $user_data['Orderer']['phone_number'];
                    $result = [
                        'auth' => 'success',
                        'token' => $token,
                        'data' => $user_data_arr,
                    ];
                    $this->saveToken($user_id, $token);
                    $big_time = 60 * 60 * 24 * 30 * 12 * 1000;
                    if ($this->request->accepts('application/json')) {
                        response_api($result, "success");
                    } else {
                        $host = $this->request->host();
                        $cookie_domain = $host;
                        setcookie("token", $token, time() + $big_time, "/", $cookie_domain);
                        if (in_array($host, $this->valid_hosts)) {
                            if (key_exists("HTTP_ORIGIN", $_SERVER)) {
                                $this->redirect($_SERVER['HTTP_ORIGIN']);
                            } else {
                                $this->redirect("https://terpikka.ru");
                            }
                        } else {
                            $this->redirect(site_url());
                        }
                    }
                    exit;
                    /*
                    $this->loadModel('Userauth');
                    $auth_data = array('user_id' => $user_id, 'ip' => get_ip(), 'browser' => get_ua(), 'os' => get_os());
                    $this->Userauth->save($auth_data);
                    */
                } else {
                    $auth_error_text = "WRONG_LOGIN_OR_PASSWORD";
                    response_api($auth_error_text, "error");
                    exit;
                }
            } else {
                $auth_error_text = "WRONG_LOGIN_OR_PASSWORD";
                response_api($auth_error_text, "error");
                exit;
            }
        }
    }

    /**
     * @param $user_id
     * @param $user_data
     * @return array
     */
    private function forceLogin($user_id, $user_data)
    {
        //запускается сразу после авторизации, по просьбе фронта :)
        $token = $this->generateToken($user_id);
        $this->Session->write('token', $token);
        $user_data_arr = [];
        $user_data_arr['id'] = $user_id;
        $user_data_arr['firstname'] = $user_data['firstname'];
        $user_data_arr['lastname'] = "";//$user_data['lastname'];
        $user_data_arr['email'] = $user_data['email'];
        $user_data_arr['city_id'] = 0;//$user_data['city_id'];
        $user_data_arr['phone_number'] = "";//$user_data['phone_number'];
        $user_data_arr['middlename'] = "";
        $user_data_arr['sex'] = "";
        $user_data_arr['birthday'] = "";
        $user_data_arr['avatar'] = "";
        $user_data_arr['subscriptions'] = $this->UserCom->getUserSubscriptions($user_id);
        $user_data_arr['favourite_products_ids'] = [];
        $user_data_arr['compare_products_ids'] = [];
        $user_data_arr['offer_count'] = [];

        $result = [
            'auth' => 'success',
            'success' => true,
            'token' => $token,
            'data' => $user_data_arr,
        ];
        $this->saveToken($user_id, $token);
        $big_time = 60 * 60 * 24 * 30 * 12 * 1000;
        $host = $this->request->host();
        $cookie_domain = $host;
        setcookie("token", $token, time() + $big_time, "/", $cookie_domain);
        return $result;
    }

    public function logout()
    {
        $token = $this->request->data('token') ?? $this->request->query('token');
        if (!empty($token)) {
            if ($this->UserCom->checkToken($token)) {
                $this->UserCom->logout($token);
                $result = [
                    'logout' => 'success',
                ];
                response_api($result, "success");
            } else {
                $result = [
                    'error' => 'token not found',
                ];
                response_api($result, "error");
            }
        } else {
            $result = [
                'error' => 'token not found',
            ];
            response_api($result, "error");
        }
        exit;
    }

    public function login_form()
    {

    }

    /**
     * @param $user_id
     * @return string
     */
    private function generateToken($user_id)
    {
        return md5(time() . " token " . $user_id . "Orderer");
    }

    /**
     * @param $user_id
     * @param $token
     * @return string
     */
    private function saveToken($user_id, $token)
    {
        $token_data = [
            'token' => $token,
            'user_id' => $user_id,
            'user_type' => 'Orderer',
            'expired' => date('Y-m-d', strtotime('+1 year'))
        ];
        $this->Token->save($token_data);
    }

    // РЕГИСТРАЦИЯ

    public function register_form()
    {
    }

    public function register()
    {
        $errors = [];
        if ($this->request->is('get')) {
            $errors[] = "registration  action requires post method";
            response_api($errors, "error");
            exit;
        }
        $data = $this->params['data'];

        if (empty($data['email'])) {
            $errors[] = "required field is empty: email";
            exit;
        }

        // валидация фио, почты, телефона
        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $name_regexp = "/^[a-zA_ZА-Яа-яёЁ-]{2,32}$/iu";

        $data['firstname'] = trim($data['firstname']);
        $data['email'] = trim($data['email']);
        $data['password'] = trim($data['password']);

        $data['firstname'] = mb_ucfirst(mb_strtolower($data['firstname']));

        if (!preg_match($mail_regexp, $data['email'])) {
            $errors[] = "incorrect field: email";
        }

//		if (!preg_match($phone_regexp, $data['phone'])) {
//            $errors[] = "incorrect field: phone number";
//		}

//        if (!empty($data['Register']['lastname'])) {
//            if (!preg_match($name_regexp, $data['lastname'])) {
//                $errors[] = "incorrect field: lastname";
//            }
//        }

        if (!$this->Validator->valid_firstname($data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!$this->Validator->valid_password($data['password'])) {
            $errors[] = "incorrect field: password";
        }

        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if ($this->checkEmailExists($data['email'])) {
            $errors[] = "email exists ";
        }

        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }
        //данные пользователя
        $user_data["firstname"] = isset($data["firstname"]) ? $data["firstname"] : null;
        //$user_data["lastname"] = isset($data["lastname"]) ? $data['Register']["lastname"] : null;
        $user_data["email"] = isset($data["email"]) ? $data["email"] : null;

        $user_data["main_foto"] = "";
        $user_data["last_activity"] = date("Y-m-d H:i:s");

        $user_data["mail_key"] = md5(time() . $user_data["email"]);
        //генерация пароля с солью
        $real_pwd = $data["password"];
        $user_data["password"] = get_hash(Configure::read('USER_AUTH_SALT'), $real_pwd);

        $this->Orderer->save($user_data);

        $user_id = $this->Orderer->getLastInsertId();
        // сохранение подписки
        if (isset($data['subscription_checked']) AND ($data['subscription_checked'] == "on" OR $data['subscription_checked'] == "true")) {
            $this->UserCom->saveUserSubscription($user_id, "discount");
            $this->UserCom->saveUserSubscription($user_id, "articles");
        }

        //ключ активации
        $mail_key_salt = Configure::read('MAIL_KEY_SALT');
        $mail_key = generate_mail_key($user_id, $mail_key_salt);
        $this->Orderer->id = $user_id;
        $this->Orderer->save(array('mail_key' => $mail_key));

        //force login
        $login_data = $this->forceLogin($user_id, $user_data);

        //запись номера телефона
        /*
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $user_phone = $user_data["User"]["phone"];
        if ($user_phone != null AND preg_match($phone_regexp, $user_phone)) {
            $data_to_save = array('user_id' => $user_id, 'phone' => $user_phone);
            $this->UserPhone->save($data_to_save);
        }
        */

        $message_data = $this->getRegistrationMessage($data["firstname"], $real_pwd, $mail_key, $user_data["email"]);
        response_api($login_data, "success");
        /*
        $this->EmailCom->sendEmailNow(
            $user_data["email"],
            "",
            "REGISTER_ON_PROJECT" . " " . site_url(),
            'user_register_mail_layout',
            'user_register_mail_template',
            $message_data,
            $attachment = "",
            $start_sending = ""
        );
        */

        // отправка письма через очередь
        /*
        $this->EmailCom->sendEmailLater(
            $user_data["email"], "", "REGISTER_ON_PROJECT" . " " . site_url(),
            'user_register_mail_layout',
            'user_register_mail_template',
            $message_data,
            $this->EmailCom->critical_priority,
            "",
            date('Y-m-d H:i:s'),
            0
        );*/

        $this->EmailCom->sendRegisterOrdererEmail($data["firstname"], $user_data["email"], "REGISTER_ON_PROJECT" . " " . site_url(), $message_data);
        exit;
    }


    // Смена пароля
    public function changePassword()
    {
        if ($this->UserCom->is_auth()) {
            if (count($this->params->data) > 0) {
                // Параметры пришли POST данными или строкой json
                if (!empty($this->request->data('current_password')) AND !empty($this->request->data('new_password'))) {
                    $hash_current_pass = get_hash(Configure::read('USER_AUTH_SALT'), $this->request->data('current_password'));
                    $new_password = trim($this->request->data('new_password'));
                } else {
                    $data = json_decode(array_key_first($this->params->data));
                    $hash_current_pass = get_hash(Configure::read('USER_AUTH_SALT'), $data->current_password);
                    $new_password = trim($data->new_password);
                }

                if ($new_password == '' or mb_strlen($new_password) < $this->MIN_PASS_LENGTH) {
                    response_api(['error' => 'Пароль меньше 8 символов'], 'error');
                    exit();
                }

                $user_data = $this->UserCom->user_data();
                $user_id = $user_data['Orderer']['id'];

                if ($hash_current_pass == $user_data['Orderer']['password']) {
                    $res = $this->UserCom->changePassword($user_id, $new_password);
                    if ($res) {
                        response_api(['success' => true], 'success');
                    } else {
                        response_api(['error' => 'Новый пароль не сохранен'], 'error');
                    }
                } else {
                    response_api(['error' => 'Пароли не совпали'], 'error');
                }
            } else {
                response_api(['error' => 'Данные не пришли'], 'error');
            }
        } else {
            response_api(['error' => 'Пользователь не авторизован'], 'error');
        }
        exit();
    }

    // Отправить письмо с новым паролем
    public function resetPasswordSendNew()
    {
        if (!empty($this->request->data('login'))) {
            $email = trim($this->request->data('login'));
            $user_data = $this->UserCom->getUserDataByEmail($email);

            if ($user_data) {
                //$hash = get_hash(Configure::read('USER_AUTH_SALT'), $this->UserCom->generateRandomPass(10));
                $new_password = $this->UserCom->generateRandomPass(10);
                $hash_new_password = get_hash(Configure::read('USER_AUTH_SALT'), $new_password);

                $text_message = 'Временный пароль от личного кабинета: ' . $new_password;//.Router::url('/', true).'route?token='.$hash;
                $recipiet_name = $user_data['firstname'] . ' ' . $user_data['lastname'];

                $email_send = $this->EmailCom->sendBaseEmail($recipiet_name, $email, 'Новый пароль - terpikka.ru', $text_message);
                if ($email_send) {
                    $this->Orderer->id = $user_data['id'];
                    $this->Orderer->save(['password' => $hash_new_password]);
                    response_api(['success' => true], 'success');
                } else {
                    response_api(['error' => 'Письмо с новым паролем не ушло'], 'error');
                }
            } else {
                response_api(['error' => 'Пользователь с таким логином не найден'], 'error');
            }
        } else {
            response_api(['error' => 'Логин не передан'], 'error');
        }

        exit();
    }

    // Сменить пароль
    public function resetPassword()
    {

    }

    //Загрузка фото аватара
    public function uploadAvatarPhoto()
    {
        $file_max_size = 5; // Максимальный размер загружаемого файла
        if ($this->UserCom->is_auth()) {
            if ((isset($_FILES['file'])) AND ($_FILES['file']['size'] > 0) AND (!empty($_FILES['file']['name']))) {
                if ($_FILES['file']['size'] > $file_max_size * 1024 * 1024) {
                    response_api(['error' => 'Файл больше ' . $file_max_size . ' MB'], 'error');
                    exit();
                }

                $res = $this->UserCom->saveAvatarPhoto($_FILES['file']);
                if ($res['status'] == 'ok') {
                    $data = [
                        'success' => true,
                        'avatar' => $res['url'],
                    ];
                    response_api($data, 'success');
                } else {
                    response_api(['error' => $res['msg']], 'error');
                }
            } else {
                response_api(['error' => 'Файла нет'], 'error');
            }
        } else {
            response_api(['error' => 'Пользователь не автризован'], 'error');
        }
        exit();
    }

    // Удаление фото-аватара пользователя
    public function deleteAvatarPhoto()
    {
        if ($this->UserCom->is_auth()) {
            $res = $this->UserCom->deleteAvatarPhoto();
            if ($res['status'] == 'success') {
                response_api([$res['status'] => $res['msg']], $res['status']);
            } else {
                response_api([$res['status'] => $res['msg']], $res['status']);
            }
        } else {
            response_api(['error' => 'Пользователь не автризован'], 'error');
        }
        exit();
    }

    // Последние просмотренные товары
    public function lastView()
    {
        $count = 10; // Количество последних просмотренных товаров
        if ($this->UserCom->is_auth()) {
            $user_id = $this->UserCom->getUserId();
            $lastView = $this->ProductCom->getLastProductViewForUser($user_id, $count);
            if ($lastView) {
                response_api(['product_id' => $lastView], 'success');
            } else {
                response_api(['success' => []], 'success');
            }
        } else {
            response_api(['error' => 'Пользователь не автризован'], 'error');
        }
        exit();
    }

    /**
     * @param $firstname
     * @param $password
     * @param $mail_key
     * @param $email
     * @return string
     */
    private function getRegistrationMessage($firstname, $password, $mail_key, $email)
    {
        $message_data = "Вы только что зарегистрировались на проекте" . " " . site_url() . "<br>";
        $message_data .= "Ваши регистрационные данные<br> почтовый ящик: " . $email;
        $message_data .= "Имя : " . $firstname;
        $message_data .= "Пароль: " . $password . "<br>";
        $message_data .= "<a href='" . site_url() . "activate_account/user/" . $mail_key . "'>Ссылка активации аккаунта</a> <br><br>";
        return $message_data;
    }

    //УПРАВЛЕНИЕ ПРОФИЛЕМ И ЛИЧНЫМИ ДАННЫМИ

    public function saveProfile()
    {
        /*
         * имя, фамилия, отчество, пол, д.р., телефон, почта, город, адреса (массив), пароль новый, пароль старый, изображение, подписка на россылку Полезные статьи, подписка скидки и спецпредложения
         *
         * */
        $this->UserCom->auth_requires();
        $errors = [];
        $data = $this->params['data'];

        // валидация фио, почты, телефона
        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $data['firstname'] = trim($data['firstname']);
        $data['email'] = trim($data['email']);
        $data['password'] = trim($data['password']);

        if (!empty($data['email'])) {
            if ($this->checkEmailExists($data['email']) > 0) {
                $errors[] = "email exists ";
            }
        }

        if (!preg_match($mail_regexp, $data['email'])) {
            $errors[] = "incorrect field: email";
        }

//		if (!preg_match($phone_regexp, $data['phone'])) {
//            $errors[] = "incorrect field: phone number";
//		}

//        if (!empty($data['Register']['lastname'])) {
//            if (!preg_match($name_regexp, $data['lastname'])) {
//                $errors[] = "incorrect field: lastname";
//            }
//        }

        if (!$this->Validator->valid_firstname($data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!$this->Validator->valid_password($data['password'])) {
            $errors[] = "incorrect field: password";
        }

        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }
        //данные пользователя
        $user_data["firstname"] = isset($data["firstname"]) ? $data["firstname"] : null;
        //$user_data["lastname"] = isset($data["lastname"]) ? $data['Register']["lastname"] : null;
        $user_data["email"] = isset($data["email"]) ? $data["email"] : null;
        //$user_data["city_id"] = isset($user_data["city_id"]) ? $data["city_id"] : 0;
        //$user_data["country_id"] = isset($user_data["country_id"]) ? $data["country_id"] : 0;
        $user_data["money_amount"] = 0;
        $user_data["main_foto"] = "";
        $user_data["last_activity"] = date("Y-m-d H:i:s");
        $user_data["uptime"] = date("Y-m-d H:i:s");
        //$user_data["User"]["phone"] = isset($data['Register']["phone"]) ? $data['Register']["phone"] : null;
        $user_data["mail_key"] = md5(time() . $user_data["email"]);
        //генерация пароля с солью
        $real_pwd = $data["password"];
        $user_data["password"] = get_hash(Configure::read('USER_AUTH_SALT'), $real_pwd);

        $this->Orderer->save($user_data);

        $user_id = $this->Orderer->getLastInsertId();

        //ключ активации
        $mail_key_salt = Configure::read('MAIL_KEY_SALT');
        $mail_key = generate_mail_key($user_id, $mail_key_salt);
        $this->Orderer->id = $user_id;
        $this->Orderer->save(array('mail_key' => $mail_key));

        //запись номера телефона
        /*
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $user_phone = $user_data["User"]["phone"];
        if ($user_phone != null AND preg_match($phone_regexp, $user_phone)) {
            $data_to_save = array('user_id' => $user_id, 'phone' => $user_phone);
            $this->UserPhone->save($data_to_save);
        }
        */
        exit;
    }

    public function saveSubscription()
    {
        if ($this->UserCom->is_auth()) {
            $data = $this->request->data;
            $user_id = $this->UserCom->getUserId();
            $result = "";
            if ($data['name']== "discount") {
                if($data['value'] == "true" OR $data['value'] == "false"){
                    $this->UserCom->saveUserSubscription($user_id, "discount", $data['value']);
                }
                if($data['value'] == "true"){
                    $result .= "Вы успешно подписались на подписку discount ";
                } else if($data['value'] == "false"){
                    $result .= "Вы успешно отписались от подписки discount ";
                }
            }
            if ($data['name']== "articles"){
                if($data['value'] == "true" OR $data['value'] == "false") {
                    $this->UserCom->saveUserSubscription($user_id, "articles", $data['value']);
                }
                if($data['value'] == "true"){
                    $result .= "Вы успешно подписались на подписку articles ";
                } else if($data['value'] == "false"){
                    $result .= "Вы успешно отписались от подписки articles ";
                }
            }
            response_api(['success' => $result], 'success');

        } else {
            response_api(['error' => 'Пользователь не авторизован'], 'error');
        }
        exit;
    }

    // Изменение профиля
    public function editProfile()
    {
        if ($this->UserCom->is_auth()) {
            $data = [];
            $error = [];
            $user_id = $this->UserCom->getUserId();
            $validation = $this->Validator;
            $param = $this->params['data'];

            if (!empty($param['firstname'])) {
                if ($validation->valid_firstname($param['firstname'])) {
                    $data['firstname'] = $param['firstname'];
                } else {
                    $error[] = 'Не корректное имя';
                }
            }

            if (!empty($param['lastname'])) {
                if ($validation->valid_lastname($param['lastname'])) {
                    $data['lastname'] = $param['lastname'];
                } else {
                    $error[] = 'Не корректная Фамилия';
                }
            }

            if (!empty($param['middlename'])) {
                if ($validation->valid_middlename($param['middlename'])) {
                    $data['middlename'] = $param['middlename'];
                } else {
                    $error[] = 'Не корректное отчество';
                }
            }

            if (!empty($param['sex'])) {
                if ($validation->valid_sex($param['sex'])) {
                    $data['sex'] = $param['sex'];
                } else {
                    $error[] = 'Не корректный пол';
                }
            }

            if (!empty($param['birthday'])) {
                if ($validation->valid_date($param['birthday'])) {
                    $data['birthday'] = date('Y-m-d', strtotime($param['birthday']));
                } else {
                    $error[] = 'Не корректная дата рождения';
                }
            }

            if (!empty($param['email'])) {
                if ($validation->valid_mail($param['email'])) {
                    $data['email'] = $param['email'];
                } else {
                    $error[] = 'Не корректный email';
                }
            }

            if (!empty($param['phone'])) {
                if ($validation->valid_phone_number($param['phone'])) {
                    $data['phone_number'] = $param['phone'];
                } else {
                    $error[] = 'Не корректный телефон';
                }
            }

            if (count($error) > 0) {
                $str = implode(", ", $error);
                response_api(['error' => $str], 'error');
            } else {
                $this->Orderer->id = $user_id;
                if (isset($data['middlename']) AND $data['middlename'] == "null") {
                    $data['middlename'] = null;
                }
                if (isset($data['lastname']) AND $data['lastname'] == "null") {
                    $data['lastname'] = null;
                }
                $this->Orderer->save($data);
                response_api(['success' => 'Данные пользователя изменены'], 'success');
            }
        } else {
            response_api(['error' => 'Пользователь не авторизован'], 'error');
        }
        exit();
    }

    public function sendSmsConfirmPhone()
    {
        if ($this->UserCom->is_auth()) {
            $user_data = $this->UserCom->user_data()['Orderer'];
            $phone = $user_data['phone_number'];
            if ($phone != '' or !is_null($phone)) {
                $code_confirm = rand(100000, 999999);
                $res = $this->Sms->sendSms($phone, 'user_phone_confirm', ['code_confirm' => $code_confirm]);
                if ($res['status'] == 'ok') {
                    $this->Orderer->id = $user_data['id'];
                    $this->Orderer->save(['last_code_sms' => $code_confirm]);

                    response_api(['success' => true], 'success');
                } else {
                    response_api(['error' => $res['msg']], 'error');
                }
            } else {
                response_api(['error' => 'Нет номера телефона в БД'], 'error');
            }
        } else {
            response_api(['error' => 'Пользователь не автризован'], 'error');
        }
        exit();
    }

    public function confirmPhone()
    {
        if ($this->UserCom->is_auth()) {
            $user_data = $this->UserCom->user_data()['Orderer'];
            $code = $this->request->data['code'] ?? $this->request->query('code');
            $code = trim($code);

            if ($code == $user_data['last_code_sms']) {
                $this->Orderer->id = $user_data['id'];
                $this->Orderer->save(['phone_confirmed' => 1]);

                response_api(['success' => true], 'success');
            } else {
                response_api(['error' => 'Не верный код'], 'error');
            }
        } else {
            response_api(['error' => 'Пользователь не автризован'], 'error');
        }
        exit();
    }

    // ФУНКЦИОНАЛ ИЗБРАННОГО

    public function clearFavourite()
    {
        $this->UserCom->auth_requires();
        $this->UserCom->clearFavourite($this->UserCom->getUserId(), "product");
        $result = ['success' => true];
        response_api($result, "success");
        exit;
    }

    public function deleteProductFromFavourite()
    {
        $product_id = $this->request->data['product_id'] ?? $this->request->query('product_id');
        if (intval($product_id) == 0) {
            response_api(["error" => " product with id $product_id not found "], "error");
            exit;
        }
        $product = $this->Product->find("first",
            array('conditions' =>
                array(
                    'id' => $product_id,
                ),
            )
        );
        if (count($product) == 0) {
            response_api(["error" => " product_id with id $product_id not found "], "error");
            exit;
        }
        $this->UserCom->auth_requires();
        if ($this->UserCom->isElementInFavouriteList($this->UserCom->getUserId(), "product", $product_id) > 0) {
            $this->UserCom->deleteElementFromFavourite($this->UserCom->getUserId(), "product", $product_id);
            $result = ['success' => true];
            response_api($result, "success");
            exit;
        }
        $result = ['error' => "product_id #$product_id not found in favourite"];
        response_api($result, "error");
        exit;
    }

    public function deleteProducsInCategoryFromFavourite()
    {
        $category_id = $this->request->data['category_id'] ?? null;
        if (intval($category_id) == 0) {
            response_api(["error" => " category with id $category_id not found "], "error");
            exit;
        }
        $this->UserCom->auth_requires();
        $products_in_fav = $this->UserCom->getFavouriteList($this->UserCom->getUserId(), "product");
        if (count($products_in_fav) == 0) {
            response_api(['success' => true], "success");
            exit;
        }
        foreach ($products_in_fav as $product_in_fav) {
            $p_id = $product_in_fav['Favourite']['content_id'];
            $product_category_id = $this->ProductCom->getProductField("category_id", $p_id, null);
            if ($product_category_id == $category_id) {
                $this->UserCom->deleteElementFromFavourite($this->UserCom->getUserId(), "product", $p_id);
            }
        }

        $this->UserCom->clearFavourite($this->UserCom->getUserId(), "product");

        if ($this->UserCom->isElementInFavouriteList($this->UserCom->getUserId(), "product", $p_id) > 0) {
            $this->UserCom->deleteElementFromFavourite($this->UserCom->getUserId(), "product", $p_id);
        }

        $result = ['success' => true];
        response_api($result, "success");
        exit;
    }

    public function addProductToFavourite()
    {
        $this->UserCom->auth_requires();
        $product_id = $this->request->data['product_id'] ?? $this->request->query('product_id');
        if (intval($product_id) == 0) {
            response_api(["error" => " product with id $product_id not found "], "error");
            exit;
        }
        $product = $this->Product->find("first",
            array('conditions' =>
                array(
                    'id' => $product_id,
                ),
            )
        );
        if (count($product) == 0) {
            response_api(["error" => " product_id with id $product_id not found "], "error");
            exit;
        }
        if ($this->UserCom->isElementInFavouriteList($this->UserCom->getUserId(), "product", $product_id) == 0) {
            $this->UserCom->addElementToFavouriteList($this->UserCom->getUserId(), "product", $product_id);
        }
        $result = ['success' => true];
        response_api($result, "success");
        exit;
    }

    public function getFavouriteList()
    {
        $result = $this->getFavouriteListContent();
        response_api($result, "success");
        exit;
    }

    private function getFavouriteListContent()
    {
        $this->UserCom->auth_requires();
        $list = $this->UserCom->getFavouriteList($this->UserCom->getUserId(), "");
        return $list;
    }

    // ФУНКЦИОНАЛ КОРЗИНЫ

    public function clearCart()
    {
        $this->UserCom->auth_requires();
        $this->UserCom->clearCart($this->UserCom->getUserId());

        $cart_items_count = $this->UserCom->getCartOffersCount($this->UserCom->getUserId());

        $result = ['success' => true, "cart_items" => $cart_items_count];
        response_api($result, "success");
        exit;
    }

    public function deleteProductFromCart()
    {
        $this->UserCom->auth_requires();
        $offer_id = $this->request->data('offer_id') ?? $this->request->query('offer_id');
        if (intval($offer_id) == 0) {
            response_api(["error" => " offer with id $offer_id not found "], "error");
            exit;
        }
        $offer = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'id' => $offer_id,
                ),
            )
        );
        if (count($offer) == 0) {
            response_api(["error" => " offer_id with id $offer_id not found "], "error");
            exit;
        }
        $this->UserCom->deleteProductFromCart($this->UserCom->getUserId(), $offer_id);
        $cart_items_count = $this->UserCom->getCartOffersCount($this->UserCom->getUserId());
        $result = ['success' => true, "cart_items" => $cart_items_count];
        response_api($result, "success");
        exit;
    }

    public function addProductToCart()
    {
        $this->UserCom->auth_requires();
        $shop_product_id = $this->request->data('offer_id') ?? $this->request->query('offer_id');

        $amount = $this->request->data('amount') ?? $this->request->query('amount');
        if (intval($amount) <= 0) {
            response_api(["error" => " product amount must be more then zero"], "error");
            exit;
        }

        if (intval($shop_product_id) == 0) {
            response_api(["error" => " product with id $shop_product_id not found "], "error");
            exit;
        }
        $product = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'id' => $shop_product_id,
                ),
            )
        );
        if (count($product) == 0) {
            response_api(["error" => " product_id with shop_product_id $shop_product_id not found "], "error");
            exit;
        }

        $product_id = $product['Shop_Product']['product_id'];
        $price = $product['Shop_Product']['base_price'];


        $final_price = $this->ProductCom->calculateProductPriceByUser($this->UserCom->getUserId(), $shop_product_id, $price);

        if ($this->UserCom->isProductInCart($this->UserCom->getUserId(), $shop_product_id) == 0) {
            $this->UserCom->addProductToCart($this->UserCom->getUserId(), $shop_product_id, $amount, $product_id, $final_price);
        }
        $cart_items_count = $this->UserCom->getCartOffersCount($this->UserCom->getUserId());
        $result = ['success' => true, "cart_items" => $cart_items_count];
        response_api($result, "success");
        exit;
    }

    public function getCartList()
    {
        $result = $this->getCartListContent();
        response_api($result, "success");
        exit;
    }

    private function getCartListContent()
    {
        $this->UserCom->auth_requires();
        $list = $this->UserCom->getCartList($this->UserCom->getUserId());
        return $list;
    }

    public function changeAmountProductInCart()
    {
        $this->UserCom->auth_requires();

        $shop_product_id = $this->request->data['offer_id'] ?? $this->request->query('offer_id');
        $product_amount = $this->request->data['amount'] ?? $this->request->query('amount');

        if ($product_amount <= 0) {
            response_api(["error" => " product amount must be one at minimum"], "error");
            exit;
        }
        if (intval($shop_product_id) == 0) {
            response_api(["error" => " offer with id $shop_product_id not found "], "error");
            exit;
        }
        $product = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'id' => $shop_product_id,
                ),
            )
        );
        if (count($product) == 0) {
            response_api(["error" => " product_id with id $shop_product_id not found "], "error");
            exit;
        }
        if ($this->UserCom->isProductInCart($this->UserCom->getUserId(), $shop_product_id)) {
            $this->UserCom->UpdateProductCart($this->UserCom->getUserId(), $shop_product_id, $product_amount);
        } else {
            response_api(["error" => " offer_id with id $shop_product_id not found in user cart"], "error");
            exit;
        }

        $result = ['success'];
        response_api($result, "success");
        exit;
    }

    public function changeOfferCompanyInCart()
    {
        $this->UserCom->auth_requires();

        $old_offer_id = $this->request->data['old_offer_id'] ?? $this->request->query('old_offer_id');
        $new_offer_id = $this->request->data['new_offer_id'] ?? $this->request->query('new_offer_id');
        $product_amount = $this->request->data['amount'] ?? $this->request->query('amount');

        if ($product_amount <= 0) {
            response_api(["error" => " product amount must be one at minimum"], "error");
            exit;
        }
        if (intval($old_offer_id) == 0) {
            response_api(["error" => " old offer with id $old_offer_id not found "], "error");
            exit;
        }
        if (intval($new_offer_id) == 0) {
            response_api(["error" => " new offer with id $new_offer_id not found "], "error");
            exit;
        }
        if (!$this->UserCom->isProductInCart($this->UserCom->getUserId(), $old_offer_id)) {
            response_api(["error" => " offer_id with id $old_offer_id not found in user cart"], "error");
            exit;
        }
        $old_product_offer = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'id' => $old_offer_id,
                    'status' => 'active'
                ),
            )
        );
        if (count($old_product_offer) == 0) {
            response_api(["error" => " offer_id with id $old_offer_id not found "], "error");
            exit;
        }
        $product_id = $old_product_offer['Shop_Product']['product_id'];
        $product = $this->Product->find("first",
            array('conditions' =>
                array(
                    'id' => $product_id,
                ),
            )
        );
        if (count($product) == 0) {
            response_api(["error" => " product_id with id $product_id not found"], "error");
            exit;
        }

        $new_company_offer = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'id' => $new_offer_id,
                ),
            )
        );

        if (count($new_company_offer) == 0) {
            response_api(["error" => " offer with offer_id $new_offer_id not found "], "error");
            exit;
        }
        $new_offer_company_id = $new_company_offer['Shop_Product']['company_id'];
        $new_offer_company = $this->CompanyCom->getCompanyByid($new_offer_company_id);

        $new_offer_company_name = $new_offer_company[0]['Company']['company_name'];

        $product_id = $new_company_offer['Shop_Product']['product_id'];
        $price = $new_company_offer['Shop_Product']['base_price'];

        if ($this->UserCom->isProductInCart($this->UserCom->getUserId(), $new_offer_id) == 0) {
            $this->UserCom->addProductToCart($this->UserCom->getUserId(), $new_offer_id, $product_amount, $product_id, $price);
        }
        $this->UserCom->deleteProductFromCart($this->UserCom->getUserId(), $old_offer_id);
        $result = [];
        $result['success'] = true;

        $alternative_offers = $this->ProductCom->getProductOfferInAllCompanies($product_id);
        $alternative_offers_arr = [];
        foreach ($alternative_offers as $alternative_offer) {
            $of_id = $alternative_offer['Shop_Product']['id'];
            $of_company_id = $alternative_offer['Company']['id'];
            $of_company_name = $alternative_offer['Company']['company_name'];
            $alternative_offers_arr[] = [
                'offer_id' => $of_id,
                "company_offer" => ['id' => $of_company_id, 'name' => $of_company_name]
            ];
        }
        $final_price = $this->ProductCom->calculateProductPriceByUser($this->UserCom->getUserId(), $new_offer_id, $price);
        $result['offer'] = [
            'offer_id' => $new_offer_id,
            'company_offer' => ["id" => $new_offer_company_id, "name" => $new_offer_company_name],
            'amount' => $product_amount,
            'old_price' => $new_company_offer['Shop_Product']['old_price'],
            'price' => $final_price,
            'is_available' => $new_company_offer['Shop_Product']['status'],
            'delivery' => [
                "transport" => [],
                "pickup" => []
            ],
            'alternative_offers' => $alternative_offers_arr,
            "amount_available" => 0
        ];

        response_api($result, "success");
        exit;
    }

    public function makeOrder()
    {
        $this->UserCom->auth_requires();
        $user_id = $this->UserCom->getUserId();

        $order_company_id = $this->request->data['company_id'] ?? $this->request->query('company_id');
        if (intval($order_company_id) <= 0) {
            response_api(["error" => "Undefined order company identifier"], "error");
            exit;
        }

        $order_firstname = $this->request->data['order_firstname'] ?? $this->request->query('order_firstname');
        $order_lastname = $this->request->data['order_lastname'] ?? $this->request->query('order_lastname');
        $order_middlename = $this->request->data['order_middlename'] ?? $this->request->query('order_middlename');

        $order_email = $this->request->data['order_email'] ?? $this->request->query('order_email');
        $order_phone = $this->request->data['order_phone'] ?? $this->request->query('order_phone');

        $order_city = $this->request->data['order_city'] ?? $this->request->query('order_city');

        $order_street = $this->request->data['order_street'] ?? $this->request->query('order_street');
        $order_building = $this->request->data['order_building'] ?? $this->request->query('order_building');
        $order_house = $this->request->data['order_house'] ?? $this->request->query('order_house');


        if (empty($order_firstname)) {
            response_api(["error" => " field [order firstname] cannot be empty"], "error");
            exit;
        }

        if (empty($order_lastname)) {
            response_api(["error" => " field [order lastname] cannot be empty"], "error");
            exit;
        }

        if (empty($order_fathername)) {
            response_api(["error" => " field [order fathername] cannot be empty"], "error");
            exit;
        }

        if (empty($order_email)) {
            response_api(["error" => " field [order email] cannot be empty"], "error");
            exit;
        }

        if (empty($order_phone)) {
            response_api(["error" => " field [order phone] cannot be empty"], "error");
            exit;
        }

        if (empty($order_city)) {
            response_api(["error" => " field [order city] cannot be empty"], "error");
            exit;
        }

        if (empty($order_street)) {
            response_api(["error" => " field [order street] cannot be empty"], "error");
            exit;
        }

        if (empty($order_building)) {
            response_api(["error" => " field [order building] cannot be empty"], "error");
            exit;
        }

        if (empty($order_house)) {
            response_api(["error" => " field [order house] cannot be empty"], "error");
            exit;
        }


        if (intval($order_city) > 0) {
            $order_city_name = $this->CityCom->getCityNameById($order_city);
        } elseif (intval($order_city) == 0) {
            $order_city_name = $order_city;
        }

        // если все данные заказа введены корректно и проверки пройдены, получаем список товаров и прилагаем его к заказу
        $products_list = [];
        $products_items = $this->UserCom->getCartListFull($user_id);

        $products_company_list = [];

        $order_companies = [];
        $old_company_id = 0;
        foreach ($products_items as $products_item) {
            $company_id = $products_item['Company']['id'];

            if (!in_array($company_id, $order_companies)) {
                if (!empty($register_order_company_id)) {
                    if ($register_order_company_id == $company_id) {
                        $order_companies[] = $company_id;
                    }
                } else {
                    $order_companies[] = $company_id;
                }
            }
            $products_item['Cart']['company_id'] = $company_id;
            $products_list[] = $products_item['Cart'];
            if ($old_company_id != $company_id) {

                $products_company_list[] = $products_list;
                $products_list = [];
            }
            $old_company_id = $company_id;
        }
        if (count($order_companies) > 0) {

            foreach ($order_companies as $order_company) {
                $order_data = [
                    'company_id' => $order_company,
                    'order_firstname' => $order_firstname,
                    'order_lastname' => $order_lastname,
                    'order_fathername' => $order_fathername,
                    'orderer_id' => $user_id,
                    'order_email' => $order_email,
                    'order_phone' => $order_phone,
                    'order_city' => $order_city_name,
                    'order_street' => $order_street,
                    'order_building' => $order_building,
                    'order_house' => $order_house,
                    'status' => 'new',
                ];
                $this->OrderCom->makeOrder($user_id, $order_data, $products_company_list, $order_company);
            }
        } else {
            $result = ['error_message' => 'not found order company_id'];
            response_api($result, "error");
            exit;
        }

        $result = ['success'];
        response_api($result, "success");
        exit;
    }

    // СРАВНЕНИЕ
    public function getCompareListByCategory()
    {
        $category_id = $this->request->param('category_id');
        if ($category_id <= 0) {
            die("категория не найдена");
        }
        $this->UserCom->auth_requires();
        $user_id = $this->UserCom->getUserId();
        $compared_category_products = $this->UserCom->getCompareListByCategory($user_id, $category_id);
        $result = [$compared_category_products];
        response_api($result, "success");
        exit;
    }

    /*список уникальных категорий, товары из которых находятся в избранном*/
    public function getCompareCategories()
    {
        $result = $this->getCompareListContent();
        response_api($result, "success");
        exit;
    }

    public function getCompareListContent()
    {
        $this->UserCom->auth_requires();
        $user_id = $this->UserCom->getUserId();
        $compared_category_products = $this->UserCom->getCompareCategories($user_id);
        if (!is_array($compared_category_products)) {
            $compared_category_products = [];
        }
        return $compared_category_products;
    }

    public function addProductToCompare()
    {
        $this->UserCom->auth_requires();
        $product_id = $this->request->data('product_id') ?? $this->request->query('product_id');


        if (intval($product_id) == 0) {
            response_api(["error" => " product with id $product_id not found "], "error");
            exit;
        }
        $product = $this->Product->find("first",
            array('conditions' =>
                array(
                    'id' => $product_id,
                ),
            )
        );
        if (count($product) == 0) {
            response_api(["error" => " product_id with product_id $product_id not found "], "error");
            exit;
        }

        if ($this->UserCom->isProductInCompared($this->UserCom->getUserId(), $product_id) == 0) {
            $this->UserCom->addProductToCompare($this->UserCom->getUserId(), $product_id);
        }
        $result = ['success' => true];
        response_api($result, "success");
        exit;
    }

    public function deleteProductFromCompare()
    {
        $product_id = $this->request->data('product_id') ?? $this->request->query('product_id');
        if (intval($product_id) == 0) {
            response_api(["error" => " product with id $product_id not found "], "error");
            exit;
        }
        $product = $this->Product->find("first",
            array('conditions' =>
                array(
                    'id' => $product_id,
                ),
            )
        );
        if (count($product) == 0) {
            response_api(["error" => " product_id with id $product_id not found "], "error");
            exit;
        }
        $this->UserCom->auth_requires();
        if ($this->UserCom->isProductInCompared($this->UserCom->getUserId(), $product_id) > 0) {
            $this->UserCom->deleteProductFromCompare($this->UserCom->getUserId(), $product_id);
        }

        $result = ['success' => true];
        response_api($result, "success");
        exit;
    }

    public function deleteCategoryFromCompare()
    {
        $category_id = $this->request->data('category_id') ?? $this->request->query('category_id');
        if (intval($category_id) == 0) {
            response_api(["error" => " category id $category_id not found "], "error");
            exit;
        }

        $this->UserCom->auth_requires();
        $this->UserCom->deleteCategoryFromCompare($this->UserCom->getUserId(), $category_id);

        $result = ['success' => true];
        response_api($result, "success");
        exit;
    }

    public function shortlink()
    {
        $link = $this->request->query('link');
        $hash = $this->request->param('hash');
        if (!empty($link)) {
            if (!$this->ShortLink->isTerpikkaUrl($link)) {
                $errors = "инородный хост";
                response_api($errors, "error");
                exit;
            }

            $hash = $this->ShortLink->makeShortlink($link);
            $result = ["hash" => $hash];
            response_api($result, "success");
            exit;
        } else if (!empty($hash)) {
            $link = $this->ShortLink->getShortlink($hash);
            if ($link == null) {
                $errors = "короткая ссылка не найдена";
                response_api($errors, "error");
                exit;
            }
            $result = ["link" => $link];
            response_api($result, "success");
            exit;
        }

    }

    // ПОМОЙКА
    public function sendTestMailJet()
    {
        $this->EmailCom->sendViaMailJet();
        exit;
    }

    //поиск параметров категории товара
    public function params_search_ajax()
    {
        $category_id = $this->request->param('category_id');
        if ($category_id <= 0) {
            die("категория не найдена");
        }

        $complete_params = array();
        do {
            $params = $this->ProductCategoryParam->find('all', array(
                'conditions' => array(
                    'category_id' => $category_id,
                ),
                'order' => array(
                    'id' => 'ASC',
                )
            ));


            foreach ($params as $param) {
                $param_values_complete = [];
                // подгрузка предустановленных значений фильтра, при необходимости
                $param_id = $param['ProductCategoryParam']['id'];
                $param_type = $param['ProductCategoryParam']['param_type'];
                if ($param_type == "checkbox") {

                    $param_values = $this->ProductCategoryParamValues->find('all', array(
                        'conditions' => array(
                            'param_id' => $param_id,
                        ),
                        'order' => array(
                            'value' => 'ASC',
                        )
                    ));
                    if (count($param_values) > 0) {
                        foreach ($param_values as $param_value) {
                            $param_values_complete[] = array(
                                'id' => $param_value['ProductCategoryParamValues']['id'],
                                'value' => $param_value['ProductCategoryParamValues']['value']
                            );
                        }
                    }
                }
                $complete_params[] = array(
                    'param' => $param['ProductCategoryParam'],
                    'param_values' => $param_values_complete
                );
            }

            $parent_category = $this->ProductCategory->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $category_id
                        ),
                )
            );
            $category_id = $parent_category['ProductCategory']['parent_id'];
        } while ($category_id != 0);

        response_ajax($complete_params, 'success');
        exit;
    }
}