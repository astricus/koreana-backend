<?php
//локализация
App::uses('L10n', 'L10n');
App::uses('Controller', 'Controller');

//контроллер Пользователь

class AppController extends Controller
{
    public $uses = array(
        'ProductCategory',
        'Order',
        'Product',
        'User',
    );

    public $user_data;

    public $components = array(
        'Cookie',
        'Session',
        'DebugKit.Toolbar',
        'UserCom'
    );

    const ConnectLimit = 120;
    const FreeTime = 20; //секунд
    const BlockConnectMessage = 'Ошибка доступа! Вами превышен допустимый лимит запросов!';

    /*
        private function __connect_control()
        {
            $ip = get_ip();
            $ua = get_ua();
            $os = get_os();

            $connect = $this->_check_connect($ip, $ua, $os);
            if (!empty($connect)) {
                // очистить запись если интервал соединения больше чем FreeTime
                $id = $connect['Connect']['id'];
                $this->Connect->id = $id;
                if ($connect[0]['active_time'] > AppController::FreeTime) {
                    $this->Connect->delete();
                } else {
                    $connect['Connect']['count']++;
                    if ($connect['Connect']['count'] >= AppController::ConnectLimit) {
                        $this->_block_connect();
                    }
                    $data_for_save = array('count' => $connect['Connect']['count']);
                    $this->Connect->save($data_for_save);
                }
            } else {
                $data_for_save = array(
                    'count' => 1,
                    'os' => $os,
                    'ua' => $ua,
                    'ip' => $ip
                );
                $this->Connect->save($data_for_save);
            }
        }


        private function _check_connect($ip, $ua, $os)
        {
            $connect = $this->User->find('count',
                array('conditions' =>
                    array(
                        'ip' => $ip,
                        'browser' => $ua,
                        'os' => $os,
                    ),
                    'fields' => array(
                        'User.*',
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS active_time',
                    )
                )
            );
            return isset($connect) ? $connect : null;
        }

        private function _block_connect()
        {
            die(AppController::BlockConnectMessage);
        }*/

    public function beforeFilter()
    {
        $not_auth_valid_methods = array('login', 'passreminder');
        $auth_error = $this->request->query('auth_error') ?? null;
        $auth_error_text = $this->request->query('auth_error_text') ?? null;
        $this->set('auth_error', $auth_error);
        $this->set('auth_error_text', $auth_error_text);
        $show_login_form = true;
        $this->set('show_login_form', $show_login_form);
        $this->UserCom->getUserSessionId();

        /*  ЗАГЛУШКА ОТКЛЮЧЕНА
        if(!$this->is_auth()){
            if($this->request->controller != 'index' OR
                !in_array($this->request->action, $not_auth_valid_methods )
            ){
                if ($this->request->is('post')) {
                    die("need to be auth");
                }
                else {

                    $this->redirect(
                        [
                            'controller' => 'index',
                            'action' => 'login',
                        ]
                    );
                }
                return;
            }
        }

        $this->_setLanguage();

        $this->shop_owner_data();

        // новые заказы поставщика, кол-во и сумма
        $new_orders = $this->OrderCom->get_new_orders_by_shop_owner_id();
        $this->set('new_orders', $new_orders);

        // новые сообщения
        $new_messages = $this->Chat->get_new_messages("shop_owner", $this->ShopOwnerCom->shop_owner_id());
        $this->set('new_messages', count($new_messages));

        $active_page = strtolower($this->request->controller);

        if($active_page == "index") {
            $active_page = "";
        }
        if($active_page == "datareport") {
            $active_page = "stat";
        }
        if($active_page == "product") {
            $active_page = "products";
        }
        if($active_page == "complaint") {
            $active_page = "complaints";
        }
        if($active_page == "shop") {
            $active_page = "shops";
        }
        if($active_page == "order") {
            $active_page = "orders";
        }
        if($active_page == "orderer") {
            $active_page = "orderers";
        }
        if($this->request->action == "activity"){
            $active_page = "activity";
        }
        if($this->request->action == "settings"){
            $active_page = "settings";
        }
        if($this->request->controller == "Activity" AND $this->request->action == "index") {
            $active_page = "notifications";
        }
        if($this->request->action == "statistic") {
            $active_page = "calls_stat";
        }
        if($this->request->controller == "orderer" AND $this->request->action == "index") {
            $active_page = "orderers";
        }
        if($this->request->controller == "master" AND $this->request->action == "index") {
            $active_page = "masters";
        }
        if($this->request->controller == "masterservice" AND $this->request->action == "index") {
            $active_page = "master_services";
        }
        if($this->request->controller == "ProductCategory") {
            $active_page = "category";
        }

        $this->set('active_page', $active_page);
        $this->set('is_auth', $this->is_auth() ? 'true' : 'false');
        $this->set('title', "Личный кабинет");
        $this->set('shop_owner_data', $this->ShopOwnerCom->shop_owner_data());
        $this->set('content_title', "");

        */

        parent::beforeFilter();
    }

}