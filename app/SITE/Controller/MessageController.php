<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class MessageController extends AppController
{
    public $uses = array(
        'City',
        'Message',
        'Orderer',
    );

    public $components = array(
        'Session',
        'Breadcrumbs',
        'Flash',
        'Uploader',
        'CompanyCom',
        'ProductCom',
        'OrderCom',
        'Activity',
        'Chat'
    );

    public $receiver_role = "shop_owner";

    public $valid_receiver_role = array("shop_owner", "orderer", "admin");

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(_("Личные сообщения"), Router::url(array('plugin' => false, 'controller' => 'message', 'action' => 'index')));
        parent::beforeFilter();
    }


    /*список последних сообщений с разными пользователями */
    public function index()
    {
        if ($this->CompanyCom->shop_list() == null) {
            $this->set('shops', null);
            return;
        }
//        $orderer_id = $this->request->query['orderer_id'] ?? null;
//
//        if ($orderer_id == null OR $orderer_id == "0") {
//            $orderer_ids_array = [];
//        } else {
//            $orderer_ids_array = ['Order.orderer_id' => $orderer_id];
//        }


        $page = $this->request->query['page'] ?? 1;
        $c_count = 5;

        $show_count = $this->request->query['count'] ?? $c_count;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        $cur_company_id = $this->CompanyCom->company_id();

        $total_recipinets = $this->Message->find("all",
            array(
                'conditions' =>
                    array(
                        'or' => array(
                            array(
                                'receiver_id' => $cur_company_id,
                                'receiver_role' => $this->receiver_role),
                            array(
                                'sender_id' => $cur_company_id,
                                'sender_role' => $this->receiver_role),
                        ),
                    ),
                "order" => array("Message.created DESC"),
                'fields' => array(
                    'DISTINCT Message.receiver_id, Message.receiver_role, DISTINCT Message.sender_id, Message.sender_role',
                ),
            )
        );

        $last_messages = array();

        foreach ($total_recipinets as $recipinet) {
            $recipinet = $recipinet['Message'];

            $sender_id = $recipinet['sender_id'];
            $sender_role = $recipinet['sender_role'];
            $receiver_id = $recipinet['receiver_id'];
            $receiver_role = $recipinet['receiver_role'];

            // переписка владельца магазина с клиентом
            if ($sender_role == "orderer") {
                $last_message = $this->Message->find("first",
                    array(
                        'conditions' =>
                            array(
                                'receiver_id' => $cur_company_id,
                                'receiver_role' => $this->receiver_role,
                                'sender_id' => $sender_id,
                                'sender_role' => $sender_role
                            ),
                        "order" => array("Message.created DESC"),
                        'joins' => array(
                            array(
                                'table' => 'orderers',
                                'alias' => 'Orderer',
                                'type' => 'INNER',
                                'conditions' => array(
                                    'Message.sender_id = Orderer.id'
                                )
                            ),
                        ),
                        'fields' => array(
                            'Orderer.*',
                            'Message.*',
                        ),
                    )
                );
                $last_message['message_direct'] = "receive";
                $last_message['message_from_type'] = "orderer";
            }
            if ($receiver_role == "orderer") {
                $last_message = $this->Message->find("first",
                    array(
                        'conditions' =>
                            array(
                                'receiver_id' => $receiver_id,
                                'receiver_role' => $receiver_role,
                                'sender_id' => $cur_company_id,
                                'sender_role' => $this->receiver_role
                            ),
                        "order" => array("Message.created DESC"),
                        'joins' => array(
                            array(
                                'table' => 'orderers',
                                'alias' => 'Orderer',
                                'type' => 'INNER',
                                'conditions' => array(
                                    'Message.receiver_id = Orderer.id'
                                )
                            ),
                        ),
                        'fields' => array(
                            'Orderer.*',
                            'Message.*',
                        ),
                    )
                );

                $last_message['message_direct'] = "send";
                $last_message['message_from_type'] = "orderer";
            } else if ($sender_role == "admin") {
                $last_message = $this->Message->find("first",
                    array(
                        'conditions' =>
                            array(
                                'receiver_id' => $cur_company_id,
                                'receiver_role' => $this->receiver_role,
                                'sender_id' => $sender_id,
                                'sender_role' => $sender_role
                            ),
                        "order" => array("Message.created DESC"),
                        'joins' => array(
                            array(
                                'table' => 'managers',
                                'alias' => 'Manager',
                                'type' => 'INNER',
                                'conditions' => array(
                                    'Message.sender_id = Manager.id'
                                )
                            )
                        ),

                        'fields' => array(
                            'Manager.*',
                            'Message.*',
                        ),
                    )
                );
                $last_message['message_direct'] = "receive";
                $last_message['message_from_type'] = "admin";
            } else if ($receiver_role == "admin") {
                $last_message = $this->Message->find("first",
                    array(
                        'conditions' =>
                            array(
                                'receiver_id' => $receiver_id,
                                'receiver_role' => $receiver_role,
                                'sender_id' => $cur_company_id,
                                'sender_role' => $this->receiver_role
                            ),
                        "order" => array("Message.created DESC"),
                        'joins' => array(
                            array(
                                'table' => 'managers',
                                'alias' => 'Manager',
                                'type' => 'INNER',
                                'conditions' => array(
                                    'Message.sender_id = Manager.id'
                                )
                            ),
                        ),
                        'fields' => array(
                            'Manager.*',
                            'Message.*',
                        ),
                    )
                );
                $last_message['message_direct'] = "send";
                $last_message['message_from_type'] = "admin";
            }
            $cite_message = $this->Chat->get_message_cite($last_message['Message']['id']);
            $last_message['cite_message'] = $cite_message;
            $last_messages[] = $last_message;
        }

        /*

        $page_orders = $this->Order->find("all",
            array('conditions' =>
                array(
                    'Shop.id' => $this->CompanyCom->shop_list(),
                    $city_ids_array,
                    $orderer_ids_array,
                    $shop_ids_array,
                    $status_array
                ),
                'joins' => array(
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop.id = Order.shop_id'
                        )
                    ),
                    array(
                        'table' => 'orderers',
                        'alias' => 'Orderer',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Order.orderer_id = Orderer.id'
                        )
                    ),
                    // город заказа
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'City.id = Order.order_city'
                        )
                    ),
                ),
                'fields' => array(
                    'City.*',
                    'Order.*',
                    'Shop.*',
                    'Orderer.*',
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array('order.id DESC')
            )
        );

        foreach ($page_orders as &$order) {
            $order_id = $order['Order']['id'];
            $order['product_count'] = $this->OrderCom->shop_order_amount($order_id);
            $order['order_total_cost'] = $this->OrderCom->shop_order_sum($order_id);
        }
        $form_data = [];
        $form_data['active_only'] = "on";
        $form_data['orderer_id'] = $orderer_id;
        $form_data['shop_id'] = $shop_id;
        $form_data['city_id'] = $city_id;
        $form_data['status'] = $status;


        $this->set('form_data', $form_data);
        */
        //$this->set('orders', $page_orders);
        $this->set('last_messages', $last_messages);

        //$pages = ceil($total_orders_count / $show_count);
//        $this->set('page', $page);
//        $this->set("pages", $pages);
        $this->set('title', "Личные сообщения");
    }

    public function chat()
    {
        $recipient_role = $this->request->params['recipient_role'];
        $recipient_id = $this->request->param('recipient_id');
        if ($recipient_role == "admin") {
            $recipient_type = "manager";
        } else if ($recipient_role == "orderer") {
            $recipient_type = "orderer";
        }

        $model_name = ucfirst($recipient_type);
        $recipient_data = $this->$model_name->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $recipient_id
                    ),
            )
        );
        $recipient_data = $recipient_data[$model_name];
        $recipient_name = prepare_fio($recipient_data['lastname'], $recipient_data['firstname'], "");

        $cur_company_id = $this->CompanyCom->company_id();
        if ($recipient_role == "admin") {
            $last_dialog_messages = $this->Message->find("all",
                array(
                    'conditions' =>
                        array(
                            'or' => array(
                                array(
                                    'receiver_id' => $recipient_id,
                                    'receiver_role' => $recipient_role,
                                    'sender_id' => $cur_company_id,
                                    'sender_role' => $this->receiver_role,
                                ),
                                array(
                                    'receiver_id' => $cur_company_id,
                                    'receiver_role' => $this->receiver_role,
                                    'sender_id' => $recipient_id,
                                    'sender_role' => $recipient_role,
                                ),
                            ),
                        ),
                    "order" => array("Message.created ASC"),
                    'joins' => array(
                        array(
                            'table' => 'managers',
                            'alias' => 'Manager',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Message.sender_id = Manager.id'
                            )
                        ),
                    ),
                    'fields' => array(
                        'Manager.*',
                        'Message.*',
                    ),
                )
            );
        } else if ($recipient_role == "orderer") {
            $last_dialog_messages = $this->Message->find("all",
                array(
                    'conditions' =>
                        array(
                            'or' => array(
                                array(
                                    'receiver_id' => $recipient_id,
                                    'receiver_role' => $recipient_role,
                                    'sender_id' => $cur_company_id,
                                    'sender_role' => $this->receiver_role
                                ),
                                array(
                                    'receiver_id' => $cur_company_id,
                                    'receiver_role' => $this->receiver_role,
                                    'sender_id' => $recipient_id,
                                    'sender_role' => $recipient_role,
                                ),
                            )
                        ),
                    "order" => array("Message.created ASC"),
                    'joins' => array(
                        array(
                            'table' => 'orderers',
                            'alias' => 'Orderer',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Message.sender_id = Orderer.id'
                            )
                        ),
                    ),
                    'fields' => array(
                        'Orderer.*',
                        'Message.*',
                    ),
                )
            );
        }

        $this->set("recipient_name", $recipient_name);
        $this->set("recipient_id", $recipient_id);
        $this->set("recipient_role", $recipient_role);
        $this->set("last_dialog_messages", $last_dialog_messages);
    }

    public function send_chat_message(){
        $recipient_role = $this->request->data('recipient_role');
        $recipient_id = $this->request->data('recipient_id');
        $message = $this->request->data('message');

        if(!in_array($recipient_role, $this->valid_receiver_role)){
            $result_status = "error";
            $error_message = "Не определен получатель сообщения";
            response_ajax($error_message, $result_status);
            exit;
        }
        if(empty($message)){
            $result_status = "error";
            $error_message = "Нельзя отправлять пустое сообщение";
            response_ajax($error_message, $result_status);
            exit;
        }
        if(intval($recipient_id)<=0){
            $result_status = "error";
            $error_message = "Не определен получатель сообщения";
            response_ajax($error_message, $result_status);
            exit;
        }
        // проверка что это не дубль сообщения и что они не отправляются слишком часто, интервал - 5 секунды
        $limit_time_interval  = 5;//seconds
        // TODO проверка существования получателя сообщения в БД
        $cur_company_id = $this->CompanyCom->company_id();
        $check_enable_sending = $this->Message->find("count",
            array(
                'conditions' =>
                    array(
                        'sender_id' => $cur_company_id,
                        'sender_role' => $this->receiver_role,
                        'receiver_id' => $recipient_id,
                        'receiver_role' => $recipient_role,
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) - ' . $limit_time_interval . ' <0',
                    ),
            )
        );

        if($check_enable_sending>0){
            $result_status = "error";
            $error_message = "Вы отправляете сообщения слишком часто, пожалуйста, подождите...";
            response_ajax($error_message, $result_status);
            exit;
        }

        $new_message = array(
            'sender_id' => $cur_company_id,
            'sender_role' => $this->receiver_role,
            'receiver_id' => $recipient_id,
            'receiver_role' => $recipient_role,
            'message' => $message,
            'readed' => 'no',
            'cite_id' => 0
        );
        $this->Message->save($new_message);
        $result_status = "success";
        $result_message = array(
            "message" => "Сообщение отправлено",
            "created" => lang_calendar(now_date())
        );
        response_ajax($result_message, $result_status);
        exit;
        /*


        if ($recipient_role == "admin") {
            $recipient_type = "manager";
        } else if ($recipient_role == "orderer") {
            $recipient_type = "orderer";
        }

        $model_name = ucfirst($recipient_type);
        $recipient_data = $this->$model_name->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $recipient_id,
                    ),
            )
        );
        $recipient_data = $recipient_data[$model_name];
        $recipient_name = prepare_fio($recipient_data['lastname'], $recipient_data['firstname'], "");*/



    }

    // TODO доделать вывод детальной страницы заказа
    public function view()
    {
        $id = $this->request->param('id');

        //проверка владения магазином
        //TODO сделать проверку на наличие прав для редактирования заказа
        /*
        if(!$this->CompanyCom->has_owner_rights($id)){
            $this->Flash->set(__('У вас нет права на доступ к данным этого магазина!'));
            return $this->redirect(['action' => 'index']);
        }
        */

        $shops = $this->Shop->find("all",
            array(
                'conditions' =>
                    array(//'id' => $id
                    ),
                'order' => 'shop_name ASC'
            )
        );
        $this->set('shops', $shops);
        $order = $this->Order->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $id
                    ),
            )
        );
        $this->set('order', $order);

        $order_comments = $this->Order_Comment->find("all",
            array(
                'conditions' =>
                    array(
                        'order_id' => $id
                    ),
                'order' => array('id' => 'DESC'),
            )
        );
        // russian regions
        $russian_country_id = 218;
        $regions = $this->Region->find("all",
            array(
                'conditions' =>
                    array(
                        'country_id' => $russian_country_id
                    ),
                'order' => 'name ASC'
            )
        );

        $regions_list = [];
        foreach ($regions as $region) {
            $region = $region['Region']['id'];
            if (!in_array($region, $regions_list)) {
                $regions_list[] = $region;
            }
        }

        $cities = $this->City->find("all",
            array(
                'conditions' =>
                    array(
                        'region_id' => $regions_list
                    ),
                'order' => 'name ASC'
            )
        );
        $this->set('cities', $cities);

        // список товаров в заказе
        $order_products_list = $this->OrderCom->shop_order_products($id);
        $order_product_results = [];
        foreach ($order_products_list as $product) {
            $order_product_result = [];
            $p_id = $product['Order_Product']['product_id'];
            $product_data = $this->Product->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $p_id
                        ),
                )
            )['Product'];
            $category_id = $product_data['category_id'];
            $category_name = $this->ProductCom->buildProductNameByCategories($category_id);
            $order_product_result['product_name'] = $category_name['name'] . " / " . $product_data['product_name'];
            $order_product_result['amount'] = $product['Order_Product']['amount'];
            $order_product_result['order_price'] = $product['Order_Product']['order_price'];
            $order_product_result['product_id'] = $p_id;
            //получение базовой цены
            $product_shop = $this->Shop_Product->find("first",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $p_id
                        ),
                )
            )['Shop_Product'];
            $order_product_result['base_price'] = $product_shop['base_price'];
            $order_product_results[] = $order_product_result;
        }

        $this->set('order_products_list', $order_product_results);
        $action_title = "Заказ №$id от " . lang_calendar($order['Order']['created']);
        $action_title_html = "Заказ <b>№$id</b> от <b>" . lang_calendar($order['Order']['created']) . " </b>";
        $this->set('title', $action_title);

        $this->Breadcrumbs->add($action_title_html, Router::url(array('plugin' => false, 'controller' => 'order', 'action' => 'view', 'id' => $id)));

        $this->set('order_comments', $order_comments);
        $this->set('order_id', $id);
    }

}