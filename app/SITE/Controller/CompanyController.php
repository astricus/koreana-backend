<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class CompanyController extends AppController
{
    public  $uses        = [
        'Company',
        'Orderer',
        'Token',
        'Company_Agent',
    ];

    private $valid_hosts = [
        'terpikka.ru',
        'terpikka.local',
    ];

    public  $components  = [
        'Api',
        'Cookie',
        'CompanyCom',
        'EmailCom',
        'Flash',
        'Session',
        'UserCom',
    ];

    public  $layout      = "default";

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Cookie->name = 'token';
        $this->Cookie->key  = null;
    }

    private function checkEmailExists($email)
    {
        $check = $this->Company_Agent->find(
            "count",
            [
                'conditions' =>
                    [
                        'email' => $email,
                    ],
            ]
        );

        return $check;
    }

    private function checkPhoneExists($phone)
    {
        $check = $this->Company_Agent->find(
            "count",
            [
                'conditions' =>
                    [
                        'phone' => $phone,
                    ],
            ]
        );

        return $check;
    }

    // АВТОРИЗАЦИЯ API
    public function login()
    {
        if ($this->request->is('post')) {

            //пароль
            $password = trim($this->request->data('password'));
            if (empty($password) or !valid_password($password)) {
                $result = [
                    'auth'    => 'error',
                    'message' => 'password is empty or is not valid format',
                ];
                $this->Api->response_api($result, "error");
                exit;
            }

            //почта или телефон
            $login = trim($this->request->data('login'));
            if (empty($login)) {
                $result = [
                    'auth'    => 'error',
                    'message' => 'login is empty',
                ];
                response_api($result, "error");
                exit;
            }

            if (valid_mail($login)) {
                $login_type = "email";
            } else {
                if ((valid_phone_number($login))) {
                    $login_type = "phone";
                } else {
                    $result = [
                        'auth'    => 'error',
                        'message' => 'unknown login type',
                    ];
                    response_api($result, "error");
                    exit;
                }
            }
            $hashed_pass = get_hash(Configure::read('USER_AUTH_SALT'), $password);
            if ($login_type == "email") {
                $check_user = $this->Company_Agent->find('first',
                                                         [
                                                             'conditions' => [
                                                                 'password' => $hashed_pass,
                                                                 'email'    => $login,
                                                             ],
                                                         ]
                );
            } else {
                $check_user = $this->Company_Agent->find('first',
                                                         [
                                                             'conditions' => [
                                                                 'password' => $hashed_pass,
                                                                 'phone'    => $login,
                                                             ],
                                                         ]
                );
            }
            if (count($check_user) > 0) {
                //удачная авторизация
                //$this->Session->write('Company_Agent', $login);

                if ($login_type == "email") {
                    $user_id_data = $this->Company_Agent->find('first', ['conditions' => ['email' => $login]]);
                } else {
                    $user_id_data = $this->Company_Agent->find('first', ['conditions' => ['phone' => $login]]);
                }

                $company_agent_id = $user_id_data['Company_Agent']['id'];
                $company_id       = $user_id_data['Company_Agent']['company_id'];

                //$this->Session->write('company_id', $company_id);
                //$this->Session->write('company_agent_id', $company_agent_id);

                $token = $this->generateToken($company_agent_id);
                //$this->Session->write('agent_token', $token);
                $this->saveToken($company_agent_id, $token);

                $result = [
                    'auth'        => 'success',
                    'agent_token' => $token,
                ];

                $big_time = 60 * 60 * 24 * 30 * 12 * 1000;
                if ($this->request->accepts('application/json')) {
                    response_api($result, "success");
                    exit;
                } else {
                    $host = $this->request->host();
                    //                    pr($host);
                    //                    exit;
                    if ($host == "terpikka.local") {
                        $cookie_domain = $host;
                    } else {
                        $cookie_domain = "terpikka.ru";
                    }

                    setcookie("agent_token", $token, time() + $big_time, "/", $cookie_domain);
                    if ($host == "terpikka.local") {
                        //                        response_api($result, "success");
                        //                        exit;
                        $this->redirect("http://shop.terpikka.local/gate");
                        exit;
                    } else {
                        if ($host == "terpikka.ru") {
                            response_api($result, "success");
                            exit;
                        }
                    }
                    /*
                    if (in_array($host, $this->valid_hosts)) {
                        if (key_exists("HTTP_ORIGIN", $_SERVER)) {
                            $this->redirect($_SERVER['HTTP_ORIGIN']);
                        } else {
                            $this->redirect("https://shop.terpikka.ru");
                        }
                    } else {
                        $this->redirect(site_url());
                    }*/
                }

            } else {
                $result = [
                    'auth'    => 'error',
                    'message' => 'WRONG LOGIN OR PASSWORD',
                ];
                response_api($result, "error");
            }
            exit;
        }
    }

    public function login_form()
    {

    }

    /**
     * @param $user_id
     *
     * @return string
     */
    private function generateToken($user_id)
    {
        return md5(time() . " token " . $user_id . "Company");
    }

    /**
     * @param $user_id
     * @param $token
     *
     * @return string
     */
    private function saveToken($user_id, $token)
    {
        $token_data = [
            'token'     => $token,
            'user_id'   => $user_id,
            'user_type' => 'Company',
            'expired'   => date('Y-m-d', strtotime('+1 year')),
        ];
        $this->Token->save($token_data);
    }

    // РЕГИСТРАЦИЯ

    public function register_form()
    {
    }

    public function register()
    {
        $errors = [];
        $data   = $this->params['data'];

        // валидация фио, почты, телефона
        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp     = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp    = "/^[0-9]{11,12}$/iu";
        $name_regexp     = "/^[a-zA_ZА-Яа-яёЁ-]{2,32}$/iu";
        $password_regexp = "/^[a-zA-Z0-9_]{8,32}$/iu";

        $data['firstname']    = trim($data['firstname']);
        $data['lastname']     = trim($data['lastname']);
        $data['company_name'] = trim($data['company_name']);
        $data['email']        = trim($data['email']);
        $data['password']     = trim($data['password']);
        $data['phone']        = trim($data['phone']);
        $data['phone']        = str_replace("+", "", $data['phone']);

        if (empty($data['company_name'])) {
            $errors[] = "incorrect field: empty company name";
        }

        if (!preg_match($mail_regexp, $data['email'])) {
            $errors[] = "incorrect field: email";
        }

        if (!preg_match($phone_regexp, $data['phone'])) {
            $errors[] = "incorrect field: phone number";
        }

        if (!preg_match($name_regexp, $data['lastname'])) {
            $errors[] = "incorrect field: lastname";
        }

        if (!preg_match($name_regexp, $data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!preg_match($password_regexp, $data['password'])) {
            $errors[] = "incorrect field: password";
        }

        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if ($this->checkEmailExists($data['email'])) {
            $errors[] = "incorrect field: entered email " . $data['email'] . " is busy";
        }

        if ($this->checkPhoneExists($data['phone'])) {
            $errors[] = "incorrect field: entered phone number " . $data['phone'] . " is busy";
        }

        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }

        $data["mail_key"] = md5(time() . $data["email"]);
        //генерация пароля с солью
        $real_pwd         = $data["password"];
        $data["password"] = get_hash(Configure::read('USER_AUTH_SALT'), $real_pwd);

        $company_data = [
            'company_name'         => $data['company_name'],
            'status'               => 'init',
            'phone_number'         => $data['phone'],
            'stat__current_rating' => 0,
        ];

        $this->Company->save($company_data);
        $company_id = $this->Company->id;

        $company_agent_data = [
            'company_id'   => $company_id,
            'firstname'    => $data["firstname"],
            'lastname'     => $data["lastname"],
            'role'         => "owner",
            'email'        => $data['email'],
            'phone'        => $data['phone'],
            'password'     => $data["password"],
            'status'       => 'new',
            'subscription' => 'true',

        ];
        $this->Company_Agent->save($company_agent_data);
        $company_agent_id = $this->Company_Agent->id;

        //ключ активации
        $mail_key_salt           = Configure::read('MAIL_KEY_SALT');
        $mail_key                = generate_mail_key($company_agent_id, $mail_key_salt);
        $this->Company_Agent->id = $company_agent_id;
        $this->Company_Agent->save(['mail_key' => $mail_key]);

        //запись номера телефона
        /*
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $user_phone = $user_data["User"]["phone"];
        if ($user_phone != null AND preg_match($phone_regexp, $user_phone)) {
            $data_to_save = array('user_id' => $user_id, 'phone' => $user_phone);
            $this->UserPhone->save($data_to_save);
        }
        */

        $message_data = "Вы только что зарегистрировались на проекте" . " " . site_url() . "<br>";
        $message_data .= "Ваши регистрационные данные<br> почтовый ящик: " . $data["email"];
        $message_data .= "Имя : " . $data["firstname"];
        $message_data .= "Пароль: " . $real_pwd . "<br>";
        $message_data .= "<a href='" . site_url(
            ) . "activate_account/user/" . $mail_key . "'>Ссылка активации аккаунта</a> <br><br>";
        $result       = ['success' => true];
        response_api($result, "success");
        $this->EmailCom->sendEmailNow(
            $data["email"],
            "",
            "REGISTER_ON_PROJECT" . " " . site_url(),
            'user_register_mail_layout',
            'user_register_mail_template',
            $message_data,
            $attachment = "",
            $start_sending = ""
        );
        exit;
    }

    public function reset_password_form()
    {
    }

    /**
     * @param $email
     *
     * @return bool
     */
    private function getCompanyAgentByEmail($email)
    {
        $check = $this->Company_Agent->find(
            "first",
            [
                'conditions' =>
                    [
                        'email' => $email,
                    ],
            ]
        );
        if (count($check) == 0) {
            return false;
        }

        return $check['Company_Agent']['id'];
    }

    public function reset_password()
    {
        $errors        = [];
        $data          = $this->params['data'];
        $data['email'] = trim($data['login']);

        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }
        if (!$this->checkEmailExists($data['email'])) {
            $errors[] = "incorrect field:  email is not found " . $data['email'];
        }
        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }

        $data["mail_key"] = md5(time() . $data["email"]);

        $company_agent_id = $this->getCompanyAgentByEmail($data["email"]);
        if (!$company_agent_id) {
            $errors[] = "incorrect field:  email is not found " . $data['email'];
            response_api($errors, "error");
            exit;
        }
        $company_agent_new_mail_key = [
            'mail_key' => $data["mail_key"],

        ];
        $this->Company_Agent->id    = $company_agent_id;
        $this->Company_Agent->save($company_agent_new_mail_key);

        //ключ активации
        $mail_key_salt           = Configure::read('MAIL_KEY_SALT');
        $mail_key                = generate_mail_key($company_agent_id, $mail_key_salt);
        $this->Company_Agent->id = $company_agent_id;
        $this->Company_Agent->save(['mail_key' => $mail_key]);

        $message_data = "Вы запросили сброс пароля в личном кабинtnt поставщика на проекте Терпика";
        $message_data .= "<a href='" . site_url() . "reset_password/" . $mail_key . "'>Сбросить пароль</a> <br><br>";
        $result       = ['success' => true];
        response_api($result, "success");
        $this->EmailCom->sendEmailNow(
            $data["email"],
            "",
            "Reset Password" . " " . site_url(),
            'user_register_mail_layout',
            'user_register_mail_template',
            $message_data,
            $attachment = "",
            $start_sending = ""
        );
        exit;
    }

}