<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ComplaintController extends AppController
{
    public $uses = array(
        'Complaint',
        'Orderer',
        'Shop'
    );

    public $components = array(
        'Session',
        'Breadcrumbs',
        'Flash',
        'ComplaintCom',
        'Uploader',
        'CompanyCom'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add("Жалобы", Router::url(array('plugin' => false, 'controller' => 'complaint', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function index()
    {
        $active_only = $this->request->query('active_only') ?? null;
        $new_only = $this->request->query('new_only') ?? null;

        $shop_id = $this->request->query('shop_id') ?? null;

        $sort_type = $this->request->query('sort_type') ?? null;
        $sort_dir = $this->request->query('sort_dir') ?? null;
        $page = $this->request->query('page') ?? 1;
        $orderer_id = $this->request->query('orderer_id') ?? null;

        $form_data = array(
            'orderer_id' => $orderer_id,
            'active_only' => $active_only,
            'new_only' => $new_only,
            'sort_type' => $sort_type,
            'sort_dir' => $sort_dir,
            'shop_id' => $shop_id,
            'page' => $page,
        );
        $this->set('form_data', $form_data);
        $this->set('title', "Управление жалобами");

        $orderers = $this->Orderer->find("all",
            array(
                'conditions' =>
                    array(
                        //*'id' => $orderer_id
                    ),
                'order' => array(
                    'lastname' => 'ASC',
                )
            )
        );
        $this->set('orderers', $orderers);

        $shops = $this->Shop->find("all",
            array(
                'conditions' =>
                    array(
                        'company_id' => $this->CompanyCom->company_id()
                    ),
                'order' => 'id ASC'
            )
        );
        $this->set('shops', $shops);

    }

    /** контент страницы - жалобы + пагинация */
    public function complaints_content()
    {
        $this->layout = false;
        $active_only = $this->request->data('active_only') ?? null;
        $new_only = $this->request->data('new_only') ?? null;

        $shop_id = $this->request->data('shop_id') ?? null;
        $orderer_id = $this->request->data('orderer_id') ?? null;

        $sort_type = $this->request->data('sort_type') ?? "created";
        $sort_dir = $this->request->data('sort_dir') ?? null;

        $sort_dir = strtoupper($sort_dir);
        if ($sort_dir != "ASC" AND $sort_dir != "DESC") {
            $sort_dir = "ASC";
        }

        $sort_type_query = "";
         if ($sort_type == "created") {
            $sort_type_query = "Complaint.created";
        } else if ($sort_type == "name") {
            $sort_type_query = "Orderer.lastname";
        } else {
             $sort_type_query = "Complaint.created";
         }
        $order_query = array("$sort_type_query $sort_dir");

        $show_count = 10;
        $page = $this->request->data('page') ?? 1;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        if ($orderer_id == null OR $orderer_id == "0") {
            $orderer_ids_array = [];
        } else {
            $orderer_ids_array = ['Orderer.id' => $orderer_id];
        }

        if ($shop_id == null OR $shop_id == "0") {
            $shop_ids_array = [];
        } else {
            $shop_ids_array = ['Complaint.shop_id' => $shop_id];
        }

        if ($active_only == null) {
            $active_only_array = [];
        } else {
            $active_only_array = ['Complaint.status' => 'active'];
        }
        if ($new_only == null) {
            $new_only_array = [];
        } else {
            $new_only_array = ['Complaint.status' => 'new'];
        }

        $complaint_counts = $this->Complaint->find('count', array(
                'conditions' =>
                    array(
                        $active_only_array,
                        $new_only_array,
                        $shop_ids_array,
                        $orderer_ids_array
                    ),
                'joins' => array(
                    array(
                        'table' => 'orderers',
                        'alias' => 'Orderer',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Complaint.orderer_id = Orderer.id'
                        )
                    ),
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop.id = Complaint.shop_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Complaint.*',
                ),
            )
        );
        $pages = ceil($complaint_counts / $show_count);

        $found_complaints = $this->Complaint->find("all",
            array('conditions' =>
                array(
                    $active_only_array,
                    $new_only_array,
                    $shop_ids_array,
                    $orderer_ids_array
                ),
                'joins' => array(
                    array(
                        'table' => 'orderers',
                        'alias' => 'Orderer',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Complaint.orderer_id = Orderer.id'
                        )
                    ),
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop.id = Complaint.shop_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Shop.*',
                    'Complaint.*',
                    'Orderer.*',
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array("$sort_type_query" => "$sort_dir")
            )
        );

        $this->set('complaints', $found_complaints);
        $this->set('page', $page);
        $this->set('pages', $pages);
        $this->set('complaint_counts', $complaint_counts);
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $name = $this->request->data('name');
            $category_id = $this->request->data('category_id') ?? 0;
            $description = $this->request->data('description') ?? '';
            $package_weight = $this->request->data('package_weight') ?? '';
            $package_widht = $this->request->data('package_widht') ?? '';
            $package_height = $this->request->data('package_height') ?? '';
            $package_length = $this->request->data('package_length') ?? '';
            $package_count = $this->request->data('package_count') ?? '';

            $country_id = $this->request->data('country') ?? '';
            $manufacturer_id = $this->request->data('manufacturer_id') ?? '';
            $brand_id = $this->request->data('brand_id') ?? '';

            $shop_ids = $this->request->data('shop_ids') ?? '';

            $saved_data = array(
                'product_name' => $name,
                'category_id' => $category_id,
                'description' => $description,
                'status' => "active",
                'system_barcode' => "",
                'package_weight' => $package_weight,
                'package_widht' => $package_widht,
                'package_height' => $package_height,
                'package_length' => $package_length,
                'package_count' => $package_count,
                'country_id' => $country_id,
                'manufacturer_id' => $manufacturer_id,
                'brand_id' => $brand_id,

            );

            if ($this->Product->save($saved_data)) {
                $id = $this->Product->id;

                $system_barcode = $this->ProductCom->systemBarcode($id, $category_id);
                $this->Product->id = $id;
                $this->Product->save(array('system_barcode' => $system_barcode));

                //сохранение изображения
                $upload_config = 'USER_IMAGE_UPLOAD_CONFIG';

                if ((isset($_FILES['file'])) AND ($_FILES['file']['size'] > 0) AND (!empty($_FILES['file']['name']))) {

                    if ($_FILES['file']['size'] > 10 * 1000 * 1000) {
                        $this->Session->write('image_error', 'image_weight');
                        response_ajax(array('error' => 'Некорректный размер изображения', 'field' => 'map_image'), 'error');
                        exit;
                    }

                    // проверки файла
                    $settings = Configure::read("USER_IMAGE_UPLOAD_CONFIG");
                    $max_file_size = $settings['max_file_size'];
                    $exts = $settings['ext'];

                    //проверка типа
                    if (!in_array(ext($_FILES['file']['name']), $exts)) {
                        $this->Session->write('image_error', 'image_ext');
                        response_ajax(array('error' => 'Некорректный формат изображения', 'field' => 'map_image'), 'error');
                        exit;
                    }

                    //проверка веса
                    if ($_FILES['file']['size'] > $max_file_size) {
                        $this->Session->write('image_error', 'image_weight');
                        response_ajax(array('error' => 'Слишком большое изображение', 'field' => 'map_image'), 'error');
                        exit;
                    }

                    //началась загрузка файла на сервер
                    $result_upload = $this->Uploader->upload($upload_config, $_FILES['file']);
                    list($file_x, $file_y) = getimagesize($result_upload['full_path']);

                    if ($file_x < 100 OR $file_y < 100) {
                        $this->Session->write('image_error', 'image_size');
                        response_ajax(array('error' => 'все еще слишком большое изображение', 'field' => 'map_image'), 'error');
                        exit;
                    }

                } else {
                    $result_upload = null;
                }
                //загрузка файла, если есть
                if ($result_upload !== null) {
                    //если файл был загружен во временную директорию переносим его в директорию хранения
                    $file_transfer = $this->Uploader->transfer_file($result_upload['file'], Configure::read('FILE_TEMP_DIR'), Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR'), true);
                    if ($file_transfer) {
                        $uploaded_image = $this->Uploader->new_filename;

                        //идентификатор изображения
                        $image_id = md5(time() . " " . $uploaded_image);
                        // конфигуратор создаваемых изображений
                        $product_images_generator_param_list = array();
                        $product_images_generator_param_list[] = ['big', [800, 600]];
                        $product_images_generator_param_list[] = ['normal', [250, 250]];
                        $product_images_generator_param_list[] = ['small', [60, 60]];

                        foreach ($product_images_generator_param_list as $param_list) {
                            $type_name = $param_list[0];
                            $type_width = $param_list[1][0];
                            $type_height = $param_list[1][1];
                            $resizeded_image = resize_image(Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR') . DS . $uploaded_image, $type_width, $type_height);
                            $image_generate_prefix = substr(md5(time() . $system_barcode), 0, 8) . "-" . $system_barcode;
                            $result_file_name = $image_generate_prefix . "_" . $type_name . ".jpg";
                            $resized_image_name = Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR') . DS . pathinfo($uploaded_image, PATHINFO_FILENAME) . $result_file_name;
                            $result_resized_name = pathinfo($uploaded_image, PATHINFO_FILENAME) . $result_file_name;
                            if (!file_exists($resized_image_name)) {
                                imagejpeg($resizeded_image, $resized_image_name, 100);
                            }

                            $image_for_save = array(
                                'type' => $type_name,
                                "file" => $result_resized_name,
                                'image_id' => $image_id,
                                'product_id' => $id,
                            );
                            $this->Product_Image->create();
                            $this->Product_Image->save($image_for_save);
                        }
                    }
                }

                // сохранение фильтров товара
                $filter_params = $this->request->data('params') ?? null;
                foreach ($filter_params as $key => $filter_param) {
                    $new_filter_params = array(
                        'product_id' => $id,
                        'param_id' => $key,
                        'value' => $filter_param
                    );
                    $this->Product_Param->create();
                    $this->Product_Param->save($new_filter_params);
                }

                //сохранение товара в списке магазинов
                $base_price = $this->request->data('base_price') ?? '';
                foreach ($shop_ids as $shop_id) {
                    $new_shop_product = array(
                        'product_id' => $id,
                        'shop_id' => $shop_id,
                        'base_price' => $base_price,
                        'status' => 'active'
                    );
                    $this->Shop_Product->create();
                    $this->Shop_Product->save($new_shop_product);
                }

                $this->Flash->set(__('Товар был успешно добавлен'));
                //$this->Activity->add("add_object", $this->Admin->manager_id(), $name);
                return $this->redirect(['controller' => 'product', 'action' => 'index']); //'id' => $id
            }
        }

        $categories = $this->ProductCategory->find("all",
            array(
                'conditions' =>
                    array(),
                'order' => array(
                    'parent_id' => 'ASC',
                )
            )
        );

        foreach ($categories as &$category) {
            // составление читабельных названий категорий товара
            $category_id = $category['ProductCategory']['id'];
            $use_build_name = $this->ProductCom->buildProductNameByCategories($category_id);
            $category['use_build_name'] = array(
                'name' => $use_build_name['name'],
                'level' => $use_build_name['level']
            );
        }

        $countries = $this->Country->find("all",
            array(
                'conditions' =>
                    array(//    'id' => $user_id
                    ),
                'order' => 'name ASC'
            )
        );

        $brands = $this->Brand->find("all",
            array(
                'conditions' =>
                    array(//    'id' => $user_id
                    ),
                'order' => 'name ASC'
            )
        );

        $shops = $this->Shop->find("all",
            array(
                'conditions' =>
                    array(
                        'company_id' => $this->CompanyCom->company_id()
                    ),
                'order' => 'id ASC'
            )
        );

        $manufacturers = $this->Manufacturer->find("all",
            array(
                'conditions' =>
                    array(//    'id' => $user_id
                    ),
                'order' => 'name ASC'
            )
        );
        $this->set('categories', $categories);
        $this->set('countries', $countries);
        $this->set('brands', $brands);
        $this->set('manufacturers', $manufacturers);
        $this->set('shops', $shops);

        $action_title = "Добавление товара";
        $this->set('title', $action_title);
        $this->set('content_title', "Добавление товара");

        $this->Breadcrumbs->add($action_title, Router::url(array('plugin' => false, 'controller' => 'product', 'action' => 'add')));
    }

    public function view()
    {
        $id = $this->request->param('id');

        $complaint = $this->Complaint->find("first",
            array('conditions' =>
                array(
                    'Complaint.id' => $id
                ),
                'joins' => array(
                    array(
                        'table' => 'orderers',
                        'alias' => 'Orderer',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Complaint.orderer_id = Orderer.id'
                        )
                    ),
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop.id = Complaint.shop_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Shop.*',
                    'Complaint.*',
                    'Orderer.*',
                ),
            )
        );

        $this->set('complaint', $complaint);

        $this->Breadcrumbs->add("жалоба #" . $id, Router::url(array('plugin' => false, 'controller' => 'complaint', 'action' => 'view', 'id' => $id)));
        $this->set('title', "жалоба #$id");
    }

    public function edit(){}

    public function delete(){}

    public function block(){}

    public function unblock(){}
}