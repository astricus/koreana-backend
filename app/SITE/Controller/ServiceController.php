<?php
//локализация
App::uses('L10n', 'L10n');
App::uses('Controller', 'Controller');

//контроллер Сервис

class ServiceController extends Controller
{
    public $uses = array(
        'ProductCategory',
        'Manufacturer',
        'Order',
        'Product',
        'User',
        'Street',
        'City'
    );

    public $user_data;

    public $SPB_REGION_ID = 1;
    public $MSC_REGION_ID = 1611;

    public $components = array(
        'Cookie',
        'Session',
        'CompanyCom',
        'OrderCom',
        'Chat',
        'DebugKit.Toolbar',
        'UserCom'
    );

    /**
     * @param $content
     * @return array
     */
    private function parseStreet($content)
    {
        $type_regex = '/ra noRightBorder">(.*?)<\/td>/si';
        preg_match_all($type_regex,
            $content,
            $type_matches);
        $name_regex = '/noLeftBorder">(.*?)<\/a>/si';
        preg_match_all($name_regex,
            $content,
            $name_matches);
        return [
            'type' => $type_matches[1],
            'name' => $name_matches[1],
        ];
    }

    /**
     * @param $url
     * @param $city_id
     * @return false|string
     */
    public function parseUrl($url, $city_id)
    {
        $parsed_content = file_get_contents($url);
        $street_data = $this->parseStreet($parsed_content);
        foreach ($street_data['name'] as $key => $value) {
            $value = trim($value);
            $value_arr = explode('">', $value);
            $value_name = str_replace("</a>", "", $value_arr[1]);
            $type = trim($street_data['type'][$key]);
            if($value_name == ""){
                continue;
            }
            $check_street = $this->checkStreet($value_name, $city_id);
             if ($check_street == 0) {
                 $this->addStreet($value_name, $type, $city_id);
            }
        }
        return $parsed_content;
    }

    /**
     * @param $city_name
     * @return |null
     */
    private function checkCity($city_name){
        $city = $this->City->find("first",
            array('conditions' =>
                array(
                    'name' => $city_name,
                )
            )
        );
        if (count($city) == 0) {
            return null;
        }
        return $city;
    }

    /**
     * @param $street_name
     * @param $city_id
     * @return mixed
     */
    private function checkStreet($street_name, $city_id){
        $check_street = $this->Street->find("count",
            array('conditions' =>
                array(
                    'name' => $street_name,
                    'city_id' => $city_id
                )
            )
        );
        return $check_street;
    }

    /**
     * @param $city_name
     * @param $region_id
     */
    private function addCity($city_name, $region_id){
        if($region_id == null){
            $region_id = $this->SPB_REGION_ID;
        }
        $new_street = [
            'name' => $city_name,
            'region_id' => $region_id,
        ];
        $this->City->create();
        $this->City->save($new_street);
    }

    /**
     * @param $street_name
     * @param $type
     * @param $city_id
     */
    private function addStreet($street_name, $type, $city_id){
        $new_street = [
            'city_id' => $city_id,
            'name' => $street_name,
            'type' => $type,
        ];
        $this->Street->create();
        $this->Street->save($new_street);
    }

    /**
     * Генерация улиц в БД на основе ссылки для парсинга из БД ФИАС
     */
    public function streetParser()
    {
        $parsing_url = "http://basicdata.ru/online/fias/";// . 752d9929-945c-4464-ae38-20a626108da9/
        $fias_id = $this->request->query('fias_id');
        $city_name = $this->request->query('city');
        $url_template = $parsing_url . $fias_id . "/";
        $page_count = $this->request->query('page');
        if($city_name==null){
            echo "город не указан";
            exit;
        }

        $check_city  = $this->checkCity($city_name);

        if (count($check_city) == 0) {
            $this->addCity($city_name, null);
        }
        $city_id = $check_city['City']['id'];
        for ($page = 1; $page <= $page_count; $page++) {
            $url = $url_template . $page . "/";
            $this->parseUrl($url, $city_id);
        }
        echo "Улицы города были успешно сохранены";
        exit;
    }
}