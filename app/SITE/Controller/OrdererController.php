<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class OrdererController extends AppController
{
    public $uses = array(
        'City',
        'Orderer',
        'Order',
        'Order_Comment',
        'Region',
        'Shop',
    );

    public $components = array(
        'Session',
        'Breadcrumbs',
        'Flash',
        'Uploader',
        'ShopOwnerCom',
        'ProductCom',
        'OrderCom',
        'Activity'
    );

    public $layout = "default";

    public function beforeFilter()
    {
         $this->Breadcrumbs->add(_("Клиенты"), Router::url(array('plugin' => false, 'controller' => 'order', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function profile()
    {
        $profile_url_prefix = $this->request->params['profile'];
        if (is_numeric($profile_url_prefix)) {
            $param = 'id';
        } else {
            $param = 'login';
        }
        $admin = $this->Orderer->find('first', array('conditions' => array(
            $param => $profile_url_prefix,
        ),
            'fields' => array('Orderer.*',
                'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS reg_time',
                //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(last_activity) AS last_act',
            )
        ));
        $this->set("profile", $admin);
    }

    public function index()
    {
        //TODO
        $active_only = $this->request->query('active_only') ?? null;
        $stopped_offers = $this->request->query('stopped_offers') ?? null;
        //$user_id = $this->request->query('user_id') ?? null;

        $city_id = $this->request->query('city_id') ?? null;
        $shop_id = $this->request->query('shop_id') ?? null;

        $sort_type = $this->request->query('sort_type') ?? null;
        $sort_dir = $this->request->query('sort_dir') ?? null;
        $page = $this->request->query('page') ?? 1;
        $orderer_id = $this->request->query('orderer_id') ?? null;

        $form_data = array(
            //'user_id' => $user_id,
            'active_only' => $active_only,
            'stopped_offers' => $stopped_offers,
            'sort_type' => $sort_type,
            'sort_dir' => $sort_dir,
            'city_id' => $city_id,
            'shop_id' => $shop_id,
            'page' => $page,
            'orderer_id' => $orderer_id
        );
        $this->set('form_data', $form_data);
        $this->set('title', "список клиентов");

        $shops = $this->Shop->find("all",
            array(
                'conditions' =>
                    array(
                        'shop_owner_id' => $this->ShopOwnerCom->shop_owner_id()
                    ),
                'order' => 'id ASC'
            )
        );
        $this->set('shops', $shops);

        // russian regions
        $russian_country_id = 218;
        $regions = $this->Region->find("all",
            array(
                'conditions' =>
                    array(
                        'country_id' => $russian_country_id
                    ),
                'order' => 'name ASC'
            )
        );

        $regions_list = [];
        foreach ($regions as $region) {
            $region = $region['Region']['id'];
            if (!in_array($region, $regions_list)) {
                $regions_list[] = $region;
            }
        }
        $cities = $this->City->find("all",
            array(
                'conditions' =>
                    array(
                        'region_id' => $regions_list
                    ),
                'order' => 'name ASC'
            )
        );
        $this->set('cities', $cities);

        $shop_orderers = $this->Orderer->find("all",
            array('conditions' =>
                array(
                    'Shop.id' => $this->ShopOwnerCom->shop_list(),
                ),
                'joins' => array(
                    array(
                        'table' => 'orders',
                        'alias' => 'Order',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Order.orderer_id = Orderer.id'
                        )
                    ),
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Shop.id = Order.shop_id'
                        )
                    ),

                ),
                'fields' => array(
                    'DISTINCT Orderer.*',
                ),
                'order' => array("Orderer.lastname ASC")
            )
        );

        $this->set('orderers', $shop_orderers);

    }

    /**контент страницы - клиенты  + пагинация */
    public function orderers_content()
    {
        $this->layout = false;
        $city_id = $this->request->data['city_id'] ?? null;
        $orderer_id = $this->request->data['orderer_id'] ?? null;
        $shop_id = $this->request->data['shop_id'] ?? null;
        $status = $this->request->data['status'] ?? null;

        if ($city_id == null OR $city_id == "0") {
            $city_ids_array = [];
        } else {
            $city_ids_array = ['City.id' => $city_id];
        }

        if ($orderer_id == null OR $orderer_id == "0") {
            $orderer_ids_array = [];
        } else {
            $orderer_ids_array = ['Order.orderer_id' => $orderer_id];
        }

        if ($shop_id == null OR $shop_id == "0") {
            $shop_ids_array = [];
        } else {
            $shop_ids_array = ['Order.shop_id' => $shop_id];
        }
        if ($status == null OR $status == "" OR !in_array($status, array_keys($this->OrderCom->valid_order_status_list))) {
            $status_array = [];
        } else {
            $status_array = ['Order.status' => $status];
        }

        $sort_type = $this->request->data('sort_type') ?? "created";
        $sort_dir = $this->request->data('sort_dir') ?? null;

        $sort_dir = strtoupper($sort_dir);
        if ($sort_dir != "ASC" AND $sort_dir != "DESC") {
            $sort_dir = "ASC";
        }

        $sort_type_query = "";
         if ($sort_type == "created") {
            $sort_type_query = "Order.created";
        } else if ($sort_type == "name") {
            $sort_type_query = "Orderer.lastname";
        } else if ($sort_type == "activity") {
            $sort_type_query = "Orderer.created";
        } else {
            $sort_type_query = "Orderer.id";
        }
        $order_query = array("$sort_type_query $sort_dir");

        $page = $this->request->query['page'] ?? 1;
        $c_count = 5;

        $show_count = $this->request->query['count'] ?? $c_count;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        $total_orders_count = $this->Orderer->find("count",
            array('conditions' =>
                array(
                    'Order.shop_id' => $this->ShopOwnerCom->shop_list(),
                    $city_ids_array,
                    $orderer_ids_array,
                    $shop_ids_array,
                    $status_array
                ),
                'joins' => array(
                    array(
                        'table' => 'orders',
                        'alias' => 'Order',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Orderer.id = Order.orderer_id'
                        )
                    ),
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Shop.id = Order.shop_id'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Orderer.city_id = City.id'
                        )
                    ),
                ),
                'fields' => array(
                    'DISTINCT Orderer.*',
                ),
            )
        );

        $page_orderers = $this->Orderer->find("all",
            array('conditions' =>
                array(
                    'Shop.id' => $this->ShopOwnerCom->shop_list(),
                    $city_ids_array,
                    $orderer_ids_array,
                    $shop_ids_array,
                    $status_array
                ),
                'joins' => array(
                    array(
                        'table' => 'orders',
                        'alias' => 'Order',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Order.orderer_id = Orderer.id'
                        )
                    ),
                    array(
                        'table' => 'shops',
                        'alias' => 'Shop',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Shop.id = Order.shop_id'
                        )
                    ),
                    // город заказа
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'City.id = Orderer.city_id'
                        )
                    ),
                ),
                'fields' => array(
                    'DISTINCT Orderer.*',
                    'City.*',
                    //'Order.*',
                    //'Shop.*',

                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array("$sort_type_query" => "$sort_dir")
            )
        );

        foreach ($page_orderers as &$orderer) {
            $orderer_id = $orderer['Orderer']['id'];
            $orderer['order_count'] = count($this->OrderCom->get_orders_by_orderer($orderer_id));
            $orderer['last_order'] = $this->OrderCom->get_last_order_by_orderer($orderer_id);
        }
        $form_data = [];
        $form_data['active_only'] = "on";
        $form_data['orderer_id'] = $orderer_id;
        $form_data['shop_id'] = $shop_id;
        $form_data['city_id'] = $city_id;
        $form_data['status'] = $status;


        $this->set('form_data', $form_data);
        $this->set("orderers", $page_orderers);
        $this->set('shop_list', $this->ShopOwnerCom->shop_list_full());
        $this->set('orderers_list', $this->OrderCom->shop_orderers_list());

        $this->set('orderers_city_list', $this->OrderCom->shop_orderers_city_list());

        $pages = ceil($total_orders_count / $show_count);
        $this->set('page', $page);
        $this->set("pages", $pages);

        $this->set("order_status_list", $this->OrderCom->valid_order_status_list);
    }

    public function view()
    {
        $id = $this->request->param('id');

        $orderer = $this->Orderer->find("first",
            array('conditions' =>
                array(
                    'Orderer.id' => $id
                ),
                'joins' => array(
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'City.id = Orderer.city_id'
                        )
                    ),
                ),
                'fields' => array(
                    'DISTINCT Orderer.*',
                    'City.*',
                ),
            )
        );

        //last order in current shop owner
        $last_order = $this->Order->find("first",
            array('conditions' =>
                array(
                    //'Orderer.id' => $id
                ),
                'fields' => array(
                    'Order.*',
                ),
                'order' => array('id DESC')
            )
        );
        $last_order['order_total_cost'] = $this->OrderCom->shop_order_sum($last_order['Order']['id']);


        $this->set('orderer', $orderer);
        $this->set('last_order', $last_order);
        $orderer_name = prepare_fio($orderer['Orderer']['lastname'], $orderer['Orderer']['firstname'], "");
        $this->set('orderer_name', $orderer_name );

        $this->Breadcrumbs->add("Клиент " . $orderer_name, Router::url(array('plugin' => false, 'controller' => 'orderer', 'action' => 'view', 'id' => $id)));

        $this->set('title', "Клиент " . $orderer_name);
    }

}