<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');
App::uses('Xml', 'Utility');

class ProductController extends AppController
{
    public  $uses                  = [
        'Brand',
        'Country',
        'Manufacturer',
        'Order',
        'Orderer',
        'Order_Product',
        'ProductCategory',
        'ProductCategoryParam',
        'ProductCategoryParamValues',
        'Product_Image',
        'Product_Moderation',
        'Product_Moderation_List',
        'Product',
        'Product_Param',
        'Product_Rating',
        'Product_View',
        'Shop_Product',
        'Shop',
        'Shop_Import',
        'Shop_Import_Item',
    ];

    public  $components            = [
        'Breadcrumbs',
        'Cacher',
        "Country",
        'CompanyCom',
        'Flash',
        'ProductCom',
        'Parser',
        'Session',
        'Search',
        'Uploader',
        'UserCom',
        'Validator',
    ];

    public  $layout                = "default";

    private $count_product_default = 18;

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(
            "Товары",
            Router::url(['plugin' => false, 'controller' => 'product', 'action' => 'index'])
        );
        parent::beforeFilter();
    }

    public function view()
    {
        $id = $this->request->param('id');
        if (!$this->Validator->valid_int($id)) {
            $this->Api->response_api(["error" => " product with id $id not found "], "error");
            exit;
        }

        $no_cache     = $this->request->query('no_cache') ?? $this->request->data('no_cache');
        $flush_cache  = $this->request->query('flush_cache') ?? $this->request->data('flush_cache');
        $cached_route = "/v1/product/$id";

        if ($flush_cache != null) {
            $cache_check = $this->Cacher->json_cache($cached_route);
            if (is_array($cache_check)) {
                unlink($cache_check['file']);
            }
        }

        if ($no_cache == null) {

            $cache_check = $this->Cacher->json_cache($cached_route);
            if (is_array($cache_check)) {
                echo file_get_contents($cache_check['file']);
                exit;
            }
        }

        $product = $this->Product->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($product) == 0) {
            $this->Api->response_api(["error" => " product with id $id not found "], "error");
            exit;
        }

        $product_name    = $product['Product']['product_name'];
        $product_barcode = $this->ProductCom->systemBarcode(
            $product['Product']['id'],
            $product['Product']['category_id']
        );

        $result                = [];
        $result['category_id'] = (int)$product['Product']['category_id'];
        $category_name         = $this->ProductCom->getCategoryById($product['Product']['category_id']);

        $product_images = $this->Product_Image->find(
            "all",
            [
                'conditions' =>
                    [
                        'product_id' => $id,
                        //'type' => 'normal'
                    ],
                'order'      => ['file ASC'],
            ]
        );
        // отзывы о товары
        $product_rating_parent = $this->Product_Rating->find(
            "all",
            [
                'conditions' =>
                    [
                        'product_id' => $id,
                        'parent_id'  => '0',
                    ],
            ]
        );

        $country_name = $product['Product']['country_id'] > 0 ? $this->Country->getCountryNameById(
            $product['Product']['country_id']
        ) : " Не определено";

        $product_image_array = count($product_images) > 0 ? $product_images : null;
        //$this->set('product_images', $product_image_array);
        // $this->set('country_name', $country_name);

        /* ЗАГЛУШКА - ДЛЯ ПОКАЗА ТОВАРОВ ПЕТРОВИЧА, переделать TODO  */
        $company_id                  = 1;
        $product_default_params_list = [];
        $product_shop_data           = $this->ProductCom->ShopProductData($id, $company_id);

        $product_default_params_list['package_weight'] = $product_shop_data['Shop_Product']['package_weight'] ?? null;
        $product_default_params_list['package_count']  = $product_shop_data['Shop_Product']['package_count'] ?? null;
        $product_default_params_list['package_width']  = $product_shop_data['Shop_Product']['package_width'] ?? null;
        $product_default_params_list['package_height'] = $product_shop_data['Shop_Product']['package_height'] ?? null;
        $product_default_params_list['package_length'] = $product_shop_data['Shop_Product']['package_length'] ?? null;
        $product_default_params_list['shop_url']       = $product_shop_data['Shop_Product']['shop_url'] ?? null;

        $result['preorder_only'] = $product_shop_data['Shop_Product']['preorder_only'] ?? null;
        $result['sales_notes']   = $product_shop_data['Shop_Product']['sales_notes'] ?? null;

        $result['description'] = $product['Product']['description'];

        /* фильтры для конкретного товара в конкретной категории */
        $product_param_data = $this->ProductCom->getFiltersAndValuesByProductId($id);
        $this->set('product_param_data', $product_param_data);

        $result['barcode']        = $product_barcode;
        $result['name']           = $product_name;
        $result['content_server'] = Configure::read('CONTENT_SERVER');
        $result['weight']         = $product['Product']['weight'];
        $result['width']          = $product['Product']['width'];
        $result['height']         = $product['Product']['height'];
        $result['length']         = $product['Product']['length'];
        $result['volume']         = $product['Product']['volume'];

        $image_types = [];
        if (count($product_image_array) > 0) {
            foreach ($product_image_array as $item) {
                $image_hash = substr($item['Product_Image']['file'], 0, 8);
                if (empty($old_hash)) {
                    $old_hash      = $image_hash;
                    $image_types[] = $item['Product_Image']['type'];
                    continue;
                }
                if ($image_hash == $old_hash) {
                    if (!in_array($item['Product_Image']['type'], $image_types)) {
                        $image_types[] = $item['Product_Image']['type'];
                    }
                } else {
                    $result['images'][] = [
                        'alt'      => '',
                        'hash'     => $image_hash,
                        'types'    => $image_types,
                        'image_id' => $item['Product_Image']['image_id'],
                    ];
                    $old_hash           = $image_hash;
                    $image_types        = [];
                }
            }
            $result['images'][] = [
                'alt'      => '',
                'hash'     => $image_hash,
                'types'    => $image_types,
                'image_id' => $item['Product_Image']['image_id'],
            ];
        }

        $result['id']             = (int)$id;
        $result['rating']         = (float)$product[0]['stat__current_rating'];
        $result['comments_count'] = count($product_rating_parent);
        $result['category_name']  = $category_name;
        $result['country_name']   = $country_name;

        // количество просмотров
        $result['product_view'] = $this->ProductCom->getTotalProductViews($id);

        $specifications = [];
        foreach ($product_param_data as $product_param_item) {
            if (empty($product_param_item['Category_Product_Param']['is_important']) or $product_param_item['Category_Product_Param']['is_important'] == "false") {
                $isImportant = false;
            } else {
                $isImportant = true;
            }

            $spec             = [
                'name'         => $product_param_item['Category_Product_Param']['param_name'],
                'is_important' => $isImportant,
                'unit'         => $product_param_item['Category_Product_Param']['unit'],
                'min'          => $product_param_item['Category_Product_Param']['min_size'],
                'max'          => $product_param_item['Category_Product_Param']['max_size'],
                'value'        => $product_param_item['Category_Product_Param_Value']['value'],
                'type'         => $product_param_item['Category_Product_Param']['param_type'],
            ];
            $specifications[] = $spec;
        }

        $specifications[] = [
            'value'        => $product_default_params_list['package_weight'],
            'name'         => 'Вес упаковки',
            'is_important' => false,
            'unit'         => 'кг',
            'type'         => 'number',
        ];

        $specifications[] = [
            'value'        => $product_default_params_list['package_count'],
            'name'         => 'Количество в упаковке',
            'is_important' => false,
            'unit'         => 'шт',
            'type'         => 'number',
        ];

        $specifications[] = [
            'value'        => $product_default_params_list['package_width'],
            'name'         => 'Ширина упаковки',
            'is_important' => false,
            'unit'         => 'мм',
            'type'         => 'number',
        ];

        $specifications[] = [
            'value'        => $product_default_params_list['package_height'],
            'name'         => 'Высота упаковки',
            'is_important' => false,
            'unit'         => 'мм',
            'type'         => 'number',
        ];

        $specifications[] = [
            'value'        => $product_default_params_list['package_length'],
            'name'         => 'Длина упаковки',
            'is_important' => false,
            'unit'         => 'мм',
            'type'         => 'number',
        ];

        $result['specifications'] = $specifications;

        $allCompanyOffersResult = [];
        $allCompanyOffers       = $this->ProductCom->getProductOfferInAllCompanies($id);

        foreach ($allCompanyOffers as $allCompanyOffer) {
            $offer_id = $allCompanyOffer['Shop_Product']['id'];
            if (key_exists('Shop_Product', $allCompanyOffer)) {
                $offerShop = $allCompanyOffer['Shop_Product'];
            }
            if (key_exists('Company', $allCompanyOffer)) {
                $offerCompany = $allCompanyOffer['Company'];
            }
            if (key_exists('CompanyComment', $allCompanyOffer)) {
                $companyComments = $allCompanyOffer['CompanyComment'];
            }
            $delivery = $offerShop['delivery'];
            if ($delivery) {
                $delivery_type = [
                    "transportCompany" => [
                        'price' => $offerShop['delivery_cost'],
                        'date'  => [$offerShop['delivery_days_min'], $offerShop['delivery_days_max']],
                    ],
                ];
            } else {
                $delivery_type = [];
            }
            $company_ratings_count    = count($this->CompanyCom->company_comments($offerCompany['id']));
            $allCompanyOffersResult[] = [
                'offer_id'       => (int)$offer_id,
                'price'          => (float)$offerShop['base_price'],
                'company_offer'  => [
                    'id'     => (int)$offerCompany['id'],
                    'name'   => $offerCompany['company_name'],
                    'rating' => (float)$allCompanyOffer[0]['stat__current_rating'],
                ],
                'old_price'      => (float)$offerShop['old_price'],
                'url'            => $offerShop['shop_url'] ?? null,
                'comments_count' => $company_ratings_count,
                'has_chat'       => false,
                'delivery'       => [$delivery_type, 'pickup' => $offerShop['pickup']],
                //'phone' => $offerCompany['phone_number'],
            ];
        }
        $result['offers'] = $allCompanyOffersResult;

        $this->Cacher->set_json_cache($cached_route, $result);
        $this->Api->response_api($result);
        $user_id                = $this->UserCom->getUserSessionId();
        $result['product_view'] = $this->ProductCom->addProductView($user_id, $id);
        exit;
    }

    /**
     *  Отдать список товаров по пришедших по АПИ id товаров
     */
    public function productListApi()
    {
        if (!empty($this->request->data('product_ids'))) {
            $product_ids = json_decode($this->request->data('product_ids'));

            if (is_null($product_ids)) {
                $this->Api->response_api(['error' => 'Пустой массив с ID товаров'], 'error');
                exit();
            }

            $data = [
                'ids'        => $product_ids,
                'count'      => count($product_ids),
                'page'       => 1,
                'sort_field' => false,
            ];

            $offers_collection = $this->ProductCom->getOffersFilterId($data);

            if ($offers_collection) {
                $data['ids'] = $offers_collection['products'];

                $products    = $this->ProductCom->getProductList($data);
                $data['ids'] = $products['ids'];

                $images_product = $this->ProductCom->getImagesProductList($data['ids']);
                $params_product = $this->ProductCom->getParamsProductList($data['ids']);
                $offers_product = $this->ProductCom->getOffersProductList($data);

                $i = 0;
                foreach ($products['products'] as $k => $v) {
                    $response[$i]                   = $v;
                    $response[$i]['images']         = !empty($images_product[$k]) ? $images_product[$k] : [];
                    $response[$i]['offers']         = !empty($offers_product[$k]) ? $offers_product[$k] : [];
                    $response[$i]['specifications'] = !empty($params_product[$k]) ? $params_product[$k] : [];
                    $i++;
                }

                $return = [
                    'products'    => $response,
                    'total_items' => $offers_collection['count'],
                ];
                $this->Api->response_api($return, 'success');
            } else {
                $this->Api->response_api(['error' => 'Нет предложений по данным товарам'], 'error');
            }
        } else {
            $this->Api->response_api(['error' => 'ID товаров не получены'], 'error');
        }
        exit();
    }

    /**
     * Поиск предложений по строке поиска с фильтрами и пагинецией
     */
    public function searchProducts()
    {
        if (!empty($this->request->query('q'))) {
            $data = [
                'query'         => trim($this->request->query('q')),
                'count'         => trim($this->request->query('count')) ?? $this->count_product_default,
                'page'          => $this->request->query('page') ?? 1,
                'sort_field'    => $this->request->query('sort_field') ?? false,
                'sort_dir'      => $this->request->query('sort_dir') ?? false,
                'price_min'     => $this->request->query('price_min') ?? false,
                'price_max'     => $this->request->query('price_max') ?? false,
                'active_offers' => $this->request->query('active_offers') ?? false,
            ];

            $res = $this->Search->baseSearch($data['query']);

            if (count($res['products']) > 0) {
                $data['ids']  = $res['products'];
                $offers_query = $this->ProductCom->getOffersFilterId($data);
                if ($offers_query) {
                    $data['ids'] = $offers_query['products'];
                } else {
                    response_api(
                        [
                            'error' => 'Найдено - ' . count(
                                    $res['products']
                                ) . ' позиций, но нет предложений партнеров по данному товару.',
                        ],
                        'error'
                    );
                    exit();
                }

                $products_collection = $this->ProductCom->getProductList($data);
                if ($products_collection) {
                    $data['ids'] = $products_collection['ids'];

                    $images_product = $this->ProductCom->getImagesProductList($data['ids']);
                    $params_product = $this->ProductCom->getParamsProductList($data['ids']);
                    $offers_product = $this->ProductCom->getOffersProductList($data);

                    $i = 0;
                    foreach ($products_collection['products'] as $k => $v) {
                        $response[$i]                   = $v;
                        $response[$i]['images']         = !empty($images_product[$k]) ? $images_product[$k] : [];
                        $response[$i]['offers']         = !empty($offers_product[$k]) ? $offers_product[$k] : [];
                        $response[$i]['specifications'] = !empty($params_product[$k]) ? $params_product[$k] : [];
                        $i++;
                    }

                    response_api(['products' => $response], 'success');
                } else {
                    response_api(['error' => 'Товары не найдены'], 'error');
                }
            } else {
                response_api(['error' => 'Товары не найдены'], 'error');
            }
        } else {
            response_api(['error' => 'Строка поиска не получена'], 'error');
        }
        exit();
    }

    public function searchProductsInCat()
    {
        $data = [
            'cat_id'         => $this->request->param('category_id'),
            'query'          => trim($this->request->query('q')) ?? false,
            'count'          => $this->request->query('count') ?? $this->count_product_default,
            'page'           => $this->request->query('page') ?? 1,
            'sort_field'     => $this->request->query('sort_field') ?? false,
            'sort_dir'       => $this->request->query('sort_dir') ?? false,
            'price_min'      => $this->request->query('price_min') ?? false,
            'price_max'      => $this->request->query('price_max') ?? false,
            'active_offers'  => $this->request->query('active_offers') ?? false,
            'delivery'       => $this->request->query('delivery') ?? false,
            'payment_method' => $this->request->query('payment_method') ?? false,
            'company_rating' => $this->request->query('company_rating') ?? false,
        ];

        $filters = $this->ProductCom->getFiltersFromRequest($this->request);
        if ($filters) {
            $data['filters'] = $filters;
        }

        $all_products_in_cat = $this->ProductCom->getProductsInCatId($data);
        if ($all_products_in_cat) {
            $data['ids']   = $all_products_in_cat['products'];
            $offers_filter = $this->ProductCom->getOffersFilterId($data);

            if ($offers_filter) {
                $data['ids']         = $offers_filter['products'];
                $products_collection = $this->ProductCom->getProductList($data);
                if ($products_collection) {
                    $data['ids'] = $products_collection['ids'];

                    $images_product = $this->ProductCom->getImagesProductList($data['ids']);
                    $params_product = $this->ProductCom->getParamsProductList($data['ids']);
                    $offers_product = $this->ProductCom->getOffersProductList($data);

                    $i = 0;
                    foreach ($products_collection['products'] as $k => $v) {
                        $response[$i]                   = $v;
                        $response[$i]['images']         = !empty($images_product[$k]) ? $images_product[$k] : [];
                        $response[$i]['offers']         = !empty($offers_product[$k]) ? $offers_product[$k] : [];
                        $response[$i]['specifications'] = !empty($params_product[$k]) ? $params_product[$k] : [];
                        $i++;
                    }
                    $return = [
                        'products'    => $response,
                        'pages'       => $data['page'],
                        'total_items' => $offers_filter['count'],
                    ];
                    response_api($return, 'success');
                } else {
                    response_api(['error' => 'В категории нет товаров'], 'error');
                }
            } else {
                response_api(
                    [
                        'error' => 'Найдено - ' . count(
                                $all_products_in_cat['products']
                            ) . ' позиций, но нет предложений партнеров по данному товару.',
                    ],
                    'error'
                );
                exit();
            }
        } else {
            response_api(['error' => 'В категории нет товаров'], 'error');
        }
        exit();
    }

    /**
     * Добавление отзыва к товару
     */
    public function addProductComment()
    {
        $user_id = $this->UserCom->getUserId();
        /*if(intval($user_id)<=0){
            response_api(["error" => "user is not authorized"], "error");
            exit;
        }*/
        $rating = $this->request->query('product_rating') ?? $this->request->data('product_rating');
        if ($rating <= 0 or $rating > 5) {
            response_api(["error" => "product rating incorrect value"], "error");
            exit;
        }

        $text = $this->request->query('text') ?? $this->request->data('text');
        $text = trim($text);
        if (empty($text)) {
            response_api(["error" => "empty comment text"], "error");
            exit;
        }

        $parent_id = $this->request->query('parent_id') ?? $this->request->data('parent_id');

        $product_id = $this->request->param('product_id') ?? null;
        if ($product_id <= 0) {
            response_api(["error" => " product_id is not set"], "error");
            exit;
        }

        $experience = $this->request->query('experience') ?? $this->request->data('experience');
        $use_photos = $this->request->query('photos') ?? $this->request->data('photos');
        if ($use_photos == null) {
            $use_photos = [];
        }

        if ($this->ProductCom->userAddProductComment(
            $user_id,
            $product_id,
            $rating,
            $text,
            $experience,
            $use_photos,
            $parent_id
        )) {
            response_api(["success" => true], "success");
            exit;
        }
    }

    public
    function collectSearchPreferProduct()
    {
        $product_id = $this->request->param('product_id');
        $search_id  = $this->request->param('search_id');
        $orderer_id = $this->UserCom->getUserId();
        if (!$orderer_id) {
            $user_id = $this->UserCom->getUserSessionId();
        }
        $result = $this->Search->collectSearchPrefer($search_id, $user_id, "product", $product_id);
        response_api($result, "success");
        exit;
    }

    public
    function collectSearchPreferBrand()
    {
        $search_id  = $this->request->param('search_id');
        $brand_id   = $this->request->param('brand_id');
        $orderer_id = $this->UserCom->getUserId();
        if (!$orderer_id) {
            $user_id = $this->UserCom->getUserSessionId();
        }
        $result = $this->Search->collectSearchPrefer($search_id, $user_id, "brand", $brand_id);
        response_api($result, "success");
        exit;
    }

    public
    function collectSearchPreferCategory()
    {
        $search_id   = $this->request->param('search_id');
        $category_id = $this->request->param('category_id');
        $orderer_id  = $this->UserCom->getUserId();
        if (!$orderer_id) {
            $user_id = $this->UserCom->getUserSessionId();
        }
        $result = $this->Search->collectSearchPrefer($search_id, $user_id, "category", $category_id);
        response_api($result, "success");
        exit;
    }

    public
    function search()
    {
        $q = $this->request->query('q');
        if (mb_strlen($q) <= 1) {
            response_api(["error" => " search string is empty"], "error");
            exit;
        }

        $page = $this->request->query('page') ?? null;
        // защита от чрезмерно активного поиска
        $orderer_id = $this->UserCom->getUserId();
        $user_id    = null;
        if (!$orderer_id) {
            $user_id = $this->UserCom->getUserSessionId();
        }

        if (!$this->Search->checkSearch($orderer_id, $user_id)) {
            response_api(["error" => " too much request per user "], "error");
            exit;
        }

        //$this->ProductCom->searchProductsByString($q);

        //если используется прямой поиск без кеширования и если закешированный файл существует, возвращается кеш результатов поиска
        $no_cache = $this->request->query('no_cache') ?? null;
        if ($no_cache == null) {
            $cached_route = "/v1/search/?q=" . $q . "&page=" . $page;
            $cache_check  = $this->Cacher->json_cache($cached_route);
            if ($cache_check) {
                echo file_get_contents($cache_check);
                exit;
            }
        }

        // если поисковый кеш отсутствует, запускается прямой поиск
        $found_data = $this->Search->baseSearch($q);
        response_api($found_data, "success");
        exit;

        $product = $this->Product->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($product) == 0) {
            response_api(["error" => " product with id $id not found "], "error");
            exit;
        }

        $product_name    = $product['Product']['product_name'];
        $product_barcode = $this->ProductCom->systemBarcode(
            $product['Product']['id'],
            $product['Product']['category_id']
        );

        $category_name = $this->ProductCom->getCategoryById($product['Product']['category_id']);

        $product_images = $this->Product_Image->find(
            "all",
            [
                'conditions' =>
                    [
                        'product_id' => $id,
                        //'type' => 'normal'
                    ],
                'order'      => ['file ASC'],
            ]
        );
        // отзывы о товары
        $product_rating_parent = $this->Product_Rating->find(
            "all",
            [
                'conditions' =>
                    [
                        'product_id' => $id,
                        'parent_id'  => '0',
                    ],
            ]
        );

        $country_name = $product['Product']['country_id'] > 0 ? $this->Country->getCountryNameById(
            $product['Product']['country_id']
        ) : " Не определено";

        $product_image_array = count($product_images) > 0 ? $product_images : null;
        //$this->set('product_images', $product_image_array);
        // $this->set('country_name', $country_name);

        /* ЗАГЛУШКА - ДЛЯ ПОКАЗА ТОВАРОВ ПЕТРОВИЧА, переделать TODO  */
        $company_id                  = 1;
        $product_default_params_list = [];
        $product_shop_data           = $this->ProductCom->ShopProductData($id, $company_id);

        $product_default_params_list['package_weight'] = $product_shop_data['Shop_Product']['package_weight'];
        $product_default_params_list['package_count']  = $product_shop_data['Shop_Product']['package_count'];
        $product_default_params_list['package_width']  = $product_shop_data['Shop_Product']['package_width'];
        $product_default_params_list['package_height'] = $product_shop_data['Shop_Product']['package_height'];
        $product_default_params_list['package_length'] = $product_shop_data['Shop_Product']['package_length'];
        $product_default_params_list['shop_url']       = $product_shop_data['Shop_Product']['shop_url'];

        $result['preorder_only'] = $product_shop_data['Shop_Product']['preorder_only'];
        $result['sales_note']    = $product_shop_data['Shop_Product']['sales_note'];

        $result['description'] = $product['Product']['description'];

        /* фильтры для конкретного товара в конкретной категории */
        $product_param_data = $this->ProductCom->getFiltersAndValuesByProductId($id);
        $this->set('product_param_data', $product_param_data);

        $result                   = [];
        $result['barcode']        = $product_barcode;
        $result['name']           = $product_name;
        $result['content_server'] = Configure::read('CONTENT_SERVER');

        $image_types = [];
        foreach ($product_image_array as $item) {
            $image_hash = substr($item['Product_Image']['file'], 0, 8);
            if (empty($old_hash)) {
                $old_hash      = $image_hash;
                $image_types[] = $item['Product_Image']['type'];
                continue;
            }
            //$url_file = contentServerFile($item['Product_Image']['file'], "product_images", Configure::read('CONTENT_SERVER'), $id);
            if ($image_hash == $old_hash) {
                if (!in_array($item['Product_Image']['type'], $image_types)) {
                    $image_types[] = $item['Product_Image']['type'];
                }
            } else {
                $result['images'][] = [
                    'alt'      => '',
                    'hash'     => $image_hash,
                    'types'    => $image_types,
                    'image_id' => $item['Product_Image']['image_id'],
                ];
                $old_hash           = $image_hash;
                $image_types        = [];
            }
        }
        $result['images'][] = [
            'alt'      => '',
            'hash'     => $image_hash,
            'types'    => $image_types,
            'image_id' => $item['Product_Image']['image_id'],
        ];

        $result['rating']        = $product[0]['stat__current_rating'];
        $result['commentsCount'] = count($product_rating_parent);
        $result['categoryName']  = $category_name;
        $result['countryName']   = $country_name;
        $specifications          = [];
        foreach ($product_param_data as $product_param_item) {
            //pr($product_param_item);

            if (empty($product_param_item['Category_Product_Param']['is_important']) or $product_param_item['Category_Product_Param']['is_important'] == "false") {
                $isImportant = false;
            } else {
                $isImportant = true;
            }

            $spec             = [
                'name'        => $product_param_item['Category_Product_Param']['param_name'],
                'isImportant' => $isImportant,
                'unit'        => $product_param_item['Category_Product_Param']['unit'],
                'min'         => $product_param_item['Category_Product_Param']['min_size'],
                'max'         => $product_param_item['Category_Product_Param']['max_size'],
                'value'       => $product_param_item['Category_Product_Param_Value']['value'],
                'type'        => $product_param_item['Category_Product_Param']['param_type'],
            ];
            $specifications[] = $spec;
        }

        $specifications[] = [
            'value'       => $product_default_params_list['package_weight'],
            'name'        => 'Вес упаковки',
            'isImportant' => false,
            'unit'        => 'кг',
            'type'        => 'number',
        ];

        $specifications[] = [
            'value'       => $product_default_params_list['package_count'],
            'name'        => 'Количество в упаковке',
            'isImportant' => false,
            'unit'        => 'шт',
            'type'        => 'number',
        ];

        $specifications[] = [
            'value'       => $product_default_params_list['package_width'],
            'name'        => 'Ширина упаковки',
            'isImportant' => false,
            'unit'        => 'мм',
            'type'        => 'number',
        ];

        $specifications[] = [
            'value'       => $product_default_params_list['package_height'],
            'name'        => 'Высота упаковки',
            'isImportant' => false,
            'unit'        => 'мм',
            'type'        => 'number',
        ];

        $specifications[] = [
            'value'       => $product_default_params_list['package_length'],
            'name'        => 'Длина упаковки',
            'isImportant' => false,
            'unit'        => 'мм',
            'type'        => 'number',
        ];

        $result['specifications'] = $specifications;

        $allCompanyOffersResult = [];
        $allCompanyOffers       = $this->ProductCom->getProductOfferInAllCompanies($id);

        foreach ($allCompanyOffers as $allCompanyOffer) {

            //pr($allCompanyOffer);
            if (key_exists('Shop_Product', $allCompanyOffer)) {
                $offerShop = $allCompanyOffer['Shop_Product'];
            }
            if (key_exists('Company', $allCompanyOffer)) {
                $offerCompany = $allCompanyOffer['Company'];
            }
            if (key_exists('CompanyComment', $allCompanyOffer)) {
                $companyComments = $allCompanyOffer['CompanyComment'];
            }

            $delivery = $offerShop['delivery'];
            if ($delivery) {
                $delivery_type = [
                    "transportCompany" => [
                        'price' => $offerShop['delivery_cost'],
                        'date'  => [$offerShop['delivery_days_min'], $offerShop['delivery_days_max']],
                    ],
                ];
            } else {
                $delivery_type = [];
            }
            $company_ratings_count    = count($this->CompanyCom->company_comments($offerCompany['id']));
            $allCompanyOffersResult[] = [
                'price'         => $offerShop['base_price'],
                'id'            => $offerCompany['id'],
                'name'          => $offerCompany['company_name'],
                'oldPrice'      => $offerShop['old_price'],
                'rating'        => $allCompanyOffer[0]['stat__current_rating'],
                'url'           => $offerShop['shop_url'],
                'commentsCount' => $company_ratings_count,
                'hasChat'       => false,
                'delivery'      => [$delivery_type, 'pickup' => $offerShop['pickup']],
                'phone'         => $offerCompany['phone_number'],

            ];
        }
        $result['companies'] = $allCompanyOffersResult;

        $this->Cacher->set_json_cache($cached_route, $result);
        response_api($result);
        exit;
    }

    public
    function searchBigData()
    {
        $this->Search->searchBigData();
    }

    //    public function findElastic(){
    //        pr($this->Product->query('SELECT LEVENSHTEIN(`product_name`, "вантуc") as differrence, product_name, id FROM Products ORDER BY differrence ASC LIMIT 10'));
    //        exit;
    //    }

    public
    function realTimeParserProductPrice()
    {
        $shop_product_id = $this->request->param('shop_product_id') ?? null;

        $price_new = $this->Parser->realTimeParserProductPrice($shop_product_id, null);
        response_api(["price" => $price_new], "success");
        exit;
    }

    public
    function parsingProductPrice()
    {
        $this->Parser->parsingPriceQuery();
        response_api("success", "success");
        exit;
    }

    public
    function findElasticBrandName()
    {
        $query          = $this->request->query('q') ?? $this->request->data('q');
        $founded_brands = $this->Search->findElasticBrandName($query);

        response_api($founded_brands, "success");
        exit;
    }

    //____________________________________________________________________________________________________________________________
    /*
    public function index()
    {
        $active_only = $this->request->query('active_only') ?? null;
        $stopped_offers = $this->request->query('stopped_offers') ?? null;
        $user_id = $this->request->query('user_id') ?? null;

        $category_id = $this->request->query('category_id') ?? null;
        $shop_id = $this->request->query('shop_id') ?? null;

        $sort_type = $this->request->query('sort_type') ?? null;
        $sort_dir = $this->request->query('sort_dir') ?? null;
        $page = $this->request->query('page') ?? 1;
        $product_id = $this->request->query('product_id') ?? null;

        $form_data = array(
            'user_id' => $user_id,
            'active_only' => $active_only,
            'stopped_offers' => $stopped_offers,
            'sort_type' => $sort_type,
            'sort_dir' => $sort_dir,
            'category_id' => $category_id,
            'shop_id' => $shop_id,
            'page' => $page,
            'product_id' => $product_id
        );
        $this->set('form_data', $form_data);
        $this->set('title', "Управление товарами");

        $categories = $this->ProductCategory->find("all",
            array(
                'conditions' =>
                    array(//'parent_id' => $id
                    ),
                'order' => array(
                    'name' => 'ASC',
                )
            )
        );
        $this->set('categories', $categories);

        $shops = $this->Shop->find("all",
            array(
                'conditions' =>
                    array(
                        'shop_owner_id' => $this->ShopOwnerCom->shop_owner_id()
                    ),
                'order' => 'id ASC'
            )
        );
        $this->set('shops', $shops);

        $products = $this->Product->find("all",
            array(
                'conditions' =>
                    array(//'shop_owner_id' => $this->ShopOwnerCom->shop_owner_id()
                    ),
                'order' => 'product_name ASC'
            )
        );
        $this->set('products', $products);
    }

    public function product()
    {
        $product_id = $this->request->data('id') ? $this->request->data('id') : $this->request->query('id');
        if($product_id == null OR intval($product_id)<=0){
            response_api(['error_message' => "product with id $product_id not found"], "error");
            exit;
        }

        $found_products = $this->Product->find("first",
            array('conditions' =>
                array(
                    'id' => $product_id
                ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop_Product.product_id = Product.id'
                        )
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'ProductCategory.id = Product.category_id'
                        )
                    ),
                ),
                'fields' => array(
                    'ProductCategory.*',
                    'Product.*',
                    'Shop_Product.*',
                    //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Map.created) AS active_time',
                ),
            )
        );

        foreach ($found_products as &$found_product) {
            // составление читабельных названий категорий товара
            $category_id = $found_product['ProductCategory']['id'];
            $id = $found_product['Product']['id'];
            $category_build_name = $this->ProductCom->buildProductNameByCategories($category_id);
            $system_barcode = $this->ProductCom->systemBarcode($id, $category_id);

            $product_offers = $this->Shop_Product->find("all",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $id
                        ),
                )
            );

            $product_offers_count = count($product_offers);

            $product_order_counter = $this->Order_Product->find("count",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $id
                        ),
                )
            );

            $found_product['category_build_name'] = $category_build_name['name'];
            $found_product['system_barcode'] = $system_barcode;
            $found_product['offer_count'] = $product_offers_count;
            $found_product['order_count'] = $product_order_counter;

            // изображения товара

            $product_image = $this->Product_Image->find("first",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $id,
                            //'small !=' => ''
                        ),
                )
            );
            $found_product['image'] = count($product_image) > 0 ? $product_image['Image']['normal'] : null;
        }

        response_api(null, "success");
        exit;
    }

    public function findSubCats($category_id, &$subcats)
    {
        $sub_categories = $this->ProductCategory->find("all",
            array(
                'conditions' =>
                    array(
                        'parent_id' => $category_id
                    ),
            )
        );
        if (count($sub_categories) > 0) {
            foreach ($sub_categories as $category) {
                $cat_id = $category['ProductCategory']['id'];
                if (!in_array($cat_id, $subcats)) {
                    $subcats[] = $cat_id;
                }
                $this->findSubCats($cat_id, $subcats);
            }
        }
    }

    public function products_content()
    {
        $this->layout = false;
        $active_only = $this->request->data('active_only') ?? null;
        $stopped_offers = $this->request->data('stopped_offers') ?? null;
        $user_id = $this->request->data('user_id') ?? null;
        $shop_id = $this->request->data('shop_id') ?? null;
        $category_id = $this->request->data('category_id') ?? null;
        $product_id = $this->request->data('product_id') ?? null;

        $sort_type = $this->request->data('sort_type') ?? "created";
        $sort_dir = $this->request->data('sort_dir') ?? null;

        $sort_dir = strtoupper($sort_dir);
        if ($sort_dir != "ASC" AND $sort_dir != "DESC") {
            $sort_dir = "ASC";
        }

        $sort_type_query = "";
        if ($sort_type == "price") {
            $sort_type_query = "Shop_Product.base_price";
        } else if ($sort_type == "created") {
            $sort_type_query = "Shop_Product.created";
        } else if ($sort_type == "name") {
            $sort_type_query = "Product.product_name";
        } else if ($sort_type == "popular") {
            $sort_type_query = "Product.product_views)";
        } else {
            $sort_type_query = "Shop_Product.created";
        }
        $order_query = array("$sort_type_query $sort_dir");

        $category_names = [];

        $show_count = 3;
        $page = $this->request->data('page') ?? 1;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        if ($user_id == null OR $user_id == "0") {
            $user_ids_array = [];
        } else {
            $user_ids_array = ['Users.id' => $user_id];
        }

        if ($stopped_offers == null) {
            $stopped_offers_only_array = [];
        } else {

            $exclude = [];
            $stopped_offers_list = $this->Shop_Product->find("all",
                array(
                    'conditions' =>
                        array(
                            'parse_status' => 'stop'
                        ),
                )
            );
            foreach ($stopped_offers_list as $offer) {
                $id = $offer['Shop_Product']['product_id'];
                if (!in_array($id, $exclude)) {
                    $exclude[] = $id;
                }
            }

            $stopped_offers_only_array = [
                array(
                    'Product.id' => $exclude
                )
            ];
        }

        if ($active_only == null) {
            $active_only_array = [];
        } else {
            $active_only_array = ['Product.status' => 'active'];
        }

        // связь товара с магазином
        if ($product_id == null) {
            $product_id_array = [];
        } else {
            $product_id_array = ['Product.id' => $product_id];
        }

        if ($shop_id == null) {
            $shop_array = [];
        } else {
            $shop_array = ['Shop_Product.shop_id' => $shop_id];
        }

        // фильтр по категории
        if ($category_id == null) {
            $category_ids_array = [];
        } else {
            // список всех вложенных категорий
            $all_subcats_list = [$category_id];

            $this->findSubCats($category_id, $all_subcats_list);
            $category_ids_array = array('Product.category_id' => $all_subcats_list);
        }

        $product_counts = $this->Product->find('count', array(
                'conditions' =>
                    array(
                        //    'id' => $user_id
                        $stopped_offers_only_array,
                        $shop_array,
                        $category_ids_array,
                        $product_id_array
                    ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop_Product.product_id = Product.id'
                        )
                    ),
//                    array(
//                        'table' => 'product_categories',
//                        'alias' => 'ProductCategory',
//                        'type' => 'LEFT',
//                        'conditions' => array(
//                            'ProductCategory.id = Product.category_id'
//                        )
//                    ),
                ),
                'fields' => array(
                    'Product.*',
                    'Shop_Product.*',
                ),
            )
        );

        $pages = ceil($product_counts / $show_count);

        $popular_query_groupper = "";
        if ($sort_type == "popular") {
            $popular_query_groupper = "'group' => array(\"count(Product_View.product_id)\")";
        }

        $found_products = $this->Product->find("all",
            array('conditions' =>
                array(
                    //    'id' => $user_id
                    $stopped_offers_only_array,
                    $shop_array,
                    $category_ids_array,
                    $product_id_array

                ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop_Product.product_id = Product.id'
                        )
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'ProductCategory.id = Product.category_id'
                        )
                    ),
                ),
                'fields' => array(
                    'ProductCategory.*',
                    'Product.*',
                    'Shop_Product.*',
                    //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Map.created) AS active_time',
                ),

                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array("$sort_type_query" => "$sort_dir")
            )
        );


        foreach ($found_products as &$found_product) {
            // составление читабельных названий категорий товара
            $category_id = $found_product['ProductCategory']['id'];
            $id = $found_product['Product']['id'];
            $category_build_name = $this->ProductCom->buildProductNameByCategories($category_id);
            $system_barcode = $this->ProductCom->systemBarcode($id, $category_id);

            $product_offers = $this->Shop_Product->find("all",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $id
                        ),
                )
            );

            $product_offers_count = count($product_offers);

            $product_order_counter = $this->Order_Product->find("count",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $id
                        ),
                )
            );

            $found_product['category_build_name'] = $category_build_name['name'];
            $found_product['system_barcode'] = $system_barcode;
            $found_product['offer_count'] = $product_offers_count;
            $found_product['order_count'] = $product_order_counter;

            // изображения товара

            $product_image = $this->Product_Image->find("first",
                array(
                    'conditions' =>
                        array(
                            'product_id' => $id,
                            //'small !=' => ''
                        ),
                )
            );
            $found_product['image'] = count($product_image) > 0 ? $product_image['Image']['normal'] : null;
        }

        //$this->set('form_data', $form_data);
        $this->set('products', $found_products);
        $this->set('page', $page);
        $this->set('pages', $pages);

        $this->set('product_counts', $product_counts);
    }

    //поиск параметров категории товара
    public function params_search_ajax()
    {
        $category_id = $this->request->param('category_id');
        if ($category_id <= 0) {
            die("категория не найдена");
        }

        $complete_params = array();
        do {
            $params = $this->ProductCategoryParam->find('all', array(
                'conditions' => array(
                    'category_id' => $category_id,
                ),
                'order' => array(
                    'id' => 'ASC',
                )
            ));


            foreach ($params as $param) {
                $param_values_complete = [];
                // подгрузка предустановленных значений фильтра, при необходимости
                $param_id = $param['ProductCategoryParam']['id'];
                $param_type = $param['ProductCategoryParam']['param_type'];
                if ($param_type == "checkbox") {

                    $param_values = $this->ProductCategoryParamValues->find('all', array(
                        'conditions' => array(
                            'param_id' => $param_id,
                        ),
                        'order' => array(
                            'value' => 'ASC',
                        )
                    ));
                    if (count($param_values) > 0) {
                        foreach ($param_values as $param_value) {
                            $param_values_complete[] = array(
                                'id' => $param_value['ProductCategoryParamValues']['id'],
                                'value' => $param_value['ProductCategoryParamValues']['value']
                            );
                        }
                    }
                }
                $complete_params[] = array(
                    'param' => $param['ProductCategoryParam'],
                    'param_values' => $param_values_complete
                );
            }

            $parent_category = $this->ProductCategory->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $category_id
                        ),
                )
            );
            $category_id = $parent_category['ProductCategory']['parent_id'];
        } while ($category_id != 0);

        response_ajax($complete_params, 'success');
        exit;
    }
    */

}