<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ProductCategoryController extends AppController
{
    public $uses = array(
        'ProductCategory',
        'Category_Product_Param',
        'Category_Product_Param_Value',
        'Product',
        'Product_Param',
        'Product_Rating',
        'Category_Connect',
        'Product_Image',
        'Shop_Product',
        'Company',
        'Company_Agent',
    );

    public $components = array(
        'Cacher',
        'Session',
        'Breadcrumbs',
        'Flash',
        'ProductCom',
        'Uploader',
        'CompanyCom'
    );

    const max_size_number = 99999999;

    public $value_interval_list = array(
        '0.01', '0.05', '0.1', '0.5', '1', '2', '5', '10', '50', '100', '500', '1000'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add("Категории товаров", Router::url(array('plugin' => false, 'controller' => 'productCategory', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function category_filter()
    {
        $no_cache = $this->request->query('no_cache') ?? $this->request->data('no_cache');

        $category_id = $this->request->param('category_id') ?? null;

        $cached_route = "/v1/categories/$category_id/products";
        $this->Cacher->return_cached($cached_route, $this->request);

        $price_min = $this->request->query('price_min') ?? null;
        $price_max = $this->request->query('price_max') ?? null;
        if ($price_min == null OR intval($price_min) <= 0) {
            $price_min = 0;
        }

        if ($price_max == null OR intval($price_max) <= 1) {
            $price_max = 999999999;
        }

        $page = $this->request->query('page') ?? $this->request->data('page');
        $page = (intval($page) <= 0) ? 1 : intval($page);

        $payment_method = $this->request->query('payment_method') ?? null;
        $company_rating = $this->request->query('company_rating') ?? null;
        $delivery = $this->request->query('delivery') ?? null;

        $count = $this->request->query('count') ?? $this->request->data('count');
        $count = (intval($count) <= 0) ? 20 : intval($count);

        $sort_dir = $this->request->query('sort_dir') ?? null;
        $sort_type = $this->request->query('sort_field') ?? null;

        $filterator = [];

        $filterator["price_min"] = $price_min;
        $filterator["price_max"] = $price_max;
        $filterator["payment_method"] = $payment_method;
        $filterator["delivery"] = $delivery;
        $filterator["company_rating"] = $company_rating;
        $filterator["category_id"] = $category_id;
        $filterator["page"] = $page;

        $filter_array = [];
        $filtering_substring = "filter_";
        foreach ($this->request->query as $key => $value) {
            if (substr_count($key, $filtering_substring) > 0) {
                $filter_data = explode("_", $key);
                $filter_id = intval($filter_data[1]);
                $filter_values = $this->request->query($filtering_substring . $filter_id) ?? null;
                if ($filter_values != null) {
                    if (substr_count($filter_values, ",") == 0) {
                        $filter_array[$filter_id] = $this->request->query($filtering_substring . $filter_id);
                    } else {
                        $filter_values = str_replace("[", "", $filter_values);
                        $filter_values = str_replace("]", "", $filter_values);
                        $filter_values_list = explode(",", $filter_values);
                        $filter_array[$filter_id] = $filter_values_list;
                    }
                }

            }
        }

        $filterator["filters"] = $filter_array;
        $product_filter_search_hash = md5(serialize($filterator));
        // проверка, имеется ли закешированный результат поиска по данным фильтрам товаров, если имеется, отдаем его, если нет, то производим поиск заново
        if ($no_cache == null) {
            if ($this->Cacher->getValue($product_filter_search_hash) == false) {
                $products = $this->findProductByFilters($category_id, $sort_type, $sort_dir, $page, $count, $filterator);
                $this->Cacher->setValue($product_filter_search_hash, $products);

            }
            $get_filtered_products = $this->Cacher->getValue($product_filter_search_hash);
            if (empty($get_filtered_products)) {
                $get_filtered_products = $products;
            }
        } else {
            $get_filtered_products = $this->findProductByFilters($category_id, $sort_type, $sort_dir, $page, $count, $filterator);
        }

        $result = [
            "filter_array" => $filter_array,
            "pages" => $get_filtered_products['pages'],
            'total_items' => $get_filtered_products['total_count'],
            "products" => $get_filtered_products['products'],
        ];


        $this->Cacher->set_json_cache($cached_route, $result);
        response_api($result, "success");
        exit;
    }

    /**
     * @param $cat_id
     * @return array
     */
    public function allProductsInCategory($cat_id)
    {
        $products_ids_result = $this->Product->find("all",
            array(
                'conditions' =>
                    array(
                        'Product.category_id' => $cat_id,
                    ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Product.id = Shop_Product.product_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Product.id',
                ),
            )
        );
        $products_ids_result_list = [];
        foreach ($products_ids_result as $id_item) {
            $products_ids_result_list[] = $id_item['Product']['id'];
        }
        return $products_ids_result_list;
    }

    /**
     * @param $cat_id
     * @param $sort_type
     * @param $sort_dir
     * @param $filters
     * @return array
     */
    public function findProductsListByFilters($cat_id, $sort_type, $sort_dir, $filters)
    {
        $unique_product_ids = [];
        $all_products_in_cat = $this->allProductsInCategory($cat_id);
        if (count($filters["filters"]) > 0) {
            foreach ($filters["filters"] as $key => $val) {
                $found_filter_products = $this->Product_Param->find("all", array(
                        'conditions' =>
                            array(
                                'param_id' => $key,
                                'value' => $val,
                            ),
                        'fields' => array(
                            'DISTINCT Product_Param.product_id',
                        ),
                    )
                );
                foreach ($found_filter_products as $product_id_cur) {
                    $product_id_cur = $product_id_cur['Product_Param']['product_id'];
                    if (!in_array($product_id_cur, $unique_product_ids)) {
                        $unique_product_ids[] = $product_id_cur;
                    }
                }
            }
            $prepared_product_ids = array_intersect($all_products_in_cat, $unique_product_ids);
        } else {
            $prepared_product_ids = $all_products_in_cat;
        }

        if (count($prepared_product_ids) > 0) {
            $prod_ids_ar = ['Product.id IN' => $prepared_product_ids];
        } else {
            $prod_ids_ar = [];
        }

        if (key_exists("price_min", $filters)) {
            $price_min_ar = ['Shop_Product.base_price >=' => $filters['price_min']];
        } else {
            $price_min_ar = [];
        }

        if (key_exists("price_max", $filters)) {
            $price_max_ar = ['Shop_Product.base_price <=' => $filters['price_max']];
        } else {
            $price_max_ar = [];
        }

        if ($sort_type == null) {
            $sort_type = "price";
        }

        if ($sort_type == "price") {
            $order_arr = array('order' => 'Shop_Product.base_price ' . $sort_dir);
        } else if ($sort_type == "rating") {
            $order_arr = array('order' => 'Product.stat__current_rating ' . $sort_dir);
        }

        $products_ids_result = $this->Product->find("all",
            array(
                'conditions' =>
                    array(
                        'Product.category_id' => $cat_id,
                        $prod_ids_ar,
                        $price_min_ar,
                        $price_max_ar
                        // TODO ЗАГЛУШКА
                        //'status' => 'active'

                    ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Product.id = Shop_Product.product_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Product.id',
                ),
                $order_arr
            )
        );
        $products_ids_result_list = [];
        foreach ($products_ids_result as $id_item) {
            $products_ids_result_list[] = $id_item['Product']['id'];
        }
        return $products_ids_result_list;
    }

    /**
     * @param $cat_id
     * @param $sort_type
     * @param $sort_dir
     * @param $page
     * @param $show_count
     * @param $filters
     * @return array
     */
    public function findProductByFilters($cat_id, $sort_type, $sort_dir, $page, $show_count, $filters)
    {
        $product_ids_list = $this->findProductsListByFilters($cat_id, $sort_type, $sort_dir, $filters);

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil(count($product_ids_list) / $show_count);

        if ($sort_type == null) {
            $sort_type = "price";
        }
        if ($sort_type == "price") {
            $order_arr = array('order' => 'Shop_Product.base_price ' . $sort_dir);
        } else if ($sort_type == "rating") {
            $order_arr = array('order' => 'Product.stat__current_rating ' . $sort_dir);
        }

        $products = $this->Product->find("all",
            array(
                'conditions' =>
                    array(
                        'Product.id' => $product_ids_list,
                        'Shop_Product.status' => 'active'
                    ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Product.id = Shop_Product.product_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Shop_Product.base_price', 'Product.product_model', 'Product.prefix',
                    'Product.system_barcode',
                    'Product.id', 'Product.product_name', 'Product.product_name'
                ),
                $order_arr,
                'limit' => $show_count,
                'offset' => $limit_page,
            )
        );

        $products_complete = [];
        foreach ($products as $product) {
            $p_id = $product['Product']['id'];
            $category_params = $this->Product_Param->find("all",
                array(
                    'conditions' =>
                        array(
                            'Product_Param.product_id' => $p_id
                        ),
                    'joins' => array(
                        array(
                            'table' => 'category_product_params',
                            'alias' => 'Category_Product_Param',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Product_Param.param_id = Category_Product_Param.id'
                            )
                        ),
                        array(
                            'table' => 'category_product_param_values',
                            'alias' => 'Category_Product_Param_Value',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Product_Param.value = Category_Product_Param_Value.id'
                            )
                        ),
                    ),
                    'fields' => array(
                        'Category_Product_Param_Value.*',
                        'Category_Product_Param.*',
                        'Product_Param.*'
                    ),
                )
            );
            $spec_params = [];
            foreach ($category_params as $category_param) {
                $spec_params[] = array(
                    'name' => $category_param['Category_Product_Param']['param_name'],
                    'value' => $category_param['Category_Product_Param_Value']['value'],
                    'unit' => $category_param['Category_Product_Param']['unit'],
                    // TODO ЗАГЛУШКА добавить поле в базу данных
                    'is_important' => false
                );
            }
            $product_complete['id'] = $product['Product']['id'];
            $product_complete['name'] = $product['Product']['product_name'];
            $product_complete['specifications'] = $spec_params;
            $product_complete['rating'] = $product['Product']['rating'] ?? 0;
            $product_complete['product_model'] = $product['Product']['product_model'];
            $product_complete['prefix'] = $product['Product']['prefix'];
            $product_complete['barcode'] = $product['Product']['system_barcode'];

            // подгрузка изображений
            $images = $this->Product_Image->find("all",
                array(
                    'conditions' =>
                        array(
                            'Product_Image.product_id' => $p_id,
                        ),
                )
            );

            $image_types = [];
            foreach ($images as $item) {
                $image_hash = substr($item['Product_Image']['file'], 0, 8);
                if (empty($old_hash)) {
                    $old_hash = $image_hash;
                    $image_types[] = $item['Product_Image']['type'];
                    continue;
                }
                if ($image_hash == $old_hash) {
                    if (!in_array($item['Product_Image']['type'], $image_types)) {
                        $image_types[] = $item['Product_Image']['type'];
                    }
                } else {
                    $images_complete[] = array(
                        'alt' => '',
                        'hash' => $image_hash,
                        'types' => $image_types,
                        'image_id' => $item['Product_Image']['image_id'],
                    );
                    $old_hash = $image_hash;
                    $image_types = [];
                }
            }
            $images_complete[] = array(
                'alt' => '',
                'hash' => $image_hash,
                'types' => $image_types,
                'image_id' => $item['Product_Image']['image_id'],
            );


            $product_complete['images'] = $images_complete;
            //TODO заглушка HARDCODE
            $product_complete['is_favourite'] = false;
            $product_complete['in_cart'] = false;
            $product_complete['in_compare'] = false;

            // TODO companies HARDCODE
            /*
            привязка к региону Санкт-Петербург и область*/
            $offers_count = 10;
            $region_id = 1;
            $product_offers = $this->Product->find("all",
                array(
                    'conditions' =>
                        array(
                            'Product.id' => $p_id,
                            'Shop_Product.status' => 'active',
                            'Company.status' => 'active',
                            // TODO
                            //'Shop_Product.region_id' => $region_id
                        ),
                    'joins' => array(
                        array(
                            'table' => 'shop_products',
                            'alias' => 'Shop_Product',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Product.id = Shop_Product.product_id'
                            )
                        ),
                        array(
                            'table' => 'companies',
                            'alias' => 'Company',
                            'type' => 'INNER',
                            'conditions' => array(
                                'Company.id = Shop_Product.company_id'
                            )
                        ),
                    ),
                    'fields' => array(
                        'Shop_Product.base_price',
                        'Product.id',
                        'Company.id',
                        'Company.company_name',
                    ),
                    'order' => 'Shop_Product.base_price DESC',
                    'limit' => $offers_count,
                )
            );

            $product_offer_arr = [];
            foreach ($product_offers as $product_offer) {
                $base_price = $product_offer['Shop_Product']['base_price'];
                $company_id = $product_offer['Company']['id'];
                $company_name = $product_offer['Company']['company_name'];
                $product_offer_arr[] = array(
                    'id' => (int)$company_id,
                    'name' => $company_name,
                    'price' => (int)$base_price
                );
            }
            $product_complete['companies'] = $product_offer_arr;
            $products_complete[] = $product_complete;
        }

        return ['pages' => $pages, 'products' => $products_complete, 'total_count' => count($product_ids_list)];
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function getFiltersByCategoryId($category_id)
    {

        $category_params = $this->Category_Product_Param->find("all",
            array(
                'conditions' =>
                    array(
                        'Category_Product_Param.category_id' => $category_id
                    ),
                'joins' => array(
                    array(
                        'table' => 'category_product_param_values',
                        'alias' => 'Category_Product_Param_Value',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Category_Product_Param.id = Category_Product_Param_Value.param_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Category_Product_Param_Value.*',
                    'Category_Product_Param.*',
                ),
            )
        );

        $param_values = [];

        // добавление фильтра цены
        $minHintPrice = $this->findExtremumProductPriceInCategory($category_id, null, "min");
        $maxHintPrice = $this->findExtremumProductPriceInCategory($category_id, null, "max");

        $param_values[] = [
            'name' => 'price',
            'filterId' => "",
            'type' => 'number-range',
            'unit' => 'руб.',
            'isImportant' => 'true',
            'title' => 'Цена',
            'min' => 0,
            'minHint' => $minHintPrice,
            'maxHint' => $maxHintPrice
        ];

        //TODO HARDCODE
        $payment_types = [
            ['title' => 'Картой на сайте',
                'value' => 'card_online'],
            ['title' => 'Картой курьеру',
                'value' => 'card_delivery'],
            ['title' => 'Наличными курьеру',
                'value' => 'cash_delivery']
        ];
        $param_values[] = [
            'filterId' => "",
            'type' => 'select',
            'name' => 'pay-method',
            'unit' => '',
            'isImportant' => 'true',
            'title' => 'Способ оплаты',
            'items' => $payment_types
        ];

        $param_values[] = [
            'filterId' => "",
            'type' => "bool",
            'name' => 'manufacturer-warranty',
            'unit' => '',
            'isImportant' => 'false',
            'title' => 'Гарантия от производителя',
        ];

        foreach ($category_params as $category_param) {
            $param_id = $category_param['Category_Product_Param']['id'];
            $unit = $category_param['Category_Product_Param']['unit'];
            $param_name = $category_param['Category_Product_Param']['param_name'];
            $is_important = $category_param['Category_Product_Param']['is_important'];
            $param_value_items = $this->Category_Product_Param_Value->find("all",
                array(
                    'conditions' =>
                        array(
                            'param_id' => $param_id
                        )
                )
            );
            $p_values = [];
            foreach ($param_value_items as $param_value_item) {
                $param_value_item = $param_value_item['Category_Product_Param_Value'];
                $v_id = $param_value_item['id'];
                $v_value = $param_value_item['value'];
                $p_values[] = [
                    'id' => $v_id,
                    'value' => $v_value,
                ];
            }
            $new_param_arr = [
                'filterId' => $param_id,
                'type' => 'check',
                'unit' => $unit,
                'items' => $p_values,
                'isImportant' => $is_important,
                'title' => $param_name
            ];
            if (!in_array($new_param_arr, $param_values)) {
                $param_values[] = $new_param_arr;
            }
        }

        return $param_values;
    }

    public function findExtremumProductPriceInCategory($category_id, $region_id, $type)
    {
        //TODO HARDCODE
        if ($region_id == null) {
            $region_id = 1;
        }
        $sort_dir = "DESC";
        if ($type == "min") {
            $sort_dir = "ASC";
        } else if ($type == "max") {
            $sort_dir = "DESC";
        }

        $all_products_in_cat = $this->allProductsInCategory($category_id);

        if (count($all_products_in_cat) > 0) {
            $prod_ids_ar = ['Product.id IN' => $all_products_in_cat];
        } else {
            $prod_ids_ar = [];
        }

        $products_extremum_price = $this->Product->find("first",
            array(
                'conditions' =>
                    array(
                        'Product.category_id' => $category_id,
                        //'Shop_Product.region_id' => $region_id,
                        $prod_ids_ar,
                        // TODO ЗАГЛУШКА
                        //'status' => 'active'
                    ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Product.id = Shop_Product.product_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Shop_Product.base_price',
                    'Shop_Product.product_id',
                ),
                'order' => 'base_price ' . $sort_dir
            )
        );

        if (count($products_extremum_price) > 0) {
            return $products_extremum_price['Shop_Product']['base_price'];
        } else {
            return 0;
        }
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public function getCategoryInfoById()
    {
        $category_id = $this->request->param('category_id') ?? null;
        $with_filters = $this->request->query('with-filters') == "false" ? false : true;
        if (intval($category_id) == 0) {
            response_api(["error" => " category with id $category_id not found "], "error");
            exit;
        }

        $cached_route = "/v1/categories/$category_id";
        $this->Cacher->return_cached($cached_route, $this->request);

        $category_data = $this->ProductCategory->find("first",
            array(
                'conditions' =>
                    array(
                        'id' => $category_id
                    )
            )
        );
        if (count($category_data) == 0) {
            response_api(["error" => " category with id $category_id not found "], "error");
            exit;
        }

        $master_subcats = $this->ProductCategory->find("all",
            array(
                'conditions' =>
                    array(
                        'parent_id' => $category_id
                    ),
            )
        );

        $subcat_arr = [];
        foreach ($master_subcats as $master_subcat) {
            $master_subcat = $master_subcat['ProductCategory'];
            $subcat_arr[] = [
                'id' => $master_subcat['id'],
                'name' => $master_subcat['name'],
                'picture_url' => $master_subcat['image']
            ];
        }

        // определение древа родительских категорий
        $parent_cat_data_arr = [];
        $cur_category_id = $category_id;
        while ($cur_category_id > 0) {
            $cur_category_id = $this->ProductCom->getParentCategory($cur_category_id);
            if ($cur_category_id == 0) break;

            $level_category_data = $this->ProductCategory->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $cur_category_id
                        ),
                )
            );
            $level_category = $level_category_data['ProductCategory'];
            if (count($level_category) > 0) {

                // получение подкатегорий
                $subcats = $this->ProductCategory->find("all",
                    array(
                        'conditions' =>
                            array(
                                'parent_id' => $cur_category_id
                            ),
                    )
                );
                $subcat_data_list = [];
                foreach ($subcats as $subcat) {
                    $subcat = $subcat['ProductCategory'];
                    $subcat_data_list[] = [
                        'id' => $subcat['id'],
                        'name' => $subcat['name'],
                        'picture_url' => $subcat['image']
                    ];
                }
            }

            $parent_cat_data_arr[] = [
                'id' => $level_category['id'],
                'name' => $level_category['name'],
                'parent_id' => $level_category['parent_id'],
                'picture_url' => $level_category['image'],
                'sub_categories' => $subcat_data_list
            ];
        }

        $category_data = $category_data['ProductCategory'];
        $result = [
            'id' => $category_id,
            'parent_id' => $category_data['parent_id'],
            'title' => $category_data['name'],
            'picture_url' => $category_data['image'],
            'sub_categories' => $subcat_arr,
            'parent_categories' => $parent_cat_data_arr,
        ];

        if (count($master_subcats) == 0) {
            if($with_filters == true){
                $result['filters'] = $this->getFiltersByCategoryId($category_id);
            } else {
                $result['filters'] = [];
            }
        }

        // сохранение кеша в случае его изменения
        $this->Cacher->set_json_cache($cached_route, $result);
        response_api($result, "success");
        exit;
    }

    public function category_list()
    {
        $parents_arr = [];
        $parents = $this->ProductCategory->find("all",
            array(
                'conditions' =>
                    array(
                        'parent_id' => 0,
                        'status' => 'active'
                    )
            )
        );
        foreach ($parents as $parent) {
            $id = $parent['ProductCategory']['id'];
            $title = $parent['ProductCategory']['name'];

            $sub_parents = $this->ProductCategory->find("all",
                array(
                    'conditions' =>
                        array(
                            'parent_id' => $id,
                            'status' => 'active'
                        )
                )
            );

            $sub_parents_arr = [];
            if (count($sub_parents) > 0) {
                foreach ($sub_parents as $sub_parent) {

                    $sub_id = $sub_parent['ProductCategory']['id'];
                    $sub_title = $sub_parent['ProductCategory']['name'];
                    $sub_sub_parents = $this->ProductCategory->find("all",
                        array(
                            'conditions' =>
                                array(
                                    'parent_id' => $sub_id,
                                    'status' => 'active'
                                )
                        )
                    );

                    $sub_sub_cats = [];
                    if (count($sub_sub_parents) > 0) {
                        foreach ($sub_sub_parents as $sub_sub_parent) {
                            $sub_sub_id = $sub_sub_parent['ProductCategory']['id'];
                            $sub_sub_title = $sub_sub_parent['ProductCategory']['name'];
                            $sub_sub_cats[] = array(
                                'id' => $sub_sub_id,
                                'name' => $sub_sub_title,
                            );
                        }
                    }
                    $sub_parents_arr[] = array(
                        'id' => $sub_id,
                        'name' => $sub_title,
                        'sub_categories' => $sub_sub_cats
                    );
                }
            }
            $parents_arr[] = array(
                'id' => $id,
                'name' => $title,
                'sub_categories' => $sub_parents_arr

            );
        }
        response_api($parents_arr, "success");
        exit;
    }

    public function also_needed()
    {
        $category_id = $this->request->param('category_id') ?? null;
        if (intval($category_id) == 0) {
            response_api(["error" => " category with id $category_id not found "], "error");
            exit;
        }
        $cached_route = "/v1/categories/also-needed/$category_id";
        $this->Cacher->return_cached($cached_route, $this->request);

        $also_needed = $this->Category_Connect->find("all",
            array(
                'conditions' =>
                    array(
                        'Category_Connect.category_id' => $category_id
                    ),
                'joins' => array(
                    array(
                        'table' => 'product_categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Category.id = Category_Connect.category_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Category.id',
                    'Category.name',
                    'Category.image',
                ),
            )
        );
        $also_needed_arr =[];
        foreach ($also_needed as $also_needed_item){
            $also_needed_item = $also_needed_item['Category'];
            $also_needed_arr[] = [
                'id' => $also_needed_item['id'],
                'title' => $also_needed_item['name'],
                'picture_url' => $also_needed_item['image'],
            ];
        }

        // сохранение кеша в случае его изменения
        $this->Cacher->set_json_cache($cached_route, $also_needed_arr);
        response_api($also_needed_arr, "success");
        exit;
    }

    public function popular_categories()
    {
        $category_id = $this->request->param('category_id') ?? null;
        if (intval($category_id) == 0) {
            response_api(["error" => " category with id $category_id not found "], "error");
            exit;
        }
        $cached_route = "/v1/categories/also-needed/$category_id";
        $this->Cacher->return_cached($cached_route, $this->request);

        $also_needed = $this->Category_Connect->find("all",
            array(
                'conditions' =>
                    array(
                        'Category_Connect.category_id' => $category_id
                    ),
                'joins' => array(
                    array(
                        'table' => 'product_categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Category.id = Category_Connect.category_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Category.id',
                    'Category.name',
                    'Category.image',
                ),
            )
        );
        $also_needed_arr =[];
        foreach ($also_needed as $also_needed_item){
            $also_needed_item = $also_needed_item['Category'];
            $also_needed_arr[] = [
                'id' => $also_needed_item['id'],
                'title' => $also_needed_item['name'],
                'picture_url' => $also_needed_item['image'],
            ];
        }

        // сохранение кеша в случае его изменения
        $this->Cacher->set_json_cache($cached_route, $also_needed_arr);
        response_api($also_needed_arr, "success");
        exit;
    }


}