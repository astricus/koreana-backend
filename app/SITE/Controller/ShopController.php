<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ShopController extends AppController
{
    public $uses = array(
        'Country',
        'Shop_Product',
        'Shop',
        'City',
        'Region',
        'Company'
    );

    public $components = array(
        'Activity',
        'Breadcrumbs',
        'CityCom',
        'CompanyCom',
        'Flash',
        'ProductCom',
        'Session',
        'Uploader',
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add("Управление магазинами", Router::url(array('plugin' => false, 'controller' => 'shop', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function index()
    {
        $shops = $this->Shop->find("all",
            array('conditions' =>
                array(
                    'shop_owner_id' => $this->ShopOwnerCom->shop_owner_id(),
                ),
                'joins' => array(
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop.city_id = City.id'
                        )
                    ),
                ),
                'fields' => array(
                    'City.*',
                    'Shop.*'
                ),
                'order' => array('Shop.id DESC')
            )
        );
        $this->set('shops', $shops);

        foreach ($shops as &$shop) {
            $shop_id = $shop['Shop']['id'];
            $shop_products = $this->Shop_Product->find("count",
                array('conditions' =>
                    array(
                        'shop_id' => $shop_id,
                    ),
                )
            );
            $shop['product_count'] = $shop_products;
        }
        $this->set('shops', $shops);

        $this->set('title', "Управление магазинами");
        $this->set('content_title', "Управление магазинами");
    }

    /*
     *
     * {
    id: 2,
    name: 'Петрович',
    price: 1500,
    url: '',
    rating: 5,
    commentsCount: 101,
    delivery: {
        transportCompany: {price: 500, date: 1},
    },
    phone: '+78123348888',
    shops: [
        {
            id: 4,
            name: 'Петрович на Мурманском',
            address: 'Санкт-Петербург, Мурманское шоссе 12-13 км',
            coordinates: [59.8868678, 30.502329099999997],
            operatingMode: [[0, 0], [24, 0]],
        },
        {
            id: 5,
            name: 'Петрович на Софийской',
            address: 'Санкт-Петербург, ул. Софийская, д.59, корп.2, стр.1',
            coordinates: [59.860732, 30.418533900000057],
            operatingMode: [[0, 0], [24, 0]],
        },
        {
            id: 6,
            name: 'Петрович на Индустриальном',
            address: 'Санкт-Петербург, Индустриальный пр., 73',
            coordinates: [59.9799196, 30.45980110000005],
            operatingMode: [[0, 0], [24, 0]],
        },
    ],
}
     * */

    public function view()
    {
        $id = $this->request->param('company_id');
        if (intval($id) == 0) {
            response_api(["error" => " company with id $id not found "], "error");
            exit;
        }
        $company = $this->Company->find("first",
            array('conditions' =>
                array(
                    'id' => $id,
                ),
            )
        );
        if (count($company) == 0) {
            response_api(["error" => " company with id $id not found "], "error");
            exit;
        }

        $company_shops = $this->CompanyCom->shop_list_full($id);

        $result['id'] = $id;
        $result['name'] = $company['Company']['company_name'];
        $result['phone'] = $company['Company']['phone_number'];
        $result['city'] = $this->CityCom->getCityNameById($company['Company']['city_id']);
        $result['rating'] = $company['0']['stat__current_rating'];

        $shops = [];
        if(count($company_shops)==0) {
            foreach ($company_shops as &$shop) {
                $city_id = $shop['Shop']['city_id'];
                if ($city_id > 0) {
                    $city = $this->CityCom->getCityNameById($shop['Shop']['city_id']);
                } else {
                    $city = " не определен";
                }
                $shop = [
                    'id' => $shop['Shop']['id'],
                    'name' => $shop['Shop']['shop_name'],
                    'address' => $shop['Shop']['address'],
                    'coordinates' => [$shop['Shop']['shop_latitude'], $shop['Shop']['shop_longitude']],
                    'city' => $city,
                    'phone' => $shop['Shop']['phone'],
                    'operatingMode' => [$shop['Shop']['shop_open'], $shop['Shop']['shop_close']],
                    'status' => $shop['Shop']['status'],
                ];
                $shops[] = $shop;
            }
        } else {
            $shops = null;
        }
        $result['shops'] = $shops;
        response_api($result, "success");
        exit;
    }

    //поиск параметров категории товара
    public function params_search_ajax()
    {
        $category_id = $this->request->param('category_id');
        if ($category_id <= 0) {
            die("категория не найдена");
        }

        $complete_params = array();
        do {
            $params = $this->ProductCategoryParam->find('all', array(
                'conditions' => array(
                    'category_id' => $category_id,
                ),
                'order' => array(
                    'id' => 'ASC',
                )
            ));


            foreach ($params as $param) {
                $param_values_complete = [];
                // подгрузка предустановленных значений фильтра, при необходимости
                $param_id = $param['ProductCategoryParam']['id'];
                $param_type = $param['ProductCategoryParam']['param_type'];
                if ($param_type == "checkbox") {

                    $param_values = $this->ProductCategoryParamValues->find('all', array(
                        'conditions' => array(
                            'param_id' => $param_id,
                        ),
                        'order' => array(
                            'value' => 'ASC',
                        )
                    ));
                    if (count($param_values) > 0) {
                        foreach ($param_values as $param_value) {
                            $param_values_complete[] = array(
                                'id' => $param_value['ProductCategoryParamValues']['id'],
                                'value' => $param_value['ProductCategoryParamValues']['value']
                            );
                        }
                    }
                }
                $complete_params[] = array(
                    'param' => $param['ProductCategoryParam'],
                    'param_values' => $param_values_complete
                );
            }

            $parent_category = $this->ProductCategory->find("first",
                array(
                    'conditions' =>
                        array(
                            'id' => $category_id
                        ),
                )
            );
            $category_id = $parent_category['ProductCategory']['parent_id'];
        } while ($category_id != 0);

        response_ajax($complete_params, 'success');
        exit;
    }
}