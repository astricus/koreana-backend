<div class="row">
    <script>
        $(document).on('nifty.ready', function () {

            $(".datepicker").datepicker();

        })</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Добавление плановой задачи</h3>
        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/task/list">Список фоновых задач</a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_task" method="post" action="/task/add" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="task_name">Название задачи</label>
                    <div class="col-sm-6">
                        <input type="text" id=task_name class="form-control" name="name" required placeholder="Название задачи" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="task_code">Кодовое обозначение</label>
                    <div class="col-sm-6">
                        <input type="text" id="task_code" class="form-control" name="code" required placeholder="Кодовое обозначение" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="args">Входящие данные</label>
                    <div class="col-sm-6">
                        <input id="args" class="form-control" name="args" placeholder="Входящие данные" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="start_now">Запустить немедленно</p>
                    </label>
                    <div class="col-sm-6">
                        <input type="checkbox" name="start_now" id="start_now"/>
                    </div>
                </div>

                <div class="form-group" id="start_date">
                    <label class="col-lg-3 control-label" for="start_date">Дата запуска задачи
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате гггг-мм-дд </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="start_date" id="datepicker"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="start_hour">Час запуска задачи</p>
                    </label>
                    <div class="col-sm-6">
                        <select id="start_hour" class="use_select2" name="start_hour" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? for ($hour = 0; $hour<=23; $hour++) {?>
                                <option value="<?= sprintf('%02d', $hour); ?>"><?= sprintf('%02d', $hour) ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="start_minute">Минуты запуска задачи</p>
                    </label>
                    <div class="col-sm-6">
                        <select id="start_minute" class="use_select2" name="start_minute" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? for ($min = 0; $min<=59; $min++) {?>
                                <option value="<?= sprintf('%02d', $min); ?>"><?= sprintf('%02d', $min) ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>