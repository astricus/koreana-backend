<script>
    $(document).ready(function () {
        $(".datepicker").datepicker({
                dateFormat: "yy-mm-dd"
        });

        /* Результат запуска задачи  */
        $(document).on("change", ".filter_result_status", function () {
            let filter_result_status = $('.filter_result_status option:selected').val(), new_url;
            if (filter_result_status  !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'result_status', filter_result_status);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'result_status', '');
            }
            redirect(new_url);
        });

        /* Статус задачи */
        $(document).on("change", ".filter_status", function () {
            let filter_status = $('.filter_status option:selected').val(), new_url;
            if (filter_status !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'status', filter_status);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'status', '');
            }
            redirect(new_url);
        });

        /* Дата запуска */
        $(document).on("change", ".start_date", function () {
            let start_date = $('.start_date').val(), new_url;
            if (start_date !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'start_date', start_date);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'start_date', '');
            }
            redirect(new_url);
        });

        $(document).on('click', ".delete_task", function (e) {
            if (confirm('Вы уверены, что хотите удалить данную задачу?')) {
                var link = $(this).attr('href');
                window.location.href = link;
            }
            e.preventDefault();
            return false;
        });

        $(document).on("click", ".click_modal", function (event) {
            $(".modal-body").html('');
            let payload = $(this).attr('data-payload');
            if (payload.length > 0) {
                $(".modal-body").html(payload);
            }
        });
    });
</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список фоновых задач</h3>
        </div>
        <div class="panel-body">

            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/task/add/"><i class="fa fa-plus"></i>
                            Добавить плановую задачу</a>

                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/task/list" class="btn btn-success">показать все задачи</a>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="filter_status" class="main_select use_select2 filter_status" name="status">
                            <option value="">Все задачи по статусу</option>
                            <?
                            foreach ($valid_statuses as $k => $v){?>
                               <option <? if($k==$form_data['status']) echo 'selected';?> value="<?=$k?>"><?=$v?></option>
                           <? }?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="filter_result_status" class="main_select use_select2 filter_result_status" name="filter_result_status">
                            <option value="">Все задачи по результату</option>
                            <?
                            foreach ($valid_result_statuses as $k => $v){?>
                                <option <? if($k==$form_data['result_status']) echo 'selected';?> value="<?=$k?>"><?=$v?></option>
                            <? }?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left">
                        <label class="control-label" for="start_date">Дата запуска задачи</label>
                        <input class="start_date datepicker" name="start_date" value="<?=$form_data['start_date']?>"/>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($task_list) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Код задачи</th>
                            <th>Входящие данные</th>
                            <th>Длилась</th>
                            <th>Статус</th>
                            <th>Результат выполнения</th>
                            <th>Задача создана</th>
                            <th>Дата/время запуска</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        foreach ($task_list as $task_item) {
                            $n++;
                            $task = $task_item['Task'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/task/view/<?= $task['id'] ?>"><?= $task['name'] ?></a></td>
                                <td><p><?= $task['code'] ?></p></td>
                                <td><p><?= $task['args'] ?></p></td>

                                <td>
                                    <? if($task['status']!=="wait") {?>
                                    <?= $task['timer'] ?> сек.
                                    <?}?>
                                </td>
                                <td>
                                    <? if($task['status']=="wait") {?>
                                        <button class="btn btn-icon btn-default" title="ожидает запуска"><i class="fa fa-spinner"></i></button>
                                    <?} else if($task['status']=="active") {?>
                                        <button class="btn btn-icon btn-warning " title="запущена"><i class="fa fa-arrow-circle-right"></i></button>
                                    <?} else if($task['status']=="finished") {?>
                                        <button class="btn btn-icon  btn-success " title="завершена"><i class="fa fa-check-square"></i></button>
                                    <?} else {?>
                                        <button class="btn btn-icon btn-danger " title="неизвестный статус"><i class="fa fa-question"></i></button>
                                    <?}?>
                                </td>
                                <td>
                                    <? if($task['result_status']=='failure') {?>
                                        <button class="btn btn-danger click_modal"
                                                data-payload="<?=$task['result_payload']?>"
                                                data-target="#modal-wo-anim" data-toggle="modal">Ошибка</button>

                                    <?} if($task['result_status']=='unknown') {?>

                                    <?} if($task['result_status']=='success') {?>
                                        <button data-payload="<?=$task['result_payload']?>" class="click_modal btn btn-success" data-target="#modal-wo-anim" data-toggle="modal">Успешно завершена</button>
                                    <?}?>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($task['created']) ?>
                                    </span>
                                </td>
                                <td><span class="">
                                        <?
                                        $str_datetime = $task['start_date'] . " ". sprintf('%02d', $task['start_hour'])
                                            .":". sprintf('%02d',$task['start_minute']) .":00";
                                        if($task['start_date']=="0000-00-00"){
                                            echo $str_datetime;
                                        }
                                        else {
                                            echo lang_calendar($str_datetime);
                                        }
                                        ?>
                                    </span>
                                </td>
                                <td>
                                    <div class="btn-group">
                                       <? if($task['status']=="wait") {?>
                                    <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                       href="/task/edit/<?= $task['id'] ?>"></a>
                                        <?}?>
                                    </div>
                                    <a class="btn btn-sm btn-warning btn-hover-info fa fa-trash delete_task"
                                       href="/task/delete/<?= $task['id'] ?>"></a>
                                </td>

                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/task/list";
                            $params = array(
                                'start_dat' => $form_data['start_date'],
                                'result_status' => $form_data['result_status'],
                                'status' => $form_data['status'],
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Фоновых задач нет</p>
            <? } ?>

        </div>
    </div>
</div>

<div class="modal" id="modal-wo-anim" role="dialog" tabindex="-1" aria-labelledby="default-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title">Результат запуска плановой задачи</h4>
            </div>


            <!--Modal body-->
            <div class="modal-body">

            </div>


            <!--Modal footer-->
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Закрыть</button>
            </div>
        </div>
    </div>
</div>