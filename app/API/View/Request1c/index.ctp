<div class="row">
    <script>
        $(document).on('nifty.ready', function () {

            $(".datepicker").datepicker();

            $(document).on("change", ".get_car_numbers_by_id", function () {
                let optionSelectedText = $(".get_car_numbers_by_id option:selected").text();
                let optionSelectedVal = $(".get_car_numbers_by_id option:selected").val();
                let user_id = optionSelectedVal;
                let phone = optionSelectedText.trim().substr(0, 11);

                $("#car_number").html('');
                $(".phone").val(phone);
                $(".user_id").val(user_id);

                let send_data = {};
                send_data['user_id'] = user_id;

                $.ajax({
                    url: "/request1c/get_user_cars/",
                    type: "post",
                    dataType: 'json',
                    data: send_data,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        if (data.data.status === "error") {
                        } else {
                            for (var i = 0; i < data.data.cars.length; i++) {
                                let car = data.data.cars[i];
                                let car_item = $("<option value='" + car + "'>" + car + "</option>");
                                $("#car_number").append(car_item);
                            }
                        }
                    }
                });
            });

        });</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Получение списка заказов в 1с</h3>
        </div>

        <form class="form-horizontal" name="order_list" method="post" action="/request1c">
            <input type="hidden" name="operation" required value="order_list"/>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="date_start">Дата от </label>
                    <div class="col-sm-3">
                        <input type="text" id=date_start class="form-control datepicker" name="date_start" required
                               value="<?= $date_start ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="date_finish">Дата до </label>
                    <div class="col-sm-3">
                        <input type="text" id=date_finish class="form-control datepicker" name="date_finish" required
                               value="<?= $date_finish ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="user_id">Клиент</label>
                    <div class="col-sm-6">
                        <select id="user_id" class="use_select2 get_car_numbers_by_id" name="user_id"
                                style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($all_users as $all_user) { ?>
                                <option value="<?= $all_user['User']['id'] ?>"
                                    <? if ($user_id == $all_user['User']['id']) echo 'selected="true"'; ?>>
                                    <?= $all_user['User']['firstname'] ?>
                                    <?= $all_user['User']['lastname'] ?>
                                    <?= $all_user['User']['phone'] ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="car_number">Номер авто</label>
                    <div class="col-sm-6">
                        <select id="car_number" class="use_select2 car_number" name="car_number" style="width: 300px">
                            <? if ($car_number == null) { ?>
                                <option value="0">Не выбрано</option>
                            <? } else { ?>
                                <option value="<?= $car_number ?>"><?= $car_number ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit" type="submit"
                               value="Сделать запрос списка заказов в 1с"/>
                    </div>
                </div>
            </div>
            <? if (!empty($request_result) && $operation == "order_list") { ?>
                <div class="alert alert-info">
                    <? var_dump($request_result); ?>
                </div>
            <? } ?>
        </form>

    </div>

    <hr>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Получение информации по заказу в 1с</h3>
        </div>

        <form class="form-horizontal" name="order_info" method="post" action="/request1c">
            <input type="hidden" name="operation" required value="order_info"/>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="user_id">Клиент</label>
                    <div class="col-sm-6">
                        <select id="user_id" class="use_select2" name="user_id" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <?
                            $phone_selected_number = null;
                            $user_selected_id = null;
                            foreach ($all_users as $all_user) { ?>
                                <option
                                    <? if ($user_id == $all_user['User']['id']) {
                                        $phone_selected_number = $all_user['User']['phone'];
                                        $user_selected_id = $all_user['User']['id'];
                                        echo 'selected="true"';
                                    } ?>
                                        value="<?= $all_user['User']['id'] ?>">
                                    <?= $all_user['User']['firstname'] ?>
                                    <?= $all_user['User']['lastname'] ?>
                                    <?= $all_user['User']['phone'] ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="phone">Номер телефона (через 7)</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control phone" name="phone" required disabled
                               value="<?= $phone_selected_number ?>"/>
                    </div>

                    <label class="col-lg-3 control-label" for="id">#id клиента</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control user_id" name="id" required disabled
                               value="<?= $user_selected_id ?>"/>
                    </div>

                </div>

                <div class="form-group">

                    <label class="col-lg-3 control-label" for="car_number">Номер авто</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control car_number" name="car_number" required
                               value="<?= $car_number ?>"/>
                    </div>

                    <label class="col-lg-3 control-label" for="order_id">#id заказа в 1с</label>
                    <div class="col-sm-3">
                        <input type="text" id=order_id class="form-control" name="order_id" required
                               value="<?= $order_id ?>"/>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit" type="submit"
                               value="Сделать запрос заказ-наряда в 1с"/>
                    </div>
                </div>
            </div>


            <? if (!empty($request_result) && $operation == "order_info") { ?>
                <div class="alert alert-info">
                    <? var_dump($request_result); ?>
                </div>
            <? } ?>

        </form>

    </div>
    <hr>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Получение информации по последнему заказ-наряду в 1с</h3>
        </div>

        <form class="form-horizontal" name="add_task" method="post" action="/request1c">
            <input type="hidden" name="operation" required value="last_order"/>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="user_id">Клиент</label>
                    <div class="col-sm-6">
                        <select id="user_id" class="use_select2" name="user_id" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($all_users as $all_user) { ?>

                                <option <? if ($user_id == $all_user['User']['id']) {
                                    echo 'selected="true"';
                                } ?> value="<?= $all_user['User']['id'] ?>">
                                    <?= $all_user['User']['firstname'] ?>
                                    <?= $all_user['User']['lastname'] ?>
                                    <?= $all_user['User']['phone'] ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="phone">Номер телефона (через 7)</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control phone" name="phone" required disabled value="<?= $phone_selected_number ?>"/>
                    </div>

                    <label class="col-lg-1 control-label" for="id">#id клиента</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control user_id" name="id" required disabled value="<?= $user_selected_id ?>"/>
                    </div>

                    <label class="col-lg-1 control-label" for="car_number">Номер авто</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control car_number" name="car_number" required value="<?= $car_number?>"/>
                    </div>

                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit" type="submit"
                               value="Сделать запрос последнего заказ-наряда в 1с"/>
                    </div>
                </div>
            </div>

            <? if (!empty($request_result) && $operation == "last_order") { ?>
                <div class="alert alert-info">
                    <? var_dump($request_result); ?>
                </div>
            <? } ?>
        </form>

        <hr>

    </div>
</div>