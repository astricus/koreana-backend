<div class="row" xmlns="">
    <script>
        $(document).on('nifty.ready', function () {

            $('.summernote').summernote({
                'height': '230px',
            });

            setInterval(function () {
                $("#content_new").val($(".summernote").summernote('code'));
            }, 100);

            $('#save-text').on('click', function () {
                $('#summernote-edit').summernote('destroy');
            });

            $(".datepicker").datepicker();

            $(document).on('click',".delete_confirm", function (e) {
                if (confirm('Вы уверены, что невозможно хотите удалить данную акцию?')) {
                    var link = $(this).attr('href');
                    console.log(link);
                    window.location.href = link;
                }
                e.preventDefault();
                return false;
            });


        })</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Акция <i><?= $action['title'] ?> </i></h3>
        </div>
        <form class="form-horizontal" name="edit_action" method="post" action="/action/edit/<?= $action['id'] ?>" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="pad-btm form-inline">

                    <div class="row">
                        <div class="col-sm-6 table-toolbar-left">
                            <a class="btn btn-success btn-bock" href="/actions"><i class="fa fa-list"></i> Список акций</a>

                            <? if ($action['enabled'] == 1) { ?>
                                <a class="btn btn-warning btn-bock" href="/action/block/<?= $action['id'] ?>"><i class="fa fa-close"></i> Скрыть акцию из показа</a>
                            <? } else { ?>
                                <a class="btn btn-warning btn-bock" href="/action/unblock/<?= $action['id'] ?>"><i class="fa fa-eye"></i> Показывать акцию (сейчас скрыта)</a>

                            <? } ?>

                            <a class="btn btn-danger btn-bock delete_confirm" href="/action/delete/<?=$action['id']?>"><i class="fa fa-minus"></i> Удалить данную акцию</a>
                        </div>
                    </div>

                </div>

                Автор новости: <a class="btn-link" href="/manager/view/<?= $action['author_id'] ?>"><?= $action['author_name'] ?></a>

                <hr>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="new_title">Заглавие</label>
                    <div class="col-sm-6">
                        <input type="text" id=new_title class="form-control" name="title" required placeholder="Заглавие акции" value="<?= $action['title'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="new_preview">Анонс</label>
                    <div class="col-sm-6">
                        <textarea id="new_preview" class="form-control" name="preview"><?= $action['preview'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="new_image">Изображение</label>
                    <div class="col-sm-6">
                        <input type="file" id="image" class="form-control" name="image"/>
                        <a href="<?= $action['image_url'] ?>" target="_blank"><img alt="Иллюстрация акции" src="<?= $action['image_url'] ?>" style="max-width: 140px; max-height: 250px;"></a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="new_content">Содержание акции</label>
                    <div class="col-sm-6">
                        <div class="summernote"><?= $action['content'] ?></div>
                        <textarea name="content" id="content_new"
                                  style="visibility: hidden"><?= $action['content'] ?></textarea>
                    </div>
                </div>

                <div class="form-group" id="datetime_input">
                    <label class="col-lg-3 control-label" for="start_date">Дата запуска показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате гггг-мм-дд </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="start_date" id="datepicker" value="<?= only_date($action['start_datetime']) ?>"/>
                    </div>
                    <label class="col-lg-3 control-label" for="start_time">время запуска показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате чч:мм </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="time" name="start_time" id="start_time" value="<?= only_time($action['start_datetime']) ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="stop_date">Дата остановки показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате гггг-мм-дд </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="stop_date" value="<?=only_date($action['stop_datetime'])?>" class="datepicker"/>
                    </div>
                    <label class="col-lg-3 control-label" for="stop_time">время остановки показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате чч:мм </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="time" name="stop_time" id="stop_time" value="<?=only_time($action['stop_datetime'])?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="cities">Города показа</p></label>
                    <div class="col-sm-6">
                        <select id="cities" class="use_select2" multiple name="cities[]" style="width: 300px">
                            <option value="">Все города</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if (in_array($city_id, $action_cities)) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Платформа показа</p>
                    </label>
                    <div class="col-sm-6">
                        <select id="platform" class="use_select2" multiple name="platform[]" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($platforms as $platform) { ?>
                                <option value="<?= $platform?>" <? if (in_array($platform, $action_platforms)) echo 'selected'; ?>><?= $platform ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"></input>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>