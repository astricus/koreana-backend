<div class="row">
    <script>
        $(document).on('nifty.ready', function () {

            $('.summernote').summernote({
                'height': '230px',
            });

            setInterval(function () {
                $("#new_content").val($(".summernote").summernote('code'));
            }, 120);

            $('#save-text').on('click', function () {
                $('#summernote-edit').summernote('destroy');
            });

            $(".datepicker").datepicker();

            $(document).on("click", "#start_now", function () {
                if ($("#start_now").prop('checked')) {
                    $("#datetime_input").hide()
                } else {
                    $("#datetime_input").show()
                }
            });

        })</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Создание акции</h3>
        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/actions">Список акций </a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_action" method="post" action="/action/add" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="action_title">Заглавие</label>
                    <div class="col-sm-6">
                        <input type="text" id=action_title class="form-control" name="title" required placeholder="Заглавие акции" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="action_image">Изображение</label>
                    <div class="col-sm-6">
                        <input type="file" id="action_image" class="form-control" name="image"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="action_preview">Анонс</label>
                    <div class="col-sm-6">
                        <textarea id="action_preview" class="form-control" name="preview" placeholder="Анонс акции"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="new_content">Содержание новости</label>
                    <div class="col-sm-6">
                        <div class="summernote"></div>
                        <textarea name="content" id="new_content" style="visibility: hidden"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="start_now">Запустить немедленно</p>
                    </label>
                    <div class="col-sm-6">
                        <input type="checkbox" name="start_now" id="start_now"/>
                    </div>
                </div>

                <div class="form-group" id="datetime_input">
                    <label class="col-lg-3 control-label" for="start_date">Дата запуска показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате гггг-мм-дд </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="start_date" id="datepicker"/>
                    </div>
                    <label class="col-lg-3 control-label" for="start_time">время запуска показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате чч:мм </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="time" name="start_time" id="start_time" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="stop_date">Дата остановки показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате гггг-мм-дд </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="stop_date" class="datepicker"/>
                    </div>
                    <label class="col-lg-3 control-label" for="stop_time">время остановки показа
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате чч:мм </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="time" name="stop_time" id="stop_time" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Платформа показа</p>
                    </label>
                    <div class="col-sm-6">
                        <select id="platform" class="use_select2 dropdownPlatformSelect" multiple name="platform[]" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($platforms as $platform) { ?>
                                    <option value="<?= $platform; ?>"><?= $platform ?></option>
                                <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Города показа</p></label>
                    <div class="col-sm-6">
                        <select id="show_city" class="use_select2 dropdownPlatformSelect" multiple name="cities[]" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($cities as $city) { ?>
                                <option value="<?= $city['City']['id']; ?>"><?= $city['City']['name'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>


            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>