<script>
    $(document).ready(function () {
        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Платформа */
        $(document).on("change", ".platform_select", function () {
            let platform = $('.platform_select option:selected').val(), new_url;
            if (platform != 0) {
                new_url = updateQueryStringParameter(window.location.href, 'platform', platform);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'platform', '');
            }
            redirect(new_url);
        });

        /* Дата показа */
        $(document).on("change", "#start_datetime", function () {
            let start_datetime = $('#start_datetime').val(), new_url;
            if (start_datetime !="") {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', start_datetime);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', '');
            }
            redirect(new_url);
        });

    });

</script>
<?php


?>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список акций</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/action/add/"><i
                                    class="fa fa-plus"></i> Создать акцию</a>

                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/actions" class="btn btn-success">показать все акции</a>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="region_id" class="main_select use_select2 city_select" name="region_id">

                            <option value="0">Все регионы</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 platform_select" name="city_id">
                            <option value="0">Все платформы</option>
                            <? foreach ($platforms as $key => $platform) {
                                ?>
                                <option value="<?= $platform ?>" <? if ($platform == $form_data['platform']) echo 'selected'; ?>>
                                    <?= $platform ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left form-control">
                        <label class="control-label" for="start_datetime">Дата начала показа (начиная от)</label>
                        <input type="date" id="start_datetime" name="start_datetime"/>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($actions) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Краткий анонс акции</th>
                            <th>Изображение</th>
                            <th>Автор</th>
                            <th>Добавлена</th>
                            <th>Дата/время запуска</th>
                            <th>Дата/время окончания</th>
                            <th>Размещение акции</th>
                            <th>Регионы показа</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list[1] = array('success', 'активна');
                        $status_list[0] = array('danger', 'скрыта');

                        foreach ($actions as $action) {
                            $n++;
                            $action_data = $action['Action'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/action/view/<?= $action_data['id'] ?>"><?= $action_data['title'] ?></a>
                                </td>
                                <td><?= $action_data['preview'] ?></a></td>

                                <td>
                                    <? if (!empty($action_data['image'])) { ?>
                                        <a target="_blank" class="btn-link" href="<?= $content_dir ?>/<?= $action_data['image'] ?>">
                                            <img style="max-width: 140px; max-height: 250px;" src="<?= $content_dir ?>/<?= $action_data['image'] ?>"></a>
                                    <? } ?>
                                </td>

                                <td><a class="btn-link"
                                       href="/admin/view/<?= $action_data['author_id'] ?>"><?= $action['author_name'] ?></a>
                                </td>
                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($action_data['created']) ?>
                                </span>
                                </td>

                                <td>
                                    <span class="text-info"><?= lang_calendar($action_data['start_datetime']) ?></span>
                                </td>
                                <td>
                                    <span class="text-info"><?= lang_calendar($action_data['stop_datetime']) ?></span>
                                </td>

                                <td>
                                    <? foreach ($action['platforms'] as $platform) {
                                        ?><span class="text-info"><?= $platform ?>
                                        </span>
                                    <? } ?>
                                </td>
                                <td>
                                    <? foreach ($action['cities'] as $city) {
                                        $city_name = $city['City']['name'];
                                        ?><span class=""><?= $city_name ?>
                                        </span>
                                    <? } ?>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$action_data['enabled']][0] ?>"><?= $status_list[$action_data['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($action_data['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/action/unblock/<?= $action_data['id'] ?>"
                                               data-original-title="Открыть акцию" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/action/block/<?= $action_data['id'] ?>"
                                               data-original-title="Скрыть акцию" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/action/edit/<?= $action_data['id'] ?>"
                                           data-original-title="Изменить данные" data-container="body"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/actions/";
                            $params = array();
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>
                <p class="muted">Акций нет</p>
            <? } ?>

        </div>
    </div>
</div>