
<div class="panel-body">
    <div class="blog-title media-block">
        <div class="media-body">
                <h3><?=$action['title'];?></h3>

            <p>автор <a href="#" class="btn-link"><?=$action['author_name'];?> </a></p>
        </div>
    </div>
    <div class="blog-content">
        <div class="blog-body">
            <?=$action['content'];?>
        </div>
    </div>


    <div class="blog-footer">
        <div class="media-left">
            <span class="label label-success"><?=$action['days_later']?></span>
            <small>автор: <a href="#" class="btn-link"><?=$action['author_name'];?></a></small>
        </div>
    </div>
</div>