<!DOCTYPE html>
<html lang="en">
<head>

    <?= $this->Html->charset() ?>
    <title>
        <?php echo $title; ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?= site_url(); ?>">

    <?= $this->Html->meta('icon') ?>

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <? //$this->Html->css('base.css') ?>
    <?= $this->Html->css('fa.all.min') ?>

    <!--CSS Loaders [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/css-loaders/css/css-loaders') ?>

    <!--Animate.css [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/animate-css/animate.min.css') ?>

    <!--Switchery [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/switchery/switchery.min.css') ?>

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('bootstrap.min.css') ?>

    <?= $this->Html->css('premium-line-icons.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('nifty.min.css') ?>
    <!--Nifty Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('nifty.icons.min.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <? //= $this->Html->css('nifty.css') ?>

    <!--Unite Gallery [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/unitegallery/css/unitegallery.min.css') ?>

    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>

    <?= $this->Html->script('../plugins/pace/pace.min.js') ?>

    <!--Demo [ DEMONSTRATION ]-->

    <?= $this->Html->css('../plugins/switchery/switchery.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-select/bootstrap-select.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.css') ?>

    <?= $this->Html->css('../plugins/chosen/chosen.min.css') ?>

    <?= $this->Html->css('../plugins/noUiSlider/nouislider.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') ?>

    <?= $this->Html->css('../plugins/ionicons/css/ionicons.min.css') ?>

    <?= $this->Html->css('../plugins/select2/css/select2.css') ?>
    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->css('../plugins/morris-js/morris.min.css') ?>
    <?= $this->Html->css('../plugins/summernote/summernote.min.css') ?>

    <?= $this->Html->css('../plugins/bootstrap-validator/bootstrapValidator.min.css') ?>
    <?= $this->Html->css('admin.css') ?>
    <?= $this->Html->css('chat.css') ?>

    <!--jQuery [ REQUIRED ]-->
    <?= $this->Html->script('jquery.min.js') ?>

    <!--BootstrapJS [ RECOMMENDED ]-->
    <?= $this->Html->script('bootstrap.min.js') ?>

    <!--NiftyJS [ RECOMMENDED ]-->
    <?= $this->Html->script('nifty.min.js') ?>

    <!--Flot Chart [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.resize.min.js') ?>
    <?= $this->Html->script('../plugins/flot-charts/jquery.flot.tooltip.min.js') ?>

    <?= $this->Html->script('../plugins/select2/js/select2.min.js') ?>

    <?= $this->Html->script('../plugins/masked-input/jquery.maskedinput.js') ?>

    <!--Sparkline [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/sparkline/jquery.sparkline.min.js') ?>

    <?= $this->Html->script('../plugins/bootstrap-datepicker/bootstrap-datepicker.js') ?>

    <?= $this->Html->script('../plugins/chosen/chosen.jquery.js') ?>

    <?= $this->Html->script('../plugins/unitegallery/js/unitegallery.min') ?>
    <?= $this->Html->script('../plugins/unitegallery/themes/tilesgrid/ug-theme-tilesgrid') ?>

    <!--Switchery [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/switchery/switchery.min.js') ?>

    <!--Bootbox Modals [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/bootbox/bootbox.min.js') ?>
    <?= $this->Html->script('../plugins/summernote/summernote.min.js') ?>
    <!--Modals [ SAMPLE ]-->
    <? /*
    <script src="js/demo/ui-modals.js"></script>
 */ ?>
    <?= $this->Html->script('notify.js') ?>
    <?= $this->Html->script('index.js') ?>
    <?= $this->Html->script('super_slider.js') ?>
    <?= $this->Html->script('api/http_timing.js') ?>
    <?= $this->Html->script('api/manage') ?>

    <?= $this->Html->script('chat_notify.js') ?>


    <!--Morris.js [ OPTIONAL ]-->
    <?= $this->Html->script('../plugins/morris-js/morris.min.js') ?>
    <?= $this->Html->script('../plugins/morris-js/raphael-js/raphael.min.js') ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <!-- DX CHART -->
    <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/18.1.6/css/dx.common.css"/>
    <link rel="dx-theme" data-theme="generic.light" href="https://cdn3.devexpress.com/jslib/18.1.6/css/dx.light.css"/>
    <script src="https://cdn3.devexpress.com/jslib/18.1.6/js/dx.all.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <?= $this->Html->script('widget.js') ?>
    <script>

        /* Локализация datepicker */

        $.datepicker.regional['ru'] = {

            closeText: 'Закрыть',

            prevText: 'Предыдущий',

            nextText: 'Следующий',

            currentText: 'Сегодня',

            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],

            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],

            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],

            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],

            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],

            weekHeader: 'Не',

            dateFormat: 'yy-mm-dd',

            firstDay: 1,

            isRTL: false,

            showMonthAfterYear: false,

            yearSuffix: ''

        };

        $.datepicker.setDefaults($.datepicker.regional['ru']);


        $(document).ready(function () {
            $(function () {
                $("#datepicker").datepicker({
                    dateFormat: "yy-mm-dd"
                });

            });
        });

    </script>

    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/highlight.min.js"></script>
    <link id="theme" href="css/themes/type-c/theme-ocean.min.css" rel="stylesheet">
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>

<body>
<div id="container"
     class="effect aside-float aside-bright mainnav-lg <? if ($chat_user_layout == 'true') { ?> page-fixedbar<? } ?>">

    <header id="navbar">
        <div id="navbar-container" class="boxed">

            <div class="navbar-header">
                <a href="/" class="navbar-brand">
                    <div class="brand-title">
                        <span class="brand-text"><img src="/img/api.svg" width="40px">&nbsp;ADMIN&DEV KOREANA</span>
                    </div>
                </a>

            </div>

            <div class="navbar-content">

                <ul class="nav navbar-top-links">

                    <!--Navigation toogle button-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="tgl-menu-btn">
                        <a class="mainnav-toggle" href="#">
                            <i class="pli-list-view"></i>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-top-links">

                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">
                            <i class="pli-bell"></i>
                            <span class="badge badge-header badge-danger"></span>
                        </a>


                        <!--Notification dropdown menu-->
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right" style="">
                            <div class="nano scrollable has-scrollbar" style="height: 265px;">
                                <div class="nano-content" tabindex="0" style="right: -17px;">
                                    <ul class="head-list">
                                        <li>
                                            <a href="#" class="media add-tooltip" data-title="Used space : 95%"
                                               data-container="body" data-placement="bottom" data-original-title=""
                                               title="">
                                                <div class="media-left">
                                                    <i class="pli-data-settings icon-2x text-main"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="text-nowrap text-main text-semibold">HDD is full</p>
                                                    <div class="progress progress-sm mar-no">
                                                        <div style="width: 95%;"
                                                             class="progress-bar progress-bar-danger">
                                                            <span class="sr-only">95% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <i class="pli-file-edit icon-2x"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Write a news
                                                        article</p>
                                                    <small>Last Update 8 hours ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <span class="label label-info pull-right">New</span>
                                                <div class="media-left">
                                                    <i class="pli-speech-bubble-7 icon-2x"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Comment
                                                        Sorting</p>
                                                    <small>Last Update 8 hours ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <i class="pli-add-user-star icon-2x"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">New User
                                                        Registered</p>
                                                    <small>4 minutes ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <img class="img-circle img-sm" alt="Profile Picture"
                                                         src="img/profile-photos/9.png">
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Lucy sent you
                                                        a message</p>
                                                    <small>30 minutes ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <img class="img-circle img-sm" alt="Profile Picture"
                                                         src="img/profile-photos/3.png">
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Jackson sent
                                                        you a message</p>
                                                    <small>40 minutes ago</small>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="nano-pane" style="">
                                    <div class="nano-slider" style="height: 168px; transform: translate(0px);"></div>
                                </div>
                            </div>

                            <!--Dropdown footer-->
                            <div class="pad-all bord-top">
                                <a href="#" class="btn-link text-main box-block">
                                    <i class="pci-chevron chevron-right pull-right"></i>Show All Notifications
                                </a>
                            </div>
                        </div>
                    </li>

                    <li id="dropdown-user" class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                            <span class="ic-user pull-right">
                                <i class="pli-male"></i>
                            </span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right panel-default">
                            <ul class="head-list">
                                <li>
                                    <a href="/change_password"><i class="fa fa-user-secret icon-lg icon-fw"></i> Смена пароля</a>
                                </li>
                                <li>
                                    <a href="/logout"><i class="pli-unlock icon-lg icon-fw"></i>Выход</a>
                                </li>
                            </ul>
                        </div>
                    </li>


                </ul>
            </div>

        </div>
    </header>

    <div class="boxed">

        <div id="content-container">
            <div id="page-head">
                <div class="text-center">
                    <h3><span class="fa fa-cogs"></span> Добро пожаловать в систему администрирования ADMIN&DEV KOREANA
                    </h3>
                </div>
            </div>
            <div id="page-content">
                <div class="row">
                    <!-- CONTENT -->
                    <?= $this->Flash->render() ?>
                    <div class="clearfix">
                        <div class="panel">
                            <div class="panel-body">
                                <?= $this->fetch('content') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav id="mainnav-container">
            <div id="mainnav">

                <div id="mainnav-menu-wrap">
                    <div class="nano">
                        <div class="nano-content">

                            <div id="mainnav-profile" class="mainnav-profile">
                                <div class="profile-wrap text-center">
                                    <div class="pad-btm">
                                        <a href="/profile/<?= $manager_data['Manager']['id'] ?>"><img
                                                    class="img-circle img-md" src="img/profile-photos/1.png"
                                                    alt="Profile Picture"></a>
                                    </div>
                                    <a href="#profile-nav" class="box-block" data-toggle="collapse"
                                       aria-expanded="false">
                                            <span class="pull-right dropdown-toggle">
                                                <i class="dropdown-caret"></i>
                                            </span>
                                        <p class="mnp-name"><?= prepare_fio(
                                                $manager_data['Manager']['firstname'],
                                                $manager_data['Manager']['lastname'],
                                                ""
                                            ) ?></p>
                                        <span class="mnp-desc"><?= $manager_data['Manager']['email'] ?></span>
                                    </a>
                                </div>
                                <div id="profile-nav" class="collapse list-group bg-trans">
                                    <a href="/settings" class="list-group-item">
                                        <i class="pli-male icon-lg icon-fw"></i> Cменить пароль
                                    </a>

                                    <a href="/logout" class="list-group-item">
                                        <i class="pli-unlock icon-lg icon-fw"></i> Выход
                                    </a>

                                </div>
                            </div>

                            <div id="mainnav-shortcut" class="hidden">
                                <ul class="list-unstyled shortcut-wrap">
                                    <li class="col-xs-3" data-content="My Profile">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                                <i class="pli-male"></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Messages">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                                <i class="pli-speech-bubble-3"></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Activity">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                                <i class="pli-thunder"></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Lock Screen">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                                <i class="pli-lock-2"></i>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <ul id="mainnav-menu" class="list-group">


                                <?
                                $navigate_menu = [
                                    [
                                        'functional' => 'god_mode',
                                        'url'        => "/",
                                        'icon'       => '<i class="fa fa-home"></i>',
                                        'title'      => 'Начало',
                                        'page_name'  => "index",
                                    ],
                                    [
                                        'functional' => 'god_mode',
                                        'url'        => "/site_data",
                                        'icon'       => '<i class="fa fa-info-circle"></i>',
                                        'title'      => 'Данные сайта',
                                        'page_name'  => "site_data",
                                        'sub_menu'   => [
                                            [
                                                'url'   => "/site_data",
                                                'title' => 'Данные сайта',
                                            ],
                                            [
                                                'url'   => "/welcome_screens",
                                                'title' => 'Экраны приветствия',
                                            ],
                                            [
                                                'url'   => "/services",
                                                'title' => 'Сервисы на главном',
                                            ],
                                        ],
                                    ],
                                    [
                                        'functional' => 'god_mode',
                                        'url'        => "/mobile",
                                        'icon'       => '<i class="fas fa-code"></i>',
                                        'title'      => 'Запросы из МП',
                                        'page_name'  => "mobile",
                                    ],
                                    [
                                        'functional' => 'clients',
                                        'url'        => "/users",
                                        'icon'       => '<i class="fas fa-user-edit"></i>',
                                        'title'      => 'Клиенты',
                                        'page_name'  => "users",
                                        'sub_menu'   => [
                                            [
                                                'url'   => "/users",
                                                'title' => 'Клиенты',
                                            ],
                                            [
                                                'url'   => "/user_codes",
                                                'title' => 'Привязки кодов смс',
                                            ],
                                            [
                                                'url'   => "/request_1c",
                                                'title' => 'История посещений из 1с',
                                            ],
                                        ]
                                    ],
                                    [
                                        'functional' => 'clients',
                                        'url'        => "/cars",
                                        'icon'       => '<i class="fas fa-car"></i>',
                                        'title'      => 'Марки и модели машин',
                                        'page_name'  => "cars",
                                    ],
                                    [
                                        'functional' => 'clients',
                                        'url'        => "/sto",
                                        'icon'       => '<i class="fas fa-wrench"></i>',
                                        'title'      => 'Запись на СТО',
                                        'page_name'  => "sto",
                                    ],
                                    [
                                        'functional' => 'actions',
                                        'url'        => "/actions",
                                        'icon'       => '<i class="fas fa-money-bill"></i>',
                                        'title'      => 'Акции',
                                        'page_name'  => "actions",
                                    ]
                                    ,
                                    [
                                        'functional' => 'news',
                                        'url'        => "/news",
                                        'icon'       => '<i class="fas fa-newspaper"></i>',
                                        'title'      => 'Новости',
                                        'page_name'  => "news",
                                    ],
                                    [
                                        'functional' => 'contacts',
                                        'url'        => "/locations",
                                        'icon'       => '<i class="fas fa-map-marker"></i>',
                                        'title'      => 'Локации (контакты)',
                                        'page_name'  => "locations",
                                    ],
                                    [
                                        'functional' => 'admins',
                                        'url'        => "/admins",
                                        'icon'       => '<i class="fas fa-user-edit"></i>',
                                        'title'      => 'Администраторы',
                                        'page_name'  => "admins",
                                    ],
                                    [
                                        'functional' => 'god_mode',
                                        'url'        => "/admin/roles",
                                        'icon'       => '<i class="fas fa-cogs"></i>',
                                        'title'      => 'Настройки доступа',
                                        'page_name'  => "settings",
                                    ],
                                    [
                                        'functional' => 'god_mode',
                                        'url'        => "/admin/activity",
                                        'icon'       => '<i class="fas fa-clock"></i>',
                                        'title'      => 'Активность в системе',
                                        'page_name'  => "activity",
                                    ],
                                    [
                                        'functional' => 'god_mode',
                                        'url'        => "/task/list",
                                        'icon'       => '<i class="fas fa-cogs"></i>',
                                        'title'      => 'Фоновые задачи',
                                        'page_name'  => "task_list",
                                    ],
                                    [
                                        'functional' => 'god_mode',
                                        'url'        => "/push/list",
                                        'icon'       => '<i class="fas fa-envelope"></i>',
                                        'title'      => 'Push-уведомления',
                                        'page_name'  => "push",
                                    ],
                                    [
                                        'functional' => 'create_new_pages',
                                        'url'        => "/static_pages",
                                        'icon'       => '<i class="fas fa-newspaper"></i>',
                                        'title'      => 'Конструктор страниц',
                                        'page_name'  => "static_page",
                                        'sub_menu'   => [
                                            [
                                                'url'   => "/static_pages",
                                                'title' => 'Статические страницы',
                                            ],
                                            [
                                                'url'   => "/webforms",
                                                'title' => 'Веб-формы',
                                            ],
                                            [
                                                'url'   => "/webform/reports",
                                                'title' => 'Отчеты по веб-формам',
                                            ],
                                            [
                                                'url'   => "/data_providers",
                                                'title' => 'Источники данных веб-форм',
                                            ],

                                        ],
                                    ],

                                ];

                                foreach ($navigate_menu as $key => $navigate_item) {

                                    $functional = $navigate_item['functional'];
                                    $nav_url    = $navigate_item['url'];
                                    $page_name  = $navigate_item['page_name'];
                                    $nav_icon   = $navigate_item['icon'];
                                    $nav_title  = $navigate_item['title'];
                                    $has_access = true;
                                    if (!empty($functional)) {
                                        $has_access = false;
                                        if (in_array($functional, $access_list)) {
                                            $has_access = true;
                                        }
                                    }
                                    if ($has_access) {
                                        ?>
                                        <li <?= is_active_page($active_page, $page_name, "active-link") ?>>
                                            <a href="<?= $nav_url ?>" data-original-title="" title=""
                                               aria-expanded="false">
                                                <?= $nav_icon ?>
                                                <span class="menu-title"><?= $nav_title ?></span>
                                                <? if (isset($navigate_item['sub_menu']) && count(
                                                        $navigate_item['sub_menu']
                                                    ) > 0) { ?>
                                                    <i class="arrow"></i>
                                                    <?
                                                } ?>
                                            </a>
                                            <? if (isset($navigate_item['sub_menu']) && count(
                                                    $navigate_item['sub_menu']
                                                ) > 0) { ?>


                                                <ul class="collapse" aria-expanded="false">
                                                    <? foreach ($navigate_item['sub_menu'] as $sub_menu_elem) { ?>
                                                        <li <?= is_active_page(
                                                            $active_page,
                                                            $page_name,
                                                            "active-link"
                                                        ) ?>>
                                                            <a href="<?= $sub_menu_elem['url'] ?>"><?= $sub_menu_elem['title'] ?></a>
                                                        </li>
                                                        <?
                                                    } ?>
                                                    <li class="list-divider"></li>
                                                </ul>
                                                <?
                                            } ?>

                                        </li>
                                        <?
                                        if ($key >= 0) {
                                            echo " _______________________________________________";
                                        }
                                    }

                                }

                                ?>
                            </ul>

                            <div class="mainnav-widget">

                                <div class="show-small">
                                    <a href="#" data-toggle="menu-widget" data-target="#wg-server">
                                        <i class="pli-monitor-2"></i>
                                    </a>
                                </div>

                                <div id="wg-server" class="hide-small mainnav-widget-content">
                                    <ul class="list-group">
                                        <!-- раздел статистики в меню админки, добавить пункты по необходимости -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <footer id="footer">

        <div class="show-fixed pad-rgt pull-right">
            You have <a href="#" class="text-main"><span class="badge badge-danger">3</span> pending action.</a>
        </div>
        <p class="pad-lft">&#0169; 2021 Административная система ADMIN&DEV KOREANA</p>

    </footer>
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
</div>
</body>
</html>