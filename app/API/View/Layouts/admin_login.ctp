<!DOCTYPE html>
<html lang="en">
<head>
    <?= $this->Html->charset() ?>
    <title>
        ADMIN::<?php echo $title; ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?= $this->Html->meta('icon') ?>

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <?//= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('bootstrap.min.css') ?>

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <?= $this->Html->css('nifty.min.css') ?>

    <?= $this->Html->css('premium-line-icons.css') ?>

    <?= $this->Html->css('../plugins/pace/pace.min.css') ?>

    <link href="plugins/pace/pace.min.css" rel="stylesheet">
    <script src="plugins/pace/pace.min.js"></script>

    <?= $this->Html->script('../plugins/pace/pace.min.js') ?>

    <?= $this->Html->css('ambi_admin') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>

<body>
<div id="container" class="cls-container">

    <div id="bg-overlay"></div>

    <?= $this->Flash->render() ?>

    <?= $this->fetch('content') ?>

<!--jQuery [ REQUIRED ]-->
<script src="js/jquery.min.js"></script>

<!--BootstrapJS [ RECOMMENDED ]-->
<script src="js/bootstrap.min.js"></script>

<!--NiftyJS [ RECOMMENDED ]-->
<script src="js/nifty.min.js"></script>


<!--Flot Chart [ OPTIONAL ]-->
<script src="plugins/flot-charts/jquery.flot.min.js"></script>
<script src="plugins/flot-charts/jquery.flot.resize.min.js"></script>
<script src="plugins/flot-charts/jquery.flot.tooltip.min.js"></script>

<!--Sparkline [ OPTIONAL ]-->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>

</body>
</html>