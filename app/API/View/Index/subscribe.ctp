<script type="module">

    // Import the functions you need from the SDKs you need

    import {} from "https://www.gstatic.com/firebasejs/9.6.1/firebase-app-compat.js";

    import {initializeApp} from "https://www.gstatic.com/firebasejs/9.6.1/firebase-app.js";

    import {getAnalytics} from "https://www.gstatic.com/firebasejs/9.6.1/firebase-analytics.js";

    import {getMessaging, getToken} from "https://www.gstatic.com/firebasejs/9.6.1/firebase-messaging.js";


    // TODO: Add SDKs for Firebase products that you want to use

    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration

    // For Firebase JS SDK v7.20.0 and later, measurementId is optional

    const firebaseConfig = {

        apiKey: "AIzaSyCgAbbBKTzMyjke1acyIiN8TrImHyEcg58",

        authDomain: "koreana-43c90.firebaseapp.com",

        projectId: "koreana-43c90",

        storageBucket: "koreana-43c90.appspot.com",

        messagingSenderId: "597557318697",

        appId: "1:597557318697:web:ee54ff0e5ab7b9e7b33838",

        measurementId: "G-LHM5LVBZJQ"

    };


    // Initialize Firebase

    const app = initializeApp(firebaseConfig);
    console.log(firebaseConfig);

    const analytics = getAnalytics(app);

    console.log(self && 'ServiceWorkerGlobalScope' in self ? 1 : 2);

    console.log('PushManager' in self,
        'Notification' in self,
        ServiceWorkerRegistration.prototype.hasOwnProperty('showNotification'),
        PushSubscription.prototype.hasOwnProperty('getKey'));


    // Get registration token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    const messaging = getMessaging();
    getToken(messaging, {vapidKey: 'AIzaSyCgAbbBKTzMyjke1acyIiN8TrImHyEcg58'}).then((currentToken) => {
        if (currentToken) {
            console.log("TOKEN: " + currentToken);
            // Send the token to your server and update the UI if necessary
            // ...
        } else {
            // Show permission request UI
            console.log('No registration token available. Request permission to generate one.');
            // ...
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        // ...
    });

</script>

