<!-- LOGIN FORM -->
<!--===================================================-->
<div class="cls-content">
    <div class="cls-content-sm panel">
        <div class="panel-body">
            <div class="mar-ver pad-btm">
                <h1 class="h3">Вход в панель администратора</h1>
                <p>Требуется авторизация</p>
            </div>
            <form action="/login" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="email" name="email" autofocus>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="пароль" name="password">
                </div>
                <!--
                <div class="checkbox pad-btm text-left">
                    <input id="form-checkbox" class="magic-checkbox" type="checkbox">
                    <label for="form-checkbox">Запомнить меня</label>
                </div>
                -->
                <button class="btn btn-primary btn-lg btn-block" type="submit">Вход</button>
            </form>
        </div>

        <div class="pad-all">
            <a href="/passreminder" class="btn-link mar-rgt">Забыли пароль ?</a>
            <?php if ($auth_error) {

                ?>
                <div class="auth_error">
                    Некорректный логин или пароль
                </div>
            <? } ?>

            <div class="media pad-top bord-top"></div>
        </div>
    </div>
</div>