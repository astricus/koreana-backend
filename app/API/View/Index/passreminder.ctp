<!-- LOGIN FORM -->
<!--===================================================-->
<div class="cls-content">
    <div class="cls-content-sm panel">
        <div class="panel-body">
            <div class="mar-ver pad-btm">
                <h1 class="h3">Восстановление доступа в панель администратора</h1>
                <p>Требуется авторизация</p>
            </div>
            <form action="/passreminder" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="email" name="email" autofocus>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Отправить ссылку для сброса пароля</button>
            </form>
        </div>

        <div class="pad-all">
            <a href="/login" class="btn-link mar-rgt">Войти в систему</a>
            <?php if ($auth_error) {

                ?>
                <div class="auth_error">
                    Некорректный логин или пароль
                </div>
            <? } ?>

            <div class="media pad-top bord-top"></div>
        </div>
    </div>
</div>