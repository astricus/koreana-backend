<hr>

<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-chart-bar"></i> Сводная статистика</b>
            </h3>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        $(".datepicker").datepicker({
            language: "ru",
            format: "yyyy-mm-dd"
        });

        /* Дата начала фильтрации */
        $(document).on("change", "#date_from", function () {
            let date_from = $('#date_from').val(), new_url;
            if (date_from !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'date_from', date_from);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'date_from', '');
            }
            redirect(new_url);
        });

        $(document).on("change", "#date_to", function () {
            let date_to = $('#date_to').val(), new_url;
            if (date_to !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'date_to', date_to);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'date_to', '');
            }
            redirect(new_url);
        });

    });

</script>

<h4>Выборка добавленных автомобилей и новых регистраций за указанный период</h4>

<div class="row">

    <div class="col-sm-4 table-toolbar-left">
        <label class="control-label" for="date_from">Дата выборки от</label>
        <input  id="date_from" class="datepicker" name="date_from" value="<?=$date_from?>"/>
    </div>

    <div class="col-sm-8 table-toolbar-left">
        <label class="control-label" for="date_to">Дата выборки до</label>
        <input id="date_to" class="datepicker" name="date_to" value="<?=$date_to?>"/>
    </div>

</div>

<div class="row">

    <div class="col-md-4">
        <?
        $panel_class = ($today_new_cars>0) ? "warning" : "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-car icon-3x"></i>
                </div>
            </div>
            <div class="media-body">

                <p class="text-2x mar-no text-semibold"><?= $cars_by_date ?></p>
                <p class="mar-no">Добавленных машин за период</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-user icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $users_by_date ?></p>
                <p class="mar-no">Новых регистраций за период</p>
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">

    <div class="col-md-4">
        <?
        $panel_class = "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-mobile icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $settings['actual_build'] ?></p>
                <p class="mar-no">Актуальный билд</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-mobile icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $settings['min_build_version'] ?></p>
                <p class="mar-no">минимально поддерживаемый билд</p>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <?
            $panel_class = ($today_new_users>0) ? "warning" : "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-handshake icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $today_new_users ?></p>
                <p class="mar-no">Сегодня новых регистраций</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-handshake icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $total_users ?></p>
                <p class="mar-no">Всего клиентов</p>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <?
        $panel_class = ($today_new_cars>0) ? "warning" : "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-car icon-3x"></i>
                </div>
            </div>
            <div class="media-body">

                <p class="text-2x mar-no text-semibold"><?= $today_new_cars ?></p>
                <p class="mar-no">Сегодня добавленных машин</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-car icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $total_cars ?></p>
                <p class="mar-no">Всего машин пользователей</p>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <?
        $panel_class = ($today_new_api_reqs>0) ? "warning" : "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-code icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $today_new_api_reqs ?></p>
                <p class="mar-no">Сегодня запросов API</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-code icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $total_api_reqs ?></p>
                <p class="mar-no">Всего api запросов</p>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <?
        $panel_class = ($today_new_sto_records>0) ? "warning" : "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-wrench icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $today_new_sto_records ?></p>
                <p class="mar-no">Сегодня записей на СТО</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-wrench icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $total_sto_records ?></p>
                <p class="mar-no">Всего записей на СТО</p>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <?
        $panel_class = ($today_new_push>0) ? "warning" : "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-envelope icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $today_new_push ?></p>
                <p class="mar-no">Сегодня разослано push-уведомлений</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-envelope icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $total_push ?></p>
                <p class="mar-no">Всего разослано push-уведомлений</p>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <?
        $panel_class = ($today_ip_blocked>0) ? "warning" : "mint";
        ?>
        <div class="panel panel-<?=$panel_class?> panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-minus-circle icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $today_ip_blocked ?></p>
                <p class="mar-no">Сегодня блокировок ip адресов</p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-minus-circle icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold"><?= $total_ip_blocked ?></p>
                <p class="mar-no">Всего блокировок Ip адресов</p>
            </div>
        </div>
    </div>

</div>