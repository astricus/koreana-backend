<h1>Добро пожаловать</h1>
<hr>
<h3>Вы находитесь в системе управления платформы Кореана</h3>

<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Данные сайта</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/site_data/add/"><i
                                    class="fa fa-plus"></i> Создать поле</a>
                    </div>
                </div>
            </div>
            <hr>

            <? if (count($site_data) > 0) { ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Ключ</th>
                            <th>Содержание</th>
                            <th>Добавлено</th>
                            <th>Обновлено</th>
                            <th>Редактировать</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n           = 0;
                        $status_list    = [];
                        $status_list[1] = ['success', 'активна'];
                        $status_list[0] = ['danger', 'скрыта'];

                        foreach ($site_data as $site_data_item) {
                            $n++;
                            $site_elem = $site_data_item['Site_Data'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><?= $site_elem['title'] ?></td>
                                <td><?= $site_elem['name'] ?></a></td>
                                <td><?= $site_elem['content'] ?></a></td>

                                <td><span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $site_elem['created']
                                        ) ?></span>
                                </td>
                                <td><span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $site_elem['modified']
                                        ) ?></span>
                                </td>

                                <td>
                                    <a class="btn btn-success btn-bock  site_data_edit"
                                       data-id="<?= $site_elem['id'] ?>"><i class="fa fa-edit"></i> Редактирование</a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <? /*
                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/site-data/";
                            $params         = [];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>
*/ ?>

            <? } else { ?>
                <p class="muted">Данных сайта нет</p>
            <? } ?>

        </div>
    </div>
</div>

<? //echo $this->element('api_navigate'); ?>
<!--
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-warning panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="pli-coding icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">31</p>
                <p class="mar-no">Api-Скрипт</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-info panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="pli-cloud icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">1123</p>
                <p class="mar-no">Файлов в хранилище (1 гб из 8 доступных)</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-mint panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="pli-envelope icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">3</p>
                <p class="mar-no">Новых уведомления</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-danger panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="pli-video icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">2</p>
                <p class="mar-no">Ответа в тикетах</p>
            </div>
        </div>
    </div>

</div>
-->