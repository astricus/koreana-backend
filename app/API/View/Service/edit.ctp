<div class="row">
    <script>
        $(document).on('nifty.ready', function () {

            $('.summernote').summernote({
                'height': '230px',
            });

            setInterval(function () {
                $("#content_new").val($(".summernote").summernote('code'));
            }, 100);

            $('#save-text').on('click', function () {
                $('#summernote-edit').summernote('destroy');
            });

            $(".datepicker").datepicker();

            $(document).on('click',".delete_confirm", function (e) {
                if (confirm('Вы уверены, что хотите удалить данный сервис?')) {
                    let link = $(this).attr('href');
                    window.location.href = link;
                }
                e.preventDefault();
                return false;
            });


        })</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Сервис <i><?= $service['title'] ?> </i></h3>
        </div>
        <form class="form-horizontal" name="edit_service" method="post" action="/service/edit/<?= $service['id'] ?>" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="pad-btm form-inline">

                    <div class="row">
                        <div class="col-sm-6 table-toolbar-left">
                            <a class="btn btn-success btn-bock" href="/services"><i class="fa fa-list"></i> Список сервисов</a>

                            <? if ($service['enabled'] == 1) { ?>
                                <a class="btn btn-warning btn-bock" href="/service/block/<?= $service['id'] ?>"><i class="fa fa-close"></i> Скрыть сервис из показа</a>
                            <? } else { ?>
                                <a class="btn btn-warning btn-bock" href="/service/unblock/<?= $service['id'] ?>"><i class="fa fa-eye"></i> Показывать сервис (сейчас скрыт)</a>

                            <? } ?>

                            <a class="btn btn-danger btn-bock delete_confirm" href="/service/delete/<?=$service['id']?>"><i class="fa fa-minus"></i> Удалить данный сервис</a>
                        </div>
                    </div>

                </div>

                <hr>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="title">Заглавие</label>
                    <div class="col-sm-6">
                        <input type="text" id=title class="form-control" name="title" required
                               placeholder="Заглавие сервиса" value="<?= $service['title'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="name">Название сервиса</label>
                    <div class="col-sm-6">
                        <input type="text" id="name" class="form-control" placeholder="Название сервиса" name="name" value="<?= $service['name'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="link">Ссылка</label>
                    <div class="col-sm-6">
                        <input type="text" id="link" class="form-control" placeholder="Ссылка сервиса" name="link" value="<?= $service['link'] ?>">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-lg-3 control-label" for="image">Изображение</label>
                    <div class="col-sm-6">
                        <input type="file" id="image" class="form-control" name="image"/>
                        <a href="<?= $service['image_url'] ?>" target="_blank"><img alt="Иллюстрация сервиса" src="<?= $service['image_url'] ?>" style="max-width: 140px; max-height: 250px;"></a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="content">Содержание сервиса</label>
                    <div class="col-sm-6">

                        <textarea name="content" id="content"><?= $service['content'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="cities">Города показа</p></label>
                    <div class="col-sm-6">
                        <select id="cities" class="use_select2" multiple name="cities[]" style="width: 300px">
                            <option value="">Все города</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if (in_array($city_id, $service_cities)) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Платформа показа</p>
                    </label>
                    <div class="col-sm-6">
                        <select id="platform" class="use_select2" multiple name="platform[]" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($platforms as $platform) { ?>
                                <option value="<?= $platform?>" <? if (in_array($platform, $service_platforms)) echo 'selected'; ?>><?= $platform ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить" />
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>