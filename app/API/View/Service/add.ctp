<div class="row">
    <script>
        $(document).on('nifty.ready', function () {

            $(".datepicker").datepicker();

        })</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Добавление сервиса</h3>
        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/services">Список сервисов </a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_service" method="post" action="/service/add" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="service_title">Заглавие</label>
                    <div class="col-sm-6">
                        <input type="text" id=service_title class="form-control" name="title" required placeholder="Заглавие сервиса" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="service_image">Изображение</label>
                    <div class="col-sm-6">
                        <input type="file" id="service_image" class="form-control" name="image"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="service_preview">Название</label>
                    <div class="col-sm-6">
                        <textarea id="service_preview" class="form-control" name="name" placeholder="Название сервиса"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="service_link">Ссылка сервиса</label>
                    <div class="col-sm-6">
                        <input type="text" id=service_link class="form-control" name="link" required placeholder="Ссылка сервиса" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="content">Содержание</label>
                    <div class="col-sm-6">
                        <textarea name="content" id="content"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Платформа показа</p>
                    </label>
                    <div class="col-sm-6">
                        <select id="platform" class="use_select2 dropdownPlatformSelect" multiple name="platform[]" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($platforms as $platform) { ?>
                                    <option value="<?= $platform; ?>"><?= $platform ?></option>
                                <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Города показа</p></label>
                    <div class="col-sm-6">
                        <select id="show_city" class="use_select2 dropdownPlatformSelect" multiple name="cities[]" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($cities as $city) { ?>
                                <option value="<?= $city['City']['id']; ?>"><?= $city['City']['name'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>


            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>