<script>
    $(document).ready(function () {
        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Платформа */
        $(document).on("change", ".platform_select", function () {
            let platform = $('.platform_select option:selected').val(), new_url;
            if (platform != 0) {
                new_url = updateQueryStringParameter(window.location.href, 'platform', platform);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'platform', '');
            }
            redirect(new_url);
        });

    });

</script>
<?php


?>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список сервисов</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/service/add/"><i
                                    class="fa fa-plus"></i> Создать сервис</a>

                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/services" class="btn btn-success">показать все сервисы</a>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="region_id" class="main_select use_select2 city_select" name="region_id">

                            <option value="0">Все регионы</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 platform_select" name="city_id">
                            <option value="0">Все платформы</option>
                            <? foreach ($platforms as $key => $platform) {
                                ?>
                                <option value="<?= $platform ?>" <? if ($platform == $form_data['platform']) echo 'selected'; ?>>
                                    <?= $platform ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($services) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Заголовок</th>
                            <th>Изображение</th>
                            <th>Добавлен</th>
                            <th>Размещение сервиса</th>
                            <th>Регионы показа</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list[1] = array('success', 'активен');
                        $status_list[0] = array('danger', 'скрыт');

                        foreach ($services as $service) {
                            $n++;
                            $service_data = $service['Service'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/service/view/<?= $service_data['id'] ?>"><?= $service_data['title'] ?></a>
                                </td>
                                <td><?= $service_data['name'] ?></a></td>

                                <td>
                                    <? if (!empty($service_data['image'])) { ?>
                                        <a target="_blank" class="btn-link" href="<?= $content_dir ?>/<?= $service_data['image'] ?>">
                                            <img style="max-width: 140px; max-height: 250px;" src="<?= $content_dir ?>/<?= $service_data['image'] ?>"></a>
                                    <? } ?>
                                </td>

                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($service_data['created']) ?>
                                </span>
                                </td>

                                <td>
                                    <? foreach ($service['platforms'] as $platform) {
                                        ?><span class="text-info"><?= $platform ?>
                                        </span>
                                    <? } ?>
                                </td>
                                <td>
                                    <? foreach ($service['cities'] as $city) {
                                        $city_name = $city['City']['name'];
                                        ?><span class=""><?= $city_name ?>
                                        </span>
                                    <? } ?>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$service_data['enabled']][0] ?>"><?= $status_list[$service_data['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($service_data['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/service/unblock/<?= $service_data['id'] ?>"
                                               data-original-title="Открыть сервис" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/service/block/<?= $service_data['id'] ?>"
                                               data-original-title="Скрыть сервис" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/service/edit/<?= $service_data['id'] ?>"
                                           data-original-title="Изменить данные" data-container="body"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/services/";
                            $params = array();
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>
                <p class="muted">Сервисы не найдены</p>
            <? } ?>

        </div>
    </div>
</div>