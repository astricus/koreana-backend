
<div class="panel-body">
    <div class="blog-title media-block">
        <div class="media-body">
                <h3><?=$service['title'];?></h3>

            <p>заголовок <a href="#" class="btn-link"><?=$service['name'];?> </a></p>
        </div>
    </div>
    <div class="blog-content">
        <div class="blog-body">
            <?=$service['content'];?>
        </div>
    </div>


    <div class="blog-footer">
        <div class="media-left">
            <span class="label label-success"><?=$service['days_later']?></span>
        </div>
    </div>
</div>