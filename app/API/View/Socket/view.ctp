<h3>Api для работы с сокетами</h3>
<hr>

<div class="panel">
    <div class="panel-heading">
        <div class="panel-control">
            <span class="label label-success">Online</span>
        </div>
        <h3 class="panel-title">Server Load</h3>
    </div>
    <div class="panel-body">
        <ul class="list-inline mar-no text-right">
            <li>
                <div class="pad-hor">
                    <span class="text-lg text-semibold text-main">40%</span>
                    <p class="text-muted mar-no">
                        <small>Avg. Server Load</small>
                    </p>
                </div>
            </li>
            <li>
                <div class="pad-hor">
                    <span class="text-lg text-semibold text-main">24 Days</span>
                    <p class="text-muted mar-no">
                        <small>Up Time</small>
                    </p>
                </div>
            </li>
            <li>
                <div class="pad-hor">
                    <span class="text-lg text-semibold text-main">00:05:23</span>
                    <p class="text-muted mar-no">
                        <small>Avg. Time on Site</small>
                    </p>
                </div>
            </li>
        </ul>
        <div id="realtime-chart" class="flot-full-content" style="height: 200px; padding: 0px; position: relative;">
            <!--Flot chart placement-->
            <canvas class="flot-base"
                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 966.067px; height: 200px;"
                    width="966" height="200"></canvas>
            <canvas class="flot-overlay"
                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 966.067px; height: 200px;"
                    width="966" height="200"></canvas>
        </div>
    </div>

</div>
<script>

        var data = [], totalPoints = 100;
        function getRandomData() {
            if (data.length > 0)
                data = data.slice(1);
// Do a random walk
            while (data.length < totalPoints) {
                var prev = data.length > 0 ? data[data.length - 1] : 50,
                    y = prev + Math.random() * 10 - 5;
                if (y < 20) {
                    y = 30;
                } else if (y > 100) {
                    y = 100;
                }
                data.push(y);
            }
// Zip the generated y values with the x values
            var res = [];
            for (var i = 0; i < data.length; ++i) {
                res.push([i, data[i]])
            }
            return res;
        }

// Set up the control widget
        var updateInterval = 750;
        var flotOptions = {
            series: {
                lines: {
                    lineWidth: 1,
                    show: true,
                    fill: true,
                    fillColor: "#d8d9d9"
                },
                color: '#cccccc',
                shadowSize: 0	// Drawing is faster without shadows
            },
            yaxis: {
                min: 0,
                max: 110,
                ticks: 30,
                show: false
            },
            xaxis: {
                show: false
            },
            grid: {
                hoverable: true,
                clickable: true,
                borderWidth: 0
            },
            tooltip: false,
            tooltipOpts: {
                defaultTheme: false
            }
        }


        var plot = $.plot("#realtime-chart", [getRandomData()], flotOptions);

        function update() {
            plot.setData([getRandomData()]);

// Since the axes don't change, we don't need to call plot.setupGrid()

            plot.draw();
            setTimeout(update, updateInterval);
        }

        update();

</script>

<a href="/socket/create" class="btn btn-lg btn-success">Создать сервер сокетов</a>

<style type="text/css">
    .chat-wrapper {
        font: bold 11px/normal 'lucida grande', tahoma, verdana, arial, sans-serif;
        background: #00a6bb;
        padding: 20px;
        margin: 20px auto;
        box-shadow: 2px 2px 2px 0px #00000017;
        max-width: 700px;
        min-width: 500px;
    }

    #message-box {
        width: 97%;
        display: inline-block;
        height: 300px;
        background: #fff;
        box-shadow: inset 0px 0px 2px #00000017;
        overflow: auto;
        padding: 10px;
    }

    .user-panel {
        margin-top: 10px;
    }

    input[type=text] {
        border: none;
        padding: 5px 5px;
        box-shadow: 2px 2px 2px #0000001c;
    }

    input[type=text]#name {
        width: 20%;
    }

    input[type=text]#message {
        width: 60%;
    }

    button#send-message {
        border: none;
        padding: 5px 15px;
        background: #11e0fb;
        box-shadow: 2px 2px 2px #0000001c;
    }
</style>

<div class="chat-wrapper">
    <div id="message-box"></div>
    <div class="user-panel">
        <input type="text" name="name" id="name" placeholder="Your Name" maxlength="15"/>
        <input type="text" name="message" id="message" placeholder="Type your message here..." maxlength="100"/>
        <button id="send-message">Send</button>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script language="javascript" type="text/javascript">
    //create a new WebSocket object.
    var msgBox = $('#message-box');
    var wsUri = "ws://185.185.70.119:8000/socket_server.php";//localhost
    websocket = new WebSocket(wsUri);

    websocket.onopen = function (ev) { // connection is open
        msgBox.append('<div class="system_msg" style="color:#bbbbbb">Welcome to my "Demo WebSocket Chat box"!</div>'); //notify user
    }
    // Message received from server
    websocket.onmessage = function (ev) {
        var response = JSON.parse(ev.data); //PHP sends Json data

        var res_type = response.type; //message type
        var user_message = response.message; //message text
        var user_name = response.name; //user name
        var user_color = response.color; //color

        switch (res_type) {
            case 'usermsg':
                msgBox.append('<div><span class="user_name" style="color:' + user_color + '">' + user_name + '</span> : <span class="user_message">' + user_message + '</span></div>');
                break;
            case 'system':
                msgBox.append('<div style="color:#bbbbbb">' + user_message + '</div>');
                break;
        }
        msgBox[0].scrollTop = msgBox[0].scrollHeight; //scroll message

    };

    websocket.onerror = function (ev) {
        msgBox.append('<div class="system_error">Error Occurred - ' + ev.data + '</div>');
    };
    websocket.onclose = function (ev) {
        msgBox.append('<div class="system_msg">Connection Closed</div>');
    };

    //Message send button
    $('#send-message').click(function () {
        send_message();
    });

    //User hits enter key
    $("#message").on("keydown", function (event) {
        if (event.which == 13) {
            send_message();
        }
    });

    //Send message
    function send_message() {
        var message_input = $('#message'); //user message text
        var name_input = $('#name'); //user name

        if (message_input.val() == "") { //empty name?
            alert("Enter your Name please!");
            return;
        }
        if (message_input.val() == "") { //emtpy message?
            alert("Enter Some message Please!");
            return;
        }

        //prepare json data
        var msg = {
            message: message_input.val(),
            name: name_input.val(),
            color: '#f90'
        };
        //convert and send data to server
        websocket.send(JSON.stringify(msg));
        message_input.val(''); //reset message input
    }
</script>