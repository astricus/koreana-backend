<div class="panel panel-warning panel-colorful media middle pad-all">
    <div class="media-left">
        <div class="pad-hor">
            <i class="fa fa-cloud-download-alt icon-2x"></i>
        </div>
    </div>
    <div class="media-body">
        <p class="text-lg mar-no text-semibold">File Storage Api > Файловый менеджер</p>
    </div>
</div>
<? echo $this->element('file_navigate'); ?>
<style>
    .file_element {
        cursor: pointer;
    }
</style>
<script>
    $(document).ready(function () {

        //form Submit
        var no_action = "no-action";
        $(document).on("click", ".reload_file_list", function (evt) {
            if ($(this).hasClass(no_action)) {
                return false;
            }
            $(this).addClass(no_action);
            reloadFileList();
            $(this).removeClass(no_action);
        });

        var setDirectory = function (id) {
            $(".directory_id").val(id);
            if (id > 0) {
                var new_url = "/file/list/" + id;
                $(".parent_dir_button").show();
            } else {
                var new_url = "/file/list";
                $(".parent_dir_button").hide();
            }
            var stateObj = {};
            history.pushState(stateObj, "", new_url);
        };

        $(document).on("click", ".parent_dir", function (evt) {
            var dir_id = $(this).attr('data-id');
            if ($(this).hasClass(no_action)) {
                return false;
            }
            $(this).addClass(no_action);
            console.log("dir_id" + dir_id);
            setDirectory(dir_id);
            reloadFileList();
            $(this).removeClass(no_action);
        });

        $(document).on("click", ".is_directory", function (evt) {
            var dir_id = $(this).attr('data-id');
            var current_id = $(".directory_id").val();
            if ($(this).hasClass(no_action)) {
                return false;
            }
            $(this).addClass(no_action);
            console.log("dir_id" + dir_id);
            setDirectory(dir_id);
            $(".parent_dir").attr('data-id', current_id);
            reloadFileList();
            $(this).removeClass(no_action);
        });


        $(document).on("change", ".upload_file_form", function (evt) {
            console.log("upload_file_form");
            evt.preventDefault();
            var formData = new FormData(document.getElementById('upload_form'));
            console.log(formData);
            $.ajax({
                url: 'file/upload/',
                type: 'post',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                    reloadFileList();
                },
                error: function (response) {
                    alert(response);
                }
            });
            return false;
        });

        $(document).on("click", ".file_menu_download", function (evt) {
            var id = $(this).attr('data-id');
            console.log("download remote file");
            evt.preventDefault();
            $.ajax({
                url: 'file/download/' + id,
                type: 'post',
                data: "",
                dataType: 'json',
                success: function (response) {
                    var link = response.data.link;
                    //console.log(response);
                    var loading = "/file/get_file/?f=" + link;
                    window.open(
                        loading,
                        '_blank'
                    );
                },
                error: function (response) {
                    console.log(response);
                }
            });
            return false;
        });

        $(document).on("click", ".file_menu_delete", function (evt) {
            var id = $(this).attr('data-id');
            evt.preventDefault();
            $.ajax({
                url: 'file/delete/' + id,
                type: 'post',
                data: "",
                dataType: 'json',
                success: function (response) {
                    reloadFileList();
                },
                error: function (response) {
                    console.log(response);
                }
            });
            return false;
        });

        var updateParentDir = function (dir_id) {
            $(".parent_dir").attr('data-id', dir_id);
        };

        var reloadFileList = function () {
            var formData = "";
            var dir_id = $("input[name='dir_id']").val();
            console.log(dir_id);
            formData = "dir_id=" + dir_id;
            $.ajax({
                url: 'file/list_ajax/',
                type: "post",
                dataType: 'json',
                data: formData,
                success: function (response) {
                    var dirs = response.data.dirs;
                    var files = response.data.files;
                    $("#file-list").html("");
                    if (dirs.length > 0 || files.length > 0) {

                        if (dirs.length > 0) {
                            loadFileList(dirs);
                        }
                        loadFileList(files);
                    }
                    var parent_dir = response.data.parent_dir;
                    updateParentDir(parent_dir);
                    var dir_path = response.data.dir_path;
                    var dir_ids = response.data.dir_ids;
                    resetBreadcrumbs(dir_path, dir_ids);
                },
                error: function (response) {
                    alert(response);
                }
            });
        };

        var resetBreadcrumbs = function (dir_path, dir_ids) {
            $(".dir_path li").each(function () {
                if (!$(this).hasClass("base_path")) {
                    $(this).remove();
                }
            });
            for (var j = 0; j < dir_path.length; j++) {
                var path_url = "file/list" + "/" + dir_ids[j];
                console.log(dir_path[j]);
                if (dir_path.length != j-1) {
                    var item = '<li><a href="' + path_url + '" >' + dir_path[j] + '</a></li>';
                } else {
                    var item = '<li class="active">' + dir_path[j] + '</li>';
                }


                $(".dir_path").append(item);
            }
        };

        var showFileItem = function (file_item, file_id, file_name, file_type, created, file_size) {
            console.log(file_id);

            var format_html = file_item.replace(/FILE_ID/g, file_id);

            format_html = format_html.replace("FILE_NAME", file_name);
            format_html = format_html.replace("FILE_TYPE", setFileTypeIcon(file_type));
            format_html = format_html.replace("FILE_SIZE", file_size);
            format_html = format_html.replace("FILE_CREATED", created);
            if (file_type == "dir") {
                format_html = format_html.replace("DIR", "is_directory");
            } else {
                format_html = format_html.replace("DIR", "");
            }
            return format_html;
        };

        var setFileTypeIcon = function (type) {
            console.log(type);
            if (type == "text") {
                return "pli-file-txt";
            } else if (type == "image") {
                return "psi-file-jpg";
            } else if (type == "archive") {
                return "psi-folder-zip text-success";
            } else if (type == "music") {
                return "psi-file-music";
            } else if (type == "video") {
                return "pli-video";
            } else if (type == "doc") {
                return "psi-file-word text-info";
            } else if (type == "html") {
                return "psi-file-html text-info";
            } else if (type == "dir") {
                return "psi-folder text-info";
            } else {
                return "pli-file";
            }
        };

        reloadFileList();

        var file_item = '<li>' +
            '<div class="file-control">' +
            '<input id="FILE_ID" class="magic-checkbox" type="checkbox">' +
            '<label for="FILE_ID"></label>' +
            '</div> ' +
            '<div class="file-settings dropdown">' +
            '<a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="pci-ver-dots"></i></a>' +
            '<ul class="dropdown-menu dropdown-menu-right" style="">' +
            '<li><a class="file_menu_move" data-id="FILE_ID"><i class="fa fa-arrow-circle-right"></i> Перенести/скопировать</a></li>' +
            '<li><a class="file_menu_delete" data-id="FILE_ID"><i class="icon pli-recycling"></i> удалить</a></li>' +
            '<li><a class="file_menu_download" data-id="FILE_ID"><i class="icon pli-download-from-cloud"></i> сохранить</a></li>' +
            '<li class="divider"></li>' +
            '<li><a href="#" class="file_menu_save" data-id="FILE_ID"><i class="fa fa-file-download"></i> Создать публичную ссылку</a></li>' +
            '</ul>' +
            '</div>' +
            '<div class="file-attach-icon"></div>' +
            '<a class="file-details file_element DIR" data-id=FILE_ID>' +
            '<div class="media-block">' +
            '<div class="media-left"><i class="FILE_TYPE"></i></div>' +
            '<div class="media-body">' +
            '<p class="file-name">FILE_NAME</p>' +
            '<small>FILE_CREATED | FILE_SIZE</small>' +
            '</div>' +
            '</div>' +
            '</a>' +
            '</li>';

        var loadFileList = function (files) {
            console.log(files);
            for (var i = 0; i < files.length; i++) {
                var file_data = files[i];
                if (file_data.type == "dir") {
                    var name = file_data.name;
                    var size = "";
                } else {
                    var name = file_data.file_name;
                    var size = file_data.file_size;
                }

                var file_view_html = showFileItem(file_item, file_data.id, name, file_data.type, file_data.created, size);
                console.log(file_view_html);
                $("#file-list").append(file_view_html);
            }
        }
    });
</script>

<div class="bord-btm pad-ver">
    <ol class="breadcrumb dir_path">
        <li class="base_path"><a href="/file/list">Файловое хранилище</a></li>
        <li class="active">Начало</li>
    </ol>
</div>
<style>
    .progressbar {
        border-bottom: 1px solid #bbb;
        height: 26px;
        border-radius: 2px;
        margin: 2px;
        background: linear-gradient(-45deg,
        rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%,
        rgba(255, 255, 255, 0.15) 75%, transparent 75%) left/30px 30px repeat-x,
        linear-gradient(to right, green 0%, yellow 50%, red 100%) left/var(--p, 100%) fixed,
        lightgray;
        box-shadow: inset 0px -1px 4px rgba(0, 0, 0, 0.5);
        animation: change 2s linear infinite;
    }

    @keyframes change {
        from {
            background-position: 0 0, left
        }
        to {
            background-position: 30px 0, left
        }
    }
</style>

<div class="panel panel-default panel-colorful">
    <div class="pad-all">
        <p class="text-main"><i class="pli-data-storage icon-fw"></i> Хранилище заполнено на <?= $percent ?>% (<span
                    class="current_size text-bold"><?= $current_size ?></span>
            из <span class="total_size_limit text-bold"><?= $total_limit ?></span> по лимиту)</p>
        <div class="progress progress-xl">
            <div class="progressbar" style="width:<?= $percent ?>%;"></div>
        </div>
    </div>

</div>

<div class="fixed-fluid">

    <div class="fluid file-panel">

        <div class="file-toolbar bord-btm">
            <div class="btn-file-toolbar">
                <a class="btn btn-icon add-tooltip" href="#" data-original-title="Начало" data-toggle="tooltip"><i
                            class="icon-2x pli-home"></i></a>
                <a class="btn btn-icon add-tooltip reload_file_list" data-original-title="Обновить"
                   data-toggle="tooltip"><i
                            class="icon-2x pli-reload-3"></i></a>
            </div>
            <div class="btn-file-toolbar">
                <a class="btn btn-icon add-tooltip" href="#" data-original-title="Создать директорию"
                   data-toggle="tooltip"><i
                            class="icon-2x pli-folder"></i></a>
                <a class="btn btn-icon add-tooltip" href="#" data-original-title="Создать файл" data-toggle="tooltip"><i
                            class="icon-2x pli-file-add"></i></a>
                <a class="btn btn-icon add-tooltip" href="#" data-original-title="Редактировать"
                   data-toggle="tooltip"><i
                            class="icon-2x pli-file-edit"></i></a>
            </div>
            <div class="btn-file-toolbar pull-right">
                <a class="btn btn-icon add-tooltip" href="#" data-original-title="удалить" data-toggle="tooltip"><i
                            class="icon-2x pli-recycling"></i></a>
                <a class="btn btn-icon add-tooltip" href="#" data-original-title="сохранить" data-toggle="tooltip"><i
                            class="icon-2x pli-download-from-cloud"></i></a>
            </div>
            <div class="btn-file-toolbar">
                <form name="upload" id="upload_form">
                    <input type="hidden" name="dir_id" value="<?= $dir_id ?>" class="directory_id">
                    <input type="file" class="btn btn-block btn-md btn-info v-middle upload_file_form" name="uploaded"
                           value="Загрузить файлы"/>
                </form>
            </div>
        </div>
        <ul id="mail-list" class="file-list">

            <li <? if ($parent_dir == null) { ?>style="display: none"<? } ?> class="parent_dir_button">
                <div class="file-control">
                    <input id="file-list-1" class="magic-checkbox" type="checkbox">
                    <label for="file-list-1"></label>
                </div>
                <div class="file-attach-icon"></div>
                <a class="file-details file_element parent_dir" data-id="<?= $parent_dir ?>">
                    <div class="media-block">
                        <div class="media-left"><i class="psi-folder"></i></div>
                        <div class="media-body">
                            <p class="file-name single-line">...</p>
                        </div>
                    </div>
                </a>
            </li>

        </ul>
        <ul id="file-list" class="file-list" style="margin-top: -50px"></ul>

    </div>
</div>