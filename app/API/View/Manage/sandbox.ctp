<h1>Содержание</h1>
<hr>

<div class="row">

    <div class="col-md-4">
        <div class="panel panel-primary panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-cogs icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Backend Api Koreana</p>
                <p class="mar-no">Общее api backend платформы </p>
            </div>
        </div>
        <p class="">Общее api backend платформы (сайты + мобильное приложения).</p>
        <? echo $this->element('hive_navigate'); ?>
    </div>

    <div class="col-md-4">
        <div class="panel panel-success panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-users-cog icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Управление администраторами</p>
                <p class="mar-no">Управление администраторами, редакторами</p>
            </div>
        </div>
        <p class="">Раздел содержит API для работы с администраторами и редакторами. Роли администраторов, блокировка</p>
        <? echo $this->element('user_navigate'); ?>
    </div>

    <!--
    <div class="col-md-4">
        <div class="panel panel-warning panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-cloud-download-alt icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">File Storage Api</p>
                <p class="mar-no">Api для работы с файловым хранилищем</p>
            </div>
        </div>
        <p class="">Здесь можно найти API для работы файлами по сети - создание, сохранение, передача, редактирование, удаление.
            Функции сжатия файлов, возможности сжатия изображений.
            Также разработчикам доступно файловое хранилище для хранения файлов.</p>
        <? //echo $this->element('file_navigate'); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-at icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Email @ Api</p>
                <p class="mar-no">api методы для работы с email</p>
            </div>
        </div>
        <p class="">Раздел содержит API для отправки Email. Можно отправить как одно письмо, так и запустить
            рассылку писем на отправку, плюс управление рассылками почты - запуск, установка приоритетности и т.д.</p>
        <? //echo $this->element('email_navigate'); ?>
    </div>

    <div class="col-md-4">
        <div class="panel panel-danger panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fas fa-database icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Data Api</p>
                <p class="mar-no">api для работы с пользовательскими данными</p>
            </div>
        </div>
        <p class="">Раздел содержит функционал для работы с данными.
            Стандартный CRUD, связанные таблицы, категоризация, управление структурами, поиск по данным, фильтрация
            запросы с аггрегацией, поиск, оптимизация, работа с разными проектами.</p>
        <? //echo $this->element('data_navigate'); ?>
    </div>

    <div class="col-md-4">
        <div class="panel panel-purple panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-tasks icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Task Api</p>
                <p class="mar-no">api для работы с задачами</p>
            </div>
        </div>
        <p class="">Задача - нечто, что требуется сделать, набор действий, запускаемый с некой периодичностью, разово
            или при наступлении заданных условий или событий.</p>
        <? //echo $this->element('task_navigate'); ?>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-dark panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-laptop-code icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Http + Curl Api</p>
                <p class="mar-no">api для работы с протоколом http и библиотекой curl</p>
            </div>
        </div>
        <p class="">Раздел содержит необходимые API для работы с протоколом HTTP. Можно отправить как простые get и post
            запросы, так
            и протестировать пинг и отклик для сайта, а также можно сделать хук для регулярных запросов сайта для
            проверки доступности</p>
        <? //echo $this->element('http_navigate'); ?>
    </div>

    <div class="col-md-4">
        <div class="panel panel-pink panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fas fa-plug icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Web Socket Api</p>
                <p class="mar-no">api для работы с web socket сервером</p>
            </div>
        </div>
        <p class="">Раздел содержит функционал для запуска и работы web-сокет сервера. Отправка данных в сокет и чтение данных из сокета,
            кастомная авторизация для соединения с сокет-сервером.</p>
        <? //echo $this->element('socket_navigate'); ?>
    </div>

    <div class="col-md-4">
        <div class="panel panel-mint panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fas fa-edit icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Forms Api</p>
                <p class="mar-no">Api для работы с веб формами</p>
            </div>
        </div>
        <p class="">Раздел содержит необходимое API для работы с данными. Сохранение данных, запрос,
            обновление и удаление.
            Категоризация, управление структурой, поиск по данным, фильтрация</p>
        <? //echo $this->element('form_navigate'); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-success panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fa fa-envelope icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Sms Api</p>
                <p class="mar-no">api для отправки смс сообщений</p>
            </div>
        </div>
        <p class="">Раздел содержит необходимые API для отправки смс сообщений</p>
        <? //echo $this->element('sms_navigate'); ?>
    </div>

    <div class="col-md-4">
        <div class="panel panel-primary panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="fas fa-language icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">Translate Api</p>
                <p class="mar-no">api для языковых переводов</p>
            </div>
        </div>
        <p class="">Раздел содержит функционал для работы языковых переводов текстов.</p>
        <? //echo $this->element('translate_navigate'); ?>
         -->
    </div>
</div>