<? echo $this->element('api_breadcrumbs'); ?>
<? echo $this->element('api_navigate'); ?>
<? echo $this->element('api_component_list'); ?>

<div class="row">
    <div class="col-sm-6">
        <div class="panel">
            <form class="form-horizontal run_api_request_form" name="api_post" method="post"
                  action="<?= $action_url ?>">
                <div class="panel-body">

                    <h4>Запустить Http Post запрос API</h4>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="api_url">Url</label>
                        <div class="col-sm-9">
                            <input type="url" placeholder="целевой url" id="api_url" name="url" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="user-agent-field">User-Agent</label>
                        <div class="col-sm-9">
                            <select name="user-agent" id="user-agent-field">
                                <option value="">Использовать текущий браузер</option>
                                <option value="Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1">
                                    Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML,
                                    like Gecko) Version/11.0 Mobile/15A372 Safari/604.1
                                </option>
                                <option value="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.">
                                    Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1
                                </option>
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="content-type-field">Content-Type</label>
                        <div class="col-sm-9">
                            <select name="content-type" id="content-type-field">
                                <option value="application/x-www-form-urlencoded">application/x-www-form-urlencoded
                                </option>
                                <option value="application/json">application/json</option>
                                <option value="application/json">application/zip</option>
                                <option value="text/html; charset=UTF-8">text/html; charset=UTF-8</option>
                                <option value="text/plain; charset=UTF-8">text/plain;</option>
                                <option value="image/jpeg">image/jpeg</option>
                                <option value="text/xml">text/xml</option>
                                <option value="multipart/form-data">multipart/form-data</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="content-type-manual">Content-Type (установить
                            вручную)</label>
                        <div class="col-sm-9">
                            <input placeholder="Content-Type рукописный" id="content-type-manual"
                                   name="content_type_manual" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="cookie">Cookie</label>
                        <div class="col-sm-9">
                            <input placeholder="Cookie" id="cookie" name="cookie" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="data">Данные</label>
                        <div class="col-sm-9">
                            <textarea placeholder="Данные для метода" id="data" name="data"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                </div>

                <div class="panel-footer text-center">
                    <button class="btn btn-success run_api_request" type="submit">Запуск</button>
                </div>
            </form>


            <form class="form-horizontal save_api_request_form" name="api_post" method="post" action="<?= $action_url ?>">
                <div class="panel-body">

                    <h4>Сохранить api метод для дальнейшего использования</h4>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="user_api_method_name">Название метода</label>
                        <div class="col-sm-9">
                            <input placeholder="Название запроса (в двух словах,)" id="user_api_method_name"
                                   name="user_api_method_name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="user_api_method_name">Alias метода</label>
                        <div class="col-sm-9">
                            <input placeholder="Alias запроса, для дальнейшего использования в скриптах, если будет пустой сгенерируется автоматически"
                                   id="user_api_method_alias"
                                   name="user_api_method_alias" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="user_api_method_comment">Описание метода</label>
                        <div class="col-sm-9">
                            <textarea placeholder="Здесь вы можете указать описание или предназначение метода"
                                      id="user_api_method_comment"
                                      name="user_api_method_comment" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <button class="btn btn-success save_api_request">Сохранить метод</button>
                </div>
            </form>

        </div>
    </div>
    <div class="col-sm-6 result_template">
        <?= $this->element("result"); ?>
    </div>
</div>