<? echo $this->element('api_breadcrumbs'); ?>
<? echo $this->element('api_navigate'); ?>
<? echo $this->element('api_component_list'); ?>

<div class="row">
    <div class="col-sm-6">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Запустить Http Get запрос API</h3>
            </div>
            <form class="form-horizontal run_api_request_form" name="api_get" method="post" action="<?=$action_url?>">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="api_url">Url</label>
                        <div class="col-sm-9">
                            <input type="url" placeholder="целевой url" id="api_url" name="url" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="user-agent-field">User-Agent</label>
                        <div class="col-sm-9">
                            <select name="user-agent" id="user-agent-field">
                                <option value="">Использовать текущий браузер</option>
                                <option value="Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1">
                                    Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML,
                                    like Gecko) Version/11.0 Mobile/15A372 Safari/604.1
                                </option>
                                <option value="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.">
                                    Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1
                                </option>
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="content-type-field">Content-Type</label>
                        <div class="col-sm-9">
                            <select name="content-type" id="content-type-field">
                                <option value="application/x-www-form-urlencoded">application/x-www-form-urlencoded</option>
                                <option value="application/json">application/json</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="data">Данные</label>
                        <div class="col-sm-9">
                            <textarea placeholder="Данные для метода" id="data" name="data" class="form-control"></textarea>
                        </div>
                    </div>


                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-success run_api_request" type="submit">Начать</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-6 result_template">
        <?=$this->element("result");?>
    </div>
</div>