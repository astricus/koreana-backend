<script>
    $(document).ready(function () {
        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Платформа */
        $(document).on("change", ".platform_select", function () {
            let platform = $('.platform_select option:selected').val(), new_url;
            if (platform != "") {
                new_url = updateQueryStringParameter(window.location.href, 'platform', platform);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'platform', '');
            }
            redirect(new_url);
        });

        /* Дата показа */
        $(document).on("change", "#start_datetime", function () {
            let start_datetime = $('#start_datetime').val(), new_url;
            if (start_datetime != "") {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', start_datetime);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', '');
            }
            redirect(new_url);
        });

    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список новостей</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/news/add/"><i class="fa fa-plus"></i>
                            Создать новость</a>

                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/news" class="btn btn-success">показать все новости</a>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 city_select" name="city_id">

                            <option value="0">Все города</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 platform_select" name="city_id">
                            <option value="">Все платформы</option>
                            <? foreach ($platforms as $key => $platform) {
                                ?>
                                <option value="<?= $platform ?>" <? if ($platform == $form_data['platform']) echo 'selected'; ?>>
                                    <?= $platform ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left form-control">
                        <label class="control-label" for="start_datetime">Дата начала показа (начиная от)</label>
                        <input type="date" id="start_datetime" name="start_datetime"/>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($news) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Категория</th>
                            <th>Изображение</th>
                            <th>Автор</th>
                            <th>Добавлена</th>
                            <th>Дата/время запуска</th>
                            <th>Дата/время окончания</th>
                            <th>Размещение</th>
                            <th>Регионы показа</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list[1] = array('success', 'активна');
                        $status_list[0] = array('danger', 'скрыта');

                        foreach ($news as $new) {
                            $n++;

                            $news_data = $new['News'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/news/view/<?= $news_data['id'] ?>"><?= $news_data['title'] ?></a></td>
                                <td><?= $news_data['category'] ?></td>
                                <td>
                                    <? if (!empty($news_data['image'])) { ?>
                                        <a target="_blank" class="btn-link" href="<?= $content_dir ?>/<?= $news_data['image'] ?>"><img
                                                    style="max-width: 140px; max-height: 250px;"
                                                    src="<?= $content_dir ?>/<?= $news_data['image'] ?>"></a>
                                    <? } ?>
                                </td>

                                <td><a class="btn-link"
                                       href="/admin/view/<?= $news_data['author_id'] ?>"><?= $new['author_name'] ?></a>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($news_data['created']) ?>
                                    </span>
                                </td>
                                <td>
                                    <span class="text-info"><?= lang_calendar($news_data['start_datetime']) ?></span>
                                </td>
                                <td>
                                    <span class="text-info"><?= lang_calendar($news_data['stop_datetime']) ?></span>
                                </td>

                                <td>
                                    <?= $news_data['platforms'] ?></a>
                                </td>
                                <td>
                                    <? foreach ($new['cities'] as $city) {
                                        $city_name = $city['City']['name'];
                                        ?><span class=""><?= $city_name ?>
                                        </span>
                                    <? } ?>
                                </td>

                                <td>
                                    <div class="label label-<?= $status_list[$news_data['enabled']][0] ?>"><?= $status_list[$news_data['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($news_data['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/news/unblock/<?= $news_data['id'] ?>"
                                               data-original-title="Открыть новость" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/news/block/<?= $news_data['id'] ?>"
                                               data-original-title="Скрыть новость" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/news/edit/<?= $news_data['id'] ?>"
                                           data-original-title="Изменить данные" data-container="body"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/news/";
                            $params = array(
                                'platform' => $form_data['platform'],
                                'city_id' => $form_data['city_id'],
                                'start_datetime' => $form_data['start_datetime'],
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Новостей нет</p>
            <? } ?>

        </div>
    </div>
</div>