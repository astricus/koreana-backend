
<div class="panel-body">
    <div class="blog-title media-block">
        <div class="media-body">
                <h3><?=$new['title'];?></h3>
            <p>категория <a href="#" class="btn-link"><?=$new['category'];?> </a></p>
            <p>автор <a href="#" class="btn-link"><?=$new['author_name'];?> </a></p>
        </div>
    </div>
    <div class="blog-content">
        <div class="blog-body">
            <?=$new['content'];?>
        </div>
    </div>


    <div class="blog-footer">
        <div class="media-left">
            <span class="label label-success"><?=$new['days_later']?></span>
            <small>автор: <a href="#" class="btn-link"><?=$new['author_name'];?></a></small>
        </div>
    </div>
</div>