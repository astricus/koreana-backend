<script>

    let last_string = '<?=$form_data['search'];?>';
    $(document).ready(function () {
        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Дата показа */
        $(document).on("change", "#start_datetime", function () {
            let created = $('#start_datetime').val(), new_url;
            if (created != "") {
                new_url = updateQueryStringParameter(window.location.href, 'created', created);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'created', '');
            }
            redirect(new_url);
        });



        $(document).on("keyup", "#search_string", function () {

            let search_delay = function (last_string) {

                let string = $("#search_string").val().trim();

                if (string.length <= 2) {
                    return false;
                }

                if (last_string === string) {
                    return false;
                }
                let new_url;
                if (string !== "") {
                    new_url = updateQueryStringParameter(window.location.href, 'search', string);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'search', '');
                }
                last_string = string;
                redirect(new_url);
            };

            setTimeout(search_delay(last_string), 700);
        });

        $(document).on("click", "#export_csv", function () {
            let new_url = updateQueryStringParameter(window.location.href, 'export_csv', 'export_csv');
            window.open(new_url).focus();
        });

    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список клиентов</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
<!--                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/user/add/"><i class="fa fa-plus"></i>-->
<!--                            Создать пользова</a>-->

                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/users" class="btn btn-success">показать всех клиентов</a>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 city_select" name="city_id">

                            <option value="0">Все города</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left">
                        <label class="control-label" for="created">Дата регистрации (начиная от)</label>
                        <input type="date" id="created" name="created"/>
                    </div>

                    <div class="col-sm-2 table-toolbar-left">
                        <input type="text" id="search_string" name="search_string" placeholder="Поиск"
                               value="<? if (isset($form_data['search'])) echo $form_data['search']; ?>"/>
                    </div>

                    <div id="export_csv" class="btn btn-primary col-sm-1"> => в excel</div>

                </div>

            </div>

            <hr>

            <? if (count($users) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>ФИО</th>
                            <th>Телефон</th>
                            <th>Город</th>
                            <th>Email</th>
                            <th>Регистрация</th>
                            <th>День рождения</th>
                            <th>Последняя активность</th>
                            <th>Список машин</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?
                        $status_list = array();
                        $status_list[1] = array('success', 'активна');
                        $status_list[0] = array('danger', 'скрыта');

                        foreach ($users as $user) {
                            $news_data = $user['User'];
                            ?>
                            <tr>

                                <td><?= $news_data['id'] ?></td>
                                <td><a class="btn-link"
                                       href="/user/<?= $news_data['id'] ?>"><?= prepare_fio($news_data['firstname'], $news_data['lastname'], $news_data['middlename']) ?></a></td>
                                <td>
                                    <a class="btn-link" href="/user/<?= $news_data['id'] ?>">
                                        <?= $news_data['phone'] ?>  <? if(!empty($news_data['login'])) { echo "/" . $news_data['login'];} ?>
                                    </a>
                                </td>
                                <td>
                                    <span class="text-info"><?= $user['City']['name'] ?></span>
                                </td>
                                <td>
                                    <span class="text-info"><?= $news_data['email'] ?></span>
                                </td>
                                <td>
                                    <span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($news_data['created']) ?>
                                    </span>
                                </td>
                                <td>
                                    <span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($news_data['date_of_birth']) ?>
                                    </span>
                                </td>
                                <td>
                                    <span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($user['User']['last_auth']) ?>
                                    </span>
                                </td>

                                <td>
                                    <? foreach ($user['cars'] as $car) {
                                        $car_mark = $car['Car_Mark']['name'];
                                        $car_model = $car['Car_Model']['name'];
                                        $car_number = $car['User_Car']['car_number'];
                                        ?><span class="text-info"><?= $car_mark . " " . $car_model ?><b>[<?=$car_number?>]</b></span>,
                                    <? } ?>
                                </td>
                                <td>
                                    <!--
                                    <div class="btn-group">
                                        <? if ($news_data['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/news/unblock/<?= $news_data['id'] ?>"
                                               data-original-title="Открыть новость" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/news/block/<?= $news_data['id'] ?>"
                                               data-original-title="Скрыть новость" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/news/edit/<?= $news_data['id'] ?>"
                                           data-original-title="Изменить данные" data-container="body"></a>
                                           -->
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/users/";
                            $params = array(
                                'created' => $form_data['created'],
                                'city_id' => $form_data['city_id'],
                                'search' => $form_data['search']
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Клиентов нет</p>
            <? } ?>

        </div>
    </div>
</div>