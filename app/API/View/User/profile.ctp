<div class="row">
    <a class="btn btn-success btn-bock" href="/users/"><i class="fa fa-list"></i> Список клиентов</a>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Профиль клиента</h3>

        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <? if (count($user) > 0) { ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td width="140">
                                        ID
                                    </td>
                                    <td>
                                        <b><?= $user['User']['id'] ?></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ФИО
                                    </td>
                                    <td>
                                        <?= prepare_fio($user['User']['firstname'], $user['User']['lastname'], $user['User']['middlename']) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        Город
                                    </td>
                                    <td>
                                        <?= $user['City']['name'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        Номер телефона
                                    </td>
                                    <td>
                                        <?= $user['User']['phone'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        Логин
                                    </td>

                                    <td>
                                        <?= $user['User']['login'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        Email
                                    </td>
                                    <td>
                                        <?= $user['User']['email'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        Регистрация
                                    </td>

                                    <td class="">
                                        <?= lang_calendar($user['User']['created']) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        Дата рождения
                                    </td>
                                    <td class="">
                                        <?= lang_calendar($user['User']['date_of_birth']) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        Последнее посещение
                                    </td>
                                    <td class="">
                                        <?= lang_calendar($auths[0]['Auth']['created']) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    <? } ?>
                </div>
                <div class="col-sm-8">
                    <? if (count($auths) > 0) { ?>
                        <h3 class="panel-title">Авторизации в системе</h3>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td>
                                        Дата
                                    </td>
                                    <td>
                                        Номер телефона
                                    </td>
                                    <td>
                                        Система / Браузер
                                    </td>
                                    <td>
                                        IP
                                    </td>
                                </tr>
                                <? foreach ($auths as $auth) { ?>
                                    <tr>
                                        <td class="">
                                            <?= lang_calendar($auth['Auth']['created']) ?>
                                        </td>
                                        <td class="">
                                            <?= $auth['Auth']['phone'] ?>
                                        </td>
                                        <td class="">
                                            <?= $auth['Auth']['os'] ?>
                                        </td>
                                        <td class="">
                                            <?= $auth['Auth']['ip'] ?>
                                        </td>
                                    </tr>
                                <? } ?>
                            </table>
                        </div>
                    <? } ?>

                    <? if (count($cars) > 0) {
                        $car_status = [
                            'active' => 'активна',
                            'hidden' => 'удалена',
                        ];
                        ?>
                        <h3 class="panel-title">Машины клиента</h3>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td>
                                        Дата добавления
                                    </td>
                                    <td>
                                        Госномер
                                    </td>
                                    <td>
                                        Марка
                                    </td>
                                    <td>
                                        Модель
                                    </td>
                                    <td>
                                        Статус
                                    </td>
                                </tr>
                                <? foreach ($cars as $car) {
                                    ?>
                                    <tr class="">
                                        <td>
                                            <?= lang_calendar($car['User_Car']['created']) ?>
                                        </td>
                                        <td>
                                            <?= $car['User_Car']['car_number'] ?>
                                        </td>
                                        <td>
                                            <?= $car['Car_Mark']['name'] ?>
                                        </td>
                                        <td>
                                            <?= $car['Car_Model']['name'] ?>
                                        </td>
                                        <td>
                                            <?= $car_status[$car['User_Car']['status']]; ?>
                                        </td>
                                    </tr>
                                <? } ?>
                            </table>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</div>