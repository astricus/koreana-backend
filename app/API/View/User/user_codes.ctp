<script>
    $(document).on('click', ".delete_code", function (e) {
        if (confirm('Вы уверены, что хотите полностью удалить данную привязку?')) {
            var link = $(this).attr('href');
            window.location.href = link;
        }
        e.preventDefault();
        return false;
    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-envelope"></i> Привязки кодов смс</h3>
        </div>
        <div class="panel-body">

            <hr>

            <? if (count($user_codes) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Номер телефона</th>
                            <th>Код</th>
                            <th>Дата создания</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;

                        foreach ($user_codes as $user_code) {
                            $n++;
                            $user_code = $user_code['User_Code'];
                            $phone = $user_code['phone'];
                            $code = $user_code['code'];
                            $created = $user_code['created'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><?= $phone ?></td>
                                <td><?= $code ?></td>
                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($created) ?>
                                    </span>
                                </td>
                                <td>
                                    <a class="btn btn-large btn-warning fa fa-trash delete_code"
                                       href="/delete_user_code/<?= $user_code['id'] ?>"
                                       title="Удалить привязку"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>


            <? } else { ?>
                <p class="muted">привязок нет</p>
            <? } ?>
            <hr>
            <form class="form-horizontal" name="add_user_code" method="post" action="/create_user_code">
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="code">Код для входа</label>
                        <div class="col-sm-2">
                            <input type="text" id=code class="form-control" name="code" required placeholder="код"
                                   value=""/>
                        </div>

                        <label class="col-lg-2 control-label" for="phone">Номер телефона</label>

                        <div class="col-sm-3 table-toolbar-left dropdown">
                            <select id="phone" class="main_select use_select2" name="phone" required>
                                <? foreach ($user_phones as $user_phone) {
                                    ?>
                                    <option value="<?= $user_phone ?>">
                                        <?= $user_phone ?>
                                    </option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-sm-2"><input class="btn btn-success" id="save_user_code" type="submit"
                                                     value="Добавить"/></div>

                    </div>

                </div>

            </form>

        </div>
    </div>
</div>