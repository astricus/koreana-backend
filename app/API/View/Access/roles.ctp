<h4>Функциональность администраторов</h4>
<script>
    $(document).ready(function () {
        $(document).on("click", '.switch_role', function () {
            let cur_input = $(this).prev();
            if ($(this).hasClass('btn-success')) {
                cur_input.val("0");
                $(this).removeClass('btn-success').addClass("btn-default");
            } else {
                cur_input.val("1");
                $(this).removeClass('btn-default').addClass("btn-success");
            }
        });
    });
</script>
<div class="row">


    <div class="col-lg-12">
        <div class="panel">
            <form class="form-horizontal" name="update_role" method="post" action="/admin/role/update">
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-vcenter mar-top">
                            <thead>
                            <tr>
                                <th class="min-w-td">Функциональность</th>
                                <?
                                foreach ($roles_list as $role) {
                                    ?>

                                    <th><?= $role ?></th>
                                    <?
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?
                            foreach ($access_list_arr as $key => $access_role) {
                                //echo $key;
                                //pr($access_list);
                                $access_name = $access_elems[$key];
                                //foreach ($roles as $name => $role) {?>
                                <tr>
                                    <td><p class="text-bold"><?= mb_ucfirst($access_name) ?></p> [<?= $key ?>]</td>

                                    <? foreach ($access_role as $access_rolename => $access_role_item) {
                                        //pr($access_rolename);
                                        //pr($access_role_item);
                                        ?>
                                        <td>

                                            <input class="checkbox_role" type="hidden" id="<?= $access_rolename . "_" . $key ?>"
                                                   name="<?= $access_rolename . "[" . $key . "]" ?>"
                                                   value="<?=$access_role_item[0]['Access_Role']['status']?>"/>
                                            <a class="btn btn-default switch_role <? if ($access_role_item[0]['Access_Role']['status'] == 1) {
                                                echo "btn-success";
                                            } else echo "btn-default";?>"><? if ($access_role_item[0]['Access_Role']['status'] == 1) {
                                                    echo "+";
                                                } else echo "-";?></a>

                                        </td>

                                    <? } ?>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>


                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>