<div class="panel panel-primary panel-colorful media middle pad-all">
    <div class="media-left">
        <div class="pad-hor">
            <i class="fa fa-cogs icon-3x"></i>
        </div>
    </div>
    <div class="media-body">
        <p class="text-2x mar-no text-semibold">Koreana Backend Api</p>
        <p class="mar-no">api для работы с сервисами Koreana</p>
    </div>
</div>
<? echo $this->element('hive_navigate'); ?>
<script>
    $(document).ready(function () {
        $(document).on("click", ".generate_token", function () {

        });
    });
</script>

<div class="col-sm-6">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Токены авторизации API</h3>
        </div>
        <div class="panel-body">
            <p class="text-main text-semibold">Для чего нужны токены</p>
            <p>
                <mark>Токены</mark>
                авторизации необходимы для авторизации в системе Koreana Backend Api и организации доступа к ресурсам сервиса.
                Токен создается при API-авторизации для клиентских приложений, также Api токен можно сгенерировать в
                этом разделе.
                Токен по умолчанию создается сроком на 1 год, при необходимости токен можно уничтожить раньше.
            </p>
        </div>
    </div>
</div>

<script>
    $(document).on('nifty.ready', function () {
        $('.generate_token').niftyOverlay({
            iconClass: 'psi-repeat-2 spin-anim icon-2x'
        }).on('click', function () {
            var $el = $(this);
            $el.niftyOverlay('show');
            var token_field = $(".token_field");
            if ($(this).hasClass("no_action")) {
                return false;
            }
            $(this).addClass("no_action");
            var formData = "";
            $.ajax({
                url: 'hive/generate_token/',
                type: "post",
                dataType: 'json',
                data: formData,
                before: function () {
                    token_field.text("");
                    token_field.hide();
                },
                success: function (response) {
                    $el.niftyOverlay('hide');
                    var token = response.data.token;
                    token_field.text(token);
                    token_field.show();
                },
                error: function (response) {
                    console.log(response);
                }
            });
            $(this).removeClass("no_action");
        });
    });
</script>

<div class="col-sm-6">
    <div class="panel">
        <a class="btn btn-success generate_token" data-toggle="panel-overlay"
           data-target="#panel-collapse-default">Создать Api токен</a>
        <!--Panel body-->
        <div id="panel-collapse-default" class="panel-body">
            <p class="token_field well well-lg text-semibold text-2x"></p>
        </div>
    </div>

</div>

<div class="col-sm-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Активные токены</h3>
        </div>

        <!-- Striped Table -->
        <? if (count($tokens) > 0) { ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>токен</th>
                            <th>создан</th>
                            <th>срок истечения</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($tokens as $token):
                            $token = $token['Token'];
                            ?>
                        <tr>
                            <td><a href="#" class="btn-link"><?=$token['token']?></a></td>
                            <td><?=$token['created']?></td>
                            <td><?=$token['expired']?></td>
                        </tr>
                        <? endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <? } ?>

    </div>
</div>
