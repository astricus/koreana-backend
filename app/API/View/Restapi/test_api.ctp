<div class="panel panel-primary panel-colorful media middle pad-all">
    <div class="media-left">
        <div class="pad-hor">
            <i class="fa fa-cogs icon-3x"></i>
        </div>
    </div>
    <div class="media-body">
        <p class="text-2x mar-no text-semibold">Hive Api</p>
        <p class="mar-no">api для работы с пользователями</p>
    </div>
</div>
<? echo $this->element('hive_navigate'); ?>
<script src="https://cdn.jsdelivr.net/npm/htmlson.js@1.0.4/src/htmlson.js"></script>
<script>
    $(document).on('nifty.ready', function () {
        var panel_color;
        $('.init_request').niftyOverlay({
            iconClass: 'psi-repeat-2 spin-anim icon-2x'
        }).on('click', function (e) {
            if(panel_color!=null) {
                $(".panel_color").removeClass(panel_color);
            }
            e.preventDefault();
            var $el = $(this);
            $el.niftyOverlay('show');
            //var token_field = $(".token_field");
            if ($(this).hasClass("no_action")) {
                return false;
            }
            $(this).addClass("no_action");
            var api_route = $("#api_route").val();
            var api_request = $("#api_request").val();
            var api_token= $("#api_token").val();
            var formData = "api_route=" + api_route + "&api_request=" + api_request + "&api_token=" + api_token;
            $.ajax({
                url: 'hive/test/',
                type: "post",
                dataType: 'json',
                data: formData,
                before: function () {
                },
                success: function (response) {
                    $el.niftyOverlay('hide');
                    var response_data = response.data;

                    $("#api_response").text(response_data.data.response_data);
                    var api_name = response.api;
                    var server_time = response.server_time;
                    var response_status = response.status;
                    var response_content = response_data.data.data;
                    var generate_time = response_data.data.time;
                    var content_length = response_data.data.size;
                    var url = response_data.data.url;
                    var request_data = response_data.data.request_data;
                    console.log(response_data.data);
                    $("#api_name__info").text(api_name);
                    $("#time__info").text(server_time);
                    $("#status__info").text(response_status);
                    $("#content__info").text(response_content);
                    $("#generate_time__info").text(generate_time);
                    $("#length__info").text(content_length);
                    $("#url__info").text(url);
                    $("#request_data__info").text(request_data);

                    $(".api_response_content").show();
                    if(response_status=="error"){
                        panel_color = "panel-danger";
                    } else if(response_status=="success"){
                        panel_color = "panel-success";
                    } else if(response_status=="warning"){
                        // метод не реализован, deprecated
                        panel_color = "panel-warning";
                        //panel-success
                    } else {
                        panel_color = "panel-dark";
                    }
                    $(".panel_color").addClass(panel_color);
                },
                error: function (response) {
                    console.log(response);
                }
            });
            $(this).removeClass("no_action");
            return false;
        });
    })
    ;
</script>

<div class="col-sm-6">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Запрос Api</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" name="test_api" id="test_api_form" method="post" action="/">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="api_route" class="col-sm-2 control-label">Api запрос</label>
                        <div class="col-sm-10">
                            <div class="input-group mar-btm">
                                <span class="input-group-addon">/api/</span>
                                <input type="text" class="form-control" placeholder="Api запрос" id="api_route">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="api_token" class="col-sm-2 control-label">Токен авторизации</label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Токен авторизации" class="form-control" id="api_token">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="api_route" class="col-sm-2 control-label">Данные запроса (request data)</label>
                        <div class="col-sm-10">
                            <textarea rows="8" placeholder="Api данные запроса" class="form-control"
                                      id="api_request"></textarea>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-12 center-block">
                            <button class="btn btn-success init_request" data-toggle="panel-overlay"
                                    data-target="#panel-collapse-default">Отправить
                            </button>
                            <button class="btn btn-warning" type="reset">Очистить</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Ответ Api</h3>
        </div>
        <div class="panel-body" id="panel-collapse-default">
            <div class="api_response_content">
            <textarea rows="6" placeholder="Api ответ" class="form-control" id="api_response"></textarea>


            <div class="row">
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-default panel-colorful">
                        <div class="pad-all">
                            <p class="text-muted">Имя Api: <span id="api_name__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-default panel-colorful">
                        <div class="pad-all">
                            <p class="text-muted">Время на сервере: <span id="time__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-colorful panel_color">
                        <div class="pad-all">
                            <p class="ext-muted">Статус ответа: <span id="status__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-default panel-colorful">
                        <div class="pad-all">
                            <p class="text-muted">Данные ответа: <span id="content__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-default panel-colorful">
                        <div class="pad-all">
                            <p class="text-muted">Время генерации ответа: <span id="generate_time__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-default panel-colorful">
                        <div class="pad-all">
                            <p class="text-muted">Объем возвращенных данных: <span id="length__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-default panel-colorful">
                        <div class="pad-all">
                            <p class="text-muted">Api Url: <span id="url__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-default panel-colorful">
                        <div class="pad-all">
                            <p class="text-muted">Данные запроса API: <span id="request_data__info" class="text-semibold"></span></p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

