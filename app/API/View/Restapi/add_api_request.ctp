<div class="panel panel-primary panel-colorful media middle pad-all">
    <div class="media-left">
        <div class="pad-hor">
            <i class="fa fa-cogs icon-3x"></i>
        </div>
    </div>
    <div class="media-body">
        <p class="text-2x mar-no text-semibold">RestApi Backend Koreana</p>
        <p class="mar-no">api для работы с системой</p>
    </div>
</div>

<p class="text-lg text-main text-bold text-uppercase">Создание API запроса </p>


<div class="panel-body">

    <div id="add_api_request" class=" active">
        <form class="form-horizontal" name="add_api_request" method="post" action="/restapi/request/add" enctype="multipart/form-data">

            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label text-bold" for="api_name">Название API-запроса</label>
                    <div class="col-sm-6">
                        <input type="text" id=api_name class="form-control" name="api_name" required placeholder="Название api-запроса" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label text-bold" for="api_url">Url запроса</label>
                    <div class="col-sm-1">
                        <i class="text-bold text-lg text-muted" >/api/1/</i>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" id=api_url class="form-control" name="api_url" required placeholder="Url запроса" value=""></i>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-bold" for="request_method">Тип запроса</label>
                    <div class="col-sm-6">
                        <select id="request_method" class="select select2 form-control" name="request_method">
                            <?
                            $request_methods = ["GET", "POST"];
                            foreach ($request_methods as $request_method) { ?>
                                <option value="<?= $request_method ?>">
                                    <?= $request_method ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="description">Описание запроса</label>
                    <div class="col-sm-6">
                        <textarea id="description" class="form-control" name="description"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-bold" for="auth_requires">Требуется ли авторизация</label>
                    <div class="col-sm-6">
                        <select id="auth_requires" class="select select2 form-control" name="auth_requires">
                                <option value="no">Нет</option>
                                <option value="yes">Да</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-bold" for="platform">Для какой платформы запрос</label>
                    <div class="col-sm-6">
                        <select id="platform" class="select select2 form-control" name="platform">
                            <?
                            $platforms = ["all", "mobile", "sites"];
                            foreach ($platforms as $platform) { ?>
                                <option value="<?= $platform ?>">
                                    <?= $platform ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="request_data">Данные запроса</label>
                    <div class="col-sm-6">
                        <textarea name="request_data" id="request_data" class="form-control"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label text-bold" for="response_data">Данные ответа</label>
                    <div class="col-sm-6">
                        <textarea name="response_data" id="response_data" class="form-control"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="errors">Ошибки</label>
                    <div class="col-sm-6">
                        <textarea name="errors" id="errors" class="form-control"></textarea>
                    </div>
                </div>


            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success" type="submit">Создать API-запрос</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>