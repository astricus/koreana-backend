<div class="panel panel-primary panel-colorful media middle pad-all">
    <div class="media-left">
        <div class="pad-hor">
            <i class="fa fa-cogs icon-3x"></i>
        </div>
    </div>
    <div class="media-body">
        <p class="text-2x mar-no text-semibold">RestApi Backend Koreana</p>
        <p class="mar-no">api для работы с системой</p>
    </div>
</div>
<? echo $this->element('hive_navigate');
$table_header = "<tr>
                                <td>
                                    URL
                                </td>
                                <td>
                                    Тип запроса
                                </td>
                                <td>
                                    Требуется ли авторизация
                                </td>
                                <td>
                                    Где применим
                                </td>
                                <td>
                                    Данные запроса
                                </td>
                                <td>
                                    Данные ответа
                                </td>
                                <td>
                                    Список ошибок
                                </td>
                            </tr>";
$api_server = $_SERVER['SERVER_NAME'];
$api_server_scheme = $_SERVER['REQUEST_SCHEME'] . "://";
$api_url = $api_server_scheme . $api_server;
$api_requests = [
    [
        "name" => "Тестовый запрос на проверку работоспособности Api",
        "url" => $api_url . "/api/{1}/{platform}/app/test",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "-",
        "response_data" => '[{\"success\"}]',
        "errors" => "200"

    ],
    [
        "name" => "Список регионов",
        "url" => $api_url . "/api/{1}/{platform}/region/list",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "-",
        "response_data" => '[{"success"}, "region_list": regions], где regions - массив вида ["1" =>
                                        "Московская область", "2" => "Ленинградская область", и т.п.]',
        "errors" => "200 в случае успешного запроса, 401 в случае если запрос API не найден, 500 - в
                                    случае ошибки выполнения запроса"
    ],
    [
        "name" => "Список городов в регионе",
        "url" => $api_url . "api/{1}/{platform}/region/{id}/cities",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "-",
        "response_data" => '[{"success"}, "cities": cities], где cities - массив примерного вида ["1" =>
                                    "Москва", "2" => "Липецк", и т.п.]',
        "errors" => "200 в случае успешного запроса, 401 в случае если запрос API не найден, 500 - в
                                    случае ошибки выполнения запроса"
    ],
    [
        "name" => "Список локаций",
        "url" => $api_url . "/api/{1}/{platform}/location/list?region_id={region_id}&location_type={location_type}",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "region_id - опциональный параметр, уточнение региона для выборки локаций, location_type
                                - уточнение типа локаций, одно из значений массива
                                {\"shops\", \"services\", \"salons\", \"pick-ups\"}",
        "response_data" => '[{"success"}, "locations": cities], где cities - массив примерного вида ["1" =>
                                    "Москва", "2" => "Липецк", и т.п.]',
        "errors" => "200 в случае успешного запроса, 401 в случае если запрос API не найден, 500 - в
                                    случае ошибки выполнения запроса"
    ],
    [
        "name" => "Список новостей",
        "url" => $api_url . "/api/{1}/{platform}/new/list?region_id={region_id}",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "region_id - опциональный параметр, уточнение региона для выборки локаций",
        "response_data" => '<code>
                                    [{"success"}, "news": news], где news - массив примерного вида [[
                                    "id" => "1",
                                    "title" => "Название новости",
                                    "preview" => "Анонс новости",
                                    "content" => "Контент новости",
                                    "created" => "Дата создания новости",
                                    "modified" => "Дата обновления новости",
                                    "author_id" => "Автор новости (идентификатор)",
                                    "author_name" => "Автор новости (ФИО)",
                                    и т.п.]
                                </code>',
        "errors" => "200 в случае успешного запроса, 401 в случае если запрос API не найден, 500 - в
                                    случае ошибки выполнения запроса"
    ],
    [
        "name" => "Содержание новости",
        "url" => $api_url . "/api/{1}/{platform}/new/{id}/get",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "-",
        "response_data" => '<code>
                                    [{"success"}, "news": news], где news - массив примерного вида [[
                                    "id" => "1",
                                    "name" => "Название новости",
                                    "preview" => "Анонс новости",
                                    "content" => "Контент новости",
                                    "created" => "Дата создания новости",
                                    "update" => "Дата обновления новости",
                                    "author_id" => "Автор новости (идентификатор)",
                                    "author_name" => "Автор новости (ФИО)",
                                    и т.п.]
                                </code>',
        "errors" => "200 в случае успешного запроса, 401 в случае если запрос API не найден, 500 - в
                                    случае ошибки выполнения запроса"
    ],
    [
        "name" => "Список акций",
        "url" => $api_url . "/api/{1}/{platform}/action/list?region_id={region_id}",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "region_id - опциональный параметр, уточнение региона для выборки локаций",
        "response_data" => '<code>
                                [{"success"}, "actions": actions], где actions - массив примерного вида [[
                                "id" => "1",
                                "name" => "Название акции",
                                "preview" => "Анонс акции",
                                "content" => "Контент акции",
                                "created" => "Дата создания акции",
                                "update" => "Дата обновления акции",
                                "author_id" => "Автор акции (идентификатор)",
                                "author_name" => "Автор акции (ФИО)",
                                и т.п.]
                            </code>',
        "errors" => "200 в случае успешного запроса, 401 в случае если запрос API не найден, 500 - в
                                    случае ошибки выполнения запроса"
    ],
    [
        "name" => "Содержание акции",
        "url" => $api_url . "/api/{1}/{platform}/action/{id}/",
        "method" => "GET",
        "auth_requires" => "no",
        "platform" => "all",
        "request_data" => "-",
        "response_data" => '<code>
                                    [{"success"}, "action": action], где action - массив примерного вида [[
                                    "id" => "1",
                                    "name" => "Название акции",
                                    "preview" => "Анонс акции",
                                    "content" => "Контент акции",
                                    "created" => "Дата создания акции",
                                    "update" => "Дата обновления акции",
                                    "author_id" => "Автор акции (идентификатор)",
                                    "author_name" => "Автор акции (ФИО)",
                                    и т.п.]
                                </code>',
        "errors" => "200 в случае успешного запроса, 401 в случае если запрос API не найден, 500 - в
                                    случае ошибки выполнения запроса"
    ],
];

?>

<div class="row">

    <div class="panel-group accordion">

        <?
        $api_index = 1;
        foreach ($api_requests as $api_request){
            $api_index?>

            <div class="panel panel-bordered panel-info">

                <!--Accordion title-->
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-parent="#api_list" data-toggle="collapse" href="#api_list-<?=$api_index?>"><?=$api_request['name']?></a>
                    </h4>
                </div>

                <!--Accordion content-->
                <div class="panel-collapse collapse" id="api_list-<?=$api_index?>">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped">

                                <?= $table_header ?>
                                <tr>
                                    <td>
                                        <a href="#"><?=$api_request['url']?></a>
                                    </td>
                                    <td>
                                        <div class="label label-table label-success"><?=$api_request['method']?></div>
                                    </td>
                                    <td>
                                        <?=$api_request['auth_requires']?>
                                    </td>
                                    <td>
                                        <?=$api_request['platform']?>
                                    </td>
                                    <td>
                                        <?=$api_request['request_data']?>
                                    </td>
                                    <td>
                                        <code>
                                            [{"success"}]
                                        </code>
                                    </td>
                                    <td>
                                        200
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

        <?$api_index++;}
        ?>

    </div>
</div>