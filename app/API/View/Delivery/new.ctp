<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Создание новой доставки</h3>
        </div>
        <div class="panel-body">
            <form method="post" action="/delivery/add">
                <p>Тип доставки</p>
                <select name="delivery_type" id="demo-chosen-select" tabindex="2">
                    <option value="from_company_to_provider_store">Поставщик - склад ТК</option>
                    <option value="from_store_to_orderer">Склад ТК - Покупатель</option>
                    <option value="from_company_to_orderer">Поставщик - Покупатель</option>
                    <option value="from_orderer_to_provider_store">Покупатель - склад ТК</option>
                    <option value="from_provider_store_to_company">Склад ТК - Поставщик</option>
                    <option value="from_orderer_to_company">Покупатель - Поставщик</option>
                </select>
                <div>
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>
        </div>
    </div>
</div>
