<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Доставка № <?= sprintf("%08d", $delivery['delivery_id']); ?> { <?=$delivery_type[$delivery['delivery_type']]; ?> }</h3>
        </div>
        <?php //pr($delivery); ?>
        <div>
            <a href="#" class="btn btn-success btn-bock">Добавить заказ</a>

            <a href="#" class="btn btn-success btn-bock">На доставку в ТК</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">
                    <div class="panel panel-bordered panel-mint">
                        <div class="panel-heading">
                            <h3 class="panel-title">Статус доставки ( <?= date('d.m.Y H:i:s', strtotime($delivery['status_date'])) ?> )</h3>
                        </div>
                        <div class="panel-body">
                            <div id="getStatusDelivery">
                                <select id="getStatusDeliverySelect" class="chosen-container chosen-container-single chosen-with-drop chosen-container-active" style="width: 200px;">
                                    <?php foreach ($delivery_status as $key => $status): ?>
                                        <option value="<?= $key ?>"
                                            <?php
                                                if($key == $delivery['status']){echo 'selected'; }
                                            ?>>
                                            <?= $status ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <button id="getStatusDeliveryLink" data-order-id="<?= $delivery['delivery_id'] ?>" class="btn btn-mint btn-bock">Изменить статус</button>
                            </div>
                            <div id="setStatusDelivery">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Рассчитать стоимость доставки</h3>
                        </div>
                        <div class="panel-body">
                            <div id="getCostDelivery">
                                <select id="getCostDeliverySelect" class="chosen-container chosen-container-single chosen-with-drop chosen-container-active" style="width: 200px;">
                                    <option value="0">Все ТК</option>
                                    <?php foreach ($provider_active as $provider): ?>
                                    <option value="<?= $provider['Delivery_Provider']['id'] ?>"><?= $provider['Delivery_Provider']['title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <button id="getCostDeliveryLink" data-order-id="<?= $delivery['delivery_id'] ?>" class="btn btn-info btn-bock">Получить цену</button>
                            </div>
                            <div id="setCostDelivery">
                                <?php if($delivery['cost_delivery_all'] != ''): ?>
                                <?php $delivery_cost_all = json_decode($delivery['cost_delivery_all'], JSON_OBJECT_AS_ARRAY); ?>
                                <?php foreach ($delivery_cost_all as $val): ?>
                                    <h5><?= $val['provider_title'] ?></h5>
                                    <p>Полная стоимость: <?= $val['delivery_cost_data']['total_cost'] ?> р.</p>
                                    <p>Время в пути: <?= $val['delivery_cost_data']['period']['min'] ?> - <?= $val['delivery_cost_data']['period']['max'] ?> д.</p>
                                    <p>Подробно:</p>
                                    <ul>
                                        <?php foreach ($val['delivery_cost_data']['detail_cost'] as $v): ?>
                                            <li><?= $v['cost'] ?> р. (<?= $v['name'] ?>)</li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div>
                        <ul class="sortable-list tasklist list-unstyled ui-sortable">
                            <li class="task-warning ui-sortable-handle">
                                <p class="text-bold text-main text-sm bord-btm">Откуда</p>
                                <p class="pad-btm"><b>Адрес забора:</b> <?= $delivery['sender']['address']['city'] ?> ,
                                    <?= $delivery['sender']['address']['street_type']." ".$delivery['sender']['address']['street'] ?> ,
                                    <?= $delivery['sender']['address']['building'] ?> ,
                                    <?= $delivery['sender']['address']['house'] ?>
                                </p>
                                <p class="pad-btm">
                                    <b>Котактные данные:</b> <?= $delivery['sender']['contact']['contact_name'] ?>
                                </p>
                                <p class="pad-btm">
                                    <b>Телефон:</b> <?= $delivery['sender']['contact']['phone'] ?>
                                </p>
                                <p>
                                    <b>Email:</b> <?php echo isset($delivery['sender']['contact']['email'])?$delivery['sender']['contact']['email']:''; ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div>
                        <ul class="sortable-list tasklist list-unstyled ui-sortable">
                            <li class="task-warning ui-sortable-handle">
                                <p class="text-bold text-main text-sm bord-btm">Куда</p>
                                <p class="pad-btm"><b>Адрес доставки:</b> <?= $delivery['recipient']['address']['city'] ?> ,
                                    <?= $delivery['recipient']['address']['street_type']." ".$delivery['recipient']['address']['street'] ?> ,
                                    <?= $delivery['recipient']['address']['building'] ?> ,
                                    <?= $delivery['recipient']['address']['house'] ?>
                                </p>
                                <p class="pad-btm">
                                    <b>Котактные данные:</b> <?= $delivery['recipient']['contact']['contact_name'] ?>
                                </p>
                                <p class="pad-btm">
                                    <b>Телефон:</b> <?= $delivery['recipient']['contact']['phone'] ?>
                                </p>
                                <p>
                                    <b>Email:</b> <?php echo isset($delivery['recipient']['contact']['email'])?$delivery['recipient']['contact']['email']:''; ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div>
                        <ul class="sortable-list tasklist list-unstyled ui-sortable">
                            <li class="task-warning ui-sortable-handle">
                                <p class="text-bold text-main text-sm bord-btm">Данные заказа</p>
                                <p class="pad-btm"><b>Статус:</b> <?= $delivery['status'] ?></p>
                                <p class="pad-btm"><b>Дата:</b> <?= date('d.m.Y H:i',strtotime($delivery['date'])) ?></p>
                                <p class="pad-btm"><b>Стоимость доставки:</b> <?= $delivery['cost_delivery'] ?></p>
                                <p class="pad-btm"><b>Общая стоимость:</b> <?= $delivery['total_price'] ?> руб.</p>
                                <p class="pad-btm"><b>Общий вес:</b> <?= $delivery['weight'] ?> кг.</p>
                                <p class="pad-btm"><b>Количество мест:</b> <?= count($delivery['products']) ?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php $i = 1; ?>
            <?php foreach ($delivery['products'] as $k => $v): ?>
                <div class="panel tasklist">
                    <div class="panel-heading">
                        <h3 class="panel-title">Место № <?= $i ?> для заказа
                            <a href="/order/view/<?= $k ?>" class="btn-link">
                                <?= sprintf("%08d", $k); ?>
                            </a>
                        </h3>
                    </div>

                    <!-- Striped Table -->
                    <!--===================================================-->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Название</th>
                                    <th>Количество</th>
                                    <th>Цена за ед.</th>
                                    <th>Вес</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($v as $product): ?>
                                <tr>
                                    <td>
                                        <a href="/offer/view/<?= $product['id']?>" class="btn-link">
                                            <?= $product['id']?>
                                        </a>
                                    </td>
                                    <td><?= $product['name']?></td>
                                    <td><?= $product['amount']?></td>
                                    <td><?= $product['price']?></td>
                                    <td><?= $product['weight']?></td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->

                </div>
            <?php $i++ ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>

