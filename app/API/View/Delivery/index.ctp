<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список Доставок</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <button id="buttonOpenDelivery" data-target="#modal-new-delivery" data-toggle="modal" class="btn btn-success btn-bock">Создать новую доставку</button>
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr class="">
                        <th>№</th>
                        <th scope="col">СД</th>
                        <th scope="col" class="sorting">Стоимость доставки</th>
                        <th scope="col" class="sorting">Статус</th>
                        <th scope="col" class="sorting">Кол-во мест</th>
                        <th scope="col">Дата создания</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? //pr($deliveries); ?>
                    <?php foreach ($deliveries as $key =>$delivery): ?>
                    <tr>
                        <td>
                            <a href="<?='/delivery/show/'.$key; ?>">
                                <?= sprintf("%08d", $key); ?>
                            </a>
                        </td>
                        <td></td>
                        <td><?=$delivery['cost_delivery'] ;?></td>
                        <td><?=$status[$delivery['status']];?></td>
                        <td><?=count($delivery['products']) ;?></td>
                        <td><?=date('d.m.Y H:i',strtotime($delivery['date']));?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class="fixed-table-pagination">
                <div class="pull-right pagination">
                    <ul class="pagination">
                        <?php /*$ctrl_pgn = "/delivery/";
                        $params = array(
//                                'category_id' => $category_id,
//                                'source' => $source,
                        );
                        echo new_paginator($page, $pages, "", $params, $ctrl_pgn);*/ ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Bootstrap Modal New Delivery-->
<!--===================================================-->
<div class="modal" id="modal-new-delivery" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title">Новая доставка</h4>
            </div>
            <!--Modal body-->
            <div class="modal-body">
                <form method="post" action="/delivery/new">
                    <label>Тип доставки</label>
                    <select id="selectTypeDelivery" name="typeDelivery">
                        <option value="0">Укажите тип доставки</option>
                        <?php foreach($delivery_type as $k => $v):; ?>
                            <option value="<?= $k ?>"><?= $v ?></option>
                        <?php endforeach; ?>
                    </select>
                    <br /><br /><hr />
                    <div id="routeDelivery" class="row">
                        <div class="col-md-6">
                            <p>Откуда</p>
                            <div id="routeDeliveryTo" ></div>
                        </div>
                        <div class="col-md-6">
                            <p>Куда</p>
                            <div id="routeDeliveryFrom"></div>
                        </div>
                    </div>
                </div>
                <!--Modal footer-->
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Закрыть</button>
                    <input type="submit" id="newDelivery" class="btn btn-primary" value="Создать новую доставку" disabled />
                </div>
            </form>
        </div>
    </div>
</div>
<!--===================================================-->