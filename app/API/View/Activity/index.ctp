<h4>Активность в системе </h4>

<script>
    $(document).ready(function () {
        /* Компонент  */
        $(document).on("change", ".component_select", function () {
            let component = $('.component_select option:selected').val(), new_url;
            if (component !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'component', component);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'component', '');
            }
            redirect(new_url);
        });

        /* Администратор */
        $(document).on("change", ".admin_select", function () {
            let admin_id = $('.admin_select option:selected').val(), new_url;
            if (admin_id != 0) {
                new_url = updateQueryStringParameter(window.location.href, 'admin_id', admin_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'admin_id', '');
            }
            redirect(new_url);
        });

        $(document).on("change", ".select_date", function () {
            let date = $('.select_date').val(), new_url;
            if (date != "") {
                new_url = updateQueryStringParameter(window.location.href, 'date', date);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'date', '');
            }
            redirect(new_url);
        });


    });

</script>

<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-body">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/admin/activity" class="btn btn-success">показать все</a>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="component" class="main_select use_select2 component_select" name="component">

                            <option value="0">Все компоненты</option>
                            <? foreach ($components as $ck => $cv) {
                                ?>
                                <option value="<?= $ck ?>" <? if ($ck == $form_data['component']) echo 'selected'; ?>>
                                    <?= $cv ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 admin_select" name="city_id">
                            <option value="0">Все администраторы</option>
                            <? foreach ($admins as $admin) {

                                $admin_id = $admin['Manager']['id'];
                                $admin_name = prepare_fio($admin['Manager']['firstname'], $admin['Manager']['lastname'], "");
                                ?>
                                <option value="<?= $admin_id ?>" <? if ($admin_id == $form_data['admin_id']) echo 'selected'; ?>>
                                    <?= $admin_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left">
                        <label class="control-label" for="datetime">Дата события</label>
                        <input type="date" id="datetime" name="datetime" class="select_date" value="<?=$form_data['date']?>"/>
                    </div>

                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="min-w-td">#</th>
                            <th>Компонент</th>
                            <th>Описание</th>
                            <th>Администратор</th>
                            <th class="text-center">Дата</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?
                        $act = 0;

                        foreach ($actions as $action_item) {
                            $action = $action_item['Activities'];
                            $admin = $action_item['Manager'];
                            $action['comment'] = str_replace("[", '<span class="action_item text-bold">', $action['comment']);
                            $action['comment'] = str_replace("]", "</span>", $action['comment']);
                            $act++;?>
                            <tr>
                                <td class="min-w-td"><?=$act?></td>
                                <td><?= $action['name'] ?></td>
                                <td><?= $action['comment'] ?></td>
                                <td><a class="btn-link" href="/admin/view/<?= $action['manager_id'] ?>"><?= $admin['firstname'] . " " .  $admin['lastname'] ?></a></td>
                                <td><?= $action['created'] ?> <small class="text-muted">(<?= $action['created_ago'] ?>)</small></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                    <hr>
                    <div class="fixed-table-pagination">
                        <div class="pull-right pagination">
                            <ul class="pagination action_item">
                                <?php $ctrl_pgn = "/activity/index";
                                $params = array(
                                );
                                echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>