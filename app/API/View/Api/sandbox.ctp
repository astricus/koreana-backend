<h1>Песочница API</h1>
<p>Здесь будет расположено нечто вроде мастера для начала работы с приложением, по каким разделам и что смотреть, как
    начать торговлю, как загрузить товары, где смотреть заказы, где смотреть посещаемость и прочее...</p>


<? echo $this->element('api_breadcrumbs'); ?>
<? echo $this->element('api_navigate'); ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-warning panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="demo-pli-file-word icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">241</p>
                <p class="mar-no">Documents</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-info panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="demo-pli-file-zip icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">241</p>
                <p class="mar-no">Zip Files</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-mint panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="demo-pli-camera-2 icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">241</p>
                <p class="mar-no">Photos</p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-danger panel-colorful media middle pad-all">
            <div class="media-left">
                <div class="pad-hor">
                    <i class="demo-pli-video icon-3x"></i>
                </div>
            </div>
            <div class="media-body">
                <p class="text-2x mar-no text-semibold">241</p>
                <p class="mar-no">Videos</p>
            </div>
        </div>
    </div>

</div>