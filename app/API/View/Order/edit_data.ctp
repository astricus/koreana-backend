<div class="panel-body">
    <div class="tab-base">

        <!--Nav Tabs-->
        <ul class="nav nav-tabs nav-justified">
            <li class="">
                <a data-toggle="tab" href="#order_info" aria-expanded="true">
                    <img alt="order_comment mini_icon" src="https://image.flaticon.com/icons/svg/1169/1169924.svg"
                         width="40px "/> Информация о заказчике
                </a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#order_products" aria-expanded="false">
                    <img alt="order_list mini_icon" src="https://image.flaticon.com/icons/svg/1205/1205767.svg"
                         width="40px "/> Состав заказа</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#order_delivery" aria-expanded="false">
                    <img alt="delivery_icon mini_icon" src="<?= site_url() ?>/images/lkp/delivery.svg" height="40px "/>
                    Доставка</a>

            </li>
            <li class="">
                <a data-toggle="tab" href="#order_events" aria-expanded="false">
                    <img alt="order_events mini_icon" src="<?= site_url() ?>/images/lkp/list.svg" width="40px "/>
                    История заказа</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#order_comment" aria-expanded="false">
                    <img alt="order_comment mini_icon" src="https://image.flaticon.com/icons/svg/134/134891.svg"
                         width="40px "/> Комментарии к
                    заказу</a>
            </li>
        </ul>

        <!--Tabs Content-->
        <div class="tab-content">
            <div id="order_info" class="tab-pane fade active in">
                <div class="">
                    <form class="form-horizontal" name="edit_order" method="post"
                          action="/order/edit_data/<?= $client_order_id ?>"
                          enctype="multipart/form-data">

                        <div class="panel-body">

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="order_firstname">Имя заказчика</label>
                                <div class="col-sm-6">
                                    <input type="text" id=order_firstname class="form-control" name="order_firstname"
                                           required
                                           placeholder="Имя заказчика "
                                           value="<?= $order['Client_Order']['order_firstname'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="order_firstname">Фамилия заказчика</label>
                                <div class="col-sm-6">
                                    <input type="text" id=order_lastname class="form-control" name="order_lastname"
                                           required
                                           placeholder="Фамилия заказчика "
                                           value="<?= $order['Client_Order']['order_lastname'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="order_firstname">Отчество заказчика</label>
                                <div class="col-sm-6">
                                    <input type="text" id=order_middlename class="form-control" name="order_middlename"
                                           required
                                           placeholder="Отчество заказчика "
                                           value="<?= $order['Client_Order']['order_middlename'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="order_email">Почта заказчика</label>
                                <div class="col-sm-6">
                                    <input type="text" id=order_email class="form-control" name="order_email" required
                                           placeholder="Почта заказчика"
                                           value="<?= $order['Client_Order']['order_email'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="order_phone">телефон заказчика</label>
                                <div class="col-sm-6">
                                    <input type="text" id=order_phone class="form-control" name="order_phone" required
                                           placeholder="телефон заказчика"
                                           value="<?= $order['Client_Order']['order_phone'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="order_email">Адрес</label>
                                <div class="col-sm-4">

                                    <input type="text" id=order_street class="form-control" name="order_street" required
                                           placeholder="Улица" value="<?= $order['Street']['name'] ?>">
                                </div>
                                <div class="col-sm-1">
                                    <input type="text" id=order_building class="form-control" name="order_building"
                                           required
                                           placeholder="Здание" value="<?= $order['Address']['building'] ?>">
                                </div>
                                <div class="col-sm-1">
                                    <input type="text" id=order_building class="form-control" name="order_building"
                                           required
                                           placeholder="Квартира/Офис" value="<?= $order['Address']['house'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="city_id">Город</label>
                                <div class="col-sm-6 dropdown">
                                    <select id="city_id" class="city_select use_select2" name="city_id">

                                        <?
                                        foreach ($cities as $city) { ?>
                                            <option value="<?= $city['City']['id'] ?>" <? if ($order['Client_Order']['order_city'] == $city['City']['id']) echo 'selected'; ?>>
                                                <?= $city['City']['name'] ?>
                                            </option>
                                        <? } ?>

                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button class="btn btn-success" type="submit">Сохранить заказ</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="order_products" class="tab-pane fade">
                <? if (count($order_products_list) == 0) { ?>
                    <div class="well">Внимание! В заказе отсутствуют товары!</div>
                <? } else { ?>


                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Наименование</th>
                                <th>Кол-во</th>
                                <th>Цена в заказе</th>
                                <th>Цена в магазине</th>
                                <th>% стоимости от базовой цены</th>
                                <th>Управление</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?
                            $c = 0;
                            pr($order_products_list);
                            foreach ($order_products_list as $order_product) {
                                $c++;
                                $order_price = $order_product['order_price'];
                                $base_price = $order_product['base_price'];
                                $price_div = sprintf("%.2f", (($order_price - $base_price) / $base_price) * 100);//sprintf("%.2f",($order_price - $base_price)/100);
                                if ($price_div > 0) {
                                    $price_class = "success";
                                } else if ($price_div < 0) {
                                    $price_class = "danger";
                                } else {
                                    $price_class = "default";
                                }
                                ?>
                                <tr>

                                    <td class="text-center"><?= $c ?></td>
                                    <td><a data-position_id="<?= $order_product['id'] ?>"
                                           href="/product/<?= $order_product['product_id'] ?>"
                                           class="btn-link removed_product_item product_name"><?= $order_product['product_name'] ?></a>
                                    </td>
                                    <td><span class="label label-purple"><?= $order_product['amount'] ?></span></td>
                                    <td><span class="label label-purple"><?= $order_product['order_price'] ?></span>
                                    </td>
                                    <td><span class="label label-purple"><?= $order_product['base_price'] ?></span></td>
                                    <td><span class="text-<?= $price_class ?> text-semibold"><?= $price_div ?>%</span>
                                    </td>
                                    <td><span class="">
                                        <a data-position_id="<?= $order_product['id'] ?>"
                                           href="/order/<?= $order_id ?>/product/remove/<?= $order_product['product_id'] ?>"
                                           class="btn-link remove_or_return_product_position " title="удалить позицию">
                                            <span class="fa fa-trash position_status"></span>
                                        </a>
                                    </td>
                                </tr>

                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                <? } ?>
            </div>
            <div id="order_delivery" class="tab-pane fade">
                <div class="well">
                    <input id="sw-sz-lg" type="checkbox" checked class="inline"/>
                    <div class="delivery_status_accept_text inline" style="display: none">Клиент заказал доставку товара
                        до дома
                    </div>
                    <div class="delivery_status_reject_text inline">Клиенту доставка не нужна</div>

                </div>
            </div>
            <div id="order_events" class="tab-pane fade">

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center min-w-td">#</th>
                            <th>Событие</th>
                            <th>Инициатор</th>
                            <th>Дата</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?
                        $c = 0;
                        foreach ($order_events as $order_event_data) {
                                $c++;
                            $order_event = $order_event_data['Order_Event'];
                            $name = $order_event['event_name'];
                            $time_ago = $order_event_data[0]['time_ago'];
                            $event_data = unserialize($order_event['event_data']);


                            $action_agent_type = $order_event['action_agent_type'];
                            if ($action_agent_type == "orderer") {
                                $action_agent_title = "Покупатель";
                            } else if ($action_agent_type == "company") {
                                $action_agent_title = "Поставщик";
                            } else if ($action_agent_type == "admin") {
                                $action_agent_title = "Менеджер";
                            } else if ($action_agent_type == "provider") {
                                $action_agent_title = "Оператор доставки";
                            }


                            //$DateTime->day_separator();
                            $event_title = $order_event_names[$name];
                            $action_agent_id = $order_event['action_agent_id'];
                            ?>
                            <tr>

                                <td class="text-center"><?= $c ?></td>

                                <td>
                                    <?=$event_title?> <?=print_r($event_data, true)?>
                                </td>
                                <td>
                                    <a href="/<?= $action_agent_type ?>/<?= $action_agent_id ?>" class="btn-link"><span
                                                class="label label-default"><?= $action_agent_title ?></span></a>
                                </td>
                                <td>
                                    <span class="label label-purple"><?=  $DateTime->day_separator($time_ago) ?></span>
                                </td>
                            </tr>

                        <? } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div id="order_comment" class="tab-pane fade">
                <form class="form-horizontal" name="add_comment" method="post"
                      action="/order/add_comment/<?= $order_id ?>"
                      enctype="multipart/form-data">

                    <div class="list-group bg-trans">
                        <? foreach ($order_comments as $comment) {
                            $comment = $comment['Order_Comment'];
                            $c_id = $comment['id'];
                            $c_text = $comment['comment'];
                            $c_date = $comment['modified'];
                            ?>
                            <div href="#" class="list-group-item">
                                <div class="media-left pos-rel">
                                    <img class="img-circle img-xs" src="img/profile-photos/9.png" alt="Profile Picture">
                                    <i class="badge badge-danger badge-stat badge-icon pull-left"></i>

                                </div>
                                <div class="media-body">
                                    <p class="mar-no text-main">Администратор магазина</p>
                                    <small><?= lang_calendar($c_date) ?></small>
                                    <p class="pad-top text-sm"><?= $c_text ?>.</p>
                                </div>
                                <div class="media-right pos-rel">
                                    <a href="/order/delete_comment/<?= $order_id ?>/<?= $c_id ?>">удалить</a>
                                </div>
                            </div>
                        <? } ?>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <textarea placeholder="комментарий" id=order_comment name="comment"
                                              class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success" type="submit">Сохранить комментарий</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>