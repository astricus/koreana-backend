<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список заказов</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-purple" href="/order/stat/"><i class="pli-statistic"></i>
                            Аналитика заказов</a>
                    </div>
                </div>

                <div class="row">

                    <? if($client_order_id){?>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/orders/" class="btn btn-success">показать все заказы</a>
                    </div>

                    <?}?>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="orderer_id" class="main_select use_select2 dropdownOrdererSearch" name="orderer_id"
                                style="width: 300px">
                            <option value="0">Все заказчики</option>
                            <? foreach ($orderers_list as $orderer) {
                                $orderer = $orderer['Orderer'];
                                $orderer_id = $orderer['id'];
                                $orderer_name = prepare_fio($orderer['firstname'], $orderer['lastname'], "");
                                ?>
                                <option value="<?= $orderer_id ?>" <? if ($orderer_id == $form_data['orderer_id']) echo 'selected'; ?>>
                                    <?= $orderer_name ?></option>
                            <? } ?>
                        </select>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 dropdownOrdererCitySearch" name="city_id"
                                style="width: 300px">
                            <option value="0">Все города</option>
                            <? foreach ($orderers_city_list as $order_city) {
                                $order_city = $order_city['City'];
                                $city_id = $order_city['id'];
                                $city_name = $order_city['name'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) echo 'selected'; ?>>
                                    <?= $city_name ?></option>
                            <? } ?>
                        </select>
                    </div>


                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="company_id" class="main_select use_select2 dropdownCompanySearch" name="company_id"
                                style="width: 300px">
                            <option value="0">Все компании</option>
                            <option value="1">Петрович</option>
                            <option value="2">Леруа Мерлен</option>
                            <option value="3">Максидом</option>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="status" class="main_select use_select2 dropdownOrderStatus" name="status"
                                style="width: 300px">
                            <option value="">Все заказы</option>
                            <? foreach ($order_status_list as $key => $name) {
                                ?>
                                <option value="<?= $key ?>" <? if ($key == $form_data['status']) echo 'selected'; ?>>
                                    <?= $name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                </div>
            </div>

            <hr>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th># заказа</th>
                        <th># cоставного заказа</th>
                        <th>Магазин</th>
                        <th>Заказчик</th>
                        <th>Создан</th>
                        <th>Город</th>
                        <th>Наименований</th>
                        <th>Сумма заказа</th>
                        <th>Телефон</th>
                        <th>Статус</th>
                        <th>Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? $n = 0;
                    $shop_status_list = array();
                    $shop_status_list['new'] = array('default', 'новый заказ');
                    $shop_status_list['complete'] = array('success', 'отгружен');
                    $shop_status_list['confirm'] = array('warning', 'подтвержден');
                    $shop_status_list['proceed'] = array('warning', 'в обработке (собирается)');
                    $shop_status_list['collected'] = array('warning', 'заказ собран и готов к отгрузке');
                    $shop_status_list['abort_by_shop'] = array('danger', 'отклонен магазином');
                    $shop_status_list['abort_by_orderer'] = array('danger', 'отклонен пользователем');
                    $shop_status_list['pickup'] = array('success', 'заказ забран со склада поставщика (в доставке)');
                    $shop_status_list['partial_confirm'] = array('warning', 'частично подтвержден');
                    $shop_status_list['problem'] = array('danger', 'возникла проблема');

                    foreach ($orders as $order) {
                        //pr($order);
                        $client_order = $order['Client_Order'];
                        $order_data = $order['Order'];
                        $order_address = $order['Address'];
                        $order_street = $order['Street'] ?? null;
                        //подзаказ
                        $shop_data = $order['Company'];
                        $orderer_data = $order['Orderer'];

                        $product_count = $order['product_count'];
                        $order_total_cost = $order['order_total_cost'];
                        $order_city = $order['City'];

                        $last_order_id = rand(250, 450);
                        $n++;
                        $shop_status = $shop_status_list[$order_data['status']];
                        $blocked_shop = ($order_data['status'] == "blocked") ? "blocked_shop" : "";
                        ?>
                        <tr class="<?= $blocked_shop ?>">
                            <td><?= $n ?></td>
                            <td><a class="btn-link" href="/order/view/<?= $order_data['id'] ?>">заказ №<?= $order_data['id'] ?></a></td>
                            <td><a class="btn-link" href="/orders/?client_order_id=<?= $client_order['id'] ?>">
                                    пользовательский заказ №<?= $client_order['id'] ?></a></td>
                            <td>
                                <a class="btn-link" href="/company/<?= $shop_data['id'] ?>">
                                    <span style="text-transform: uppercase">
                                        <?= $shop_data['company_name'] ?></span></a>, <p
                                        class="text-success"><span
                                            class=" text-bold">г. <?= $order_city['name'] ?></span>,
                                    <? /* $order_data['order_street'] ?>, <?= $order_data['order_building'] ?>,
                                    <?= $order_data['order_house'] */ ?>
                                </p></td>
                            <td>
                                <a class="btn-link" href="/orderer/<?= $orderer_data['id'] ?>">
                                    <?= prepare_fio($orderer_data['firstname'], $orderer_data['lastname'], "") ?>
                                </a>
                            </td>
                            <td><span class="text-info"><i
                                            class="pli-clock"></i> <?= lang_calendar($order_data['created']) ?>
                                </span>
                            </td>
                            <td><?= $order_city['name'] ?></td>
                            <td><?= $product_count ?></td>
                            <td><?= $order_total_cost ?></td>
                            <td><?= $client_order['order_phone'] ?></td>
                            <td>
                                <div class="label label-<?= $shop_status[0] ?>"><?= $shop_status[1] ?></div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-default btn-hover-success pli-gear add-tooltip"
                                       href="/order/edit/<?= $order_data['id'] ?>"
                                       data-original-title="Редактировать заказ" data-container="body"></a>
                                    &nbsp;
                                    <a class="btn btn-sm btn-info btn-hover-info pli-envelope add-tooltip"
                                       href="/order/comments/<?= $order_data['id'] ?>"
                                       data-original-title="комментарии к заказу" data-container="body"></a>

                                    <? if ($order_data['status'] == "init" OR $order_data['status'] == "stopped") { ?>
                                        <a class="btn btn-sm btn-success btn-hover-warning pli-unlock add-tooltip"
                                           href="/shop/unblock/<?= $shop_data['id'] ?>"
                                           data-original-title="Запустить магазин" data-container="body"></a>
                                    <? } else if ($order_data['status'] == "active") { ?>
                                        <a class="btn btn-sm btn-danger btn-hover-success pli-lock add-tooltip"
                                           href="/shop/block/<?= $order_data['id'] ?>"
                                           data-original-title="Приостановить магазин" data-container="body"></a>
                                    <? } else if ($order_data['status'] == "blocked") { ?>

                                    <? } ?>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>

            <div class="fixed-table-pagination">
                <div class="pull-right pagination">
                    <ul class="pagination">
                        <?php $ctrl_pgn = "/orders/";
                        $params = array(
//                                'category_id' => $category_id,
//                                'source' => $source,
                        );
                        echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>