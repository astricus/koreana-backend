<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $title ?> </h3>
        </div>
        <div class="panel-body">

            <div class="tab-base">

                <!--Nav Tabs-->
                <ul class="nav nav-tabs nav-justified">
                    <li class="">
                        <a data-toggle="tab" href="#order_info" aria-expanded="true">
                            <img alt="order_comment mini_icon"
                                 src="https://image.flaticon.com/icons/svg/1169/1169924.svg"
                                 width="40px "/> Заказчик
                        </a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#order_products" aria-expanded="false">
                            <img alt="order_list mini_icon" src="https://image.flaticon.com/icons/svg/1205/1205767.svg"
                                 width="40px "/> Состав заказа</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#order_control" aria-expanded="false">
                            <img alt="order_control mini_icon" src="<?= site_url() ?>/images/lkp/settings.svg"
                                 width="40px "/> Управление</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#order_delivery" aria-expanded="false">
                            <img alt="delivery_icon mini_icon" src="<?= site_url() ?>/images/lkp/delivery.svg"
                                 height="40px "/>
                            Доставка</a>

                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#order_events" aria-expanded="false">
                            <img alt="order_events mini_icon" src="<?= site_url() ?>/images/lkp/list.svg"
                                 width="40px "/>
                            История заказа</a>
                    </li>

                    <li class="">
                        <a data-toggle="tab" href="#order_comment" aria-expanded="false">
                            <img alt="order_comment mini_icon" src="https://image.flaticon.com/icons/svg/134/134891.svg"
                                 width="40px "/> Комментарии к
                            заказу</a>
                    </li>
                </ul>

                <!--Tabs Content-->
                <div class="tab-content">
                    <div id="order_info" class="tab-pane fade active in">
                        <div class="">

                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_firstname">Номер пользовательского
                                        заказа</label>
                                    <div class="col-sm-6">
                                        <span id=order_id> <?= $order['Client_Order']['id'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_firstname">Имя заказчика</label>
                                    <div class="col-sm-6">
                                        <span id=order_firstname> <?= $order['Client_Order']['order_firstname'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_firstname">Фамилия
                                        заказчика</label>
                                    <div class="col-sm-6">
                                        <span id=order_lastname> <?= $order['Client_Order']['order_lastname'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_middlename">Отчество
                                        заказчика</label>
                                    <div class="col-sm-6">
                                        <span id=order_middlename> <?= $order['Client_Order']['order_middlename'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_email">Почта </label>
                                    <div class="col-sm-6">
                                        <span id=order_email
                                              class="text-bold"> <?= $order['Client_Order']['order_email'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_phone">телефон </label>
                                    <div class="col-sm-6">
                                        <span id=order_phone> <?= $order['Client_Order']['order_phone'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="city_id">Город </label>
                                    <div class="col-sm-6">
                                        <?= $order['City']['name'];?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_email">Улица (адрес
                                        доставки)</label>
                                    <div class="col-sm-3">
                                        <span id=order_street> <?= $order['Street']['name'] . " " . $order['Street']['type'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_building">Номер здания (адрес
                                        доставки)</label>
                                    <div class="col-sm-2">
                                        <span id=order_building> <?= $order['Address']['building'] ?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="order_house">Номер квартиры / офиса
                                        (адрес доставки)</label>

                                    <div class="col-sm-2">
                                        <span id=order_house> <?= $order['Address']['house'] ?></span>
                                    </div>
                                </div>

                            </div>

                            <div class="panel-footer">
                                <div class="row">
                                    <a class="btn btn-success" href="/order/edit_data/<?= $order['Order']['id'] ?>"
                                       type="submit"><span class="fa fa-edit"></span> Изменить данные заказа</a>

                                    <a class="btn btn-success" href="/orderer/<?= $order['Orderer']['id'] ?>" target="_blank"
                                       type="submit"><span class="fa fa-user"></span> Перейти к заказачику</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="order_products" class="tab-pane fade">
                        <? if (count($order_products_list) == 0) { ?>
                            <div class="well">Внимание! В заказе отсутствуют товары!</div>
                        <? } else { ?>


                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Наименование</th>
                                        <th>Кол-во</th>
                                        <th>Цена в заказе</th>
                                        <th>Цена в магазине</th>
                                        <th>% стоимости от базовой цены</th>
                                        <th>Управление</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?
                                    $c = 0;
                                    foreach ($order_products_list as $order_product) {
                                        $c++;
                                        $position_status = $order_product['position_status'];
                                        $order_price = $order_product['order_price'];
                                        $base_price = $order_product['base_price'];
                                        $price_div = sprintf("%.2f", (($order_price - $base_price) / $base_price) * 100);//sprintf("%.2f",($order_price - $base_price)/100);
                                        if ($price_div > 0) {
                                            $price_class = "success";
                                        } else if ($price_div < 0) {
                                            $price_class = "danger";
                                        } else {
                                            $price_class = "default";
                                        }
                                        ?>
                                        <tr>

                                            <td class="text-center"><?= $c ?></td>
                                            <td><a data-position_id="<?= $order_product['id'] ?>"
                                                   href="/product/<?= $order_product['product_id'] ?>"
                                                   class="btn-link <? if ($position_status == "ignore") { ?>removed_product_item<? } ?> product_name"><?= $order_product['product_name'] ?></a>
                                            <td>
                                                <input type="text" style="width: 60px" class="form-control" name="amount" data-position_id="<?= $order_product['id'] ?>" value="<?= $order_product['amount'] ?>"/>
                                            </td>
                                            <td>
                                                <span class="label label-purple"><?= $order_product['order_price'] ?></span>
                                            </td>
                                            <td>
                                                <span class="label label-purple"><?= $order_product['base_price'] ?></span>
                                            </td>
                                            <td>
                                                <span class="text-<?= $price_class ?> text-semibold"><?= $price_div ?>%</span>
                                            </td>
                                            <td><span class="">
                                                    <? if ($position_status == "active") { ?>
                                                    <a data-position_id="<?= $order_product['id'] ?>"
                                                       href="/order/<?= $order_id ?>/product/remove/<?= $order_product['product_id'] ?>"
                                                       class="btn-link remove_or_return_product_position "
                                                       title="удалить позицию">
                                                            <span class="fa fa-trash position_status"></span>
                                                        </a><? } else { ?>

                                                        <a data-position_id="<?= $order_product['id'] ?>"
                                                           href="/order/<?= $order_id ?>/product/restore/<?= $order_product['product_id'] ?>"
                                                           class="btn-link remove_or_return_product_position"
                                                           title="вернуть позицию">
                                                            <span class="fa fa-reply position_status"></span>
                                                            </a>

                                                    <? } ?>

                                            </td>
                                        </tr>

                                    <? } ?>
                                    </tbody>
                                </table>

                                <hr class="hr-xs"/>

                                <br>
                                <div class="col-sm-2">
                                    <div class="panel panel-success panel-colorful">
                                        <div class="pad-all">
                                            <p class="text-lg text-semibold"><i
                                                        class="demo-pli-basket-coins icon-fw"></i> Сумма заказа</p>
                                            <p class="mar-no">
                                                <span class="pull-right text-bold">1829 рублей</span>  итого
                                            </p>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        <? } ?>
                    </div>
                    <div id="order_delivery" class="tab-pane fade">
                        <? if (count($order_products_list) == 0) { ?>
                            <div class="well">Внимание! В заказе отсутствуют товары!</div>
                        <? } else { ?>
                            <form method="post" action="/order/changeDelivery/changeDeliveryFromPosition" class="table-responsive">
                                <input type="hidden" name="order_id" value="<?= $order_id ?>"/>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Наименование</th>
                                            <th>Склад отправления</th>
                                            <th>Добавить в доставку</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?
                                    $c = 0;
                                    foreach ($order_products_list as $order_product) {
                                        $c++;
                                        ?>
                                        <tr>

                                            <td class="text-center"><?= $c ?></td>
                                            <td><a data-position_id="<?= $order_product['id'] ?>"
                                                   href="/product/<?= $order_product['product_id'] ?>"
                                                   class="btn-link <? if ($position_status == "ignore") { ?>removed_product_item<? } ?> product_name"
                                                    title="<?= $order_product['product_name'] ?>"><?php echo  substr($order_product['product_name'],0,100); ?></a>
                                            </td>
                                            <td>
                                                <?= $order_product['store_name']." - ".$order_product['store_address'] ?>
                                            </td>
                                            <td>
                                                <?php if($order_product['position_status'] == 'confirm'): ?>
                                                    <select name="pos_id[<?php echo $order_product['id']; ?>]">
                                                        <?php if ($order_product['delivery_id'] > 0): ?>
                                                            <option value="0">Отвязать от доставки</option>
                                                        <?php else: ?>
                                                            <option value="0">Не прикреплен к доставке</option>
                                                        <?php endif; ?>
                                                    <?php foreach($deliveries as $v):?>
                                                        <?php if($order_product['delivery_id'] == $v['Delivery']['id']){$selected = 'selected';}else{$selected = '';} ?>
                                                        <option value="<?php echo $v['Delivery']['id']; ?>" <?php echo $selected; ?>><?php echo $v['Delivery']['id']." - ".$delivery_type[$v['Delivery']['delivery_type']];  ?></option>
                                                    <?php endforeach; ?>
                                                    </select>
                                                <?php else: ?>
                                                    Не подходящий статус позиции- "<?= $order_product['position_status'] ?>"
                                                <?php endif; ?>
                                            </td>
                                        </tr>

                                    <? } ?>
                                    </tbody>
                                </table>

                                <hr class="hr-xs"/>

                                <br>
                                <div class="col-sm-2">
                                    <div class="panel panel-success panel-colorful">
                                        <div class="pad-all">
                                            <p class="text-lg text-semibold"><i
                                                    class="demo-pli-basket-coins icon-fw"></i> Сумма заказа</p>
                                            <p class="mar-no">
                                                <span class="pull-right text-bold">1829 рублей</span>  итого
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="panel panel-info panel-colorful">
                                        <div class="pad-all">
                                            <p class="text-lg text-semibold"><i
                                                    class="demo-pli-basket-coins icon-fw"></i> Обновить данные доставки</p>
                                            <p class="mar-no">
                                                <input class="btn btn-purple" type="submit" value="Обновить">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="panel panel-warning panel-colorful">
                                        <div class="pad-all">
                                            <p class="text-lg text-semibold"><i
                                                    class="demo-pli-basket-coins icon-fw"></i> Создать новую доставку</p>
                                            <p class="mar-no">
                                                <input id="buttonOpenDelivery" data-target="#modal-new-delivery" data-toggle="modal"  class="btn btn-pink" value="Создать">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </div>
                        <? } ?>
                    </div>
                    <!--Bootstrap Modal New Delivery-->
                    <!--===================================================-->
                    <div class="modal" id="modal-new-delivery" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!--Modal header-->
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                                    <h4 class="modal-title">Новая доставка</h4>
                                </div>
                                <!--Modal body-->
                                <div class="modal-body">
                                    <form method="post" action="/delivery/new">
                                        <label>Тип доставки</label>
                                        <select id="selectTypeDelivery" name="typeDelivery">
                                            <option value="0">Укажите тип доставки</option>
                                            <?php foreach($delivery_type as $k => $v):; ?>
                                                <option value="<?= $k ?>"><?= $v ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <br /><br /><hr />
                                        <div id="routeDelivery" class="row">
                                            <div class="col-md-6">
                                                <p>Откуда</p>
                                                <div id="routeDeliveryTo" ></div>
                                            </div>
                                            <div class="col-md-6">
                                                <p>Куда</p>
                                                <div id="routeDeliveryFrom"></div>
                                            </div>
                                        </div>
                                </div>
                                <!--Modal footer-->
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Закрыть</button>
                                    <input type="submit" id="newDelivery" class="btn btn-primary" value="Создать новую доставку" disabled />
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--===================================================-->
                    <div id="order_events" class="tab-pane fade">

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center min-w-td">#</th>
                                    <th>Событие</th>
                                    <th>Инициатор</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $c = 0;
                                foreach ($order_events as $order_event_data) {
                                    $c++;
                                    $order_event = $order_event_data['Order_Event'];
                                    $name = $order_event['event_name'];
                                    $time_ago = $order_event_data[0]['time_ago'];
                                    $event_data = unserialize($order_event['event_data']);


                                    $action_agent_type = $order_event['action_agent_type'];
                                    if ($action_agent_type == "orderer") {
                                        $action_agent_title = "Покупатель";
                                    } else if ($action_agent_type == "company") {
                                        $action_agent_title = "Поставщик";
                                    } else if ($action_agent_type == "admin") {
                                        $action_agent_title = "Менеджер";
                                    } else if ($action_agent_type == "provider") {
                                        $action_agent_title = "Оператор доставки";
                                    }


                                    //$DateTime->day_separator();
                                    $event_title = $order_event_names[$name];
                                    $event_type = $order_event_types[$name];
                                    $action_agent_id = $order_event['action_agent_id'];
                                    ?>
                                    <tr>

                                        <td class="text-center"><?= $c ?></td>

                                        <td>
                                            <span style="font-size: 12px; line-height: 20px"
                                                  class="<?= $event_type ?>"><?= $event_title ?></span> <?= print_r($event_data, true) ?>
                                        </td>
                                        <td>
                                            <a href="/<?= $action_agent_type ?>/<?= $action_agent_id ?>"
                                               class="btn-link"><span
                                                        class="label label-default"><?= $action_agent_title ?></span></a>
                                        </td>
                                        <td width="150px">
                                            <span class="label label-purple"><?= $DateTime->day_separator($time_ago) ?> назад</span>
                                        </td>
                                    </tr>

                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div id="order_control" class="tab-pane fade">

                        <div class="well well-lg">
                            Статус заказа:
                            <strong>Заказ собирается поставщиком</strong>

                            Статус оплаты:
                            <strong><?
                                if($order['Order']['payment_status'] == "true" )
                                echo "Заказ оплачен";
                                else {
                                    echo "Заказ не оплачен";
                                }
                            ?> </strong>
                        </div>


                        <a class="btn  btn-default btn-hover-success" href="/order/orderPaidSuccess/?order_id=<?= $order_id ?>">Заказ успешно оплачен</a>

                        <a class="btn  btn-danger btn-hover-danger" href="/order/stop/<?= $order_id ?>"><span
                                    class="fa fa-stop-circle"></span> Заказчик отменил заказ</a>

                        <a class="btn  btn-info btn-hover-info" href="/order/change_address/<?= $order_id ?>"><span
                                    class="fa fa-home"></span> Изменить адрес доставки</a>

                        <a class="btn  btn-info btn-hover-info" href="/order/change_delivery/<?= $order_id ?>"><span
                                    class="fa fa-truck"></span> Изменить доставку</a>

                        <a class="btn btn-default btn-hover-success pli-gear add-tooltip"
                           href="/settings/order/"
                           data-original-title="Перейти в настройки заказов" data-container="body"></a>
                        <hr class="hr-wide">

                        <a disabled="disabled" class="btn btn-success btn-hover-success"
                           href="/order/complete/<?= $order_id ?>"><span
                                    class="fa fa-home"></span> Заказ отгружен</a>

                        <a disabled="disabled" class="btn btn-warning btn-hover-warning"
                           href="/order/status/problem/<?= $order_id ?>"><span
                                    class="fa fa-exclamation-triangle"></span> Пометить заказ как проблемный</a>

                        <a disabled="disabled" class="btn btn-info btn-hover-info"
                           href="/order/status/problem/<?= $order_id ?>"><span
                                    class="fa fa-user"></span> Забор через самовывоз</a>

                        <a disabled="disabled" class="btn btn-info btn-hover-info"
                           href="/order/status/problem/<?= $order_id ?>"><span
                                    class="fa fa-truck"></span> Забор через транспортную компанию</a>

                    </div>
                    <div id="order_comment" class="tab-pane fade">
                        <form class="form-horizontal" name="add_comment" method="post"
                              action="/order/add_comment/<?= $order_id ?>"
                              enctype="multipart/form-data">
                            <div class="list-group bg-trans">
                                <? foreach ($order_comments as $comment) {
                                    $comment = $comment['Order_Comment'];
                                    $c_id = $comment['id'];
                                    $c_text = $comment['comment'];
                                    $c_date = $comment['modified'];
                                    ?>
                                    <div href="#" class="list-group-item">
                                        <div class="media-left pos-rel">
                                            <img class="img-circle img-xs" src="img/profile-photos/9.png"
                                                 alt="Profile Picture">
                                            <i class="badge badge-danger badge-stat badge-icon pull-left"></i>

                                        </div>
                                        <div class="media-body">
                                            <p class="mar-no text-main">Администратор магазина</p>
                                            <small><?= lang_calendar($c_date) ?></small>
                                            <p class="pad-top text-sm"><?= $c_text ?>.</p>
                                        </div>
                                        <div class="media-right pos-rel">
                                            <a href="/order/delete_comment/<?= $order_id ?>/<?= $c_id ?>">удалить</a>
                                        </div>
                                    </div>
                                <? } ?>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                    <textarea placeholder="комментарий" id=order_comment name="comment"
                                              class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button class="btn btn-success" type="submit">Сохранить комментарий</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>