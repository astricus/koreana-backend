<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список компаний</h3>
        </div>
        <div class="panel-body">

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Компания</th>
                        <th>Телефон</th>
                        <th>Агенты</th>
                        <th>Добавлена в систему</th>
                        <th>Магазинов</th>
                        <th>Статус</th>
                        <th>Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? $n = 0;

                    $shop_status_list = array();
                    $shop_status_list['init'] = array('default', 'не запущена');
                    $shop_status_list['active'] = array('success', 'активена');
                    $shop_status_list['moderate'] = array('warning', 'модерируется');
                    $shop_status_list['blocked'] = array('danger', 'заблокирована системой');
                    $shop_status_list['stopped'] = array('danger', 'остановлена владельцем');

                    foreach ($companies as $company_elem) {
                        $n++;
                        $company = $company_elem['Company'];
                        $shop_count = $company_elem['shop_count'];
                        $company_agents = $company_elem['agents'];

                        $shop_status = $shop_status_list[$company['status']];
                        $blocked_shop = ($company['status'] == "blocked") ? "blocked_shop" : "";
                        ?>
                    <tr class="<?=$blocked_shop?>">
                        <td><?=$n?></td>
                        <td><a class="btn-link" href="/company/<?=$company['id']?>" style="text-transform: uppercase"><?=$company['company_name']?></a></p></td>
                        <td><?=$company['phone_number']?></td>
                        <td><?
                            foreach ($company_agents as $company_agent){
                                ?>
                                <a class="btn-link" href="/shop/agent/<?=$company_agent['id']?>">
                                    <?=prepare_fio($company_agent['firstname'], $company_agent['lastname'], "")?>
                                </a> [<?=$company_agent['role']?>]
                                <?
                            }
                            ?></td>
                        <td><span class="text-info"><i class="pli-clock"></i> <?=lang_calendar($company['created'])?></span></td>
                        <td><?=$shop_count?></td>
                        <td>
                            <div class="label label-<?=$shop_status[0]?>"><?=$shop_status[1]?></div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-sm btn-default btn-hover-success pli-pen-5 add-tooltip" href="/shop/edit/<?=$company['id']?>" data-original-title="Редактировать компанию" data-container="body"></a>
                                <?if($company['status'] == "init" OR $company['status'] == "stopped"){?>
                                    <a class="btn btn-sm btn-success btn-hover-warning pli-unlock add-tooltip" href="/shop/unblock/<?=$company['id']?>" data-original-title="Запустить компанию" data-container="body"></a>
                                <?} else if($company['status'] == "active"){?>
                                    <a class="btn btn-sm btn-danger btn-hover-success pli-lock add-tooltip" href="/shop/block/<?=$company['id']?>" data-original-title="Приостановить компанию" data-container="body"></a>
                                <?} else if($company['status'] == "blocked"){?>

                                <?}?>
                        </td>
                    </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>