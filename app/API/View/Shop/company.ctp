<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список компаний</h3>
        </div>
        <div class="panel-body">

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Компания</th>
                        <th>Телефон</th>
                        <th>Агенты</th>
                        <th>Добавлена в систему</th>
                        <th>Статус</th>
                        <th>Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?

                    $shop_status_list = array();
                    $shop_status_list['init'] = array('default', 'не запущена');
                    $shop_status_list['active'] = array('success', 'активена');
                    $shop_status_list['moderate'] = array('warning', 'модерируется');
                    $shop_status_list['blocked'] = array('danger', 'заблокирована системой');
                    $shop_status_list['stopped'] = array('danger', 'остановлена владельцем');

                    foreach ($companies as $company_elem) {

                        $company = $company_elem['Company'];

                        $shop_status = $shop_status_list[$company['status']];
                        $blocked_shop = ($company['status'] == "blocked") ? "blocked_shop" : "";
                        ?>
                    <tr class="<?=$blocked_shop?>">
                        <td><a class="btn-link" href="/company/<?=$company['id']?>" style="text-transform: uppercase"><?=$company['company_name']?></a></p></td>
                        <td><?=$company['phone_number']?></td>
                        <td><?
                            foreach ($agents as $company_agent){
                                ?>
                                <a class="btn-link" href="/shop/agent/<?=$company_agent['id']?>">
                                    <?=prepare_fio($company_agent['firstname'], $company_agent['lastname'], "")?>
                                </a> [<?=$company_agent['role']?>]
                                <?
                            }
                            ?></td>
                        <td><span class="text-info"><i class="pli-clock"></i> <?=lang_calendar($company['created'])?></span></td>
                        <td>
                            <div class="label label-<?=$shop_status[0]?>"><?=$shop_status[1]?></div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-sm btn-default btn-hover-success pli-pen-5 add-tooltip" href="/shop/edit/<?=$company['id']?>" data-original-title="Редактировать компанию" data-container="body"></a>
                                <?if($company['status'] == "init" OR $company['status'] == "stopped"){?>
                                    <a class="btn btn-sm btn-success btn-hover-warning pli-unlock add-tooltip" href="/shop/unblock/<?=$company['id']?>" data-original-title="Запустить компанию" data-container="body"></a>
                                <?} else if($company['status'] == "active"){?>
                                    <a class="btn btn-sm btn-danger btn-hover-success pli-lock add-tooltip" href="/shop/block/<?=$company['id']?>" data-original-title="Приостановить компанию" data-container="body"></a>
                                <?} else if($company['status'] == "blocked"){?>

                                <?}?>
                        </td>
                    </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>



            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Магазин</th>
                        <th>Телефон</th>
                        <th>Режим работы</th>
                        <th>Добавлен в систему</th>
                        <th>Статус</th>
                        <th>Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? $n = 0;

                    $shop_status_list = array();
                    $shop_status_list['init'] = array('default', 'не запущен');
                    $shop_status_list['active'] = array('success', 'активен');
                    $shop_status_list['moderate'] = array('warning', 'модерируется');
                    $shop_status_list['blocked'] = array('danger', 'заблокирован системой');
                    $shop_status_list['stopped'] = array('danger', 'остановлен владельцем');

                    foreach ($shops as $shop) {
                        $product_count = $shop['product_count'];
                        $shop_city = $shop['City'];
                        $shop = $shop['Shop'];
                        $last_order_id = rand(250, 450);
                        $n++;
                        $shop_status = $shop_status_list[$shop['status']];
                        $blocked_shop = ($shop['status'] == "blocked") ? "blocked_shop" : "";
                        ?>
                        <tr class="<?=$blocked_shop?>">
                            <td><?=$n?></td>
                            <td><a class="btn-link" href="/shop/<?=$shop['id']?>" style="text-transform: uppercase"><?=$shop['shop_name']?></a>, <p class="text-success"><span class=" text-bold">г. <?= $shop_city['name']?></span>, <?=$shop['address']?></p></td>
                            <td><?=$shop['phone']?></td>
                            <td><?=$shop['shop_open'] . " - " . $shop['shop_close'] ?></td>
                            <td><span class="text-info"><i class="pli-clock"></i> <?=lang_calendar($shop['created'])?></span></td>
                            <?/*
                        <td>54 091 045.00 руб.</td>
                        <td><?=$product_count?></td>
                        <td><a class="btn-link" href="#"> заказ #<?=$last_order_id?></a> от 13-07-2019</td>*/ ?>
                            <td>
                                <div class="label label-<?=$shop_status[0]?>"><?=$shop_status[1]?></div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-default btn-hover-success pli-pen-5 add-tooltip" href="/shop/edit/<?=$shop['id']?>" data-original-title="Редактировать магазин" data-container="body"></a>
                                    <?if($shop['status'] == "init" OR $shop['status'] == "stopped"){?>
                                        <a class="btn btn-sm btn-success btn-hover-warning pli-unlock add-tooltip" href="/shop/unblock/<?=$shop['id']?>" data-original-title="Запустить магазин" data-container="body"></a>
                                    <?} else if($shop['status'] == "active"){?>
                                        <a class="btn btn-sm btn-danger btn-hover-success pli-lock add-tooltip" href="/shop/block/<?=$shop['id']?>" data-original-title="Приостановить магазин" data-container="body"></a>
                                    <?} else if($shop['status'] == "blocked"){?>

                                    <?}?>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>

            <script>
                var TEST_YA_KEY = "19cf06fa-5c82-486c-9f0e-cad2decf81cc";

                // Группы объектов
                var groups = [
                    {
                        name: "Мои магазины",
                        style: "twirl#redIcon",
                        items: [ <?
                            foreach ($shops as $shop) {
                            $shop_item = $shop['Shop'];
                            $shop_city = $shop['City'];
                            ?>
                            {
                                center: [<?=$shop_item['shop_latitude']?>, <?=$shop_item['shop_longitude']?>],
                                address: "г. <?=mb_ucfirst( $shop_city['name'])?>, <?=mb_ucfirst($shop_item['address'])?>",
                                name: "<?=str_replace('"', '', $shop_item['shop_name'])?>"
                            },
                            <?}?>
                        ]},
                ];

            </script>
            <script src="https://api-maps.yandex.ru/2.0/?load=package.standard&amp;lang=ru-RU&amp;apikey=19cf06fa-5c82-486c-9f0e-cad2decf81cc" type="text/javascript"></script>
            <script src="https://yandex.st/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>

            <script>

                ymaps.ready(init);

                function init() {

                    // Создание экземпляра карты.
                    var myMap = new ymaps.Map('map', {
                            center: [50.443705, 30.530946],
                            zoom: 14
                        }),
                        // Контейнер для меню.
                        menu = $('<ul class="menu"></ul>');

                    myMap.controls
                    // Кнопка изменения масштаба.
                        .add('zoomControl', { left: 5, top: 5 })
                        // Список типов карты
                        .add('typeSelector')
                        // Стандартный набор кнопок
                        .add('mapTools', { left: 35, top: 5 });

                    // Перебираем все группы.
                    for (var i = 0, l = groups.length; i < l; i++) {
                        createMenuGroup(groups[i]);
                    }

                    function createMenuGroup (group) {
                        // Пункт меню.
                        var menuItem = $('<li><a href="#">' + group.name + '</a></li>'),
                            // Коллекция для геообъектов группы.
                            collection = new ymaps.GeoObjectCollection(null, { preset: group.style }),
                            // Контейнер для подменю.
                            submenu = $('<ul class="submenu"></ul>');

                        // Добавляем коллекцию на карту.
                        myMap.geoObjects.add(collection);

                        // Добавляем подменю.
                        menuItem
                            .append(submenu)
                            // Добавляем пункт в меню.
                            .appendTo(menu)
                            // По клику удаляем/добавляем коллекцию на карту и скрываем/отображаем подменю.
                            .find('a')
                            .toggle(function () {
                                myMap.geoObjects.remove(collection);
                                submenu.hide();
                            }, function () {
                                myMap.geoObjects.add(collection);
                                submenu.show();
                            });

                        // Перебираем элементы группы.
                        for (var j = 0, m = group.items.length; j < m; j++) {
                            createSubMenu(group.items[j], collection, submenu);
                        }
                    }

                    function createSubMenu (item, collection, submenu) {
                        // Пункт подменю.
                        var submenuItem = $('<li><a href="#">' + item.name + '</a></li>');
                        // Создаем метку.
                        var placemark = new ymaps.Placemark(item.center, {balloonContent: item.name});


                        // Добавляем метку в коллекцию.
                        collection.add(placemark);
                        // Добавляем пункт в подменю.
                        submenuItem
                            .appendTo(submenu)
                            // При клике по пункту подменю открываем/закрываем баллун у метки.
                            .find('a')
                            .toggle(function () {
                                placemark.balloon.open();
                            }, function () {
                                placemark.balloon.close();
                            });

                    }

                    // Добавляем меню в тэг BODY.
                    menu.appendTo($('.map_shop_list'));
                    // Выставляем масштаб карты чтобы были видны все группы.
                    myMap.setBounds(myMap.geoObjects.getBounds());
                }
                /*
                            var myGeocoder = ymaps.geocode("Moscow");
                            myGeocoder.then(function (res) {
                                myMap.geoObjects.add(res.geoObjects);
                                // Выведем в консоль данные, полученные в результате геокодирования объекта.
                                console.log(res.geoObjects.get(0)
                                    .properties.get('metaDataProperty')
                                    .getAll());
                            }, function (err) {
                                // Обработка ошибки.
                            });*/

            </script>

            <style type="text/css">
                /* Оформление меню (начало)*/
                .menu {
                    list-style: none;

                    margin: 0;
                    padding: 0;
                }
                .submenu {
                    list-style: none;

                    margin: 0 0 0 20px;
                    padding: 0;
                }
                .submenu li {
                    font-size: 90%;
                }
                /* Оформление меню (конец)*/
                .map_shop_list {
                    width: 50%;
                    display: inline-block;
                    vertical-align: top;
                    padding: 10px;
                    margin-left: 10px;
                }
                #map {
                    width: 50%;
                    display: inline-block;
                    vertical-align: top;
                }
            </style>

            <div id="map" style="width:480px;height:480px; margin: auto;"></div>

            <div class="map_shop_list"></div>

        </div>
    </div>
</div>