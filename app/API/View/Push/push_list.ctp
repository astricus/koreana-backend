<script>
    $(document).ready(function () {
        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Платформа */
        $(document).on("change", ".platform_select", function () {
            let platform = $('.platform_select option:selected').val(), new_url;
            if (platform != 0) {
                new_url = updateQueryStringParameter(window.location.href, 'platform', platform);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'platform', '');
            }
            redirect(new_url);
        });

        /* Дата показа */
        $(document).on("change", "#start_datetime", function () {
            let start_datetime = $('#start_datetime').val(), new_url;
            if (start_datetime != "") {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', start_datetime);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', '');
            }
            redirect(new_url);
        });

    });

    $(document).on('click', ".delete_push", function (e) {
        if (confirm('Вы уверены, что хотите полностью удалить данное уведомление?')) {
            var link = $(this).attr('href');
            window.location.href = link;
        }
        e.preventDefault();
        return false;
    });

</script>
<?php

?>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-envelope"></i> push-уведомления</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/push/add/"><i class="fa fa-plus"></i>
                            Создать push уведомление</a>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/push/list" class="btn btn-success">показать все уведомления</a>
                    </div>
                    <? /*
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 city_select" name="city_id">

                            <option value="0">Все регионы</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) {
                                    echo 'selected';
                                } ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
*/ ?>

                </div>

            </div>

            <hr>

            <? if (count($push_messages) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Заголовок</th>
                            <th>Иконка</th>
                            <th>Тип сообщения</th>
                            <th>Дата создания</th>
                            <th>Отправлено / доставлено / ожидает / ошибка</th>
                            <th>Отправка производилась</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;

                        foreach ($push_messages as $push_message) {
                            $n++;
                            $push = $push_message['Push_Message'];
                            $push_name = $push['name'];
                            $push_title = $push['push_title'];
                            $push_content = $push['push_message'];
                            $push_type = $push['push_type'];
                            $push_image = $push['push_image'];
                            $push_status = $push['status'];
                            $created = $push['created'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/push/view/<?= $push['id'] ?>"><?= $push_name ?>
                                    </a>
                                </td>
                                <td><?= $push_title ?></td>
                                <td><img src="<?= $push_component->getPushImageUrl($push_image) ?>" alt="push image"
                                         style="max-width: 80px; max-height: 80px">
                                </td>
                                <td><?= $push_type ?></td>
                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($created) ?>
                                    </span>
                                </td>
                                <td>
                                    <span class="text-info text-bold"><?= $push_message['sent'] ?> / <?= $push_message['received'] ?> / <?= $push_message['new'] ?>
                                       / <?= $push_message['error'] ?></span>
                                </td>

                                <td>
                                    ---
                                </td>
                                <td>
                                    <a class="btn btn-large btn-success btn-hover-info pli-pencil add-tooltip"
                                       href="/push/edit/<?= $push['id'] ?>"
                                       title="Изменить данные"
                                       data-container="body"></a>
                                    <? if ($push_message['error'] + $push_message['new'] > 0) { ?>
                                        <a class="btn btn-large btn-success btn-hover-info pli-arrow-right add-tooltip"
                                           href="/push/send_now/<?= $push['id'] ?>"
                                           title="Запустить отправку немедленно" data-container="body"></a>
                                    <? } else { ?>
                                        <a class="btn btn-large btn-default btn-hover-info pli-arrow-right add-tooltip"
                                           disabled="disabled"
                                           title="Запустить отправку немедленно" data-container="body"></a>
                                        <?
                                    } ?>
                                    <!--
                                    <a class="btn btn-large btn-success btn-hover-info fa fa-copy add-tooltip"
                                       href="/push/copy/<?= $push['id'] ?>"
                                       title="Создать копию уведомления" data-container="body"></a>
                                    -->
                                    <a class="btn btn-large btn-success btn-hover-info fa fa-users"
                                       href="/push/users/<?= $push['id'] ?>"
                                       title="Получатели"></a>
                                    <a class="btn btn-large btn-warning btn-hover-info fa fa-trash-alt delete_push"
                                       href="/push/delete/<?= $push['id'] ?>"
                                       title="Удалить"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/push/push_list";
                            $params = [];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>
                <p class="muted">push-уведомлений нет</p>
            <? } ?>

        </div>
    </div>
</div>