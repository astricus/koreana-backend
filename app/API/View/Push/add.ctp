<div class="row">
    <script>
        $(document).on('nifty.ready', function () {

            $(".datepicker").datepicker();

            $(document).on("click", "#start_now", function () {
                if ($("#start_now").prop('checked')) {
                    $("#datetime_input").hide()
                } else {
                    $("#datetime_input").show()
                }
            });

        })</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Создание push уведомления</h3>
        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/push_list">Список push уведомлений</a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_push" method="post" action="/push/add" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="name">Название (для внутреннего обозначения)</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required placeholder="название"
                               value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="push_title">Заголовок уведомления</label>
                    <div class="col-sm-6">
                        <textarea id="push_title" class="form-control" placeholder="Заголовок"
                                  name="push_title"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="push_image">Изображение</label>
                    <div class="col-sm-6">
                        <input type="file" id="push_image" class="form-control" name="push_image"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="push_message">Текст</label>
                    <div class="col-sm-6">
                        <textarea id="push_message" class="form-control" name="push_message"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="push_type">Тип уведомления (для создания обычного push
                        уведомления поле оставить пустым)</label>
                    <div class="col-sm-6">
                        <div class="col-sm-3 table-toolbar-left dropdown">
                            <select id="push_type" class="main_select use_select2" name="push_type">
                                <? foreach ($valid_message_types as $key => $valid_message_type) {
                                    ?>
                                    <option value="<?= $key ?>">
                                        <?= $valid_message_type ?>
                                    </option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <? /*

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="start_now">Начать рассылку немедленно</p>
                    </label>
                    <div class="col-sm-6">
                        <input type="checkbox" name="start_now" id="start_now"/>
                    </div>
                </div>

                <div class="form-group" id="datetime_input">
                    <label class="col-lg-3 control-label" for="start_date">Дата запуска рассылки
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате гггг-мм-дд </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="start_date" id="datepicker"/>
                    </div>
                    <label class="col-lg-3 control-label" for="start_time">время запуска рассылки
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате чч:мм </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="time" name="start_time" id="start_time" value=""/>
                    </div>
                </div>

                <div class="form-group" id="datetime_input">
                    <label class="col-lg-3 control-label" for="stop_date">Дата остановки рассылки
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате гггг-мм-дд </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="stop_date" class="datepicker"/>
                    </div>
                    <label class="col-lg-3 control-label" for="stop_time">время остановки рассылки
                        <p class="text-muted">В случае если браузер не поддерживают элемент формы установки времени,
                            выберите время в формате чч:мм </p>
                    </label>
                    <div class="col-sm-6">
                        <input type="time" name="stop_time" id="stop_time" value=""/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Группа клиентов</p></label>
                    <div class="col-sm-6">
                        <select id="client_group" class="use_select2 " multiple name="client_group"
                                style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($client_groups as $client_group) { ?>
                                <option value="<?= $client_group['Client_Group']['id']; ?>"><?= $client_group['Client_Group']['name'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

*/ ?>


            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>