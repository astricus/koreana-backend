<script>
    $(document).ready(function () {
        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Платформа */
        $(document).on("change", ".platform_select", function () {
            let platform = $('.platform_select option:selected').val(), new_url;
            if (platform != 0) {
                new_url = updateQueryStringParameter(window.location.href, 'platform', platform);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'platform', '');
            }
            redirect(new_url);
        });

        /* Дата показа */
        $(document).on("change", "#start_datetime", function () {
            let start_datetime = $('#start_datetime').val(), new_url;
            if (start_datetime != "") {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', start_datetime);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'start_datetime', '');
            }
            redirect(new_url);
        });

        $(".add_push_user_form").hide();
        $(document).on("click", ".add_push_user", function () {
            $(".add_push_user_form").show(200);
        });


    });

</script>
<?php

?>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-envelope"></i>Получатели push-уведомления <b><?= $push_name ?></b>
            </h3>
        </div>

        <div class="panel-body add_push_user_form">
            <form class="form-horizontal" name="add_push_user" method="post" action="/push/add_receiver/<?= $push_id ?>"
                  enctype="multipart/form-data">
                <div class="panel-body">


                    <?
                    //pr($all_users);?>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="push_title">Отдельные клиенты</label>
                        <div class="col-sm-6">
                            <select id="user_ids" class="use_select2" multiple name="user_ids[]"
                                    style="width: 300px">
                                <option value="0">Не выбрано</option>
                                <? foreach ($all_users as $all_user) { ?>
                                    <option value="<?= $all_user['User']['id']; ?>">
                                        <?= prepare_fio($all_user['User']['firstname'], $all_user['User']['lastname'], $all_user['User']['middlename']) ?>
                                        - <?=$all_user['User']['phone']?>
                                    </option>
                                <? } ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="platform">Группа клиентов</p></label>
                        <div class="col-sm-6">
                            <select id="client_group" class="use_select2 " multiple name="client_group[]"
                                    style="width: 300px">
                                <option value="0">Не выбрано</option>
                                <? foreach ($client_groups as $client_group) { ?>
                                    <option value="<?= $client_group['Client_Group']['id']; ?>"><?= $client_group['Client_Group']['name'] ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-4 table-toolbar-left">
                        <a class="btn btn-default btn-bock" href="/push/view/<?= $push_id ?>"><i
                                    class="fa fa-backward"></i> Вернуться к уведомлению</a>
                        <a class="btn btn-success btn-bock add_push_user"><i class="fa fa-plus"></i> Добавить получателя</a>
                    </div>
                </div>

                <div class="row">


                    <? /*
                     <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/push/users" class="btn btn-success">показать всех получателей</a>
                    </div>
                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 city_select" name="city_id">

                            <option value="0">Все регионы</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) {
                                    echo 'selected';
                                } ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                */?>

                </div>

            </div>

            <hr>

            <? if (count($push_users) > 0) { ?>

                Всего получателей: <?= $push_total_users ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Пользователь</th>
                            <th>Статус отправки</th>
                            <th>Отправка производилась</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;

                        foreach ($push_users as $push_user) {
                            $n++;
                            $push = $push_user['Push_Sent'];
                            $user_name = prepare_fio($push_user['User']['firstname'], $push_user['User']['lastname'], "");
                            $push_status = $push['status'];
                            $created = $push['created'];
                            $status_list = array();
                            $status_list['received'] = 'success';
                            $status_list['sent'] = 'warning';
                            $status_list['error'] = 'danger';
                            $status_list['new'] = 'default';

                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/user/<?= $push_user['User']['id'] ?>"><?= $user_name ?>
                                    </a>
                                    [<?= $push_user['User']['phone'] ?>
                                    <b><?= $push_user['User_Device']['os'] ?></b> <?= substr($push_user['Push_Sent']['device_id'], 0, 8) ?>
                                    ...]
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$push_status] ?>">
                                        <?= $push_sent_statuses[$push_status]; ?>
                                    </div>
                                </td>
                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($created) ?>
                                    </span>
                                </td>
                                <td>

                                    <? if ($push_status == 'new' or $push_status == 'error') { ?>
                                        <a class="btn btn-large btn-success btn-hover-info pli-arrow-right add-tooltip"
                                           href="/push/send_push_to_user/<?= $push_user['Push_Sent']['push_id'] ?>/<?= $push_user['Push_Sent']['id'] ?>"
                                           title="Запустить отправку немедленно" data-container="body"></a>
                                    <? } ?>
                                    <? if ($push_status == 'new' or $push_status == 'error') { ?>
                                        <a class="btn btn-large btn-danger btn-hover-info fa fa-trash-alt add-tooltip"
                                           href="/push/user/delete/<?= $push_id ?>/<?= $push_user['Push_Sent']['id'] ?>"
                                           title="Удалить пользователя из списка отправки"></a>
                                    <? } ?>

                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/push/list";
                            $params = [];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>
                <p class="muted">получателей push-уведомления нет, добавьте их для возможности отправки</p>
            <? } ?>

        </div>
    </div>
</div>