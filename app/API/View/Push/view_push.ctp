<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-envelope"></i>push-уведомление <b><?= $push['name'] ?></b>
            </h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-4 table-toolbar-left">
                        <a class="btn btn-success btn-bock" href="/push/list/"><i
                                    class="fa fa-list"></i> Все уведомления</a>
                        <a class="btn btn-default btn-bock" href="/push/edit/<?= $push['id'] ?>"><i
                                    class="fa fa-pencil-alt"></i> Редактировать уведомление</a>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <td>
                                    создано
                                </td>
                                <td>
                                    заголовок
                                </td>
                                <td>
                                    тип уведомления
                                </td>
                                <td>
                                    иконка
                                </td>
                                <td>
                                    содержание
                                </td>
                            </tr>
                            <tr class="">
                                <td>
                                    <?= lang_calendar($push['created']) ?>
                                </td>
                                <td>
                                    <?= $push['push_title']; ?>
                                </td>
                                <td>
                                    <?= $push['push_type']; ?>
                                </td>
                                <td>
                                    <? if (!empty($push['push_image'])) {
                                        ?>
                                        <img src="<?= $push_component->getPushImageUrl($push['push_image']) ?>"
                                             style="max-width: 80px; max-height: 80px">
                                        <?
                                    } ?>
                                </td>
                                <td>
                                    <?= $push['push_message']; ?>
                                </td>
                            </tr>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>