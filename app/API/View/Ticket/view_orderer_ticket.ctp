<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Поддержка</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">
                <div class="row">
                    <div class="col-sm-6 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success" href="/ticket/add/"><i class="pli-add"></i> Создать
                            новый тикет</a>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>

                        <th>Название</th>
                        <th>Тема</th>
                        <th>Обновлено</th>
                        <th>Статус</th>
                        <th> -</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?

                    $sender_type_titles= [];
                    $sender_type_titles['admin'] = 'ответ администрации';
                    $sender_type_titles['user'] = 'ваше сообщение';

                    $ticket_status_list = array();
                    $ticket_status_list['new'] = array('default', 'новый тикет');
                    $ticket_status_list['user_answered'] = array('success', 'Обновлен пользователем');
                    $ticket_status_list['admin_answered'] = array('warning', 'Обновлен администратором');
                    $ticket_status_list['user_closed'] = array('danger', 'Закрыт пользователем');
                    $ticket_status_list['admin_closed'] = array('danger', 'Закрыт  администратором');
                    $ticket_theme = $ticket['Company_Ticket_Theme'];
                    $ticket = $ticket['Company_Ticket'];

                    $ticket_status = $ticket_status_list[$ticket['ticket_status']];
                    $blocked_ticket = ($ticket['ticket_status'] == "blocked") ? "blocked_ticket" : "";
                    ?>
                    <tr class="<?= $blocked_ticket ?>">
                        <td><a class="btn-link" href="/ticket/<?= $ticket['id'] ?>"
                               style="text-transform: uppercase"><?= $ticket['ticket_name'] ?></a></td>
                        <td><?= $ticket_theme['name'] ?></td>
                        <td><span class="text-info"><i
                                        class="pli-clock"></i> <?= lang_calendar($ticket['created']) ?></span></td>
                        <td>
                            <div class="label label-<?= $ticket_status[0] ?>"><?= $ticket_status[1] ?></div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <? if ($ticket['ticket_status'] == "admin_closed" OR $ticket['ticket_status'] == "user_closed") { ?>
                                    <a class="btn btn-sm btn-success btn-hover-warning pli-unlock add-tooltip"
                                       href="/ticket/unblock/<?= $ticket['id'] ?>" data-original-title="Запустить тикет"
                                       data-container="body"></a>
                                <? } else if ($ticket['ticket_status'] == "new" OR $ticket['ticket_status'] == "user_answered" OR $ticket['ticket_status'] == "admin_answered") { ?>
                                    <a class="btn btn-sm btn-danger btn-hover-success pli-lock add-tooltip"
                                       href="/ticket/block/<?= $ticket['id'] ?>"
                                       data-original-title="Приостановить тикет" data-container="body"></a>
                                <? } else if ($ticket['ticket_status'] == "blocked") { ?>

                                <? } ?>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>


            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Отправитель</th>
                        <th>сообщение</th>
                        <th>Отправлено</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?

                    foreach ($ticket_messages as $ticket_message) {
                        $ticket_message = $ticket_message['Company_Ticket_Message'];
                        ?>
                        <tr>
                            <td><?= $sender_type_titles[$ticket_message['sender_type']]?></td>
                            <td><?= $ticket_message['text'] ?></td>
                            <td><span class="text-info"><i
                                            class="pli-clock"></i> <?= lang_calendar($ticket['created']) ?></span></td>


                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>

            <? if($ticket['ticket_status'] != "user_closed"){?>

            <div class="row">
                <div class="col-sm-6">
                    <form action="/ticket/add_answer/<?= $ticket['id'] ?>" method="post">
                        <div class="form-group">
                                <textarea class="form-control" name="message" rows="10"
                                          placeholder="Ваше сообщение"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-lg">Ответить</button>
                    </form>
                </div>
            </div>

            <?}?>


        </div>
    </div>
</div>