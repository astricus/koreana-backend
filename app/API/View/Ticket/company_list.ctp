<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Поддержка</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">
                <div class="row">
                    <div class="col-sm-6 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success" href="/ticket/add/"><i class="pli-add"></i> Создать
                            новый тикет</a>
                    </div>
                </div>
            </div>

            <? if (count($tickets) > 0) { ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Компания</th>
                            <th>Название</th>
                            <th>Тема</th>
                            <th>Послед.сообщение</th>
                            <th>Обновлено</th>
                            <th>Статус</th>
                            <th> -</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;

                        $ticket_status_list = array();
                        $ticket_status_list['new'] = array('default', 'новый тикет');
                        $ticket_status_list['user_answered'] = array('success', 'Обновлен пользователем');
                        $ticket_status_list['admin_answered'] = array('warning', 'Обновлен администратором');
                        $ticket_status_list['user_closed'] = array('danger', 'Закрыт пользователем');
                        $ticket_status_list['admin_closed'] = array('danger', 'Закрыт  администратором');

                        foreach ($tickets as $ticket_item) {

                            $ticket = $ticket_item['Company_Ticket'];


                            $company = $ticket_item['Company'];
                            $company_name = $company['company_name'];

                            $n++;
                            $ticket_status = $ticket_status_list[$ticket['ticket_status']];
                            $blocked_ticket = ($ticket['ticket_status'] == "blocked") ? "blocked_ticket" : "";
                            ?>
                            <tr class="<?= $blocked_ticket ?>">
                                <td><?= $n ?></td>
                                <td><p class="text-bold"><?= $company_name ?></p></td>

                                <td><a class="btn-link" href="/ticket/company/view/<?= $ticket['id'] ?>"
                                       style="text-transform: uppercase"><?= $ticket['ticket_name'] ?></a></td>
                                <td><?= $ticket_item['Company_Ticket_Theme']['name'] ?></td>
                                <td style="max-width: 250px"><a class="btn-link"
                                                                href="/ticket/company/view/<?= $ticket['id'] ?>"><?= $ticket_item['message']['Company_Ticket_Message']['text'] ?></a>
                                </td>
                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($ticket['created']) ?></span>
                                </td>
                                <td>
                                    <div class="label label-<?= $ticket_status[0] ?>"><?= $ticket_status[1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-sm btn-default btn-hover-success pli-pen-5 add-tooltip"
                                           href="/ticket/company/view/<?= $ticket['id'] ?>"
                                           data-original-title="Ответить в тикет" data-container="body"></a>
                                        <? if ($ticket['ticket_status'] == "admin_closed" OR $ticket['ticket_status'] == "user_closed") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning pli-unlock add-tooltip"
                                               href="/ticket/company/unblock/<?= $ticket['id'] ?>"
                                               data-original-title="Запустить тикет" data-container="body"></a>
                                        <? } else if ($ticket['ticket_status'] == "new" OR $ticket['ticket_status'] == "user_answered" OR $ticket['ticket_status'] == "admin_answered") { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success pli-lock add-tooltip"
                                               href="/ticket/company/block/<?= $ticket['id'] ?>"
                                               data-original-title="Приостановить тикет" data-container="body"></a>
                                        <? } else if ($ticket['ticket_status'] == "blocked") { ?>

                                        <? } ?>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>
            <? } else if (count($tickets) == 0) { ?>
                <p class="text-muted">На даннй момент отсутствуют обращения в поддержку от компаний</p>
            <? } ?>

            <div class="fixed-table-pagination">

                <?if($ticket_count>0){?>
                <div class="pull-left pagination-detail">
                    <span class="pagination-info">Всего создано тикетов: <?= $ticket_count ?></span>
                </div>
                <?}?>

                <div class="pull-right pagination">
                    <ul class="pagination">
                        <?php $ctrl_pgn = "/support/company/";
                        $params = array();
                        echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>