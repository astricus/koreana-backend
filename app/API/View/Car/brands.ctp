<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список марок машин</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/car/brands" class="btn btn-success">показать все марки машин</a>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($brands) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Добавлена</th>
                            <th>Моделей</th>
                            <th>Популярность</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;

                        foreach ($brands as $brand) {
                            $n++;
                            $brand_data = $brand['Car_Mark'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/car/brand/view/<?= $brand_data['id'] ?>"><?= $brand_data['name'] ?></a>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($brand_data['created']) ?>
                                    </span>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= $brand['count'] ?>
                                    </span>
                                </td>
                                <td><? if ($brand_data["popular"] == 0) { ?>
                                        <a class="btn btn-success btn-bock"
                                           href="/car/brand/set_popular/<?= $brand_data['id'] ?>">
                                            <i class="fa fa-bar-chart-o"></i>Сделать марку популярной</a>
                                    <? } else { ?>
                                        <a class="btn btn-warning btn-bock"
                                           href="/car/brand/set_not_popular/<?= $brand_data['id'] ?>"><i
                                                    class="fa fa-bar-chart-o"></i>Убрать марку из популярных</a>

                                    <? } ?>
                                </td>

                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/car/brands";
                            $params         = [];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Марок машин нет</p>
            <? } ?>

        </div>
    </div>
</div>