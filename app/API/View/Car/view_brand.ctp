<div class="panel-body">
    <div class="blog-title media-block">
        <div class="media-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/car/brands" class="btn btn-success">показать все марки машин</a>
                    </div>

                </div>

            </div>
            <h3><?= $brand['Car_Mark']['name']; ?></h3>

        </div>
    </div>
    <div class="blog-content">
        <div class="blog-body">
            <? //$new['content'];?>
        </div>
    </div>


    <div class="blog-footer">
        <div class="media-left">
            <? if (count($models) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Машин в базе</th>

                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;

                        $sum = 0;
                        foreach ($models as $model) {
                            $n++;

                            $model_data = $model['Car_Model'];
                            $sum        += (int)$model['count'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/car/model/view/<?= $model_data['id'] ?>"><?= $model_data['name'] ?></a>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= $model['count'] ?>
                                    </span>
                                </td>
                                <td><? if ($model_data["popular"] == 0) { ?>
                                        <a class="btn btn-success btn-bock"
                                           href="/car/model/set_popular/<?= $model_data['id'] ?>">
                                            <i class="fa fa-bar-chart-o"></i>Сделать модель популярной</a>
                                    <? } else { ?>
                                        <a class="btn btn-warning btn-bock"
                                           href="/car/model/set_not_popular/<?= $model_data['id'] ?>"><i
                                                    class="fa fa-bar-chart-o"></i>Убрать модель из популярных</a>

                                    <? } ?>
                                </td>

                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                    <p>всего машин данного бренда у клиентов - <b><?= $sum ?></b></p>
                </div>

            <? } else { ?>

                <p class="muted">Моделей данной марки нет</p>
            <? } ?>
        </div>
    </div>
</div>