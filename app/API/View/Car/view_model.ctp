<div class="panel-body">
    <div class="blog-title media-block">
        <div class="media-body">

            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/car/brands" class="btn btn-success">показать все марки машин</a>
                    </div>

                </div>

            </div>

            <h5>Модель машины <a class="btn-link"
                                 href="/car/model/view/<?= $brand['Car_Mark']['id'] ?>"><?= $brand['Car_Mark']['name'] ?></a>
                - <?= $model[0]['Car_Model']['name']; ?></h5>

            Количество машин данной модели в базе клиентов: <b><span class=""><?= $model_count ?></span></b>
        </div>
    </div>
    <div class="blog-content">
        <div class="blog-body">

        </div>
    </div>
</div>