
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список марок машин</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/car/brands" class="btn btn-success">показать все марки машин</a>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($users_cars) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Марка</th>
                            <th>Модель</th>
                            <th>Госномер</th>
                            <th>Добавлена</th>
                            <th>Клиент</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;


                        foreach ($users_cars as $users_car) {
                            $n++;
                            $brand_data = $users_car['Car_Mark'];
                            $model_data = $users_car['Car_Model'];
                            $user_car_data = $users_car['User_Car'];
                            $user_data = $users_car['User'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/car/brand/view/<?= $brand_data['id'] ?>"><?= $brand_data['name'] ?></a></td>
                                <td><a class="btn-link"
                                       href="/car/model/view/<?= $model_data['id'] ?>"><?= $model_data['name'] ?></a></td>
                                <td><span class=""><?= $user_car_data['car_number'] ?>
                                    </span>
                                </td>
                                <td><span class=""><i class="pli-clock"></i> <?= lang_calendar($user_data['created']) ?>
                                    </span>
                                </td>
                                <td><span class=""><i class="fa fa-user"></i> <a class="btn-link" href="/user/<?=$user_data['id']?>"><b><?= prepare_fio($user_data['firstname'], $user_data['lastname'], $user_data['middlename'])?></b></a>, <b><?=$user_data['phone']?></b>
                                    </span>
                                </td>

                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/cars";
                            $params = array();
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Марок машин нет</p>
            <? } ?>

        </div>
    </div>
</div>