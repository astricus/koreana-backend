<script>
    $(document).ready(function () {

        /* Поиск по названию */
        $(document).on("change", ".find_by_name", function () {
            let find_by_name = $('.find_by_name').val(), new_url;
            if (find_by_name.length >= 2) {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', find_by_name);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', '');
            }
            redirect(new_url);
        });

    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список вебформ</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class=" table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock"
                           href="/webform/add"><i class="fa fa-plus"></i>
                            Создать веб-форму</a>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/webforms" class="btn btn-success">показать все веб-формы</a>
                    </div>


                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <label class="label control-label label-info" for="find_by_name">поиск веб-формы по
                            названию</label>
                        <input id="find_by_name" type="text" placeholder="поиск веб-формы по названию"
                               class="main_select use_select2 find_by_name" name="find_by_name"
                               value="<?= $form_data['find_by_name'] ?>">
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($webforms) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Добавлена</th>
                            <th>Где показывается</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list[1] = array('success', 'активна');
                        $status_list[0] = array('danger', 'скрыта');

                        foreach ($webforms as $webform) {
                            $name = $webform['Webform']['name'];
                            $webform_id = $webform['Webform']['id'];
                            $n++;

                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link" href="/webform/view/<?= $webform_id ?>"><?= $name ?></a>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($webform['Webform']['created']) ?>
                                    </span>
                                </td>
                                <td>
                                    place
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$webform['Webform']['enabled']][0] ?>"><?= $status_list[$webform['Webform']['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($webform['Webform']['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/webform/unblock/<?= $webform_id ?>"
                                               data-original-title="Открыть веб-форму" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/webform/block/<?= $webform_id?>"
                                               data-original-title="Скрыть веб-форму" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/webform/edit/<?= $webform_id?>"
                                           data-original-title="Изменить веб-форму" data-container="body"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/webforms/";
                            $params = array(
                                'find_by_name' => $form_data['find_by_name'],
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Веб-форм нет</p>
            <? } ?>

        </div>
    </div>
</div>