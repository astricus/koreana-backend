<?
$status_list = array();
$status_list[1] = array('success', 'активна');
$status_list[0] = array('danger', 'скрыта');

$days_later = $webform['days_later'];
$webform = $webform['Webform'];
?>
<?= $this->Html->script('webforms') ?>
<div class="row">
    <a href="/webforms" class="btn btn-success">показать все веб-формы</a>
    <a class="btn btn-mint" href="/webform/edit/<?= $webform['id'] ?>">Редактировать форму</a>
    <a class="btn btn-default" href="webform/reports/view/<?= $webform['id'] ?>">Смотреть отчеты по форме</a>
    <? if ($webform['enabled'] == 1) { ?>
        <a class="btn btn-warning btn-bock" href="/webform/block/<?= $webform['id'] ?>"><i class="fa fa-close"></i>
            Скрыть форму из показа (сейчас показывается)</a>
    <? } else { ?>
        <a class="btn btn-warning btn-bock" href="/webform/unblock/<?= $webform['id'] ?>"><i class="fa fa-eye"></i>
            Показывать форму (сейчас скрыта)</a>

    <? } ?>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $webform['name'] ?></h3>
        </div>
        <div class="panel-body">


            <p class="label label-success">Форма создана <?= $days_later ?></p>
            <hr>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Содержание</h3>
                </div>

                <?= $webform['content'] ?>
            </div>

            <? if (!empty($story)) { ?>
                <div class="panel panel-info">
                    История изменений страницы
                </div>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Действие</th>
                            <th>Автор</th>
                            <th>Дата и время</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?
                        foreach ($story as $story_elem) {
                            $story_item = $story_elem['Static_Page_Log'];
                            $author = $story_item['author_id'];
                            $action = $story_item['action'];
                            $created = lang_calendar($story_item['created']);
                            ?>
                            <tr class="">
                                <td>
                                    <span class="text-info"><?= $action ?></span>
                                </td>

                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($st_page['created']) ?>
                                    </span>
                                </td>
                                <td>
                                    <span class="text-info"><?= $story_elem['author_name'] ?></span>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>
            <? } ?>

            <form class="form-horizontal panel-body" name="edit_action" method="post"
                  action="/webform/fill/<?= $webform['id'] ?>"
                  enctype="multipart/form-data">


                <? foreach ($form_data as $form_data_elem) {
                    pr($form_data_elem);
                    $form_field = $form_data_elem['Webform_Field'];
                    $provider_list = $form_data_elem['provider_list'] ?? null;
                    ?>

                    <div class="form-group">
                        <label class="col-lg-3 control-label"
                               for="<?= $form_field['field_code'] ?>"><?= $form_field['field_name'] ?></label>
                        <div class="col-sm-9">
                            <? if ($form_field['field_type'] == "textarea") { ?>
                                <textarea id="<?= $form_field['field_code'] ?>" class="form-control"
                                          placeholder="<?= $form_field['placeholder'] ?>"
                                          name="<?= $form_field['id'] ?>"></textarea>
                            <? } ?>
                            <? if ($form_field['field_type'] == "input_text") { ?>
                                <input id="<?= $form_field['field_code'] ?>" class="form-control"
                                       placeholder="<?= $form_field['placeholder'] ?>"
                                       name="<?= $form_field['id'] ?>"/>
                            <? } ?>
                            <? if ($form_field['field_type'] == "select") { ?>
                                <select name="<?= $form_field['id'] ?>" class="form-control"
                                        id="<?= $form_field['id'] ?>">
                                    <?

                                    foreach ($provider_list as $provider_list_item) {
                                        $provider_list_item = $provider_list_item['Data_List_Item'] ?>
                                        <option value="<?= $provider_list_item['id'] ?>"><?= $provider_list_item['name'] ?></option>
                                    <? } ?>
                                </select>

                            <? } ?>
                            <? if ($form_field['field_type'] == "input_date") { ?>
                                <input type="date" id="<?= $form_field['field_code'] ?>" class="form-control datepicker"
                                       placeholder="<?= $form_field['placeholder'] ?>"
                                       name="<?= $form_field['id'] ?>"/>
                            <? } ?>
                            <? if ($form_field['field_type'] == "input_name") { ?>
                                <input type="text" id="<?= $form_field['field_code'] ?>" minlength="2"
                                       maxlength="32" class="form-control"
                                       placeholder="<?= $form_field['placeholder'] ?>"
                                       name="<?= $form_field['id'] ?>"/>
                            <? } ?>

                            <? if ($form_field['field_type'] == "input_phone") { ?>
                                <input type="text" id="<?= $form_field['field_code'] ?>" class="form-control masked_phone"
                                       placeholder="<?= $form_field['placeholder'] ?>"
                                       name="<?= $form_field['id'] ?>"/>
                            <? } ?>

                            <? if ($form_field['field_type'] == "input_email") { ?>
                                <input type="email" id="<?= $form_field['field_code'] ?>" class="form-control"
                                       placeholder="<?= $form_field['placeholder'] ?>"
                                       name="<?= $form_field['id'] ?>"/>
                            <? } ?>

                            <? if ($form_field['field_type'] == "input_radio") { ?>
                                <div class="radio">
                                    <? foreach ($provider_list as $provider_list_item) { ?>
                                        <input type="radio" id="<?= $form_field['field_code'] ?>" class="form-control"
                                               value="<?= $provider_list_item['value'] ?>"
                                               name="<?= $form_field['id'] ?>"/>

                                    <? } ?>
                                </div>

                            <? } ?>

                            <? if ($form_field['field_type'] == "input_checkbox") { ?>
                                <div class="checkbox">
                                    <input type="checkbox" id="<?= $form_field['field_code'] ?>" class="magic-checkbox"
                                           value="" name="<?= $form_field['id'] ?>"/>
                                    <label for="<?= $form_field['field_code'] ?>"><?= $form_field['placeholder'] ?></label>
                                </div>
                            <? } ?>

                        </div>
                    </div>

                <? } ?>


                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-3 col-sm-offset-3">
                            <input class="btn btn-success" id="submit_edit" type="submit"
                                   value="Тестовое заполнение формы">
                        </div>

                    </div>
                </div>
            </form>


        </div>
    </div>
</div>