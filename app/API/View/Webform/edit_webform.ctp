<div class="row">
    <?= $this->Html->script('webforms') ?>
    <script>
        form_type_fields = <?= $form_fields_json?>;
    </script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Редактирование веб-формы <b><?= $webform['name'] ?></b></h3>

        </div>

        <div class="row">
            <div class="col-sm-12 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/webforms">Список веб-форм</a>
                <? if ($webform['enabled'] == 1) { ?>
                    <a class="btn btn-warning btn-bock" href="/webform/block/<?= $webform['id'] ?>"><i
                                class="fa fa-close"></i> Скрыть веб-форму из показа</a>
                <? } else { ?>
                    <a class="btn btn-warning btn-bock" href="/webform/unblock/<?= $webform['id'] ?>"><i
                                class="fa fa-eye"></i> Показывать веб-форму (сейчас скрыта)</a>

                <? } ?>
                <a class="btn btn-danger btn-bock delete_form" href="/webform/delete/<?= $webform['id'] ?>"><i
                            class="fa fa-minus"></i> Удалить данную веб-форму</a>

                <a class="btn btn-default" href="webform/reports/view/<?= $webform['id'] ?>">Смотреть отчеты по
                    форме</a>
            </div>


        </div>

        <hr>

        <form class="form-horizontal" name="edit_webform" method="post" action="/webform/edit/<?= $webform['id'] ?>"
              enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group <? if (key_exists("title", $errors)) {
                    echo "has-error";
                } ?>">
                    <label class="col-lg-3 control-label" for="name">Заглавие</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required
                               placeholder="Название страницы"
                               value="<?= $webform['name'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("title", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="title"><?= $errors['title'] ?></label>
                <? } ?>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">Содержание страницы</label>
                    <div class="col-sm-6">
                        <div class="summernote"><?= $webform['content'] ?></div>
                        <textarea name="content" id="page_content" style="visibility: hidden">
                            <?= $webform['content'] ?>
                        </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">UTM-метка</label>
                    <div class="col-sm-6">
                        <input type='text' name='utm' placeholder='utm-метка формы'
                               class='form-control' value="<?= $webform['utm'] ?>"/>
                    </div>
                </div>

                <select class="main_select data_providers" name="data_provider" style="display: none">

                    <? foreach ($data_providers as $data_provider) {
                        $data_provider = $data_provider['Data_Provider'];
                        $id = $data_provider['id'];
                        $name = $data_provider['name'];
                        ?>
                        <option value="<?= $id ?>">
                            <?= $name ?>
                        </option>
                    <? } ?>
                </select>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="new_field_type">Добавить новое поле</label>
                    <div class="col-sm-9 table-toolbar-left dropdown">
                        <select class="new_field_type main_select use_select2" id="new_field_type" name="field_type">
                            <option value="" disabled>
                                Выберите тип поля
                            </option>
                            <? foreach ($form_fields as $key => $field_type) {
                                ?>
                                <option value="<?= $key ?>">
                                    <?= $field_type ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form_fields_header form_group"></div>

                <? foreach ($form_data as $form_data_elem) {
                    $form_field = $form_data_elem['Webform_Field'];
                    pr($form_data_elem);
                    $provider_list = $form_data_elem['provider_list'] ?? null;
                    ?>

                    <div class="form-group form_fields_container">
                        <input type='hidden' name='webform[id][]' value="<?= $form_field['id'] ?>"/>
                        <label class='col-md-2 control-label'><?= $form_fields[$form_field['field_type']] ?></label>
                        <div class='col-md-2'>
                            <input type='text' name='webform[name][]' placeholder='Название поля формы'
                                   class='form-control' value="<?= $form_field['field_name'] ?>"/>
                        </div>
                        <div class='col-md-2'>
                            <input type='text' name='webform[code][]' placeholder='Код поля формы'
                                   class='form-control' value="<?= $form_field['field_code'] ?>"/>
                        </div>
                        <input name='webform[type][]' type='hidden' value="<?= $form_field['field_type'] ?>"/>
                        <?
                        if ($form_field['field_type'] === "select") { ?>
                            <div class='col-md-2'>
                                <input type='hidden' name='webform[placeholder][]' value='' />
                                <select name='webform[data_provider_id][]' class='form-control'>
                                    <?

                                    foreach ($data_providers as $provider_item) {
                                        $item = $provider_item['Data_Provider'];
                                        ?>
                                        <option value="<?= $item['id'] ?>" <?if($item['id']==$form_field['field_value_list_id']) echo
                                        "selected";?>><?= $item['name'] ?></option>
                                        <?
                                    }
                                    ?>
                                </select></div>

                        <? }
                        else if($form_field['field_type'] === "input_radio"){ ?>
                            <div class='col-md-2'>
                                <input type='text' name='webform[placeholder][]'
                                       value='<?= $form_field['placeholder'] ?>'
                                       value=''
                                       class='form-control'/>
                            </div>
                            <input name='webform[data_provider_id][]' type='hidden' value=''/>
                        <? }
                        else { ?>
                            <div class='col-md-2'>
                                <input type='text' name='webform[placeholder][]'
                                       value='<?= $form_field['placeholder'] ?>'
                                       placeholder='Подсказка поля формы'
                                       class='form-control'/>
                            </div>
                            <input name='webform[data_provider_id][]' type='hidden' value=''/>
                        <? } ?>


                        <div class='col-md-4'>
                            <a class='btn btn-warning btn-bock delete_form_field' href='#'
                               title='Удалить'><span class='fa fa-trash'></span>
                            </a>
                            <a class='btn btn-default btn-bock up_form_field' href='#' title='Перенести вверх'><span
                                        class='fa fa-arrow-up'></span>
                            </a>
                            <a class='btn btn-default btn-bock down_form_field' href='#' title='Перенести вниз'><span
                                        class='fa fa-arrow-down'></span>
                            </a>
                        </div>
                    </div>
                    <?
                }

                ?>


            </div>


            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить поля формы">
                        <a class="btn btn-success btn-bock" href="/webform/view/<?= $webform['id'] ?>">Предварительный
                            просмотр</a>
                    </div>
                </div>
            </div>
        </form>


        <h3>Привязка к категориям</h3>
        <div class="form-group <? if (key_exists("category_id", $errors)) {
            echo "has-error";
        } ?>">
            <label class="col-lg-3 control-label" for="title">Категория страниц</label>
            <div class="col-sm-6">
                <select id="category_id" class="main_select use_select2 city_select form-control" name="category_id">

                    <? foreach ($categories as $category) {
                        $category_name = $category['Static_Category']['name'];
                        $category_id = $category['Static_Category']['id'];
                        ?>
                        <option value="<?= $category_id ?>" <? if ($category_id == 88) {
                            echo ' selected="selected" ';
                        } ?>>
                            <?= $category_name ?>
                        </option>
                    <? } ?>
                </select>

                <? if (key_exists("category_id", $errors)) { ?>
                    <span class="text-danger error-message text-semibold"><?= $errors['category_id'] ?></span>
                <? } ?>
            </div>
        </div>

        <h3>Привязка к страницам</h3>
        <div class="form-group <? if (key_exists("category_id", $errors)) {
            echo "has-error";
        } ?>">
            <label class="col-lg-3 control-label" for="title">Страница</label>
            <div class="col-sm-6">
                <select id="category_id" class="main_select use_select2 city_select form-control" name="category_id">

                    <? foreach ($page_list as $page_list_item) {
                        $page_name = $page_list_item['Static_Page']['title'];
                        $page_id = $page_list_item['Static_Page']['id'];
                        ?>
                        <option value="<?= $page_id ?>" <? if ($page_id == 88) {
                            echo ' selected="selected" ';
                        } ?>>
                            <?= $page_name ?>
                        </option>
                    <? } ?>
                </select>

                <? if (key_exists("category_id", $errors)) { ?>
                    <span class="text-danger error-message text-semibold"><?= $errors['category_id'] ?></span>
                <? } ?>
            </div>
        </div>

    </div>
</div>