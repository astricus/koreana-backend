<div class="row">
    <?= $this->Html->script('webforms') ?>
    <script>

        form_type_fields = <?= $form_fields_json?>;
    </script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Создание веб-формы </h3>

        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/webforms">Список веб-форм</a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_webforms" method="post" action="/webform/add"
              enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group <? if (key_exists("name", $errors)) {
                    echo "has-error";
                } ?>">
                    <label class="col-lg-3 control-label" for="name">Название</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required
                               placeholder="Название веб-формы"
                               value="<?= $add_form_data['name'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("title", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="title"><?= $errors['name'] ?></label>
                <? } ?>


                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">Содержание веб-формы</label>
                    <div class="col-sm-6">
                        <div class="summernote"><?= $add_form_data['content'] ?></div>
                        <textarea name="content" id="page_content" style="visibility: hidden">
                            <?= $add_form_data['content'] ?>
                        </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="name">utm-метка</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="utm" required
                               placeholder="utm-метка веб-формы"
                               value="<?= $add_form_data['utm'] ?>"/>
                    </div>
                </div>

                <select class="main_select data_providers" name="data_provider" style="display: none">

                    <? foreach ($data_providers as $data_provider) {
                        $data_provider = $data_provider['Data_Provider'];
                        $id = $data_provider['id'];
                        $name = $data_provider['name'];
                        ?>
                        <option value="<?= $id ?>">
                            <?= $name ?>
                        </option>
                    <? } ?>
                </select>

                <h3 class="panel-title">Конструктор веб-формы </h3>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">Добавить новое поле</label>


                    <?
                    /*
                                        $field_types = [
                                            'input_text' => ['title' => 'Однострочное текстовое поле', 'name' => '', 'type' => 'input_text', 'code' => '', 'placeholder' => ''],
                                            'textarea' => ['title' => 'Многострочное текстовое поле', 'name' => '', 'type' => 'textarea', 'code' => '', 'placeholder' => ''],
                                            'input_radio' => ['title' => 'Элемент Radio', 'name' => '', 'type' => 'input_radio', 'code' => '', 'placeholder' => ''],
                                            'input_checkbox' => ['title' => 'Элемент Checkbox', 'name' => '', 'type' => 'input_checkbox', 'code' => '', 'placeholder' => ''],
                                            'select' => ['title' => 'Выпадающий список', 'name' => '', 'type' => 'select', 'code' => '', 'placeholder' => ''],
                                            'input_date' => ['title' => 'Поле типа Дата', 'name' => '', 'type' => 'input_date', 'code' => '', 'placeholder' => ''],
                                            'input_name' => ['title' => 'Поле типа ФИО/Название', 'name' => '', 'type' => 'input_name', 'code' => '', 'placeholder' => ''],
                                            'input_phone' => ['title' => 'Поле типа Номер Телефона', 'name' => '', 'type' => 'input_phone', 'code' => '', 'placeholder' => ''],
                                            'input_email' => ['title' => 'Поле типа Email', 'name' => '', 'type' => 'input_email', 'code' => '', 'placeholder' => ''],
                                        ];*/

                    ?>

                    <div class="col-sm-9 table-toolbar-left dropdown">
                        <select class="new_field_type main_select use_select2" name="field_type">
                            <option value="" disabled>
                                Выберите тип поля
                            </option>
                            <? foreach ($form_fields as $key => $field_type) {
                                ?>
                                <option value="<?= $key ?>">
                                    <?= $field_type ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form_fields_header form_group"></div>


            </div>


            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>