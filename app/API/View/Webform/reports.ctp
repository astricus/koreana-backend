<script>
    $(document).ready(function () {

        /* Поиск по названию */
        $(document).on("change", ".find_by_name", function () {
            let find_by_name = $('.find_by_name').val(), new_url;
            if (find_by_name.length >= 2) {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', find_by_name);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', '');
            }
            redirect(new_url);
        });

    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Отчеты по заполненным вебформам</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/webforms" class="btn btn-success">показать все веб-формы</a>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <label class="label control-label label-info" for="find_by_name">поиск веб-формы по
                            названию</label>
                        <input id="find_by_name" type="text" placeholder="поиск веб-формы по названию"
                               class="main_select use_select2 find_by_name" name="find_by_name"
                               value="<?= $form_data['find_by_name'] ?>">
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($webform_reports) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название формы</th>
                            <th>Заполнена</th>
                            <th>Пользователь</th>
                            <th>Результат заполнения</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list['new'] = array('warning', 'Новая форма');
                        $status_list['inwork'] = array('danger', 'Форма в работе');
                        $status_list['closed'] = array('success', 'Форма обработана и закрыта');

                        foreach ($webform_reports as $report_elem) {
                            $name = $report_elem['Webform']['name'];
                            $rid = $report_elem['Webform_User_Data']['id'];
                            $webform_id = $report_elem['Webform_User_Data']['webform_id'];
                            if ($report_elem['User']['id'] != null) {
                                $user_prefix = "Пользователь";
                                $user_name = prepare_fio($report_elem['User']['firstname'], $report_elem['User']['lastname'], $report_elem['User']['middlename']);
                            } else {
                                $user_prefix = "Администратор";
                                $user_name = prepare_fio($report_elem['Manager']['firstname'], $report_elem['Manager']['lastname'], "");
                            }
                            $user_string = $user_prefix . " " . $user_name;
                            $n++;
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td>Форма <a class="btn-link" href="/webform/view/<?= $webform_id ?>"><?= $name ?></a>
                                </td>
                                <td><a href="/webform/report/view/<?=$rid?>" class="btn-link"><i class="pli-clock"></i> <?= lang_calendar($report_elem['Webform_User_Data']['created']) ?>
                                    </a>
                                </td>
                                <td>
                                    <?= $user_string ?>
                                </td>
                                <td>
                                    <?= $report_elem['Webform_User_Data']['result'] ?>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$report_elem['Webform_User_Data']['status']][0] ?>"><?= $status_list[$report_elem['Webform_User_Data']['status']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($report_elem['Webform_User_Data']['status'] == "new") { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-danger fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/inwork"
                                               title="Перевести в работу" data-container="body"></a>
                                            <a class="btn btn-sm btn-success btn-hover-success fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/closed"
                                               title="Обработана и закрыта" data-container="body"></a>
                                        <? } else if ($report_elem['Webform_User_Data']['status'] == "inwork") { ?>
                                            <a class="btn btn-sm btn-warning btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/new"
                                               title="Перевести в новую" data-container="body"></a>
                                            <a class="btn btn-sm btn-success btn-hover-success fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/closed"
                                               title="Обработана и закрыта" data-container="body"></a>

                                            &nbsp;<? } else { ?>
                                            <a class="btn btn-sm btn-warning btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/new"
                                               title="Открыть веб-форму" data-container="body"></a>
                                            <a class="btn btn-sm btn-danger btn-hover-danger fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/inwork"
                                               title="Открыть веб-форму" data-container="body"></a>

                                            &nbsp;<? } ?>

                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/webforms/";
                            $params = array(
                                'find_by_name' => $form_data['find_by_name']
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Веб-форм нет</p>
            <? } ?>

        </div>
    </div>
</div>