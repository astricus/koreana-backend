<script>
    $(document).ready(function () {

        /* Поиск по названию */
        $(document).on("change", ".find_by_name", function () {
            let find_by_name = $('.find_by_name').val(), new_url;
            if (find_by_name.length >= 2) {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', find_by_name);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', '');
            }
            redirect(new_url);
        });

    });

</script>

<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Отчет по веб-форме <b><?= $report[0]['Webform']['name'] ?></b></h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/webform/reports" class="btn btn-success">показать все отчеты по веб-формам</a>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($report) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Заполнена</th>
                            <th>Кто заполнил</th>
                            <th>Результат заполнения</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list['new'] = array('warning', 'Новая форма');
                        $status_list['inwork'] = array('danger', 'Форма в работе');
                        $status_list['closed'] = array('success', 'Форма обработана и закрыта');

                        foreach ($report as $report_elem) {
                            $name = $report_elem['Webform']['name'];
                            $rid = $report_elem['Webform_User_Data']['id'];
                            $webform_id = $report_elem['Webform_User_Data']['webform_id'];
                            if ($report_elem['User']['id'] != null) {
                                $user_prefix = "Пользователь";
                                $user_name = prepare_fio($report_elem['User']['firstname'], $report_elem['User']['lastname'], $report_elem['User']['middlename']);
                            } else {
                                $user_prefix = "Администратор";
                                $user_name = prepare_fio($report_elem['Manager']['firstname'], $report_elem['Manager']['lastname'], "");
                            }
                            $user_string = $user_prefix . " " . $user_name;
                            $n++;

                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td>Форма <a class="btn-link" href="/webform/view/<?= $webform_id ?>"><?= $name ?></a>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($report_elem['Webform_User_Data']['created']) ?>
                                    </span>
                                </td>
                                <td>
                                    <?= $user_string ?>
                                </td>
                                <td>
                                    <?= $report_elem['Webform_User_Data']['result'] ?>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$report_elem['Webform_User_Data']['status']][0] ?>"><?= $status_list[$report_elem['Webform_User_Data']['status']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($report_elem['Webform_User_Data']['status'] == "new") { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-danger fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/inwork"
                                               title="Перевести в работу" data-container="body"></a>
                                            <a class="btn btn-sm btn-success btn-hover-success fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/closed"
                                               title="Обработана и закрыта" data-container="body"></a>
                                        <? } else if ($report_elem['Webform_User_Data']['status'] == "inwork") { ?>
                                            <a class="btn btn-sm btn-warning btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/new"
                                               title="Перевести в новую" data-container="body"></a>
                                            <a class="btn btn-sm btn-success btn-hover-success fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/closed"
                                               title="Обработана и закрыта" data-container="body"></a>

                                            &nbsp;<? } else { ?>
                                            <a class="btn btn-sm btn-warning btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/new"
                                               title="Открыть веб-форму" data-container="body"></a>
                                            <a class="btn btn-sm btn-danger btn-hover-danger fa fa-unlock add-tooltip"
                                               href="/webform/report/status/<?= $rid ?>/inwork"
                                               title="Открыть веб-форму" data-container="body"></a>

                                            &nbsp;<? } ?>

                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Название поля</th>
                            <th>Тип поля</th>
                            <th>Код поля</th>
                            <th>Выбранное значение</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?
                        foreach ($fields as $field) {

                        $data_list = $field['Webform_User_Data_List'] ?? null;
                        $field_element = $field['Webform_Field'];
                        $field_value = $field['Form_Data_List'] ?? null;
                        if($field_element['field_type']!=="select"){
                            $result_field_value = $data_list['field_value'];
                        } else {
                            $result_field_value = $field_value['name'];
                        }
                        ?>
                        <tr class="">
                            <td>
                                <?= $field_element['field_name'] ?>
                            </td>
                            <td>
                                <?= $field_element['field_type'] ?>
                            </td>
                            <td>
                                <?= $field_element['field_code'] ?>
                            </td>
                            <td>
                                <?= $result_field_value ?>
                            </td>
                        </tr>

                            <?
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

            <? } else { ?>

                <p class="muted">Отчет по веб-форме не найден</p>
            <? } ?>

        </div>
    </div>
</div>