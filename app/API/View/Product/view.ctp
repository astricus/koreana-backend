<div class="panel-body">

    <h5><?= trim($product['Product']['product_name']) ?>, артикул <?= $barcode ?> </h5>

    <hr>

    <div class="row">
        <div class="col-sm-2">Категория товара</div>
        <div class="col-sm-7"><?= $category ?></div>
        <div class="col-sm-3">
            <?= moderation_message('category', $moderations_data); ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-2">Название товара</div>
        <div class="col-sm-7"><?= $product['Product']['product_name'] ?></div>
        <div class="col-sm-3">
            <?= moderation_message('product_name', $moderations_data); ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-2">Галерея</div>
        <div class="col-sm-7">
            <div id="product_gallery">

                <?
                $id = $product['Product']['id'];
                if (count($product_images) > 0) {
                    $big_image = "";
                    $small_image = "";
                    $normal_image = "";
                    $old_image_id = "";
                    foreach ($product_images as $product_image) {
                        $product_image = $product_image['Product_Image'];
                        $img_path = site_url() . "/" . "files" . "/" . "product_images" . "/p" . $id . "/";
                        $def_img_path = site_url() . "/img/design/";
                        $real_foto = Configure::read('USER_FILE_UPLOAD_DIR') . DS . "p" . $id . DS . $product_image['file'];

                        if ((!empty($product_image['file'])) AND (file_exists($real_foto))) {
                            $image = $img_path . $product_image['file'];

                        } else {
                            $image = $def_img_path . "default_user_icon.png";
                        }

                        if($product_image['type'] == "big") {
                            $big_image = $image;
                        } else if($product_image['type'] == "normal") {
                            $small_image = $image;
                        }
                        if($big_image!="" AND $small_image!=""){
                        ?>
                        <img alt="<?= $product['Product']['product_name'] ?>"
                             src="<?=$small_image?>"
                             data-image="<?=$big_image?>">
                    <?     $big_image = "";
                            $small_image ="";
                        }
                    }
                } ?>

            </div>
        </div>
        <div class="col-sm-3">
            <?= moderation_message('images', $moderations_data); ?>
        </div>
    </div>


    <hr>

    <div class="row">
        <div class="col-sm-2">Описание товара</div>
        <div class="col-sm-7"><?= $product['Product']['description'] ?></div>
        <div class="col-sm-3">
            <?= moderation_message('description', $moderations_data); ?>
        </div>
    </div>

    <hr>

    <!--        <div class="form-group">-->
    <!--            <label class="col-sm-3 control-label" for="use_build_name">Использовать составное название (Задается в-->
    <!--                настройках категории)</label>-->
    <!--            <div class="col-sm-6">-->
    <!--                <input id="use_build_name" class="magic-checkbox" type="checkbox">-->
    <!--                <label class="col-sm-3" for="use_build_name"></label>-->
    <!--            </div>-->
    <!--        </div>-->

    <div class="row">
        <div class="col-sm-2">Страна производства</div>
        <div class="col-sm-7">
            <?= $country['Country']['name'] ?? "не указана страна производитель"; ?>
        </div>
        <div class="col-sm-3">
            <?= moderation_message('country', $moderations_data); ?>
        </div>
    </div>


    <hr class="divider">

    <div class="row">
        <div class="col-sm-2">Производитель</div>
        <div class="col-sm-7">
            <?= $manufacturer['Manufacturer']['name'] ?? "не указана страна производитель"; ?>
        </div>
        <div class="col-sm-3">
            <?= moderation_message('manufacturer', $moderations_data); ?>
        </div>
    </div>

    <hr class="divider">

    <div class="row">
        <div class="col-sm-2">Бренд / Торговая марка</div>
        <div class="col-sm-7">
            <?= $brand['Brand']['name'] ?? "не указана страна производитель"; ?>
        </div>
        <div class="col-sm-3">
            <?= moderation_message('brand', $moderations_data); ?>
        </div>
    </div>

    <hr class="divider">

    <h4>Универсальные параметры</h4>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="package_count">Кол-во единицы товара в упаковке, шт</label>
        <div class="col-sm-6">
            <input placeholder="Кол-во единицы товара в упаковке, шт" class="form-control" id="package_count"
                   type="text" name="package_count" required style="width: 300px">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="package_widht">Ширина упаковки, м </label>
        <div class="col-sm-6">
            <input placeholder="Ширина упаковки, м" class="form-control" id="package_widht" type="text"
                   name="package_widht" required style="width: 300px">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="package_height">Высота упаковки, м </label>
        <div class="col-sm-6">
            <input placeholder="Длина упаковки, м" class="form-control" id="package_height" type="text"
                   name="package_height" required style="width: 300px">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="package_length">Длина упаковки, м </label>
        <div class="col-sm-6">
            <input placeholder="Высота упаковки, м" class="form-control" id="package_length" type="text"
                   name="package_length" required style="width: 300px">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="package_weight">Вес брутто (товар + упаковка), кг</label>
        <div class="col-sm-6">
            <input placeholder="Вес брутто (товар + упаковка), кг" class="form-control" id="package_weight"
                   type="text" name="package_weight" required style="width: 300px">
        </div>
    </div>

    <hr class="divider">

    <h4>Параметры товаров в категории <span class="selected_category_name"></span></h4>

    <div class="form-group category_product_param_box">
        <div class="well well-xs no_category_info_message">Не выбрана категория товара! Для добавления товара
            необходимо выбрать категорию.
        </div>
    </div>

    <hr class="divider">

    <h4>Предложение по товару</h4>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="shop_id">В каком магазине будет продаваться товар</label>
        <div class="col-sm-6">
            <select id="shop_ids" class="main_select use_select2" multiple name="shop_ids[]" style="width: 300px">
                <? foreach ($shops as $shop) { ?>
                    <option value="<?= $shop['Shop']['id'] ?>">
                        <?= $shop['Shop']['shop_name'] ?></option>
                <? } ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label" for="base_price">Базовая цена, руб</label>
        <div class="col-sm-6">
            <input placeholder="Базовая цена, руб" class="form-control" id="base_price" type="text"
                   name="base_price" required style="width: 300px">
        </div>
    </div>

</div>


<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список
                товаров <?= tooltip("список ваших товаров", "здесь показан список продаваемых вами товаров во всех магазинах") ?> </h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-3">Название товара</div>
                <div class="col-sm-9"> <?= $product['Product']['product_name'] ?>,</div>
            </div>

            <div class="row">
                <div class="col-sm-3">Фото товара</div>
                <div class="col-sm-9">
                    <input class="form-control" id="upload_image" type="file" name="file" autocomplete="off">
                </div>
            </div>

        </div>
    </div>
</div>

<!--Custom script [ DEMONSTRATION ]-->
<!--===================================================-->
<script>
    $(document).on('nifty.ready', function () {

        $("#product_gallery").unitegallery({
            tile_enable_shadow: false
        });

        /*
            $("#demo-gallery-2").unitegallery({
                tile_enable_shadow: false,
                theme_navigation_type: "arrows"
            });
            */

    });
</script>