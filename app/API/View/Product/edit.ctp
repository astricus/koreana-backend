<form class="form-horizontal" name="add_product" method="post" action="/product/add" enctype="multipart/form-data">
    <div class="panel-body">

        <h4>Общая информация о товаре</h4>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="category_id">Категория товара</label>
            <div class="col-sm-6">
                <select id="category_id" class="main_select use_select2 productCategoryParamsSearch" name="category_id"
                        style="width: 300px">
                    <option value="">товар не привязан к категории</option>
                    <? foreach ($categories as $category) { ?>
                        <option value="<?= $category['ProductCategory']['id'] ?>">
                            <?= $category['use_build_name']['name'] ?></option>
                    <? } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="upload_image">Фото товара</label>
            <div class="col-sm-6">
                <input class="form-control" id="upload_image" type="file" name="file" autocomplete="off">
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Название товара</label>
            <div class="col-sm-6">
                <input placeholder="Название товара" class="form-control" id="name" type="text" name="name" required value="<?=$product['Product']['product_name']?>"
                       style="width: 300px" autocomplete="off">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="use_build_name">Использовать составное название (Задается в
                настройках категории)</label>
            <div class="col-sm-6">
                <input id="use_build_name" class="magic-checkbox" type="checkbox">
                <label class="col-sm-3" for="use_build_name"></label>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="product_description">Описание товара*</label>
            <div class="col-sm-6">
                <textarea placeholder="Описание товара" class="form-control" name="description" id="product_description" required cols="50" rows="6" autocomplete="off" ></textarea>
            </div>
        </div>
        <small>* Внимание! В данное поле не следует вписывать характеристики товара, по которым ведется фильтрация и
            поиск, они добавляются ниже на этой странице,
            также сюда не следует писать страну изготовления, производителя, бренд. Также это поле не предназначено для
            указания предложений по товару (наличие, ценовых категорий, акций, сроков
            доставки и прочего)
        </small>

        <hr class="divider">

        <div class="form-group">
            <label class="col-sm-3 control-label" for="country_id">Страна производства</label>
            <div class="col-sm-6">
                <select id="country_id" class="main_select use_select2" name="country_id" style="width: 300px">
                    <option value="">у товара не указан страна производитель</option>
                    <? foreach ($countries as $country) { ?>
                        <option value="<?= $country['Country']['id'] ?>">
                            <?= $country['Country']['name'] ?>
                        </option>
                    <? } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="manufacturer_id">Производитель</label>
            <div class="col-sm-6">
                <select id="manufacturer_id" class="main_select use_select2" name="manufacturer_id"
                        style="width: 300px">
                    <option value="">у товара не указан производитель</option>
                    <? foreach ($manufacturers as $manufacturer) { ?>
                        <option value="<?= $manufacturer['Manufacturer']['id'] ?>">
                            <?= $manufacturer['Manufacturer']['name'] ?></option>
                    <? } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="brand_id">Бренд / Торговая марка</label>
            <div class="col-sm-6">
                <select id="brand_id" class="main_select use_select2" name="brand_id" style="width: 300px">
                    <option value="">у товара не указан бренд</option>
                    <? foreach ($brands as $brand) { ?>
                        <option value="<?= $brand['Brand']['id'] ?>">
                            <?= $brand['Brand']['name'] ?></option>
                    <? } ?>
                </select>
            </div>
        </div>

        <hr class="divider">

        <h4>Универсальные параметры</h4>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="package_count">Кол-во единицы товара в упаковке, шт</label>
            <div class="col-sm-6">
                <input placeholder="Кол-во единицы товара в упаковке, шт" class="form-control" id="package_count" type="text" name="package_count" required style="width: 300px">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="package_widht">Ширина упаковки, м </label>
            <div class="col-sm-6">
                <input placeholder="Ширина упаковки, м" class="form-control" id="package_widht" type="text" name="package_widht" required style="width: 300px">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="package_height">Высота упаковки, м </label>
            <div class="col-sm-6">
                <input placeholder="Длина упаковки, м" class="form-control" id="package_height" type="text" name="package_height" required style="width: 300px">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="package_length">Длина упаковки, м </label>
            <div class="col-sm-6">
                <input placeholder="Высота упаковки, м" class="form-control" id="package_length" type="text" name="package_length" required style="width: 300px">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="package_weight">Вес брутто (товар + упаковка), кг</label>
            <div class="col-sm-6">
                <input placeholder="Вес брутто (товар + упаковка), кг" class="form-control" id="package_weight"
                       type="text" name="package_weight" required style="width: 300px">
            </div>
        </div>

        <hr class="divider">

        <h4>Параметры товаров в категории <span class="selected_category_name"></span></h4>

        <div class="form-group category_product_param_box">
            <div class="well well-xs no_category_info_message">Не выбрана категория товара! Для добавления товара необходимо выбрать категорию.</div>
        </div>


        <hr class="divider">

        <h4>Предложение по товару</h4>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="shop_id">В каком магазине будет продаваться товар</label>
            <div class="col-sm-6">
                <select id="shop_ids" class="main_select use_select2" multiple name="shop_ids[]" style="width: 300px">
                    <? foreach ($shops as $shop) { ?>
                        <option value="<?= $shop['Shop']['id'] ?>">
                            <?= $shop['Shop']['shop_name'] ?></option>
                    <? } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="base_price">Базовая цена, руб</label>
            <div class="col-sm-6">
                <input placeholder="Базовая цена, руб" class="form-control" id="base_price" type="text" name="base_price" required style="width: 300px">
            </div>
        </div>

        <div class="form-group add_phone_field">
            <label class="col-sm-3 control-label">Добавить предложение</label>
            <div class="col-sm-9">
                <input class="form-control phone_mask add_phone_field" type="text" name="phone[]" style="width: 300px" autocomplete="off">
            </div>
        </div>

    </div>

    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-9 col-sm-offset-3">
                <button class="btn btn-success" type="submit">Создать товар</button>
            </div>
        </div>
    </div>
</form>