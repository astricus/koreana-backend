<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список товаров</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left">
                        <a href="/product/add" class="btn btn-success btn-bock">Создать карточку товара</a>

                        <a href="/?view=list" class="btn btn-default" title="Показ товаров списком"><i
                                    class="fa fa-align-justify"></i></a>
                        <a href="/view=images" class="btn btn-default" title="Показ товаров изображениями"><i
                                    class="fa fa-th"></i></a>
                    </div>

                    <div class="col-sm-2 table-toolbar-right dropdown">
                        <input id="dropdownObjectSearch" placeholder="Поиск по категории"
                               class="form-control dropdown-toggle"
                               aria-haspopup="true" data-toggle="dropdown" role="button" autocomplete="off" type="text"
                               aria-expanded="false">
                        <div class="dropdown-menu open" role="combobox" aria-labelledby="dropdownObjectSearch">
                            <ul class="dropdown-menu inner search_ajax_object_box" role="listbox" aria-expanded="false">
                                <li data-original-index="0" class="selected">
                                    <a tabindex="0" data-tokens="null" role="option" aria-disabled="false"
                                       aria-selected="true"
                                       data-id="">
                                        <span class="text">Все</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-2 table-toolbar-left dropdown">
                        <select id="shop_id" class="main_select use_select2 dropdownShopSelect" name="shop_id" style="width: 300px">
                            <option value="0">По наличию товара в магазине</option>
                            <? foreach ($shops as $shop_elem) {
                                $shop_elem = $shop_elem['Shop'];
                                $shop_id = $shop_elem['id'];
                                $shop_name = $shop_elem['shop_name'];
                                ?>
                                <option value="<?= $shop_id ?>" <? if ($form_data['shop_id'] == $shop_id) echo 'selected'; ?>><?= $shop_name ?></option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-2 table-toolbar-right dropdown">
                        <input id="dropdownUserSearch" placeholder="Поиск по товару"
                               class="form-control dropdown-toggle"
                               aria-haspopup="true" data-toggle="dropdown" role="button" autocomplete="off" type="text"
                               aria-expanded="false">
                        <div class="dropdown-menu open" role="combobox" aria-labelledby="dropdownUserSearch">
                            <ul class="dropdown-menu inner search_ajax_user_box" role="listbox" aria-expanded="false">
                                <li data-original-index="0" class="selected">
                                    <a tabindex="0" data-tokens="null" role="option" aria-disabled="false"
                                       aria-selected="true"
                                       data-id="">
                                        <span class="text">Все</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <input id="search_ajax_active_only" class="magic-checkbox"
                               type="checkbox" <? if ($form_data['active_only'] == "on") echo 'checked'; ?>>
                        <label for="search_ajax_active_only">Только активные товары</label>
                    </div>

                    <div class="col-sm-2">
                        <input id="search_ajax_my_only" class="magic-checkbox"
                               type="checkbox" <? if ($form_data['stopped_offers'] == "on") echo 'checked'; ?>>
                        <label for="search_ajax_my_only">Остановленные предложения</label>
                    </div>

                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr class="">
                        <th>№</th>
                        <th scope="col">Картинка</th>
                        <th scope="col" class="sorting">Название</th>
                        <th scope="col" class="sorting">Артикул</th>
                        <th scope="col" class="sorting">Категория</th>
                        <? /*
        <th scope="col">Предложений</th>
        <th scope="col">Выбран раз</th>
        <th scope="col">Был показан раз</th>  */ ?>
                        <th scope="col" class="sorting">Товар создан</th>
                        <th scope="col">Предложений</th>
                        <th>Статус</th>
                        <th scope="col" class="actions">Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $n = 0;
                    foreach ($products as $product):
                        $n++;
                        $id = $product['Product']['id'];

                        $img_path = site_url() . "/" . "files" . "/" . "product_images" . "/p" . $product['Product']['id'] . "/";
                        $def_img_path = site_url() . "/img/design/";
                        $real_foto = Configure::read('USER_FILE_UPLOAD_DIR') . DS . "p" . $product['Product']['id'] . DS . $product['image'];

                        if ((!empty($product['image'])) AND (file_exists($real_foto))) {
                            $image = $img_path . $product['image'];

                        } else {
                            $image = $def_img_path . "default_user_icon.png";
                        }

                        ?>
                        <tr class="<? if ($product['Product']['status'] == "active") {
                            echo 'status_view_list';
                        } else {
                            echo 'status_view_hidden';
                        } ?>">
                            <td><?= $n ?></td>
                            <td width="60">
                                <a class="" href="/product/edit/<?= $product['Product']['id'] ?>">
                                    <div class="product_image"
                                         style="background-image: url(<?php echo $image ?>);"></div>
                                </a>
                            </td>
                            <td>

                                <a class="main_link"
                                   href="/product/edit/<?= $product['Product']['id'] ?>"><?= $product['Product']['product_name'] ?></a>
                            </td>
                            <td>
                                <?= $product['system_barcode'] ?>
                            </td>
                            <td style="max-width: 220px">
                                <? if ($product['Product']['category_id'] == 0) {
                                    ?>
                                    <a href="/product/edit/<?= $product['Product']['id'] ?>"
                                       class="btn btn-warning">Товар не
                                        привязан к категории</a>

                                <? } else { ?>
                                    <a class="main_link"
                                       href="/products/?category_id=<?= $product['Product']['category_id'] ?>"><?= $product['ProductCategory']['name'] ?></a>
                                    Полная категория:
                                    <?= $product['category_build_name'] ?>
                                <? } ?>
                            </td>
                            <? /*
            <td><?= $product['offer_count'] ?></td>
            <td><?= $product['order_count'] ?></td>*/ ?>

                            <td><?= lang_calendar($product['Product']['created']) ?></td>
                            <td><?= 354;//$product['product_views']    ?></td>
                            <td>
                                <?
                                if ($product['Product']['status'] == "active") { ?>
                                    <div class="label label-success">активен</div>
                                <? } else if($product['Product']['status'] == "moderate") {  ?>
                                    <div class="label label-warning">в обработке</div>
                                <? }else if($product['Product']['status'] == "stopped") {  ?>
                                    <div class="label label-danger">недоступен</div>
                                <?}?>

                            </td>

                            <td class="actions">
                                <? /*
                <? if($product['Product']['status'] == "active") {?>
                    <a href="/product/block/<?=$product['Product']['id']?>" class="btn  btn-success" title="Заблокировать"><i class="ion-locked"></i></a>
                <?} else {?>
                    <a href="/product/unblock/<?=$product['Product']['id']?>" class="btn btn-danger" title="Разблокировать"><i class="ion-unlocked"></i></a>
                <?}?>
                */
                                ?>
                                <a href="/product/add_offer/<?= $product['Product']['id'] ?>" class="btn btn-default btn-hover-success" title="Предложения по товару"><i class="pli-coins"></i></a>
                                <a href="/product/edit/<?= $product['Product']['id'] ?>" class="btn btn-default btn-hover-success" title="Управление"><i class="fa  fa-cogs"></i></a>
                                <a href="/offer/stat/<?= $product['Product']['id'] ?>" class="btn btn-default btn-hover-success" title="Аналитика"><i class="pli-bar-chart"></i></a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class="fixed-table-pagination">
                <div class="pull-right pagination">
                    <ul class="pagination">
                        <?php $ctrl_pgn = "/products/";
                        $params = array(
//                                'category_id' => $category_id,
//                                'source' => $source,
                        );
                        echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Custom script [ DEMONSTRATION ]-->
<!--===================================================-->
<script>
    $(document).on('nifty.ready', function () {

        $("#product_gallery").unitegallery({
            tile_enable_shadow: false
        });

        /*
            $("#demo-gallery-2").unitegallery({
                tile_enable_shadow: false,
                theme_navigation_type: "arrows"
            });
            */

    });
</script>