<div class="panel panel-success panel-colorful media middle pad-all">
    <div class="media-left">
        <div class="pad-hor">
            <i class="fa fa-users-cog icon-3x"></i>
        </div>
    </div>
    <div class="media-body">
        <p class="text-2x mar-no text-semibold">User Api</p>
        <p class="mar-no">api для работы с пользователями</p>
    </div>
</div>
<?
$empty_order = "------";
if (count($users) > 0) { ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-body">

                <div class="pad-btm form-inline">

                    <div class="row">

                        <div class="col-sm-2 table-toolbar-left dropdown">
                            <select id="orderer_id" class="main_select use_select2 dropdownOrdererSelect"
                                    name="orderer_id" style="width: 300px">
                                <option value="0">Не выбрано</option>
                                <?/* foreach ($orderers as $orderer) { ?>
                                    <option value="<?= $orderer['Orderer']['id'] ?>" <? if ($form_data['orderer_id'] == $orderer['Orderer']['id']) echo 'selected'; ?>>
                                        <?= prepare_fio($orderer['Orderer']['firstname'], $orderer['Orderer']['lastname'], "") ?>
                                    </option>
                                <? }*/ ?>
                            </select>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-vcenter mar-top">
                            <thead>
                            <tr>
                                <th class="min-w-td">#</th>
                                <th class="min-w-td">Пользователь</th>
                                <th>Логин</th>
                                <th>Пользовательские данные</th>
                                <th>Статус</th>
                                <th class="text-center">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?
                            $t = 0;
                            foreach ($users as $user) {
                                $t++; ?>
                                <tr>
                                    <td class="min-w-td"><?= $t ?></td>
                                    <td><img src="img/profile-photos/1.png" alt="Profile Picture"
                                             class="img-circle img-sm"></td>
                                    <td><a class="btn-link"
                                           href="/orderer/<?= $orderer['Orderer']['id'] ?>"><?= prepare_fio($orderer['Orderer']['firstname'], $orderer['Orderer']['lastname'], "") ?></a>
                                    </td>
                                    <td><?= $orderer['orders_count'] ?></td>
                                    <td>
                                        <? if (isset($orderer['last_order_id'])) {
                                            ?><a class="btn-link" href="/order/<?= $orderer['last_order_id'] ?>">заказ
                                            №<?= $orderer['last_order_id'] ?>
                                            от <?= $orderer['last_order_date'] ?></a><? } else {
                                            echo $empty_order;
                                        } ?></td>
                                    <td><? if ($orderer['Orderer']['status'] == "new") { ?><span
                                                class="label label-table label-warning">Не активирован</span><?
                                        } else if ($orderer['Orderer']['status'] == "active") { ?><span
                                                class="label label-table label-info">Активирован</span>
                                        <? } else { ?>
                                            <span class="label label-table label-danger">Блокирован</span>
                                        <?
                                        }
                                        ?></td>

                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-default btn-hover-success pli-pen-5 add-tooltip"
                                               href="#" data-original-title="Редактировать" data-container="body"></a>
                                            <a class="btn btn-sm btn-default btn-hover-warning pli-unlock add-tooltip"
                                               href="#" data-original-title="Заблокировать заказчика"
                                               data-container="body"></a>
                                        </div>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                        <hr>
                        <!--Pagination-->
                        <div class="fixed-table-pagination">
                            <div class="pull-right pagination">
                                <ul class="pagination">
                                    <?php $ctrl_pgn = "/user/list";
                                    $params = array(
                                    );
                                    echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <? } ?>