<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Блокировки запросов от мобильного приложения с адреса <?=$ip?> (всего запросов - <?=$total_count?>) </h3>

        </div>

        <div class="panel-body">

            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <? if($ip_status == "blocked"){?>
                            <a href="/mobile/unblock_ip/<?=$ip?>" class="btn btn-success">Разблокировать ip адрес</a>
                        <?} else {?>
                            <a href="/mobile/block_ip/<?=$ip?>" class="btn btn-danger">Блокировать ip адрес</a>
                        <?} ?>
                        <a href="/mobile/requests_by_ip/<?=$ip?>" class="btn btn-success">История запросов api с данного ip адреса</a>

                    </div>

                </div>

            </div>

            <? if (count($block_list) > 0) { ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Правило</th>
                            <th>Время блокировки</th>
                            <th>Время разблокировки</th>
                            <th>Текущий статус</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n           = 0;

                        foreach ($block_list as $block) {
                            $n++;
                            $block_data = $block['Blocked_Ip'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><?= $block_data['rule'] ?></td>
                                <td>
                                    <span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $block_data['created']
                                        ) ?></span>
                                </td>
                                <td>
                                    <span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $block_data['expired']
                                        ) ?></span>
                                </td>
                                <td><? if($block_data['status'] == "blocked"){?>
                                        <p class=text-danger">Заблокирован</p>
                                    <?} else {?>
                                        <p class=text-success">Доступ открыт</p>
                                    <?}?>
                                </td>

                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/mobile/blocks_by_ip/" . $ip;
                            $params         = [];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>


            <? } else { ?>
                <p class="muted">Блокировок нет</p>
            <? } ?>

        </div>

    </div>
</div>