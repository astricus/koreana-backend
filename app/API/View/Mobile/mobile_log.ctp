<div class="row">
    <script>
        $(document).ready(function () {
            /* component  */
            $(document).on("change", ".component_select", function () {
                let component = $('.component_select option:selected').val(), new_url;
                if (component !="") {
                    new_url = updateQueryStringParameter(window.location.href, 'component', component);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'component', '');
                }
                redirect(new_url);
            });

            /* Клиент */
            $(document).on("change", ".client_select", function () {
                let client_id = $('.client_select option:selected').val(), new_url;
                if (client_id != "") {
                    new_url = updateQueryStringParameter(window.location.href, 'client_id', client_id);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'client_id', '');
                }
                redirect(new_url);
            });

            /* Сортировка */
            $(document).on("change", ".sort_select", function () {
                let sort = $('.sort_select option:selected').val(), new_url;
                if (sort != "") {
                    new_url = updateQueryStringParameter(window.location.href, 'sort', sort);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'sort', '');
                }
                redirect(new_url);
            });

            /* Дата запроса */
            $(document).on("change", ".request_date", function () {
                let request_date = $('.request_date').val(), new_url;
                if (request_date != "") {
                    new_url = updateQueryStringParameter(window.location.href, 'request_date', request_date);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'request_date', '');
                }
                redirect(new_url);
            });

        });

    </script>
    <div class="panel">
        <div class="panel-heading">

            <h3 class="panel-title">Данные запросов от мобильного приложения</h3>
        </div>
        <div class="panel-body">

            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/mobile/" class="btn btn-success">показать все запросы</a>
                    </div>

                    <div class="col-sm-2 table-toolbar-left dropdown">
                        <select id="client_id" class="main_select use_select2 client_select" name="client_id">

                            <option value="0">Все клиенты</option>
                            <? foreach ($clients as $client) {
                                $client_name = prepare_fio($client["firstname"], $client["lastname"], "") . " " .
                                    $client["phone"];
                                ?>
                                <option value="<?= $client['id'] ?>" <? if ($client['id'] == $form_data['client_id']) echo 'selected'; ?>>
                                    <?= $client_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-2 table-toolbar-left dropdown">
                        <select id="component" class="main_select use_select2 component_select" name="component">

                            <option value="">Все компоненты</option>
                            <? foreach ($components as $component) {
                                ?>
                                <option value="<?= $component ?>" <? if ($component == $form_data['component']) echo 'selected'; ?>>
                                    <?= $component ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-2 table-toolbar-left dropdown">
                        <select id="sort" class="main_select use_select2 sort_select" name="sort">

                            <option value="created" <? if ($form_data['sort'] == "created") echo 'selected'; ?>>По дате запроса</option>
                            <option value="time" <? if ($form_data['sort'] == "time") echo 'selected'; ?>>По времени обработки</option>

                        </select>
                    </div>

                    <div class="col-sm-3 dropdown">
                        <label class="control-label" for="request_date">Дата запроса</label>
                        <input type="date" class="request_date" name="request_date"
                               value="<? if (isset($form_data['request_date'])) echo $form_data['request_date']; ?>"/>
                    </div>

                </div>

            </div>

            <hr>


            <? if (count($requests) > 0) { ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Компонент</th>
                            <th>ID запроса</th>
                            <th>Метод</th>
                            <th>Данные запроса</th>
                            <th>ip адрес</th>
                            <th>Ошибка (наличие)</th>
                            <th>Длительность отработки</th>
                            <th>Время отработки</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n           = 0;

                        foreach ($requests as $request) {
                            $n++;
                            $request_data = $request['Api_Log'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><?= $request_data['entity'] ?></td>
                                <td><a class="link btn-link" href="/mobile/view/<?=$request_data['id']?>" ><?= $request_data['id'] ?></a></td>
                                <td><?= $request_data['method'] ?></a></td>
                                <td>
                                    <span class="text-dark">token:</span> <?= $request_data['token'] ?><br>
                                    <span class="text-dark">id:</span> <?= $request_data['id'] ?><br>
                                    <span class="text-dark">Данные</span>:<br>
                                    <?
                                    if(!empty($request_data['data'])){
                                        echo $request_data['data'];
                                    }
                                    ?><br>
                                    <span class="text-dark">Параметры:</span> <br><?

                                    if(!empty($request_data['params'])){
                                        echo $request_data['params'];
                                    }?><br>
                                   <span class="text-dark">Результат:</span> <br>
                                    <?
                                    if(!empty($request_data['result'])){
                                        if(mb_strlen($request_data['result'])>200){
                                            echo mb_substr($request_data['result'], 0, 200) . " ...";
                                        } else {
                                            echo  $request_data['result'];
                                        }
                                    }


                                    ?>
                                </td>
                                <td><a class="link btn-link " href="/mobile/requests_by_ip/<?=$request_data['ip']?>" >
                                        <p class="<?if(in_array($request_data['ip'], $blocked_ips)) {echo "text-danger";} else {
                                            echo "text-success";
                                        }?>"><?= $request_data['ip'] ?>
                                        </p>
                                    </a>
                                </td>
                                <td><?= $request_data['error'] ?></td>
                                <td><?= substr($request_data['time'], 0, 6) ?></td>
                                <td>
                                    <span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $request_data['created']
                                        ) ?></span>
                                </td>
                                <td><a class="link btn-link" href="/mobile/delete/<?=$request_data['id']?>" >удалить запрос</a></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/mobile/";
                            $params         = [
                                'client_id' => $form_data['client_id'],
                                'component' => $form_data['component'],
                                'request_date' => $form_data['request_date'],
                                'sort' => $form_data['sort'],
                            ];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>


            <? } else { ?>
                <p class="muted">Запросов нет</p>
            <? } ?>

        </div>
    </div>
</div>