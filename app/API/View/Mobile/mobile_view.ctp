<div class="panel-body">
    <div class="blog-title media-block">
        <div class="media-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/mobile/" class="btn btn-success">показать все запросы</a>
                    </div>

                </div>

            </div>
            <h3>Запрос № <?= $request['Api_Log']['id']; ?></h3>

        </div>
    </div>

    <div class="blog-footer">
        <div class="media-left">
            <? if (count($request) > 0) {


                $request_data = $request['Api_Log'];
                ?>

            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Сущность</th>
                        <td><?= $request_data['entity'] ?></td>
                    </tr>
                    <tr>
                        <th>ID запроса (при наличии)</th>
                        <td><?= $request_data['id'] ?></td>
                    </tr>
                    <tr>
                        <th>Метод</th>
                        <td><?= $request_data['method'] ?></td>
                    </tr>
                    <tr>
                        <th>Токен</th>
                        <td><?= $request_data['token'] ?></td>
                    </tr>
                    <tr>
                        <th>Данные запроса</th>
                        <td><?   if(!empty($request_data['data'])){
                                echo $request_data['data'];
                            } ?></td>
                    </tr>
                    <tr>
                        <th>Параметры запроса</th>
                        <td><?   if(!empty($request_data['params'])){
                                echo $request_data['params'];
                            } ?></td>
                    </tr>
                    <tr>
                        <th>Результат запроса</th>
                        <td><?   if(!empty($request_data['result'])){
                                echo $request_data['result'];
                            } ?></td>
                    </tr>
                    <tr>
                        <th>ip адрес</th>
                        <td><?= $request_data['ip'];?></td>
                    </tr>
                    <tr>
                        <th>Ошибка (наличие)</th>
                        <td><?= $request_data['error']; ?></td>
                    </tr>
                    <tr>
                        <th>Длительность отработки</th>
                        <td><?= substr($request_data['time'], 0, 6) ?></td>
                    </tr>
                    <tr>
                        <th>Время отработки</th>
                        <td><span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                    $request_data['created']
                                ) ?></span></td>
                    </tr>
                    <tr>
                        <th>Действия</th>
                        <td><a class="link btn-link" href="/mobile/delete/<?=$request_data['id']?>" >удалить запрос</a></td>
                    </tr>
                </table>
            </div>
            <? } ?>
        </div>
    </div>
</div>