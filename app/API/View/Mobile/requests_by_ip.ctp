<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Данные запросов от мобильного приложения с адреса <?=$ip?> (всего запросов - <?=$total_count?>) </h3>
        </div>

        <div class="panel-body">

            <div class="row">

                <div class="table-toolbar-left">
                    <? if($ip_status == "blocked"){?>
                        <a href="/mobile/unblock_ip/<?=$ip?>" class="btn btn-success">Разблокировать ip адрес</a>
                    <?} else {?>
                        <a href="/mobile/block_ip/<?=$ip?>" class="btn btn-danger">Блокировать ip адрес</a>
                    <?} ?>
                    <a href="/mobile/blocks_by_ip/<?=$ip?>" class="btn btn-success">История блокировок ip адреса</a>

                </div>

            </div>

            <? if (count($requests) > 0) { ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Сущность</th>
                            <th>ID запроса (при наличии)</th>
                            <th>Метод</th>
                            <th>Данные запроса</th>
                            <th>Ошибка (наличие)</th>
                            <th>Длительность отработки</th>
                            <th>Время отработки</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n           = 0;

                        foreach ($requests as $request) {
                            $n++;
                            $request_data = $request['Api_Log'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><?= $request_data['entity'] ?></td>
                                <td><a class="link btn-link" href="/mobile/view/<?=$request_data['id']?>" ><?= $request_data['id'] ?></a></td>
                                <td><?= $request_data['method'] ?></a></td>
                                <td>
                                    <span class="text-dark">token:</span> <?= $request_data['token'] ?><br>
                                    <span class="text-dark">id:</span> <?= $request_data['id'] ?><br>
                                    <span class="text-dark">Данные</span>:<br>
                                    <?
                                    if(!empty($request_data['data'])){
                                        echo $request_data['data'];
                                    }
                                    ?><br>
                                    <span class="text-dark">Параметры:</span> <br><?

                                    if(!empty($request_data['params'])){
                                        echo $request_data['params'];
                                    }?><br>
                                   <span class="text-dark">Результат:</span> <br>
                                    <?
                                    if(!empty($request_data['result'])){
                                        if(mb_strlen($request_data['result'])>200){
                                            echo mb_substr($request_data['result'], 0, 200) . " ...";
                                        } else {
                                            echo  $request_data['result'];
                                        }
                                    }


                                    ?>
                                </td>
                                <td><?= $request_data['error'] ?></td>
                                <td><?= substr($request_data['time'], 0, 6) ?></td>
                                <td>
                                    <span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $request_data['created']
                                        ) ?></span>
                                </td>
                                <td><a class="link btn-link" href="/mobile/delete/<?=$request_data['id']?>" >удалить запрос</a></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/mobile/requests_by_ip/" . $ip;
                            $params         = [];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>


            <? } else { ?>
                <p class="muted">Запросов нет</p>
            <? } ?>

        </div>
    </div>
</div>