<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Данные сайта</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/data_field/add/"><i
                                    class="fa fa-plus"></i> Создать поле данных</a>
                    </div>
                </div>
            </div>
            <hr>

            <? if (count($site_data) > 0) { ?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Ключ</th>
                            <th>Содержание</th>
                            <th>Добавлено</th>
                            <th>Обновлено</th>
                            <th>Редактировать</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n           = 0;
                        $status_list    = [];
                        $status_list[1] = ['success', 'активно'];
                        $status_list[0] = ['danger', 'скрыто'];

                        foreach ($site_data as $site_data_item) {
                            $n++;
                            $site_elem = $site_data_item['Site_Data'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><?= $site_elem['title'] ?></td>
                                <td><?= $site_elem['name'] ?></a></td>
                                <td><?= text_preview($site_elem['content'], 300) ?></a></td>

                                <td><span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $site_elem['created']
                                        ) ?></span>
                                </td>
                                <td><span class="text-info"><i class="pli-clock"></i> <?= lang_calendar(
                                            $site_elem['modified']
                                        ) ?></span>
                                </td>

                                <td>
                                    <a class="btn btn-success btn-bock"
                                       href="/data_field/edit/<?= $site_elem['id'] ?>"
                                       data-id="<?= $site_elem['id'] ?>"><i class="fa fa-edit"></i> Редактирование поля
                                        данных</a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

            <? } else { ?>
                <p class="muted">Данных сайта нет</p>
            <? } ?>

        </div>
    </div>
</div>