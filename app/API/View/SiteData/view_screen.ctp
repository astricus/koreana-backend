<?
$status_list    = [];
$status_list[1] = ['success', 'активна'];
$status_list[0] = ['danger', 'скрыта'];

$screen_data = $screen['Screen'];
?>
<?= $this->Html->script('webforms') ?>
<div class="row">
    <a href="/welcome_screens" class="btn btn-success">показать все экраны приветствия</a>
    <a class="btn btn-mint" href="/welcome_screen/edit/<?= $screen_data['id'] ?>">Редактировать экран</a>

    <? if ($screen_data['enabled'] == 1) { ?>
        <a class="btn btn-warning btn-bock" href="/welcome_screen/block/<?= $screen_data['id'] ?>"><i
                    class="fa fa-close"></i>
            Скрыть экран из показа (сейчас показывается)</a>
    <? } else { ?>
        <a class="btn btn-warning btn-bock" href="/welcome_screen/unblock/<?= $screen_data['id'] ?>"><i
                    class="fa fa-eye"></i>
            Показывать экран (сейчас скрыт)</a>

    <? } ?>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $screen_data['title'] ?></h3>
        </div>
        <div class="panel-body">


            <p class="label label-success">Экран создан <?= $screen_data['created'] ?></p>
            <hr>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Содержание</h3>
                </div>

                <?= $screen_data['content'] ?>
            </div>

        </div>
    </div>
</div>