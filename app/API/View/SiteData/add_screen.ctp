<div class="row">

    <script>

        form_type_fields = <?= $form_fields_json?>;
    </script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Создание экрана приветствия </h3>

        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/welcome_screens">Список экранов приветствия</a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_screen" method="post" action="/welcome_screen/add"
              enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group <? if (key_exists("name", $errors)) {
                    echo "has-error";
                } ?>">
                    <label class="col-lg-3 control-label" for="name">Название</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required
                               placeholder="Название экрана приветствия"
                               value="<?= $add_form_data['name'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("name", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="title"><?= $errors['name'] ?></label>
                <? } ?>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="title">Заголовок экрана</label>
                    <div class="col-sm-6">
                        <input type="text" id=title class="form-control" name="title" required
                               placeholder="Заголовок экрана" value="<?= $add_form_data['title'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("title", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="title"><?= $errors['title'] ?></label>
                <? } ?>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">Содержание экрана приветствия</label>
                    <div class="col-sm-6">

                        <textarea name="content" id="page_content"><?= $add_form_data['content'] ?></textarea>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>