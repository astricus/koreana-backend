<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список экранов приветствия</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class=" table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock"
                           href="/welcome_screen/add"><i class="fa fa-plus"></i>
                            Создать экран приветствия</a>
                    </div>
                </div>

            </div>

            <hr>

            <? if (count($screens) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название экрана</th>
                            <th>Заголовок экрана</th>
                            <th>Добавлен</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n           = 0;
                        $status_list    = [];
                        $status_list[1] = ['success', 'открыт'];
                        $status_list[0] = ['danger', 'скрыт'];

                        foreach ($screens as $screen) {
                            $name         = $screen['Screen']['name'];
                            $screen_title = $screen['Screen']['title'];
                            $screen_id    = $screen['Screen']['id'];
                            $n++;

                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><?= $name ?></td>
                                <td><a class="btn-link"
                                       href="/welcome_screen/view/<?= $screen_id ?>"><?= $screen_title ?></a>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar(
                                            $screen['Screen']['created']
                                        ) ?>
                                    </span>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$screen['Screen']['enabled']][0] ?>"><?= $status_list[$screen['Screen']['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($screen['Screen']['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/welcome_screen/unblock/<?= $screen_id ?>"
                                               data-original-title="Открыть экран" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/welcome_screen/block/<?= $screen_id ?>"
                                               data-original-title="Скрыть экран" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/welcome_screen/edit/<?= $screen_id ?>"
                                           data-original-title="Изменить экран" data-container="body"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <? /*
                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/webforms/";
                            $params         = [
                                'find_by_name' => $form_data['find_by_name'],
                            ];
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div> */ ?>

            <? } else { ?>

                <p class="muted">Экранов приветствия нет</p>
            <? } ?>

        </div>
    </div>
</div>