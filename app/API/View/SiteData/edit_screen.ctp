<div class="row">

    <script>
        form_type_fields = <?= $form_fields_json?>;
    </script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Редактирование экрана приветствия <b><?= $screen['title'] ?></b></h3>

        </div>

        <div class="row">
            <div class="col-sm-12 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/welcome_screens">Список экранов приветствия</a>
                <? if ($screen['enabled'] == 1) { ?>
                    <a class="btn btn-warning btn-bock" href="/welcome_screen/block/<?= $screen['id'] ?>"><i
                                class="fa fa-close"></i> Скрыть экран из показа</a>
                <? } else { ?>
                    <a class="btn btn-warning btn-bock" href="/welcome_screen/unblock/<?= $screen['id'] ?>"><i
                                class="fa fa-eye"></i> Показывать экран (сейчас скрыт)</a>
                <? } ?>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="edit_screen" method="post"
              action="/welcome_screen/edit/<?= $screen['id'] ?>"
              enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group <? if (key_exists("title", $errors)) {
                    echo "has-error";
                } ?>">
                    <label class="col-lg-3 control-label" for="title">Заголовок экрана</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="title" required
                               placeholder="Заголовок экрана"
                               value="<?= $screen['title'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("title", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="title"><?= $errors['title'] ?></label>
                <? } ?>

                <div class="form-group <? if (key_exists("name", $errors)) {
                    echo "has-error";
                } ?>">
                    <label class="col-lg-3 control-label" for="name">Название экрана</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required
                               placeholder="Название страницы"
                               value="<?= $screen['name'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("name", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="name"><?= $errors['name'] ?></label>
                <? } ?>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">Содержание страницы</label>
                    <div class="col-sm-6">
                        <textarea name="content" id="page_content"><?= $screen['content'] ?></textarea>
                    </div>
                </div>

            </div>


            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить экран">
                        <a class="btn btn-success btn-bock" href="/welcome_screen/view/<?= $screen['id'] ?>">Предварительный
                            просмотр</a>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>