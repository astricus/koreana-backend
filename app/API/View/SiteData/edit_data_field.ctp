<div class="row">
    <script>

        $(document).on('nifty.ready', function () {

            $('.summernote').summernote({
                'height': '230px',
                callbacks: {
                    onImageUpload: function (files, editor, welEditable) {

                        for (let i = files.length - 1; i >= 0; i--) {
                            sendFile(files[i], this);
                        }
                    }
                }
            });

            setInterval(function () {
                $("#page_content").val($(".summernote").summernote('code'));
            }, 120);
        });

    </script>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Редактирование поля данных <b><?= $data_field['title'] ?></b></h3>

        </div>

        <div class="row">
            <div class="col-sm-12 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/site_data">Список полей данных</a>
                <a class="btn btn-warning btn-bock" href="/data_field/delete/<?= $data_field['id'] ?>"><i
                            class="fa fa-trash"></i> Удалить</a>

            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="edit_data_field" method="post"
              action="/data_field/edit/<?= $data_field['id'] ?>" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group <? if (key_exists("title", $errors)) {
                    echo "has-error";
                } ?>">
                    <label class="col-lg-3 control-label" for="title">Заголовок поля данных</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="title" required
                               placeholder="Заголовок экрана"
                               value="<?= $data_field['title'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("title", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="title"><?= $errors['title'] ?></label>
                <? } ?>

                <div class="form-group <? if (key_exists("name", $errors)) {
                    echo "has-error";
                } ?>">
                    <label class="col-lg-3 control-label" for="name">Название поля</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required
                               placeholder="Название поля"
                               value="<?= $data_field['name'] ?>"/>
                    </div>
                </div>
                <? if (key_exists("name", $errors)) { ?>
                    <label class="col-lg-3 control-label error-message" for="name"><?= $errors['name'] ?></label>
                <? } ?>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">Содержание поля</label>
                    <div class="col-sm-6">
                        <div class="summernote"><?= $data_field['content'] ?></div>
                        <textarea name="content" id="page_content" style="visibility: hidden">
                            <?= $data_field['content'] ?>
                        </textarea>
                    </div>
                </div>

            </div>


            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить поле данных">
                        <a class="btn btn-success btn-bock" href="/welcome_screen/view/<?= $data_field['id'] ?>">Предварительный
                            просмотр</a>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>