<script>
    $(document).ready(function () {

        let last_string = '<?=$form_data['search'];?>';
        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Тип локации */
        $(document).on("change", "#location_type", function () {
            let location_type_id = $('#location_type').val(), new_url;
            if (location_type_id != "") {
                new_url = updateQueryStringParameter(window.location.href, 'location_type_id', location_type_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'location_type_id', '');
            }
            redirect(new_url);
        });

        $(document).on("keyup", "#search_string", function () {

            let search_delay = function (last_string) {

                let string = $("#search_string").val().trim();

                if (string.length <= 2) {
                    return false;
                }

                if (last_string === string) {
                    return false;
                }
                let new_url;
                if (string !== "") {
                    new_url = updateQueryStringParameter(window.location.href, 'search', string);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'search', '');
                }
                last_string = string;
                redirect(new_url);
            };

            setTimeout(search_delay(last_string), 700);
        });

    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список локаций</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class="col-sm-3 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/location/add/">
                            <i class="fa fa-plus"></i>Добавить локацию
                        </a>

                    </div>
                </div>

                <hr/>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/locations" class="btn btn-success">показать все локации</a>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 city_select" name="city_id">

                            <option value="0">Все города</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['filter']['city_id']) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="location_type" class="main_select use_select2 location_type_select" name="location_type_id">
                            <option value="">Все типы локаций</option>
                            <? foreach ($location_types as $location_type) {
                                $location_type = $location_type['Location_Type'];
                                ?>
                                <option value="<?= $location_type['id'] ?>" <? if ($location_type['id'] == $form_data['filter']['location_type_id']) echo 'selected="selected"'; ?>>
                                    <?= $location_type['name'] ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 dropdown">
                        <label class="control-label" for="search_string">Поиск</label>
                        <input type="text" id="search_string" name="search_string"
                               value="<? if (isset($form_data['search'])) echo $form_data['search']; ?>"/>
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($locations) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Тип</th>
                            <th>Город</th>
                            <th>Адрес</th>
                            <th>Изображение</th>
                            <th>Телефон</th>
                            <th>Добавлена</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list[1] = array('success', 'активна');
                        $status_list[0] = array('danger', 'скрыта');

                        foreach ($locations as $location_elem) {
                            $n++;

                            $location = $location_elem['Location'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td>
                                    <?= $location_elem['Location_Type']['name'] ?>
                                </td>
                                <td>
                                    <?= $location['city_name'] ?>
                                </td>
                                <td>
                                    <?= $location['address'] ?>
                                </td>
                                <td>
                                    <? if (!empty($location['image'])) { ?>
                                        <a target="_blank" class="btn-link" href="<?= site_url() . "/". $content_dir ?>/<?= $location['image'] ?>"><img
                                                    style="max-width: 140px; max-height: 250px;"
                                                    src="<?= $content_dir ?>/<?= $location['image'] ?>">
                                        </a>
                                    <? } ?>
                                </td>
                                <td>
                                    <?= $location['phones'] ?>
                                </td>
                                <td>
                                    <span class="text-info"><?= lang_calendar($location['created']) ?></span>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$location['enabled']][0] ?>"><?= $status_list[$location['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($location['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/location/unblock/<?= $location['id'] ?>"
                                               data-original-title="Открыть локацию" data-container="body">
                                            </a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/location/block/<?= $location['id'] ?>"
                                               data-original-title="Скрыть локацию" data-container="body">
                                            </a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/location/edit/<?= $location['id'] ?>"
                                           data-original-title="Изменить данные" data-container="body">
                                        </a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <? /*

                <div id="map" style="width: 600px; height: 600px;"></div>
                <script>
                    let lat = 57.5262;
                    let lon = 38.3061;
                </script>
                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=<?=$ya_map_api_key?>" type="text/javascript" charset="utf-8"></script>
            <? $this->Html->script('map.location.list.js') */?>



                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/locations";
                            $params = array(
                                'location_type_id' => $form_data['filter']['location_type_id'] ?? null,
                                'city_id' => $form_data['filter']['city_id'] ?? null,
                                'search' => $form_data['search'] ?? "",
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Локаций нет</p>
            <? } ?>

        </div>
    </div>
</div>