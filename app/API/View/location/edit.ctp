<div class="row">
    <?= $this->Html->script('work_time.js') ?>
    <script>
        $(document).on('nifty.ready', function () {
            $(document).on('click', ".delete_location", function (e) {
                if (confirm('Вы уверены, что невозможно хотите удалить данную локацию?')) {
                    var link = $(this).attr('href');
                    window.location.href = link;
                }
                e.preventDefault();
                return false;
            });
        })
    </script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Локация <i><?= $location['title'] ?> </i></h3>
        </div>
        <form class="form-horizontal" name="edit_location" method="post" action="/location/edit/<?= $location['id'] ?>"
              enctype="multipart/form-data">
            <div class="panel-body">

                <div class="pad-btm form-inline">

                    <div class="row">
                        <div class="col-sm-6 table-toolbar-left">
                            <a class="btn btn-success btn-bock" href="/locations"><i class="fa fa-list"></i> Список
                                локаций</a>

                            <? if ($location['enabled'] == 1) { ?>
                                <a class="btn btn-warning btn-bock" href="/location/block/<?= $location['id'] ?>"><i
                                            class="fa fa-close"></i> Скрыть локацию из показа</a>
                            <? } else { ?>
                                <a class="btn btn-warning btn-bock" href="/location/unblock/<?= $location['id'] ?>"><i
                                            class="fa fa-eye"></i> Показывать локацию (сейчас скрыта)</a>

                            <? } ?>

                            <a class="btn btn-danger btn-bock delete_location"
                               href="/location/delete/<?= $location['id'] ?>"><i
                                        class="fa fa-minus"></i> Удалить данную локацию</a>
                        </div>
                    </div>

                </div>

                <hr>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="title">Название локации (опционально)</label>
                    <div class="col-sm-6">
                        <input type="text" id=title class="form-control" name="title" placeholder="Название локации"
                               value="<?= $location['title'] ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Тип локации</p></label>
                    <div class="col-sm-6">
                        <?

                        ?>
                        <select id="location_type" class="use_select2 dropdownPlatformSelect" name="location_type_id"
                                style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($location_types as $location_type) {
                                $location_type = $location_type['Location_Type'];
                                ?>
                                <option value="<?= $location_type['id']; ?>" <? if ($location['location_type_id'] == $location_type['id']) echo 'selected="selected"'; ?>><?= $location_type['name'] ?></option>
                            <? } ?>
                        </select>

                        <a class="btn btn-success btn-bock" href="/location/types"><i class="fa fa-plus"></i> Добавить новый тип локации</a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="image">Изображение</label>
                    <div class="col-sm-6">
                        <input type="file" id="image" class="form-control" name="image"/>
                        <img src="<?= $location['image_url'] ?>" style="max-width: 400px; max-height: 400px"
                             alt="Изображение локации"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="city_id">Город локации</p></label>
                    <div class="col-sm-6">
                        <select id="city_id" class="use_select2 dropdownPlatformSelect" name="city_id"
                                style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($cities as $city) { ?>
                                <option value="<?= $city['City']['id']; ?>" <? if ($location['city_id'] == $city['City']['id']) echo 'selected'; ?>><?= $city['City']['name'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="address">Адрес</label>
                    <div class="col-sm-6">
                        <textarea id="address" class="form-control" name="address"><?= $location['address'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="address_desc">Адрес (описание)</label>
                    <div class="col-sm-6">
                        <textarea id="address_desc" class="form-control"
                                  name="address_desc"><?= $location['address_desc'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="phones">Телефон(ы)</label>
                    <div class="col-sm-6">
                        <input type="text" id=phones class="form-control" name="phones" placeholder="Телефон"
                               value="<?= $location['phones'] ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="work_time">Время работы</label>
                    <div class="col-sm-6">

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>День недели</th>
                                    <th>Начало работы</th>
                                    <th>Окончание работы</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $weeks = [
                                    'пнд' => 'pnd',
                                    'вт' => 'vt',
                                    'ср' => 'sr',
                                    'чт' => 'cht',
                                    'пт' => 'pt',
                                    'сб' => 'sb',
                                    'вс' => 'vs',
                                ];
                                $hours_start = range(6, 14);
                                $hours_end = range(13, 23);
                                $minutes_interval = array('00', '15', '30','45');
                                foreach ($weeks as $key => $day_code) {
                                    $day_start_time = $work_time[$day_code]["start_time"] ?? "9:00";
                                    $day_end_time = $work_time[$day_code]["end_time"] ?? "18:00";
                                    $is_work_day = $work_time[$day_code]["is_work_day"] ?? 1;
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $key ?>
                                            <input type=hidden name="work_time[<?= $day_code ?>][is_work_day]" data-day="<?=$day_code?>" value="<?=$is_work_day?>">
                                        </td>
                                        <td>
                                            <select name="work_time[<?=$day_code?>][start_time]" data-day="<?=$day_code?>" <?if($is_work_day==0) echo 'disabled="disabled"';?>>
                                                <? foreach ($hours_start as $val_hour) {
                                                foreach ($minutes_interval as $val_min) {
                                                    ?>
                                                    <option value="<?=$val_hour?>:<?=$val_min?>" <?if($val_hour . ":" . $val_min==$day_start_time) echo 'selected="selected"'?>><?=$val_hour?>:<?=$val_min?></option>
                                                <? }} ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="work_time[<?=$day_code?>][end_time]" data-day="<?=$day_code?>" <?if($is_work_day==0) echo 'disabled="disabled"';?>>
                                                <? foreach ($hours_end as $val_hour) {
                                                foreach ($minutes_interval as $val_min) {
                                                    ?>
                                                    <option value="<?=$val_hour?>:<?=$val_min?>" <?if($val_hour . ":" . $val_min==$day_end_time) echo 'selected="selected"';?>><?=$val_hour?>:<?=$val_min?></option>
                                                <? }} ?>
                                            </select>
                                        </td>
                                        <td>
                                            <a class="btn  <?if($is_work_day==1){ echo 'btn-success';} else {
                                                echo 'btn-default';
                                            }?> btn-bock is_work_day work_day" data-day="<?=$day_code?>">Рабочий день</a> /
                                            <a class="btn <?if($is_work_day==0){ echo 'btn-success';} else {
                                                echo 'btn-default';
                                            }?> btn-bock is_work_day no_work_day" data-day="<?=$day_code?>">Выходной день</a>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="email">Email</label>
                    <div class="col-sm-6">
                        <input type="email" id=email class="form-control" name="email" placeholder="email"
                               value="<?= $location['email'] ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="lat">Широта</label>
                    <div class="col-sm-6">
                        <input type="text" id="lat" class="form-control" name="lat" required value="<?= $location['lat'] ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="lon">Долгота</label>
                    <div class="col-sm-6">
                        <input type="text" id="lon" class="form-control" name="lon" required value="<?= $location['lon'] ?>"/>
                    </div>
                </div>

            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <div id="map" style="width: 600px; height: 600px;"></div>
                </div>
            </div>
            <script>
                let lat = <?= $location['lat']?>;
                let lon = <?=$location['lon']?>;
            </script>
            <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=<?= $ya_map_api_key ?>"
                    type="text/javascript" charset="utf-8"></script>

            <?

            $map_script= "map.location.js";
            echo $this->Html->script($map_script)?> ?>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить">
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>