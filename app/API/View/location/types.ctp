<div class="row">

    <div class="panel">
        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/locations">Список локаций</a>
            </div>
        </div>

        <? if (count($location_types) > 0) { ?>

            <div class="panel-heading">
                <h3 class="panel-title">Список типов локаций</h3>
            </div>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Управление</th>
                </tr>
                </thead>
                <tbody>
                <?
                foreach ($location_types as $location_type) {
                    $location_type = $location_type['Location_Type'];
                    ?>
                    <tr class="">
                        <td>
                            <?= $location_type['name'] ?>
                        </td>
                        <td>
                            <?= $location_type['type'] ?>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-sm btn-danger btn-hover-info" href="/location/delete_type/<?= $location_type['id'] ?>">
                                    <i class="fa fa-minus"></i>Удалить тип
                                </a>
                            </div>
                        </td>
                    </tr>
                <? } ?>
                </tbody>
            </table>
        </div>
        <?}?>

        <hr>

        <div class="panel-heading">
            <h3 class="panel-title">Добавление типа локации</h3>
        </div>

        <form class="form-horizontal" name="add_type" method="post" action="/location/add_type">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="title">Название типа локации</label>
                    <div class="col-sm-6">
                        <input type="text" id=title class="form-control" name="name" placeholder="Название типа локации" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="title">Код типа</label>
                    <div class="col-sm-6">
                        <input type="text" id=code class="form-control" name="type" placeholder="Код типа" value=""/>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>