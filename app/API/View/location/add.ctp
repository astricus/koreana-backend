<div class="row">
    <?= $this->Html->script('work_time.js') ?>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Добавление локации</h3>
        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/locations">Список локаций</a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_location" method="post" action="/location/add"
              enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="title">Название локации (опционально)</label>
                    <div class="col-sm-6">
                        <input type="text" id=title class="form-control" name="title" placeholder="Название локации" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Тип локации</p></label>
                    <div class="col-sm-6">
                        <select id="location_type_id" class="use_select2 dropdownPlatformSelect" name="location_type_id"
                                style="width: 300px">
                            <? foreach ($location_types as $location_type) {
                                $location_type = $location_type['Location_Type'];
                                ?>
                                <option value="<?= $location_type['id']; ?>"><?= $location_type['name'] ?></option>
                            <? } ?>
                        </select>
                        <a class="btn btn-success btn-bock" href="/location/types"><i class="fa fa-plus"></i> Добавить новый тип локации</a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Город локации</p></label>
                    <div class="col-sm-6">
                        <select id="city_id" class="use_select2 dropdownPlatformSelect" name="city_id"
                                style="width: 300px">
                            <? foreach ($cities as $city) { ?>
                                <option value="<?= $city['City']['id']; ?>"><?= $city['City']['name'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="address">Адрес</label>
                    <div class="col-sm-6">
                        <textarea id="address" class="form-control" name="address"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="address_desc">Адрес (описание)</label>
                    <div class="col-sm-6">
                        <textarea id="address_desc" class="form-control" name="address_desc"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="phones">Телефон(ы)</label>
                    <div class="col-sm-6">
                        <input type="text" id=phones class="form-control" name="phones" placeholder="Телефон" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="work_time">Время работы</label>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>День недели</th>
                                    <th>Начало работы</th>
                                    <th>Окончание работы</th>
                                    <th>Рабочий/Выходной</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $weeks = [
                                    'пнд' => 'pnd',
                                    'вт' => 'vt',
                                    'ср' => 'sr',
                                    'чт' => 'cht',
                                    'пт' => 'pt',
                                    'сб' => 'sb',
                                    'вс' => 'vs',
                                ];
                                $hours_start = range(6, 14);
                                $hours_end = range(13, 23);
                                $minutes_interval = array('00', '15', '30','45');
                                foreach ($weeks as $key => $val) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?= $key ?>
                                            <input type=hidden name="work_time[<?= $val ?>][is_work_day]" data-day="<?=$val?>" value="1">
                                        </td>
                                        <td>
                                            <select name="work_time[<?= $val ?>][start_time]" data-day="<?=$val?>">
                                                <? foreach ($hours_start as $val_hour) {
                                                     foreach ($minutes_interval as $val_min) {
                                                    ?>
                                                    <option value="<?=$val_hour . ":" . $val_min?>" <?if($val_hour==9 && $val_min=='00') echo 'selected="selected"'?>><?=$val_hour?>:<?=$val_min?></option>
                                                <? }} ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="work_time[<?= $val ?>][end_time]" data-day="<?=$val?>">
                                                <? foreach ($hours_end as $val_hour) {
                                                foreach ($minutes_interval as $val_min) {
                                                    ?>
                                                    <option value="<?=$val_hour  . ":" . $val_min?>" <?if($val_hour==18  && $val_min=='00') echo 'selected="selected"'?>><?=$val_hour?>:<?=$val_min?></option>
                                                <? }} ?>
                                            </select>
                                        </td>
                                        <td>
                                            <a class="btn btn-success btn-bock is_work_day work_day" data-day="<?=$val?>">Рабочий день</a> /
                                            <a class="btn btn-default btn-bock is_work_day no_work_day" data-day="<?=$val?>">Выходной день</a>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="email">Email</label>
                    <div class="col-sm-6">
                        <input type="email" id=email class="form-control" name="email" placeholder="email" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="image">Изображение</label>
                    <div class="col-sm-6">
                        <input type="file" id="image" class="form-control" name="image"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="lat">Широта</label>
                    <div class="col-sm-6">
                        <input type="text" id="lat" class="form-control" name="lat" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="lon">Долгота</label>
                    <div class="col-sm-6">
                        <input type="text" id="lon" class="form-control" name="lon" required/>
                    </div>
                </div>


            </div>
            <div id="map" style="width: 600px; height: 600px;"></div>
            <script>
                let lat = 57.5262;
                let lon = 38.3061;
            </script>
            <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=<?= $ya_map_api_key ?>"
                    type="text/javascript" charset="utf-8"></script>
            <?= $this->Html->script('map.location.js') ?>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>