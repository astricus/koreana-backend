<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Добавление администратора</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">
                <div class="row">
                    <div class="col-sm-6 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success" href="/admins">Перейти к администраторам</a>
                    </div>
                </div>
            </div>

            <form class="form-horizontal" name="add_admin" method="post" action="/admin/add" enctype="multipart/form-data">
            <div class="row">

                <div class="form-group <?if(array_key_exists("firstname", $errors)) echo 'has-error';?>">
                    <label class="col-lg-3 control-label" for="firstname">Имя</label>
                    <div class="col-sm-6">
                        <input type="text" id=firstname class="form-control" name="firstname" required placeholder="Имя" value="<? print_value("firstname", $data_value)?>">
                        <? print_error("firstname", $errors)?>
                    </div>
                </div>

                <div class="form-group <?if(array_key_exists("lastname", $errors)) echo 'has-error';?>">
                    <label class="col-lg-3 control-label" for="lastname">Фамилия</label>
                    <div class="col-sm-6">
                        <input type="text" id=lastname class="form-control" name="lastname" required placeholder="Фамилия" value="<? print_value("lastname", $data_value)?>">
                        <? print_error("lastname", $errors)?>
                    </div>
                </div>

                <div class="form-group <?if(array_key_exists("email", $errors)) echo 'has-error';?>">
                    <label class="col-lg-3 control-label" for="email">Email</label>
                    <div class="col-sm-6">
                        <input type="text" id=email class="form-control" name="email" required placeholder="Email" value="<? print_value("email", $data_value)?>">
                        <? print_error("email", $errors)?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="role">Роль</label>
                    <div class="col-sm-6">
                        <select id="role" name="role" class="form-control">
                        <? foreach ($roles as $role_name => $role) {
                            ?>
                            <option value="<?= $role_name ?>">
                                <?= $role ?>
                            </option>
                        <? } ?>
                        </select>
                    </div>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button class="btn btn-success" type="submit">Создать администратора</button>
                        </div>
                    </div>
                </div>

            </div>
            </form>

        </div>
    </div>
</div>