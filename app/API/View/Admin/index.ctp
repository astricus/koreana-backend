<h4>Список администраторов </h4>


    <script>
        $(document).ready(function () {
            /* Роль  */
            $(document).on("change", ".role_select", function () {
                let role = $('.role_select option:selected').val(), new_url;
                if (role !== "") {
                    new_url = updateQueryStringParameter(window.location.href, 'role', role);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'role', '');
                }
                redirect(new_url);
            });

            /* Администратор */
            $(document).on("change", ".admin_select", function () {
                let admin_id = $('.admin_select option:selected').val(), new_url;
                if (admin_id != 0) {
                    new_url = updateQueryStringParameter(window.location.href, 'admin_id', admin_id);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'admin_id', '');
                }
                redirect(new_url);
            });

        });

    </script>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-6 table-toolbar-left">
                                <a id="btn-addrow" class="btn btn-success" href="/admin/add/"><i class="pli-add"></i>
                                    Добавить администратора</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-3 table-toolbar-left dropdown">
                            <a href="/admins" class="btn btn-success">показать всех администраторов</a>
                        </div>
                        <div class="col-sm-3 table-toolbar-left dropdown">
                            <select id="role" class="main_select use_select2 role_select" name="role_select">

                                <option value="0">Все роли</option>
                                <? foreach ($roles_list as $rk => $rv) {
                                    ?>
                                    <option value="<?= $rk ?>" <? if ($rk == $form_data['role']) echo 'selected'; ?>>
                                        <?= $rv ?>
                                    </option>
                                <? } ?>
                            </select>
                        </div>

                        <div class="col-sm-3 table-toolbar-left dropdown">
                            <select id="admin_id" class="main_select use_select2 admin_select" name="admin_id">

                                <option value="0">Все администраторы</option>
                                <? foreach ($all_admins as $admin) {
                                    $admin_id = $admin['Manager']['id'];
                                    $admin_name = prepare_fio($admin['Manager']['firstname'], $admin['Manager']['lastname'], "");
                                    ?>
                                    <option value="<?= $admin_id ?>" <? if ($admin_id == $form_data['admin_id']) echo 'selected'; ?>>
                                        <?= $admin_name ?>
                                    </option>
                                <? } ?>
                            </select>
                        </div>


                    </div>

                    <? if (count($admins) > 0) { ?>

                    <div class="table-responsive">
                        <table class="table table-vcenter mar-top">
                            <thead>
                            <tr>
                                <th class="min-w-td">#</th>
                                <th class="min-w-td">Администратор</th>
                                <th>Имя</th>
                                <th>Email</th>
                                <th>Статус</th>
                                <th class="text-center">Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?
                            $n = 0;
                            foreach ($admins as $admin) {
                                $n++; ?>
                                <tr>
                                    <td class="min-w-td"><?= $n ?></td>
                                    <td><img src="img/profile-photos/1.png" alt="Profile Picture"
                                             class="img-circle img-sm"></td>
                                    <td><a class="btn-link"
                                           href="/admin/view/<?= $admin['Manager']['id'] ?>"><?= prepare_fio($admin['Manager']['firstname'], $admin['Manager']['lastname'], "") ?></a>
                                    </td>
                                    <td><?= $admin['Manager']['email'] ?></td>
                                    <td><?= $roles_list[$admin['Manager']['role']] ?></td>
                                    <td> <?
                                        if ($admin['Manager']['status'] == "active") {
                                            ?>
                                            <span class="label label-table label-success">Активирован</span>
                                        <? } else if ($admin['Manager']['status'] == "blocked") { ?>
                                            <span class="label label-table label-danger">Блокирован</span>
                                        <? } ?></td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-default btn-hover-success pli-pen-5 add-tooltip"
                                               href="admin/edit/<?= $admin['Manager']['id'] ?>"
                                               title="Редактировать"></a>

                                            <?
                                            if ($admin['Manager']['status'] == "active") {
                                                ?>
                                            <a class="btn btn-sm btn-default btn-hover-warning pli-unlock add-tooltip"
                                               href="admin/block/<?= $admin['Manager']['id'] ?>"
                                               title="Заблокировать администратора"></a><? } else if ($admin['Manager']['status'] == "blocked") { ?>
                                                <a class="btn btn-sm btn-danger btn-hover-success pli-unlock add-tooltip"
                                                   href="admin/unblock/<?= $admin['Manager']['id'] ?>"
                                                   title="Разблокировать администратора"></a>
                                            <?
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                        <hr>
                        <!--Pagination-->
                        <div class="fixed-table-pagination">
                            <div class="pull-right pagination">
                                <ul class="pagination">
                                    <?php $ctrl_pgn = "/admin/";
                                    $params = array(
//                                'category_id' => $category_id,
//                                'source' => $source,
                                    );
                                    echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <? } ?>
                </div>
            </div>
        </div>
    </div>