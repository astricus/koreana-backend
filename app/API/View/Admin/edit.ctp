
<script>

    $(document).on("click", ".reset_password", function (e) {
        let reset_link = $(this).attr('href');
        if (confirm('Сбросить пароль администратору?')) {
            window.location.href(reset_link);
            e.preventDefault();
            return false;
        } else {
            e.preventDefault();
            return false;
        }
    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">
                Администратор <?= prepare_fio($admin['Manager']['firstname'], $admin['Manager']['lastname'], "") ?></h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">
                <div class="row">
                    <div class="col-sm-6 table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success" href="/admins">Перейти к администраторам</a>
                    </div>
                </div>
            </div>

            <form class="form-horizontal" name="edit_admin" method="post" action="/admin/edit/<?=$admin['Manager']['id']?>" enctype="multipart/form-data">
                <div class="row">

                    <div class="form-group <?if(array_key_exists("firstname", $errors)) echo 'has-error';?>">
                        <label class="col-lg-3 control-label" for="firstname">Имя </label>
                        <div class="col-sm-6">
                            <input type="text" id=firstname class="form-control" name="firstname" required
                                   placeholder="Имя" value="<?= $admin['Manager']['firstname'] ?>">
                        </div>
                    </div>

                    <div class="form-group <?if(array_key_exists("firstname", $errors)) echo 'has-error';?>">
                        <label class="col-lg-3 control-label" for="firstname">Фамилия </label>
                        <div class="col-sm-6">
                            <input type="text" id=lastname class="form-control" name="lastname" required
                                   placeholder="Фамилия" value="<?= $admin['Manager']['lastname'] ?>">
                        </div>
                    </div>

                    <div class="form-group <?if(array_key_exists("email", $errors)) echo 'has-error';?>">
                        <label class="col-lg-3 control-label" for="email">Email </label>
                        <div class="col-sm-3">
                            <input type="text" id=email class="form-control" name="email" required disabled placeholder="Email" value="<?= $admin['Manager']['email'] ?>">
                        </div>
                        <div class="col-sm-3">
                            <a class="btn btn-warning reset_password" href="/admin/reset_pass/<?=$admin['Manager']['id']?>">Сбросить пароль</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="firstname">Роль </label>
                        <div class="col-sm-6">
                            <select id="role" name="role" class="form-control">
                                <? foreach ($roles as $role_name => $role) {
                                    ?>
                                    <option value="<?= $role_name ?>" <? if ($admin['Manager']['role'] == $role_name) echo "selected='true'"; ?>>
                                        <?= $role ?>
                                    </option>
                                <? } ?>
                            </select>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success" type="submit">Сохранить</button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>

        </div>
    </div>
</div>