<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Администратор <b><?= prepare_fio($profile['Manager']['firstname'], $profile['Manager']['lastname'], "") ?> </b><span class="text-muted"><?= $is_you ?></span></h3>
        </div>
        <div class="panel-body">

            <? if (count($profile) > 0) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <img alt="Profile Picture" class="img-lg img-circle mar-ver"
                                     src="img/profile-photos/2.png">
                                <hr>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr class="">
                                            <th>ID</th>
                                            <th scope="col">ФИО</th>
                                            <th scope="col">роль</th>
                                            <th scope="col" class="sorting">Email</th>
                                            <th>Создан</th>
                                            <th>Статус</th>
                                            <th scope="col" class="actions">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <b><?= $profile['Manager']['id'] ?></b>
                                            </td>
                                            <td>
                                                <?= prepare_fio($profile['Manager']['firstname'], $profile['Manager']['lastname'], "") ?>

                                            </td>

                                            <td>
                                                <b><?= $profile_role ?></b>
                                            </td>
                                            <td>
                                                <?= $profile['Manager']['email'] ?>
                                            </td>
                                            <td class="">
                                                <?= $profile['Manager']['created'] ?>
                                            </td>
                                            <td class="">
                                                <?
                                                if ($profile['Manager']['status'] == "active") { ?>
                                                    <div class="label label-success">активен</div>
                                                <? } else if ($profile['Manager']['status'] == "blocked") { ?>
                                                    <div class="label label-danger">заблокирован</div>
                                                <? } ?>
                                            </td>
                                            <td class="">
                                                <a href="/admin/edit/<?= $profile['Manager']['id'] ?>"
                                                   class="btn btn-default btn-hover-success" title="Редактировать"><i
                                                            class="pli-pen-5"></i> Редактировать</a>
                                                <?
                                                if ($profile['Manager']['status'] == "active") { ?>
                                                    <a href="/admin/block/<?= $profile['Manager']['id'] ?>"
                                                       class="btn btn-default btn-hover-danger" title="заблокировать"><i
                                                                class="fa fa-lock-open"></i> заблокировать</a>
                                                <? } else if ($profile['Manager']['status'] == "blocked") { ?>
                                                    <a href="/admin/unblock/<?= $profile['Manager']['id'] ?>"
                                                       class="btn btn-default btn-hover-success" title="разблокировать"><i
                                                                class="fa fa-lock-open"></i> разблокировать</a>
                                                <? } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
</div>
