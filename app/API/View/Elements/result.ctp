<div class="hidden result_api__container">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Результат запроса</h3>
        </div>
    </div>
    <span class="text-uppercase text-semibold text-main text-lg">
    Статус ответа: <span class="result_api__status"></span><br>
    Время исполнения api-запроса: <span class="result_api__time"></span><br>
    Размер возвращенных данных: <span class="result_api__size"></span>
    </span>


    <ul class="nav nav-tabs">
        <li class="active"><a href="#main_result_data" data-toggle="tab" aria-expanded="true">Заголовки</a></li>
        <li class=""><a href="#result_details" data-toggle="tab" aria-expanded="false">Содержимое</a></li>
        <li class=""><a href="#result_preview" data-toggle="tab" aria-expanded="false">Предпросмотр</a></li>
        <li class=""><a href="#result_timings" data-toggle="tab" aria-expanded="false">Тайминг</a></li>
    </ul>

    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane fade active in" id="main_result_data">
                <p class="text-main text-semibold">Заголовки</p>
                <pre><code class="result_api__headers http lang-http"></code></pre>
            </div>

            <div class="tab-pane fade" id="result_details">
                <p class="text-main text-semibold">Содержимое</p>
                <pre><code class="result_api__body html"></code></pre>
            </div>

            <div class="tab-pane fade" id="result_preview">
                <p class="text-main text-semibold">Предпросмотр</p>
                <iframe class="preview_api_result_html" srcdoc="" width="600" height="1000" frameborder="1" sandbox></iframe>
            </div>
            <div class="tab-pane fade active show" id="result_timings" role="tabpanel">
                <div class="timings">
                    <div class="panel-body">
                        <div class="pad-btm timing_dns_lookup_time">
                            <p class="text-semibold text-main">Поиск DNS</p>
                            <div class="progress progress-md">
                                <div class="progress-bar progress-bar-purple" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%;" role="progressbar">
                                    <span class="sr-only">15%</span>
                                </div>
                            </div>
                            <small>15% Completed</small>
                        </div>

                        <div class="timing_tcp_connection_time">
                            <p class="text-semibold text-main">Время соединения TCP</p>
                            <div class="progress progress-md">
                                <div class="progress-bar progress-bar-success" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;" role="progressbar">
                                    <span class="sr-only">70%</span>
                                </div>
                            </div>
                            <small>70% Completed</small>
                        </div>

                        <div class="timing_content_generation_time">
                            <p class="text-semibold text-main">Время генерации ответа</p>
                            <div class="progress progress-md">
                                <div class="progress-bar progress-bar-success" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;" role="progressbar">
                                    <span class="sr-only">70%</span>
                                </div>
                            </div>
                            <small>70% Completed</small>
                        </div>

                        <div class="timing_content_transfer_time">
                            <p class="text-semibold text-main">Время передачи данных</p>
                            <div class="progress progress-md">
                                <div class="progress-bar progress-bar-success" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;" role="progressbar">
                                    <span class="sr-only">70%</span>
                                </div>
                            </div>
                            <small>70% Completed</small>
                        </div>
                    </div>

                    <div class="apiNode-desc">
                        Сервер расположен в: <span class="apiNode-location">Российская Федерация</span>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>