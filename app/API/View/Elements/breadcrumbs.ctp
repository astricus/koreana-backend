<?php
if (!empty($breadcrumbs) && (count($breadcrumbs) > 1)) {
	$start_elem = true;
	$count = count($breadcrumbs);
	$c = 0;
	foreach ($breadcrumbs as $breadcrumb_link) {
		$c++;
		if (!$start_elem) echo " ";
		$last_element = ($c == $count) ? 'last_element' : '';
		echo " <a href='" . $breadcrumb_link['url'] . "' class='breadcrumb_link " . $last_element . "'>" . $breadcrumb_link['title'] . "</a> ";
		$start_elem = false;
	}
}