<div class="row">
    <?= $this->Html->script('webforms'); ?>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Источник данных веб-форм "<?= $data_provider['name'] ?>"</h3>
        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/data_providers">Источники данных</a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_provider" method="post" action="/data_provider/edit/<?= $data_provider['id'] ?>"
              enctype="multipart/form-data">
            <input type="hidden" name="provider_id" value="<?= $data_provider['id'] ?>">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="name">Название</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required
                               placeholder="Название источника" value="<?= $data_provider['name'] ?>"/>
                    </div>
                </div>

                <hr>

                <div class="panel-heading">
                    <h3 class="panel-title">Перечислите элементы списка</h3>
                </div>

                <div class="provider_elements">

                    <?
                    foreach ($provider_list as $provider_item) {
                        $item = $provider_item['Data_List_Item'];
                        ?>
                        <input type="hidden" name="provider[id][]" value="<?= $item['id'] ?>"/>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="element">Название элемента списка</label>
                            <div class="col-sm-5">
                                <input type="text" data-value="<?= $item['id'] ?>" name="provider[name][]" id=element class="form-control" required
                                       value="<?= $item['name'] ?>"/>
                            </div>
                            <div class="col-sm-1">
                                <a href="#" class="btn btn-danger delete_provider_list_element" data-value="<?= $item['id'] ?>" title="Удалить элемент списка"><i class="icon pli-recycling"></i></a>
                            </div>
                        </div>
                        <?
                    }
                    ?>

                </div>

                <a href="#" class="btn btn-success new_provider_element">Добавить элемент списка</a>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit"
                               value="Сохранить источник данных"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>