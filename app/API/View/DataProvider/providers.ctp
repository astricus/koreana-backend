<script>
    $(document).ready(function () {

        /* Поиск по названию */
        $(document).on("change", ".find_by_name", function () {
            let find_by_name = $('.find_by_name').val(), new_url;
            if (find_by_name.length >= 2) {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', find_by_name);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', '');
            }
            redirect(new_url);
        });

    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Источники данных для веб-форм</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-6 table-toolbar-left dropdown">
                        <a id="btn-addrow" class="btn btn-success btn-bock" href="/data_provider/add"><i class="fa fa-plus"></i>
                            Создать источник данных</a>
                        <a href="/data_providers" class="btn btn-success">показать все источники данных</a>

                    </div>

                    <div class="col-sm-6">
                        <label class="label control-label label-info" for="find_by_name">поиск по названию</label>
                        <input id="find_by_name" type="text" placeholder="поиск по названию" class="main_select use_select2 find_by_name" name="find_by_name"
                               value="<?= $form_data['find_by_name'] ?>">
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($providers) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Создан</th>
                            <th>Кол-во элементов списка</th>
                            <th>Используется в веб-формах</th>
                            <th>Статус</th>

                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list[1] = array('success', 'активен');
                        $status_list[0] = array('danger', 'скрыт');

                        foreach ($providers as $provider) {
                            $name = $provider['Data_Provider']['name'];
                            $id = $provider['Data_Provider']['id'];
                            $n++;

                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td>
                                    <a class="btn-link" href="/data_provider/view/<?= $id ?>"><?= $name ?></a>
                                </td>
                                <td>
                                    <span class="">
                                        <i class="pli-clock"></i> <?= lang_calendar($provider['Data_Provider']['created']) ?>
                                    </span>
                                </td>
                                <td>
                                    <?=$provider['count']?>
                                </td>
                                <td>
                                    <?=$provider['form_used']?>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$provider['Data_Provider']['enabled']][0] ?>"><?= $status_list[$provider['Data_Provider']['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($provider['Data_Provider']['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/data_provider/unblock/<?= $id ?>"
                                               data-original-title="Открыть веб-форму" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/data_provider/block/<?= $id?>"
                                               data-original-title="Скрыть веб-форму" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/data_provider/edit/<?= $id?>"
                                           data-original-title="Изменить источник данных" data-container="body"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/data_providers";
                            $params = array(
                                'find_by_name' => $form_data['find_by_name'],
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Источников данных форм нет</p>
            <? } ?>

        </div>
    </div>
</div>