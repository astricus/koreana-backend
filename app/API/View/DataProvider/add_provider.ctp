<div class="row">
    <?= $this->Html->script('webforms') ?>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Создание источника данных веб-форм</h3>
        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/data_providers">Источники данных</a>
            </div>
        </div>

        <hr>

        <form class="form-horizontal" name="add_provider" method="post" action="/data_provider/add_provider" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="name">Название</label>
                    <div class="col-sm-6">
                        <input type="text" id=name class="form-control" name="name" required placeholder="Название источника" value=""/>
                    </div>
                </div>

                <hr>

                <div class="panel-heading">
                    <h3 class="panel-title">Перечислите элементы списка</h3>
                </div>

                <div class="provider_elements">

                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="stop_date">Название элемента списка
                        </label>
                        <div class="col-sm-6">
                            <input type="text" name="provider[name][]" class="form-control" required/>
                        </div>
                    </div>

                </div>

                <a href="#" class="btn btn-success new_provider_element">Добавить элемент списка</a>


                <hr>

                <div class="panel-heading">
                    <h3 class="panel-title">Или создайте список используя альтернативный вариант - перечисление элементов через ";"</h3>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="stop_date">Перечисление элементов нового списка
                    </label>
                    <div class="col-sm-6">
                        <textarea cols="20" rows="10" name="elements" class="form-control"></textarea>
                    </div>
                </div>




            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить источник данных"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>