<script>
    let last_string = '<?=$form_data['search'];?>';
    $(document).ready(function () {

        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });

        /* Город  */
        $(document).on("change", ".city_select", function () {
            let city_id = $('.city_select option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* Платформа */
        $(document).on("change", ".platform_select", function () {
            let platform = $('.platform_select option:selected').val(), new_url;
            if (platform !== 0) {
                new_url = updateQueryStringParameter(window.location.href, 'platform', platform);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'platform', '');
            }
            redirect(new_url);
        });

        /* Дата показа */
        $(document).on("change", "#date_from", function () {
            let date_from = $('#date_from').val(), new_url;
            if (date_from !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'date_from', date_from);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'date_from', '');
            }
            redirect(new_url);
        });

        $(document).on("change", "#date_to", function () {
            let date_to = $('#date_to').val(), new_url;
            if (date_to !== "") {
                new_url = updateQueryStringParameter(window.location.href, 'date_to', date_to);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'date_to', '');
            }
            redirect(new_url);
        });

        $(document).on("click", "#export_csv", function () {
            let new_url = updateQueryStringParameter(window.location.href, 'export_csv', 'export_csv');
            window.open(new_url, '_blank').focus();
        });

        let last_string = $(this).val();

        $(document).on("keyup", "#search_string", function () {

            let search_delay = function (last_string) {

                let string = $("#search_string").val().trim();

                if (string.length <= 2) {
                    return false;
                }

                if (last_string === string) {
                    return false;
                }
                let new_url;
                if (string !== "") {
                    new_url = updateQueryStringParameter(window.location.href, 'search', string);
                } else {
                    new_url = updateQueryStringParameter(window.location.href, 'search', '');
                }
                last_string = string;
                redirect(new_url);
            };

            setTimeout(search_delay(last_string), 700);
        });

    });

</script>
<?php


?>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Записи на автосервис</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">

                    <div class="col-sm-2 table-toolbar-left dropdown">
                        <a href="/sto/record_list" class="btn btn-success">Сбросить фильтр</a>
                    </div>
                    <div class="col-sm-2 dropdown">
                        <select id="city_id" class="main_select use_select2 city_select" name="city_id">

                            <option value="0">Все регионы</option>
                            <? foreach ($cities as $city) {
                                $city_name = $city['City']['name'];
                                $city_id = $city['City']['id'];
                                ?>
                                <option value="<?= $city_id ?>" <? if ($city_id == $form_data['city_id']) echo 'selected'; ?>>
                                    <?= $city_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-2 dropdown">
                        <input type="text" id="search_string" name="search_string" placeholder="Поиск..."
                               value="<? if (isset($form_data['search'])) echo $form_data['search']; ?>"/>
                    </div>

                    <div class="col-sm-1 dropdown">
                        <label class="control-label">Фильтр по дате (от / до)</label>
                    </div>

                    <div class="col-sm-2 dropdown">
                        <input type="text" id="date_from" name="date_from"  class="datepicker"
                               value="<? if (isset($form_data['date_from'])) echo $form_data['date_from']; ?>"/>
                    </div>

                    <div class="col-sm-2 dropdown">
                        <input type="text" id="date_to" name="date_to" class="datepicker"
                               value="<? if (isset($form_data['date_to'])) echo $form_data['date_to']; ?>"/>
                    </div>

                    <div id="export_csv" class="btn btn-primary col-sm-1"> => в excel</div>

                </div>

            </div>

            <hr>

            <? if (count($record_list) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Запись</th>
                            <th>Пользователь</th>
                            <th>Дата записи</th>
                            <th>Желаемая дата</th>
                            <th>Машина</th>
                            <th>Автосервис</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;

                        foreach ($record_list as $record) {
                            $n++;
                            $record_data = $record['Auto_Service_Record'];
                            $User_data = $record['User'];
                            $Car_data = $record['User_Car'];
                            $Location_data = $record['Location'];
                            $Car_mark = $record['Car_Mark'];
                            $Car_model = $record['Car_Model'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/sto/view/<?= $record_data['id'] ?>">Запись №<?= $record_data['id'] ?>
                                    </a>
                                </td>
                                <td><?= prepare_fio($User_data['lastname'], $User_data['firstname'], $User_data['middlename']) ?></a></td>
                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($record_data['created']) ?>
                                    </span>
                                </td>
                                <td><span class="text-info"><i
                                                class="pli-clock"></i> <?= lang_calendar($record_data['service_datetime']) ?>
                                </span>
                                </td>
                                <td>
                                    <b><?=$Car_mark['name'] ?>, <?=$Car_model['name'] ?></b> рег.номер <b><?=$Car_data['car_number'] ?></b>,
                                </td>
                                <td>
                                    <?=$Location_data['address'] ?>
                                </td>
                                <td>
                                    ---
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/sto/record_list";
                            $params = array(
                                'city_id' => $form_data['city_id'],
                                'search' => $form_data['search'],
                                'date_from' =>  $form_data['date_from'],
                                'date_to' =>  $form_data['date_to'],
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>
                <p class="muted">Записей нет</p>
            <? } ?>

        </div>
    </div>
</div>