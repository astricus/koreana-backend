<?php

$record_data = $record['Auto_Service_Record'];
$User_data = $record['User'];
$Car_data = $record['User_Car'];
$Location_data = $record['Location'];
$Car_mark = $record['Car_Mark'];
$Car_model = $record['Car_Model'];
?>
<div class="row">
    <a class="btn btn-success btn-bock" href="/sto/record_list"><i class="fa fa-list"></i> Список записей на автосервис</a>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><h3>Запись №<?= $record_data['id'] ?></h3></h3>

        </div>
        <div class="panel-body">
            <div class="panel-body">
                <div class="blog-title media-block">
                    <div class="media-body">
                        <p>Владелец машины <a href="/user/<?=$record_data['user_id']?>"
                                              class="btn-link"><?= prepare_fio($User_data['lastname'], $User_data['firstname'], $User_data['middlename']) ?></a></a>
                        </p>
                    </div>
                </div>
                <div class="blog-content">
                    <div class="blog-body">
                        Комментарий к записи:
                        <?= $record_data['comment']; ?>
                    </div>
                </div>

                <div class="blog-content">
                    <div class="blog-body">Запись добавлена
                        <span class="text-info"><i
                                    class="pli-clock"></i> <?= lang_calendar($record_data['created']) ?>
                                    </span>
                    </div>
                </div>

                <div class="blog-content">
                    <div class="blog-body">Запись на дату/время
                        <span class="text-info"><i
                                    class="pli-clock"></i> <?= lang_calendar($record_data['service_datetime']) ?>
                                    </span>
                    </div>
                </div>

                <div class="blog-content">
                    <div class="blog-body">
                        Автомобиль: <b><?= $Car_mark['name'] ?>, <?= $Car_model['name'] ?></b> рег.номер
                        <b><?= $Car_data['car_number'] ?></b>,
                    </div>
                </div>

                <div class="blog-content">
                    <div class="blog-body">
                        Автосервис: <?= $Location_data['address'] ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>