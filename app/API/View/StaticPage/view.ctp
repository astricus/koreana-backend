<?
$status_list = array();
$status_list[1] = array('success', 'активна');
$status_list[0] = array('danger', 'скрыта');


$st_page_data = $static_page;
$cat_name = $category['Static_Category']['name'];
$cat_id = $category['Static_Category']['id'];

$st_page = $st_page_data['Static_Page'];
$page_content = $st_page_data['Static_Page']['content'];
?>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $st_page['title'] ?></h3>

        </div>
        <div class="panel-body">

            <p>автор: <a href="#" class="btn-link"><?= $st_page_data['author_name']; ?></a></p>
            <p class="label label-success">Страница создана <?= $static_page['days_later'] ?></p>

            <p class="">Категория <a target="_blank" class="btn-link"
                                     href="/static_category/view/<?= $cat_id ?>"><?= $cat_name ?></a></p>
            <hr>


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Содержание</h3>
                </div>

                <?= $page_content ?>
            </div>

            <div class="panel panel-info">
                История изменений страницы
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Действие</th>
                        <th>Автор</th>
                        <th>Дата и время</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($story as $story_elem) {
                        $story_item = $story_elem['Static_Page_Log'];
                        $author = $story_item['author_id'];
                        $action = $story_item['action'];
                        $created = lang_calendar($story_item['created']);
                        ?>
                        <tr class="">
                            <td>
                                <span class="text-info"><?= $action ?></span>
                            </td>

                            <td><span class=""><i
                                            class="pli-clock"></i> <?= lang_calendar($st_page['created']) ?>
                                    </span>
                            </td>
                            <td>
                                <span class="text-info"><?= $story_elem['author_name'] ?></span>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>


        </div>
    </div>
</div>