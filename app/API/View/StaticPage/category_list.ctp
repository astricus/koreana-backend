<script>
    $(document).ready(function () {

    });
</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список категорий</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-2 table-toolbar-left">
                    <a id="btn-addrow" class="btn btn-success btn-bock"
                       href="/static_category/add"><i class="fa fa-plus"></i> Создать подкатегорию</a>
                </div>
                <div class="col-sm-10">
                    <a class="btn btn-success btn-bock" href="/static_pages"><i class="fa fa-list"></i> Список страниц</a>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 table-toolbar-left">
                    <div class="list-group">

                        <? foreach ($categories as $category) {
                            $step_header = $category['step'] + 1;
                            if ($step_header == 1) {
                                $shifter = "";
                            } else if ($step_header == 2) {
                                $shifter = " --- ";
                            } else if ($step_header == 3) {
                                $shifter = " ------ ";
                            } else if ($step_header == 4) {
                                $shifter = " ----------- ";
                            } else if ($step_header == 5) {
                                $shifter = " ---------------- ";
                            }
                            ?>

                            <a href="/static_page/category/view/<?= $category['id'] ?>" class="list-group-item <?if($category['parent_id']==0) echo 'active';?>">
                                <?if($category['pages']>0){?>
                                    <span class="badge badge-success"><?= $category['pages'] ?></span>
                                <?}?>
                                <h6 class="list-group-item-heading"><?= $shifter ?><?= $category['name'] ?></h6>

                            </a>

                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>