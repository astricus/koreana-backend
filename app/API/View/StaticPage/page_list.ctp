<script>
    $(document).ready(function () {
        /* Категория  */
        $(document).on("change", ".category_select", function () {
            let category_id = $('.category_select option:selected').val(), new_url;
            if (category_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'category_id', category_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'category_id', '');
            }
            redirect(new_url);
        });

        /* Платформа */
        $(document).on("change", ".platform_select", function () {
            let platform = $('.platform_select option:selected').val(), new_url;
            if (platform != "") {
                new_url = updateQueryStringParameter(window.location.href, 'platform', platform);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'platform', '');
            }
            redirect(new_url);
        });

        /* Поиск по названию */
        $(document).on("change", ".find_by_name", function () {
            let find_by_name = $('.find_by_name').val(), new_url;
            if (find_by_name.length >= 2) {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', find_by_name);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'find_by_name', '');
            }
            redirect(new_url);
        });

    });

</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Список страниц</h3>
        </div>
        <div class="panel-body">
            <div class="pad-btm form-inline">

                <div class="row">
                    <div class=" table-toolbar-left">
                        <a id="btn-addrow" class="btn btn-success btn-bock"
                           href="/static_page/add"><i class="fa fa-plus"></i>
                            Создать страницу</a>
                        <a class="btn btn-success btn-bock" href="/static_page/categories"><i class="fa fa-cogs"></i>
                            Управление категориями страниц</a>

                        <a class="btn btn-success btn-bock" href="/webforms"><i class="fa fa-code"></i>
                            Управление веб-формами</a>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <a href="/static_pages" class="btn btn-success">показать все страницы</a>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="category_id" class="main_select use_select2 category_select" name="category_id">

                            <option value="0">Все категории</option>
                            <? foreach ($categories as $category) {
                                $category_name = $category['Static_Category']['name'];
                                $category_id = $category['Static_Category']['id'];
                                ?>
                                <option value="<?= $category_id ?>" <? if ($category_id == $form_data['category_id']) echo 'selected'; ?>>
                                    <?= $category_name ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <select id="city_id" class="main_select use_select2 platform_select" name="city_id">
                            <option value="">Все платформы</option>
                            <? foreach ($platforms as $key => $platform) {
                                ?>
                                <option value="<?= $platform ?>" <? if ($platform == $form_data['platform']) echo 'selected'; ?>>
                                    <?= $platform ?>
                                </option>
                            <? } ?>
                        </select>
                    </div>

                    <div class="col-sm-3 table-toolbar-left dropdown">
                        <label class="label control-label label-info" for="find_by_name">Найти статью по
                            названию</label>
                        <input id="find_by_name" type="text" placeholder="поиск статьи по названию"
                               class="main_select use_select2 find_by_name" name="find_by_name"
                               value="<?= $form_data['find_by_name'] ?>">
                    </div>

                </div>

            </div>

            <hr>

            <? if (count($static_pages) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Url для показа</th>
                            <th>Категория</th>
                            <th>Автор</th>
                            <th>Добавлена</th>
                            <th>Платформа</th>
                            <th>Статус</th>
                            <th>Управление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $n = 0;
                        $status_list = array();
                        $status_list[1] = array('success', 'активна');
                        $status_list[0] = array('danger', 'скрыта');

                        foreach ($static_pages as $st_page_data) {
                            $cat_name = $st_page_data['category']['Static_Category']['name'];
                            $cat_id = $st_page_data['category']['Static_Category']['id'];
                            $n++;

                            $st_page = $st_page_data['Static_Page'];
                            ?>
                            <tr class="">
                                <td><?= $n ?></td>
                                <td><a class="btn-link"
                                       href="/static_page/view/<?= $st_page['id'] ?>"><?= $st_page['title'] ?></a>
                                </td>
                                <td>
                                    <span class="text-info"><?= $st_page['url'] ?></span>
                                </td>
                                <td>
                                    <a target="_blank" class="btn-link"
                                       href="static_page/category/view/<?= $cat_id ?>"><?= $cat_name ?></a>
                                </td>

                                <td>
                                    <a class="btn-link"><?= $st_page_data['author_name'] ?></a>
                                </td>
                                <td><span class=""><i
                                                class="pli-clock"></i> <?= lang_calendar($st_page['created']) ?>
                                    </span>
                                </td>
                                <td>
                                    <?= $st_page['platform'] ?></a>
                                </td>
                                <td>
                                    <div class="label label-<?= $status_list[$st_page['enabled']][0] ?>"><?= $status_list[$st_page['enabled']][1] ?></div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <? if ($st_page['enabled'] == "0") { ?>
                                            <a class="btn btn-sm btn-success btn-hover-warning fa fa-unlock add-tooltip"
                                               href="/static_page/unblock/<?= $st_page['id'] ?>"
                                               data-original-title="Открыть страницу" data-container="body"></a>
                                        <? } else { ?>
                                            <a class="btn btn-sm btn-danger btn-hover-success fa fa-unlock add-tooltip"
                                               href="/static_page/block/<?= $st_page['id'] ?>"
                                               data-original-title="Скрыть страницу" data-container="body"></a>

                                            &nbsp;<? } ?>
                                        <a class="btn btn-sm btn-success btn-hover-info pli-pencil add-tooltip"
                                           href="/static_page/edit/<?= $st_page['id'] ?>"
                                           data-original-title="Изменить страницу" data-container="body"></a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div class="fixed-table-pagination">
                    <div class="pull-right pagination">
                        <ul class="pagination">
                            <?php $ctrl_pgn = "/static_pages/";
                            $params = array(
                                'platform' => $form_data['platform'],
                                'category_id' => $form_data['category_id'],
                                //'start_datetime' => $form_data['start_datetime'],
                            );
                            echo new_paginator($page, $pages, "", $params, $ctrl_pgn); ?>
                        </ul>
                    </div>
                </div>

            <? } else { ?>

                <p class="muted">Статичных страниц нет</p>
            <? } ?>

        </div>
    </div>
</div>