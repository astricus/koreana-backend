<div class="row">
    <script>
        $(document).on('nifty.ready', function () {

            $('.summernote').summernote({
                'height': '230px',
                callbacks: {
                    onImageUpload: function (files, editor, welEditable) {

                        for (var i = files.length - 1; i >= 0; i--) {
                            sendFile(files[i], this);
                        }
                    }
                }
            });


            function sendFile(file, el) {
                var form_data = new FormData();
                form_data.append('file', file);
                $.ajax({
                    data: form_data,
                    dataType: 'json',
                    type: "POST",
                    url: '/static_uploader/',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if(data.status=="success"){
                            $(el).summernote('editor.insertImage', data.data.url);
                        } else {
                            alert(data.data.message);
                        }

                    }
                });
            }

            setInterval(function () {
                $("#page_content").val($(".summernote").summernote('code'));
            }, 120);

            $('#save-text').on('click', function () {
                $('#summernote-edit').summernote('destroy');
            });

            $(".datepicker").datepicker();

            $(document).on("change", "#title", function () {
                var val = $(this).val();
                $("#page_url").val(translit(val));
            });

            $(document).on('click', ".delete_page", function (e) {
                if (confirm('Вы уверены, что невозможно хотите удалить данную страницу?')) {
                    var link = $(this).attr('href');
                    window.location.href = link;
                }
                e.preventDefault();
                return false;
            });

        })</script>

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Редактирование статичной страницы <b><?=$st_page['title']?></b></h3>

        </div>

        <div class="row">
            <div class="col-sm-6 table-toolbar-left">
                <a class="btn btn-success btn-bock" href="/static_pages">Список страниц</a>
                <? if ($st_page['enabled'] == 1) { ?>
                    <a class="btn btn-warning btn-bock" href="/static_page/block/<?= $st_page['id'] ?>"><i class="fa fa-close"></i> Скрыть страницу из показа</a>
                <? } else { ?>
                    <a class="btn btn-warning btn-bock" href="/static_page/unblock/<?= $st_page['id'] ?>"><i class="fa fa-eye"></i> Показывать страницу (сейчас скрыта)</a>

                <? } ?>
                <a class="btn btn-danger btn-bock delete_page" href="/static_page/delete/<?=$st_page['id']?>"><i class="fa fa-minus"></i> Удалить данную страницу</a>
            </div>



        </div>

        <hr>

        <form class="form-horizontal" name="edit_page" method="post" action="/static_page/edit/<?=$st_page['id']?>" enctype="multipart/form-data">
            <div class="panel-body">

                <div class="form-group <?if(key_exists("title", $errors)){echo "has-error";}?>">
                    <label class="col-lg-3 control-label" for="title">Заглавие</label>
                    <div class="col-sm-6">
                        <input type="text" id=title class="form-control" name="title" required placeholder="Название страницы"
                               value="<?=$st_page['title']?>"/>
                    </div>
                </div>
                <?if(key_exists("title", $errors)){?>
                    <label class="col-lg-3 control-label error-message" for="title"><?=$errors['title']?></label>
                    <?}?>

                <div class="form-group <?if(key_exists("category_id", $errors)){echo "has-error";}?>">
                    <label class="col-lg-3 control-label" for="title">Категория статьи</label>
                    <div class="col-sm-6">
                        <select id="category_id" class="main_select use_select2 city_select form-control" name="category_id">

                            <option value="0">Все категории</option>
                            <? foreach ($categories as $category) {
                                $category_name = $category['Static_Category']['name'];
                                $category_id = $category['Static_Category']['id'];
                                ?>
                                <option value="<?= $category_id ?>" <?if($category_id == $st_page['category_id']) {echo ' selected="selected" ';}?>>
                                    <?= $category_name ?>
                                </option>
                            <? } ?>
                        </select>

                        <?if(key_exists("category_id", $errors)){?>
                            <span class="text-danger error-message text-semibold"><?=$errors['category_id']?></span>
                        <?}?>
                    </div>
                </div>

                <div class="form-group <?if(key_exists("url", $errors)){echo "has-error";}?>">
                    <label class="col-lg-3 control-label" for="page_url">URL страницы</label>
                    <div class="col-sm-6">
                        <input type="text" id="page_url" class="form-control" name="url" value="<?=$st_page['url']?>"/>
                    </div>
                </div>
                <?if(key_exists("url", $errors)){?>
                    <label class="control-label error-message" for="url"><?=$errors['url']?></label>
                <?}?>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="page_content">Содержание страницы</label>
                    <div class="col-sm-6">
                        <div class="summernote"><?=$st_page['content']?></div>
                        <textarea name="content" id="page_content" style="visibility: hidden">
                            <?=$st_page['content']?>
                        </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="meta-keywords">Мета тег Keywords</label>
                    <div class="col-sm-6">
                        <textarea id="meta-keywords" class="form-control" name="meta-keywords"><?=$st_page['meta-keywords']?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="meta-title">Мета тег title</label>
                    <div class="col-sm-6">
                        <textarea id="meta-title" class="form-control" name="meta-title"><?=$st_page['meta-title']?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="meta-description">Мета тег description</label>
                    <div class="col-sm-6">
                        <textarea id="meta-description" class="form-control" name="meta-description"><?=$st_page['meta-description']?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="meta-custom">Мета тег custom (произвольный Html код в блоке head)</label>
                    <div class="col-sm-6">
                        <textarea id="meta-custom" class="form-control" name="meta-custom"><?=$st_page['meta-custom']?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label" for="platform">Платформа показа</p>
                    </label>
                    <div class="col-sm-6">
                        <select id="platform" class="use_select2 dropdownPlatformSelect" multiple name="platform[]" style="width: 300px">
                            <option value="0">Не выбрано</option>
                            <? foreach ($platforms as $platform) { ?>
                                <option value="<?= $platform; ?>" <?if(substr_count($st_page['platform'], $platform)) {echo ' selected="selected" ';}?>><?= $platform ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input class="btn btn-success" id="submit_edit" type="submit" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>