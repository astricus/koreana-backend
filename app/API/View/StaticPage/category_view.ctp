<script>
    $(document).ready(function () {

    });
</script>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $category['Static_Category']['name'] ?></h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-2 table-toolbar-left">
                    <a id="btn-addrow" class="btn btn-success btn-bock"
                       href="/static_page/category/<?= $category['Static_Category']['id'] ?>"><i class="fa fa-plus"></i>
                        Создать подкатегорию</a>
                </div>
                <div class="col-sm-10">
                    <a class="btn btn-success btn-bock" href="/static_page/categories"><i class="fa fa-list"></i> Список
                        категорий</a>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 table-toolbar-left">
                    <div class="panel panel-colorful panel-dark">
                        <div class="list-group">

                            <? foreach ($sub_categories as $category_item) {
                                $step_header = $category_item['step'] + 1;
                                if ($step_header == 1) {
                                    $shifter = "";
                                } else if ($step_header == 2) {
                                    $shifter = " --- ";
                                } else if ($step_header == 3) {
                                    $shifter = " ------ ";
                                } else if ($step_header == 4) {
                                    $shifter = " ----------- ";
                                } else if ($step_header == 5) {
                                    $shifter = " ---------------- ";
                                }
                                ?>
                                <a href="/static_page/category/view/<?= $category_item['id'] ?>"
                                   class="list-group-item <? if ($category_item['parent_id'] == 0) echo 'active'; ?>">
                                    <? if ($category_item['pages'] > 0) { ?>
                                        <span class="badge badge-success"><?= $category_item['pages'] ?></span>
                                    <? } ?>
                                    <h6 class="list-group-item-heading"><?= $shifter ?><?= $category_item['name'] ?></h6>

                                </a>

                            <? } ?>
                        </div>
                    </div>

                    <form class="panel-body form-horizontal form-padding" name="create_category" method="get"
                          action="/static_page/category/create/<?= $category['Static_Category']['id'] ?>">

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="parent_id">Создать подкатегорию</label>

                            <div class="col-lg-6">
                                <input type="text" name="name" value="" class="form-control"
                                       placeholder="название подкатегории"/>
                            </div>

                            <div class="col-lg-3">
                                <input type="submit" class="btn btn-success" value="Создать подкатегорию">
                            </div>
                        </div>
                    </form>


                    <form class="panel-body form-horizontal form-padding" name="rename_category" method="get"
                          action="/static_page/category/rename/<?= $category['Static_Category']['id'] ?>">

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="category_name">переименовать категорию</label>

                            <div class="col-lg-6">
                                <input type="text" name="name" class="form-control" value="" id="category_name"
                                       placeholder="Название категории"/>
                            </div>

                            <div class="col-lg-3">
                                <input type="submit" class="btn btn-success" value="Переименовать категорию">
                            </div>
                        </div>
                    </form>

                    <form class="panel-body form-horizontal form-padding" name="remove_category" method="get"
                          action="/static_page/category/remove/<?= $category['Static_Category']['id'] ?>">

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="parent_id">Перенести категорию</label>

                            <div class="col-lg-6">
                                <select id="parent_id" class="main_select form-control" name="parent_id">
                                    <option value="0">Все категории (корень)</option>
                                    <?

                                    foreach ($categories as $cur_category) {

                                        $category_name = $cur_category['Static_Category']['name'];
                                        $category_id = $cur_category['Static_Category']['id'];
                                        if ($category_id == $category['Static_Category']['id']) continue;
                                        ?>
                                        <option value="<?= $category_id ?>">
                                            <?= $category_name ?>
                                        </option>
                                    <? } ?>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <input type="submit" class="btn btn-success" value="Перенести категорию">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>