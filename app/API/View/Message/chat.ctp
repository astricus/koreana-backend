<h1 class="select_recipient_title"> Выберите собеседника для общения </h1>

<div class="panel">
    <div class="media-block pad-all bord-btm">
        <div class="pull-right">
            <div class="btn-group dropdown">
                <a href="#" class="dropdown-toggle btn btn-trans" data-toggle="dropdown" aria-expanded="false"><i
                            class="pci-ver-dots"></i></a>
                <ul class="dropdown-menu dropdown-menu-right" style="">
                    <li><a href="#"><i class="icon-lg icon-fw psi-pen-5"></i> редактировать</a></li>
                    <li><a href="#"><i class="icon-lg icon-fw pli-recycling"></i> удалить</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-lg icon-fw pli-mail"></i> Отправить сообщение</a></li>
                    <li><a href="#"><i class="icon-lg icon-fw pli-calendar-4"></i> Смотреть детали</a></li>
                    <li><a href="#"><i class="icon-lg icon-fw pli-lock-user"></i> Закрыть</a></li>
                </ul>
            </div>
        </div>
        <div class="media-left">
            <img class="img-circle img-xs" src="img/profile-photos/8.png" alt="Profile Picture">
        </div>
        <div class="media-body">
            <a data-user_id="" data-user_role="" class="mar-no text-main text-bold text-lg chat_recipient_name" href="//view/">Собеседник - имя</a>
            <small class="text-muteds">Ваш клиент</small>
        </div>
    </div>

    <div class="nano has-scrollbar" style="height: 60vh">
        <div class="nano-content" tabindex="0" style="right: -17px;">
            <div class="panel-body chat-body media-block">
                <div id="chat_container">
                    <?
                    /*
                                            $mess_count = count($last_dialog_messages);
                                            foreach ($last_dialog_messages as $dialog_message) {

                                                $message = $dialog_message['Message'];
                                                $message_direct = ($message['receiver_role'] == $recipient_role) ? "send" : "receive";
                                                $from_type = $message['receiver_role'];

                                                $message_text = $message['message'];
                                                $cite_message = $dialog_message['cite_message'] ?? null;
                                                $message_date = lang_calendar($message['created']);


                                                if ($message_direct == "receive") {
                                                    $class_chat = "chat-user";
                                                    if ($from_type == "orderer") {
                                                        $message_recipient_query = "клиент " . $recipient_name_link . " написал вам";
                                                    } else if ($from_type == "admin") {
                                                        $message_recipient_query = "Администратор " . $recipient_name_link . " написал вам";
                                                    }
                                                    // сообщение входящее
                                                } else {
                                                    $class_chat = "chat-me";
                                                    // сообщение исходящее
                                                    if ($from_type == "orderer") {
                                                        $message_recipient_query = "Вы написали клиенту " . $recipient_name_link;
                                                    } else if ($from_type == "admin") {
                                                        $message_recipient_query = "Вы написали администратору " . $recipient_name_link;
                                                    }
                                                }
                                                if ($message_direct == "receive") {
                                                    $recipient_avatar = "img/profile-photos/3.png";
                                                } else {
                                                    $recipient_avatar = "img/profile-photos/1.png";
                                                }


                                                ?>
                                                <div class="<?= $class_chat ?> chat_message">
                                                    <div class="media-left">
                                                        <img src="<?= $recipient_avatar ?>" class="img-circle img-sm" alt="Profile Picture">
                                                    </div>
                                                    <div class="media-body">
                                                        <div>
                                                            <p><?= $message_text ?>
                                                                <small><?= $message_date ?></small>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? } */ ?>
                </div>
                <div class="chat-meta-day"><span
                            class="badge">
                        <span class="message_counter"><?= 5 . " " . word_count_format(5, array("сообщение", "сообщения", "сообщений")) ?></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="nano-pane">
            <div class="nano-slider" style="height: 57px; transform: translate(0px);"></div>
        </div>
    </div>
    <div class="pad-all bord-all">
        <form action="/chat/send_message" method="post" name="send_chat_message" id="send_chat_message">
            <div class="input-group">
                <input type="hidden" class="" name="chat_id" value="">
                <input type="hidden" class="" name="recipient_role" value="">
                <input type="hidden" class="" name="recipient_id" value="">
                <input type="hidden" class="" name="type" value="">
                <textarea placeholder="Напечатать сообщение" class="form-control form-control-trans" name="message"
                          id="chat_textarea"><?= now_date() ?></textarea>

                <span class="input-group-btn">
                     <button class="btn btn-icon add-tooltip" data-original-title="Добавить файл"
                             type="button"><i class="fa fa-file-image icon-lg"></i></button>
                    <!--
                     <button class="btn btn-icon add-tooltip" data-original-title="Эмоции"
                             type="button"><i class="pli-smile icon-lg"></i></button> -->
                     <button class="btn btn-icon add-tooltip" data-original-title="Отправить"
                             type="submit"><i class="pli-paper-plane icon-lg"></i></button>
                </span>

            </div>
        </form>
    </div>
</div>
