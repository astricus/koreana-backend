<?php

App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class MobileController extends AppController
{

    public $components = [
        'Activity',
        'Admin',
        'Api',
        'Breadcrumbs',
        'EmailCom',
        'Flash',
        'Log',
        'Session',
    ];

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(
            "Запросы в МП",
            Router::url(['plugin' => false, 'controller' => 'index', 'action' => 'index'])
        );

        parent::beforeFilter();
    }

    public function delete_mobile(){
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор api req"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Api->deleteApiRequest($id);
        if (!$delete) {
            $this->Flash->set(__("Ошибка удаления api запроса"), ['element' => 'flash']);
        } else {
            $comment = 'Удаление запроса API с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("api", $this->Admin->manager_id(), $comment, $activity);
            $this->Flash->set(__("Запрос успешно удален"), ['element' => 'flash']);
        }
        return $this->redirect($this->referer());
    }

    public function mobile_log(){
            $show_count = 20;
            $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
            $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
            $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

            $request_date = $this->request->query['request_date'] ?? null;
            $client_id = $this->request->query['client_id'] ?? null;
            $component = $this->request->query['component'] ?? null;

            $page = (is_numeric($page)) ? $page : 0;
            if ($page <= 0 or !is_numeric($page)) $page = 1;
            $limit_page = $show_count * ($page - 1);

            $filter = [];
            if($request_date!=null){
                $filter["request_date"] = $request_date;
            }
            if($client_id!=null){
                $filter["client_id"] = $client_id;
            }
            if($component!=null){
                $filter["component"] = $component;
            }

            $pages = ceil($this->Api->totalAPICount($filter) / $show_count);

            if ($sort !== "id" && $sort !== "created" && $sort !== "time") {
                $sort = "created";
            }

            if ($sort_dir !== "asc" && $sort_dir !== "desc") {
                $sort_dir = "desc";
            }



            $req_list = $this->Api->getAPIRequests($show_count, $page, $sort, $sort_dir, $filter);

            $this->set("requests", $req_list);
            $this->set("page", $page);
            $this->set("pages", $pages);
            $form_data = array(
                'filter' => $filter,
                'sort' => $sort,
                'client_id' => $client_id,
                'component' => $component,
                'request_date' => $request_date
            );

            $this->set('components', $this->Api->uniqueComponents());
            $this->set('clients', $this->Api->uniqueRequestClients());

            $this->set('form_data', $form_data);
            $this->set('blocked_ips', $this->Api->getBlockedIp());
    }

    public function mobile_view(){
        $id = $this->request->param('id');
        $mobile_request = $this->Api->getAPIRequest($id);
        $this->set("request", $mobile_request);
    }

    public function requests_by_ip(){
        $ip = $this->request->param('ip');

        $show_count = 20;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $totals = $this->Api->totalAPICountByIp($ip);
        $pages = ceil($totals / $show_count);

        $req_list = $this->Api->getRequestsByIp($ip, $show_count, $page);

        $this->set("requests", $req_list);
        $this->set("ip", $ip);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $this->set("total_count", $totals);

        // блокировки
        $ip_status = $this->Api->filterIp($ip) ? "blocked" : "active";
        $this->set("ip_status", $ip_status);
    }

    public function blocks_by_ip(){
        $ip = $this->request->param('ip');

        $this->Api->checkUnblock($ip);

        $show_count = 20;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $totals = $this->Api->totalIpBlocksCountByIp($ip);
        $pages = ceil($totals / $show_count);


        $this->set("ip", $ip);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $this->set("total_count", $totals);

        $block_list = $this->Api->getBlocksByIp($ip, $show_count, $page);
        $this->set("block_list", $block_list);

        $ip_status = $this->Api->filterIp($ip) ? "blocked" : "active";
        $this->set("ip_status", $ip_status);
        $this->set("ip_status", $ip_status);
    }

    public function block_ip()
    {
        $ip = $this->request->param('ip');
        if (empty($ip)) {
            $this->Flash->set(__("Передан пустой ip адрес"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Api->manualBlockIp($ip);

        $comment = 'Блокирование ip адрес в мобильных запросах ip[#$ip]';
        $activity['$ip'] = $ip;
        $this->Activity->add("mobile", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Ip адрес $ip заблокирован для мобильных запросов"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public function unblock_ip()
    {
        $ip = $this->request->param('ip');
        if (empty($ip)) {
            $this->Flash->set(__("Передан пустой ip адрес"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Api->manualUnBlockIp($ip);

        $comment = 'Разблокирование ip адрес в мобильных запросах ip[#$ip]';
        $activity['ip'] = $ip;
        $this->Activity->add("mobile", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Ip адрес $ip разблокирован для мобильных запросов"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }
}