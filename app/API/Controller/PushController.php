<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class PushController extends AppController {
    public $uses = [
        'Push',
        'User',
        'Push_Sent',
        'User_Device',
    ];

    public $components = [
        'Activity',
        'Admin',
        'Api',
        'Breadcrumbs',
        'City',
        'Content',
        'Flash',
        'Push',
        'Session',
        'User'
    ];

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(
            L('MAIN_PAGE'),
            Router::url(['plugin' => false, 'controller' => 'index', 'action' => 'index'])
        );
        parent::beforeFilter();
    }

    public function city_list()
    {
        $this->set('cities', $this->City->getCityList());
    }

    public function view_push()
    {
        $id = $this->params->id ?? null;
        if ($id == null or intval($id) == 0) {
            die("push id is null!");
        }
        $push_message = $this->Push->getPushByid($id);
        $this->set("push", $push_message);
        $this->set('push_component', $this->Push);
    }

    public function push_list()
    {
        $show_count = 10;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $city_id = $this->request->query['city_id'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];

        $filter["city_id"] = null;
        if ($city_id != null) {
            $filter["city_id"] = $city_id;
        }

        $push_messages = $this->Push->pushList($show_count, $page, $sort, $sort_dir, $filter);
        //        if (count($locations) > 0) {
        //            $cities_arr = [];
        //            foreach ($locations as &$location) {
        //                if (!in_array($location['Location']['city_id'], array_keys($cities_arr))) {
        //                    $city_name = $this->City->getCityNameById($location['Location']['city_id']);
        //                    $cities_arr[$location['Location']['city_id']] = $city_name;
        //                    $location['Location']['city_name'] = $city_name;
        //                } else {
        //                    $location['Location']['city_name'] = $cities_arr[$location['Location']['city_id']];
        //                }
        //            }
        //        }
        $this->set("push_messages", $push_messages);
        $this->set("page", $page);
        $pages = ceil($this->Push->totalPushCount($filter) / $show_count);
        $this->set("pages", $pages);
        $form_data = [
            'filter' => $filter,
        ];

        $form_data['city_id'] = $city_id;
        $this->set('form_data', $form_data);
        $this->set('cities', $this->City->getCityList());
        $this->set('push_component', $this->Push);
    }

    public function push_users()
    {
        $show_count = 10;
        $push_id = $this->request->param("id");
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $push_users = $this->Push->pushSentList($push_id, $show_count, $page, $sort, $sort_dir);
        $this->set("push_users", $push_users);
        $this->set("page", $page);
        $push_total_users = $this->Push->totalPushSentCount($push_id);
        $pages = ceil($push_total_users / $show_count);
        $this->set("pages", $pages);

        $this->set("push_sent_statuses", $this->Push->push_sent_statuses);
        $this->set("push_id", $push_id);
        $push = $this->Push->getPushById($push_id);
        $this->set("push_name", $push['name']);
        $this->set("push_total_users", $push_total_users);
        // все пользователи
        $all_users = $this->User->userSimpleList();
        $this->set("all_users", $all_users);

    }

    public function edit_push()
    {
        $id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор сообщения"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $push_message = $this->Push->getPushById($id);
            if ($push_message == null) {
                $this->Flash->set(__("Сообщение с таким id ($id) не найдено"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $new_push_message = $this->Push->updatePushMessage($id, $data);

            $image_name = "push_image";
            $file_max_size = $this->Content->image_params['max_size_mb'];
            if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                    $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                }
                //width & height
                $res = $this->Push->savePushImage(
                    $_FILES[$image_name],
                    $this->Content->image_params['default_image_width'],
                    $this->Content->image_params['default_image_height'],
                    $id
                );
                if ($res['status'] == 'ok') {
                    $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash']);
                } else {
                    $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash']);
                }
            }

            if (!empty($new_push_message["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $new_push_message["error"]), ['element' => 'flash']);

                return $this->redirect($this->referer());
            } else {

                $comment = 'Изменение данных сообщения с id[#id]';
                $activity['id'] = $id;
                $this->Activity->add("push_messages", $this->Admin->manager_id(), $comment, $activity);

                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/push/edit/" . $id);
            }
        } else {
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор сообщения"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $push_message = $this->Push->getPushById($id);
            if ($push_message == null) {
                $this->Flash->set(__("Сообщение с таким id ($id) не найдена"));

                return $this->redirect($this->referer());
            }
            $this->set('title', $push_message['name']);
            $this->set('push_message', $push_message);
            if (!empty($push_message['push_image'])) {
                $push_image_file = $this->Push->getPushImageUrl($push_message['push_image']);
            } else {
                $push_image_file = null;
            }

            $this->set('push_image', $push_image_file);
            $this->set('cities', $this->City->getCityList());
            $this->set('valid_message_types', $this->Push->valid_message_types);
        }
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash_error']);

                return $this->redirect($this->referer());
            }
            $new_push_message = $this->Push->createPushMessage($data);
            if (!empty($new_push_message["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $new_push_message["error"]), ['element' => 'flash_error']);

                return $this->redirect($this->referer());
            } else {
                $id = $new_push_message;

                $file_max_size = $this->Content->image_params['max_size_mb'];
                $image_name = "push_image";
                if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                    if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                        $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                    }
                    //width & height
                    $res = $this->Push->savePushImage(
                        $_FILES[$image_name],
                        $this->Content->image_params['default_image_width'],
                        $this->Content->image_params['default_image_height'],
                        $id
                    );
                    if ($res['status'] == 'ok') {
                        $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash_success']);
                    } else {
                        $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash_error']);
                    }
                } else {
                    $this->Flash->set(__("Уведомление успешно создано"), ['element' => 'flash_success']);
                }

                $comment = 'Добавление push-сообщения с id[#id]';
                $activity['id'] = $id;
                $this->Activity->add("push", $this->Admin->manager_id(), $comment, $activity);

                $this->redirect("/push/users/" . $id);
            }
        } else {
            $this->set('title', "Добавление push-сообщения");
            $cities = $this->City->getCityList();
            $this->set('cities', $cities);
            $this->set('valid_message_types', $this->Push->valid_message_types);
        }
    }

    public function delete_push()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор сообщения"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $delete = $this->Push->deletePush($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);

            return $this->redirect($this->referer());
        } else {

            $comment = 'Удаление push-сообщения с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("push", $this->Admin->manager_id(), $comment, $activity);

            $this->Flash->set(__("push-сообщение была удалено"), ['element' => 'flash']);
            $this->redirect("/push/list");
        }
        exit;
    }

    public function delete_receiver()
    {
        $id = $this->request->param('push_id') ?? null;
        $push_sent_id = $this->request->param('push_sent_id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $delete = $this->Push->deleteReceiver($push_sent_id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);

            return $this->redirect($this->referer());
        } else {

            $comment = 'Удаление пользователя из подписки на push-уведомления с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("push", $this->Admin->manager_id(), $comment, $activity);

            $this->Flash->set(__("Получатель push-уведомления был удален из списка"), ['element' => 'flash']);
            $this->redirect("/push/users/" . $id);
        }
        exit;
    }


    public function send_push()
    {
        $id = $this->request->param('id') ?? null;

        if ($id == null) {
            $this->Flash->set(__("Не задан идентификатор сообщения"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $push_message = $this->Push->getPushById($id);
        if ($push_message == null) {
            $this->Flash->set(__("Уведомление с таким id ($id) не найдено"));

            return $this->redirect($this->referer());
        }
        $rs = $this->Push->getPushRecipients($id);
        $this->Push->sendPush($rs, $id);
        $this->Flash->set(__("Уведомление успешно отправлено!"), ['element' => 'flash']);

        return $this->redirect("/push/list");

    }

    public function add_receiver()
    {
        $push_id = $this->params->push_id ?? null;
        if ($push_id == null or intval($push_id) == 0) {
            die("push id is null!");
        }
        if ($this->request->is('post')) {
            $user_ids = $this->request->data('user_ids') ? $this->request->data('user_ids') : [];
            if (count($user_ids) > 0) {
                foreach ($user_ids as $user_id) {
                    if ($this->User->checkUserExists($user_id)) {
                        $this->Push->addPushReceiver($push_id, $user_id);
                    }
                }
            }
            $this->Flash->set(__("Получатель уведомления успешно добавлен"), ['element' => 'flash']);
            //exit;
        }
        return $this->redirect("/push/users/" . $push_id);
    }

    public function send_push_to_user()
    {
        $sent_id = $this->request->param('sent_id') ?? null;
        $push_id = $this->request->param('push_id') ?? null;
        if ($push_id == null or intval($push_id) == 0) {
            die("push id is null!");
        }
        if ($sent_id == null or intval($sent_id) == 0) {
            die("push sent id is null!");
        }
        $result = $this->Push->sendPushToUser($sent_id);
        if ($result) {
            $this->Flash->set(__("Уведомление успешно отправлено!"), ['element' => 'flash_success']);
        } else {
            $this->Flash->set(__("Уведомление не удалось отправить!" . " " . $this->Push->firebase_error), ['element' => 'flash_error']);
        }

        return $this->redirect("/push/users/" . $push_id);
    }
}