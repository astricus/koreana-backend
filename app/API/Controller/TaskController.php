<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class TaskController extends AppController
{
    public $uses = array(
        'Task',
    );

    public $components = array(
        'Activity',
        'Access',
        'Api',
        'Breadcrumbs',
        'Http',
        'Flash',
        'Session',
        'Task'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
        //$this->Access->checkControlAccess("news");
    }

    public function task_list()
    {
        $show_count = 20;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $status = $this->request->query['status'] ?? null;
        $start_date = $this->request->query['start_date'] ?? null;
        $result_status = $this->request->query['result_status'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->Task->totalTaskCount($status, $start_date, $result_status) / $show_count);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if($status!=null){
            $filter["status"] = $status;
        }
        if($start_date!=null){
            $filter["start_date"] = $start_date;
        }
        if($result_status!=null){
            $filter["result_status"] = $result_status;
        }

        $task_list = $this->Task->taskList($show_count, $limit_page, $sort, $sort_dir, $filter);
        $this->set("task_list", $task_list);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['status'] = $status;
        $form_data['start_date'] = $start_date;
        $form_data['result_status'] = $result_status;


        $this->set('valid_statuses', $this->Task->valid_statuses);
        $this->set('valid_result_statuses', $this->Task->valid_result_statuses);

        $this->set('form_data', $form_data);
    }

    public function view()
    {
        $id = $this->request->param('id');
        $news = $this->New->getNewById($id);
        $news_author_id = $news['author_id'];
        $author = $this->New->getAuthorByAuthorId($news_author_id);
        $news['author_name'] = $author;
        $news['days_later'] = days_later(time() - strtotime($news['created']));

        $this->set("new", $news);
    }

    public function edit_task()
    {
        $task_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($task_id == null) {
                $this->Flash->set(__("Не задан идентификатор плановой задачи"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $task = $this->Task->getTaskById($task_id);
            if ($task == null) {
                $this->Flash->set(__("Плановая задача с таким id ($task_id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $this->Task->updateTask($task_id, $data);

            $comment = 'Изменение плановой задачи с id[#id] и названием [#title]';
            $activity['id'] = $task_id;
            $activity['title'] = $task['name'];
            $this->Activity->add("tasks", $this->Admin->manager_id(), $comment, $activity);

            if (count( $this->Task->errors) > 0) {
                $this->Flash->set(__("Ошибка " . join(" ", $this->Task->errors)), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/task/edit/" . $task_id);
            }
        } else {
            if ($task_id == null) {
                $this->Flash->set(__("Не задан идентификатор плановой задачи"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $task = $this->Task->getTaskById($task_id);
            if ($task == null) {
                $this->Flash->set(__("Плановая задача с таким id ($task_id) не найдена"));
                return $this->redirect($this->referer());
            }

            $this->set('title', "Плановая задача №$task_id");
            $this->set('task', $task);
        }
    }

    public function add_task()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }

            $task_name = $data['name'];
            $args = $data['args'];
            $code = $data['code'];
            $start_date = $data['start_date'];
            $start_hour = $data['start_hour'];
            $start_minute = $data['start_minute'];
            $task_create = $this->Task->addTaskToQueue($task_name, $args, $code, $start_date, $start_hour, $start_minute);

            if (count( $this->Task->errors) > 0) {
                $this->Flash->set(__("Ошибка " . join(" ", $this->Task->errors)), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $id = $task_create;
                $comment = 'Создание плановой задачи с id[#id] и названием [#title]';
                $activity['id'] = $id;
                $activity['title'] = $task_name;
                $this->Activity->add("news", $this->Admin->manager_id(), $comment, $activity);

                $this->redirect("/task/list/");
            }
        } else {
            $this->set('title', "Добавление плановой задачи");
        }
    }

    public function delete_task()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор плановой задачи"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Task->deleteTask($id);
        if (count( $this->Task->errors) > 0) {
            $this->Flash->set(__("Ошибка " . join(" ", $this->Task->errors)), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {
            $comment = 'Удаление плановой задачи с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("task", $this->Admin->manager_id(), $comment, $activity);
            $this->Flash->set(__("Задача была успешно удалена"), ['element' => 'flash']);
            $this->redirect("/task/list");
        }
        exit;
    }

}