<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ActivityController extends AppController
{
    public $components = array(
        'Session',
        'Breadcrumbs',
        'Flash',
        'Activity'
    );

    public $uses = ["Search"];

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function getComponents()
    {
        return  $this->Activity->getComponents();
    }

    public function index()
    {
        $show_count = 10;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $component = $this->request->query['component'] ?? null;
        $activity_date = $this->request->query['date'] ?? null;
        $admin_id  = $this->request->query['admin_id'] ?? null;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $action_count = $this->Activity->getActivityCount(null, null, null);
        $pages = ceil($action_count / $show_count);

        $actions = $this->Activity->getActivityList($page, null, $admin_id, $component, $activity_date);
        $this->set("actions", $actions);

        $this->set('page', $page);
        $this->set('pages', $pages);

        $this->set('activityCom', $this->Activity);

        $this->set('action_count', $action_count);
        $this->set('title', "Активность");
        $form_data = array(
            'component' => $component,
            'date' => $activity_date,
            'admin_id' => $admin_id
        );
        $this->set('form_data', $form_data);
        $this->set('components', $this->getComponents());
        $this->set('admins', $this->Admin->get_managers());


    }

}