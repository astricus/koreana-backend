<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ApiController extends AppController
{
    public $components        = [
        "Account",
        "Access",
        "Admin",
        "Api",
        'Session',
        'Breadcrumbs',
        'Flash',
        'SiteData',
        'User',
        'Validator',
    ];

    public $api_error_list    = [
        'undefined_version'  => 'Неопределенная версия API',
        'auth_required'      => 'Требуется авторизация для допуска к данному методу/сущности API',
        'undefined_platform' => 'Неопределенный тип приложения',
        'undefined_entity'   => 'Неопределенный тип данных',
        'undefined_method'   => 'Неопределенный метод',
        'undefined_token'    => 'Нет токена авторизации',
    ];

    public $api_platform_list = [
        'site',
        'mobile',
        'all',
    ];

    public $api_entity_list   = [
        "app",
        "action",
        "new",
        "region",
        'location',
        "user",
        "car",
        "sto",
        "static",
        'service',
        "push",
    ];

    public $error_detected;

    public function beforeFilter()
    {
        $this->time = microtime(true);
        $this->Breadcrumbs->add(
            L('MAIN_PAGE'),
            Router::url(['plugin' => false, 'controller' => 'index', 'action' => 'index'])
        );
        parent::beforeFilter();
    }

    public $layout = "default";

    private function isJson($data)
    {
        return (json_decode($data) != null) ? true : false;
    }

    public function addApiRequest()
    {
        if ($this->request->is('post')) {
            $api_request = $this->request->data;
            if ($this->validateApiRequest($api_request)) {
                if ($this->Api_Request->save($api_request)) {
                    $api_request_id = $this->Api_Request->getLastInsertId();
                    $data           = ["api_request_id" => $api_request_id];
                    $this->Api->response_api($data, "success");
                } else {
                    $data = $this->Api_Request->errors;
                    $this->Api->response_api($data, "error");
                }
                exit;
            }
        } else {
            $this->render("../Restapi/add_api_request");
        }

    }

    private function validateApiRequest($data)
    {
        $errors = [];
        if (empty($data['api_url'])) {
            //$errors[] =
        }
        $name           = $data['api_name'] ?? null;
        $request_method = $data['request_method'] ?? null;
        $auth_requires  = $data['auth_requires'] ?? null;
        $platform       = $data['platform'] ?? null;
        $description    = $data['description'] ?? null;
        $request_data   = $data['request_data'] ?? null;
        $response_data  = $data['response_data'] ?? null;
        $api_errors     = $data['errors'] ?? null;
        $enabled        = "false";
        $author_id      = $this->user_id();
        $enabled        = "false";
        //if()

        //url 	name 	request_method 	auth_requires 	platform 	description 	request_data 	response_data 	errors 	enabled 	author_id
    }

    public function auth()
    {
        //информационный ответ
        $token = $this->request->data('token');
        pr($this->request->data);
        exit;
        $this->Api->auth_requires($token);
        exit;
    }

    /**
     * @param $error
     * @param $http_error_code
     * @param $platform
     * @param $version
     *
     * @return mixed
     */
    public function apiErrorReturn($error, $http_error_code, $platform, $version)
    {
        return $this->Api->apiErrorReturn($error, $http_error_code, $platform, $version);
    }

    /**
     * @param $version
     */
    private function validateApiVersion($version)
    {
        if (empty($version)) {
            $api_error = $this->api_error_list['undefined_version'];
            $this->apiErrorReturn($api_error, "500", null, null);
        }
        $this->version = $version;
    }

    /**
     * @param $platform
     */
    private function validateApiPlatform($platform)
    {
        if (!in_array($platform, $this->api_platform_list)) {
            $api_error = $this->api_error_list['undefined_platform'];
            $this->apiErrorReturn($api_error, "500", null, $this->version);
        }
        $this->platform = $platform;
    }

    /**
     * @param $method
     */
    private function validateApiMethod($method)
    {
        if (empty($method)) {
            $api_error = $this->api_error_list['undefined_method'];
            $this->apiErrorReturn($api_error, "500", $this->platform, $this->version);
        }
        $this->method = $method;
    }

    /**
     * @param $entity
     */
    private function validateApiEntity($entity)
    {
        if (!in_array($entity, $this->api_entity_list)) {
            $api_error = $this->api_error_list['undefined_entity'];
            $this->apiErrorReturn($api_error, "500", $this->platform, $this->version);
        }
        $this->entity = $entity;
    }

    /**
     * @param $token
     */
    private function validateApiToken($token)
    {
        if (empty($token)) {
            $api_error = $this->api_error_list['undefined_token'];
            $this->apiErrorReturn($api_error, "500", $this->platform, $this->version);
        }
        $this->Api->auth_requires($token);
    }

    /**
     * @param $version
     * @param $platform
     * @param $entity
     * @param $id
     * @param $method
     * @param $token
     * @param $data
     * @param $params
     */
    private function preValidateApiMethod($version, $platform, $entity, $id, $method, $token, $data, $params)
    {
        $this->validateApiVersion($version);
        $this->validateApiPlatform($platform);
        $this->validateApiMethod($method);
        $this->validateApiPlatform($platform);
        $this->validateApiEntity($entity);

        if (($method == "auth" && $entity == "app") || ($entity =="user" && $method == "check_code")
            || ($entity =="user" && $method == "auth")
        ) {

        } else {
            $this->validateApiToken($token);
        }

    }



    public function apiMethod()
    {
        $this->Api->start_time = microtime(true);
        $this->token = $this->request->query('token') ?? null;
        $data        = $this->request->data ?? null;
        if ($data == null) {
            $data = $this->request->input('json_decode');
            if (!empty($data)) {
                $data = (array)$data;
            }
        }
        $params   = $this->request->query ?? null;
        $version  = $this->request->param('version') ?? null;
        $platform = $this->request->param('platform') ?? null;
        $entity   = $this->request->param('entity') ?? null;
        $id       = $this->request->param('id') ?? null;
        $method   = $this->request->param('method') ?? null;

        // filter block ip
        $is_blocked = $this->Api->checkBlockIp(get_ip());
        if($is_blocked){
            $this->Api->response_api("your ip is banned for all API requests", "error", null, null);
            exit;
        }

        $this->checkBuildVersion($data);

        $this->Api->preToken($this->token);
        //метод конфиденциальных данных должен срабатывать до валидации полей запроса и токена
        $this->catchPrivateApiMethod($version, $platform, $entity, $id, $method, null, $data, $params);

        $this->preValidateApiMethod($version, $platform, $entity, $id, $method, $this->token, $data, $params);
        $this->catchUserAuthApiMethod($version, $platform, $entity, $id, $method, $this->token, $data, $params);
        $this->catchTestApiMethod($version, $platform, $entity, $id, $method, $this->token, $data, $params);

        //Api ping method
        $this->catchPingApiMethod($version, $platform, $entity, $id, $method, $this->token, $data, $params);

        //$this->catchSiteInfoApiMethod($entity, $method);
        $this->catchSiteDataApiMethod($version, $platform, $entity, $id, $method, $this->token, $data, $params);

        $this->layout = false;
        $result       = $this->Api->runApiBaseMethod(
            $entity,
            $method,
            $this->token,
            $id,
            $params,
            $platform,
            $version,
            $data
        );
        if (empty($result)) {
            $error_status = "error";
            //$this->apiErrorReturn($this->Api->API_RESPONSE_DATA_NOT_FOUND, "400", $platform, $version);
        } else {
            $error_status = "success";
        }
        if (empty($result) && ($method == "user_records" && $entity == "sto")) {
            $error_status = "success";
        }
        if (empty($result) && ($method == "list" && $entity == "service")) {
            $error_status = "success";
        }

        /*
         * Изначально было нереализовано некорректно, result содержал только одно поле, планируется
         *  переход к возвращению массива, первый элемент массива - данные ответа, второй элемент массива - текст ошибок,
         * имплементировано для методов: User->SaveProfile
         */
        if ($method == "save_profile" && $entity == "user") {
            $error_status = $result["status"];
            $result = $result["error"] ?? ['success' => true];
        }

        $this->Api->response_api($result, $error_status, $platform, $version);

        $this->Api->saveApiRequest(
            $version,
            $platform,
            $entity,
            $id,
            $method,
            $this->token,
            $data,
            $params,
            $result,
            null
        );
        exit;
    }

    /**
     * @param $entity
     * @param $method
     */
    private function catchTestApiMethod($version, $platform, $entity, $id, $method, $token, $data, $params)
    {
        if ($entity == "app" && $method == "test") {
            $this->Api->response_api("test api method", "success", $this->platform, $this->version);
            $this->Api->saveApiRequest(
                $version,
                $platform,
                $entity,
                $id,
                $method,
                $token,
                $data,
                $params,
                " method App:Test ",
                ""
            );
            exit;
        }
    }

    /**
     * @param $version
     * @param $platform
     * @param $entity
     * @param $id
     * @param $method
     * @param $token
     * @param $data
     * @param $params
     * @return void
     */
    private function catchPingApiMethod($version, $platform, $entity, $id, $method, $token, $data, $params)
    {
        if ($entity == "app" && $method == "ping") {
            $this->Api->response_api("ping api method", "success", $this->platform, $this->version);
            exit;
        }
    }

    /**
     * @param $version
     * @param $platform
     * @param $entity
     * @param $id
     * @param $method
     * @param $token
     * @param $data
     * @param $params
     * @return void
     */
    private function catchUserAuthApiMethod($version, $platform, $entity, $id, $method, $token, $data, $params)
    {
        if ($entity == "user" && $method == "auth") {
            $token = $this->Api->createNewToken();
            $this->Api->saveApiRequest(
                $version,
                $platform,
                $entity,
                $id,
                $method,
                $token,
                $data,
                $params,
                " method User:Auth",
                $error = ""
            );
            $this->User->authUserApi($token, $params, $data);
            exit;
        }
    }

    /*
    public function catchSiteInfoApiMethod($entity, $method)
    {
        if ($entity == "app" && $method == "site_info") {
            $data_array = [];

           $this->Api->response_api([$data_array], "success", $this->platform, $this->version);
            exit;
        }
    }
    */

    public function catchSiteDataApiMethod($version, $platform, $entity, $id, $method, $token, $data, $params)
    {
        if ($entity == "app" && $method == "site_data") {
            $data_array = [];
            if ($id == "private_agree") {
                $data_array['private_agree'] = $this->SiteData->getSiteData("private_agree");
            } elseif ($id == "site_info") {
                $data_array['site_phone'] = $this->SiteData->getSiteData("site_phone");
                $data_array['site_name']  = $this->SiteData->getSiteData("site_name");
                //                if (is_prod_app()) {
                //                    $data_array = [$data_array];
                //                }
            } elseif ($id == "screens") {
                $data_array['screens'] = $this->SiteData->getScreensApi();
            } else {
                $data_array[$id] = $this->SiteData->getSiteData($id);
            }

            $this->Api->saveApiRequest(
                $version,
                $platform,
                $entity,
                $id,
                $method,
                $token,
                $data,
                $params,
                " method Site:$id ",
                ""
            );
            $this->Api->response_api($data_array, "success", $this->platform, $this->version);
            exit;
        }
    }

    public function checkBuildVersion($data){
        if(is_array($data) && count($data)>0 && key_exists("build", $data)){
            $this->Api->checkBuildVersion($data["build"]);
        }

    }

    public function catchPrivateApiMethod($version, $platform, $entity, $id, $method, $token, $data, $params)
    {
        if ($entity == "app" && $method == "site_data" && $id == "private_agree") {

            $data_array['private_agree'] = $this->SiteData->getSiteData("private_agree");
            $this->Api->saveApiRequest(
                $version,
                $platform,
                $entity,
                $id,
                $method,
                $token,
                $data,
                $params,
                " method Site:$id ",
                ""
            );
            $this->Api->response_api($data_array, "success", $this->platform, $this->version);
            exit;
        }
    }

    public function undefined_method()
    {
        $this->Api->response_api("Request of undefined api method", "warning", null, null);
        exit;
    }

}