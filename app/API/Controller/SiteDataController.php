<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class SiteDataController extends AppController
{
    public $uses       = [
        'Site_Data',
    ];

    public $components = [
        'Activity',
        'Account',
        'Admin',
        'Access',
        'Api',
        'Breadcrumbs',
        'Flash',
        'Session',
        'Platform',
        'Content',
        'SiteData',
    ];

    public $layout     = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(
            L('MAIN_PAGE'),
            Router::url(['plugin' => false, 'controller' => 'index', 'action' => 'index'])
        );
        parent::beforeFilter();
        $this->Access->checkControlAccess("create_new_pages");
    }

    public function data_list()
    {
        $this->set('title', "административная панель - данные работы");
        $site_data = $this->SiteData->getSiteDataList();
        $this->set('site_data', $site_data);
    }

    public function edit_data_field()
    {
        $id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор поля данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $data_field = $this->SiteData->getDataFieldById($id);
            if ($data_field == null) {
                $this->Flash->set(__("Поле данных с таким id ($id) не найдено"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);
            $content = $data['content'];
            $title   = $data['title'];
            $name    = $data['name'];

            $data_field_update = $this->SiteData->updateDataField(
                $id,
                $title,
                $name,
                $content
            );

            $comment           = 'Изменение поля данных с id[#id] и названием [#title]';
            $activity['id']    = $id;
            $activity['title'] = $data['title'];
            $this->Activity->add("site_data", $this->Admin->manager_id(), $comment, $activity);

            if (!empty($data_field_update["error"]) > 0) {
                $this->Session->write("errors", $this->SiteData->errors);
                $this->Session->write("error_fields", $this->SiteData->error_fields);
                $this->Flash->set(__("Ошибка " . $data_field_update["error"]), ['element' => 'flash']);

                return $this->redirect($this->referer());
            } else {
                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/data_field/edit/" . $id);
            }
        } else {
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор экрана"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $data_field = $this->SiteData->getDataFieldById($id);
            if ($data_field == null) {
                $this->Flash->set(__("Поле данных с таким id ($id) не найден"));

                return $this->redirect($this->referer());
            }

            $this->set('errors', $this->Session->read("errors") ?? []);
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);

            $this->set('title', "Поле данных №$id");
            $this->set('data_field', $data_field['Site_Data']);
        }
    }

    public function add_data_field()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                response_ajax("Нет данных", "error");
                exit;
            }

            $this->Session->write("add_form_data", null);
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);

            $content = $data['content'];
            $title   = $data['title'];
            $name    = $data['name'];

            $field_create = $this->SiteData->createDataField(
                $title,
                $name,
                $content
            );

            if (!$field_create) {
                $this->Session->write("add_form_data", $data);
                $this->Session->write("errors", $this->SiteData->errors);
                $this->Session->write("error_fields", $this->SiteData->error_fields);
                $this->Flash->set(__("Возникли ошибки при создании поля данных: "), ['element' => 'flash']);

                //response_ajax(["errors" => $this->Static->errors, "error_fields" => $this->Static->error_fields], "error");
                return $this->redirect($this->referer());
            } else {
                $this->Session->write("add_form_data", null);
                $this->Session->write("errors", null);
                $this->Session->write("error_fields", null);
                $id = $field_create;

                $comment           = 'Создание экрана с id[#id] и названием [#title]';
                $activity['id']    = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("site_data", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Поле данных успешно создано"), ['element' => 'flash']);

                return $this->redirect("/site_data");

            }
        } else {
            $reset = $this->request->query('reset');
            if ($reset == "reset") {
                $this->Session->write("add_form_data", null);

                return $this->redirect($this->referer());
            }
            $this->set('add_form_data', $this->Session->read("add_form_data"));
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);
            $this->set("errors", $this->Session->read('errors') ?? []);
            $this->set('title', "Добавление поля данных");
        }
    }

    public function delete_data_field()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор поля данных"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $delete = $this->SiteData->deleteDataField($id);

        $comment        = 'Удаление поля данных с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("site_data", $this->Admin->manager_id(), $comment, $activity);

        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);

            return $this->redirect($this->referer());
        } else {
            return $this->redirect("/site_data");
        }

    }

    // ЭКРАНЫ ПРИВЕТСТВИЙ

    public function saveStaticImage($name, $file)
    {

    }

    public function screen_uploader()
    {
        $image_name    = "file";
        $file_max_size = $this->max_size_mb;
        if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
            if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                $error = ['message' => "Файл больше " . $file_max_size . " MB"];
                response_ajax($error, "error");
                exit;
            }
            $file_tmp = $_FILES[$image_name]['tmp_name'];
            list($width, $height) = getimagesize($file_tmp);
            $file_width  = $width;
            $file_height = $height;
            //width & height
            $res = $this->Content->saveContentImage($_FILES[$image_name], $file_width, $file_height);
            if ($res['status'] == 'ok') {
                response_ajax(['message' => "Файл успешно сохранен", 'url' => $res['url']], "success");
                exit;
            } else {
                response_ajax(['message' => "Ошибка при сохранении файла: " . $res['msg']], "error");
                exit;
            }
        } else {
            response_ajax(['message' => "Файла нет"], "error");
        }

        exit;
    }

    public function screen_list()
    {
        $screen_list = $this->SiteData->getScreenList();
        $this->set("screens", $screen_list);
        $this->set('content_dir', Configure::read('CONTENT_UPLOAD_DIR_RELATIVE'));

    }

    public function view_screen()
    {
        $id     = $this->request->param('id');
        $screen = $this->SiteData->getScreenById($id);
        $this->set("screen", $screen);
    }

    public function edit_screen()
    {
        $screen_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            if ($screen_id == null) {
                $this->Flash->set(__("Не задан идентификатор страницы"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $screen = $this->SiteData->getScreenById($screen_id);
            if ($screen == null) {
                $this->Flash->set(__("Экран с таким id ($screen_id) не найден"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);
            $content = $data['content'];
            $title   = $data['title'];
            $name    = $data['name'];

            $screen_update = $this->SiteData->updateScreen(
                $screen_id,
                $title,
                $name,
                $content
            );

            $comment           = 'Изменение экрана с id[#id] и названием [#title]';
            $activity['id']    = $screen_id;
            $activity['title'] = $data['title'];
            $this->Activity->add("screen", $this->Admin->manager_id(), $comment, $activity);

            if (!empty($screen_update["error"]) > 0) {
                $this->Session->write("errors", $this->SiteData->errors);
                $this->Session->write("error_fields", $this->SiteData->error_fields);
                $this->Flash->set(__("Ошибка " . $screen_update["error"]), ['element' => 'flash']);

                return $this->redirect($this->referer());
            } else {
                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/welcome_screen/edit/" . $screen_id);
            }
        } else {
            if ($screen_id == null) {
                $this->Flash->set(__("Не задан идентификатор экрана"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $screen = $this->SiteData->getScreenById($screen_id);
            if ($screen == null) {
                $this->Flash->set(__("Экран с таким id ($screen_id) не найден"));

                return $this->redirect($this->referer());
            }

            $this->set('errors', $this->Session->read("errors") ?? []);
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);

            $this->set('title', "Экран №$screen_id");
            $this->set('screen', $screen['Screen']);
        }
    }

    public function add_screen()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                response_ajax("Нет данных", "error");
                exit;
            }

            $this->Session->write("add_form_data", null);
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);

            $content = $data['content'];
            $title   = $data['title'];
            $name    = $data['name'];

            $screen_create = $this->SiteData->createScreen(
                $title,
                $name,
                $content
            );

            if (!$screen_create) {
                $this->Session->write("add_form_data", $data);
                $this->Session->write("errors", $this->SiteData->errors);
                $this->Session->write("error_fields", $this->SiteData->error_fields);
                $this->Flash->set(__("Возникли ошибки при создании экрана: "), ['element' => 'flash']);

                //response_ajax(["errors" => $this->Static->errors, "error_fields" => $this->Static->error_fields], "error");
                return $this->redirect($this->referer());
            } else {
                $this->Session->write("add_form_data", null);
                $this->Session->write("errors", null);
                $this->Session->write("error_fields", null);
                $id = $screen_create;

                $comment           = 'Создание экрана с id[#id] и названием [#title]';
                $activity['id']    = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("screen", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Экран успешно создан"), ['element' => 'flash']);

                return $this->redirect("/welcome_screen/view/" . $id);

            }
        } else {
            $reset = $this->request->query('reset');
            if ($reset == "reset") {
                $this->Session->write("add_form_data", null);

                return $this->redirect($this->referer());
            }
            $this->set('add_form_data', $this->Session->read("add_form_data"));
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);
            $this->set("errors", $this->Session->read('errors') ?? []);
            $this->set('title', "Добавление экрана");
        }
    }

    public function block_screen()
    {
        $id = $this->request->param('id');
        if ($this->SiteData->getScreenById($id) == null) {
            $this->Flash->set(__("Экран с таким id ($id) не найден"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->SiteData->blockScreen($id);

        $comment        = 'Отключение из показа экрана с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("welcome_screens", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Экран скрыт из показа в мобильном приложении"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function unblock_screen()
    {
        $id = $this->request->param('id');
        if ($this->SiteData->getScreenById($id) == null) {
            $this->Flash->set(__("Экран с таким id ($id) не найден"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->SiteData->unblockScreen($id);

        $comment        = 'Открытие для показа экрана с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("welcome_screens", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Экран открыт для показа в мобильном приложении"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function screens_by_api()
    {
        $screen_list = $this->Static->getScreenList();

        return $screen_list;
    }

}