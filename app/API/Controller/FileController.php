<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class FileController extends AppController
{
    public $uses = array(
        'File',
        'Directory',
    );

    public $components = array(
        'Api',
        'Account',
        'Admin',
        'Session',
        'Breadcrumbs',
        'Flash',
        'File',
        'Infrastructure'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        // HARDCODE TODO
        $this->project_id = 1;
        parent::beforeFilter();
    }

    public function index()
    {
        //список файлов
        $this->set('title', "Файловое хранилище");
    }

    public function get_dir_path(){
        echo $this->File->getPathByDirectoryId(1);
        exit;
    }

    public function uploadFileInManager(){
        $file = $this->request->params['form']['uploaded'];
        $dir_id = $this->request->data('dir_id');

        $file_name = $file['name'];
        $file_type = $file['type'];
        $tmp_name = $file['tmp_name'];
        $error = $file['error'];
        $size = $file['size'];
        if(!empty($error)){
            $result = ["error" => "не удалось загрузить файл"];
            response_ajax($result, "error");
        }
        $upload_dir = Configure::read('FILE_TEMP_DIR');
        $new_file_name = $upload_dir . DS . $file_name;
        move_uploaded_file($tmp_name, $new_file_name);
        $this->File->saveUploadedFile($new_file_name, $file_name, $dir_id, $size);

        exit;
    }

    public function downloadRemoteFile(){
        $file_id = $this->request->param("file_id");
        // проверка наличия файла в кеше для того, чтобы не загружать его с удаленного хранилища
        $file_cached = $this->File->getRemoteFileCached($file_id);
        if($file_cached!==null){
            $result = [
                "link" => $file_cached,
            ];
            response_ajax($result, "success");
            exit;
        }
        //кеш отсутствует, грузим по запросу
        $file = $this->File->getRemoteFile($file_id);
        $result = [
            "link" => $file,
        ];
        response_ajax($result, "success");
        exit;
    }


    public function getDownloadFile(){
        $file_link = $this->request->query("f");
        $upload_dir = Configure::read('FILE_TEMP_DIR');
        $local_file = $upload_dir . DS . $file_link;
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $file_link);
        readfile($local_file);
        exit;
    }

    public function delete_file(){
        $id = $this->request->param('file_id') ?? null;
        if($id==null){
            response_ajax($this->File->FILE_NOT_EXISTS_ERROR, "error");
            exit;
        }
        $this->File->deleteFile($id);
        response_ajax("deleted", "success");
        exit;
    }

    public function list_ajax(){
        $dir_id = (int)$this->request->data("dir_id") ?? 0;
        $list = $this->File->getDirectoryList($dir_id, "all", null, null, 0);
        $dirs = $this->File->getChildrenDirectories($dir_id);
        if($dirs==null){
            $dirs =[];
        }
        $current_size = $this->File->limitFilesSizeTotal();
        if($dir_id>0){
            $parent_dir = $this->File->getParentDirectory($dir_id);
        } else {
            $parent_dir = null;
        }
        $dir_path = explode("/", $this->File->getPathByDirectoryId($dir_id));
        $dir_part_array = $this->File->getPathIdsByDirId($dir_id);

        $result = [
            "files" => $list,
            "dirs" => $dirs,
            "parent_dir" => $parent_dir,
            "current_size" => $current_size,
            "dir_path" => $dir_path,
            "dir_ids" => $dir_part_array

        ];
        response_ajax($result, "success");
        exit;
    }

    public function file_list()
    {
        //список файлов
        $this->set('title', "Ваше хранилище файлов");
        $dir_id = $this->params['directory'];
        $this->set('dir_id', $this->params['directory']);
        $this->File->getCurrentStorageSize();
        $limit = $this->File->formatSizeUnits($this->File->limitFilesSizeTotal());
        $current_size = $this->File->formatSizeUnits($this->File->getCurrentStorageSize());
        if($current_size == 0){
            $percent = 0;
        } else {
            $percent = sprintf("%1.2f", (float)$current_size/(float)$limit*100);
        }
        $parent_dir = $this->File->getParentDirectory($dir_id);
        $this->set('parent_dir', $parent_dir);
        $this->set('percent', $percent);
        $this->set('percent', $percent);
        $this->set('total_limit', $limit);
        $this->set('current_size', $current_size);
        /*
        return;
        $this->File->test_synchro();

        $this->Infrastructure->connectSSHToAccountDb(1);*/
/*
        $this->Account->loadRemoteDataSource();
        pr($this->File->getDirectoryList(null, "all", null, null, 0));
        exit;
        */



/*
        $connect = ssh2_connect('185.185.70.119', '22');
        ssh2_auth_password($connect, "root", "4goaskuns4");

        $remote_com = ssh2_exec($connect , 'ls -al');
        stream_set_blocking($remote_com, true);

        $stream_out = ssh2_fetch_stream($remote_com, SSH2_STREAM_STDIO);
        $rl = stream_get_contents($stream_out);


        pr($rl);*/
/*
        $rootDir = __DIR__; // __DIR__ = C:\xampp\htdocs\CodeWall

        $allFiles = array_diff(scandir($rootDir . "/"), [".", ".."]); // Use array_diff to remove both period values eg: ("." , "..")

        print_r($allFiles);

        //scp /path/to/local/file username@hostname:/path/to/remote/file
        exit;*/
    }

    public function preview_file()
    {
        /*
        //список машин
        $drivers = $this->Driver->getAllDrivers();
        $this->set('drivers', $drivers);*/
        $this->set('title', "Песочница методов API");
        $api_method = $this->request->param('api_method') ?? null;
        $api_component = $this->request->param('api_component') ?? null;
        $this->set("action_url", "manage" . DS . "sandbox" . DS .  $api_component . DS . $api_method . DS . "run");
        $this->set("method_list", $this->Api->getApiComponentActiveMethods($api_component));
        $this->set("api_component", $api_component);
        $this->set("current_method", $api_method);
        $this->render($api_component . DS . $api_method);
    }

    public function upload_file()
    {
        $api_type = $this->params['api_type'];
        $project_id = $this->params['project_id'];
        if ($api_type == self::HTTP_POST) {
            $this->render("http_post");
            return $this->apiSimpleHttpPost();
        }
        $this->set("project_id", $this->project_id);
    }

    /**
     * @param $api_type
     * @param $api_id
     * @param $project_id
     * @param $data
     */
    public function move_file()
    {
        $api_component = $this->request->data('api_component');
        $api_id = $this->request->data('api_id');
        $project_id = $this->request->data('project_id');
        $data = $this->request->data('data');
        $access_control = $this->Api->isAvailableApiMethod($api_component, $api_id, $project_id);
        if ($access_control["status"]) {
            $result = $this->Api->runApiMethod($api_component, $api_id, $project_id, $data);
            response_ajax($result, "success");
        } else {
            $result = $access_control["error"];
            response_ajax($result, "error");
        }
        exit;
    }

    public function edit_file()
    {

        $api_method = $this->request->param('api_method') ?? null;
        $api_component = $this->request->param('api_component') ?? null;
        $data = $this->request->data;
        if(count($data) == 0){
            $data = $this->request->query;
        }
        $user_data = $data['data'];
        $params = $data['params'];
        $result = $this->Api->runApiMethodInSandbox($api_component, $api_method, $user_data, $params);
        response_ajax($result, "success");
        exit;
    }

    public function save_file()
    {
        $filename = $this->request->param('filename') ?? null;
        $data = $this->request->data;
        if(count($data) == 0){
            $data = $this->request->query;
        }
        $user_data = $data['data'];
        $params = $data['params'];
        $result = $this->File->runApiMethodInSandbox($api_component, $api_method, $user_data, $params);
        response_ajax($result, "success");
        exit;
    }

    public function search_file()
    {

        $api_method = $this->request->param('api_method') ?? null;
        $api_component = $this->request->param('api_component') ?? null;
        $data = $this->request->data;
        if(count($data) == 0){
            $data = $this->request->query;
        }
        $user_data = $data['data'];
        $params = $data['params'];
        $result = $this->Api->runApiMethodInSandbox($api_component, $api_method, $user_data, $params);
        response_ajax($result, "success");
        exit;
    }







    public function apiSimpleHttpPost()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            $project_id = $this->request->data['project_id'] ?? null;//
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));
                return $this->redirect($this->referer());
            }
            $api_item_create = $this->Api->createDriver($data);
            if (!empty($api_item_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $api_item_create["error"]));
                return $this->redirect($this->referer());
            } else {
                $api_item_id = $api_item_create;
                $this->redirect("/sandbox/http_post/$project_id/" . $api_item_id);
            }
        } else {
            $this->set('title', "Добавление нового метода API");
        }
        $this->render("http_post");
    }

    // ---  помойка ---

    public function edit_driver()
    {
        $driver_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));
                return $this->redirect($this->referer());
            }
            if ($driver_id == null) {
                $this->Flash->set(__("Не задан идентификатор водителя"));
                return $this->redirect($this->referer());
            }
            $driver = $this->Driver->getDriverById($driver_id);
            if ($driver == null) {
                $this->Flash->set(__("Водитель с таким id ($driver_id) не найден"));
                return $this->redirect($this->referer());
            }
            $driver_create = $this->Driver->saveDriver($driver_id, $data);
            if (!empty($driver_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $driver_create["error"]));
                return $this->redirect($this->referer());
            } else {
                $driver_id = $driver_create;
                $this->redirect("/driver/edit/" . $driver_id);
            }
        } else {
            if ($driver_id == null) {
                $this->Flash->set(__("Не задан идентификатор машины"));
                return $this->redirect($this->referer());
            }
            $driver = $this->Driver->getDriverById($driver_id);
            if ($driver == null) {
                $this->Flash->set(__("Водитель с таким id ($id) не найден"));
                return $this->redirect($this->referer());
            }

            $this->set('title', "Водитель №$driver_id");
            $this->set('driver', $driver);
        }
    }

    public function add_driver()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));
                return $this->redirect($this->referer());
            }
            $driver_create = $this->Driver->createDriver($data);
            if (!empty($driver_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $driver_create["error"]));
                return $this->redirect($this->referer());
            } else {
                $driver_id = $driver_create;
                $this->redirect("/driver/edit/" . $driver_id);
            }
        } else {
            $this->set('title', "Добавление нового водителя");
        }
    }

    public function delete_driver()
    {
        $driver_id = $this->request->param('id') ?? null;
        if ($driver_id == null) {
            $this->Flash->set(__("Некорректный идентификатор водителя"));
            return $this->redirect($this->referer());
        }
        $driver_delete = $this->Driver->deleteDriver($driver_id);
        if (!empty($driver_delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $driver_delete["error"]));
            return $this->redirect($this->referer());
        } else {
            $this->redirect("/drivers");
        }
        exit;
    }

    /**
     * @return CakeResponse|null
     */
    public function unblock_driver()
    {

        $id = $this->request->param('id');
        if ($this->Driver->getDriverById($id) == null) {
            $this->Flash->set(__("Водитель с таким id ($id) не найден"));
            return $this->redirect($this->referer());
        }
        $this->Driver->setDriverActive($id);
        $this->Flash->set(__("Водитель разблокирован"));
        return $this->redirect($this->referer());
    }

    /**
     * @param $id
     * @return CakeResponse|null
     */
    public function block_driver()
    {
        $id = $this->request->param('id');
        if ($this->Driver->getDriverById($id) == null) {
            $this->Flash->set(__("Водитель с таким id ($id) не найдена"));
            return $this->redirect($this->referer());
        }
        $this->Driver->setDriverBlocked($id);
        $this->Flash->set(__("Водитель заблокирован"));
        return $this->redirect($this->referer());
    }


}