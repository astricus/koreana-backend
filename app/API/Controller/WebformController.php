<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class WebformController extends AppController
{
    public $uses = array(
        'Webform',
    );

    public $components = array(
        'Access',
        'Activity',
        'Breadcrumbs',
        'Content',
        'DataProvider',
        'Flash',
        'Session',
        'Static',
        'Webform'
    );

    public $layout = "default";

    public function beforeFilter()
    {

        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
        $this->Access->checkControlAccess("create_new_pages");
    }

    public function webforms()
    {
        $show_count = 2;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $find_by_name = $this->request->query['find_by_name'] ?? null;
        $enabled = $this->request->query['enabled'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->Webform->totalWebformCount($find_by_name, $enabled) / $show_count);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if ($enabled != null) {
            $filter["enabled"] = $enabled;
        }
        if ($find_by_name != null) {
            $filter["find_by_name"] = $find_by_name;
        }

        if ($pages > 0) {
            $webforms = $this->Static->webformList($show_count, $limit_page, $sort, $sort_dir, $filter);
            /*
            if (count($webforms) > 0) {
                foreach ($webforms as &$webform) {
                    $webform_id = $webform['Static_Page']['id'];
                    $page_item['author_name'] = $this->Static->getStaticPageAuthor($page_id);
                    $page_item['category'] = $this->Static->getCategoryById($page_item['Static_Page']['category_id']);
                }
            }*/
        } else {
            $webforms = null;
        }
        $this->set("webforms", $webforms);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['find_by_name'] = $find_by_name;
        $this->set('form_data', $form_data);

    }

    public function view_webform()
    {
        $id = $this->request->param('id');
        $webform = $this->Webform->getWebformById($id);
        $webform['days_later'] = days_later(time() - strtotime($webform['Webform']['created']));
        $this->set("webform_id", $id);
        $this->set("webform", $webform);
        //$this->set("form_data",  $this->Webform->getFormData($id));
        $form_data = $this->Webform->getFormData($id);
        foreach ($form_data as &$form_data_elem) {
            $provider_id = $form_data_elem['Webform_Field']['field_value_list_id'] ?? null;
            if ($provider_id != null) {
                $provider_list = $this->DataProvider->getProviderList($provider_id);
                $form_data_elem['provider_list'] = $provider_list;
            }
        }
        $this->set("form_data", $form_data);
    }

    public function edit_webform()
    {
        $webform_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($webform_id == null) {
                $this->Flash->set(__("Не задан идентификатор веб-формы"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $webform = $this->Webform->getWebformById($webform_id);
            if ($webform == null) {
                $this->Flash->set(__("Веб-форма с таким id ($webform_id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);
            $content = $data['content'];
            $name = $data['name'];
            $utm = $data['utm'];

            $news_update = $this->Static->updateWebform($webform_id, $content, $name, $utm);

            if (count($data['webform']) > 0) {
//                pr($data['webform']);
////                exit;
                $updated_ids = [];
                $old_ids = $this->Webform->getFormFieldIds($webform_id);

                $elem_count = count($data['webform']['name']);
                for ($x = 0; $x < $elem_count; $x++) {
                    $last_code = null;
                    $last_field_id = null;
                    //получение идентификаторов, которые есть и в новой форме и в старой и обновление данных по ним
                    // получение полей, у которых пустые идентификаторы для добавления их в форму
                    // удаление полей, у которых есть идентификаторы в старой форме, но которые отсутствуют в новой форме
                    if ($data['webform']['id'][$x] == "") {
                        // новое поле
                        $this->Webform->addWebformField($webform_id,
                            $data['webform']['name'][$x],
                            $data['webform']['type'][$x],
                            $data['webform']['data_provider_id'][$x],
                            $data['webform']['code'][$x],
                            $data['webform']['placeholder'][$x],
                            $x
                        );
                            //trigger same radio buttons
                            $field__type = $data['webform']['type'][$x];
                            $field__code = $data['webform']['code'][$x];
                            if ($last_code !== $field__code) {
                                //new element
                                $field_id = $this->Webform->addWebformField($webform_id,
                                    $data['webform']['name'][$x],
                                    $field__type,
                                    0,
                                    $field__code,
                                    $data['webform']['placeholder'][$x] ?? "",
                                    $x
                                );
                                $last_field_id = $field_id;
                            } else {
                                $field_id = $last_field_id;
                            }
                            if ($field__type == "input_radio") {
                                // create first radio element
                                $this->Webform->addRadioValue($field_id, $data['webform']['input_radio'][$x]);
                            } else if ($field__type == "input_checkbox") {
                                // create first checkbox element
                                $this->Webform->addCheckboxValue($field_id, $data['webform']['input_checkbox'][$x]);
                            }
                            $last_code = $field__code;
                    } else {
                        // обновляется поле
                        $updated_ids[] = $data['webform']['id'][$x];
                        $this->Webform->updateWebformField($webform_id,
                            $data['webform']['id'][$x],
                            $data['webform']['name'][$x],
                            $data['webform']['type'][$x],
                            $data['webform']['data_provider_id'][$x],
                            $data['webform']['code'][$x],
                            $data['webform']['placeholder'][$x],
                            $x
                        );
                    }

                }
                $deleted_ids = array_diff($old_ids, $updated_ids);
                if (count($deleted_ids) > 0 && $this->Webform->hasWebformOrders($webform_id) == 0) {
                    foreach ($deleted_ids as $deleted_id) {
                        $this->Webform->deleteWebformField($webform_id, $deleted_id);
                    }
                }

                $radio_values[] = $data['webform']['radio'];
                if(count($radio_values)) {
                    foreach ($radio_values as $key => $radio_value) {
                        if ($radio_value == "" OR $radio_value == null) {
                            $this->Webform->deleteRadioValue($key);
                        } else {
                            $this->Webform->updateRadioValue($key, $radio_value);
                        }
                    }
                }

                $checkbox_values[] = $data['webform']['checkbox'];
                if(count($checkbox_values)) {
                    foreach ($checkbox_values as $key => $checkbox_value) {
                        if ($checkbox_value == "" OR $checkbox_value == null) {
                            $this->Webform->deleteCheckboxValue($key);
                        } else {
                            $this->Webform->deleteCheckboxValue($key, $checkbox_value);
                        }
                    }
                }
            }

            $comment = 'Изменение вебформы с id[#id] и названием [#name]';
            $activity['id'] = $webform_id;
            $activity['name'] = $data['name'];
            $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

            if (!empty($new_create["error"]) > 0) {
                $this->Session->write("errors", $this->Static->errors);
                $this->Session->write("error_fields", $this->Static->error_fields);
                $this->Flash->set(__("Ошибка " . $news_update["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/webform/edit/" . $webform_id);
            }
        } else {
            if ($webform_id == null) {
                $this->Flash->set(__("Не задан идентификатор страницы"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $webform = $this->Webform->getWebformById($webform_id);
            if ($webform == null) {
                $this->Flash->set(__("Веб-форма с таким id ($webform_id) не найдена"));
                return $this->redirect($this->referer());
            }

            $this->set('errors', $this->Session->read("errors") ?? []);
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);

            $this->set('title', "Веб-форма №$webform_id");
            $this->set('webform', $webform['Webform']);

            $this->set('categories', $this->Static->categoryList());
            $this->set('page_list', $this->Static->pageNameList());

            $form_fields = $this->Webform->getWebformFields();
            $this->set("form_fields", $form_fields);
            $this->set("form_data", $this->Webform->getFormData($webform_id));
            $this->set("form_fields_json", json_encode($form_fields));
            $this->set('data_providers', $this->DataProvider->getActiveDataProviders());

        }
    }

    public function fill_form()
    {
        $webform_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $form_data = $this->request->data ?? null;
            if ($form_data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($webform_id == null) {
                $this->Flash->set(__("Не задан идентификатор веб-формы"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $webform = $this->Webform->getWebformById($webform_id);
            if ($webform == null) {
                $this->Flash->set(__("Веб-форма с таким id ($webform_id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);

            $fill = $this->Webform->fillWebform($webform_id, null, $this->Admin->manager_id(), $form_data);

            if (!$fill) {
                $this->Session->write("errors", $this->Webform->errors);
                $this->Session->write("error_fields", $this->Webform->error_fields);
                $this->Flash->set(__("Ошибка " . join(" ", $this->Webform->errors)), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Flash->set(__("Тестовое заполнение веб-формы произведено "), ['element' => 'flash']);
                $this->redirect("/webform/view/" . $webform_id);
            }
        }
    }

    public function add_webform()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                response_ajax("Нет данных", "error");
                exit;
            }

            $this->Session->write("add_form_data", null);
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);

            $content = $data['content'];
            $name = $data['name'];
            $utm = $data['utm'];
            $validate_webform = $this->Webform->validateWebform($data['webform']);
            if (!$validate_webform) {
                $this->Session->write("add_form_data", $data);
                $this->Session->write("errors", $this->Webform->errors);
                $this->Session->write("error_fields", $this->Webform->error_fields);
                $this->Flash->set(__("Возникли ошибки при создании веб-формы: "), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $id = $this->Static->createWebform($content, $name, $utm);

            if (!$id) {
                $this->Session->write("add_form_data", $data);
                $this->Session->write("errors", $this->Static->errors);
                $this->Session->write("error_fields", $this->Static->error_fields);
                $this->Flash->set(__("Возникли ошибки при создании веб-формы: "), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Session->write("add_form_data", null);
                $this->Session->write("errors", null);
                $this->Session->write("error_fields", null);
                //$radios = [];
                $last_code = null;
                $last_field_id = null;
                //$checkboxes = [];
                if (count($data['webform']) > 0) {
                    pr($data['webform']);
                    $elem_count = count($data['webform']['name']);
                    for ($x = 0; $x < $elem_count; $x++) {
                        //trigger same radio buttons
                        $field__type = $data['webform']['type'][$x];
                        $field__code = $data['webform']['code'][$x];
                        if ($last_code !== $field__code) {
                            //new element
                            $field_id = $this->Webform->addWebformField($id,
                                $data['webform']['name'][$x],
                                $field__type,
                                0,
                                $field__code,
                                $data['webform']['placeholder'][$x] ?? "",
                                $x
                            );
                            $last_field_id = $field_id;
                        } else {
                            $field_id = $last_field_id;
                        }
                        if ($field__type == "input_radio") {
                            // create first radio element
//                            if(empty($data['webform']['input_radio'][$x])){
////                                pr($data['webform']['input_radio']);
////                                echo $x;
////                                exit;
//                            }
                            $this->Webform->addRadioValue($field_id, $data['webform']['input_radio'][$x]);
                        } else if ($field__type == "input_checkbox") {
//                            if(empty($data['webform']['input_checkbox'][$x])){
//                                pr($data['webform']['input_checkbox']);
//                                echo $x;
//                                exit;
//                            }
                            // create first checkbox element
                            $this->Webform->addCheckboxValue($field_id, $data['webform']['input_checkbox'][$x]);
                        }
                        $last_code = $field__code;

                    }

                }

                $comment = 'Создание веб-формы с id[#id] и названием [#name]';
                $activity['id'] = $id;
                $activity['name'] = $data['name'];
                $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Веб форма успешно создана"), ['element' => 'flash']);
                return $this->redirect("/webform/view/" . $id);

            }
        } else {
            $reset = $this->request->query('reset');
            if ($reset == "reset") {
                $this->Session->write("add_webform_data", null);
                return $this->redirect($this->referer());
            }
            $this->set('add_form_data', $this->Session->read("add_form_data"));
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);
            $this->set("errors", $this->Session->read('errors') ?? []);
            $this->set('name', "Добавление веб-формы");
            $this->set('data_providers', $this->DataProvider->getActiveDataProviders());
            $form_fields = $this->Webform->getWebformFields();
            $this->set("form_fields", $form_fields);
            $this->set("form_fields_json", json_encode($form_fields));
        }
    }

    public
    function delete_webform()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор веб-формы"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Static->deleteWebform($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {

            $comment = 'Удаление веб-формы с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

            $this->redirect("/webforms");
        }
        exit;
    }

    public
    function block_webform()
    {
        $id = $this->request->param('id');
        if ($this->Webform->getWebformById($id) == null) {
            $this->Flash->set(__("Веб-форма с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Static->blockWebform($id);

        $comment = 'Отключение из показа веб-формы с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Веб-форма скрыта из показа"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public
    function unblock_webform()
    {
        $id = $this->request->param('id');
        if ($this->Webform->getWebformById($id) == null) {
            $this->Flash->set(__("Веб-форма с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Static->unblockWebform($id);

        $comment = 'Открытие для показа веб-формы с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Веб-форма открыта для показа на сайте"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public
    function connect_to_category()
    {
        $webform_id = $this->request->param('id');
        $connect_id = $this->request->query['connect_id'] ?? null;
        $connect_type = "connect_to_category";
        $connect = $this->Static->connectWF($webform_id, $connect_id, $connect_type);
        if ($connect) {
            $comment = 'Привязка категории category[#category] к веб-форме id[#id]';
            $activity['id'] = $webform_id;
            $activity['category'] = $connect_id;
            $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

            $this->Flash->set(__("Веб-форма показывается на страницах категории №$connect_id"), ['element' => 'flash']);
        } else {
            $this->Flash->set(__("Возникла ошибка при попытке привязки категории страниц к веб-форме"), ['element' => 'flash']);
        }

        return $this->redirect($this->referer());
    }

    public
    function disconnect_from_category()
    {
        $webform_id = $this->request->param('id');
        $connect_id = $this->request->query['connect_id'] ?? null;
        $connect_type = "disconnect_from_category";
        $connect = $this->Static->connectWF($webform_id, $connect_id, $connect_type);
        if ($connect) {
            $comment = 'Категория страниц category[#category] отвязана от веб-формы id[#id]';
            $activity['id'] = $webform_id;
            $activity['category'] = $connect_id;
            $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

            $this->Flash->set(__("Веб-форма не показывается на страницах категории №$connect_id"), ['element' => 'flash']);
        } else {
            $this->Flash->set(__("Возникла ошибка при попытке отвязки категории страниц от веб-формы"), ['element' => 'flash']);
        }

        return $this->redirect($this->referer());
    }

    public
    function connect_to_page()
    {
        $webform_id = $this->request->param('id');
        $connect_id = $this->request->query['connect_id'] ?? null;
        $connect_type = "connect_to_page";
        $connect = $this->Static->connectWF($webform_id, $connect_id, $connect_type);
        if ($connect) {
            $comment = 'Привязка категории category[#category] к веб-форме id[#id]';
            $activity['id'] = $webform_id;
            $activity['category'] = $connect_id;
            $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

            $this->Flash->set(__("Веб-форма показывается на странице №$connect_id"), ['element' => 'flash']);
        } else {
            $this->Flash->set(__("Возникла ошибка при попытке привязки страницы к веб-форме"), ['element' => 'flash']);
        }

        return $this->redirect($this->referer());
    }

    public
    function disconnect_from_page()
    {
        $webform_id = $this->request->param('id');
        $connect_id = $this->request->query['connect_id'] ?? null;
        $connect_type = "disconnect_from_page";
        $connect = $this->Static->connectWF($webform_id, $connect_id, $connect_type);
        if ($connect) {
            $comment = 'Категория страниц category[#category] отвязана от веб-формы id[#id]';
            $activity['id'] = $webform_id;
            $activity['category'] = $connect_id;
            $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

            $this->Flash->set(__("Веб-форма не показывается на странице №$connect_id"), ['element' => 'flash']);
        } else {
            $this->Flash->set(__("Возникла ошибка при попытке отвязки страницы от веб-формы"), ['element' => 'flash']);
        }

        return $this->redirect($this->referer());
    }

    public
    function remove_connect()
    {
        $webform_id = $this->request->param('id');
        $connect_id = $this->request->query['connect_id'] ?? null;
        $connect_type = $this->request->query['connect_type'] ?? null;
        $connect = $this->Static->removeConnectWF($webform_id, $connect_id, $connect_type);
        if ($connect_type == "category") {
            $connect_name = "Категория";
        } else {
            $connect_name = "Страница";
        }
        if ($connect) {
            $comment = "$connect_name category[#category] удалена привязка от веб-формы id[#id]";
            $activity['id'] = $webform_id;
            $activity['category'] = $connect_id;
            $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);
            $this->Flash->set(__("Удалена привязка $connect_name и веб-форма №$connect_id"), ['element' => 'flash']);
        } else {
            $this->Flash->set(__("Возникла ошибка при попытке удаления привязки $connect_name №$connect_id от веб-формы"), ['element' => 'flash']);
        }

        return $this->redirect($this->referer());
    }

    public
    function reports()
    {
        $show_count = 10;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $find_by_name = $this->request->query['find_by_name'] ?? null;
        $enabled = $this->request->query['enabled'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->Webform->totalWebformReportCount($find_by_name, $enabled) / $show_count);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if ($enabled != null) {
            $filter["enabled"] = $enabled;
        }
        if ($find_by_name != null) {
            $filter["find_by_name"] = $find_by_name;
        }

        if ($pages > 0) {
            $webform_reports = $this->Webform->webformReportList($show_count, $limit_page, $sort, $sort_dir, $enabled, $find_by_name);
            /*
            if (count($webforms) > 0) {
                foreach ($webforms as &$webform) {
                    $webform_id = $webform['Static_Page']['id'];
                    $page_item['author_name'] = $this->Static->getStaticPageAuthor($page_id);
                    $page_item['category'] = $this->Static->getCategoryById($page_item['Static_Page']['category_id']);
                }
            }*/
        } else {
            $webform_reports = null;
        }
        $this->set("webform_reports", $webform_reports);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['find_by_name'] = $find_by_name;
        $this->set('form_data', $form_data);

    }

    public
    function view_report()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            die("Отчет по веб-форме не найден");
        }
        $report = $this->Webform->webformReportById($id);
        $this->set("report", $report);
        $this->set("fields", $this->Webform->webformReportDataById($id));
    }

    /**
     * @param $status
     * @return bool
     */
    private
    function validateReportStatus($status)
    {
        if (in_array($status, $this->Webform->valid_report_statuses)) {
            return true;
        }
        return false;
    }

    public
    function update_report_status()
    {
        $id = $this->request->param('id');
        $status = $this->request->param('status');
        if ($this->Webform->webformReportById($id) == null) {
            $this->Flash->set(__("Отчет по веб-форме с таким id ($id) не найден"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }

        if (!$this->validateReportStatus($status)) {
            $this->Flash->set(__("Статус отчета некорректен! Ошибка!"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }

        $this->Webform->updateWebformReportStatus($id, $status);

        $comment = 'Изменение статуса отчета веб-формы с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Статус отчета веб-формы изменен"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    //---------------------------------- управление полями
    /*
     * создание поля, удаление, редактирование, перенос порядка, отключение поля, включение поля
     * Типовые поля {имя, фамилия, отчество, адрес, город, дата, число}
     * Кастомные поля: Тип поля,  обязательное/не обяз., макс значение, мин значение, маска
     * Типы полей: textarea, input_text, input_date, input_file, input_radio, input_checkbox, select,
     *
     * код формы code для написания обработчика формы
     *
     * Настройки формы:
     * 1. Разрешить редактировать форму пользователю после заполнения (да, нет)
     * 2. Форма заполняется только разово, или можно заполнять многократно
     * 3. Кого уведомлять о заполнении формы (пользователь / администратор / никого / обоих)
     * */


    /*
     * список недоработок:
     * 1. Не корректно работает поиск
     * 2. Нет отвязки веб-форм
     * 3. Нет привязки веб-форм (только к одному элементу)
     * */

}