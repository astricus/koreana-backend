<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ActionController extends AppController
{
    public $uses = array(
        'Action',
    );

    public $components = array(
        'Activity',
        'Account',
        'Action',
        'Admin',
        'Breadcrumbs',
        'City',
        'Content',
        'Flash',
        'Session',
        'Platform',
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
        $this->Access->checkControlAccess("actions");
    }

    public function actions()
    {
        $show_count = 10;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";

        $platform = $this->request->query['platform'] ?? null;
        $city_id = $this->request->query['city_id'] ?? null;
        $start_datetime = $this->request->query['start_datetime'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->Action->totalActionCount() / $show_count);

        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";
        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if ($platform != null) {
            $filter["platform"] = $platform;
        }
        if ($city_id != null) {
            $filter["city_id"] = $city_id;
        }
        if ($start_datetime != null) {
            $filter["start_datetime"] = $start_datetime;
        }

        $list = $this->Action->actionList($show_count, $limit_page, $sort, $sort_dir, $filter);
        if (count($list) > 0) {
            foreach ($list as &$item) {
                $author_id = $item['Action']['author_id'];
                $author = $this->Admin->getManagerById($author_id);
                $item['author_name'] = prepare_fio($author['firstname'], $author['lastname'], "");
                $item['cities'] = $this->City->getCityByAction($item['Action']['id']);
            }
        }
        $this->set("actions", $list);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['platform'] = $platform;
        $form_data['city_id'] = $city_id;

        $this->set('platforms', $this->Platform->platforms());
        $this->set('cities', $this->City->getCityList());
        $this->set('form_data', $form_data);
        $this->set('content_dir', Configure::read('CONTENT_UPLOAD_DIR_RELATIVE'));
    }

    public function view()
    {
        $id = $this->request->param('id');
        $action = $this->Action->getActionById($id);
        $action_author_id = $action['author_id'];
        $author = $this->Admin->getManagerById($action_author_id);
        $author = prepare_fio($author['firstname'], $author['lastname'], "");
        $action['author_name'] = $author;
        $action['days_later'] = days_later(time() - strtotime($action['created']));

        $this->set("action", $action);
    }

    public function edit()
    {
        $action_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($action_id == null) {
                $this->Flash->set(__("Не задан идентификатор акции"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $action = $this->Action->getActionById($action_id);
            if ($action == null) {
                $this->Flash->set(__("Акция с таким id ($action_id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            //обновление городов
            if(!key_exists('cities', $data)){
                $data['cities'] = [];
            }
            $this->City->updateCityByAction($action_id, $data['cities']);

            //сохранение изображения
            $file_max_size = $this->Content->image_params['max_size_mb'];
            $image_name = "image";

            if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                    $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                }
                //width & height
                $res = $this->Action->saveActionImage($_FILES[$image_name],
                    $this->Content->image_params['default_image_width'],
                    $this->Content->image_params['default_image_height'],
                    $action_id
                );
                if ($res) {
                    $this->Flash->set(__("Файл успешно сохранен"), ['element' => 'flash']);
                } else {
                    $this->Flash->set(__("Ошибка при сохранении файла"), ['element' => 'flash']);
                }
            }

            $action_create = $this->Action->updateAction($action_id, $data);
            if (!empty($action_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $action_create["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {

                $comment = 'Изменение акции с id[#id] и названием [#title]';
                $activity['id'] = $action_id;
                $activity['title'] = $data['title'];
                $this->Activity->add("actions", $this->Admin->manager_id(), $comment, $activity);

                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/action/edit/" . $action_id);
            }
        } else {
            if ($action_id == null) {
                $this->Flash->set(__("Не задан идентификатор акции"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $action = $this->Action->getActionById($action_id);
            if ($action == null) {
                $this->Flash->set(__("Акция с таким id ($action_id) не найдена"));
                return $this->redirect($this->referer());
            }

            $action_cities = $this->City->getCityByAction($action_id);
            $city_arr = [];
            foreach ($action_cities as $action_city){
                $city_arr[] = $action_city['City']['id'];
            }
            $this->set('action_cities', $city_arr);
            $this->set('cities', $this->City->getCityList());

            $author = $this->Admin->getManagerById($action['author_id']);
            $author = prepare_fio($author['firstname'], $author['lastname'], "");
            $action['author_name'] = $author;

            $this->set('title', "Акция №$action_id");
            $this->set('action', $action);
            $this->set('platforms', $this->Platform->platforms());
            $this->set('action_platforms', $this->Platform->platformsToArray($action['platforms']));
        }
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $create = $this->Action->createAction($data);
            if (!empty($create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $create["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $id = $create;
                $this->City->updateCityByAction($id, $data['cities']);
                $file_max_size = $this->Content->image_params['max_size_mb'];
                $image_name = "image";
                if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                    if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                        $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                    }
                    //width & height
                    $res = $this->Action->saveActionImage($_FILES[$image_name],
                        $this->Content->image_params['default_image_width'],
                        $this->Content->image_params['default_image_height'],
                        $id
                    );
                    if ($res['status'] == 'ok') {
                        $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash']);
                    } else {
                        $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash']);
                    }
                }

                $comment = 'Добавление акции с id[#id] и названием [#title]';
                $activity['id'] = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("actions", $this->Admin->manager_id(), $comment, $activity);

                $this->redirect("/action/view/" . $id);
            }
        } else {
            $this->set('title', "Добавление акции");
            $this->set('platforms', $this->Platform->platforms());
            $this->set('cities', $this->City->getCityList());
        }
    }

    public function delete()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор акции"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Action->deleteAction($id);

        $comment = 'Удаление акции с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("actions", $this->Admin->manager_id(), $comment, $activity);

        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {
            $this->redirect("/actions");
        }
        exit;
    }

    public function block()
    {
        $id = $this->request->param('id');
        if ($this->Action->getActionById($id) == null) {
            $this->Flash->set(__("Акция с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Action->blockAction($id);

        $comment = 'Скрытие акции из показа с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("actions", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Акция скрыта из показа на сайт"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public function unblock()
    {
        $id = $this->request->param('id');
        if ($this->Action->getActionById($id) == null) {
            $this->Flash->set(__("Акция с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Action->unblockAction($id);

        $comment = 'Открытие акции к показу с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("actions", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Акция открыта для показа на сайт"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }


}