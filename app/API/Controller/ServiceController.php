<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ServiceController extends AppController
{
    public $uses = array(
        'Service',
    );

    public $components = array(
        'Activity',
        'Account',
        'Service',
        'Admin',
        'City',
        'Content',
        'Flash',
        'Session',
        'Platform',
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Access->checkControlAccess("actions");
        parent::beforeFilter();
    }

    public function services()
    {
        $show_count = 10;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";

        $platform = $this->request->query['platform'] ?? null;
        $city_id = $this->request->query['city_id'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->Service->totalServiceCount() / $show_count);

        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";
        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if ($platform != null) {
            $filter["platform"] = $platform;
        }
        if ($city_id != null) {
            $filter["city_id"] = $city_id;
        }

        $list = $this->Service->ServiceList($show_count, $limit_page, $sort, $sort_dir, $filter);
        if (count($list) > 0) {
            foreach ($list as &$item) {
                $item['cities'] = $this->City->getCityByService($item['Service']['id']);
            }
        }
        $this->set("services", $list);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['platform'] = $platform;
        $form_data['city_id'] = $city_id;

        $this->set('platforms', $this->Platform->platforms());
        $this->set('cities', $this->City->getCityList());
        $this->set('form_data', $form_data);
        $this->set('content_dir', Configure::read('CONTENT_UPLOAD_DIR_RELATIVE'));
    }

    public function view()
    {
        $id = $this->request->param('id');
        $service = $this->Service->getServiceById($id);
        $service['days_later'] = days_later(time() - strtotime($service['created']));
        $this->set("service", $service);
    }

    public function edit()
    {
        $service_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($service_id == null) {
                $this->Flash->set(__("Не задан идентификатор сервиса"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $service = $this->Service->getServiceById($service_id);
            if ($service == null) {
                $this->Flash->set(__("Сервис с таким id ($service_id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            //обновление городов
            if(!key_exists('cities', $data)){
                $data['cities'] = [];
            }
            $this->City->updateCityByService($service_id, $data['cities']);

            //сохранение изображения
            $file_max_size = $this->Content->image_params['max_size_mb'];
            $image_name = "image";

            if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                    $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                }
                //width & height
                $res = $this->Service->saveServiceImage($_FILES[$image_name],
                    $this->Content->image_params['default_image_width'],
                    $this->Content->image_params['default_image_height'],
                    $service_id
                );
                if ($res) {
                    $this->Flash->set(__("Файл успешно сохранен"), ['element' => 'flash']);
                } else {
                    $this->Flash->set(__("Ошибка при сохранении файла"), ['element' => 'flash']);
                }
            }

            $service_create = $this->Service->updateService($service_id, $data);
            if (!empty($Service_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $service_create["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {

                $comment = 'Изменение сервиса с id[#id] и названием [#title]';
                $activity['id'] = $service_id;
                $activity['title'] = $data['title'];
                $this->Activity->add("services", $this->Admin->manager_id(), $comment, $activity);

                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/service/edit/" . $service_id);
            }
        } else {
            if ($service_id == null) {
                $this->Flash->set(__("Не задан идентификатор сервиса"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $service = $this->Service->getServiceById($service_id);
            if ($service == null) {
                $this->Flash->set(__("Сервис с таким id ($service_id) не найдена"));
                return $this->redirect($this->referer());
            }

            $service_cities = $this->City->getCityByService($service_id);
            $city_arr = [];
            foreach ($service_cities as $service_city){
                $city_arr[] = $service_city['City']['id'];
            }
            $this->set('service_cities', $city_arr);
            $this->set('cities', $this->City->getCityList());

            $this->set('title', "Сервис №$service_id");
            $this->set('service', $service);
            $this->set('platforms', $this->Platform->platforms());
            $this->set('service_platforms', $this->Platform->platformsToArray($service['platforms']));
        }
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $create = $this->Service->createService($data);
            if (!empty($create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $create["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $id = $create;
                $this->City->updateCityByService($id, $data['cities']);
                $file_max_size = $this->Content->image_params['max_size_mb'];
                $image_name = "image";
                if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                    if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                        $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                    }
                    //width & height
                    $res = $this->Service->saveServiceImage($_FILES[$image_name],
                        $this->Content->image_params['default_image_width'],
                        $this->Content->image_params['default_image_height'],
                        $id
                    );
                    if ($res['status'] == 'ok') {
                        $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash']);
                    } else {
                        $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash']);
                    }
                }

                $comment = 'Добавление сервиса с id[#id] и названием [#title]';
                $activity['id'] = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("services", $this->Admin->manager_id(), $comment, $activity);

                $this->redirect("/service/view/" . $id);
            }
        } else {
            $this->set('title', "Добавление сервиса");
            $this->set('platforms', $this->Platform->platforms());
            $this->set('cities', $this->City->getCityList());
        }
    }

    public function delete()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор сервиса"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Service->deleteService($id);

        $comment = 'Удаление сервиса с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("services", $this->Admin->manager_id(), $comment, $activity);

        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {
            $this->redirect("/services");
        }
        exit;
    }

    public function block()
    {
        $id = $this->request->param('id');
        if ($this->Service->getServiceById($id) == null) {
            $this->Flash->set(__("Сервис с таким id ($id) не найден"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Service->blockService($id);

        $comment = 'Скрытие сервиса из показа с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("Services", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Сервис скрыт из показа"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public function unblock()
    {
        $id = $this->request->param('id');
        if ($this->Service->getServiceById($id) == null) {
            $this->Flash->set(__("Сервис с таким id ($id) не найден"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Service->unblockService($id);

        $comment = 'Открытие сервиса к показу с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("services", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Сервис открыт для показа"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }


}