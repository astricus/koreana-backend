<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class RouteController extends AppController
{
    public $uses = array(
        'Car',
        'Car_Type',
        'Driver',
        'Loader',
        'Route_Point',
    );

    public $default_view_points = 50;

    public $components = array(
        'Admin',
        'Address',
        'Breadcrumbs',
        'Car',
        'DateTime',
        'Driver',
        'Flash',
        'Route',
        'Session',
        'Validator'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function route_list()
    {
        /*
         *
         *         $show_count = 3;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $admin_count = $this->Manager->find("count",
            array('conditions' =>
                array(),
            )
        );
        $pages = ceil($admin_count / $show_count);

        $admins = $this->Manager->find('all',
            array('conditions' => array(),
                'fields' => array('Manager.*',
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS reg_time',
                    //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(last_activity) AS last_act',
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
            ));
        $this->set("admins", $admins);
         * */
        $now_date = $this->DateTime->now_date();
        $date = $this->request->query('date') ?? $now_date;
        $route_id = $this->request->query('route_id') ?? null;
        $address_id = $this->request->query('address_id') ?? null;
        $order_id = $this->request->query('order_id') ?? null;
        if (!$this->Validator->valid_date($date)) {
            $date = $now_date;
        }
        //маршрутный лист в выбранную дату
        $filter = [
            'date' => $date,
            "route_id" => $route_id,
            "address_id" => $address_id,
            "order_id" => $order_id,
        ];
        $this->set('date', $date);
        $this->set('current_route_id', $route_id);

        $points = $this->Route->getRoutePoints($this->default_view_points, $filter);
        $this->set('points', $points);

        $this->set('route_com', $this->Route);
        $this->set('datetime', $this->DateTime);
        $this->set('address_com', $this->Address);
        $this->set('route_list', $this->Route->getCouriersByDate($date));

        $this->set('title', "Маршрутный лист");
    }

    // --- помойка ---

    public function edit_car()
    {
        $car_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));
                return $this->redirect($this->referer());
            }
            if ($car_id == null) {
                $this->Flash->set(__("Не задан идентификатор машины"));
                return $this->redirect($this->referer());
            }
            $car = $this->Car->getCarById($car_id);
            if ($car == null) {
                $this->Flash->set(__("Машина с таким id ($car_id) не найдена"));
                return $this->redirect($this->referer());
            }
            $car_create = $this->Car->saveCar($car_id, $data);
            if (!empty($car_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $car_create["error"]));
                return $this->redirect($this->referer());
            } else {
                $car_id = $car_create;
                $this->redirect("/car/edit/" . $car_id);
            }
        } else {
            if ($car_id == null) {
                $this->Flash->set(__("Не задан идентификатор машины"));
                return $this->redirect($this->referer());
            }
            $car = $this->Car->getCarById($car_id);
            if ($car == null) {
                $this->Flash->set(__("Машина с таким id ($id) не найдена"));
                return $this->redirect($this->referer());
            }

            $this->set('title', "Машина №$car_id");
            $this->set('car', $car);
        }
    }

    public function edit_car_type()
    {
        $car_type_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));
                return $this->redirect($this->referer());
            }
            if ($car_type_id == null) {
                $this->Flash->set(__("Не задан идентификатор типа машины"));
                return $this->redirect($this->referer());
            }
            $car_type = $this->Car->getCarTypeByTypeId($car_type_id);
            if ($car_type == null) {
                $this->Flash->set(__("Тип машины с таким id ($car_type_id) не найден"));
                return $this->redirect($this->referer());
            }
            $car_type_create = $this->Car->saveCarType($car_type_id, $data);
            if (!empty($car_type_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $car_type_create["error"]));
                return $this->redirect($this->referer());
            } else {
                $car_type_id = $car_type_create;
                $this->redirect("/car_type/edit/" . $car_type_id);
            }
        } else {
            if ($car_type_id == null) {
                $this->Flash->set(__("Не задан идентификатор типа машины"));
                return $this->redirect($this->referer());
            }
            $car_type = $this->Car->getCarTypeByTypeId($car_type_id);
            if ($car_type == null) {
                $this->Flash->set(__("Тип машины с таким id ($car_type_id) не найден"));
                return $this->redirect($this->referer());
            }

            $this->set('title', "Машина №$car_type_id");
            $this->set('car_type', $car_type);
        }
    }


    public function add_car_type()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));
                return $this->redirect($this->referer());
            }
            $car_create = $this->Car->createCarType($data);
            if (!empty($car_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $car_create["error"]));
                return $this->redirect($this->referer());
            } else {
                $car_type_id = $car_create;
                $this->Flash->set(__("Новый тип машины добавлен"));
                $this->redirect("/car_types/");
            }
        } else {
            $car_types = $this->Car->getAllTypes();
            $this->set('car_types', $car_types);
            $this->set('title', "Добавление нового типа машины");
        }
    }

    public function car_types()
    {
        $car_types = $this->Car->getAllTypes();
        $this->set('car_types', $car_types);
        $this->set('title', "Типы машин");
    }

    public function add_car()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));
                return $this->redirect($this->referer());
            }
            $car_create = $this->Car->createCar($data);
            if (!empty($car_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $car_create["error"]));
                return $this->redirect($this->referer());
            } else {
                $car_id = $car_create;
                $this->redirect("/car/edit/" . $car_id);
            }
        } else {
            $car_types = $this->Car->getAllTypes();
            $this->set('car_types', $car_types);
            $this->set('title', "Добавление новой машины");
        }
    }

    public function delete_car()
    {
        $car_id = $this->request->param('id') ?? null;
        if ($car_id == null) {
            $this->Flash->set(__("Некорректный идентификатор машины"));
            return $this->redirect($this->referer());
        }
        $car_delete = $this->Car->deleteCar($car_id);
        if (!empty($car_delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $car_delete["error"]));
            return $this->redirect($this->referer());
        } else {
            $this->redirect("/cars");
        }
        exit;
    }

    public function delete_car_type()
    {
        $car_type_id = $this->request->param('id') ?? null;
        if ($car_type_id == null) {
            $this->Flash->set(__("Некорректный идентификатор типа машины"));
            return $this->redirect($this->referer());
        }
        $cars = $this->Car->getCarTypeByTypeId($car_type_id);
        if ($cars != null) {
            foreach ($cars as $car) {
                $car_id = $car['Car']['id'];
                $this->Car->deleteCar($car_id);
            }
        }
        $car_type_delete = $this->Car->deleteCarType($car_type_id);
        if (!empty($car_type_delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $car_type_delete["error"]));
            return $this->redirect($this->referer());
        } else {
            $this->Flash->set(__("Тип машин успешно удален"));
            $this->redirect("/car_types");
        }
    }

    /**
     * @return CakeResponse|null
     */
    public function unblock_car()
    {

        $id = $this->request->param('id');
        if ($this->Car->getCarById($id) == null) {
            $this->Flash->set(__("Машина с таким id ($id) не найдена"));
            return $this->redirect($this->referer());
        }
        $this->Car->setCarActive($id);
        $this->Flash->set(__("Машина разблокирована и доступна для доставки"));
        return $this->redirect($this->referer());
    }

    /**
     * @param $id
     * @return CakeResponse|null
     */
    public function block_car()
    {
        $id = $this->request->param('id');
        if ($this->Car->getCarById($id) == null) {
            $this->Flash->set(__("Машина с таким id ($id) не найдена"));
            return $this->redirect($this->referer());
        }
        $this->Car->setCarBlocked($id);
        $this->Flash->set(__("Машина скрыта и недоступна для доставки"));
        return $this->redirect($this->referer());
    }


}