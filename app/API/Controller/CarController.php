<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class CarController extends AppController
{
    public $uses       = [
        'Car',
    ];

    public $components = [
        'Activity',
        'Account',
        'Action',
        'Admin',
        'Breadcrumbs',
        'Car',
        'City',
        'Content',
        'Flash',
        'Session',
    ];

    public $layout     = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(
            L('MAIN_PAGE'),
            Router::url(['plugin' => false, 'controller' => 'index', 'action' => 'index'])
        );
        $this->Access->checkControlAccess("clients");
        parent::beforeFilter();
    }

    public function users_cars()
    {
        $show_count = 30;
        $page       = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter     = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);
        $pages      = ceil($this->Car->totalUserCarCount() / $show_count);

        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";
        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter    = [];
        $user_cars = $this->Car->getUsersCars($show_count, $page, $sort, $sort_dir, $filter);
        $this->set("users_cars", $user_cars);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = [
            'filter' => $filter,
        ];

        $this->set('form_data', $form_data);
    }

    public function brands()
    {
        $show_count = 30;
        $page       = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter     = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);
        $pages      = ceil($this->Car->totalBrandCount() / $show_count);

        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";
        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        $brands = $this->Car->brandList($show_count, $limit_page, $sort, $sort_dir, $filter);
        $this->set("brands", $brands);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = [
            'filter' => $filter,
        ];

        $this->set('form_data', $form_data);
    }

    public function view_brand()
    {
        $id     = $this->request->param('id');
        $brand  = $this->Car->getBrandById($id);
        $models = $this->Car->getModelsByBrandId($id);
        $this->set("brand", $brand);
        $this->set("models", $models);
    }

    public function edit_brand()
    {
        $brand_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            if ($brand_id == null) {
                $this->Flash->set(__("Не задан идентификатор марки машин"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $brand = $this->Car->getBrandById($brand_id);
            if ($brand == null) {
                $this->Flash->set(__("Марка машин с таким id ($brand_id) не найдена"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $update            = $this->Car->updateBrand($brand_id, $data);
            $comment           = 'Изменение марки машины с id[#id] и названием [#title]';
            $activity['id']    = $brand_id;
            $activity['title'] = $data['title'];
            $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

            if ($update) {
                $this->Flash->set(__('Марка машин успешно сохранен'), ['element' => 'flash']);
                $this->redirect("/brand/edit/" . $brand_id);
            } else {
                $this->Flash->set(__("Ошибка при сохранении марки машины: "), ['element' => 'flash']);
            }
        } else {
            if ($brand_id == null) {
                $this->Flash->set(__("Не задан идентификатор марки машины "), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $brand = $this->Car->getBrandById($brand_id);
            if ($brand == null) {
                $this->Flash->set(__("Марка машины с таким id ($brand_id) не найдена"));

                return $this->redirect($this->referer());
            }
            $this->set('title', "Марка машины №$brand_id");
            $this->set('brand', $brand);
        }
    }

    public function add_brand()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }

            $create = $this->Car->createBrand($data);
            if (!empty($this->Car->getLastError()) > 0) {
                $this->Flash->set(__("Ошибка " . $this->Car->getLastError()), ['element' => 'flash']);

                return $this->redirect($this->referer());
            } else {
                $id                = $create;
                $comment           = 'Создание марки машины с id[#id] и названием [#title]';
                $activity['id']    = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Марка машины успешно добавлена"), ['element' => 'flash']);
                $this->redirect("/brand/view/" . $id);
            }
        } else {
            $this->set('title', "Создание марки машины");
        }
    }

    public function view_model()
    {
        $id = $this->request->param('id');
        if ($id == null) {
            $this->Flash->set(__("Ошибка " . ' Не найдена модель машины'), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $model = $this->Car->getModelById($id);
        if ($model == null) {
            $this->Flash->set(__("Ошибка " . ' Не найдена модель машины'), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }

        $brand_id    = $model[0]['Car_Model']['mark_id'];
        $brand       = $this->Car->getBrandById($brand_id);
        $model_count = $this->Car->getUserCarsByModel($id);
        $this->set("model_count", $model_count);
        $this->set("brand", $brand);
        $this->set("model", $model);
    }

    public function edit_model()
    {
        $model_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            if ($model_id == null) {
                $this->Flash->set(__("Не задан идентификатор модели машины"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $model = $this->Car->getModelById($model_id);
            if ($model == null) {
                $this->Flash->set(__("Модель машин с таким id ($model_id) не найдена"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $update            = $this->Car->updateModel($model_id, $data);
            $comment           = 'Изменение модели машины с id[#id] и названием [#title]';
            $activity['id']    = $model_id;
            $activity['title'] = $data['title'];
            $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

            if ($update) {
                $this->Flash->set(__('Модель машин успешно сохранена'), ['element' => 'flash']);
                $this->redirect("/model/edit/" . $model_id);
            } else {
                $this->Flash->set(__("Ошибка при сохранении модели машины: "), ['element' => 'flash']);
            }
        } else {
            if ($model_id == null) {
                $this->Flash->set(__("Не задан идентификатор модели машины "), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }
            $brand = $this->Car->getModelById($model_id);
            if ($brand == null) {
                $this->Flash->set(__("Модель машины с таким id ($model_id) не найдена"));

                return $this->redirect($this->referer());
            }
            $this->set('title', "Модель машины №$model_id");
            $this->set('brand', $brand);
        }
    }

    public function add_model()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);

                return $this->redirect($this->referer());
            }

            $create = $this->Car->createModel($data);
            if (!empty($this->Car->getLastError()) > 0) {
                $this->Flash->set(__("Ошибка " . $this->Car->getLastError()), ['element' => 'flash']);

                return $this->redirect($this->referer());
            } else {
                $id                = $create;
                $comment           = 'Создание модели машины с id[#id] и названием [#title]';
                $activity['id']    = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Модель машины успешно добавлена"), ['element' => 'flash']);
                $this->redirect("/model/view/" . $id);
            }
        } else {
            $this->set('title', "Создание марки машины");
        }
    }

    public function delete_brand()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор марки машины"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $delete = $this->Car->deleteBrand($id);
        if (!empty($this->Car->getLastError()) > 0) {
            $this->Flash->set(__("Ошибка " . $this->Car->getLastError()), ['element' => 'flash']);

            return $this->redirect($this->referer());
        } else {
            $comment        = 'Удаление марки машин с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

            $this->redirect("/brands");
        }
        exit;
    }

    public function delete_model()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор модели машины"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $delete = $this->Car->deleteModel($id);
        if (!empty($this->Car->getLastError()) > 0) {
            $this->Flash->set(__("Ошибка " . $this->Car->getLastError()), ['element' => 'flash']);

            return $this->redirect($this->referer());
        } else {

            $comment        = 'Удаление модели машины с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

            $this->redirect("/brands");
        }
        exit;
    }

    public function block_brand()
    {
        $id = $this->request->param('id');
        if ($this->Car->getBrandById($id) == null) {
            $this->Flash->set(__("Марка машин с таким id ($id) не найдена"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->blockCar($id);

        $comment        = 'Отключение из показа марки машины с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Марка машин скрыта из показа"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function unblock_brand()
    {
        $id = $this->request->param('id');
        if ($this->Car->getBrandById($id) == null) {
            $this->Flash->set(__("Новость с таким id ($id) не найдена"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->unblockCar($id);

        $comment        = 'Открытие для показа марки машины с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Марка машин открыта для показа на сайт"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function block_model()
    {
        $id = $this->request->param('id');
        if ($this->Car->getModelById($id) == null) {
            $this->Flash->set(__("Модель машин с таким id ($id) не найдена"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->blockModel($id);

        $comment        = 'Отключение из показа модели машин с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Модель машин скрыта из показа на сайт"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function unblock_model()
    {
        $id = $this->request->param('id');
        if ($this->Car->getModelById($id) == null) {
            $this->Flash->set(__("Модель машин с таким id ($id) не найдена"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->unblockModel($id);

        $comment        = 'Открытие для показа модели машин  с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Модель машин открыта для показа на сайт"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function set_popular_brand()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор марки машины"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->setBrandPopular($id);

        $comment        = 'Марка машины с id[#id] установлена как популярная';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Марка машины установлена как популярная"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function set_not_popular_brand()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор марки машины"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->setBrandNotPopular($id);

        $comment        = 'Марка машины с id[#id] установлена как обычная (не популярная)';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Марка машины установлена как обычная (не популярная)"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function set_popular_model()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор модели машины"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->setModelPopular($id);

        $comment        = 'Модель машины с id[#id] установлена как популярная';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Модель машины установлена как популярная"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

    public function set_not_popular_model()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор модели машины"), ['element' => 'flash']);

            return $this->redirect($this->referer());
        }
        $this->Car->setModelNotPopular($id);

        $comment        = 'Модель машины с id[#id] установлена как обычная (не популярная)';
        $activity['id'] = $id;
        $this->Activity->add("cars", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Модель машины установлена как обычная (не популярная)"), ['element' => 'flash']);

        return $this->redirect($this->referer());
    }

}