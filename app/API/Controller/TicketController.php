<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class TicketController extends AppController
{
    public $uses = array(
        'Company_Ticket',
        'Company_Ticket_Message',
        'Company_Ticket_Theme',

        'Orderer_Ticket',
        'Orderer_Ticket_Message',
        'Orderer_Ticket_Theme',

        'Image',
    );

    public $components = array(
        'Admin',
        'Session',
        'Breadcrumbs',
        'Flash',
        'Uploader',
        'Support'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function company_list(){
        $show_count = 20;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $ticket_count = $this->Company_Ticket->find("count",
            array('conditions' =>
                array(
                ),
            )
        );
        $pages = ceil($ticket_count / $show_count);

        $tickets = $this->Company_Ticket->find("all",
            array('conditions' =>
                array(
                    //'author_id' => $this->CompanyCom->company_agent_id(),
                ),
                'joins' => array(
                    array(
                        'table' => 'company_ticket_themes',
                        'alias' => 'Company_Ticket_Theme',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Company_Ticket_Theme.id = Company_Ticket.theme_id'
                        )
                    ),
                    array(
                        'table' => 'companies',
                        'alias' => 'Company',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Company.id = Company_Ticket.author_id'
                        )
                    )
                ),
                'fields' => array(
                    'Company_Ticket.*',
                    'Company_Ticket_Theme.*',
                    'Company.*'
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
            )
        );

        //messages
        foreach ($tickets as &$ticket){
            $t_id = $ticket['Company_Ticket']['id'];
            $ticket_message = $this->Company_Ticket_Message->find("first",
                array('conditions' =>
                    array(
                        'ticket_id' => $t_id,
                    ),
                    'order' => array('id DESC')
                )
            );
            $ticket['message'] = $ticket_message;
        }

        $this->set('tickets', $tickets);
        $this->set('page', $page);
        $this->set('pages', $pages);

        $this->set('ticket_count', $ticket_count);
        $this->set('title', "Техподдержка - тикеты");
    }

    /**
     * создание тикета
     */
    public function add_ticket(){
        $theme_list = $this->Support->getCompanyThemes();
        $this->set('theme_list', $theme_list);
    }

    public function view_orderer_ticket(){
        $ticket_id = $this->request->param('id') ?? null;
        $ticket = $this->Orderer_Ticket->find("first",
            array('conditions' =>
                array(
                    'Orderer_Ticket.id' => $ticket_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'orderer_ticket_themes',
                        'alias' => 'Orderer_Ticket_Theme',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Orderer_Ticket_Theme.id = Orderer_Ticket.theme_id'
                        )
                    )
                ),
                'fields' => array(
                    'Orderer_Ticket.*',
                    'Orderer_Ticket_Theme.*',
                ),
            )
        );

        $this->set('ticket', $ticket);

        $ticket_messages = $this->Orderer_Ticket_Message->find("all",
            array('conditions' =>
                array(
                    'ticket_id' => $ticket_id,
                ),
                'order' => array('id ASC')
            )
        );

        $this->set('ticket', $ticket);
        $this->set('ticket_messages', $ticket_messages);

        $this->set('title', "Техподдержка - тикеты");
    }

    public function view_company_ticket(){
        $ticket_id = $this->request->param('id') ?? null;
        $ticket = $this->Company_Ticket->find("first",
            array('conditions' =>
                array(
                    'Company_Ticket.id' => $ticket_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'company_ticket_themes',
                        'alias' => 'Company_Ticket_Theme',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company_Ticket_Theme.id = Company_Ticket.theme_id'
                        )
                    )
                ),
                'fields' => array(
                    'Company_Ticket.*',
                    'Company_Ticket_Theme.*',
                ),
            )
        );

        $this->set('ticket', $ticket);

        $ticket_messages = $this->Company_Ticket_Message->find("all",
            array('conditions' =>
                array(
                    'ticket_id' => $ticket_id,
                ),
                'order' => array('id ASC')
            )
        );

        $this->set('ticket', $ticket);
        $this->set('ticket_messages', $ticket_messages);

        $this->set('title', "Техподдержка - тикеты");

        $this->set('company_id', $ticket['Company_Ticket']['author_id']);
    }

    public function add_company_answer(){
        $errors = [];

        $message = $this->request->data('message') ?? null;
        $ticket_id = $this->request->param('id') ?? null;

        if(mb_strlen($message)<=2){
            $errors[] = "empty_message_content";
        }

        if(count($errors)>0){
            $this->set("errors", $errors);
        } else {

            $new_ticket_message = [
                'sender_id' => $this->Admin->manager_id(),
                'sender_type' => 'admin',
                'receiver_id' => 0,
                'text' => $message,
                'ticket_id' => $ticket_id,
            ];
            $this->Company_Ticket_Message->save($new_ticket_message);
            // обновить статус тикета
            $this->updateTicketStatus($ticket_id, "admin_answered");
            $this->redirect("/ticket/company/view/" . $ticket_id);
        }
    }

    public function add_orderer_answer(){
        $errors = [];

        $message = $this->request->data('message') ?? null;
        $ticket_id = $this->request->param('id') ?? null;

        if(mb_strlen($message)<=2){
            $errors[] = "empty_message_content";
        }

        if(count($errors)>0){
            $this->set("errors", $errors);
        } else {

            $new_ticket_message = [
                'sender_id' => $this->Admin->manager_id(),
                'sender_type' => 'user',
                'receiver_id' => 0,
                'text' => $message,
                'ticket_id' => $ticket_id,
            ];
            $this->Orderer_Ticket_Message->save($new_ticket_message);
            // обновить статус тикета
            $this->updateTicketStatus($ticket_id, "user_answered");
            $this->redirect("/ticket/orderer/view/" . $ticket_id);
        }
    }

    private function updateTicketStatus($ticket_id, $status){
        $data = [
            'ticket_status' => $status
        ];
        $this->Company_Ticket->id = $ticket_id;
        $this->Company_Ticket->save($data);
    }

    public function block_company()
    {
        $id = $this->request->param('id');
        if ($id <= 0) {
            die("Тикет не найден");
        }
        if ($this->change_status($id, 'admin_closed', "company")) {
            $this->Flash->set(__('Тикет закрыт администрацией'));
            return $this->redirect($this->referer());
        } else {
            die($this->Company_Ticket->errors());
        }
    }

    public function unblock_company()
    {
        $id = $this->request->param('id');
        if ($id <= 0) {
            die("Тикет не найден");
        }
        if ($this->change_status($id, 'admin_answered', "company")) {
            $this->Flash->set(__('Тикет закрыт администрацией'));
            return $this->redirect($this->referer());
        } else {
            die($this->Company_Ticket->errors());
        }
    }

    public function block_orderer()
    {
        $id = $this->request->param('id');
        if ($id <= 0) {
            die("Тикет не найден");
        }
        if ($this->change_status($id, 'user_closed', "orderer")) {
            $this->Flash->set(__('Тикет закрыт пользователем'));
            return $this->redirect($this->referer());
        } else {
            die($this->Orderer_Ticket->errors());
        }
    }

    public function unblock_orderer()
    {
        $id = $this->request->param('id');
        if ($id <= 0) {
            die("Тикет не найден");
        }
        if ($this->change_status($id, 'user_answered', "orderer")) {
            $this->Flash->set(__('Тикет открыт пользователем'));
            return $this->redirect($this->referer());
        } else {
            die($this->Orderer_Ticket->errors());
        }
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     */
    private function change_status($id, $status, $user_type)
    {
        if($user_type == "company"){
            $ticket_model = $this->Company_Ticket;
        } else {
            $ticket_model = $this->Orderer_Ticket;
        }
        $update_product = array('ticket_status' => $status);
        $ticket_model->id = $id;
        if ($ticket_model->save($update_product)) {
            return true;
        }
        return false;
    }

    //TODO
    /**
     *
     */
    public function editTicket(){

    }

    /**
     *
     */
    public function editTicketMessage(){

    }
}