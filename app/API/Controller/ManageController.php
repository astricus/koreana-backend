<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class ManageController extends AppController
{
    public $uses       = [
        'Api_Item',
        'Api_Request',
    ];

    public $components = [
        'Access',
        'Account',
        'Admin',
        'Api',
        'Breadcrumbs',
        'Http',
        'Flash',
        'Session',
    ];

    public $layout     = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(
            L('MAIN_PAGE'),
            Router::url(['plugin' => false, 'controller' => 'index', 'action' => 'index'])
        );
        parent::beforeFilter();
    }

    public function restapi()
    {
        $tokens = $this->Account->getTokens($this->Admin->manager_id());
        $this->set("tokens", $tokens);
        $this->render("../Restapi/index");
    }

    public function documentation()
    {
        $tokens = $this->Account->getTokens($this->Admin->manager_id());
        $this->set("tokens", $tokens);
        $this->render("../Restapi/documentation");
    }

    // TRASH

    public function test_api()
    {
        $this->render("../Hive/test_api");
        if ($this->request->is('post')) {
            $route       = $this->request->data("api_route");
            $api_request = $this->request->data("api_request");
            $api_token   = $this->request->data("api_token") ?? null;
            if ($this->isJson($api_request)) {
                $api_request_json          = json_decode($api_request);
                $api_request_json['token'] = $api_token;
                $api_request               = json_encode($api_request_json);
            } else {
                $api_request = $api_request . "&token=$api_token";
            }
            $api_test_handler = $this->Http->testApiHandler($route, [], $api_request);

            $data = ["data" => $api_test_handler];
            $this->Api->response_api($data, "success");
            exit;
        }
    }

    public function testApiRequest()
    {
        return "this is test API request. Request is valid. Response is success! Api is works fine. :)";
    }

    public function undefinedApiRequest()
    {
        return "this is undefined API request. please, use documentation and check your request uri and request data! :(";
    }

    public function user_id()
    {
        return $this->Session->read('manager_id');
    }

    public function generateToken()
    {
        if ($this->request->is('post')) {
            //            $data = $this->request->data ?? null;
            //            $project_id = $this->request->data['project_id'] ?? null;//
            //            if ($data == null) {
            //                $this->Flash->set(__("Нет данных"));
            //                return $this->redirect($this->referer());
            //            }
            $user_id = $this->user_id();
            $token   = $this->Account->createNewToken($user_id);
            $result  = ["token" => $token];
            response_ajax($result, "success");
        }
        exit;
    }

    public function sandbox()
    {
        /*
        //список машин
        $drivers = $this->Driver->getAllDrivers();
        $this->set('drivers', $drivers);*/
        $this->set('title', "Песочница методов API");
        $this->set("project_id", $this->project_id);
    }

    public function testApiSandbox()
    {
        /*
        //список машин
        $drivers = $this->Driver->getAllDrivers();
        $this->set('drivers', $drivers);*/
        $this->set('title', "Песочница методов API");
        $api_method    = $this->request->param('api_method') ?? null;
        $api_component = $this->request->param('api_component') ?? null;
        $this->set("action_url", "manage" . DS . "sandbox" . DS . $api_component . DS . $api_method . DS . "run");
        $this->set("method_list", $this->Api->getApiComponentActiveMethods($api_component));
        $this->set("api_component", $api_component);
        $this->set("current_method", $api_method);
        $this->render($api_component . DS . $api_method);
    }

    public function manage_type()
    {
        $api_type   = $this->params['api_type'];
        $project_id = $this->params['project_id'];
        if ($api_type == self::HTTP_POST) {
            $this->render("http_post");

            return $this->apiSimpleHttpPost();
        }
        $this->set("project_id", $this->project_id);
    }

    public function createApiItem($type, $data)
    {
        $api_type   = $this->request->data('api_type');
        $project_id = $this->request->data('project_id');
        $data       = $this->request->data('data');
        if ($api_type == self::HTTP_POST) {
            return $this->apiSimpleHttpPost();
        }
    }

    /**
     * @param $api_type
     * @param $api_id
     * @param $project_id
     * @param $data
     */
    public function runApiManual()
    {
        $api_component  = $this->request->data('api_component');
        $api_id         = $this->request->data('api_id');
        $project_id     = $this->request->data('project_id');
        $data           = $this->request->data('data');
        $access_control = $this->Api->isAvailableApiMethod($api_component, $api_id, $project_id);
        if ($access_control["status"]) {
            $result = $this->Api->runApiMethod($api_component, $api_id, $project_id, $data);
            response_ajax($result, "success");
        } else {
            $result = $access_control["error"];
            response_ajax($result, "error");
        }
        exit;
    }

    public function runApiSandbox()
    {

        $api_method    = $this->request->param('api_method') ?? null;
        $api_component = $this->request->param('api_component') ?? null;
        $data          = $this->request->data;
        if (count($data) == 0) {
            $data = $this->request->query;
        }
        $user_data = $data['data'];
        $params    = $data['params'];
        $result    = $this->Api->runApiMethodInSandbox($api_component, $api_method, $user_data, $params);
        response_ajax($result, "success");
        exit;
    }

    public function apiSimpleHttpPost()
    {
        if ($this->request->is('post')) {
            $data       = $this->request->data ?? null;
            $project_id = $this->request->data['project_id'] ?? null;//
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));

                return $this->redirect($this->referer());
            }
            $api_item_create = $this->Api->createDriver($data);
            if (!empty($api_item_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $api_item_create["error"]));

                return $this->redirect($this->referer());
            } else {
                $api_item_id = $api_item_create;
                $this->redirect("/sandbox/http_post/$project_id/" . $api_item_id);
            }
        } else {
            $this->set('title', "Добавление нового метода API");
        }
        $this->render("http_post");
    }

    // ---  помойка ---

    public function edit_driver()
    {
        $driver_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));

                return $this->redirect($this->referer());
            }
            if ($driver_id == null) {
                $this->Flash->set(__("Не задан идентификатор водителя"));

                return $this->redirect($this->referer());
            }
            $driver = $this->Driver->getDriverById($driver_id);
            if ($driver == null) {
                $this->Flash->set(__("Водитель с таким id ($driver_id) не найден"));

                return $this->redirect($this->referer());
            }
            $driver_create = $this->Driver->saveDriver($driver_id, $data);
            if (!empty($driver_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $driver_create["error"]));

                return $this->redirect($this->referer());
            } else {
                $driver_id = $driver_create;
                $this->redirect("/driver/edit/" . $driver_id);
            }
        } else {
            if ($driver_id == null) {
                $this->Flash->set(__("Не задан идентификатор машины"));

                return $this->redirect($this->referer());
            }
            $driver = $this->Driver->getDriverById($driver_id);
            if ($driver == null) {
                $this->Flash->set(__("Водитель с таким id ($id) не найден"));

                return $this->redirect($this->referer());
            }

            $this->set('title', "Водитель №$driver_id");
            $this->set('driver', $driver);
        }
    }

    public function add_driver()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"));

                return $this->redirect($this->referer());
            }
            $driver_create = $this->Driver->createDriver($data);
            if (!empty($driver_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $driver_create["error"]));

                return $this->redirect($this->referer());
            } else {
                $driver_id = $driver_create;
                $this->redirect("/driver/edit/" . $driver_id);
            }
        } else {
            $this->set('title', "Добавление нового водителя");
        }
    }

    public function delete_driver()
    {
        $driver_id = $this->request->param('id') ?? null;
        if ($driver_id == null) {
            $this->Flash->set(__("Некорректный идентификатор водителя"));

            return $this->redirect($this->referer());
        }
        $driver_delete = $this->Driver->deleteDriver($driver_id);
        if (!empty($driver_delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $driver_delete["error"]));

            return $this->redirect($this->referer());
        } else {
            $this->redirect("/drivers");
        }
        exit;
    }

    /**
     * @return CakeResponse|null
     */
    public function unblock_driver()
    {

        $id = $this->request->param('id');
        if ($this->Driver->getDriverById($id) == null) {
            $this->Flash->set(__("Водитель с таким id ($id) не найден"));

            return $this->redirect($this->referer());
        }
        $this->Driver->setDriverActive($id);
        $this->Flash->set(__("Водитель разблокирован"));

        return $this->redirect($this->referer());
    }

    /**
     * @param $id
     *
     * @return CakeResponse|null
     */
    public function block_driver()
    {
        $id = $this->request->param('id');
        if ($this->Driver->getDriverById($id) == null) {
            $this->Flash->set(__("Водитель с таким id ($id) не найдена"));

            return $this->redirect($this->referer());
        }
        $this->Driver->setDriverBlocked($id);
        $this->Flash->set(__("Водитель заблокирован"));

        return $this->redirect($this->referer());
    }

}