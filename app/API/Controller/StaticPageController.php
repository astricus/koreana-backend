<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class StaticPageController extends AppController
{
    public $uses = array(
        'Static_Page',
    );

    public $components = array(
        'Activity',
        'Account',
        'Admin',
        'Access',
        'Api',
        'Breadcrumbs',
        'Http',
        'Flash',
        'Session',
        'Platform',
        'Content',
        'Static',
        'City'
    );

    public $max_size_mb = 5;

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
        $this->Access->checkControlAccess("create_new_pages");
    }

    public function saveStaticImage($name, $file)
    {

    }

    public function static_uploader()
    {
        $image_name = "file";
        $file_max_size = $this->max_size_mb;
        if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
            if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                $error = ['message' => "Файл больше " . $file_max_size . " MB"];
                response_ajax($error, "error");
                exit;
            }
            $file_tmp = $_FILES[$image_name]['tmp_name'];
            list($width, $height) = getimagesize($file_tmp);
            $file_width = $width;
            $file_height = $height;
            //width & height
            $res = $this->Content->saveContentImage($_FILES[$image_name], $file_width, $file_height);
            if ($res['status'] == 'ok') {
                response_ajax(['message' => "Файл успешно сохранен", 'url' => $res['url']], "success");
                exit;
            } else {
                response_ajax(['message' => "Ошибка при сохранении файла: " . $res['msg']], "error");
                exit;
            }
        } else {
            response_ajax(['message' => "Файла нет"], "error");
        }

        exit;
    }

    public function page_list()
    {
        $show_count = 2;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $platform = $this->request->query['platform'] ?? null;
        $find_by_name = $this->request->query['find_by_name'] ?? null;
        $category_id = $this->request->query['category_id'] ?? null;
        $enabled = $this->request->query['enabled'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        $pages = ceil($this->Static->totalPageCount($platform, $category_id, $enabled) / $this->Static->default_show_count);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if ($platform != null) {
            $filter["platform"] = $platform;
        }
        if ($category_id != null) {
            $filter["category_id"] = $category_id;
        }
        if ($enabled != null) {
            $filter["enabled"] = $enabled;
        }
        if ($find_by_name != null) {
            $filter["find_by_name"] = $find_by_name;
        }

        if ($pages > 0) {
            $page_list = $this->Static->pageList($show_count, $limit_page, $sort, $sort_dir, $filter);
            if (count($page_list) > 0) {
                foreach ($page_list as &$page_item) {
                    $page_id = $page_item['Static_Page']['id'];
                    $page_item['author_name'] = $this->Static->getStaticPageAuthor($page_id);
                    $page_item['category'] = $this->Static->getCategoryById($page_item['Static_Page']['category_id']);
                }
            }
        } else {
            $page_list = null;
        }
        $this->set("static_pages", $page_list);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['platform'] = $platform;
        $form_data['category_id'] = $category_id;
        $form_data['find_by_name'] = $find_by_name;

        $this->set('platforms', $this->Platform->platforms());
        $this->set('form_data', $form_data);
        $this->set('categories', $this->Static->categoryList());

        $this->set('content_dir', Configure::read('CONTENT_UPLOAD_DIR_RELATIVE'));

    }

    public function view()
    {
        $id = $this->request->param('id');
        $static_page = $this->Static->getPageById($id);
        $author = $this->Static->getStaticPageAuthor($id);
        $static_page['author_name'] = $author;
        $static_page['days_later'] = days_later(time() - strtotime($static_page['Static_Page']['created']));
        $this->set("page_id", $id);
        $this->set("static_page", $static_page);
        $this->set("category", $this->Static->getCategoryById($static_page['Static_Page']['category_id']));

        $actions = $this->Static->getPageLog($id);

        foreach ($actions as &$action) {
            $author_id = $action['Static_Page_Log']['author_id'];
            $action['author_name'] = $this->Static->getAuthorByAuthorId($author_id);
        }

        $this->set("story", $actions);

    }

    public function edit_page()
    {
        $page_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($page_id == null) {
                $this->Flash->set(__("Не задан идентификатор страницы"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $news = $this->Static->getPageById($page_id);
            if ($news == null) {
                $this->Flash->set(__("Страница с таким id ($page_id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);
            $content = $data['content'];
            $category_id = $data['category_id'];
            $title = $data['title'];
            $url = $data['url'];
            $meta_keywords = $data['meta-keywords'];
            $meta_title = $data['meta-title'];
            $meta_description = $data['meta-description'];
            $meta_custom = $data['meta-custom'];
            $platforms = $data['platform'] ?? null;

            $news_update = $this->Static->updateStaticPage($page_id, $content, $category_id, $title, $url, $platforms, $meta_keywords, $meta_title, $meta_description, $meta_custom);

            $comment = 'Изменение страницы с id[#id] и названием [#title]';
            $activity['id'] = $page_id;
            $activity['title'] = $data['title'];
            $this->Activity->add("static_page", $this->Admin->manager_id(), $comment, $activity);

            if (!empty($new_create["error"]) > 0) {
                $this->Session->write("errors", $this->Static->errors);
                $this->Session->write("error_fields", $this->Static->error_fields);
                $this->Flash->set(__("Ошибка " . $news_update["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/static_page/edit/" . $page_id);
            }
        } else {
            if ($page_id == null) {
                $this->Flash->set(__("Не задан идентификатор страницы"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $new = $this->Static->getPageById($page_id);
            if ($new == null) {
                $this->Flash->set(__("Страница с таким id ($page_id) не найдена"));
                return $this->redirect($this->referer());
            }

            $this->set('errors', $this->Session->read("errors") ?? []);
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);

            $author = $this->Static->getStaticPageAuthor($page_id);
            $this->set('author', $author);

            $this->set('platforms', $this->Platform->platforms());

            $this->set('title', "Страница №$page_id");
            $this->set('st_page', $new['Static_Page']);
            $this->set('page_story', $this->Static->getPageLog($page_id));

            $this->set('categories', $this->Static->categoryList());
            $this->set('new_platforms', $this->Platform->platformsToArray($new['Static_Page']['platform']));
        }
    }

    public function add_page()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                response_ajax("Нет данных", "error");
                exit;
            }

            $this->Session->write("add_page_data", null);
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);

            $content = $data['content'];
            $category_id = $data['category_id'];
            $title = $data['title'];
            $url = $data['url'];
            $meta_keywords = $data['meta-keywords'];
            $meta_title = $data['meta-title'];
            $meta_description = $data['meta-description'];
            $meta_custom = $data['meta-custom'];
            $platforms = $data['platform'] ?? null;

            $news_create = $this->Static->createStaticPage($content, $category_id, $title, $url, $platforms, $meta_keywords, $meta_title, $meta_description, $meta_custom);

            if (!$news_create) {
                $this->Session->write("add_page_data", $data);
                $this->Session->write("errors", $this->Static->errors);
                $this->Session->write("error_fields", $this->Static->error_fields);
                $this->Flash->set(__("Возникли ошибки при создании страницы: "), ['element' => 'flash']);
                //response_ajax(["errors" => $this->Static->errors, "error_fields" => $this->Static->error_fields], "error");
                return $this->redirect($this->referer());
            } else {
                $this->Session->write("add_page_data", null);
                $this->Session->write("errors", null);
                $this->Session->write("error_fields", null);
                $id = $news_create;

                $comment = 'Создание статичной страницы с id[#id] и названием [#title]';
                $activity['id'] = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("static_page", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Страница успещно создана"), ['element' => 'flash']);
                return $this->redirect("/static_page/view/" . $news_create);

            }
        } else {
            $reset = $this->request->query('reset');
            if ($reset == "reset") {
                $this->Session->write("add_page_data", null);
                return $this->redirect($this->referer());
            }
            $this->set('add_page_data', $this->Session->read("add_page_data"));
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);
            $this->set("errors", $this->Session->read('errors') ?? []);
            $this->set('title', "Добавление страницы");
            $this->set('platforms', $this->Platform->platforms());
            $this->set('categories', $this->Static->categoryList());
        }
    }

    public function delete_page()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор страницы"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Static->deletePage($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {

            $comment = 'Удаление страницы с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("static_page", $this->Admin->manager_id(), $comment, $activity);

            $this->redirect("/static_pages");
        }
        exit;
    }

    public function block_page()
    {
        $id = $this->request->param('id');
        if ($this->Static->getPageById($id) == null) {
            $this->Flash->set(__("Страница с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Static->blockPage($id);

        $comment = 'Отключение из показа страницы с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("static_page", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Страница скрыта из показа на сайте"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public function unblock_page()
    {
        $id = $this->request->param('id');
        if ($this->Static->getPageById($id) == null) {
            $this->Flash->set(__("Страница с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Static->unblockPage($id);

        $comment = 'Открытие для показа страницы с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("static_page", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Страница открыта для показа на сайте"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    //------------------ category

    public function add_category()
    {
        $id = $this->request->param('id') ?? null;
        $name = $this->request->query('name') ?? null;
        if ($name == null) {
            $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        if ($id == null) {
            $this->Flash->set(__("Не задан идентификатор категории"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }

        $category = $this->Static->getCategoryById($id);
        if ($category == null) {
            $this->Flash->set(__("Категория с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }

        $new_id = $this->Static->createCategory($name, $id);

        if (!empty($this->Static->errors) > 0) {
            $this->Flash->set(__("Ошибка " . $this->Static->errors), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {
            $comment = 'Создание новости с id[#id] и названием [#name]';
            $activity['id'] = $new_id;
            $activity['name'] = $name;
            $this->Activity->add("create_caregory", $this->Admin->manager_id(), $comment, $activity);
            $this->redirect("/static_page/category/view/" . $new_id);
        }
    }

    public function rename_category()
    {
        $id = $this->request->param('id') ?? null;
        $name = $this->request->query('name') ?? null;

        if ($name == null) {
            $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        if ($id == null) {
            $this->Flash->set(__("Не задан идентификатор категории"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }

        $category = $this->Static->getCategoryById($id);
        if ($category == null) {
            $this->Flash->set(__("Категория с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $result = $this->Static->renameCategory($id, $name);

        $comment = 'Переименование статичной категории с id[#id] и в категорию [#name]';
        $activity['id'] = $id;
        $activity['name'] = $name;
        $this->Activity->add("static_category", $this->Admin->manager_id(), $comment, $activity);

        if (!$result) {
            $this->Flash->set(__("Ошибка переименования категории!"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {
            $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
            return $this->redirect("/static_page/category/view/" . $id);
        }

    }

    public function remove_category()
    {
        $id = $this->request->param('id') ?? null;
        $parent_id = $this->request->query('parent_id') ?? null;

        if ($id == null) {
            $this->Flash->set(__("Не задан идентификатор категории"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        if ($id == $parent_id) {
            $this->Flash->set(__("Категория не может быть перенесена в себя"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $category = $this->Static->getCategoryById($id);
        if ($category == null) {
            $this->Flash->set(__("Категория с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $result = $this->Static->removeCategory($id, $parent_id);

        $comment = 'Перенос статичной категории с id[#id] и в категорию [#parent_id]';
        $activity['id'] = $id;
        $activity['parent_id'] = $parent_id;
        $this->Activity->add("static_category", $this->Admin->manager_id(), $comment, $activity);

        if (!$result) {
            $this->Flash->set(__("Ошибка переноса категории!"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {
            $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
            return $this->redirect("/static_page/category/view/" . $id);
        }
    }

    public function delete_category()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор новости"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->New->deleteNew($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {

            $comment = 'Удаление новости с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("news", $this->Admin->manager_id(), $comment, $activity);

            $this->redirect("/news");
        }
        exit;
    }

    public function category_list()
    {
        $cat_tree = $this->Static->categoryTree(0);
        $this->set("categories", $cat_tree);
    }

    public function category_view()
    {
        $id = $this->request->param('id') ?? null;
        $category = $this->Static->getCategoryById($id);
        if ($category == null) {
            die("Категория статичных страниц не найдена! Вернитесь обратно.");
        }
        $sub_cats = $this->Static->getSubCategories($id);
        $category_pages = $this->Static->getPagesByCategory($id);
        $this->set('category_pages', $category_pages);
        $this->set('categories', $this->Static->categoryList());
        $this->set('category', $category);

        $cat_tree = $this->Static->categoryTree($id);
        $this->set("sub_categories", $cat_tree);
    }

    public function pages_by_api()
    {
        $id = $this->request->param('id');
        $static_page = $this->Static->getPageById($id);
        $author = $this->Static->getStaticPageAuthor($id);
        $static_page['author_name'] = $author;
        $static_page['days_later'] = days_later(time() - strtotime($static_page['Static_Page']['created']));
        $this->set("page_id", $id);
        $this->set("static_page", $static_page);
        $this->set("category", $this->Static->getCategoryById($static_page['Static_Page']['category_id']));

        $actions = $this->Static->getPageLog($id);

        foreach ($actions as &$action) {
            $author_id = $action['Static_Page_Log']['author_id'];
            $action['author_name'] = $this->Static->getAuthorByAuthorId($author_id);
        }

        $this->set("story", $actions);

    }


}