<?php

App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class IndexController extends AppController
{
    private $request1c_timeout = 40;

    public $uses = [
        "Manager",
    ];

    public $components = [
        'Activity',
        'Admin',
        'Api',
        'Car',
        'CarApi',
        'Clientapi',
        'Breadcrumbs',
        'EmailCom',
        'Flash',
        'Log',
        'Push',
        'Sms',
        'Session',
        'SiteData',
        'Sto',
        'User',
    ];

    public $layout = "default";

    private $use_memcached;

    private $memcache_host;

    private $memcache_port;

    private $memcache_time_seconds;

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(
            "Главная",
            Router::url(['plugin' => false, 'controller' => 'index', 'action' => 'index'])
        );
        $auth_error = (isset($this->request->query['auth_error'])) ? $this->request->query['auth_error'] : null;
        $auth_error_text = (isset($this->request->query['auth_error_text'])) ? $this->request->query['auth_error_text'] : null;
        $this->set('auth_error', $auth_error);
        $this->set('auth_error_text', $auth_error_text);
        $show_login_form = true;
        $this->set('show_login_form', $show_login_form);
        $this->set('show_login_form', $show_login_form);

        if (!function_exists("memcache_connect")) {
            //die("not found");
            $this->use_memcached = false;
        } else {
            $this->use_memcached = true;
            $this->memcache_host = 'localhost';
            $this->memcache_port = 11211;
            $this->memcache_time_seconds = 3600;
        }

        parent::beforeFilter();
    }

    public function index()
    {
    }

    public function test_email()
    {
        $this->EmailCom->sendTestKoreana();
    }

    public function delete_mobile()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор api req"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Api->deleteApiRequest($id);
        if (!$delete) {
            $this->Flash->set(__("Ошибка удаления api запроса"), ['element' => 'flash']);
        } else {
            $comment = 'Удаление запроса API с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("api", $this->Admin->manager_id(), $comment, $activity);
            $this->Flash->set(__("Запрос успешно удален"), ['element' => 'flash']);
        }
        return $this->redirect($this->referer());
    }

    public function mobile_log()
    {
        $show_count = 20;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        //$platform = $this->request->query['platform'] ?? null;
        //$city_id = $this->request->query['city_id'] ?? null;
        $start_datetime = $this->request->query['start_datetime'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->Api->totalAPICount() / $show_count);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
//            if($platform!=null){
//                $filter["platform"] = $platform;
//            }
//            if($city_id!=null){
//                $filter["city_id"] = $city_id;
//            }
        if ($start_datetime != null) {
            $filter["start_datetime"] = $start_datetime;
        }

        $req_list = $this->Api->getAPIRequests($show_count, $page, $sort, $sort_dir, $filter);
        if (count($req_list) > 0) {
//                foreach ($news_list as &$news_item) {
//                    $news_author_id = $news_item['News']['author_id'];
//                    $news_id = $news_item['News']['id'];
//                    $author = $this->New->getAuthorByAuthorId($news_author_id);
//                    $news_item['author_name'] = $author;
//                    $news_item['cities'] = $this->City->getCityByNews($news_id);
//                }
        }
        $this->set("requests", $req_list);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
//            $form_data['platform'] = $platform;
//            $form_data['city_id'] = $city_id;
        $form_data['start_datetime'] = $start_datetime;
        $this->set('form_data', $form_data);
    }

    public function mobile_view()
    {
        $id = $this->request->param('id');
        $mobile_request = $this->Api->getAPIRequest($id);
        $this->set("mobile_request", $mobile_request);
    }

    public function test_1c_model()
    {
        $this->Log->save1CRequestLog('123', '234', '123', '123', '1313');
    }

    public function test_post()
    {
        $api_data_k = $this->request->data('key');
        $api_data_v = $this->request->data('value');
        $token = $this->request->data('token');
        $api_component = $this->request->data('component') ?? null;

        $api_method = $this->request->data('method') ?? null;
        $api_data = [];

        if ($api_component !== null) {
            $url = $_SERVER['HTTP_ORIGIN'] . "/api/1/mobile/$api_component/$api_method/?token=$token";//b4831f21df6202f5bacade4b7bbc3e5c
            foreach ($api_data_v as $k => $value) {
                if (!is_numeric($api_data_k[$k])) {
                    $api_data[$api_data_k[$k]] = $value;
                }
            }

            $options = [
                'http' => [
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method' => 'POST',
                    'content' => http_build_query($api_data),
                ],
            ];
            $context = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === false) { /* Handle error */
            }
            var_dump($result);
        }

        $this->set("api_data", $api_data);
        $this->set("api_method", $api_method);
        $this->set("api_component", $api_component);
        $this->set("token", $token);
    }

    public function recreate_clients(){
        $this->Clientapi->apiRecreateClients();
    }

    public function update_car_base(){
        //метод обновления справочника моделей авто
        $this->CarApi->updateBase();
    }

    public function statistic()
    {
        $date_from = $this->request->query['date_from'] ?? '2022-02-01';
        $date_to = $this->request->query['date_to'] ?? date("Y-m-d");

        $this->set("date_from", $date_from);
        $this->set("date_to", $date_to);

        $this->set("users_by_date", $this->User->new_users_by_date($date_from, $date_to));
        $this->set("cars_by_date", $this->Car->new_cars_by_date($date_from, $date_to));

        $this->set('title', "Сводная статистика");
        $this->set('content_title', "Сводная статистика");

        $this->set("total_users", $this->total_users());
        $this->set("today_new_users", $this->today_new_users());

        $this->set("total_cars", $this->total_cars());
        $this->set("today_new_cars", $this->today_new_cars());

        $this->set("total_api_reqs", $this->total_api_reqs());
        $this->set("today_new_api_reqs", $this->today_new_api_reqs());

        $this->set("total_sto_records", $this->total_sto_records());
        $this->set("today_new_sto_records", $this->today_new_sto_records());

        $this->set("today_new_push", $this->today_new_push());
        $this->set("total_push", $this->total_push());

        $this->set("today_ip_blocked", $this->today_ip_blocked());
        $this->set("total_ip_blocked", $this->total_ip_blocked());

        $modelName = "Setting";
        $this->Setting = ClassRegistry::init($modelName);
        $settings = $this->Setting->find(
            "all",
            [
                'conditions' => []

            ]
        );
        $setting_arr = [];
        if (count($settings) > 0) {
            foreach ($settings as $setting) {
                $setting_arr[$setting['Setting']['name']] = $setting['Setting']['value'];
            }
        }
        $this->set("settings", $setting_arr);
    }

    public
    function passreminder()
    {
        $this->set('title', "Восстановление пароля");
        if ($this->request->is('post')) {
            //почта
            $mail = $this->request->data('email');
            //пароль
            $password = $this->request->data('password');
            $hashed_pass = get_hash(Configure::read('USER_AUTH_SALT'), $password);

            if (!empty($mail)) {

                $check_user = $this->Manager->find(
                    'first',
                    [
                        'conditions' =>
                            [
                                'password' => $hashed_pass,
                                'email' => $mail,
                            ],
                    ]
                );
                if (count($check_user) > 0) {
                    //удачная авторизация
                    $this->Session->write('Manager', $mail);

                    $user_id_data = $this->Manager->find('first', ['conditions' => ['email' => $mail]]);

                    $manager_id = $user_id_data['Manager']['id'];

                    /*
                    $this->loadModel('Userauth');
                    $auth_data = array('user_id' => $user_id, 'ip' => get_ip(), 'browser' => get_ua(), 'os' => get_os());
                    $this->Userauth->save($auth_data);
                    */

                    $this->Session->write('manager_id', $manager_id);
                    $this->redirect(['controller' => 'index', 'action' => 'index']);
                } else {
                    $auth_error_text = L("WRONG_LOGIN_OR_PASSWORD");
                    $this->set('auth_error', 'true');
                    $this->set('auth_error_text', $auth_error_text);
                    $this->redirect(
                        [
                            'controller' => 'index',
                            'action' => 'login',
                            '?' => ['auth_error' => 'true', 'auth_error_text' => $auth_error_text],
                        ]
                    );
                }
            } else {
                $auth_error_text = L("WRONG_LOGIN_OR_PASSWORD");
                $this->set('auth_error', 'true');
                $this->set('auth_error_text', $auth_error_text);
                $this->redirect(
                    [
                        'controller' => 'index',
                        'action' => 'login',
                        '?' => ['auth_error' => 'true', 'auth_error_text' => $auth_error_text],
                    ]
                );
                exit;
            }
            exit;
        }
        $this->layout = "admin_login";
    }

    public
    function login()
    {
        $this->set('title', "Авторизация в административную панель");
        if ($this->request->is('post')) {
            //почта
            $mail = $this->request->data('email');
            //пароль
            $password = $this->request->data('password');
            $hashed_pass = get_hash(Configure::read('USER_AUTH_SALT'), $password);
            if (!empty($mail)) {

                $check_user = $this->Manager->find(
                    'first',
                    [
                        'conditions' =>
                            [
                                'password' => $hashed_pass,
                                'email' => $mail,
                            ],
                    ]
                );
                if (count($check_user) > 0) {
                    //удачная авторизация
                    $this->Session->write('Manager', $mail);

                    $user_id_data = $this->Manager->find('first', ['conditions' => ['email' => $mail]]);

                    $manager_id = $user_id_data['Manager']['id'];

                    /*
                    $this->loadModel('Userauth');
                    $auth_data = array('user_id' => $user_id, 'ip' => get_ip(), 'browser' => get_ua(), 'os' => get_os());
                    $this->Userauth->save($auth_data);
                    */

                    $this->Session->write('manager_id', $manager_id);

                    $comment = 'Авторизация в системе, данные: ip адрес: [#ip],  браузер: [#browser],  ОС: [#os]';
                    //$data['name'] = $user_id_data['Manager']['firstname'] . " " .  $user_id_data['Manager']['lastname'];
                    $data['ip'] = get_ip();
                    $data['browser'] = get_ua();
                    $data['os'] = get_os();
                    $this->Activity->add("auth", $this->Admin->manager_id(), $comment, $data);

                    $this->redirect(['controller' => 'index', 'action' => 'index']);
                } else {
                    $auth_error_text = L("WRONG_LOGIN_OR_PASSWORD");
                    $this->set('auth_error', 'true');
                    $this->set('auth_error_text', $auth_error_text);
                    $this->redirect(
                        [
                            'controller' => 'index',
                            'action' => 'login',
                            '?' => ['auth_error' => 'true', 'auth_error_text' => $auth_error_text],
                        ]
                    );
                }
            } else {
                $auth_error_text = L("WRONG_LOGIN_OR_PASSWORD");
                $this->set('auth_error', 'true');
                $this->set('auth_error_text', $auth_error_text);
                $this->redirect(
                    [
                        'controller' => 'index',
                        'action' => 'login',
                        '?' => ['auth_error' => 'true', 'auth_error_text' => $auth_error_text],
                    ]
                );
                exit;
            }
            exit;
        }
        $this->set('title', "административная панель - начало работы");
        $this->layout = "admin_login";
    }

    public
    function logout()
    {
        $this->Session->write('Manager', null);
        $this->Session->write('manager_id', null);
        $this->redirect(['controller' => 'index', 'action' => 'index']);
    }


    public
    function settings()
    {
        $this->set('title', "административная панель - настройки");
        $this->set('content_title', "Настройки");
    }

    public
    function access_token()
    {
        $this->set('title', "access token");
    }

    public function today_new_users()
    {
        return $this->User->today_new_users();
    }

    public function total_users()
    {
        return $this->User->total_users();
    }

    public function today_new_cars()
    {
        return $this->Car->today_new_cars();
    }

    public function total_cars()
    {
        return $this->Car->total_cars();
    }

    public function today_new_api_reqs()
    {
        return $this->Api->today_new_api_reqs();
    }

    public function total_api_reqs()
    {
        return $this->Api->total_api_reqs();
    }

    public function today_new_sto_records()
    {
        return $this->Sto->today_new_sto_records();
    }

    public function total_sto_records()
    {
        return $this->Sto->total_sto_records();
    }

    public function today_new_push()
    {
        return $this->Push->today_new_push_stat();
    }

    public function total_push()
    {
        return $this->Push->total_push_stat();
    }

    public function today_ip_blocked()
    {
        return $this->Api->today_ip_blocked();
    }

    public function total_ip_blocked()
    {
        return $this->Api->total_ip_blocked();
    }
}