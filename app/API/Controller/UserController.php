<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');
App::import('Lib', 'CsvExport');

class UserController extends AppController
{
    public $components = array(
        'Activity',
        'Session',
        'Breadcrumbs',
        'Flash',
        'User',
        'Car'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function profile()
    {
        $id = $this->request->param('id');
        $user = $this->User->getUserById($id);
        $auths = $this->User->user_auths($id);
        $cars = $this->Car->getUserCars($id);
        $this->set("user", $user);
        $this->set("auths", $auths);
        $this->set("cars", $cars);
    }

    public function user_list()
    {
        $user_id = $this->request->query('id') ?? null;
        $export_csv = isset($this->request->query['export_csv']) ? true : false;
        $show_count = $export_csv ? 9999 : 20;

        $page = isset($this->request->query['page']) ? $this->request->query['page'] : 0;
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : [];
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $search = $this->request->query['search'] ?? null;
        $city_id = $this->request->query['city_id'] ?? null;
        $created = $this->request->query['created'] ?? null;

        $filter = [];
        if ($created != null) {
            $filter["created"] = $created;
        }
        if ($city_id != null) {
            $filter["city_id"] = $city_id;
        }

        $users_count = $this->User->userTotalCount($filter, $search);
        $pages = ceil($users_count / $show_count);
        $users = $this->User->userList($show_count, $page, $sort, $sort_dir, $filter, $search);

        if ($export_csv) {
            $this->layout = null;
            $this->autoLayout = false;
            $Csv_file = new CsvExport;

            if (count($users) == 0) {
                $Csv_file->addRow(["пользователей нет"]);
            } else {
                $line_names = ['№', 'ФИО', 'Телефон', 'Город', 'Регистрация', 'Последняя активность'];
                $Csv_file->addRow($line_names);
                $rn = 1;
                foreach ($users as $user) {
                    $User_data = $user['User'];
                    $city = $user['City'];
                    $line_row = [$rn,
                        prepare_fio($User_data['lastname'], $User_data['firstname'], $User_data['middlename']),
                        $User_data['phone'],
                        $city['name'],
                        lang_calendar($User_data['created']),
                        lang_calendar($User_data['last_auth']),
                    ];
                    $Csv_file->addRow($line_row);
                    $rn++;
                }
            }
            $filename = 'Пользователи';
            $file_comment = date("Y-m-d H:i:s");
            $Csv_file->setFilename($filename . " " . $file_comment);

            $Csv_file->renderHeaders();
            echo $Csv_file->render();
            exit;
        } else {
            $form_data = array(
                'user_id' => $user_id,
                'city_id' => $filter['city_id'] ?? null,
                'created' => $filter['created'] ?? null,
                'search' => $search ?? null,
            );
            $this->set("users", $users);
            $this->set("page", $page);
            $this->set("pages", $pages);
            $this->set('form_data', $form_data);
        }
    }

    //user codes
    public function user_codes()
    {
        $user_codes = $this->User->getUserCodes();
        $this->set("user_codes", $user_codes);

        $this->set('title', "Добавление привязки номера к коду");
        $this->set('user_phones', $this->User->getUserPhones());
    }

    public function create_user_code()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }

            $create = $this->User->createUserCode($data['phone'], $data['code']);

            if (!empty($create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $create["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $id = $create;
                $comment = 'Создание привязки номера[#phone] к коду [#code]';
                $activity['phone'] = $data['phone'];
                $activity['code'] = $data['code'];
                $this->Activity->add("user_code", $this->Admin->manager_id(), $comment, $activity);
                $this->redirect("/user_codes");
            }
        }
    }

    public function delete_user_code()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор привязки"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $connect = $this->User->getUserCode($id);
        $phone = $connect['User_Code']['phone'];
        $code = $connect['User_Code']['code'];
        $delete = $this->User->deleteUserCode($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            $this->redirect("/user_codes");
        } else {
            $comment = 'Удаление привязки с кодом [#code] с номера телефона [#phone]';
            $activity['phone'] = $phone;
            $activity['code'] = $code;
            $this->Activity->add("user_code", $this->Admin->manager_id(), $comment, $activity);
            $this->redirect("/user_codes");
        }
        exit;
    }

}