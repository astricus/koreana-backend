<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class DataProviderController extends AppController
{
    public $uses = array(
        'Webform',
    );

    public $components = array(
        'Access',
        'Activity',
        'Breadcrumbs',
        'DataProvider',
        'Flash',
        'Session',
        'Static',
        'Webform'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
        $this->Access->checkControlAccess("create_new_pages");
    }

    public function providers()
    {
        $show_count = 20;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $find_by_name = $this->request->query['find_by_name'] ?? null;
        $enabled = $this->request->query['enabled'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->DataProvider->totalDataProviderCount($find_by_name, $enabled) / $show_count);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if ($enabled != null) {
            $filter["enabled"] = $enabled;
        }
        if ($find_by_name != null) {
            $filter["find_by_name"] = $find_by_name;
        }

        if ($pages > 0) {
            $data_providers = $this->DataProvider->pageList($show_count, $limit_page, $sort, $sort_dir, $filter);

            if (count($data_providers) > 0) {
                foreach ($data_providers as &$data_provider) {
                    $p_id = $data_provider['Data_Provider']['id'];
                    $data_provider['count'] = $this->DataProvider->getProviderListElementCount($p_id);
                    $data_provider['form_used'] = $this->DataProvider->getDataProviderFormUsed($p_id);
                }
            }
        } else {
            $data_providers = null;
        }
        $this->set("providers", $data_providers);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['find_by_name'] = $find_by_name;
        $this->set('form_data', $form_data);

    }

    public function view_provider()
    {
        $id = $this->request->param('id');
        $data_provider = $this->Static->getDataProviderById($id);
        $data_provider['days_later'] = days_later(time() - strtotime($data_provider['Data_Provider']['created']));
        $this->set("data_provider_id", $id);
        $this->set("data_provider", $data_provider);
    }

    public function edit_provider()
    {
        $id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {

            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор источника данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $data_provider = $this->DataProvider->getDataProviderById($id);
            if ($data_provider == null) {
                $this->Flash->set(__("Источник данных с таким id ($id) не найден"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);
            $name = $data['name'];

            $news_update = $this->DataProvider->updateDataProvider($id, $name);

            $comment = 'Изменение источник данных с id[#id] и названием [#name]';
            $activity['id'] = $id;
            $activity['name'] = $data['name'];
            $this->Activity->add("data_provider", $this->Admin->manager_id(), $comment, $activity);
            if (!empty($new_create["error"]) > 0) {
                $this->Session->write("errors", $this->DataProvider->errors);
                $this->Session->write("error_fields", $this->DataProvider->error_fields);
                $this->Flash->set(__("Ошибка " . $news_update["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                if (count($data['provider']) > 0) {
                    $elem_count = count($data['provider']['name']);

                    for ($x = 0; $x < $elem_count; $x++) {
                        $item_id = $data['provider']['id'][$x] ?? null;
                        $item_name = trim($data['provider']['name'][$x]);
                        if ($item_id != null) {
                            if($item_name=="--"){
                                $this->DataProvider->removeDataItem($item_id);
                            } else {
                                $this->DataProvider->updateDataItem($item_id, $item_name);
                            }
                        } else {
                            $this->DataProvider->addDataItem($id, $item_name);
                        }
                    }
                }

                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/data_provider/edit/" . $id);
            }
        } else {
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор источника данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $data_provider = $this->DataProvider->getDataProviderById($id);
            if ($data_provider == null) {
                $this->Flash->set(__("источник данных с таким id ($id) не найдена"));
                return $this->redirect($this->referer());
            }

            $this->set('errors', $this->Session->read("errors") ?? []);
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);

            $this->set('title', "источник данных №$id");
            $this->set('data_provider', $data_provider['Data_Provider']);
            $this->set('provider_list', $this->DataProvider->getProviderList($id));


        }
    }

    public function add_provider()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                response_ajax("Нет данных", "error");
                exit;
            }

            $this->Session->write("add_form_data", null);
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);

            $name = $data['name'];
            $id = $this->DataProvider->createDataProvider($name);

            if (!$id) {
                $this->Session->write("add_form_data", $data);
                $this->Session->write("errors", $this->Static->errors);
                $this->Session->write("error_fields", $this->Static->error_fields);
                $this->Flash->set(__("Возникли ошибки при создании веб-формы: "), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Session->write("add_form_data", null);
                $this->Session->write("errors", null);
                $this->Session->write("error_fields", null);

                if (count($data['provider']) > 0) {
                    $elem_count = count($data['provider']['name']);

                    for ($x = 0; $x < $elem_count; $x++) {
                        $item_name = trim($data['provider']['name'][$x]);
                        $this->DataProvider->addDataItem($id,
                            $item_name
                        );
                    }
                } else if(!empty($data['elements'])){
                    //TODO functional
                }

                $comment = 'Создание источника данных с id[#id] и названием [#name]';
                $activity['id'] = $id;
                $activity['name'] = $data['name'];
                $this->Activity->add("data_provider", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Источник данных успешно создан"), ['element' => 'flash']);
                return $this->redirect("/data_provider/edit/" . $id);
            }
        } else {
            $reset = $this->request->query('reset');
            if ($reset == "reset") {
                $this->Session->write("add_webform_data", null);
                return $this->redirect($this->referer());
            }
            $this->set('add_form_data', $this->Session->read("add_form_data"));
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);
            $this->set("errors", $this->Session->read('errors') ?? []);
            $this->set('name', "Добавление источника данных");
        }
    }

    public function add_item()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                response_ajax("Нет данных", "error");
                exit;
            }

            $this->Session->write("add_form_data", null);
            $this->Session->write("errors", null);
            $this->Session->write("error_fields", null);

            $content = $data['content'];
            $name = $data['name'];
            $id = $this->Static->addDataListItem($content, $name);

            if (!$id) {
                $this->Session->write("add_form_data", $data);
                $this->Session->write("errors", $this->Static->errors);
                $this->Session->write("error_fields", $this->Static->error_fields);
                $this->Flash->set(__("Возникли ошибки при создании веб-формы: "), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Session->write("add_form_data", null);
                $this->Session->write("errors", null);
                $this->Session->write("error_fields", null);

                $comment = 'Создание источника данных с id[#id] и названием [#name]';
                $activity['id'] = $id;
                $activity['name'] = $data['name'];
                $this->Activity->add("data_provider", $this->Admin->manager_id(), $comment, $activity);
                $this->Flash->set(__("Источник данных успешно создан"), ['element' => 'flash']);
                return $this->redirect("/data_provider/view/" . $id);
            }
        } else {
            $reset = $this->request->query('reset');
            if ($reset == "reset") {
                $this->Session->write("add_webform_data", null);
                return $this->redirect($this->referer());
            }
            $this->set('add_form_data', $this->Session->read("add_form_data"));
            $this->set("error_fields", $this->Session->read('error_fields') ?? []);
            $this->set("errors", $this->Session->read('errors') ?? []);
            $this->set('name', "Добавление источника данных");
        }
    }

    public function block()
    {
        $id = $this->request->param('id');
        if (!$this->DataProvider->checkDataProviderExists($id)) {
            $this->Flash->set(__("Источник данных с таким id ($id) не найден"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->DataProvider->block($id);

        $comment = 'Отключение источника данных для веб-форм с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Источник данных отключен для веб-форм"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public function unblock()
    {
        $id = $this->request->param('id');
        if (!$this->DataProvider->checkDataProviderExists($id)) {
            $this->Flash->set(__("Источник данных с таким id ($id) не найден"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->DataProvider->unblock($id);

        $comment = 'Открытие источника данных для веб-форм с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("webforms", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Источник данных открыт для веб-форм"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

}