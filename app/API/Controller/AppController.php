<?php
//локализация
App::uses('L10n', 'L10n');
App::uses('Controller', 'Controller');

//контроллер приложения

class AppController extends Controller
{
    public $uses       = [
        'Manager',
    ];

    public $manager_data;

    public $title;

    public $components = [
        'Access',
        'Cookie',
        'Session',
        'Admin',
        'DebugKit.Toolbar',
        'Support',
    ];

    public function is_auth()
    {
        if ($this->Session->check('manager_id')) {
            return true;
        } else {
            return false;
        }
    }

    public function access_forbidden()
    {
        $functional = $this->request->query('functional');
        $role       = $this->request->query('role');
        $this->set('functional', $functional);
        $this->set('role', $role);
        $this->render("../Access/forbidden");
    }

    public function manager_data()
    {
        if (!$this->is_auth()) {
            return false;
        } else {

            if (empty($this->manager_data)) {
                $this->manager_data = $this->Admin->manager_data();
            }

            return $this->manager_data;
        }
    }

    public function manager_name()
    {
        if (!empty($this->manager_data['firstname'])) {
            return "Manager: " . $this->manager_data['firstname'];
        } else {
            return "";
        }
    }

    public function beforeFilter()
    {
        if ($this->request->params["controller"] == "api") {
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->header('Access-Control-Allow-Headers', 'X-Requested-With');
            $this->response->header('Access-Control-Allow-Methods', 'GET, POST');

            return;
        }
        $not_auth_valid_methods = ['login', 'passreminder', 'access_token'];

        $auth_error      = $this->request->query('auth_error') ?? null;
        $auth_error_text = $this->request->query('auth_error_text') ?? null;
        $this->set('auth_error', $auth_error);
        $this->set('auth_error_text', $auth_error_text);
        $show_login_form = true;
        $this->set('show_login_form', $show_login_form);

        if (!$this->is_auth()) {
            if ($this->request->controller != 'index' or
                !in_array($this->request->action, $not_auth_valid_methods)
            ) {
                if ($this->request->is('post')) {
                    die("need to be auth");
                } else {

                    $this->redirect(
                        [
                            'controller' => 'index',
                            'action'     => 'login',
                        ]
                    );
                }

                return;
            }
        }
        if ($this->request->controller == 'index' and
            $this->request->action == 'login'
        ) {
            return;
        }

        $this->_setLanguage();

        $this->manager_data();

        $active_page = strtolower($this->request->controller);

        $chat_user_layout = 'false';
        if ($this->request->controller == "Message") {
            $chat_user_layout = 'true';
        }
        $this->set('chat_user_layout', $chat_user_layout);

        //        if ($active_page == "manager") {
        //            $active_page = "";
        //        }
        if ($this->request->controller == "index" && $this->request->action == "site_data") {
            $active_page = "site_data";
        }
        if ($this->request->controller == "welcome_screen") {
            $active_page = "site_data";
        }

        if ($this->request->controller == "mobile") {
            $active_page = "mobile";
        }

        if ($active_page == "manage" && $this->request->action == "restapi") {
            $active_page = "restapi";
        }

        if ($this->request->controller == "index" && $this->request->action == "task_list") {
            $active_page = "task_list";
        }

        if ($active_page == "cars" or $active_page == "car") {
            $active_page = "cars";
        }

        if ($active_page == "admin" && $this->request->action == "roles") {
            $active_page = "settings";
        }

        if ($active_page == "admin" && $this->request->action !== "roles") {
            $active_page = "admins";
        }

        if ($this->request->controller == "user") {
            $active_page = "users";
        }

        if ($this->request->controller == "new") {
            $active_page = "news";
        }

        if ($this->request->controller == "location") {
            $active_page = "locations";
        }

        if ($this->request->controller == "action") {
            $active_page = "actions";
        }
        if ($this->request->controller == "static_page") {
            $active_page = "static_page";
        }
        if ($this->request->controller == "push") {
            $active_page = "push";
        }

        //наличие отзывов о компании
        //$answered_ticket_count = $this->Support->get_new_company_tickets();
        //$this->set('answered_ticket_count', $answered_ticket_count);

        $status = $this->Admin->manager_data()['Manager']['status'];
        if ($status == "blocked") {
            echo 'Вы заблокированы в системе. Для разблокировки обратитесь к старшему администратору';
            exit;
        }

        //user role setup
        $role        = isset($this->Admin->manager_data()['Manager']['role']) ? $this->Admin->manager_data(
        )['Manager']['role'] : false;
        $access_list = $this->Access->getAccessListByRole($role);
        $this->set('active_page', $active_page);
        $this->set('is_auth', $this->is_auth() ? 'true' : 'false');
        $this->set('title', "Личный кабинет");
        $this->set('manager_data', $this->Admin->manager_data());
        $this->set('access_list', $access_list);
        $this->set('content_title', "");

        parent::beforeFilter();
    }

    private function _setLanguage()
    {
        $session_lang = ($this->Session->read('lang')) ? $this->Session->read('lang') : null;
        if ($session_lang == null) {
            $session_lang = Configure::read('DEF_LANG');

            Configure::load('rus_config');

        } else {
            if (!in_array($session_lang, Configure::read('VALID_LANGS'))) {
                $session_lang = Configure::read('DEF_LANG');
                Configure::load('rus_config');
            }
        }
        $this->Session->write('lang', $session_lang);
        $c = 0;
        foreach (Configure::read('VALID_LANGS') as $lang) {

            if ($lang == $session_lang) {
                $key = $c;
            }
            $c++;
        }
        $locale = Configure::read('VALID_LANG_LOCALES')[$key];
        //запись в конфиг локали
        Configure::write('Config.language', $locale);
        Configure::load($locale . '_config');
        $this->Session->write('lang_locale', $locale);

        $lang_prefix = 'name_' . lang_prefix();
        $this->set('lang_prefix', $lang_prefix);
    }

}