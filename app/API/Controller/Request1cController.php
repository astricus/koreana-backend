<?php

App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class Request1cController extends AppController
{
    private $request1c_timeout = 40;

    private $def_start_date = "2000-01-01";

    public $file_tmp = APP . DS . "tmp" . DS;

    public $components = [
        'Breadcrumbs',
        'Flash',
        'Log',
        'Session',
        'User',
    ];

    public $layout = "default";

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
        if ($this->request->is('post')) {
            $user_id = $this->request->data('user_id') ?? null;
            $operation = $this->request->data('operation') ?? null;
            $car_number = $this->request->data('car_number') ?? null;
            $date_start = $this->request->data('date_start') ?? null;
            $date_finish = $this->request->data('date_finish') ?? null;
            $order_id = $this->request->data('order_id') ?? null;
            $client_phone = $this->request->data('phone') ?? null;
            $api_data = [];
            if ($operation == "last_order") {
                $operation_name = "GetLastOrderAndLinkCustomer";
                $api_data['clientID'] = $user_id;//"10";
                $api_data['carNumber'] = $car_number;// "У547НТ190";
                $api_data['phone'] = $client_phone;//"9261715085";
            } else if ($operation == "order_info") {
                $operation_name = "GetOrder";
                $api_data['clientID'] = $user_id;//"10";
                $api_data['phone'] = $client_phone;//"9261715085";
                $api_data['OrderNumber'] = $order_id;//;"ППКР-012357";
                $api_data['carNumber'] = $car_number;// "У547НТ190";
            } else if ($operation == "order_list") {
                $operation_name = "GetOrdersList";
                $api_data['clientID'] = $user_id;//"10";
                $api_data['carNumber'] = $car_number;// "У547НТ190";
                $api_data['dateStart'] = $date_start;
                $api_data['dateFinish'] = $date_finish;
            }
            if ($operation !== null) {
                $result = $this->request1c($operation_name, $api_data);
            } else {
                $result = "operation not set!";
            }

            $params = [];
            if (!empty($user_id)) {
                $params["user_id"] = $user_id;
            }
            if (!empty($operation)) {
                $params["operation"] = $operation;
            }
            if (!empty($car_number)) {
                $params["car_number"] = $car_number;
            }
            if (!empty($date_start)) {
                $params["date_start"] = $date_start;
            }
            if (!empty($date_finish)) {
                $params["date_finish"] = $date_finish;
            }
            if (!empty($order_id)) {
                $params["order_id"] = $order_id;
            }
            if (!empty($client_phone)) {
                $params["phone"] = $client_phone;
            }

            $result_file = "R_" . md5(uniqid());
            file_put_contents($this->file_tmp . $result_file, $result);
            $params["tmpfile"] = $result_file;
            $params_string = http_build_query($params);
            if ($result === false) { /* Handle error */
                $this->Flash->set(__("Ошибка выполнения запроса"), ['element' => 'flash']);
            } else {
                $this->Flash->set(__("Запрос успешно выполнен"), ['element' => 'flash']);
            }
            return $this->redirect("/request1c?" . $params_string);
        } else {
            $user_id = $this->request->query('user_id') ?? null;
            $operation = $this->request->query('operation') ?? null;
            $car_number = $this->request->query('car_number') ?? null;
            $date_start = $this->request->query('date_start') ?? $this->def_start_date;
            $date_finish = $this->request->query('date_finish') ?? date("Y-m-d");
            $order_id = $this->request->query('order_id') ?? null;
            $client_phone = $this->request->query('phone') ?? null;
            $tmpfile = $this->request->query('tmpfile') ?? null;

            if($tmpfile!=null && file_exists($this->file_tmp . $tmpfile)){
                $request_result = file_get_contents($this->file_tmp . $tmpfile);
                unlink($this->file_tmp . $tmpfile);
            } else {
                $request_result = "file error!";
            }
            $this->set('user_id', $user_id);
            $this->set('type', $operation);
            $this->set('car_number', $car_number);
            $this->set('date_start', $date_start);
            $this->set('date_finish', $date_finish);
            $this->set('order_id', $order_id);
            $this->set('phone', $client_phone);
            $all_users = $this->User->userSimpleList();
            $this->set("all_users", $all_users);
            $this->set("request_result", $request_result);
            $this->set("operation", $operation);
        }

    }

    /**
     * @return array|void|null
     */
    public function get_user_cars()
    {
        $this->layout = false;
        $user_id = $this->request->data('user_id') ?? null;
        if (!intval($user_id) or $user_id == 0) die("false user_id");
        $modelName = "User_Car";
        $this->User_Car = ClassRegistry::init($modelName);
        $cars = $this->User_Car->find(
            "all",
            [
                'conditions' =>
                    [
                        'user_id' => $user_id,
                        'status' => 'active'
                    ],
            ]
        );
        if ($cars == null) {
            response_ajax([], "success");
            exit;
        }
        $cars_arr = [];
        foreach ($cars as $car) {
            $cars_arr[] = $car['User_Car']['car_number'];
        }
        response_ajax(['cars' => $cars_arr], "success");
        exit;
    }

    /**
     * @param string $method_name
     * @param array $api_data
     * @return false|string
     */
    private function request1c(string $method_name, array $api_data)
    {
        $server_1c = Configure::read('1C_GATE');
        $url = $server_1c . $method_name;
        $options = [
            'http' => [
                'method' => 'POST',
                'timeout' => $this->request1c_timeout,
                'content' => json_encode($api_data),
                'header' => "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n" .
                    "Accept-Charset: UTF-8, *;q=0",
            ],
        ];
        $context = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }

    public function last_order()
    {
        $this->set('title', "Тестирование 1с: получение данных последнего заказа заказа");
        if ($this->request->is('post')) {
            $client_phone = $this->request->query('phone') ?? null;
            $car_number = $this->request->query('number') ?? null;
            $client_id = $this->request->query('id') ?? null;
            $api_data = [];
            $api_data['clientID'] = $client_id;//"10";
            $api_data['phone'] = $client_phone;//"9261715085";
            //$api_data['OrderNumber '] = "ППКР-012357";
            $api_data['carNumber'] = $car_number;// "У547НТ190";

            $result = $this->request1c("GetLastOrderAndLinkCustomer", $api_data);
            if ($result === false) { /* Handle error */
                $this->set("result", "Ошибка выполнения запроса");
            }
            $this->set("result", $result);
        }

    }

    public function order_info()
    {
        $this->set('title', "Тестирование 1с: получение данных заказа");
        if ($this->request->is('post')) {
            $order_id = $this->request->query('order_id') ?? null;
            $client_phone = $this->request->query('phone') ?? null;
            $car_number = $this->request->query('number') ?? null;
            $client_id = $this->request->query('id') ?? null;

            $api_data = [];
            $api_data['clientID'] = $client_id;//"10";
            $api_data['phone'] = $client_phone;//"9261715085";
            $api_data['OrderNumber'] = $order_id;//;"ППКР-012357";
            $api_data['carNumber'] = $car_number;// "У547НТ190";

            $result = $this->request1c("GetOrder", $api_data);

            if ($result === false) { /* Handle error */
                $this->set("result", "Ошибка выполнения запроса");
            }
            $this->set("result", $result);
        }
    }

    public function order_list()
    {
        $car_number = $this->request->query('number') ?? null;
        $client_id = $this->request->query('id') ?? null;
        $date_start = $this->request->query('date_start') ?? null;
        $date_finish = $this->request->query('date_finish') ?? null;
        $api_data = [];
        $api_data['clientID'] = $client_id;//"10";
        $api_data['carNumber'] = $car_number;// "У547НТ190";
        $api_data['dateStart'] = $date_start;
        $api_data['dateFinish'] = $date_finish;

        $result = $this->request1c("GetOrdersList", $api_data);
        if ($result === false) { /* Handle error */
            echo "Ошибка выполнения запроса";
        }
        var_dump($result);
        exit;
    }

}