<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');
App::import('Lib', 'CsvExport');

class StoController extends AppController
{
    public $uses = array(
        'Location',
        'Auto_Service_Record'
    );

    public $components = array(
        'Admin',
        'Api',
        'Breadcrumbs',
        'Flash',
        'Session',
        'Location',
        'City',
        'Sto'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function city_list()
    {
        $this->set('cities', $this->City->getCityList());
    }

    public function record()
    {
        $id = $this->params->id ?? null;
        if ($id == null or intval($id) == 0) {
            die("record id is null!");
        }
        $record = $this->Sto->record($id);
        $this->set("record", $record);
    }

    public function record_list()
    {
        $show_count = 10;
        $export_csv = isset($this->request->query['export_csv']) ? true : false;
        if ($export_csv) {
            $show_count = 9999;
        }
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";
        $search = $this->request->query['search'] ?? null;
        $city_id = $this->request->query['city_id'] ?? null;

        $date_from = $this->request->query['date_from'] ?? '2022-02-01';
        $date_to = $this->request->query['date_to'] ?? date("Y-m-d");

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];

        $filter["city_id"] = null;
        if ($city_id != null) {
            $filter["city_id"] = $city_id;
        }

        $filter["date_from"] = $date_from;
        $filter["date_to"] = $date_to;

        $locations = $this->Sto->recordList($show_count, $page, $sort, $sort_dir, $filter, $search);
        if (count($locations) > 0) {
            $cities_arr = [];
            foreach ($locations as &$location) {
                if (!in_array($location['Location']['city_id'], array_keys($cities_arr))) {
                    $city_name = $this->City->getCityNameById($location['Location']['city_id']);
                    $cities_arr[$location['Location']['city_id']] = $city_name;
                    $location['Location']['city_name'] = $city_name;
                } else {
                    $location['Location']['city_name'] = $cities_arr[$location['Location']['city_id']];
                }
            }
        }
        $this->set("record_list", $locations);
        $form_data = array(
            'filter' => $filter,
            'search' => $search,
            'date_from' => $date_from,
            'date_to' => $date_to,
        );

        $form_data['city_id'] = $city_id;
        $form_data['city_name'] = $this->City->getCityNameById($city_id);

        $this->set('form_data', $form_data);
        if ($export_csv) {
            $this->layout = null;
            $this->autoLayout = false;
            $Csv_file = new CsvExport;

            if (count($locations) == 0) {
                $Csv_file->addRow(["записей нет"]);
            } else {
                $line_names = ['№', 'Запись #', 'Пользователь', 'Дата записи', 'Машина', 'Автосервис'];
                $Csv_file->addRow($line_names);
                $rn = 1;
                foreach ($locations as $record) {
                    $record_data = $record['Auto_Service_Record'];
                    $User_data = $record['User'];
                    $Car_data = $record['User_Car'];
                    $Location_data = $record['Location'];
                    $Car_mark = $record['Car_Mark'];
                    $Car_model = $record['Car_Model'];

                    $line_row = [$rn, $record_data['id'],
                        $User_data['phone'] . " " . prepare_fio($User_data['lastname'],
                            $User_data['firstname'], $User_data['middlename']),
                        lang_calendar($record_data['service_datetime']),
                        $Car_mark['name'] . ", " . $Car_model['name'] . " рег.номер " . $Car_data['car_number'],
                        $Location_data['address']
                    ];
                    $Csv_file->addRow($line_row);
                    $rn++;
                }
            }
            $filename = 'Записи на автосервис';
            $file_comment = "период c " . $form_data['date_from'] . " по " . $form_data['date_to'] . " " . date("Y-m-d H:i:s");
            $Csv_file->setFilename($filename . " " . $file_comment);
            echo $Csv_file->render();
            $Csv_file->renderHeaders();
            exit;
        } else {
            $this->set("page", $page);
            $pages = ceil($this->Sto->totalCount($filter, $search) / $show_count);
            $this->set("pages", $pages);

            $this->set('cities', $this->City->getCityList());
            $this->set('location_types', $this->Location->getLocationTypes());
            $this->set('content_dir', Configure::read('CONTENT_UPLOAD_DIR_RELATIVE'));
            $this->set('ya_map_api_key', Configure::read('YA_MAP_API_KEY'));
        }
    }

    public function map_ajax()
    {
        $location_type = $this->request->query['location_type_id'] ?? null;
        $filter = [];

        $filter["location_type"] = null;
        if ($location_type != null) {
            $filter["location_type_id"] = $location_type;
        }

        $locations = $this->Location->locationMarks($filter);
        if (count($locations) > 0) {
            $maps_items = [];
            //$cities_arr = [];
            foreach ($locations as $location) {
                /*
                if (!in_array($location['Location']['city_id'], array_keys($cities_arr))) {
                    $city_name = $this->City->getCityNameById($location['Location']['city_id']);
                    $cities_arr[$location['Location']['city_id']] = $city_name;
                    $location['Location']['city_name'] = $city_name;
                } else {
                    $location['Location']['city_name'] = $cities_arr[$location['Location']['city_id']];
                }*/

                $maps_items['features'][] = array(
                    'type' => 'Feature',
                    "geometry" => array(
                        "type" => "Point",
                        "coordinates" =>
                            array(
                                $location['Location']['lat'],
                                $location['Location']['lon']
                            )
                    ), 'properties' => array("balloonContent" => $location['Location']['title'],
                        "hintContent" => "Адрес от пользователя " . $location['Location']['address']
                    )
                ,
                    "options" => array(
                        "preset" => "islands#DotIcon",
                        "iconColor" => '#ff0000'
                    ),
                    'dataContent' => array()
                );

            }
        }
        response_ajax($maps_items);
        exit;
    }

    public function types()
    {
        $location_types = $this->Location->getLocationTypes();
        $this->set("location_types", $location_types);
    }

    public function view()
    {
        $id = $this->request->param('id');
        $location = $this->Location->getLocationById($id);
        $location['days_later'] = days_later(time() - strtotime($location['created']));
        $this->set("location", $location);
    }

    /**
     * @param $loc_id
     * @param $day
     * @param $start
     * @param $end
     */
    private function saveWT($loc_id, $day, $start, $end)
    {
        $save_data = [
            'location_id' => $loc_id,
            'day' => $day,
            'start_time' => $start ?? 0,
            'end_time' => $end ?? 0,
            'is_work_day' => 1,
            'test' => 1
        ];
        $this->Work_Time->create();
        $this->Work_Time->save($save_data);
    }

    /**
     * @param $daytime
     * @param $location_id
     */
    private function formatWT($daytime, $location_id)
    {
        $daytime_item = str_replace(":00", "", $daytime);
        $daytime_item = str_replace(":", "", $daytime_item);
        if (substr_count($daytime, "пн-пт") > 0) {
            $time_str = str_replace("пн-пт ", "", $daytime_item);
            $days = ['pnd', 'vt', 'sr', 'cht', 'pt'];
        } else if (substr_count($daytime, "пн-сб") > 0) {
            //echo "found!";
            $time_str = str_replace("пн-сб ", "", $daytime_item);
            $days = ['pnd', 'vt', 'sr', 'cht', 'pt', 'sb'];
        } else if (substr_count($daytime, "сб") > 0) {
            $time_str = str_replace("сб ", "", $daytime_item);
            $days = ['sb'];
        } else if (substr_count($daytime, "вс") > 0) {
            $time_str = str_replace("вс ", "", $daytime_item);
            $days = ['vs'];
        } else {
            pr($daytime_item);

        }
        $time_arr = explode("-", $time_str);
        foreach ($time_arr as &$time_i) {
            if (substr($time_i, 0, 1) == 0 && mb_strlen($time_i) == 2) {
                $time_i = substr($time_i, 1, 1);
            }
        }
        foreach ($days as $day) {
            $this->saveWT($location_id, $day, $time_arr[0], $time_arr[1]);
        }

    }

    /**
     * @param $daytime
     * @param $location_id
     */
    private function parseWorkTime($daytime, $location_id)
    {
        //пн-пт: 09:00-19:00, сб: 09:00-17:00, вс: c 10:00-17:00
        $daytime = str_replace("c ", "", $daytime);
        if (substr_count($daytime, ",") > 0) {
            $daytime_arr = explode(",", $daytime);
            foreach ($daytime_arr as $daytime_item) {

                $this->formatWT($daytime_item, $location_id);

            }
        } else {
            $this->formatWT($daytime, $location_id);
        }
    }

    public function clearOldParsed()
    {
        $this->loadModel('Location');
        $locations = $this->Location->find("all",
            array('conditions' =>
                array(
                    'test' => 1
                )
            )
        );
        foreach ($locations as $location) {
            $lid = $location['Location']['id'];
            $this->Location->id = $lid;
            $this->Location->delete();
        }

        $wts = $this->Work_Time->find("all",
            array('conditions' =>
                array(
                    'test' => 1
                )
            )
        );
        foreach ($wts as $wt) {
            $wid = $wt['Work_Time']['id'];
            $this->Work_Time->id = $wid;
            $this->Work_Time->delete();
        }
    }

    public function clearImages()
    {
        $this->loadModel('Location');
        $locations = $this->Location->find("all",
            array('conditions' =>
                array(
                    'test' => 1
                )
            )
        );
        foreach ($locations as $location) {
            $image = $location['Location']['image'];
            $image_new = explode("?", $image)[0];
            $this->Location->id = $location['Location']['id'];
            $this->Location->save(['image' => $image_new]);
        }


    }

    public function clearPhones()
    {
        $this->loadModel('Location');
        $locations = $this->Location->find("all",
            array('conditions' =>
                array(
                    'test' => 1
                )
            )
        );
        foreach ($locations as $location) {
            $clear_phones = $location['Location']['phones'];
            $clean_phone = str_replace("phone", "", $clear_phones);
            $this->Location->id = $location['Location']['id'];
            $this->Location->save(['phones' => $clean_phone]);
        }


    }

    public function saveXML()
    {
        //$file = "https://koreanaparts.ru/sprav.xml";
//        $file_saved = Configure::read('FILE_TEMP_DIR') . DS . "file.xml";
//        if(!file_exists($file_saved)){
//            $xml = file_put_contents($file_saved, file_get_contents($file));
//        }
        $this->clearPhones();
        exit;
        $file_saved = Configure::read('FILE_TEMP_DIR') . DS . "file.xml";
        $xml = file_get_contents($file_saved);
        $xml_header = '<?xml version="1.0" encoding="UTF-8"?>';
        if (substr_count($xml, $xml_header) == 0) {
            $xml = $xml_header . $xml;
        }
        $xmlObject = Xml::build($xml);
        $xmlArray = Xml::toArray($xmlObject);
        $this->LocationCom = $this->Components->load('Location');
        $this->loadModel('Location');
        foreach ($xmlArray['companies']['company'] as $xmlArrayCompany) {
            $comp = $xmlArrayCompany;
            //pr($comp);
            $address = $comp['address']['@'];
            $name = $comp['name']['@'];

            $coordinates = $comp['coordinates'];
            $lon = $coordinates['lon'];
            $lat = $coordinates['lat'];
            $email = $comp['email'];
            $working_time = $comp['working-time']['@'];

            $loc_parse_type = $comp['company-id'];
            if (substr_count($loc_parse_type, "sto") > 0) {
                $location_type_id = 2;
            } else {
                $location_type_id = 1;
            }

            $number = "";
            if (is_array($comp['phone'])) {
                if (count($comp['phone']) > 1) {
                    foreach ($comp['phone'] as $p_number) {
                        if (is_array($p_number)) {
                            $number .= " " . $p_number['number'];
                        } else {
                            $number .= " " . $p_number;
                        }
                    }
                } else {
                    $number = $comp['phone']['number'];
                }

            }

            $address_city = explode(",", $address);
            $city_id = 0;
            if (count($address_city) > 0) {
                $city_name = trim($address_city[0]);
                $city_id = $this->City->getCityIdByName($city_name);
                if ($city_id == 0) {
                    $city_id = $this->City->createCity($city_name);
                }
            }


            $data = [
                'address' => $address,
                'title' => $name,
                'phones' => $number,
                'location_type_id' => $location_type_id,
                'address_desc' => $address,
                'view_on_map' => 1,
                'image' => '',
                'lat' => $lat,
                'lon' => $lon,
                'email' => $email ?? "",
                'test' => 1,
                'city_id' => $city_id
            ];

            $this->Location->create();
            if ($this->Location->save($data)) {
                $location_id = $this->Location->id;
            }

            if (key_exists('photos', $comp)) {
                $photos = $comp['photos']['photo']['@url'];
                if (!empty($photos)) {
                    $res = $this->Content->saveLocationImageByLink($photos);
                    $this->Location->id = $location_id;
                    $this->Location->save(['image' => $res]);
                }
            }
            $this->parseWorkTime($working_time, $location_id);
        }
        return;
    }

    public function edit()
    {
        $id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор локации"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $location = $this->Location->getLocationById($id);
            if ($location == null) {
                $this->Flash->set(__("Локация с таким id ($id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $new_location = $this->Location->updateLocation($id, $data);

            $this->Location->saveWorkTime($id, $this->request->data['work_time']);

            $image_name = "image";
            $file_max_size = $this->Content->image_params['max_size_mb'];
            if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                    $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                }
                //width & height
                $res = $this->Location->saveLocationImage($_FILES[$image_name],
                    $this->Content->image_params['default_image_width'],
                    $this->Content->image_params['default_image_height'],
                    $id);
                if ($res['status'] == 'ok') {
                    $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash']);
                } else {
                    $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash']);
                }
            }

            if (!empty($new_location["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $new_location["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {

                $comment = 'Изменение данных локации с id[#id]';
                $activity['id'] = $id;
                $this->Activity->add("locations", $this->Admin->manager_id(), $comment, $activity);

                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/location/edit/" . $id);
            }
        } else {
            if ($id == null) {
                $this->Flash->set(__("Не задан идентификатор локации"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $location = $this->Location->getLocationById($id);
            if ($location == null) {
                $this->Flash->set(__("Локация с таким id ($id) не найдена"));
                return $this->redirect($this->referer());
            }
            $this->set('title', "Локация №$id");
            $this->set('location', $location);
            $this->set('cities', $this->City->getCityList());
            $this->set('location_types', $this->Location->getLocationTypes());
            $this->set('ya_map_api_key', Configure::read('YA_MAP_API_KEY'));
            $this->set('work_time', $this->Location->getWorkTime($id));
        }
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $new_location = $this->Location->createLocation($data);
            if (!empty($new_location["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $new_location["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $id = $new_location;
                $this->Location->saveWorkTime($id, $this->request->data['work_time']);
                $file_max_size = $this->Content->image_params['max_size_mb'];
                $image_name = "image";
                if ((isset($_FILES[$image_name])) and ($_FILES[$image_name]['size'] > 0) and (!empty($_FILES[$image_name]['name']))) {
                    if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                        $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                    }
                    //width & height
                    $res = $this->Location->saveLocationImage($_FILES[$image_name],
                        $this->Content->image_params['default_image_width'],
                        $this->Content->image_params['default_image_height'],
                        $id);
                    if ($res['status'] == 'ok') {
                        $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash']);
                    } else {
                        $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash']);
                    }
                } else {
                    $this->Flash->set(__("Файла нет: "), ['element' => 'flash']);
                }

                $comment = 'Добавление локации, создана локация с id[#id]';
                $activity['id'] = $id;
                $this->Activity->add("locations", $this->Admin->manager_id(), $comment, $activity);

                $this->redirect("/location/edit/" . $id);
            }
        } else {
            $this->set('title', "Добавление локации");
            $cities = $this->City->getCityList();
            $this->set('cities', $cities);
            $this->set('location_types', $this->Location->getLocationTypes());
            $this->set('ya_map_api_key', Configure::read('YA_MAP_API_KEY'));
        }
    }

    public function delete()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор локации"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Location->deleteLocation($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {

            $comment = 'Удаление локации с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("locations", $this->Admin->manager_id(), $comment, $activity);

            $this->Flash->set(__("Локация была удалена"), ['element' => 'flash']);
            $this->redirect("/locations");
        }
        exit;
    }

    public function create_type()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($this->Location->createLocation_Type($data)) {
                $this->Flash->set(__("Тип локации успешно добавлен"));
                return $this->redirect('/location/types');
            } else {
                $error = [];
                $this->Flash->set(__("Ошибка " . $error["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }

        }
    }

    public function delete_type()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор типа локации"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->Location->deleteLocationType($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {
            $this->Flash->set(__("Тип локации был удален"), ['element' => 'flash']);
            $this->redirect("/location/types");
        }
        exit;
    }

    public function block()
    {
        $id = $this->request->param('id');
        if ($this->Location->getLocationById($id) == null) {
            $this->Flash->set(__("Локация с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Location->blockLocation($id);

        $comment = 'Локация с id[#id] скрыта для показа';
        $activity['id'] = $id;
        $this->Activity->add("locations", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Локация скрыта из показа"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public function unblock()
    {
        $id = $this->request->param('id');
        if ($this->Location->getLocationById($id) == null) {
            $this->Flash->set(__("Локация с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->Location->unblockLocation($id);

        $comment = 'Локация с id[#id] открыта для показа';
        $activity['id'] = $id;
        $this->Activity->add("locations", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Локация открыта для показа на сайт"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

}