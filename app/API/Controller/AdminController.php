<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class AdminController extends AppController
{
    public $uses = array(
        'Manager',
    );

    public $components = array(
        'Access',
        'Activity',
        'Admin',
        'Session',
        'Breadcrumbs',
        'Flash',
        'Validator',
        'EmailCom'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
    }

    public function profile()
    {
        $profile_url_prefix = $this->request->params['profile'];
        if (is_numeric($profile_url_prefix)) {
            $param = 'id';
        } else {
            $param = 'login';
        }
        $admin = $this->Manager->find('first', array('conditions' => array(
            $param => $profile_url_prefix,
        ),
            'fields' => array('Manager.*',
                'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS reg_time',
                //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(last_activity) AS last_act',
            )
        ));

        $role = $admin['Manager']['role'];
        $this->set("profile_role", $this->admin_role($role));
        $this->set("profile", $admin);

        if ($admin['Manager']['id'] == $this->Session->read('manager_id')) {
            $this->set("is_you", "(Это вы)");
        } else {
            $this->set("is_you", "");
        }

    }

    /**
     * @param $role
     * @return mixed
     */
    private function admin_role($role)
    {
        $role_names = array_flip($this->Access->roles);
        if (in_array($role, $role_names)) {
            return $this->Access->roles[$role];
        } else die("undefined admin role detected");
    }

    public function index()
    {
        $show_count = 3;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $admin_count = $this->Manager->find("count",
            array('conditions' =>
                array(),
            )
        );
        $pages = ceil($admin_count / $show_count);

        $role = $this->request->query['role'] ?? null;
        $admin_id = $this->request->query['admin_id'] ?? null;

        $admin_id = (int)$admin_id;


        $role_val = [];
        if ($role !== null && in_array($role, array_keys($this->Access->roles))) {
            $role_val = array('role' => $role);
        }
        $admin_val = [];
        if ($admin_id !== null && $admin_id>0) {
            $admin_val = array('id' => $admin_id);
        }

        $admins = $this->Manager->find('all',
            array('conditions' => array(
                $role_val,
                $admin_val
            ),
                'fields' => array('Manager.*',
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS reg_time',
                    //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(last_activity) AS last_act',
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
            ));
        $this->set("admins", $admins);

        $all_admins = $this->Manager->find('all',
            array('conditions' => array(),
                'order' => array('firstname ASC'),
            ));
        $this->set("all_admins", $all_admins);


        $this->set('page', $page);
        $this->set('pages', $pages);

        $roles = $this->Access->roles;
        $this->set("roles_list", $roles);

        $this->set('admin_count', $admin_count);
        $this->set('title', "Администраторы");

        $form_data['role'] = $role;
        $form_data['admin_id'] = $admin_id;
        $this->set('form_data', $form_data);
    }

    public function preProcessAccessList()
    {
        return $this->Access->preProcessAccessList();
    }

    public function roles()
    {
        $roles = $this->Access->roles;
        $this->set("roles_list", $roles);

        $access_list = $this->Access->getAccessListByName();

        $this->set("access_elems", $access_list);

        $access_list_arr = [];
        foreach ($access_list as $name => $title) {
            foreach (array_keys($roles) as $role) {
                $access_list_arr[$name][$role] = $this->Access->getAccessByNameAndRole($name, $role);
            }
        }
        //pr($access_list_arr);

        $this->set("access_list_arr", $access_list_arr);
        $this->render("../Access/roles");
    }

    public function update_role()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $data_arr = [];
            foreach ($data as $key => $val) {
                foreach ($val as $k2 => $v2) {
                    $data_arr[] = ['role' => $key, 'name' => $k2, 'status' => $v2];
                }
            }
            $this->Access->updateAccessList($data_arr);

            $comment = 'Изменение настроек доступа';
            $this->Activity->add("settings", $this->Admin->manager_id(), $comment, null);

            $this->Flash->set(__('Данные были успешно изменены'));
            return $this->redirect(['controller' => 'admin', 'action' => 'roles']); //'id' => $id
        }
    }

    public function block()
    {
        $id = $this->request->param('id');
        if ($id <= 0) {
            die("Администратор не найден");
        }
        if ($this->change_status($id, 'blocked')) {
            $this->Flash->set(__('Администратор заблокирован'), ['element' => 'flash_success']);
            return $this->redirect($this->referer());
        } else {
            die($this->Manager->errors());
        }
    }

    public function unblock()
    {
        $id = $this->request->param('id');
        if ($id <= 0) {
            die("Администратор не найден");
        }
        if ($this->change_status($id, 'active')) {
            $this->Flash->set(__('Администратор успешно разблокирован'), ['element' => 'flash_success']);
            return $this->redirect($this->referer());
        } else {
            die($this->Manager->errors());
        }
    }

    private function generateRandomPass($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%-_{}[]<>')
    {
        $str = '';
        $max = mb_strlen($keyspace) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;

    }

    public function add()
    {
        if ($this->request->is('post')) {
            $errors = [];
            $data = $this->params['data'];

            if (empty($data['email'])) {
                $errors["email"] = "Не заполнено поле электронная почта";
            }

            $data['firstname'] = trim($data['firstname']);
            $data['lastname'] = trim($data['lastname']);
            $data['email'] = trim($data['email']);
            $data['firstname'] = mb_ucfirst(mb_strtolower($data['firstname']));
            $data['lastname'] = mb_ucfirst(mb_strtolower($data['lastname']));

            if (!$this->Validator->valid_firstname($data['firstname'])) {
                $errors["firstname"] = "Некорректное имя";
            }

            if (!$this->Validator->valid_lastname($data['lastname'])) {
                $errors["lastname"] = "Некорректная фамилия";
            }

            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
                $errors["email"] = "Некорректный адрес почты";
            }

            if ($this->Admin->checkEmailExists($data['email'])) {
                $errors["email"] = "Указанная почта уже есть в системе!!!";
            }

            if (count($errors) > 0) {
                $this->Session->write('errors', $errors);
                $this->Session->write('data_value', $data);

                $err_str = implode(" ", $errors);
                $this->Flash->set(__('Возникла ошибка при создании администратора!'), ['element' => 'flash_error']);
                return $this->redirect($this->referer());
            }
            $this->Session->write('errors', null);
            $this->Session->write('data_value', null);

            //данные пользователя
            $user_data["firstname"] = $data["firstname"] ?? null;
            $user_data["lastname"] = $data["lastname"] ?? null;
            $user_data["email"] = isset($data["email"]) ? $data["email"] : null;

            $user_data["mail_key"] = md5(time() . $user_data["email"]);
            //генерация пароля с солью
            $real_pwd = $this->generateRandomPass(10);
            $user_data["password"] = get_hash(Configure::read('USER_AUTH_SALT'), $real_pwd);

            $this->Manager->save($user_data);
            $manager_id = $this->Manager->getLastInsertId();

            //ключ активации
            $mail_key_salt = Configure::read('MAIL_KEY_SALT');
            $mail_key = generate_mail_key($manager_id, $mail_key_salt);
            $this->Manager->id = $manager_id;
            $this->Manager->save(array('mail_key' => $mail_key));

            $this->Flash->set(__("Администратор был успешно добавлен"), ['element' => 'flash_success']);
            $comment = 'Добавление администратора [#name]';
            $data['name'] = $user_data["firstname"] . " " .  $user_data["lastname"];
            $this->Activity->add("admin", $this->Admin->manager_id(), $comment, $data);

            $this->sendAdminEmail($user_data["email"], $real_pwd, "register");
            return $this->redirect(site_url() . "/admin/view/" . $manager_id);

        }

        $this->set('errors', $this->Session->read('errors') ?? []);
        $this->set('data_value', $this->Session->read('data_value') ?? []);
        $this->set('roles', $this->Access->roles);
        $this->set('title', "Добавление администратора");
    }

    public function edit()
    {
        $id = $this->request->param('id');

        if ($this->request->is('post')) {
            $data = $this->params['data'];
            $errors = [];
            $firstname = $this->request->data('firstname');
            $lastname = $this->request->data('lastname');
            $role = $this->request->data('role') ?? null;
            $email = $this->request->data('email') ?? null;

//            if (empty($data['email'])) {
//                $errors["email"] = "Не заполнено поле электронная почта";
//            }

            $data['firstname'] = trim($data['firstname']);
            $data['lastname'] = trim($data['lastname']);
            //$data['email'] = trim($data['email']);
            $data['firstname'] = mb_ucfirst(mb_strtolower($data['firstname']));
            $data['lastname'] = mb_ucfirst(mb_strtolower($data['lastname']));

            if (!$this->Validator->valid_firstname($data['firstname'])) {
                $errors["firstname"] = "Некорректное имя";
            }

            if (!$this->Validator->valid_lastname($data['lastname'])) {
                $errors["lastname"] = "Некорректная фамилия";
            }

//            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
//                $errors["email"] = "Некорректный адрес почты";
//            }

            /*
            if ($this->Admin->checkEmailExists($data['email'])) {
                $errors["email"] = "Указанная почта уже есть в системе";
            }
            */
            if (count($errors) > 0) {
                $this->Session->write('errors', $errors);

                $err_str = implode(" ", $errors);
                $this->Flash->set(__("Ошибка!"), ['element' => 'flash_error']);
                return $this->redirect($this->referer());
            }
            $this->Session->write('errors', null);

//
//            $pass = $this->generateRandomPass(10, null);
//            $pass_hash = get_hash(Configure::read('USER_AUTH_SALT'), $pass);

            $saved_data = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'role' => $role,
                //'status' => "active",
                //'email' => $email,
                //'password' => $pass_hash
            );

            $this->Manager->id = $id;
            if ($this->Manager->save($saved_data)) {
                $this->Session->write('errors', null);
                $this->Session->write('data_value', null);
                $this->Flash->set(__("Данные администратора были успешно обновлены"), ['element' => 'flash_success']);
                $comment = 'Изменение данных администратора [#name]';
                $data['name'] = $firstname . " " .  $lastname;
                $this->Activity->add("admin", $this->Admin->manager_id(), $comment, $data);
                //$this->Activity->add("add_object", $this->Admin->manager_id(), $name);
                return $this->redirect(site_url() . "/admin/edit/" . $id); //'id' => $id
            }
        } else {
            $admin = $this->Manager->find('first',
                array(
                    'conditions' => array(
                        'id' => $id
                    ),
                )
            );
            $this->set("admin", $admin);
            $this->set('errors', $this->Session->read('errors') ?? []);
        }

        $this->set('roles', $this->Access->roles);
        $this->set('title', "Редактирование администратора");
    }

    public function resetPassword()
    {
        $admin_id = $this->request->param('id') ?? null;
        if (!$this->checkAdminExist($admin_id)) {
            return false;
        }
        $pass = $this->generateRandomPass(10);
        $pass_hash = get_hash(Configure::read('USER_AUTH_SALT'), $pass);
        $this->Manager->id = $admin_id;
        $saved_data = array(
            'password' => $pass_hash
        );

        $manager = $this->Admin->getManagerById($admin_id);

        if ($this->Manager->save($saved_data)) {
            $admin_id = $this->Manager->id;
            if(!empty($manager['email'])){
                $this->sendAdminEmail($manager['email'], $pass, "reset_pass");
                $this->Flash->set(__('Пароль был сброшен'), ['element' => 'flash_success']);
                $comment = 'Сброс пароля администратора [#name]';
                $data['name'] = $manager['firstname'] . " " .  $manager['lastname'];
                $this->Activity->add("admin", $this->Admin->manager_id(), $comment, $data);
            } else {
                $this->Flash->set(__('Не удалось сбросить пароль ввиду пустого поля email у администратора'), ['element' => 'flash_error']);
            }
            return $this->redirect(['controller' => 'admin', 'action' => 'edit', 'id' => $admin_id]);
        }
    }

    /**
     * @param $email
     * @param $content
     * @param $action
     */
    public function sendAdminEmail($email, $content, $action){
        if($action=="reset_pass"){
            $subject = "Сброс пароля на сайте" . " " . site_url();
            $mail_data = "Вам был сброшен пароль доступа в систему управления Кореана. Ваш новый пароль: $content. В качестве логина используйте ваш адрес электронной почты";
            $template = "user_reset_pass_template";
            $layout = "user_reset_pass_layout";
        } else if($action=="register"){
            $subject = "Регистрация на сайте" . " " . site_url();
            $mail_data = "Вы были зарегистрированы на сайте Кореана в роли администратора. Ваш пароль: $content. В качестве логина используйте ваш адрес электронной почты";
            $template = "user_register_template";
            $layout = "user_register_layout";
        }
        $this->EmailCom->sendEmailNow($email, "", $subject, $layout, $template, $mail_data, $attachment = "", $start_sending = "");
    }

    /**
     * @param $id
     * @return mixed
     */
    private function checkAdminExist($id)
    {
        $id = $this->request->param('id');
        if ($id <= 0) {
            die("Администратор не найден");
        }
        $checkAdmin = $this->Manager->find('count',
            array(
                'conditions' => array(
                    'id' => $id,
                )
            )
        );
        return $checkAdmin;

    }

    private function change_status($id, $status)
    {
        $update_product = array('status' => $status);
        $this->Manager->id = $id;
        if ($this->Manager->save($update_product)) {
            return true;
        }
        return false;
    }

    public function register_form()
    {/*
        //Перед загрузкой формы
        "user_cars" => [{"id" => 1, "brand_name" => "KIA", "model" => "SORENTO", "car_number" => "К 451 РНК 98"},
        {"id" => 1, "brand_name" => "KIA", "model" => "SORENTO", "car_number" => "У 341 УУВ 23"}],

        "services" => [
            {"id" => 1, "service_name" => "Регламентное ТО"},
            {"id" => 2, "service_name" => "Смена масла"}
            ]

        "sto" => [
            {"id" => 1, "address" => "Богатырский пр. д. 24"},
            {"id" => 2, "address" => "Невский пр. д. 24"},
        ]

        // после загрузки формы
        //Ожидаемые данные
        "datetime" => datetime (datetime),
        "car_id" => id (int),
        "sto" => id(int),
        "services" => [{id, ... ,id}]



         //Перед загрузкой формы
        "locations" => [
            {"id" => 1, "address" => "Богатырский пр. д. 24"},
            {"id" => 2, "address" => "Невский пр. д. 24"},
        ...
        ]

        // после загрузки формы
        //Ожидаемые данные
        "location_id" => id (int),[required]
        "mark" => id (int) [1, 2, 3, 4, 5], [custom]
        "comment" => text[{id, ... ,id}] [required]

    //-----------------------
                //Перед загрузкой формы - получение данных с сервера либо из бд мобильного приложения
        "user_cars" => [{"id" => 1, "brand_name" => "KIA", "model" => "SORENTO", "car_number" => "К 451 РНК 98",
                "last_visit" => "2021-05-01 14:30:00", "mileage" => 74733,
        "sto_visits" => [
            {
                "id" => 18276,
                "visit_time" => "2021-05-01 14:30:00",
                "mileage" => 74733,
                "comment" => "Выровнять подкрылки,выпрямить дверь"
            },
        {
            "id" => 17482,
                "visit_time" => "2021-01-04 12:45:00",
                "mileage" => 69743,
                "comment" => "Замена ремня ГРМ"
            }

        ]},
        {"id" => 1, "brand_name" => "FORD", "model" => "S-MAX", "car_number" => "У 341 УУВ 23"},
        "sto_visits" => [
            {
                "id" => 18276,
                "visit_time" => "2021-05-01 14:30:00",
                "mileage" => 74733,
                "comment" => "Выровнять подкрылки,выпрямить дверь"
            },
        {
            "id" => 17482,
                "visit_time" => "2021-01-04 12:45:00",
                "mileage" => 69743,
                "comment" => "Замена ремня ГРМ"
            }

        ]}],
*/

/*
 * сделано: нашел базу данных марок машин и импортировал
 * сделал письма на сервере по записи на СТО, тестовая отправка работает (показывать записи на СТО - добавить)
 * сделал письма на сервере с добавлением отзывов, тестовая отправка работает (множественные отзывы, ограничения?)
 * */


    }

}