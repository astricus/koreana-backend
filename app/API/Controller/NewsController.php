<?php
App::uses('AppController', 'Controller');
App::uses('L10n', 'L10n');

class NewsController extends AppController
{
    public $uses = array(
        'News',
    );

    public $components = array(
        'Activity',
        'Account',
        'Admin',
        'Access',
        'Api',
        'Breadcrumbs',
        'Http',
        'Flash',
        'Session',
        'New',
        'Platform',
        'City',
        'Content'
    );

    public $layout = "default";

    public function beforeFilter()
    {
        $this->Breadcrumbs->add(L('MAIN_PAGE'), Router::url(array('plugin' => false, 'controller' => 'index', 'action' => 'index')));
        parent::beforeFilter();
        $this->Access->checkControlAccess("news");
    }

    public function news_list()
    {
        $show_count = 10;
        $page = isset($this->request->query['page']) ? $this->request->query['page'] : "";
        $filter = isset($this->request->query['filter']) ? $this->request->query['filter'] : "";
        $sort = isset($this->request->query['sort']) ? $this->request->query['sort'] : "";
        $sort_dir = isset($this->request->query['sort_dir']) ? $this->request->query['sort_dir'] : "";

        $platform = $this->request->query['platform'] ?? null;
        $city_id = $this->request->query['city_id'] ?? null;
        $start_datetime = $this->request->query['start_datetime'] ?? null;

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);
        $pages = ceil($this->New->totalNewsCount() / $show_count);

        if ($sort !== "id" && $sort !== "created") {
            $sort = "created";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter = [];
        if($platform!=null){
            $filter["platform"] = $platform;
        }
        if($city_id!=null){
            $filter["city_id"] = $city_id;
        }
        if($start_datetime!=null){
            $filter["start_datetime"] = $start_datetime;
        }

        $news_list = $this->New->newsList($show_count, $limit_page, $sort, $sort_dir, $filter);
        if(count($news_list)>0) {
            foreach ($news_list as &$news_item) {
                $news_author_id = $news_item['News']['author_id'];
                $news_id = $news_item['News']['id'];
                $author = $this->New->getAuthorByAuthorId($news_author_id);
                $news_item['author_name'] = $author;
                $news_item['cities'] = $this->City->getCityByNews($news_id);
            }
        }
        $this->set("news", $news_list);
        $this->set("page", $page);
        $this->set("pages", $pages);
        $form_data = array(
            'filter' => $filter,
        );
        $form_data['platform'] = $platform;
        $form_data['city_id'] = $city_id;
        $form_data['start_datetime'] = $start_datetime;

        $this->set('platforms', $this->Platform->platforms());
        $this->set('form_data', $form_data);
        $this->set('cities', $this->City->getCityList());

        $this->set('content_dir', Configure::read('CONTENT_UPLOAD_DIR_RELATIVE'));

    }

    public function view()
    {
        $id = $this->request->param('id');
        $news = $this->New->getNewById($id);
        $news_author_id = $news['author_id'];
        $author = $this->New->getAuthorByAuthorId($news_author_id);
        $news['author_name'] = $author;
        $news['days_later'] = days_later(time() - strtotime($news['created']));

        $this->set("new", $news);
    }

    public function edit_new()
    {
        $new_id = $this->request->param('id') ?? null;
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            if ($new_id == null) {
                $this->Flash->set(__("Не задан идентификатор новости"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $news = $this->New->getNewById($new_id);
            if ($news == null) {
                $this->Flash->set(__("Новость с таким id ($new_id) не найдена"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $new_create = $this->New->updateNews($new_id, $data);

            $this->City->updateCityByNews($new_id, $data['cities'] ?? []);

            $image_name = "new_image";
            $file_max_size = $this->Content->image_params['max_size_mb'];
            if ((isset($_FILES[$image_name])) AND ($_FILES[$image_name]['size'] > 0) AND (!empty($_FILES[$image_name]['name']))) {
                if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                    $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                }
                //width & height
                $res = $this->New->saveNewImage($_FILES[$image_name],
                    $this->Content->image_params['default_image_width'],
                    $this->Content->image_params['default_image_height'],
                    $new_id);
                if ($res['status'] == 'ok') {
                    $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash']);
                } else {
                    $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash']);
                }
            } else {
                $this->Flash->set(__("Файла нет: "), ['element' => 'flash']);
            }

            $comment = 'Изменение новости с id[#id] и названием [#title]';
            $activity['id'] = $new_id;
            $activity['title'] = $data['title'];
            $this->Activity->add("news", $this->Admin->manager_id(), $comment, $activity);

            if (!empty($new_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $new_create["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $this->Flash->set(__("Сохранено "), ['element' => 'flash']);
                $this->redirect("/news/edit/" . $new_id);
            }
        } else {
            if ($new_id == null) {
                $this->Flash->set(__("Не задан идентификатор новости"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }
            $new = $this->New->getNewById($new_id);
            if ($new == null) {
                $this->Flash->set(__("Новость с таким id ($new_id) не найдена"));
                return $this->redirect($this->referer());
            }

            $news_author_id = $new['author_id'];
            $author = $this->New->getAuthorByAuthorId($news_author_id);
            $new['author_name'] = $author;

            $this->set('platforms', $this->Platform->platforms());

            $news_cities = $this->City->getCityByNews($new_id);
            $city_arr = [];
            foreach ($news_cities as $news_city){
                $city_arr[] = $news_city['City']['id'];
            }
            $this->set('news_cities', $city_arr);
            $this->set('title', "Новость №$new_id");
            $this->set('new', $new);

            $this->set('cities', $this->City->getCityList());
            $this->set('new_platforms', $this->Platform->platformsToArray($new['platforms']));
        }
    }

    public function add_new()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data ?? null;
            if ($data == null) {
                $this->Flash->set(__("Нет данных"), ['element' => 'flash']);
                return $this->redirect($this->referer());
            }

            $news_create = $this->New->createNew($data);

            if (!empty($news_create["error"]) > 0) {
                $this->Flash->set(__("Ошибка " . $news_create["error"]), ['element' => 'flash']);
                return $this->redirect($this->referer());
            } else {
                $id = $news_create;

                $this->City->updateCityByNews($id, $data['cities']);

                $file_max_size = $this->Content->image_params['max_size_mb'];

                $image_name = "new_image";
                if ((isset($_FILES[$image_name])) AND ($_FILES[$image_name]['size'] > 0) AND (!empty($_FILES[$image_name]['name']))) {
                    if ($_FILES[$image_name]['size'] > $file_max_size * 1024 * 1024) {
                        $this->Flash->set(__("Файл больше " . $file_max_size . " MB"), ['element' => 'flash']);
                    }
                    //width & height
                    $res = $this->New->saveNewImage($_FILES[$image_name],
                        $this->Content->image_params['default_image_width'],
                        $this->Content->image_params['default_image_height'],
                        $id);
                    if ($res['status'] == 'ok') {
                        $this->Flash->set(__("Файл успешно сохранен "), ['element' => 'flash']);
                    } else {
                        $this->Flash->set(__("Ошибка при сохранении файла: " . $res['msg']), ['element' => 'flash']);
                    }
                } else {
                    $this->Flash->set(__("Файла нет: "), ['element' => 'flash']);
                }

                $comment = 'Создание новости с id[#id] и названием [#title]';
                $activity['id'] = $id;
                $activity['title'] = $data['title'];
                $this->Activity->add("news", $this->Admin->manager_id(), $comment, $activity);

                $this->redirect("/news/view/" . $id);
            }
        } else {
            $this->set('title', "Добавление новости");
            $cities = $this->City->getCityList();
            $this->set('cities', $cities);
            $this->set('platforms', $this->Platform->platforms());
        }
    }

    public function delete_new()
    {
        $id = $this->request->param('id') ?? null;
        if ($id == null) {
            $this->Flash->set(__("Некорректный идентификатор новости"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $delete = $this->New->deleteNew($id);
        if (!empty($delete["error"]) > 0) {
            $this->Flash->set(__("Ошибка " . $delete["error"]), ['element' => 'flash']);
            return $this->redirect($this->referer());
        } else {

            $comment = 'Удаление новости с id[#id]';
            $activity['id'] = $id;
            $this->Activity->add("news", $this->Admin->manager_id(), $comment, $activity);

            $this->redirect("/news");
        }
        exit;
    }

    public function block_new()
    {
        $id = $this->request->param('id');
        if ($this->New->getNewById($id) == null) {
            $this->Flash->set(__("Новость с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->New->blockNew($id);

        $comment = 'Отключение из показа новости с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("news", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Новость скрыта из показа на сайт"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }

    public function unblock_new()
    {
        $id = $this->request->param('id');
        if ($this->New->getNewById($id) == null) {
            $this->Flash->set(__("Новость с таким id ($id) не найдена"), ['element' => 'flash']);
            return $this->redirect($this->referer());
        }
        $this->New->unblockNew($id);

        $comment = 'Открытие для показа новости с id[#id]';
        $activity['id'] = $id;
        $this->Activity->add("news", $this->Admin->manager_id(), $comment, $activity);

        $this->Flash->set(__("Новость открыта для показа на сайт"), ['element' => 'flash']);
        return $this->redirect($this->referer());
    }


}