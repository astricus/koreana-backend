<?php

if (APP_MODE == "SITE") {

    // сервисные роуты
    Router::connect('/parse/street', ['controller' => 'Service', 'action' => 'streetParser']);

    $api_prefix = "api";
    Router::connect('/', ['controller' => 'Index', 'action' => 'index']);
    Router::connect('/' . $api_prefix . '/:api_version/', ['controller' => 'Index', 'action' => 'index']);

    //короткая ссылка
    Router::connect('/' . $api_prefix . '/:api_version/short/:hash', ['controller' => 'User', 'action' => 'shortlink']);
    Router::connect('/' . $api_prefix . '/:api_version/short/', ['controller' => 'User', 'action' => 'shortlink']);

    //Router::connect('/' . $api_prefix . '/:api_version/categories/:category_id/products', array('controller' => 'ProductCategory', 'action' => 'category_filter'));
    Router::connect(
        '/' . $api_prefix . '/:api_version/categories/:category_id/products',
        ['controller' => 'Product', 'action' => 'searchProductsInCat']
    );

    //данные категории + фильтры
    Router::connect(
        '/' . $api_prefix . '/:api_version/categories/:category_id',
        ['controller' => 'ProductCategory', 'action' => 'getCategoryInfoById']
    );

    // категории товаров - подкатегории
    Router::connect(
        '/' . $api_prefix . '/:api_version/categories',
        ['controller' => 'ProductCategory', 'action' => 'category_list']
    );

    // Поиск по тексту
    Router::connect('/' . $api_prefix . '/:api_version/search', ['controller' => 'Product', 'action' => 'search']);

    Router::connect(
        '/' . $api_prefix . '/:api_version/search/redirect/:search_id/product/:product_id',
        ['controller' => 'Product', 'action' => 'collectSearchPreferProduct']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/search/redirect/:search_id/brand/:brand_id',
        ['controller' => 'Product', 'action' => 'collectSearchPreferBrand']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/search/redirect/:search_id/category/:category_id',
        ['controller' => 'Product', 'action' => 'collectSearchPreferCategory']
    );

    Router::connect(
        '/' . $api_prefix . '/:api_version/search/bigdata',
        ['controller' => 'Product', 'action' => 'searchBigData']
    );

    Router::connect(
        '/' . $api_prefix . '/:api_version/brand_search/',
        ['controller' => 'Product', 'action' => 'findElasticBrandName']
    );

    Router::connect(
        '/' . $api_prefix . '/:api_version/testmysql',
        ['controller' => 'Product', 'action' => 'testmysql']
    );

    // запрос актуальной цены в предложении товара поставщика в случае если возможен парсинг со страницы товара
    Router::connect(
        '/' . $api_prefix . '/:api_version/product/parse/price/:shop_product_id',
        ['controller' => 'Product', 'action' => 'realTimeParserProductPrice']
    );

    // фильтр категорий
    //Router::connect('/' . $api_prefix . '/:api_version/filter', array('controller' => 'ProductCategory', 'action' => 'category_filter'));

    //Категория - товары и подкатегории
    //Router::connect('/' . $api_prefix . '/:api_version/category/:id', array('controller' => 'ProductCategory', 'action' => 'view'));

    // также может потребоваться - список категорий
    Router::connect(
        '/' . $api_prefix . '/:api_version/categories/also-needed/:category_id',
        ['controller' => 'ProductCategory', 'action' => 'also_needed']
    );

    Router::connect(
        '/' . $api_prefix . '/:api_version/categories/popular/',
        ['controller' => 'ProductCategory', 'action' => 'popular_categories']
    );

    // Компания - ID
    Router::connect(
        '/' . $api_prefix . '/:api_version/companies/:company_id',
        ['controller' => 'Shop', 'action' => 'view']
    );

    //авторизация пользователя
    Router::connect('/' . $api_prefix . '/:api_version/user/auth/login', ['controller' => 'User', 'action' => 'login']);
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/auth/login_form',
        ['controller' => 'User', 'action' => 'login_form']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/auth/logout',
        ['controller' => 'User', 'action' => 'logout']
    );

    // регистрация
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/auth/register/form',
        ['controller' => 'User', 'action' => 'register_form']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/auth/registration',
        ['controller' => 'User', 'action' => 'register']
    );

    // пользовательские данные
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/get-data',
        ['controller' => 'User', 'action' => 'user_data']
    );

    // Избранное
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/favourite/list',
        ['controller' => 'User', 'action' => 'getFavouriteList']
    );
    // новый метод - удаляет категории user/favourite/category/delete
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/favourite/category/delete',
        ['controller' => 'User', 'action' => 'deleteProducsInCategoryFromFavourite']
    );

    Router::connect(
        '/' . $api_prefix . '/:api_version/user/favourite/product/add',
        ['controller' => 'User', 'action' => 'addProductToFavourite']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/favourite/clear/products/ ',
        ['controller' => 'User', 'action' => 'clearProducts']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/favourite/clear',
        ['controller' => 'User', 'action' => 'clearFavourite']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/favourite/product/delete',
        ['controller' => 'User', 'action' => 'deleteProductFromFavourite']
    );

    //оформление заказа и корзина
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/compare/category/list/:category_id',
        ['controller' => 'User', 'action' => 'getCompareListByCategory']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/compare/list',
        ['controller' => 'User', 'action' => 'getCompareCategories']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/compare/product/add',
        ['controller' => 'User', 'action' => 'addProductToCompare']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/compare/product/delete',
        ['controller' => 'User', 'action' => 'deleteProductFromCompare']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/compare/category/delete',
        ['controller' => 'User', 'action' => 'deleteCategoryFromCompare']
    );

    //сравнение товаров
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/cart/list',
        ['controller' => 'User', 'action' => 'getCartList']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/cart/offer/add',
        ['controller' => 'User', 'action' => 'addProductToCart']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/cart/offer/amount',
        ['controller' => 'User', 'action' => 'changeAmountProductInCart']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/cart/offer/change-company',
        ['controller' => 'User', 'action' => 'changeOfferCompanyInCart']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/cart/order',
        ['controller' => 'User', 'action' => 'makeOrder']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/cart/clear',
        ['controller' => 'User', 'action' => 'clearCart']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/cart/offer/delete',
        ['controller' => 'User', 'action' => 'deleteProductFromCart']
    );

    //Профиль пользователя
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/profile/change-password',
        ['controller' => 'User', 'action' => 'changePassword']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/auth/reset-password-request',
        ['controller' => 'User', 'action' => 'resetPasswordSendNew']
    );
    //Router::connect('/' . $api_prefix . '/:api_version/user/auth/reset-password-init', array('controller' => 'User', 'action' => 'resetPassword'));
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/profile/upload-photo',
        ['controller' => 'User', 'action' => 'uploadAvatarPhoto']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/profile/delete-photo',
        ['controller' => 'User', 'action' => 'deleteAvatarPhoto']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/profile/save',
        ['controller' => 'User', 'action' => 'editProfile']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/profile/subscription',
        ['controller' => 'User', 'action' => 'saveSubscription']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/get-data',
        ['controller' => 'User', 'action' => 'getUserData']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/last-view',
        ['controller' => 'User', 'action' => 'lastView']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/profile/confirm/phone/send-code',
        ['controller' => 'User', 'action' => 'sendSmsConfirmPhone']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/user/profile/confirm/phone/confirm-code',
        ['controller' => 'User', 'action' => 'confirmPhone']
    );

    // отзывы, рейтинг товаров - создание
    Router::connect(
        '/' . $api_prefix . '/:api_version/product/:product_id/comments/add',
        ['controller' => 'Product', 'action' => 'addProductComment']
    );
    //товары
    Router::connect('/' . $api_prefix . '/:api_version/product/:id', ['controller' => 'Product', 'action' => 'view']);

    Router::connect('/products', ['controller' => 'Product', 'action' => 'index']);
    Router::connect('/products/search/', ['controller' => 'Product', 'action' => 'search']);
    Router::connect('/product/view/:id', ['controller' => 'Product', 'action' => 'view']);

    Router::connect(
        '/' . $api_prefix . '/:api_version/product_list',
        ['controller' => 'Product', 'action' => 'productListApi']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/search/products',
        ['controller' => 'Product', 'action' => 'searchProducts']
    );

    //магазины
    Router::connect('/shop/:id', ['controller' => 'Shop', 'action' => 'view']);

    //запуск очереди парсинга цен
    Router::connect(
        '/' . $api_prefix . '/:api_version/parsing_price',
        ['controller' => 'Product', 'action' => 'parsingProductPrice']
    );

    //авторизация компаний
    Router::connect(
        '/' . $api_prefix . '/:api_version/companies/auth/login',
        ['controller' => 'Company', 'action' => 'login']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/companies/auth/login_form',
        ['controller' => 'Company', 'action' => 'login_form']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/companies/auth/registration',
        ['controller' => 'Company', 'action' => 'register']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/companies/auth/registration_form',
        ['controller' => 'Company', 'action' => 'register_form']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/companies/auth/reset-password',
        ['controller' => 'Company', 'action' => 'reset_password']
    );
    Router::connect(
        '/' . $api_prefix . '/:api_version/companies/auth/reset-password_form',
        ['controller' => 'Company', 'action' => 'reset_password_form']
    );

} else {
    if (APP_MODE == "API") {
        Router::connect('/', ['controller' => 'index', 'action' => 'statistic']);
        Router::connect('/index', ['controller' => 'index', 'action' => 'statistic']);
        // тестовые методы
        Router::connect('/recreate_clients', ['controller' => 'index', 'action' => 'recreate_clients']);
        Router::connect('/update_car_base', ['controller' => 'index', 'action' => 'update_car_base']);


        Router::connect('/mobile', ['controller' => 'mobile', 'action' => 'mobile_log']);
        Router::connect('/mobile/view/:id', ['controller' => 'mobile', 'action' => 'mobile_view']);
        Router::connect('/mobile/requests_by_ip/:ip', ['controller' => 'mobile', 'action' => 'requests_by_ip']);
        Router::connect('/mobile/blocks_by_ip/:ip', ['controller' => 'mobile', 'action' => 'blocks_by_ip']);
        Router::connect('/mobile/delete/:id', ['controller' => 'mobile', 'action' => 'delete_mobile']);

        Router::connect('/mobile/block_ip/:ip', ['controller' => 'mobile', 'action' => 'block_ip']);
        Router::connect('/mobile/unblock_ip/:ip', ['controller' => 'mobile', 'action' => 'unblock_ip']);

        Router::connect('/test_email', ['controller' => 'index', 'action' => 'test_email']);
        Router::connect('/welcome_screens', ['controller' => 'site_data', 'action' => 'screen_list']);
        Router::connect('/welcome_screen/add', ['controller' => 'site_data', 'action' => 'add_screen']);
        Router::connect('/welcome_screen/view/:id', ['controller' => 'site_data', 'action' => 'view_screen']);
        Router::connect('/welcome_screen/edit/:id', ['controller' => 'site_data', 'action' => 'edit_screen']);
        Router::connect('/welcome_screen/block/:id', ['controller' => 'site_data', 'action' => 'block_screen']);
        Router::connect('/welcome_screen/unblock/:id', ['controller' => 'site_data', 'action' => 'unblock_screen']);
        Router::connect('/welcome_screen_uploader', ['controller' => 'site_data', 'action' => 'screen_uploader']);

        Router::connect('/site_data', ['controller' => 'site_data', 'action' => 'data_list']);
        Router::connect('/data_field/add', ['controller' => 'site_data', 'action' => 'add_data_field']);
        Router::connect('/data_field/edit/:id', ['controller' => 'site_data', 'action' => 'edit_data_field']);
        Router::connect('/data_field/delete/:id', ['controller' => 'site_data', 'action' => 'delete_data_field']);

        Router::connect('/index/login', ['controller' => 'index', 'action' => 'login']);
        Router::connect('/login', ['controller' => 'index', 'action' => 'login']);
        Router::connect('/passreminder', ['controller' => 'index', 'action' => 'passreminder']);
        Router::connect('/test_post', ['controller' => 'index', 'action' => 'test_post']);
        Router::connect('/sw_test', ['controller' => 'index', 'action' => 'sw_test']);
        Router::connect('/test_push', ['controller' => 'index', 'action' => 'test_push']);
        Router::connect('/subscribe', ['controller' => 'index', 'action' => 'subscribe']);

        Router::connect('/task/list', ['controller' => 'task', 'action' => 'task_list']);
        Router::connect('/task/edit/:id', ['controller' => 'task', 'action' => 'edit_task']);
        Router::connect('/task/add', ['controller' => 'task', 'action' => 'add_task']);
        Router::connect('/task/delete/:id', ['controller' => 'task', 'action' => 'delete_task']);

        //push
        Router::connect('/push/create', ['controller' => 'push', 'action' => 'create_push']);
        Router::connect('/push_list', ['controller' => 'push', 'action' => 'push_list']);
        Router::connect('/push/list', ['controller' => 'push', 'action' => 'push_list']);
        Router::connect('/push/view/:id', ['controller' => 'push', 'action' => 'view_push']);
        Router::connect('/push/edit/:id', ['controller' => 'push', 'action' => 'edit_push']);
        Router::connect('/push/delete/:id', ['controller' => 'push', 'action' => 'delete_push']);
        Router::connect('/push/send_now/:id', ['controller' => 'push', 'action' => 'send_push']);
        Router::connect('/push/users/:id', ['controller' => 'push', 'action' => 'push_users']);
        Router::connect('/push/add_receiver/:push_id', ['controller' => 'push', 'action' => 'add_receiver']);
        Router::connect('/push/send_push_to_user/:push_id/:sent_id', ['controller' => 'push', 'action' => 'send_push_to_user']);
        Router::connect('/push/user/delete/:push_id/:push_sent_id', ['controller' => 'push', 'action' => 'delete_receiver']);

        Router::connect('/request_1c/get_user_cars/:user_id', ['controller' => 'request1c', 'action' => 'get_user_cars']);
        Router::connect('/request_1c/order_list', ['controller' => 'request1c', 'action' => 'order_list']);

        Router::connect('/test_1c_model', ['controller' => 'index', 'action' => 'test_1c_model']);

        Router::connect('/sms_sender', ['controller' => 'index', 'action' => 'sms_sender']);

        //пользователи
        Router::connect('/user/:id', ['controller' => 'user', 'action' => 'profile']);
        Router::connect('/users', ['controller' => 'user', 'action' => 'user_list']);

        //привязки кодов смс
        Router::connect('/user_codes', ['controller' => 'user', 'action' => 'user_codes']);
        Router::connect('/create_user_code', ['controller' => 'user', 'action' => 'create_user_code']);
        Router::connect('/delete_user_code/:id', ['controller' => 'user', 'action' => 'delete_user_code']);

        //администраторы

        Router::connect('/access_forbidden', ['controller' => 'app', 'action' => 'access_forbidden']);
        Router::connect('/admin/role/update', ['controller' => 'admin', 'action' => 'update_role']);
        Router::connect('/admin/roles', ['controller' => 'admin', 'action' => 'roles']);
        Router::connect('/admin/access_preprocess', ['controller' => 'admin', 'action' => 'preProcessAccessList']);

        Router::connect('/admin/add/', ['controller' => 'admin', 'action' => 'add']);
        Router::connect('/admin/edit/:id', ['controller' => 'admin', 'action' => 'edit']);
        Router::connect('/admin/view/:profile', ['controller' => 'admin', 'action' => 'profile']);
        Router::connect('/admins', ['controller' => 'admin', 'action' => 'index']);

        Router::connect('/admin/block/:id', ['controller' => 'admin', 'action' => 'block']);
        Router::connect('/admin/unblock/:id', ['controller' => 'admin', 'action' => 'unblock']);
        Router::connect('/admin/reset_pass/:id', ['controller' => 'admin', 'action' => 'resetPassword']);

        Router::connect("/admin/activity", ['controller' => 'Activity', 'action' => 'index']);

        Router::connect('/restapi/doc/index', ['controller' => 'manage', 'action' => 'documentation']);
        Router::connect('/restapi/request/add', ['controller' => 'manage', 'action' => 'addApiRequest']);
        Router::connect('/restapi', ['controller' => 'manage', 'action' => 'restapi']);

        Router::connect('/news', ['controller' => 'news', 'action' => 'news_list']);
        Router::connect('/news/view/:id', ['controller' => 'news', 'action' => 'view']);
        Router::connect('/news/edit/:id', ['controller' => 'news', 'action' => 'edit_new']);
        Router::connect('/news/add', ['controller' => 'news', 'action' => 'add_new']);
        Router::connect('/news/delete/:id', ['controller' => 'news', 'action' => 'delete_new']);
        Router::connect('/news/update/:id', ['controller' => 'news', 'action' => 'update_new']);
        Router::connect('/news/block/:id', ['controller' => 'news', 'action' => 'block_new']);
        Router::connect('/news/unblock/:id', ['controller' => 'news', 'action' => 'unblock_new']);

        Router::connect('/services', ['controller' => 'service', 'action' => 'services']);
        Router::connect('/service/view/:id', ['controller' => 'service', 'action' => 'view']);
        Router::connect('/service/edit/:id', ['controller' => 'service', 'action' => 'edit']);
        Router::connect('/service/delete/:id', ['controller' => 'service', 'action' => 'delete']);
        Router::connect('/service/add', ['controller' => 'service', 'action' => 'add']);
        Router::connect('/service/block/:id', ['controller' => 'service', 'action' => 'block']);
        Router::connect('/service/unblock/:id', ['controller' => 'service', 'action' => 'unblock']);

        //static_page
        Router::connect('/static_pages', ['controller' => 'static_page', 'action' => 'page_list']);
        Router::connect('/static_page/view/:id', ['controller' => 'static_page', 'action' => 'view']);
        Router::connect(
            '/static_page/find_page_by_name/',
            ['controller' => 'static_page', 'action' => 'find_page_by_name']
        );

        Router::connect('/static_page/category/view/:id', ['controller' => 'static_page', 'action' => 'category_view']);
        Router::connect(
            '/static_page/category/create/:id',
            ['controller' => 'static_page', 'action' => 'add_category']
        );
        Router::connect(
            '/static_page/category/rename/:id',
            ['controller' => 'static_page', 'action' => 'rename_category']
        );
        Router::connect(
            '/static_page/category/remove/:id',
            ['controller' => 'static_page', 'action' => 'remove_category']
        );
        Router::connect(
            '/static_page/category/delete/:id',
            ['controller' => 'static_page', 'action' => 'delete_category']
        );

        Router::connect('/static_page/edit/:id', ['controller' => 'static_page', 'action' => 'edit_page']);
        Router::connect('/static_page/add', ['controller' => 'static_page', 'action' => 'add_page']);
        Router::connect('/static_page/delete/:id', ['controller' => 'static_page', 'action' => 'delete_page']);
        Router::connect('/static_page/update/:id', ['controller' => 'static_page', 'action' => 'update_page']);
        Router::connect('/static_page/block/:id', ['controller' => 'static_page', 'action' => 'block_page']);
        Router::connect('/static_page/unblock/:id', ['controller' => 'static_page', 'action' => 'unblock_page']);

        //DATA PROVIDERS
        Router::connect('/data_providers', ['controller' => 'data_provider', 'action' => 'providers']);
        Router::connect('/data_provider/add', ['controller' => 'data_provider', 'action' => 'add_provider']);
        Router::connect('/data_provider/edit/:id', ['controller' => 'data_provider', 'action' => 'edit_provider']);

        Router::connect('/data_provider/add_item', ['controller' => 'data_provider', 'action' => 'add_item']);
        Router::connect('/data_provider/edit_item', ['controller' => 'data_provider', 'action' => 'edit_item']);
        Router::connect('/data_provider/delete_item', ['controller' => 'data_provider', 'action' => 'delete_item']);
        Router::connect('/data_provider/block/:id', ['controller' => 'data_provider', 'action' => 'block']);
        Router::connect('/data_provider/unblock/:id', ['controller' => 'data_provider', 'action' => 'unblock']);

        //WEB FORMS
        Router::connect('/webform/reports', ['controller' => 'webform', 'action' => 'reports']);
        Router::connect('/webform/report/view/:id', ['controller' => 'webform', 'action' => 'view_report']);
        Router::connect(
            '/webform/report/status/:id/:status',
            ['controller' => 'webform', 'action' => 'update_report_status']
        );

        Router::connect('/webform/fill/:id', ['controller' => 'webform', 'action' => 'fill_form']);
        Router::connect('/webforms', ['controller' => 'webform', 'action' => 'webforms']);
        Router::connect('/webform/view/:id', ['controller' => 'webform', 'action' => 'view_webform']);
        Router::connect('/webform/edit/:id', ['controller' => 'webform', 'action' => 'edit_webform']);
        Router::connect('/webform/save_fields/:id', ['controller' => 'webform', 'action' => 'save_fields']);

        Router::connect('/webform/add', ['controller' => 'webform', 'action' => 'add_webform']);
        Router::connect('/webform/delete/:id', ['controller' => 'webform', 'action' => 'delete_webform']);
        Router::connect('/webform/update/:id', ['controller' => 'webform', 'action' => 'update_webform']);
        Router::connect('/webform/block/:id', ['controller' => 'webform', 'action' => 'block_webform']);
        Router::connect('/webform/unblock/:id', ['controller' => 'webform', 'action' => 'unblock_webform']);
        Router::connect(
            '/webform/connect/category/:id',
            ['controller' => 'webform', 'action' => 'connect_to_category']
        );
        Router::connect('/webform/connect/page/:id', ['controller' => 'webform', 'action' => 'connect_to_page']);
        Router::connect(
            '/webform/disconnect/category/:id',
            ['controller' => 'webform', 'action' => 'disconnect_from_category']
        );
        Router::connect(
            '/webform/disconnect/page/:id',
            ['controller' => 'webform', 'action' => 'disconnect_from_page']
        );
        Router::connect('/webform/remove_connect/:id', ['controller' => 'webform', 'action' => 'remove_connect']);

        Router::connect('/static_uploader', ['controller' => 'static_page', 'action' => 'static_uploader']);
        Router::connect('/static_page/categories', ['controller' => 'static_page', 'action' => 'category_list']);

        Router::connect('/actions', ['controller' => 'action', 'action' => 'actions']);
        Router::connect('/action/view/:id', ['controller' => 'action', 'action' => 'view']);
        Router::connect('/action/edit/:id', ['controller' => 'action', 'action' => 'edit']);
        Router::connect('/action/delete/:id', ['controller' => 'action', 'action' => 'delete']);
        Router::connect('/action/add', ['controller' => 'action', 'action' => 'add']);
        Router::connect('/action/block/:id', ['controller' => 'action', 'action' => 'block']);
        Router::connect('/action/unblock/:id', ['controller' => 'action', 'action' => 'unblock']);

        Router::connect('/locations', ['controller' => 'location', 'action' => 'locations']);
        #Router::connect('/location/view/:id', array('controller' => 'location', 'action' => 'view'));

        Router::connect('/location/map_ajax/', ['controller' => 'location', 'action' => 'map_ajax']);
        Router::connect('/location/add_type', ['controller' => 'location', 'action' => 'create_type']);
        Router::connect('/location/delete_type/:id', ['controller' => 'location', 'action' => 'delete_type']);

        Router::connect('/location/edit/:id', ['controller' => 'location', 'action' => 'edit']);
        Router::connect('/location/delete/:id', ['controller' => 'location', 'action' => 'delete']);
        Router::connect('/location/add', ['controller' => 'location', 'action' => 'add']);
        Router::connect('/location/block/:id', ['controller' => 'location', 'action' => 'block']);
        Router::connect('/location/unblock/:id', ['controller' => 'location', 'action' => 'unblock']);
        Router::connect('/location/saveXML', ['controller' => 'location', 'action' => 'saveXML']);

        //STO
        Router::connect('/sto', ['controller' => 'sto', 'action' => 'record_list']);
        Router::connect('/sto/view/:id', ['controller' => 'sto', 'action' => 'record']);

        //CAR
        Router::connect('/cars', ['controller' => 'car', 'action' => 'users_cars']);
        Router::connect('/car/brands', ['controller' => 'car', 'action' => 'brands']);
        Router::connect('/car/brand/view/:id', ['controller' => 'car', 'action' => 'view_brand']);
        Router::connect('/car/model/view/:id', ['controller' => 'car', 'action' => 'view_model']);

        Router::connect('/car/brand/add', ['controller' => 'car', 'action' => 'add_brand']);
        Router::connect('/car/brand/edit/:id', ['controller' => 'car', 'action' => 'edit_brand']);
        Router::connect('/car/model/add', ['controller' => 'car', 'action' => 'add_model']);
        Router::connect('/car/model/edit/:id', ['controller' => 'car', 'action' => 'edit_model']);

        Router::connect('/car/brand/delete/;id', ['controller' => 'car', 'action' => 'delete_brand']);
        Router::connect('/car/model/delete/:id', ['controller' => 'car', 'action' => 'delete_brand']);

        Router::connect('/car/brand/block/:id', ['controller' => 'car', 'action' => 'block_brand']);
        Router::connect('/car/brand/unblock/:id', ['controller' => 'car', 'action' => 'unblock_brand']);
        Router::connect('/car/model/block/:id', ['controller' => 'car', 'action' => 'block_model']);
        Router::connect('/car/model/unblock/:id', ['controller' => 'car', 'action' => 'unblock_model']);

        Router::connect('/car/brand/set_popular/:id', ['controller' => 'car', 'action' => 'set_popular_brand']);
        Router::connect('/car/brand/set_not_popular/:id', ['controller' => 'car', 'action' => 'set_not_popular_brand']);
        Router::connect('/car/model/set_popular/:id', ['controller' => 'car', 'action' => 'set_popular_model']);
        Router::connect('/car/model/set_not_popular/:id', ['controller' => 'car', 'action' => 'set_not_popular_model']);

        //API
        Router::connect(
            '/api/:version/:platform/:entity/:id/:method',
            ['controller' => 'api', 'action' => 'apiMethod']
        );
        Router::connect('/api/:version/:platform/:entity/:method', ['controller' => 'api', 'action' => 'apiMethod']);

        //HIVE TRASH

        //Sandbox API
        // общая песочница в проекте
        Router::connect('/manage/:project_id/sandbox', ['controller' => 'manage', 'action' => 'sandbox']);

        Router::connect(
            '/manage/sandbox/:api_component/:api_method/run',
            ['controller' => 'manage', 'action' => 'runApiSandbox']
        );
        Router::connect(
            '/manage/sandbox/:api_component/:api_method',
            ['controller' => 'manage', 'action' => 'testApiSandbox']
        );
        Router::connect('/manage/api_result_template/', ['controller' => 'manage', 'action' => 'apiResultTemplate']);

        //GATE - непосредственный запуск api интерфейсов

        Router::connect('/api/info', ['controller' => 'api', 'action' => 'info']);
        Router::connect('/api/auth', ['controller' => 'api', 'action' => 'auth']);
        // запуск метогда базового компонента
        Router::connect('/api/:version_api/base/:component/:method/', ['controller' => 'api', 'action' => 'runBase']);
        Router::connect(
            '/api/:version_api/script/:project_id/:script_id/',
            ['controller' => 'api', 'action' => 'runProject']
        );

        /*
            Router::connect('/api/:version_api/:token/:project_id/:api_item_id', array('controller' => 'api', 'action' => 'run'));
            Router::connect('/api/:version_api/:token/:project_id/:api_item_id', array('controller' => 'api', 'action' => 'run'));
            Router::connect('/api/:version_api/:token/:project_id/:api_item_id/stop', array('controller' => 'api', 'action' => 'stop'));
            Router::connect('/api/:version_api/:token/:project_id/:api_item_id/start', array('controller' => 'api', 'action' => 'start'));
            Router::connect('/api/:version_api/:token/:project_id/:api_item_id/update', array('controller' => 'api', 'action' => 'update'));
            Router::connect('/api/:version_api/:token/:project_id/:api_item_id/status', array('controller' => 'api', 'action' => 'status'));
            Router::connect('/api/:version_api/:token/:project_id/:api_item_id/log', array('controller' => 'api', 'action' => 'get_log'));*/

        Router::connect('/api/*', ['controller' => 'api', 'action' => 'undefined_method']);

        //User Api админка
        #Router::connect('/user/list', array('controller' => 'User', 'action' => 'list'));

        Router::connect('/phpinfo', ['controller' => 'index', 'action' => 'phpinfo']);

        Router::connect('/index/logout', ['controller' => 'index', 'action' => 'logout']);
        Router::connect('/logout', ['controller' => 'index', 'action' => 'logout']);

    }
}

CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';

