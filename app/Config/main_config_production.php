<?php

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('UDS')) {
    define('UDS', "\\");
}

if (!defined('DEF_MAIL')) {
    define('DEF_MAIL', 'app@koreanagroup.ru');
}

$config = [

    'JSON_CACHE_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'api_cache',

    'DEF_LOCALE' => 'eng',

    'VALID_LANGS' => ['RU', 'EN'],

    'VALID_LANG_LOCALES' => ['rus', 'eng'],

    //общие
    'SITENAME' => 'Koreana',

    'SITE_URL' => 'https://koreana.site/',

    'SITEPHONE' => '',

    'DEF_LANG' => 'RU',

    //записывается ли лог ошибок
    'ERROR_LOG_ACTIVE' => true,

    'IS_CLOUDFLARE' => "N",

    //почта сайта
    'SITE_MAIL' => DEF_MAIL,

    'CAN_SENT_EMAIL' => false,

    //Режим работы сайта
    'SITE_STATUS' => 'ACTIVE',

    //security
    'USER_AUTH_SALT' => md5('abrakadabra123'),

    'ADMIN_AUTH_SALT' => md5('abrakadabra123'),

    'MAIL_KEY_SALT' => md5('12312321321'),

    'SMS_PILOT_API_KEY' => '528WL1T0R0YC12282T4XOX463NLJ2LJIST5QVNG48Z44W5TIL87F6U68ZN2PYGI4',

    //администратор
    //почта администратора
    'ADMIN_MAIL' => DEF_MAIL,

    'SMTP_CONFIG' => [
        'port' => '465',
        'timeout' => '30',
        'auth' => true,
        'host' => 'ssl://smtp.gmail.com',
        'username' => '{GMAIL_USER}@gmail.com',
        'password' => '{GMAIL_PASS}',
    ],

    'VALID_DEV_IPS' => [
        '127.0.0.1',
    ],

    //административная панель
    'ADMIN_PANEL' => 'Административная панель',
    'FILE_TEMP_DIR' => dirname(__DIR__) . DS . 'tmp' . DS . 'temp_image',

    //Директория загрузки изображений дизайна
    'DESIGN_IMAGE_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'img' . DS . 'design',
    'DESIGN_IMAGE_DIR_RELATIVE' => '/img' . "/" . 'design' . "/",

    //Директория загрузки акций и изображений
    'CONTENT_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'content',
    'CONTENT_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'content',

    'LOCATION_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'location',
    'LOCATION_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'location',

    //Директория загрузки иконок для push
    'PUSH_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'push',
    'PUSH_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'push',

    //Директория загрузки пользовательских изображений
    'USER_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'image_files',
    'USER_FILE_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'image_files',

    //Директория загрузки  изображений администратором и редактором
    'ADMIN_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'admin_files',

    //WEBROOT
    'WEBROOT' => dirname(__DIR__) . DS . 'webroot',

    //PHP Script
    'PHP_SCRIPT_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . "php",

    'API_LOG' => dirname(__DIR__) . DS . '..' . DS . 'logs' . DS . 'api.log',

    'YA_MAP_API_KEY' => 'a59eb2b4-2a17-4844-8ecb-06330c31c61f',

    //пользовательский конфиг загрузки изображений
    'USER_IMAGE_UPLOAD_CONFIG' => [
        'ext' => ["jpg", "jpeg", "png", "gif"],
        'max_file_size' => 5 * 1000 * 1000,
        'max_x' => 2000,
        'max_y' => 2000,
        'image' => 'true',
    ],

    "API_NAME" => 'KOREANA PRODUCTION API',

    '1C_GATE' => 'http://46.148.224.51:11000/SPB/hs/common_api/',

    'FIREBASE_API_KEY' =>
        'AAAA7naAsjs:APA91bEg1byLS2lUbiA6NTU0yNOPKCkBJa_mqX7q4RBXD8myIz4bIhEBfqU3St5nEAYFFORUCxy_l5tzw2THgwr5DybkdmXQo-ANmXhqpM-eI3tzPK_8_jXIWKQsOu2GTk34CCb9YfWg',

    'PUSH_API_VERSION' => 'old',

    'ACCESS_TOKEN_REDIRECT_URL' => 'https://test-rest-api.site/access_token',

    'GOOGLE_AUTH_CODE' => '4/0AX4XfWh2kLoxzNwIaUKn7_cUPLpVR3J0bgIjl3cku3AhrC7HJoGtYl3aj7B3siceVph8FA',

    'GOOGLE_APP_CLIENT_ID' => '179029517871-7dk2ufh8q5nuoj3tkfo84surmc20ml6k.apps.googleusercontent.com',

    'GOOGLE_APP_CLIENT_SECRET' => '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',
];