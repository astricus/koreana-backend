<?php

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('UDS')) {
    define('UDS', "/");
}

if (!defined('DEF_MAIL')) {
    define('DEF_MAIL', 'app@koreanagroup.ru');
}

$config = [

    'JSON_CACHE_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'api_cache',

    'DEF_LOCALE' => 'eng',

    'VALID_LANGS' => ['RU', 'EN'],

    'VALID_LANG_LOCALES' => ['rus', 'eng'],

    //общие
    'SITENAME' => 'Koreana',

    'SITE_URL' => 'https://test-rest-api.site/',

    'SITEPHONE' => '',

    'DEF_LANG' => 'RU',

    //записывается ли лог ошибок
    'ERROR_LOG_ACTIVE' => true,

    'IS_CLOUDFLARE' => "N",

    //почта сайта
    'SITE_MAIL' => DEF_MAIL,

    'CAN_SENT_EMAIL' => false,

    //Режим работы сайта
    'SITE_STATUS' => 'ACTIVE',

    //security
    'USER_AUTH_SALT' => md5('abrakadabra123'),

    'ADMIN_AUTH_SALT' => md5('abrakadabra123'),

    'MAIL_KEY_SALT' => md5('12312321321'),

    'SMS_PILOT_API_KEY' => '528WL1T0R0YC12282T4XOX463NLJ2LJIST5QVNG48Z44W5TIL87F6U68ZN2PYGI4',

    //администратор
    //почта администратора
    'ADMIN_MAIL' => DEF_MAIL,

    'SMTP_CONFIG' => [
        'port' => '465',
        'timeout' => '30',
        'auth' => true,
        'host' => 'ssl://smtp.gmail.com',
        'username' => '{GMAIL_USER}@gmail.com',
        'password' => '{GMAIL_PASS}',
    ],

    'VALID_DEV_IPS' => [
        '127.0.0.1',
    ],

    //административная панель
    'ADMIN_PANEL' => 'Административная панель',
    'FILE_TEMP_DIR' => dirname(__DIR__) . DS . 'tmp' . DS . 'upload',
    //'FILE_TEMP_DIR' => dirname(__DIR__) . DS . 'tmp' . DS . 'temp_image',

    //Директория загрузки изображений дизайна
    'DESIGN_IMAGE_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'img' . DS . 'design',
    'DESIGN_IMAGE_DIR_RELATIVE' => '/img' . "/" . 'design' . "/",

    //Директория загрузки пользовательских изображений
    'USER_FILE_UPLOAD_DIR' => dirname(
            __DIR__
        ) . DS . 'webroot' . DS . 'files' . DS . 'product_images',
    'USER_FILE_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'product_images',

    //Директория загрузки акций и изображений
    'CONTENT_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'content',
    'CONTENT_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'content',

    //Директория загрузки иконок для push
    'PUSH_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'push',
    'PUSH_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'push',

    //Директория загрузки контактов
    'LOCATION_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'location',
    'LOCATION_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'location',

    //Директория загрузки пользовательских изображений
    'PRODUCT_IMAGE_FILE_UPLOAD_DIR' => dirname(
            __DIR__
        ) . DS . 'webroot' . DS . 'files' . DS . 'product_images',
    'PRODUCT_IMAGE_FILE_UPLOAD_DIR_RELATIVE' => 'files' . "/" . 'product_images',

    //Директория загрузки  изображений администратором и редактором
    'ADMIN_FILE_UPLOAD_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . 'files' . DS . 'admin_files',

    //PHP Script
    'PHP_SCRIPT_DIR' => dirname(__DIR__) . DS . 'webroot' . DS . "php",

    //WEBROOT
    'WEBROOT' => dirname(__DIR__) . DS . 'webroot',

    //пользовательский конфиг загрузки изображений
    'USER_IMAGE_UPLOAD_CONFIG' => [
        'ext' => ["jpg", "jpeg", "png", "gif"],
        'max_file_size' => 5 * 1000 * 1000,
        'max_x' => 2000,
        'max_y' => 2000,
        'image' => 'true',
    ],

    'API_LOG' => dirname(__DIR__) . DS . '..' . DS . 'logs' . DS . 'api.log',

    'YA_MAP_API_KEY' => 'a59eb2b4-2a17-4844-8ecb-06330c31c61f',

    '1C_GATE' => 'http://46.148.224.51:11000/SPB/hs/common_api/',

    "API_NAME" => 'KOREANA LOCAL API',

    'FIREBASE_API_KEY' =>
        'AAAAKa79pi8:APA91bHsp8iueOpesnECuxAQmIXpc-p9utrQ55AOKbVarAQiAtWZJ3ssN_7pNy695ZlGbEQhr_RqD_yyl_rSxS99EVl3w0k3D9J_Gznv8xC5bbbGV2gJZdpJ8rjWbAU4djwqHPcRNkef',

    //'SMS_PILOT_API_KEY' => 'GOD86F2K6200Z2695T3O11VEX9SJI5AFP20I6TPPUG9NU94999VR9PO3G1C12EX3',

    'PUSH_API_VERSION' => 'new',

    'ACCESS_TOKEN_REDIRECT_URL' => 'https://test-rest-api.site/access_token',

    'GOOGLE_AUTH_CODE' => '4/0AX4XfWh2kLoxzNwIaUKn7_cUPLpVR3J0bgIjl3cku3AhrC7HJoGtYl3aj7B3siceVph8FA',

    'GOOGLE_APP_CLIENT_ID' => '179029517871-7dk2ufh8q5nuoj3tkfo84surmc20ml6k.apps.googleusercontent.com',

    'GOOGLE_APP_CLIENT_SECRET' => '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',

    'CLIENT_BASE_APP_NAME' => 'MOBILE',

    'MOBILE_APP_KEY' => 'Ttvh)*&#!_#g)&@R^gj57+',

    'CLIENT_BASE_URL' => 'http://client.test-rest-api.site/api/',
];