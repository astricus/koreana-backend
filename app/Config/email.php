<?php

/**
 * This is email configuration file.
 *
 * Use it to configure email transports of CakePHP.
 *
 * Email configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * transport => The name of a supported transport; valid options are as follows:
 *  Mail - Send using PHP mail function
 *  Smtp - Send using SMTP
 *  Debug - Do not send the email, just return the result
 *
 * You can add custom transports (or override existing transports) by adding the
 * appropriate file to app/Network/Email. Transports should be named 'YourTransport.php',
 * where 'Your' is the name of the transport.
 *
 * from =>
 * The origin email. See CakeEmail::from() about the valid values
 *
 */
class EmailConfig
{
    public $koreana    = [
        'from'          => 'app@koreanagroup.ru',
        'charset'       => 'utf-8',
        'headerCharset' => 'utf-8',
        'transport'     => 'Smtp',
        'host'          => 'mail.koreanagroup.ru',
        'port'          => 587,
        'timeout'       => 30,
        'username'      => 'app@koreanagroup.ru',
        'password'      => '0gwm0W78',
        'client'        => null,
        'log'           => true,
        'auth'          => true,
        'SMTPSecure'    => 'starttls',
        'tls'           => true,
        'context'       => [
            'ssl' => [
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true,
            ],
        ],
    ];

    public $fast       = [
        'from'          => 'you@localhost',
        'sender'        => null,
        'to'            => null,
        'cc'            => null,
        'bcc'           => null,
        'replyTo'       => null,
        'readReceipt'   => null,
        'returnPath'    => null,
        'messageId'     => true,
        'subject'       => null,
        'message'       => null,
        'headers'       => null,
        'viewRender'    => null,
        'template'      => false,
        'layout'        => false,
        'viewVars'      => null,
        'attachments'   => null,
        'emailFormat'   => null,
        'transport'     => 'Smtp',
        'host'          => 'localhost',
        'port'          => 25,
        'timeout'       => 30,
        'username'      => 'user',
        'password'      => 'secret',
        'client'        => null,
        'log'           => true,
        'charset'       => 'utf-8',
        'headerCharset' => 'utf-8',
    ];
}
