<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Машина
 */
class CarComponent extends Component
{
    public $components = array(
        'Log'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    public $driver_data_is_incorrect = "Данные водителя переданы некорректно";
    public $car_data_is_incorrect = "Данные машины переданы некорректно";
    public $car_type_data_is_incorrect = "Данные типа машины переданы некорректно";
    public $car_type_must_have_name = "Тип машины должен иметь название";
    public $car_not_found = "Машина не найдена";
    public $cargo_height_must_have_positive_value = "Высота кузова должна быть задана и быть непустой величиной";
    public $cargo_width_must_have_positive_value = "Ширина кузова должна быть задана и быть непустой величиной";
    public $cargo_length_must_have_positive_value = "Длина кузова должна быть задана и быть непустой величиной";
    public $unknown_or_empty_type_id = "Пустой тип машины или же переданный тип машины не был  найден";
    public $has_arc_must_be_boolean = "Поле has_arc (Наличие арок) должно быть логическим типом (boolean)";
    public $has_manipulator_must_be_boolean = "Поле has_manupulator (Наличие манипулятора) должно быть логическим типом (boolean)";
    public $incorrect_or_empty_car_number = "передан некорректный или пустой номер автомобиля";

    public $unknown_status = "Передан неизвестный статус";

    public function setup()
    {
        $modelName = "Car";
        $this->Car = ClassRegistry::init($modelName);
        $modelName = "Car_Type";
        $this->Car_Type = ClassRegistry::init($modelName);
        $modelName = "Delivery_Route";
        $this->Delivery_Route = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     * @return array|null
     */
    public function getCarById($id)
    {
        $this->setup();
        $car = $this->Car->find("first",
            array('conditions' =>
                array(
                    'Car.id' => $id
                ),
                'joins' => array(
                    array(
                        'table' => 'car_types',
                        'alias' => 'Car_Type',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Car.type_id = Car_Type.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Car_Type.*',
                    'Car.*'
                ),
            )
        );
        if (count($car) == 0) {
            return null;
        }
        return $car;
    }

    /**
     * @param $id
     * @return array
     */
    public function getCarTypeByTypeId($id)
    {
        $this->setup();
        $car_type = $this->Car_Type->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                ),
            )
        );
        if (count($car_type) == 0) {
            return null;
        }
        return $car_type;
    }

    /**
     * @param $id
     * @return array
     */
    public function getCarsByTypeId($id)
    {
        $this->setup();
        $cars = $this->Car->find("all",
            array('conditions' =>
                array(
                    'type_id' => $id
                ),
                'order' => array('id DESC')
            )
        );
        if (count($cars) == 0) {
            return null;
        }
        return $cars;
    }

    /**
     * @return array|null
     */
    public function getAllCars()
    {
        $this->setup();
        $cars = $this->Car->find("all",
            array('conditions' =>
                array(
                    //'status' => 'active'
                ),
                'joins' => array(
                    array(
                        'table' => 'car_types',
                        'alias' => 'Car_Type',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Car.type_id = Car_Type.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Car_Type.*',
                    'Car.*'
                ),
                'order' => array('Car.id DESC')
            )
        );
        if (count($cars) == 0) {
            return null;
        }
        return $cars;
    }

    public function getAllTypes()
    {
        $this->setup();
        $car_types = $this->Car_Type->find("all",
            array('conditions' =>
                array(
                    //'status' => 'active'
                ),
                'order' => array('id DESC')
            )
        );
        if (count($car_types) == 0) {
            return null;
        }
        return $car_types;
    }

    /**
     * @param $id
     * @return array
     */
    public function getActiveCars()
    {
        $this->setup();
        $cars = $this->Car->find("all",
            array('conditions' =>
                array(
                    'status' => 'active'
                ),
                'order' => array('id DESC')
            )
        );
        if (count($cars) == 0) {
            return null;
        }
        return $cars;
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool|array
     */
    public function setCarData($id, $data)
    {
        $validate = $this->validateCar($data);
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->car_data_is_incorrect . " " . $validate["error"] ];
        }
        $this->setup();
        $this->Car->id = $id;
        $this->Car->save($data);
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function setCarActive($id)
    {
        $this->setup();
        $data = ['status' => 'active'];
        $this->Car->id = $id;
        $this->Car->save($data);
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function setCarBlocked($id)
    {
        $data = ['status' => 'blocked'];
        $this->setup();
        $this->Car->id = $id;
        $this->Car->save($data);
    }

    /**
     * @param $width
     * @param $height
     * @param $length
     * @return float|int
     */
    public function calculateCarVolume($width, $height, $length)
    {
        $width = (float)$width;
        $height = (float)$height;
        $length = (float)$length;
        return $width * $height * $length;
    }

    /**
     * @param int $car_id
     * @param $test_weight
     * @return bool
     */
    public function isAvailableWeight($car_id, $test_weight)
    {
        $car_data = $this->getCarById($car_id);
        $type_id = $car_data['Car']['type_id'];
        $car_type_data = $this->getCarTypeByTypeId($type_id);
        $car_max_weight = $car_type_data['Car_Type']['max_weight'];
        if ($test_weight > $car_max_weight) {
            return false;
        }
        return true;
    }

    /**
     * @param $date
     * @return bool|null|mixed
     */
    public function getWorkingCarsByDate($date)
    {
        $this->setup();
        if (!$this->Validator->valid_date($date)) {
            return false;
        }
        $working_cars = $this->Delivery_Route->find("all",
            array('conditions' =>
                array(
                    'route_date' => $date
                )
            )
        );
        if (count($working_cars) == 0) {
            return null;
        }
        return $working_cars['Delivery_Route'];
    }

    /**
     * @param int $car_id
     * @return bool|null|mixed
     */
    public function getWorkingDateByCarId($car_id)
    {
        $this->setup();
        if ($this->getCarById($car_id)) {
            return false;

        }
        $working_dates = $this->Delivery_Route->find("all",
            array('conditions' =>
                array(
                    'car_id' => $car_id
                )
            )
        );
        if (count($working_dates) == 0) {
            return null;
        }
        return $working_dates['Delivery_Route'];
    }

    //--- create --saving

    /**
     * @param $data
     * @return array|null
     */
    public function createCarType($data)
    {
        $validate = $this->validateCarType($data);
        if ($validate['status'] == "success") {
            return ["status" => "error", "error" => $this->car_data_is_incorrect . " " . $validate["error"]];
        }
        $this->setup();
        //preformat data
        $max_volume = $this->calculateCarVolume($data['cargo_width'], $data['cargo_heigth'], $data['cargo_length']);
        $data['max_volume'] = $max_volume;
        $this->Car_Type->create();
        $this->Car_Type->save($data);
        if($this->Car_Type->id>0){
            return $this->Car_Type->id;
        }
        return null;
    }

    /**
     * @param $data
     * @return integer|array
     */
    public function createCar($data)
    {
        $validate = $this->validateCar($data);
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->car_data_is_incorrect . " " . $validate["error"] ];
        }
        $this->setup();

        $this->Car->create();
        $this->Car->save($data);
        if($this->Car->id>0){
            return $this->Car->id;
        }
        return null;
    }

    public function saveCar($car_id, $data)
    {
        $this->setup();
        if ($this->getCarById($car_id) == null) {
            return ["status" => "error", "error" => $this->car_not_found ];
        }
        $validate = $this->validateSaveCar($data);
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->car_data_is_incorrect . " " . $validate["error"] ];
        }
        if(!isset($data['has_arc'])){
            $data['has_arc'] = 0;
        }
        if(!isset($data['has_manipulator'])){
            $data['has_manipulator'] = 0;
        }
        $this->Car->id = $car_id;
        $this->Car->save($data);
        if($this->Car->id>0){
            return $this->Car->id;
        }
        return null;
    }

    public function deleteCar($car_id)
    {
        $this->setup();
        if ($this->getCarById($car_id) == null) {
            return ["status" => "error", "error" => $this->car_not_found ];
        }
        $this->Car->id = $car_id;
        $this->Car->delete();
        return true;
    }

    public function deleteCarType($car_type_id)
    {
        $this->setup();
        if ($this->getCarTypeByTypeId($car_type_id) == null) {
            return ["status" => "error", "error" => $this->car_not_found ];
        }
        $this->Car_Type->id = $car_type_id;
        $this->Car_Type->delete();
        return true;
    }

    public function saveCarType($car_type_id, $data)
    {
        $this->setup();
        if ($this->getCarTypeByTypeId($car_type_id) == null) {
            return ["status" => "error", "error" => $this->car_not_found ];
        }
        $this->Car_Type->id = $car_type_id;
        $this->Car_Type->save($data);
        if($this->Car_Type->id>0){
            return $this->Car_Type->id;
        }
        return null;
    }


    //--- private

    /**
     * @param $data
     * @return array
     */
    private function validateCar($data)
    {
        if (!is_integer($data['type_id']) OR !$this->checkTypeIdExists($data['type_id'])) {
            return ["status" => "error", "error" => $this->unknown_or_empty_type_id];
        }
        if($this->validateCarNumber($data['car_number'])){
            return ["status" => "error", "error" => $this->incorrect_or_empty_car_number];
        }
        if(!is_bool($data['has_manipulator'])){
            return ["status" => "error", "error" => $this->has_manipulator_must_be_boolean];
        }
        if(!is_bool($data['has_arc'])){
            return ["status" => "error", "error" => $this->has_arc_must_be_boolean];
        }
        if(empty($data['max_weight'])){
            return ["status" => "error", "error" => $this->cargo_length_must_have_positive_value];
        }
        if(empty($data['cargo_length'])){
            return ["status" => "error", "error" => $this->cargo_length_must_have_positive_value];
        }
        if(empty($data['cargo_width'])){
            return ["status" => "error", "error" => $this->cargo_width_must_have_positive_value];
        }
        if(empty($data['cargo_height'])){
            return ["status" => "error", "error" => $this->cargo_height_must_have_positive_value];
        }
        if(empty($data['name'])){
            return ["status" => "error", "error" => $this->car_type_must_have_name];
        }
        return ["status" => "success"];
    }

    private function validateSaveCar($data)
    {
        if (!empty($data['type_id'])){
            if (!is_integer($data['type_id']) OR !$this->checkTypeIdExists($data['type_id'])) {
                return ["status" => "error", "error" => $this->unknown_or_empty_type_id];
            }
        }
        if(!$this->validateCarNumber($data['car_number'])){
            return ["status" => "error", "error" => $this->incorrect_or_empty_car_number];
        }
        return ["status" => "success"];
    }

    /**
     * @param $car_number
     * @return bool
     */
    private function validateCarNumber($car_number)
    {
        if(empty($car_number) OR mb_strlen($car_number)<=5 OR mb_strlen($car_number)>10){
            return false;
        }
        return true;
    }

    /**
     * @param $type_id
     * @return bool
     */
    private function checkTypeIdExists($type_id)
    {
        $check = $this->Car_Type->find("count",
            array('conditions' =>
                array(
                    'id' => $type_id
                )
            )
        );
        if ($check == 0) {
            return false;
        }
        return true;
    }

    /**
     * @param $data
     * @return array
     */
    private function validateCarType($data)
    {
        if (!is_integer($data['type_id']) OR !$this->checkTypeIdExists($data['type_id'])) {
            return ["status" => "error", "error" => $this->unknown_or_empty_type_id];
        }
        return ["status" => "success"];
    }
}