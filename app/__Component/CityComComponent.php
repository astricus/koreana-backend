<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Город
 */
class CityComComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "City";
        $this->City = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCityNameById($id)
    {
        $this->setup();
        $city = $this->City->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
        return $city['City']['name'];
    }

    /**
     * @param $name
     * @return int
     */
    public function getCityIdByName($name)
    {
        $this->setup();
        $city = $this->City->find("first",
            array('conditions' =>
                array(
                    'name' => $name
                )
            )
        );
        return $city['City']['id'] ?? 0;
    }

    /** Сервис Геокодирования
     * @param $address
     * @return array
     */
    public function getCoordByAddress($address){
        $address = urlencode($address);
        $url = "http://geocode-maps.yandex.ru/1.x/?geocode={$address}";
        $content = file_get_contents($url);
        preg_match("/<pos>(.*?)<\/pos>/", $content, $point);
        //$coords = str_replace(' ', ', ', trim(strip_tags($point[1])));
        if(!empty($point[1])) {
            return explode(" ", $point[1]);
        }
        return null;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCitiesByRegionId($id)
    {
        $this->setup();
        $cities = $this->City->find("all",
            array('conditions' =>
                array(
                    'region_id' => $id
                )
            )
        );
        return $cities;
    }

}