<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');
App::import('Lib', 'memcached');

/**
 * Компонент Кэшер
 */
class CacherComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Log'
    );
    public $controller;

    public $cacher;

    public $port = 11211;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * соединение с кеш-сервером
     */
    public function connect()
    {
        if ($this->cacher == null) {
            $this->cacher = new Memcached();
            $this->cacher->addServer('localhost', $this->port);
        }

    }

    /**
     * @param $field
     * @return mixed
     */
    public function getValue($field)
    {
        $this->connect();
        return $this->cacher->get($field);
    }

    /**
     * @param $field
     * @param $value
     * @param $expr
     * @return mixed
     */
    public function setValue($field, $value, $expr = 3600)
    {
        $this->connect();
        return $this->cacher->set($field, $value, $expr);
    }

    /** Возвращает файл хеша по его md5
     * @param $route
     * @param $lifetime
     * @return mixed
     */
    public function json_cache($route, $lifetime = 86400)
    {
        $cache_file = $this->cache_filename($this->hash_by_route($route));
        $filemtime = @filemtime($cache_file);
        if (!$filemtime or (time() - $filemtime >= $lifetime)) {
            return false;
        }
        return [
            'file' => $cache_file,
            'modified' => $filemtime
        ];
    }

    /**
     * @param $route
     * @return string
     */
    public function hash_by_route($route)
    {
        return md5($route);
    }

    /**
     * @param $route
     * @param $data_json
     */
    public function set_json_cache($route, $data_json)
    {
        $hash = $this->hash_by_route($route);
        file_put_contents($this->cache_filename($hash), json_encode($data_json));

    }

    /**
     * @param $hash
     * @return string
     */
    private function cache_filename($hash)
    {
        $cache_dir = Configure::read("JSON_CACHE_DIR");
        if (!is_dir($cache_dir)) {
            mkdir($cache_dir, 0777);
        }
        return $cache_dir . DS . $hash . ".json";
    }

    /**
     * @param $cached_route
     */
    public function return_cached($cached_route, $request)
    {
        $no_cache = $request->query('no_cache') ?? $request->data('no_cache');
        if ($no_cache == null) {
            $cache_check = $this->json_cache($cached_route);
            if (is_array($cache_check)) {
                $cache_check_array['data'] = json_decode(file_get_contents($cache_check['file']));
                //$cache_check_array['modified'] =  $cache_check['modified'];
                //$cache_check_array['file'] =  $cache_check['file'];
                $cache_result = json_encode($cache_check_array);
                echo $cache_result;
                exit;
            }
        }
    }

    public function getCacheCostDelivery($delivery_data, $provider_id)
    {
        $modelName = "Provider_Api_Cache";
        $this->Provider_Api_Cache = ClassRegistry::init($modelName);

        /////////////////////////////////////////////////////////////////////////////////////
        // Удалить устаревшие данные (временный метод)
        $res_del = $this->Provider_Api_Cache->find('all',
            array(
                'conditions' => array(
                    'Provider_Api_Cache.expired < NOW()',
                ),
                'fields' => array(
                    'Provider_Api_Cache.id',
                ),
            )
        );
        foreach ($res_del as $val) {
            $this->Provider_Api_Cache->id = $val['Provider_Api_Cache']['id'];
            $this->Provider_Api_Cache->delete();
        }
        /////////////////////////////////////////////////////////////////////////////////////

        $data_cache = [
            'sender_city' => $delivery_data['sender']['address']['city_id'],
            'recipient_city' => $delivery_data['recipient']['address']['city_id'],
            'weight' => round($delivery_data['weight']),
            'insurance' => $delivery_data['insurance'],
        ];
        $hash = md5(json_encode($data_cache));
        $hash_data = $this->Provider_Api_Cache->find("first",
            array(
                'conditions' => array(
                    'Provider_Api_Cache.request' => $hash,
                    'Provider_Api_Cache.provider_id' => $provider_id,
                ),
                'fields' => array(
                    'Provider_Api_Cache.response',
                ),
            )
        );
        if ($hash_data) {
            return json_decode($hash_data['Provider_Api_Cache']['response'], JSON_OBJECT_AS_ARRAY);
        } else {
            return false;
        }
    }

    /**
     * Записать в базу данные о стоимости доставки
     * @param array $delivery_data данные о доставке
     * @param string $response результат ответа в json
     * @param integer $provider_id id ТК
     */
    public function setCacheCostDelivery($delivery_data,$response, $provider_id)
    {
        $data_cache = [
            'sender_city' => $delivery_data['sender']['address']['city_id'],
            'recipient_city' => $delivery_data['recipient']['address']['city_id'],
            'weight' => round($delivery_data['weight']),
            'insurance' => $delivery_data['insurance'],
        ];
        $hash = md5(json_encode($data_cache));

        $save = array(
            'provider_id' => $provider_id,
            'request_type' => 'cost_delivery',
            'request' => $hash,
            'response' => $response,
            //'created' => date,
            'expired' => date('Y-m-d H:i:s', time() + 24 * 60 *60),
        );
        $this->Provider_Api_Cache->create();
        $this->Provider_Api_Cache->save($save);
    }

}