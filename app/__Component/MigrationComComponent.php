<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');


class MigrationComComponent extends Component
{
    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);
    }

    /**
     *  Добавить вес в таблицу products из таблицы shop_product_fields
     */
    public function addWeightToProducts()
    {
        $this->setup();
        $products = $this->Product->find("all",
            array(
                'conditions' =>
                    array(
                        'weight' => 0,
                    ),
                'joins' => array(
                    array(
                        'table' => 'shop_product_fields',
                        'alias' => 'Fields',
                        'type' => 'LEFT',
                        'conditions' => array(
                            "Fields.shop_product_id = Product.id AND Fields.field = 'package_weight'"
                        )
                    ),
                ),
                'fields' => array(
                    'Product.id, Product.product_name',
                    'Fields.value',
                ),
                //'limit' => 3,
            )
        );
        //pr($products);
        foreach($products as $v){
            if($v['Fields']['value'] != ''){
                $w = trim(str_replace('кг', '', $v['Fields']['value']));
                $this->Product->set(['weight' => $w]);
                $this->Product->id = $v['Product']['id'];
                $this->Product->save();
            }
        }
        echo 'end';
    }
}