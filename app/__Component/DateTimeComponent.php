<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Для работы с датами и временем
 */
class DateTimeComponent extends Component
{
    public $components = array(
        'Log'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    public function day_separator($day_in_seconds, $days_only = false)
    {
        $viewed_string = "";
        $days = floor($day_in_seconds / 86400);

        if ($days > 0) {
            $viewed_string .= $days . " " . word_count_format($days, ['день', 'дня', 'дней']);
            $left = $day_in_seconds - $days * 86400;
        } else {
            $left = $day_in_seconds;
        }

        if ($days_only) {
            if (empty($viewed_string)) $viewed_string = "СЕГОДНЯ";
            return $viewed_string;
        }

        if ($days > 1) {
            return $viewed_string;
        }

        $viewed_string .= " ";
        $hours = floor($left / 3600);
        if ($hours > 0) {

            $viewed_string .= $hours . " " . word_count_format($hours, ['час', 'часа', 'часов']);
            $left = $left - $hours * 3600;
        }

        $viewed_string .= " ";
        $minutes = floor($left / 60);
        if ($minutes > 0) {
            $viewed_string .= $minutes . " " . word_count_format($minutes, ['минута', 'минуты', 'минут']);
            $left = $left - $minutes * 60;
        }

        $seconds = $left;
        $viewed_string .= " ";
        if ($seconds > 0) {
            $viewed_string .= $seconds . " " . word_count_format($seconds, ['секунда', 'секунд', 'секунд']);
        }

        return $viewed_string;

    }

    public function days_later($day_in_seconds, $real_time = '')
    {
        $days = floor($day_in_seconds / 86400);
        if ($days == 0) {
            if ($day_in_seconds <= 60) {
                return L('JUST_NOW');
            } else if ($day_in_seconds > 60 AND $day_in_seconds <= 3600) {
                $min = floor($day_in_seconds / 60);
                //return L('MINUTES_PLURAL');
                return $min . " " . word_count_format($min, L('MINUTES_PLURAL')) . " " . L('LATER');
            }

            if ($day_in_seconds > 3600 AND $day_in_seconds <= 86400) {
                $hour = floor($day_in_seconds / 3600);
                return $hour . " " . word_count_format($hour, L('HOURS_PLURAL')) . " " . L('LATER');
            }
            //return L('TODAY');
        } elseif ($days == 1) {
            return L('YESTERDAY') . " " . L('AT') . " " . only_time($real_time);
        } elseif ($days > 1 AND $days < 30) {
            return $days . " " . word_count_format($days, L('DAYS_PLURAL')) . " " . L('LATER');
        } elseif ($days >= 30 AND $days < 365) {
            $month = floor($days / 30);
            return $month . " " . word_count_format($month, L('MONTH_PLURAL')) . " " . L('LATER');
        } elseif ($days >= 365) {
            $years = floor($days / 365);
            return $years . " " . word_count_format($years, L('YEAR_PLURAL')) . " " . L('LATER');
        }
    }

    public function now_date()
    {
        return date("Y-m-d");
    }

    public function only_time($date)
    {
        return $date = substr($date, 11, 5);
    }


}
