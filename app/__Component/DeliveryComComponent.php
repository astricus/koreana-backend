<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');
App::uses('ApiPek', 'Lib'); // API ПЭК

/**
 * Компонент Пользователь
 */
class DeliveryComComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'ProductCom',
        'Cacher',
        'Car',
        'Driver'
    );

    public $api_providers = [
        1 => 'ApiPek'
    ];

    public $delivery_type = [
        'from_company_to_provider_store' => 'Поставщик - Склад',
        'from_provider_store_to_orderer' => 'Склад - Покупатель',
        'from_company_to_orderer' => 'Поставщик - Покупатель',
        'from_orderer_to_provider_store' => 'Покупатель - Склад (Возврат)',
        'from_provider_store_to_company' => 'Склад - Поставщик (Возврат)',
        'from_orderer_to_company' => 'Покупатель - Поставщик (Возврат)',
    ];

    public $valid_point_type = [
        'company_store' => 'Склад поставщика',
        'orderer' => 'Адрес покупателя',
        'office' => 'Офис/Склад',
    ];

    public $unknown_delivery_route_point_type = "Неизвестный тип точки марщрута доставки";
    public $empty_delivery_route_point_type = "Пуст тип точки марщрута доставки";


    //'new','transit_to_store','transit_to_client','complete','abort'
    public $delivery_status = [
        'new' => 'Новая доставка',
        'transit_to_store' => 'В пути на склад ТК',
        'transit_to_client' => 'В пути к клиенту',
        'complete' => 'Доставлен',
        'abort' => 'Отменен',
    ];

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Delivery";
        $this->Delivery = ClassRegistry::init($modelName);

        $modelName = "Delivery_Provider";
        $this->Delivery_Provider = ClassRegistry::init($modelName);

        $modelName = "Delivery_Offer";
        $this->Delivery_Offer = ClassRegistry::init($modelName);

        $modelName = "Order";
        $this->Order = ClassRegistry::init($modelName);

        $modelName = "Client_Order";
        $this->Client_Order = ClassRegistry::init($modelName);

        $modelName = "Order_Product";
        $this->Order_Product = ClassRegistry::init($modelName);

        $modelName = "Provider_Api_Cache";
        $this->Provider_Api_Cache = ClassRegistry::init($modelName);

        $modelName = "Provider_Cargo_Value";
        $this->Provider_Cargo_Value = ClassRegistry::init($modelName);

        $modelName = "Provider_Setting";
        $this->Provider_Setting = ClassRegistry::init($modelName);
    }

    // ФУНКЦИОНАЛ, КОТОРЫЙ НЕ ВОЙДЕТ В MVP

    public function getDeliveryPrice($delivery_id, $provider_id)
    {
        $data = [];
        $delivery_data = $this->getDeliveryById($delivery_id);
        if ($provider_id == 0) {
            $provider_all = $this->getActiveProviders();
            foreach ($provider_all as $v) {
                $res = $this->calculateDeliveryPrice($delivery_data, $v['Delivery_Provider']['id']);

                $data[$v['Delivery_Provider']['id']] = [
                    'provider_id'        => $v['Delivery_Provider']['id'],
                    'provider_name'      => $v['Delivery_Provider']['name'],
                    'provider_title'     => $v['Delivery_Provider']['title'],
                    'delivery_cost_data' => $res
                ];
            }
        } else {
            $provider_data = $this->getProviderId($provider_id);
            $res = $this->calculateDeliveryPrice($delivery_data, $provider_id);
            $data[$provider_id] = [
                'provider_id'        => $provider_data['Delivery_Provider']['id'],
                'provider_name'      => $provider_data['Delivery_Provider']['name'],
                'provider_title'     => $provider_data['Delivery_Provider']['title'],
                'delivery_cost_data' => $res
            ];
        }
        $this->saveDeliveryCostData($delivery_id, $data);
        return $data;
    }

    private function saveDeliveryCostData($delivery_id, $data)
    {
        $this->Delivery->id = $delivery_id;
        $this->Delivery->save(['cost_delivery_all' => json_encode($data)]);
    }

    public function calculateDeliveryPrice($delivery_data, $provider_id)
    {
        //$delivery_data = $this->getDeliveryById($delivery);
        //pr($delivery_data);
        $res_cache = $this->Cacher->getCacheCostDelivery($delivery_data, $provider_id);
        if ($res_cache) {
            return $res_cache;
        } else {
            $provider = $this->connectApiProvider($provider_id);
            if (!is_object($provider)) {
                die("Транспортная компания не найдена или API не подключено");
            }

            $res = $provider->getCostDeliveryCompanyToStore($delivery_data);
            //Кэширую ответ
            $this->Cacher->setCacheCostDelivery(
                $delivery_data,
                json_encode($res),
                $provider_id
            );

            return $res;
        }
    }

    public function calculateOrderPrice($order, $provider_id)
    {
        $order_data = $this->getOrderById($order);
        $provider = $this->connectApiProvider($provider_id);
        $res = $provider->getCostDeliveryCompanyToStore($order_data);
        return $res;
    }

    public function connectApiProvider($provider_id)
    {
        $api_class = $this->api_providers[$provider_id];
        if (class_exists($api_class)) {
            return new $this->api_providers[$provider_id];
        }
        return false;
    }

    /** Получить статус доставки
     * @param $delivery_id
     * @return mixed
     */
    public function getDeliveryStatus($delivery_id)
    {
        $res = $this->getDeliveryField($delivery_id, ['status']);
        return $res['status'];
    }

    /**
     * @param $delivery_id
     * @param array $fields
     * @return mixed
     */
    private function getDeliveryField($delivery_id, array $fields)
    {
        $this->setup();
        $res = $this->Delivery->find("first",
            array('conditions' =>
                array(
                    'id' => $delivery_id
                )
            , "field" => $fields
            )
        );
        return $res['Delivery'];
    }

    public function setDeliveryStatus($delivery_id, $status)
    {
        $this->setup();
        //'new','transit_to_store','transit_to_client','complete','abort'
        switch ($status) {
            case 'new':
                $this->setStatusNew($delivery_id);
                break;
            case 'transit_to_store':
                $this->setStatusTransitToStore($delivery_id);
                break;
            case 'transit_to_client':
                $this->setStatusTransitToClient($delivery_id);
                break;
            case 'complete':
                $this->setStatusComplete($delivery_id);
                break;
            case 'abort':
                $this->setStatusAbort($delivery_id);
                break;
        }
    }

    private function setStatusNew($id)
    {
        $this->Delivery->id = $id;
        $this->Delivery->save(['status' => 'new', 'status_date' => now_date()]);

    }

    private function setStatusTransitToStore($id)
    {
        $this->Delivery->id = $id;
        $this->Delivery->save(['status' => 'transit_to_store', 'status_date' => now_date()]);

    }

    private function setStatusTransitToClient($id)
    {
        $this->Delivery->id = $id;
        $this->Delivery->save(['status' => 'transit_to_client', 'status_date' => now_date()]);
    }

    private function setStatusComplete($id)
    {
        $this->Delivery->id = $id;
        $this->Delivery->save(['status' => 'complete', 'status_date' => now_date()]);
    }

    private function setStatusAbort($id)
    {
        $this->Delivery->id = $id;
        $this->Delivery->save(['status' => 'abort', 'status_date' => now_date()]);
    }
    //----------------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------------//
    //  Получить настройки провайдера, мексимальный вес места, негабаритный груз ит.д.

    /** Получить максимальный вес транспортного места
     * @param integer $provider_id id ТК
     * @return bool|string
     */
    public function checkMaxWeight($provider_id)
    {
        $param = 'maxWeightSeat';
        return $this->getCargoValue($provider_id, $param);
    }

    /** Получить возможность перевозки негабаритного груза
     * @param integer $provider_id id ТК
     * @return bool|string
     */
    public function checkOversizeCargo($provider_id)
    {
        $param = 'oversizeCargo';
        return $this->getCargoValue($provider_id, $param);
    }

    /** Выборка заначений по параметру для ТК
     * @param integer $provider_id
     * @param string $param
     * @return bool|string
     */
    private function getCargoValue($provider_id, $param)
    {
        $this->setup();
        $value = $result = $this->Provider_Setting->find("first",
            array(
                'conditions' => array(
                    'Provider_Setting.provider_id' => $provider_id,
                    'ProviderCargoValue.translit' => $param,
                ),
                'joins' => array(
                    array(
                        'table' => 'provider_cargo_value',
                        'alias' => 'ProviderCargoValue',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'ProviderCargoValue.id = Provider_Setting.param_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Provider_Setting.value',
                ),
            )
        );
        if ($value) {
            return $value['Provider_Setting']['value'];
        } else {
            return false;
        }
    }
    //---------------------------------------------------------------------------------------//

    /**
     * @param $delivery_id
     * @param $delivery_data
     * сохранение
     */
    private function saveDelivery($delivery_id, $delivery_data)
    {

    }

    /**
     * @param $new_delivery
     * Создание новой доставки
     */
    private function createDelivery($new_delivery)
    {

    }

    public function getDeliveryAll()
    {
        $this->setup();
        $deliveries = $result = $this->Delivery->find("all",
            array(
                'fields' => array(
                    'Delivery.id',
                ),
                'order' => array('Delivery.id DESC'),
                'limit' => 10,
            )
        );

        $data = [];
        foreach ($deliveries as $delivery) {
            $data[$delivery['Delivery']['id']] = $this->getDeliveryById($delivery['Delivery']['id']);
        }

        return $data;
    }

    /**
     * @param $delivery_id
     * @return array
     */
    public function getDeliveryById($delivery_id)
    {
        $this->setup();
        $data['delivery_id'] = $delivery_id;
        $delivery = $result = $this->Delivery->find("first",
            array(
                'conditions' => array(
                    'Delivery.id' => $delivery_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'stores',
                        'alias' => 'Provider_Store',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Provider_Store.id = Delivery.provider_store_id'
                        )
                    ),
                    array(
                        'table' => 'addresses',
                        'alias' => 'Provider_Address',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Provider_Address.id = Provider_Store.address_id'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'Provider_City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Provider_City.id = Provider_Address.city_id'
                        )
                    ),
                    array(
                        'table' => 'streets',
                        'alias' => 'Provider_Street',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Provider_Street.id = Provider_Address.street_id'
                        )
                    ),
                    array(
                        'table' => 'delivery_providers',
                        'alias' => 'Provider',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Provider.id = Delivery.provider_id'
                        )
                    ),
                    array(
                        'table' => 'stores',
                        'alias' => 'Company_Store',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company_Store.id = Delivery.company_store_id'
                        )
                    ),
                    array(
                        'table' => 'addresses',
                        'alias' => 'Company_Address',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company_Address.id = Company_Store.address_id'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'Company_City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company_City.id = Company_Address.city_id'
                        )
                    ),
                    array(
                        'table' => 'streets',
                        'alias' => 'Company_Street',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company_Street.id = Company_Address.street_id'
                        )
                    ),
                    array(
                        'table' => 'companies',
                        'alias' => 'Company',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company.id = Company_Store.company_id'
                        )
                    ),
                    array(
                        'table' => 'client_orders',
                        'alias' => 'Client_Order',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Client_order.id = Delivery.client_order_id'
                        )
                    ),
                    array(
                        'table' => 'addresses',
                        'alias' => 'Client_Address',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Client_Address.id = Client_Order.order_address'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'Client_City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Client_City.id = Client_Address.city_id'
                        )
                    ),
                    array(
                        'table' => 'streets',
                        'alias' => 'Client_Street',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Client_Street.id = Client_Address.street_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Delivery.*',
                    'Provider_Store.*',
                    'Provider_Address.*',
                    'Provider_City.*',
                    'Provider_Street.*',
                    'Provider.*',
                    'Company_Store.*',
                    'Company_Address.*',
                    'Company_City.*',
                    'Company_Street.*',
                    'Company.*',
                    'Client_Order.*',
                    'Client_Address.*',
                    'Client_City.*',
                    'Client_Street.*',
                ),
            )
        );

        //pr($delivery);

        $data = [
            'delivery_id'       => $delivery_id,
            'delivery_type'     => $delivery['Delivery']['delivery_type'],
            'status'            => $delivery['Delivery']['status'],
            'status_date'       => $delivery['Delivery']['status_date'],
            'cost_delivery_all' => $delivery['Delivery']['cost_delivery_all'],
            'date'              => $delivery['Delivery']['created'],
            'cost_delivery'     => $delivery['Delivery']['provider_cost'],
        ];

        $addresesData = [
            'delivery_type' => $delivery['Delivery']['delivery_type'],
            'company' => [
                'info' => [
                    'id'    => $delivery['Company']['id'],
                    'name'  => $delivery['Company']['company_name'],
                    'phone' => $delivery['Company']['phone_number'],
                ],
                'address' => [
                    'region_id'   => $delivery['Company_Address']['region_id'],
                    'city_id'     => $delivery['Company_Address']['city_id'],
                    'city'        => $delivery['Company_City']['name'],
                    'street_id'   => $delivery['Company_Address']['street_id'],
                    'street_type' => $delivery['Company_Street']['type'],
                    'street'      => $delivery['Company_Street']['name'],
                    'building'    => $delivery['Company_Address']['building'],
                    'house'       => $delivery['Company_Address']['house'],
                ],
                'contact' => [
                    'store_id'     => $delivery['Company_Store']['id'],
                    'store_name'   => $delivery['Company_Store']['name'],
                    'contact_name' => $delivery['Company_Store']['contact_name'],
                    'phone'        => $delivery['Company_Store']['phone_number'],
                ],
            ],
            'provider' => [
                'info' => [
                    'id'   => $delivery['Provider']['id'],
                    'name' => $delivery['Provider']['title'],
                ],
                'address' => [
                    'region_id'   => $delivery['Provider_Address']['region_id'],
                    'city_id'     => $delivery['Provider_Address']['city_id'],
                    'city'        => $delivery['Provider_City']['name'],
                    'street_id'   => $delivery['Provider_Address']['street_id'],
                    'street_type' => $delivery['Provider_Street']['type'],
                    'street'      => $delivery['Provider_Street']['name'],
                    'building'    => $delivery['Provider_Address']['building'],
                    'house'       => $delivery['Provider_Address']['house'],
                ],
                'contact' => [
                    'store_id'     => $delivery['Provider_Store']['id'],
                    'store_name'   => $delivery['Provider_Store']['name'],
                    'contact_name' => $delivery['Provider_Store']['contact_name'],
                    'phone'        => $delivery['Provider_Store']['phone_number'],
                ],
            ],
            'client' => [
                'address' => [
                    'region_id'   => $delivery['Client_Address']['region_id'],
                    'city_id'     => $delivery['Client_Order']['order_city'],
                    'city'        => $delivery['Client_City']['name'],
                    'street_id'   => $delivery['Client_Address']['street_id'],
                    'street_type' => $delivery['Client_Street']['type'],
                    'street'      => $delivery['Client_Street']['name'],
                    'building'    => $delivery['Client_Address']['building'],
                    'house'       => $delivery['Client_Address']['house'],
                ],
                'contact' => [
                    'contact_name' => $delivery['Client_Order']['order_lastname'].' '.$delivery['Client_Order']['order_firstname'].' '.$delivery['Client_Order']['order_middlename'],
                    'firstname'    => $delivery['Client_Order']['order_firstname'],
                    'lastname'     => $delivery['Client_Order']['order_lastname'],
                    'middlename'   => $delivery['Client_Order']['order_middlename'],
                    'phone'        => $delivery['Client_Order']['order_phone'],
                    'email'        => $delivery['Client_Order']['order_email'],
                ],
            ],
        ];

        $address = $this->SenderRecipient($addresesData);
        $data = array_merge( $data, $address);

        $products = $this->getDeliveryItems($delivery_id);
        $data = array_merge( $data, $products);
        return $data;
        //pr($addresesData);
    }

    private function SenderRecipient($delivery)
    {
        switch ($delivery['delivery_type']) {
            case 'from_company_to_provider_store':
                $data = [
                    'sender' => [
                        'address' => $delivery['company']['address'],
                        'contact' => $delivery['company']['contact'],
                    ],
                    'recipient' => [
                        'address' => $delivery['provider']['address'],
                        'contact' => $delivery['provider']['contact'],
                    ],
                ];
                return $data;
                break;
            case 'from_provider_store_to_orderer':
                $data = [
                    'sender' => [
                        'address' => $delivery['provider']['address'],
                        'contact' => $delivery['provider']['contact'],
                    ],
                    'recipient' => [
                        'address' => $delivery['client']['address'],
                        'contact' => $delivery['client']['contact'],
                    ],
                ];
                return $data;
                break;
            case 'from_company_to_orderer':
                $data = [
                    'sender' => [
                        'address' => $delivery['company']['address'],
                        'contact' => $delivery['company']['contact'],
                    ],
                    'recipient' => [
                        'address' => $delivery['client']['address'],
                        'contact' => $delivery['client']['contact'],
                    ],
                ];
                return $data;
                break;
            case 'from_orderer_to_provider_store':
                $data = [
                    'sender' => [
                        'address' => $delivery['client']['address'],
                        'contact' => $delivery['client']['contact'],
                    ],
                    'recipient' => [
                        'address' => $delivery['provider']['address'],
                        'contact' => $delivery['provider']['contact'],
                    ],
                ];
                return $data;
                break;
            case 'from_provider_store_to_company':
                $data = [
                    'sender' => [
                        'address' => $delivery['provider']['address'],
                        'contact' => $delivery['provider']['contact'],
                    ],
                    'recipient' => [
                        'address' => $delivery['company']['address'],
                        'contact' => $delivery['company']['contact'],
                    ],
                ];
                return $data;
                break;
            case 'from_orderer_to_company':
                $data = [
                    'sender' => [
                        'address' => $delivery['client']['address'],
                        'contact' => $delivery['client']['contact'],
                    ],
                    'recipient' => [
                        'address' => $delivery['company']['address'],
                        'contact' => $delivery['company']['contact'],
                    ],
                ];
                return $data;
                break;
        }
    }

    public function getDeliveryItems($delivery_id)
    {
        $result = $this->Delivery_Offer->find("all",
            array('conditions' =>
                array(
                    'delivery_id' => $delivery_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'order_products',
                        'alias' => 'Order_Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Delivery_Offer.order_position_id = Order_Product.id'
                        )
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Order_Product.product_id = Product.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Delivery_Offer.*',
                    'Order_Product.*',
                    'Product.*',
                ),
            )
        );

        $product = [];
        $total_price = 0; // Полная стоимость доставки (ЗГ)
        $weight = 0; // Полный вес доставки (ЗГ)
        foreach ($result as $res) {
            $total_price += $res['Order_Product']['amount'] * $res['Order_Product']['order_price'];
            $weight += $res['Product']['weight'];

            $product[$res['Delivery_Offer']['order_id']][] = [
                'id' => $res['Order_Product']['product_id'],
                'name' => $res['Product']['product_name'],
                'amount' => $res['Order_Product']['amount'],
                'price' => $res['Order_Product']['order_price'],
                'barcode' => $res['Product']['system_barcode'],
                'weight' => $res['Product']['weight'],
            ];

        }
        $data['products'] = $product;
        $data['total_price'] = $total_price;
        $data['insurance'] = $total_price;
        $data['weight'] = $weight;

        return $data;
    }


    public function getActiveProviders()
    {
        $this->setup();
        $delivery_provider = $this->Delivery_Provider->find("all",
            array('conditions' =>
                array(
                    'status' => 'active'
                ),
            )
        );
        return $delivery_provider;
    }

    public function getProviderId($id)
    {
        $provider = $this->Delivery_Provider->find("first",
            array('conditions' =>
                array(
                    'id' => $id,
                ),
            )
        );
        return $provider;
    }

    public function getDeliveryPrices($data, $provider_id)
    {
        $delivery = null;
        $active_providers = $this->getActiveProviders();
        foreach ($active_providers as $active_provider) {
            $this->calculateOrderPrice($delivery, $provider_id);
        }
    }



    //МАРКЕТИНГОВЫЕ ЗОНЫ

    private function load_delivery_marketing_model(){
        $modelName = "Marketing_Price";
        $this->Marketing_Price = ClassRegistry::init($modelName);

        $modelName = "Marketing_Zone";
        $this->Marketing_Zone = ClassRegistry::init($modelName);

        $modelName = "Marketing_Limit";
        $this->Marketing_Limit = ClassRegistry::init($modelName);
    }

    public function deliveryMarketingTable()
    {
        $this->load_delivery_marketing_model();
    }

    public function getDeliveryMarketingPrices()
    {
        $this->load_delivery_marketing_model();
        $prices = $this->Marketing_Price->find("all",
            array(
                "order" => array("id ASC")
            )
        );
        return $prices;
    }

    public function getDeliveryMarketingLimits()
    {
        $this->load_delivery_marketing_model();
        $limits = $this->Marketing_Limit->find("all",
            array(
                "order" => array("id ASC")
            )
        );
        return $limits;
    }

    public function getDeliveryMarketingZones()
    {
        $this->load_delivery_marketing_model();
        $zones = $this->Marketing_Zone->find("all",
            array(
                "order" => array("id ASC")
            )
        );
        return $zones;
    }

    public function addMarketingZone($name){
        $this->load_delivery_marketing_model();
        $zone = [
            'name' => $name,
        ];
        $this->Marketing_Zone->create();
        $this->Marketing_Zone->save($zone);
        return $this->Marketing_Zone->id;
    }

    /**
     * @param $weight
     * @param $volume
     * @return mixed
     */
    public function addMarketingLimit($weight, $volume){
        $this->load_delivery_marketing_model();
        $limit = [
            'max_volume' => $volume,
            'max_weight' => $weight,
        ];
        $this->Marketing_Limit->create();
        $this->Marketing_Limit->save($limit);
        return $this->Marketing_Limit->id;
    }

    /**
     * @param $price
     * @param $limit
     * @param $zone
     * @return mixed
     */
    public function addMarketingPrice($price, $limit, $zone){
        $this->load_delivery_marketing_model();
        $check_price_exists = $this->Marketing_Price->find("all",
            array(
                "conditions" => array(
                    "limit_id" => $limit,
                    "zone_id" => $zone,
                ),
                "order" => array("id ASC")
            )
        );
        if(count($check_price_exists) == 0) {
            $new_price = [
                'price' => $price,
                'limit_id' => $limit,
                'zone_id' => $zone,
            ];
            $this->Marketing_Price->create();
            $this->Marketing_Price->save($new_price);
        }else {
            $id = $check_price_exists[0]['Marketing_Price']['id'];
            $this->Marketing_Price->id = $id;
            $new_price = [
                'price' => $price,
            ];
            $this->Marketing_Price->save($new_price);
        }
        return $this->Marketing_Price->id;
    }

    /**
     * @param $price_id
     * @param $price
     */
    public function updateMarketingPrice($price_id, $price){
        $this->load_delivery_marketing_model();
        $price = [
            'price' => $price,
        ];
        $this->Marketing_Price->id = $price_id;
        $this->Marketing_Price->save($price);
    }

    /**
     * @param $limit_id
     * @param $weight
     * @param $volume
     */
    public function updateMarketingLimit($limit_id, $weight, $volume){
        $this->load_delivery_marketing_model();
        $limit = [
            'max_weight' => $weight,
            'max_volume' => $volume,
        ];
        $this->Marketing_Limit->id = $limit_id;
        $this->Marketing_Limit->save($limit);
    }

    /**
     * @param $zone_id
     * @param $name
     */
    public function updateMarketingZone($zone_id, $name){
        $this->load_delivery_marketing_model();
        $zone = [
            'name' => $name,
        ];
        $this->Marketing_Zone->id = $zone_id;
        $this->Marketing_Zone->save($zone);
    }

    public function getPriceByLimitAndZone($limit, $zone){
        $price = $this->Marketing_Price->find("first",
            array(
                "conditions" => array(
                    "limit_id" => $limit,
                    "zone_id" => $zone,
                ),
                "order" => array("id ASC")
            )
        );
        if(count($price)==0){
            return null;
        }
        return $price['Marketing_Price']['price'];
    }

}