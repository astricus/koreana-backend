<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Администратор
 */
class ActivityComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Log'
    );

    public $controller;
    public $user_data;
    public $user_id;

    public $uses = array('Error');

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    public $activities = array(
        'add_shop',
        'edit_shop',
        'add_order',
        'add_order_comment',

        'edit_order',
        /*
        'login',
        'add_user',
        'edit_user',
        'reset_password',
        'add_manager',
//
        'edit_manager',
        'add_object',
        'edit_object',
        'add_object_link',
        'delete_object_link',
//
        'answer_ticket',
        'close_ticket',
        'open_ticket',
        'add_ticket_message',
        'remove_ticket_message',
//
        'block_object',
        'unblock_object',
        'set_user_object',
        'reset_user_object',
        */
    );

    public $owner_types = [
        'system' => "система",
        'orderer' => "пользователь",
        'company_agent' => "поставщик",
        'manager' => "администратор/менеджер"
    ];

    public $type_objects = [
        'shop' => "Магазин",
        'product' => "Товар",
        'order' => "Заказ",
        'account' => "Аккаунт",
        'payment' => "Платеж"
    ];

    public $activity_names = array(
        'Создание магазина <span class="action_item">#2</span>',
        'Обновление данных магазина <span class="action_item">#2</span>',
        'Создан заказ <span class="action_item">#2</span>',
        'К заказу <span class="action_item">#1</span> добавлен комментарий <span class="action_item">#1</span>',
        /*
        "Менеджер #1 авторизовался",
        "Администратор #1 создал пользователя #2",
        "Администратор #1 отредактировал пользователя #2",
        "Администратор #1 сбросил пароль пользователю #2",
        "Администратор #1 создал менеджера #2",
//
        'Менеджер #1 изменил свои данные',
        'Менеджер #1 создал объект <span class="action_item">#2</span>',
        'Менеджер #1 отредактировал объект <span class="action_item">#2</span>',
        'Менеджер #1 добавил к объекту <span class="action_item">#2</span> ссылку #3',
        'Менеджер #1 удалил у объекта <span class="action_item">#2</span> ссылку #3',
//
        'Менеджер #1 написал сообщение в тикет #2',
        'Менеджер #1 закрыл тикет #2',
        'Менеджер #1 открыл тикет #2',
        'Менеджер #1 Добавил сообщение #2 в тикет #3',
        'Менеджер #1 Удалил сообщение #2 из тикета #3',
//
        'Менеджер #1 заблокировал объект <span class="action_item">#2</span>',
        'Менеджер #1 разблокировал объект <span class="action_item">#2</span>',
        'Менеджер #1 привязал объект <span class="action_item">#2</span> к пользователю <span class="action_item">#3</span>',
        'Менеджер #1 отвязал объект <span class="action_item">#2</span> от пользователя <span class="action_item">#3</span>',
        */
    );

    /* список методов добавление элементов активности
    * @param $activity
    */
    private function _check_activity($activity)
    {
        if (!in_array($activity, $this->activities)) {
            die("undefined activity");
        }
    }

    public function addSystemActivity($activity_name, $activity_type, $data_1, $data_2, $data_3){
        return $this->add($activity_name, $activity_type, "system", 0, $data_1, $data_2, $data_3);
    }

    public function addOrdererActivity($activity_name, $activity_type, $orderer_id, $data_1, $data_2, $data_3){
        return $this->add($activity_name, $activity_type, "orderer", $orderer_id, $data_1, $data_2, $data_3);
    }

    public function addManagerActivity($activity_name, $activity_type, $manager_id, $data_1, $data_2, $data_3){
        return $this->add($activity_name, $activity_type, "manager", $manager_id, $data_1, $data_2, $data_3);
    }

    public function addCompanyActivity($activity_name, $activity_type, $company_agent_id, $data_1, $data_2, $data_3){
        return $this->add($activity_name, $activity_type, "company_agent", $company_agent_id, $data_1, $data_2, $data_3);
    }

    public function add($activity_name, $activity_type, $owner_type, $owner_id, $data_1, $data_2 = "-", $data_3 = "-")
    {
        $this->_check_activity($activity_name);
        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activity = array(
            'name' => $activity_name,
            'type' => $activity_type,
            'data_1' => $data_1,
            'data_2' => $data_2,
            'data_3' => $data_3,
            'owner_id' => $owner_id,
            'owner_type' => $owner_type,
        );
        if ($this->Activities->save($activity)) {
            return true;
        } else {
            return false;
        }
    }

    public function getActivityList($page = null, $sort = null, $type = null, $manager = null, $activity_name = null){
        $show_count = 20;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        if($sort == null){
            $sort = "created";
        }

        if($type!=null){
            $type_query = array("type" => $type);
        } else {
            $type_query = [];
        }
        if($manager!=null){
            $manager_query = array("user_id" => $manager);
        } else {
            $manager_query = [];
        }

        $activity_string_search = "";
        if (!empty($activity_name)) {
            $this->_check_activity($activity_name);
            $activity_string_search = array("activity" => $activity_name);
        }

        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activities =  $this->Activities->find('all',
            array(
                'conditions' => array(
                    $activity_string_search,
                    $manager_query,
                    $type_query

                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array("$sort DESC")
            )
        );
        return $activities;
    }

    public function getActivityCount($type = null, $manager = null, $activity_name = null){
        if($type!=null){
            $type_query = array("type" => $type);
        } else {
            $type_query = [];
        }
        if($manager!=null){
            $manager_query = array("user_id" => $manager);
        } else {
            $manager_query = [];
        }

        $activity_string_search = "";
        if (!empty($activity_name)) {
            $this->_check_activity($activity_name);
            $activity_string_search = array("activity" => $activity_name);
        }

        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activities =  $this->Activities->find('count',
            array(
                'conditions' => array(
                    $activity_string_search,
                    $manager_query,
                    $type_query

                ),
            )
        );
        return $activities;
    }

    public function get_activity_list($activity_name = "", $manager_id = "", $data_1 = "")
    {
        $activity_string_search = "";
        if (!empty($activity_name)) {
            $this->_check_activity($activity_name);
            $activity_string_search = array("activity" => $activity_name);
        }
        $manager_string_search = "";
        if (!empty($manager_id)) {
            $manager_string_search = array("manager_id" => $manager_id);
        }
        $data_string_search = "";
        if (!empty($data_1)) {
            $data_string_search = array("data_1" => $data_1);
        }

        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activities =  $this->Activities->find('all',
            array(
                'conditions' => array(
                    $activity_string_search,
                    $manager_string_search,
                    $data_string_search
                )
            )
        );
        return $activities;
    }

    public function get_names()
    {
        return $this->activity_names;
    }

    public function get_activity_aliases()
    {
        return $this->activities;
    }

    public function send_sms($phone_number, $content){
        $sms_api_key = Configure::read('SMS_PILOT_API_KEY');
        //file_get_contents("http://smspilot.ru/api.php?send=test&to=79237854880&apikey=$sms_api_key");

        $sender = 'INFORM';//'Личный Кабинет Поставщика TERPIKKA'; //  имя отправителя из списка https://smspilot.ru/my-sender.php
        $url = 'https://smspilot.ru/api.php' .
            '?send=' . urlencode($content)
            . '&to=' . urlencode($phone_number)
            . '&from=' . $sender
            . '&apikey=' . $sms_api_key
            . '&format=json';
        $json = file_get_contents($url);
        //echo $json . '<br/>';
        // {"send":[{"server_id":"10000","phone":"79037672215","price":"1.68","status":"0"}],"balance":"11908.50","cost":"1.68"}
        $j = json_decode($json);
        if (!isset($j->error)) {
            $this->Log->add("SMS успешно отправлено, server_id=" . $j->send[0]->server_id . " Пользователь: " . $phone_number . " Содержимое: "  . $content, "warning");
            return true;
        } else {
            $this->Log->add("SMS не отправлено!" . " Пользователь: " . $phone_number . " Содержимое: " . $content, "warning");
            return false;
        }
    }

}