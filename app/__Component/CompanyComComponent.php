<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');
/**
 * Компонент Компания
 */
class CompanyComComponent extends Component
{
	public $components = array(
	    'CityCom',
        'Region',
		'Session',
		'Error'
	);

	public $controller;
	public $user_data;
	public $user_id;

    public $uses = array(
        'Error'
    );

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function company_agent_data()
	{
		//общие данные пользователя
		$company_agent_id = $this->Session->read('company_agent_id');
        $modelName = 'Company_Agent';
        $this->Company_Agent = ClassRegistry::init($modelName);

        $user_data = $this->Company_Agent->find('first',
            array(
                'conditions' => array(
                    'id' => $company_agent_id
                )
            )
        );
		$this->user_data = $user_data;
		$this->controller->set('company_agent_data', $user_data);
		return $user_data;
	}

	public function company_agent_id(){
		return $this->Session->read('company_agent_id');
	}

    public function company_id(){
        return $this->Session->read('company_id');
    }

    public function get_companies(){
        $modelName = 'Company';
        $this->Company = ClassRegistry::init($modelName);
        $companies =  $this->Company->find('all',
            array(
                'fields' => array(
                    'id',
                    'company_name',
                 )
            )
        );
        return $companies;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCompanyByid($id){
        $modelName = 'Company';
        $this->Company = ClassRegistry::init($modelName);
        $company =  $this->Company->find('all',
            array(
                'conditions' => array(
                    'id' => $id,
                )
            )
        );
        return $company;
    }

    //проверка владения магазином
    public function has_owner_rights($shop_id){
        $modelName = 'Shop';
        $this->Shop = ClassRegistry::init($modelName);
        return $this->Shop->find("count",
            array(
                'conditions' => array(
                    'id' => $shop_id,
                    'company_id' => $this->company_id()
                ),
            )
        );
    }

    //список id магазинов поставщика
    public function shop_list(){
        $modelName = 'Shop';
        $this->Shop = ClassRegistry::init($modelName);
        $shop_list =  $this->Shop->find("all",
            array(
                'conditions' => array(
                    'company_id' => $this->company_id(),
                ),
                'order' => array(
                    'id DESC'
                )
            )
        );
        $shop_ids = [];
        foreach ($shop_list as $shop_item){
            $shop_ids[] = $shop_item['Shop']['id'];
        }
        return $shop_ids;
    }

    public function shop_list_full($id){
        $company_id = $id ?? $this->company_id();
        $modelName = 'Shop';
        $this->Shop = ClassRegistry::init($modelName);
        $shop_list =  $this->Shop->find("all",
            array(
                'conditions' => array(
                    'company_id' => $company_id,
                ),
                'order' => array(
                    'shop_name ASC'
                )
            )
        );
        return $shop_list;
    }

    public function findShopByName($name){
        $modelName = 'Shop';
        $this->Shop = ClassRegistry::init($modelName);
        $shop_check_exists =  $this->Shop->find("count",
            array(
                'conditions' => array(
                    'company_id' => $this->company_id(),
                    'shop_name' => $name
                ),
            )
        );
        return $shop_check_exists;
    }

    public function company_comments($company_id){
        $modelName = 'Company_Rating';
        $this->Company_Rating = ClassRegistry::init($modelName);
        $list =  $this->Company_Rating->find("all",
            array(
                'conditions' => array(
                    'company_id' => $company_id,
                ),
                'order' => array(
                    'id ASC'
                )
            )
        );
        return $list;
    }

    public function company_comment_count($company_id){
        $modelName = 'Company_Rating';
        $this->Company_Rating = ClassRegistry::init($modelName);
        return $this->Company_Rating->find("count",
            array(
                'conditions' => array(
                    'company_id' => $company_id,
                ),
            )
        );
    }

    /** импортирование магазина
     * @param $shop_name
     * @param $shop_city
     * @param $shop_region
     * @param $shop_address
     * @param null $shop_phone
     * @param string $shop_open
     * @param string $shop_close
     * @return bool
     */
    public function import_shop($shop_name, $shop_city, $shop_region, $shop_address, $shop_phone = null, $shop_open = "", $shop_close = ""){
        $modelName = 'Shop';
        $this->Shop = ClassRegistry::init($modelName);

        if($this->findShopByName($shop_name) >0){
            return null;
        }

        $city_id = $this->CityCom->getCityIdByName($shop_city) ?? null;
        if($city_id ==  null) {
            return false;
        }

        $region_id = $this->Region->getRegionIdByName($shop_region) ?? null;
        if($region_id ==  null) {
            //return false;
        }

        $coords = $this->CityCom->getCoordByAddress($shop_city . " " . $shop_address);
        if($coords !== null){
            $shop_latitude = $coords[0];
            $shop_longitude = $coords[1];
        } else {
            $shop_latitude = 0;
            $shop_longitude = 0;
        }

        //TODO HARDCODE
        $region_id = 1;

        $new_shop = [
            'shop_name' => $shop_name,
            'city_id' => $city_id,
            'company_id' => $this->company_id(),
            'status' => 'init',
            'region_id' => $region_id,
            'address' => $shop_address,
            'shop_latitude' => $shop_latitude,
            'shop_longitude' => $shop_longitude,
            'phone' => $shop_phone,
            'shop_open' => $shop_open,
            'shop_close' => $shop_close,
        ];

        $this->Shop->save($new_shop);
        return $this->Shop->id;
    }

}