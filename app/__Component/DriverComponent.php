<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Водитель
 */
class DriverComponent extends Component
{
    public $components = array(
        'Log',
        'Validator',
        'Session',
        'Flash'
    );

    public $driver_data_is_incorrect = "Данные водителя переданы некорректно";
    public $incorrect_firstname = "Некорректное имя водителя";
    public $incorrect_lastname = "Некорректная фамилия водителя";
    public $incorrect_phone_number = "Некорректный номер телефона водителя";
    public $unknown_status = "Передан неизвестный статус";
    public $driver_not_found = "Водитель не найден";


    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    public function setup()
    {
        $modelName = "Driver";
        $this->Driver = ClassRegistry::init($modelName);
        $modelName = "Delivery_Route";
        $this->Delivery_Route = ClassRegistry::init($modelName);
    }

    /**
     * @param $name
     * @return array|null
     */
    public function findDriversByName($name){
        $this->setup();
        $drivers = $this->Driver->find("first",
            array('conditions' =>
                array(
                    'or' => array(
                        array(
                            'lastname LIKE' => "%$name%",
                        ),
                        array(
                            'firstname LIKE' => "%$name%",
                        ),
                    ),
                ),
            )
        );
        if (count($drivers) == 0) {
            return null;
        }
        return $drivers;
    }

    /**
     * @param $id
     * @return array|null
     */
    public function getDriverById($id)
    {
        $this->setup();
        $driver = $this->Driver->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                ),
            )
        );
        if (count($driver) == 0) {
            return null;
        }
        return $driver;

    }

    /**
     * @return array|null
     */
    public function getActiveDrivers()
    {
        $this->setup();
        $drivers = $this->Driver->find("all",
            array('conditions' =>
                array(
                    'status' => 'active'
                ),
                'order' => array('id DESC')
            )
        );
        if (count($drivers) == 0) {
            return null;
        }
        return $drivers;
    }

    /**
     * @param $id
     * @param $data
     * @return array|bool
     */
    public function setDriverData($id, $data)
    {
        $validate = $this->validateDriver($data, "update");
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->driver_data_is_incorrect . " " . $validate["error"] ];
        }
        $this->setup();
        $this->Driver->id = $id;
        $this->Driver->save($data);
        return true;
    }

    /**
     * @param $data
     * @param $action
     * @return array
     */
    private function validateDriver($data, $action)
    {
        $valid_status = ['active', 'blocked'];
        if($action == "create"){
            if (empty($data['firstname']) OR !$this->Validator->valid_firstname($data['firstname'])) {
                return ["status" => "error", "error" => $this->incorrect_firstname];
            }
            if (empty($data['lastname']) OR !$this->Validator->valid_firstname($data['lastname'])) {
                return ["status" => "error", "error" => $this->incorrect_lastname];
            }
            if (empty($data['phone_number']) OR !$this->Validator->valid_phone_number($data['phone_number'])) {
                return ["status" => "error", "error" => $this->incorrect_phone_number];
            }
        } else {
            if (!empty($data['firstname']) AND !$this->Validator->valid_firstname($data['firstname'])) {
                return ["status" => "error", "error" => $this->incorrect_firstname];
            }
            if (!empty($data['lastname']) AND !$this->Validator->valid_firstname($data['lastname'])) {
                return ["status" => "error", "error" => $this->incorrect_lastname];
            }
            if (!empty($data['phone_number']) OR !$this->Validator->valid_phone_number($data['phone_number'])) {
                return ["status" => "error", "error" => $this->incorrect_phone_number];
            }
            if (!empty($data['status']) AND !in_array($data['status'], $valid_status)){
                return ["status" => "error", "error" => $this->unknown_status];
            }
        }
        return ["status" => "success"];
    }

    /**
     * @param $id
     * @return bool
     */
    public function setDriverActive($id)
    {
        $fields = ['status' => 'active'];
        $this->setup();
        $this->Driver->id = $id;
        $this->Driver->save($fields);
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function setDriverBlocked($id)
    {
        $fields = ['status' => 'blocked'];
        $this->setup();
        $this->Driver->id = $id;
        $this->Driver->save($fields);
        return true;
    }

    /**
     * @param $date
     * @return bool|null
     */
    public function getWorkingDriverByDate($date)
    {
        $this->setup();
        if (!$this->Validator->valid_date($date)) {
            return false;
        }
        $working_drivers = $this->Delivery_Route->find("all",
            array('conditions' =>
                array(
                    'route_date' => $date
                ),
                'joins' => array(
                    array(
                        'table' => 'drivers',
                        'alias' => 'Driver',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Delivery_Route.driver_id = Driver.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Driver.*',
                    'Delivery_Route.*'
                ),
            )
        );
        if (count($working_drivers) == 0) {
            return null;
        }
        return $working_drivers['Driver'];
    }

    /**
     * @param int $driver_id
     * @return bool|null|mixed
     */
    public function getWorkingDateByDriverId($driver_id)
    {
        $this->setup();
        if ($this->getDriverById($driver_id)) {
            return false;

        }
        $working_dates = $this->Delivery_Route->find("all",
            array('conditions' =>
                array(
                    'driver_id' => $driver_id
                )
            )
        );
        if (count($working_dates) == 0) {
            return null;
        }
        return $working_dates['Delivery_Route'];
    }

    /**
     * @param $data
     * @return array|null
     */
    public function createDriver($data)
    {
        $validate = $this->validateDriver($data, "create");
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->driver_data_is_incorrect . " " . $validate["error"] ];
        }
        $this->setup();

        $data['auth_code'] = "";
        $data['status'] = 'active';
        $this->Driver->create();
        $this->Driver->save($data);
        if($this->Driver->id>0){
            return $this->Driver->id;
        }
        return null;
    }

    public function getAllDrivers()
    {
        $this->setup();
        $drivers = $this->Driver->find("all",
            array('conditions' =>
                array(
                    //'status' => 'active'
                ),
                'order' => array('id DESC')
            )
        );
        if (count($drivers) == 0) {
            return null;
        }
        return $drivers;
    }

    /**
     * @param $driver_id
     * @param $data
     * @return array|null
     */
    public function saveDriver($driver_id, $data)
    {
        $this->setup();
        if ($this->getDriverById($driver_id) == null) {
            return ["status" => "error", "error" => $this->driver_not_found ];
        }
        $validate = $this->validateSaveDriver($data);
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->driver_data_is_incorrect . " " . $validate["error"] ];
        }

        $this->Driver->id = $driver_id;
        $this->Driver->save($data);
        if($this->Driver->id>0){
            return $this->Driver->id;
        }
        return null;
    }

    /**
     * @param $data
     * @return array
     */
    private function validateSaveDriver($data)
    {
        if (!$this->Validator->valid_phone_number($data['phone_number'])){
                return ["status" => "error", "error" => $this->incorrect_phone_number];
        }
        if (!$this->Validator->valid_firstname($data['firstname'])){
            return ["status" => "error", "error" => $this->incorrect_firstname];
        }

        if (!$this->Validator->valid_lastname($data['lastname'])){
            return ["status" => "error", "error" => $this->incorrect_lastname];
        }
        return ["status" => "success"];
    }

    public function deleteDriver($driver_id)
    {
        $this->setup();
        if ($this->getDriverById($driver_id) == null) {
            return ["status" => "error", "error" => $this->driver_not_found];
        }
        $this->Driver->id = $driver_id;
        $this->Driver->delete();
        return true;
    }


}