<?php
App::uses(
    'AppController', 'Controller'
);
App::uses('Model', 'Model');
App::uses('L10n', 'L10n');

/**
 * Компонент парсер
 */
class ParserComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Manufacturer',
        'ShopOwnerCom',
        'ProductCom',
        'Country',
        'Log',
    );

    function setupModels()
    {
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);

        $modelName = "ProductCategory";
        $this->ProductCategory = ClassRegistry::init($modelName);
    }

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
        $this->setupModels();
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_product_title($content)
    {
        $tag_regex = '/ data-test="product-title">(.*?)<\/h1>/si';
        preg_match($tag_regex,
            $content,
            $matches);
        return $matches[1];
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_product_descr($content)
    {
        $tag_regex = '/ data-test="product-description">(.*?)<\/p>/si';
        preg_match($tag_regex,
            $content,
            $matches);
        return $matches[1];
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_product_spec($content)
    {
        $tag_regex = '/ class="specification--table">(.*?)<\/table>/si';
        preg_match($tag_regex,
            $content,
            $matches);
        return $matches[1] ?? null;
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_product_spec_2($content)
    {
        $tag_regex = '/ class="text--formated">(.*?)<\/div>/si';
        preg_match($tag_regex,
            $content,
            $matches);
        return $matches[1];
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_product_image($content)
    {
        $tag_regex = '/srcset="(.*?)"/si';
        preg_match_all($tag_regex,
            $content,
            $matches);
        return $matches[1];
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_price($content)
    {
        $tag_regex = '/ itemprop="price" content="(.*?)"/si';
        preg_match($tag_regex,
            $content,
            $matches);
        return $matches[1];
    }

    /***
     * @param $content
     * @return mixed
     */
    private function parse_categories($content)
    {
        $tag_regex = '/ rel="v:url" property="v:title">(.*?)<\/a>/si';
        preg_match_all($tag_regex,
            $content,
            $matches);
        return $matches[1];
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_weight($content)
    {
        $tag_regex = '/Вес брутто:<\/strong>(.*?)<\/p>/si';
        preg_match($tag_regex,
            $content,
            $matches);
        if (key_exists(1, $matches)) {
            return $matches[1];
        } else return [];
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_other_links($content)
    {
        $tag_regex = '/\/catalog\/[0-9]+\/[0-9]+\//si';
        preg_match_all($tag_regex,
            $content,
            $matches);
        return $matches;
    }

    /**
     * @param $product_spec
     * @return array
     */
    private function parse_specs($product_spec)
    {
        $values = [];
        $names = [];
        if (mb_strlen($product_spec) > 0) {
            $DOM = new DOMDocument("", "utf-8");
            $DOM->loadHTML('<?xml encoding="utf-8" ?>' . $product_spec);
            $Header = $DOM->getElementsByTagName('td');
            $r = 0;

            foreach ($Header as $NodeHeader) {
                $param = trim($NodeHeader->textContent);
                echo $param;
                if ($r % 2 == 0) {
                    $names[] = $param;
                } else {
                    $values[] = $param;
                }
                $r++;
            }
        }
        $specs = [];
        if (count($names) > 0) {
            for ($t = 0; $t < count($names); $t++) {
                $specs[$names[$t]] = $values[$t];
            }
        }
        return $specs;
    }

    /**
     * @param $product_spec
     * @return array
     */
    private function parse_specs_2($product_spec)
    {

        $splitted_text = explode("<p><strong>", $product_spec);
        $specs = [];
        foreach ($splitted_text as $splitted_item) {
            $splitted_item = str_replace("<p><strong>", "", $splitted_item);
            $splitted_item = str_replace("</p>", "", $splitted_item);
            $splitted_item = str_replace("</strong>", "", $splitted_item);
            $splitted_item_array = explode(":", $splitted_item);
            if (!key_exists(1, $splitted_item_array)) {
                continue;
            }
            $spec_value = trim($splitted_item_array[1]);
            $spec_key = trim($splitted_item_array[0]);
            if (mb_substr($spec_value, mb_strlen($spec_value, "UTF-8") - 1, 1, "UTF-8") == ".") {
                $spec_value = mb_substr($spec_value, 0, mb_strlen($spec_value, "UTF-8") - 1, "UTF-8");
            }
            if (!empty($spec_key) && !empty($spec_value)) {
                $specs[$spec_key] = $spec_value;
            }
        }
        return $specs;
    }

    private function addParsedCategory($name, $status = "moderate", $parent_id = 0)
    {
        $modelName = "ProductCategory";
        $this->ProductCategory = ClassRegistry::init($modelName);
        $new_cat = array(
            'name' => $name,
            'parent_id' => $parent_id,
            'status' => $status
        );
        $this->ProductCategory->create();
        $this->ProductCategory->save($new_cat);
        $product_category_id = $this->ProductCategory->id;
        return $product_category_id;
    }

    private function categoryParser($cat_list)
    {
        krsort($cat_list);
        $parent_id = 0;
        foreach ($cat_list as $cat_name) {
            if ($cat_name == "Главная") continue;
            $cat_id = $this->ProductCom->findCategoryByName($cat_name);
            if ($cat_id == 0) {
                $cat_id = $this->addParsedCategory($cat_name, "moderate", $parent_id);
            }
            $parent_id = $cat_id;
        }
        return $cat_id;

    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_details($content)
    {
        $tag_regex = '/ class="tabs__product-details .*?>(.*?)<\/div>/si';
        preg_match($tag_regex,
            $content,
            $matches);
        return $matches[1] ?? null;
    }

    /**
     * @param $url
     * @param $id
     */
    public function parsePetrovichUrl($url, $id)
    {
        $parsed_array = [];
        $parsed_content = file_get_contents($url);

        //категории товара
        $product_cats = $this->parse_categories($parsed_content);
        $parsed_array['cat_list'] = $product_cats;

        $category_id = $this->categoryParser($product_cats);
        if ($category_id == 0) {
            pr($product_cats);
            exit;
        }

        //название товара
        $product_title = $this->parse_product_title($parsed_content);
        $parsed_array['name'] = $product_title;

        //описание
        $product_description = $this->parse_product_descr($parsed_content);
        $parsed_array['description'] = $product_description;

        //характеристики товара
        $product_spec = $this->parse_product_spec($parsed_content);
        $parsed_array['specs'] = $this->parse_specs($product_spec);

        //второй способ парсинга характеристик
        $product_spec = $this->parse_product_spec_2($parsed_content);
        $parsed_array['specs_2'] = $this->parse_specs_2($product_spec);

        //Детали парсинга характеристик
        $product_details = $this->parse_details($parsed_content);
        $product_details = strip_tags($product_details);
        $parsed_array['details'] = $product_details;

        //массив изображений
        $product_image_test_array = [];
        $product_images = $this->parse_product_image($parsed_content);
        foreach ($product_images as $image_url) {
            if (substr($image_url, 0, 2) == "//") {
                $product_image_test = "http:" . $image_url;
                if (substr_count($product_image_test, " 500w") > 0) {
                    $product_image_test = str_replace(" 500w", "", $product_image_test);
                }
                if (!in_array($product_image_test, $product_image_test_array)) {
                    $product_image_test_array[] = $product_image_test;
                }
            }
        }
        $parsed_array['images'] = $product_image_test_array;
        if (!empty($product_image_path) AND filter_var($product_image_path, FILTER_VALIDATE_URL) != FALSE) {
            $product_image_file = file_get_contents($product_image_path);
            $parsed_array['image'] = $product_image_file;
        }

        //цена
        $parsed_price = $this->parse_price($parsed_content);
        $parsed_array['price'] = $parsed_price;

        //вес брутто
        $parsed_weight = $this->parse_weight($parsed_content);
        $parsed_array['weight'] = $parsed_weight;

        // парсинг других ссылок на товары для автиматического перехода
        $links = $this->parse_other_links($parsed_content);

        $this->Parser->id = $id;
        $parse_result = [
            'status' => 'success',
            'result' => print_r($parsed_array, true),
        ];

        $this->Parser->save($parse_result);
        foreach ($links[0] as $key => $link) {
            $complete_link = "https://petrovich.ru" . $link;
            if (filter_var($complete_link, FILTER_VALIDATE_URL) == FALSE) {
                continue;
            }
            $check_url = $this->Parser->find("count",
                array('conditions' =>
                    array(
                        'url' => $complete_link,
                    )
                )
            );
            if ($check_url == 0) {
                $new_task_parse = [
                    'url' => $complete_link,
                    'status' => 'wait',
                    'source' => 'petrovich'
                ];
                $this->Parser->create();
                $this->Parser->save($new_task_parse);
            }
        }

        // создание продукта на основе запарсенных данных
        $this->createParsedProduct($parsed_array, $category_id, $url, "petrovich");
    }

    public function createParsedProduct($parse_result, $category_id, $url, $source)
    {

        if (empty($parse_result)) {
            die("Empty parsed product");
        }
        if (empty($category_id)) {
            die("Empty category_id of product");
        }
        if (empty($url)) {
            die("Empty url of product");
        }
        $name = prepare_import_field($parse_result['name']) ?? null;
        $description = prepare_import_field($parse_result['description']) ?? null;
        $product_id = $this->ProductCom->findProductByName($name);

        $details = prepare_import_field($parse_result['details']) ?? null;
        if (!empty($details)) {
            $description = $description . " " . $details;
        }

        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);
        if ($product_id == null) {

            /*
            // определение id страны
            $country_id = $this->Country->getCountryIdByName($country_of_origin);
            if ($country_id == 0) {
                //$brand_id = $this->Country->saveBrand($vendor_name);
            }*/

            $new_product = [
                'description' => $description,
                'product_name' => $name,
                'product_model' => "",
                'prefix' => "",
                'status' => 'hidden',
                'brand_id' => "",
                'brand_barcode' => "",
                'category_id' => $category_id,
                'country_id' => 0,//$country_id
                'source' => $source
            ];
            $this->Product->create();
            $this->Product->save($new_product);
            $product_id = $this->Product->id;
            $system_barcode = $this->ProductCom->systemBarcode($product_id, $category_id);
            $this->Product->id = $product_id;
            $this->Product->save(
                array('system_barcode' => $system_barcode)
            );
        } else {
            echo "товар найден " . $product_id;

            $null = null;
            $category_id = $this->ProductCom->getProductField("category_id", $product_id, $null);

            $new_product = [
                'description' => $description,
                'product_name' => $name,
                'prefix' => "",
                'product_model' => "",
                'status' => 'hidden',
                'brand_id' => "",
                'brand_barcode' => "",
                'category_id' => $category_id,
                'country_id' => 0,
                'source' => $source
            ];

            $system_barcode = $this->ProductCom->systemBarcode($product_id, $category_id);
            $this->Product->id = $product_id;
            $this->Product->save(
                array(
                    'system_barcode' => $system_barcode
                )
            );

        }

        //загрузка изображений по ссылке, если требуется
        if (count($parse_result['images']) > 0) {
            foreach ($parse_result['images'] as $image) {
                if (!empty($image) AND filter_var($image, FILTER_VALIDATE_URL) != FALSE) {
                    // проверка, не загружалось ли это изображение ранее
                    if (!$this->ProductCom->find_image_by_hash($image, $product_id)) {
                        $image_id = $this->ProductCom->load_image_by_url($image, $system_barcode, $product_id, 0);
                    }
                }
            }
        }

        $package_weight_value = 0;
        $package_count_value = 0;
        $package_width_value = 0;
        $package_height_value = 0;
        $package_length_value = 0;
        if (count($parse_result['specs']) > 0) {
            foreach ($parse_result['specs'] as $filter_name => $filter_value) {
                if ($filter_name == "Количество в упаковке") {
                    $package_count_value = str_replace(" шт", "", $filter_value);
                } else if ($filter_name == "Длина") {
                    $package_length_value = str_replace(" мм", "", $filter_value);
                } else if ($filter_name == "Ширина") {
                    $package_height_value = str_replace(" мм", "", $filter_value);
                } else if ($filter_name == "Высота") {
                    $package_width_value = str_replace(" мм", "", $filter_value);
                } else if ($filter_name == "Вес" OR $filter_name == "Вес брутто") {
                    $package_weight_value = str_replace(" мм", "", $filter_value);
                } else {
                    $this->parseFilterParam($filter_name, $filter_value, $category_id, $product_id);
                }
            }
        }

        if (count($parse_result['specs_2']) > 0) {
            foreach ($parse_result['specs_2'] as $filter_name => $filter_value) {
                if ($filter_name == "Количество в упаковке") {
                    $package_count_value = str_replace(" шт", "", $filter_value);
                } else if ($filter_name == "Длина") {
                    $package_length_value = str_replace(" мм", "", $filter_value);
                } else if ($filter_name == "Ширина") {
                    $package_height_value = str_replace(" мм", "", $filter_value);
                } else if ($filter_name == "Высота") {
                    $package_width_value = str_replace(" мм", "", $filter_value);
                } else if ($filter_name == "Вес" OR $filter_name == "Вес брутто") {
                    $package_weight_value = str_replace(" мм", "", $filter_value);
                } else {
                    $this->parseFilterParam($filter_name, $filter_value, $category_id, $product_id);
                }
            }
        }

        if ($package_weight_value == 0) {
            $package_weight_value = $parse_result['weight'];
            if (substr_count($package_weight_value, "кг") > 0) {
                $package_weight_value = str_replace("кг", "", $package_weight_value);
                $package_weight_value = floatval(trim($package_weight_value)) * 1000;
            }

        }
        //заглушка для веса, Петрович
        if ($package_weight_value == null) {
            $package_weight_value = 0;
        }

        // создание предложения от Петрович
        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);
        $check_offer_exists = $this->Shop_Product->find("count",
            array('conditions' =>
                array(
                    'shop_url' => $url,
                )
            )
        );
        if ($check_offer_exists > 0) {
            return;
        }
        $petrovich_company_id = 1;
        $base_price = intval($parse_result['price']);
        $offer = [
            'company_id' => $petrovich_company_id,
            'base_price' => $base_price,
            'oldprice' => $base_price,
            'shop_url' => $url,
            'preorder_only' => "",
            'delivery' => "",
            'pickup' => "",
            'delivery_cost' => "",
            'delivery_days' => "",
            'delivery_order_before' => "",
            'sales_notes' => "",
            'barcode' => "",
            'product_id' => $product_id,
            'actual' => "false",
            'package_count' => $package_count_value,
            'package_width' => $package_width_value,
            'package_height' => $package_height_value,
            'package_length' => $package_length_value,
            'package_weight' => $package_weight_value,
        ];

        $this->Shop_Product->create();
        $this->Shop_Product->save($offer);
        $offer_id = $this->Shop_Product->id;
    }

    /** поиск, идентификация и добавление новых характеристик товара/категорий и их значений
     *
     * @param $filter_name
     * @param $filter_value
     * @param $category_id
     * @param $product_id
     */
    private function parseFilterParam($filter_name, $filter_value, $category_id, $product_id)
    {

        $filter_name = mb_ucfirst(mb_strtolower($filter_name));

        $filter_value_raw = $filter_value;

        // обработка значения фильтра для добавления - поиск единиц измерения
        $filter_value = $this->ProductCom->filteringParamValue($filter_value);
        $unit = $filter_value['unit'];
        $filter_value = $filter_value['clear_value'];
        if ($filter_value == "") {
            return;
        }

        //определение бренда
        if ($filter_name == "Бренд") {
            // проверка, существует ли вендор, если нет - добавляем его
            $brand_id = $this->Manufacturer->checkBrandExists($filter_value);
            if ($brand_id == 0) {
                $brand_id = $this->Manufacturer->saveBrand($filter_value);
            }
            $check_brand_connection = $this->Product->find("all",
                array('conditions' =>
                    array(
                        'id' => $product_id,
                    )
                )
            );
            if ($check_brand_connection['Product']['brand_id'] != $brand_id) {
                $this->Product->id = $product_id;
                $this->Product->save(
                    array('brand_id' => $brand_id)
                );
                $this->Product->save();
            }
        } //определение страны
        else if (
            $filter_name == "Производитель" OR
            $filter_name == "Страна" OR
            $filter_name == "Страна-производитель" OR
            $filter_name == "Страна производства"
        ) {
            $filter_value = str_replace("Республика", "", $filter_value);
            $filter_value = trim($filter_value);
            // проверка, существует ли страна, если да, привязываем
            $country_id = $this->Country->getCountryIdByName($filter_value);
            $check_product_data = $this->Product->find("first",
                array('conditions' =>
                    array(
                        'id' => $product_id,
                    )
                )
            );
            if ($country_id > 0 && $check_product_data['Product']['country_id'] != $country_id) {
                $this->Product->id = $product_id;
                $this->Product->save(
                    array('country_id' => $country_id)
                );
                $this->Product->save();
            }
            // если страну не удалось идентифицировать, ну и ладно
        } else {
            echo $filter_name . " " . pr($filter_value) . " " . $category_id;
            // тип фильтра относится к прочим характеристикам
            $filter_id = $this->ProductCom->find_filter_by_name($filter_name, $category_id);
            // если по запарсенному названию определен уже имеющийся в БД параметр, проверяем, есть ли данное значение в нем
            if ($filter_id > 0) {
                $check_param_value = $this->ProductCom->check_filter_value($filter_id, $filter_value);
                if ($check_param_value == 0) {
                    $value_id = $this->ProductCom->add_filter_value($filter_id, $filter_value);
                } else {
                    $value_id = $this->ProductCom->findParamValueByName($filter_id, $filter_value);
                }
                $this->ProductCom->addParamValueToProduct($value_id, $filter_id, $product_id);
            } else {
                // TODO добавить специальный фильтр для определения единиц измерения из запарсенных значений
                $filter_id = $this->ProductCom->add_new_param_by_import($filter_name, $category_id, $unit);
                $value_id = $this->ProductCom->add_filter_value($filter_id, $filter_value, $product_id);
            }
        }
    }

    public function parseMain($source)
    {
        $this->Parser = ClassRegistry::init("Parser");
        $this->Shop_Product = ClassRegistry::init("Shop_Product");
        while (true) {
            $success_parsed = $this->Parser->find("count",
                array('conditions' =>
                    array(
                        'status' => 'success',
                        'source' => $source
                    )
                )
            );

            $parser_list = $this->Parser->find("all",
                array('conditions' =>
                    array(
                        'status' => 'wait',
                        'source' => $source
                    ),
                    'order' => 'id ASC',
                    'limit' => 10
                )
            );
            if (count($parser_list) == 0) {
                die("all urls parsed");
            }
            foreach ($parser_list as $parse_item) {
                $parse_item = $parse_item['Parser'];
                $url = $parse_item['url'];
                $id = $parse_item['id'];

                $this->Parser->id = $id;
                $this->Parser->save(array('status' => 'active'));

                //проверка на то, нет ли данный ссылки в предложениях в уже запарсенных товарах
                $find_cur_url = $this->Shop_Product->find("count",
                    array('conditions' =>
                        array(
                            'shop_url' => $url,
                        )
                    )
                );
                if ($find_cur_url == 0) {
                    if ($source == "petrovich") {
                        $this->parsePetrovichUrl($url, $id);
                    } else if ($source == "obi") {
                        // TODO сделать парсер OBI и остальных поставщиков данных
                        //$this->parsePetrovichUrl($url, $id);
                    }
                }
            }
            if ($success_parsed > 50000) {
                die("test limit 10 has succeed");
            }

        }
    }

    public function parseMainFast($source)
    {
        $this->Parser = ClassRegistry::init("Parser");
        $this->Shop_Product = ClassRegistry::init("Shop_Product");
        $success_parsed = $this->Parser->find("count",
            array('conditions' =>
                array(
                    'status' => 'success',
                    'source' => $source
                )
            )
        );

        $parser_list = $this->Parser->find("all",
            array('conditions' =>
                array(
                    'status' => 'wait',
                    'source' => $source
                ),
                'order' => 'id ASC',
                'limit' => 25
            )
        );
        if (count($parser_list) == 0) {
            die("all urls parsed");
        }
        foreach ($parser_list as $parse_item) {
            $parse_item = $parse_item['Parser'];
            $url = $parse_item['url'];
            $id = $parse_item['id'];

            $this->Parser->id = $id;
            $this->Parser->save(array('status' => 'active'));

            //проверка на то, нет ли данный ссылки в предложениях в уже запарсенных товарах
            $find_cur_url = $this->Shop_Product->find("count",
                array('conditions' =>
                    array(
                        'shop_url' => $url,
                    )
                )
            );
            if ($find_cur_url == 0) {
                if ($source == "petrovich") {
                    $this->parsePetrovichUrl($url, $id);
                } else if ($source == "obi") {
                    // TODO сделать парсер OBI и остальных поставщиков данных
                    //$this->parsePetrovichUrl($url, $id);
                }
            }
        }
        if ($success_parsed > 50000) {
            die("test limit 10 has succeed");
        }
        die("all parsed");
    }

    public function parseCatPetrovich()
    {
        $source = 'petrovich';
        $this->ParserLink = ClassRegistry::init("ParserLink");
        $this->Parser = ClassRegistry::init("Parser");
        while (true) {
            $parser_list = $this->ParserLink->find("all",
                array(
                    'conditions' => array(
                        'status' => 'wait',
                        'source' => $source,
                    ),
                    'order' => 'id DESC'
                )
            );
            if (count($parser_list) == 0) {
                die("all urls parsed");
            }
            foreach ($parser_list as $parse_item) {
                $parse_elem = $parse_item['ParserLink'];
                $url = $parse_elem['url'];
                $id = $parse_elem['id'];

                // парсинг страницы категории
                $parsed_content = $this->parseUrlCat($url, $source);

                //категория берется в работу
                $task_parse = [
                    'status' => 'active',
                    'source' => $source,
                ];
                $this->ParserLink->id = $id;
                $this->ParserLink->save($task_parse);

                $product_links = $this->parse_other_links($parsed_content);
                foreach ($product_links[0] as $key => $link) {
                    $complete_link = "https://petrovich.ru" . $link;
                    if (filter_var($complete_link, FILTER_VALIDATE_URL) == FALSE) {
                        continue;
                    }
                    $check_url = $this->Parser->find("count",
                        array('conditions' =>
                            array(
                                'url' => $complete_link,
                                'source' => $source,
                            )
                        )
                    );
                    if ($check_url == 0) {
                        $new_task_parse = [
                            'url' => $complete_link,
                            'status' => 'wait',
                            'source' => $source,
                        ];
                        $this->Parser->create();
                        $this->Parser->save($new_task_parse);
                    }
                }
                //категория помечается как обработанная
                $task_parse = [
                    'status' => 'success'
                ];
                $this->ParserLink->id = $id;
                $this->ParserLink->save($task_parse);
            }
        }
    }

    public function parseUrlCat($url_cat, $source)
    {
        $parsed_content = file_get_contents($url_cat);
        $links = $this->parse_other_cat_links($parsed_content);

        foreach ($links[0] as $key => $link) {
            $complete_link = "https://petrovich.ru" . $link;
            if (filter_var($complete_link, FILTER_VALIDATE_URL) == FALSE) {
                continue;
            }
            $check_url = $this->ParserLink->find("count",
                array('conditions' =>
                    array(
                        'url' => $complete_link,
                        'source' => $source,
                    )
                )
            );
            if ($check_url == 0) {
                $new_task_parse = [
                    'url' => $complete_link,
                    'status' => 'wait',
                    'source' => $source
                ];
                $this->ParserLink->create();
                $this->ParserLink->save($new_task_parse);
            }
        }
        return $parsed_content;
    }

    /**
     * @param $content
     * @return mixed
     */
    private function parse_other_cat_links($content)
    {
        $tag_regex = '/\/catalog\/[0-9]+\//si';
        preg_match_all($tag_regex,
            $content,
            $matches);
        return $matches;
    }

    // Обновление parent_id КАТЕГОРИЙ
    private function categoryUpdater($cat_list)
    {
        //pr($cat_list);
        $real_parent_name = "";
        foreach ($cat_list as $cat_name) {
            if ($cat_name == "Главная") continue;
            $cat_name = trim($cat_name);
            $cat_id = $this->ProductCom->findCategoryByName($cat_name);
            if ($cat_id == 0) {
                echo " категория $cat_name не найдена";
                //$cat_id = $this->addParsedCategory($cat_name, "moderate", $parent_id);
            } else if ($cat_id > 0) {
                if ($real_parent_name == "") {
                    continue;
                }
                $real_cat_id = $this->ProductCom->findCategoryByName($real_parent_name);
//                echo " ПОСЛЕ СОХРАНЕНИЯ БУДЕТ -> категория $cat_name с id $cat_id лежит в родителе " . $real_parent_name . " с id  " . $real_cat_id . PHP_EOL;
//                echo "<br>";
                $this->ProductCom->setParentCategory($cat_id, $real_cat_id);
            }
            $real_parent_name = $cat_name;
        }

    }

    public function parsePetrovichUrlTest()
    {
        $this->ParserLink = ClassRegistry::init("ParserLink");
        $parser_list = $this->ParserLink->find("all",
            array('conditions' =>
                array(
                    'status' => 'wait',
                    'source' => 'petrovich'
                ),
                'order' => 'id DESC',
            )
        );
        if (count($parser_list) == 0) {
            die("all urls parsed");
        }
        foreach ($parser_list as $parse_item) {
            $parse_item = $parse_item['ParserLink'];
            $url = $parse_item['url'];
            $id = $parse_item['id'];

            $parsed_content = file_get_contents($url);

            //категории товара
            $product_cats = $this->parse_categories($parsed_content);
            if (count($product_cats) > 0) {
                $this->categoryUpdater($product_cats);
                $this->ParserLink->id = $id;
                $this->ParserLink->save(array('status' => 'success'));
            }
        }
        die("all parsed");
    }

    /**
     * @param $shop_product_id
     * @param $current_price
     * @return bool
     */
    public function realTimeParserProductPrice($shop_product_id, $current_price)
    {
        // получить урл товара
        // сделать парсинг страницы, забрать цену
        // сравнить цену с существующей, перезаписать ее в стоимости товара во всех карзинах и в самом товаре при отличии
        $product_company_id = $this->ProductCom->getShopProductField("company_id", $shop_product_id);
        // HARDCODE TODO
        if ($product_company_id != 1) {
            return false;
        }

        $product_url = $this->ProductCom->getShopProductField("shop_url", $shop_product_id);
        if (empty($product_url)) {
            return false;
        }
        //цена
        $parsed_price = $this->parse_price(file_get_contents($product_url));
        $parsed_price_formatted = $parsed_price;

        if ($parsed_price_formatted > 0) {
            $update_price_arr = [
                'price' => $parsed_price_formatted,
                'price_updated' => date("Y-m-d H:i:s")
            ];
            $this->Shop_Product = ClassRegistry::init("Shop_Product");
            $this->Shop_Product->id = $shop_product_id;
            $this->Shop_Product->save($update_price_arr);
            // обновление цены во всех корзинах
            $this->ProductCom->updateCartProductPrice($shop_product_id, $parsed_price_formatted);
        }
        return $parsed_price_formatted;


    }

    public function parsingPriceQuery()
    {
        // TODO HARDCODE
        $company_id = 1;// пока только петрович
        $required_price_parcing_products = $this->ProductCom->productListWithRequiredUpdatePriceParse($company_id);

         if(count($required_price_parcing_products)>0){
            foreach ($required_price_parcing_products as $product_item){
                $product_item = $product_item['Shop_Product'];
                $shop_item_id = $product_item['id'];
                $new_price = $this->realTimeParserProductPrice($shop_item_id, null);
                if ($new_price > 0) {
                    $update_price_arr = [
                        'price' => $new_price,
                        'price_updated' => date("Y-m-d H:i:s")
                    ];
                    $this->Shop_Product = ClassRegistry::init("Shop_Product");
                    $this->Shop_Product->id = $shop_item_id;
                    $this->Shop_Product->save($update_price_arr);
                    // обновление цены во всех корзинах
                    $this->ProductCom->updateCartProductPrice($shop_item_id, $new_price);
                }
            }
        }
    }


}