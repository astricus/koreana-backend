<?php

App::uses(
    'AppController', 'Controller'
);
App::uses('Model', 'Model');
App::uses('L10n', 'L10n');

/**
 * Компонент Модерация
 */
class ModerateComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Manufacturer',
        'CompanyCom',
        'Country',
        'ProductCom',
        'Log',
        'UserCom'
    );

    public $moderation_id;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function current_moderation_id($product_id)
    {
        if ($this->moderation_id == null) {
            $company_id = $this->CompanyCom->company_id();
            if ($company_id == 0) {
                $error_text = ProductComComponent::company_not_set;
                die($error_text);
            }
            $new_moderation_task = array(
                'manager_id' => 0,
                'company_id' => $company_id,
                'status' => 'wait',
                'product_id' => $product_id
            );

            $this->Product_Moderation = ClassRegistry::init("Product_Moderation");
            $this->Product_Moderation->save($new_moderation_task);
            $this->moderation_id = $this->Product_Moderation->id;
        }
        return $this->moderation_id;
    }

    /**
     * @param $moderation_id
     * @param $field
     * @param $old_data
     * @param $new_data
     * @param $old_visual
     * @param $new_visual
     */
    public function add_moderation_task($moderation_id, $field, $old_data, $new_data, $old_visual, $new_visual)
    {
        $this->Product_Moderation_List = ClassRegistry::init("Product_Moderation_List");
        $new_moderation_task = array(
            'moderation_id' => $moderation_id,
            'field_name' => $field,
            'status' => 'wait',
            'old_data' => $old_data,
            'new_data' => $new_data,
            'old_visual' => $old_visual,
            'new_visual' => $new_visual
        );
        $this->Product_Moderation_List->save($new_moderation_task);
        return;
    }


}