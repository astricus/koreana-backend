<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Логирование
 */
class LogComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;

    public $log_file;

    public $uses = array('Error');

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $this->log_file = Configure::read('LKP_LOG');
        if(!file_exists($this->log_file) OR !is_readable($this->log_file)){
            echo 'LKP LOG file is not exist or unreadable,  create file ' . $this->log_file;
            exit;
        }
    }

    public function add($message, $type)
    {
        return $this->_add($message, $type);
    }

    private function _add($message, $type)
    {
        $this->setup();
        $log_file = $this->log_file;
        $message = PHP_EOL . " " .  now_date() . " > " . $type . " > " . $message;
        file_put_contents($log_file, $message, FILE_APPEND);
        return;
    }

}