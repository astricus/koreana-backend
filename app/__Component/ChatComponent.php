<?php

App::uses(
    'AppController', 'Controller'
);
App::uses('Model', 'Model');
App::uses('L10n', 'L10n');

/**
 * Компонент Чат
 */
class ChatComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'DateTime',
        'Validator'
    );

    /*
     *
     * список методов:
     * 1. Создание комнаты
     * 2. Добавление пользователя в комнату
     * 3. Удаление создателем комнаты собеседника
     * 4. Удаление комнаты (плюс истории)
     * 5. Отправка сообщения участнику чата
     * 6. Отправка сообщения в комнату
     * 7. Поиск по истории переписки
     * 8. Прикрепление файла к сообщению
     * 9. Переименование комнаты
     * 10. Удаление файла из истории
     * */

    public $max_messages_limit = 10;

    public $interval = 60;

    public $message_min_size = 1;

    public $message_max_size = 50 * 1000;

    //ограничение кол-ва пользователей в групповом чате
    public $chat_room_max_users = 100;

    public $count_message_default = 50;

    public $error_message_is_not_correct = "сообщение не корректно";
    public $error_too_much_message_per_user = "вы отправляете слишком много сообщений, пожалуйста, попробуйте отправить это сообщение позднее";
    public $error_general = "отправка сообщений недоступна";
    public $error_room_not_found = "групповой чат, в который вы отправляете сообщение, недоступен или не существует";
    public $error_sender_cant_send_message = "Вы не можете отправлять сообщения";
    public $error_no_users_in_chat_room = "нет собеседников в групповом чате";
    public $error_recipient_is_undefined = "Получатель сообщения не найден";

    public $error_invalid_last_chat_message_id = "некорректный идентификатор последнего сообщения чата";

    public $error_unknown_chat_type = "не определен тип чата";
    public $error_invalid_chat_id = "не корректный идентификатор чата";
    public $error_invalid_role = "не корректный тип пользователя";

    public function isValidChatType($type)
    {
        if ($type !== "dialog" AND $type !== "room") {
            return false;
        }
        return true;
    }

    public $role_names = [
        'admin' => "Администратор",
        'manager' => "Менеджер",
        'driver' => "Водитель",
        'logist' => "Логист",
        'company' => "Поставщик",
        'orderer' => "Покупатель",
    ];

    function setupModels()
    {
        $modelName = "Driver";
        $this->Driver = ClassRegistry::init($modelName);

        $modelName = "Orderer";
        $this->Orderer = ClassRegistry::init($modelName);

        $modelName = "Driver";
        $this->Driver = ClassRegistry::init($modelName);

        $modelName = "Company_Agent";
        $this->Company_Agent = ClassRegistry::init($modelName);

        $modelName = "Company";
        $this->Company = ClassRegistry::init($modelName);

        $modelName = "Manager";
        $this->Manager = ClassRegistry::init($modelName);

        $modelName = "Message";
        $this->Message = ClassRegistry::init($modelName);

        $modelName = "Message_Room_User";
        $this->Message_Room_User = ClassRegistry::init($modelName);

        $modelName = "Message_Room";
        $this->Message_Room = ClassRegistry::init($modelName);

        $modelName = "Message_Dialog";
        $this->Message_Dialog = ClassRegistry::init($modelName);
    }

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
        $this->setupModels();
    }

    private function getCurrentUserRole()
    {
        return $this->Session->read('role');
    }

    private function getCurrentUserId()
    {
        return $this->Session->read('user_id');
    }

    /**
     * @param $owner_role
     * @param $owner_id
     * @return mixed
     */
    public function createChatRoom($owner_role, $owner_id)
    {
        $this->setupModels();
        $data = [
            'owner_id' => $owner_id,
            'owner_role' => $owner_role,
            'room_name' => ''
        ];
        $this->Message_Room->create();
        $this->Message_Room->save($data);
        $room_id = $this->Message_Room->id;
        $new_name = "Групповой чат №" . $room_id;
        $this->renameChatRoom($room_id, $new_name);
        return $room_id;
    }

    /**
     * @param $user_id
     * @param $user_role
     * @return void|null
     * * получение имени пользователя по идентификатору и роли
     */
    public function getUserName($user_id, $user_role)
    {
        if ($user_role == "orderer") {
            return $this->getOrdererName($user_id);
        } else if ($user_role == "company") {
            return $this->getCompanyAgentName($user_id);
        } else {
            return $this->getManagerName($user_id);
        }
    }

    /**
     * @param $id
     * @return string
     */
    private function getOrdererName($id)
    {
        $modelName = 'Orderer';
        $this->Orderer = ClassRegistry::init($modelName);

        $user_data = $this->Orderer->find('first',
            array(
                'conditions' => array(
                    'id' => $id
                )
            )
        );
        return prepare_fio($user_data['Orderer']['firstname'], $user_data['Orderer']['lastname'], "");
    }

    /**
     * @param $id
     * @return string
     */
    private function getManagerName($id)
    {
        $modelName = 'Manager';
        $this->Manager = ClassRegistry::init($modelName);

        $user_data = $this->Manager->find('first',
            array(
                'conditions' => array(
                    'id' => $id
                )
            )
        );
        return prepare_fio($user_data['Manager']['firstname'], $user_data['Manager']['lastname'], "");
    }

    private function getCompanyAgentName($id)
    {
        $modelName = 'Company_Agent';
        $this->Company_Agent = ClassRegistry::init($modelName);

        $user_data = $this->Company_Agent->find('first',
            array(
                'conditions' => array(
                    'id' => $id
                )
            )
        );
        return prepare_fio($user_data['Company_Agent']['firstname'], $user_data['Company_Agent']['lastname'], "");
    }

    /**
     * @param $role
     * @return mixed
     * * получение названия роли по ее ключу
     */
    public function getRoleNameByRole($role)
    {
        $role_names = $this->role_names;
        $role_name = $role_names[$role];
        return $role_name;
    }

    /**
     * @param $room_id
     * @param $new_name
     * @return bool
     */
    public function renameChatRoom($room_id, $new_name)
    {
        $this->setupModels();
        if (!$this->checkRoomExists($room_id)) {
            return false;
        }
        $data = [
            'room_name' => $new_name
        ];
        $this->Message_Room->id = $room_id;
        $this->Message_Room->save($data);
        return true;
    }

    /**
     * @param $room_id
     * @return bool
     */
    private function checkRoomExists($room_id)
    {
        $this->setupModels();
        $check = $this->Message_Room->find("count",
            array('conditions' =>
                array(
                    'id' => $room_id,
                )
            )
        );
        if ($check > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $role
     * @return bool
     */
    public function checkRoleExists($role)
    {
        $this->setupModels();
        $roles = array_keys($this->role_names);
        if (in_array($role, $roles)) {
            return true;
        }
        return false;
    }

    /**
     * @param $user_id
     * @param $role
     * @return bool
     */
    // TODO - добавить возможность получения статуса блокировки пользователя, заблокированные ползователи не могут отправлять сообщения в чате и получать их
    private function checkRecipientExists($user_id, $role)
    {
        if (!$this->checkRoleExists($role)) {
            return false;
        }
        $this->setupModels();
        if ($role == "orderer") {
            $orderer = $this->Orderer->find("count",
                array('conditions' =>
                    array(
                        'id' => $user_id,
                    )
                )
            );
            if ($orderer == 0) {
                return false;
            }
            return true;
        } else if ($role == "company") {
            $company_agent = $this->Company_Agent->find("count",
                array('conditions' =>
                    array(
                        'id' => $user_id,
                    )
                )
            );
            if ($company_agent == 0) {
                return false;
            }
            return true;
        } else {
            $manager = $this->Manager->find("count",
                array('conditions' =>
                    array(
                        'id' => $user_id,
                    )
                )
            );
            if ($manager == 0) {
                return false;
            }
            return true;
        }
    }

    /**
     * @param $user_role
     * @param $user_id
     * @param $room_id
     * @return bool
     */
    public function addUserIntoChatRoom($user_role, $user_id, $room_id)
    {
        $this->setupModels();
        if (!$this->checkRoomExists($room_id)) {
            return false;
        }
        if (!$this->checkRoleExists($user_role)) {
            return false;
        }

        $new_room_user = [
            'user_id' => $user_id,
            'role' => $user_role,
            'room_id' => $room_id,
        ];
        $this->Message_Room_User->create();
        $this->Message_Room_User->save($new_room_user);
        return $this->Message_Room_User->id;
    }

    public function removeUserFromChatRoomByOwner($room_id, $role, $user_id)
    {

    }

    public function removeChatRoomByOwner($room_id)
    {

    }

    public function validateNewMessage($message)
    {
        $message_content_length = mb_strlen($message);
        if ($message_content_length < $this->message_min_size) {
            return false;
        }
        if ($message_content_length >= $this->message_max_size) {
            return false;
        }
        return true;
    }

    /**
     * @param $room_id
     * @return array|bool
     */
    public function getChatRoomUsers($room_id)
    {
        if (!$this->checkRoomExists($room_id)) {
            return false;
        }
        $user_arr = [];
        $users = $this->Message_Room_User->find("all",
            array('conditions' =>
                array(
                    'room_id' => $room_id,
                )
            )
        );
        foreach ($users as $user) {
            $user_id = $user['Message_Room_User']['user_id'];
            $role = $user['Message_Room_User']['role'];
            $user_arr[] = [
                'role' => $role,
                'user_id' => $user_id,
                "user_name" => $this->getUserName($user_id, $role),
                "user_role_name" => $this->getRoleNameByRole($role)
            ];
        }
        return $user_arr;
    }

    /**
     * @param $room_id
     * @return string
     */
    public function getChatRoomNameBuId($room_id)
    {
        if (!$this->checkRoomExists($room_id)) {
            return null;
        }
        $room = $this->Message_Room->find("first",
            array('conditions' =>
                array(
                    'id' => $room_id,
                )
            )
        );

        return $room['Message_Room']['room_name'];
    }

    public function sendMessageToChatRoom($room_id, $sender_id, $sender_role, $message, $cite_id = 0)
    {
        if (!$this->checkRoomExists($room_id)) {
            return ["result" => false, "error" => $this->error_room_not_found];
        }

        // проверка отсутствия лимита кол-ва сообщений за период
        if (!$this->checkSendingAvailable($sender_id, $sender_role)) {
            return ["result" => false, "error" => $this->error_too_much_message_per_user];
        }

        // валидация сообщения на корректность
        if (!$this->validateNewMessage($message)) {
            return ["result" => false, "error" => $this->error_message_is_not_correct];
        }

        $chat_room_users = $this->getChatRoomUsers($room_id);
        if (count($chat_room_users) == 0) {
            return ["result" => false, "error" => $this->error_no_users_in_chat_room];
        }
        foreach ($chat_room_users as $chat_room_user) {
            $recipient_role = $chat_room_user['role'];
            $recipient_id = $chat_room_user['user_id'];

            if (!$this->checkRecipientExists($recipient_id, $recipient_role)) {
                continue;
            }
            // защита от отправки самому себе
            if ($sender_id == $recipient_id AND $sender_role == $recipient_role) {
                continue;
            }

            $message_id = $this->internalMessageSendingAction($sender_id, $sender_role, $recipient_id, $recipient_role, $message, $cite_id = 0, $room_id, null);
        }
        return ["result" => true, "error" => '', "message_id" => $message_id];
    }

    /**
     * @param $sender_id
     * @param $sender_role
     * @param $recipient_id
     * @param $recipient_role
     * @param $message
     * @param $cite_id
     * @return array
     */
    public function sendMessageToRecipient($sender_id, $sender_role, $recipient_id, $recipient_role, $message, $cite_id)
    {
        // проверка отсутствия лимита кол-ва сообщений за период
        if (!$this->checkSendingAvailable($sender_id, $sender_role)) {
            return ["result" => false, "error" => $this->error_too_much_message_per_user];
        }

        // валидация сообщения на корректность
        if (!$this->validateNewMessage($message)) {
            return ["result" => false, "error" => $this->error_message_is_not_correct];
        }

        if (!$this->checkRecipientExists($recipient_id, $recipient_role)) {
            return ["result" => false, "error" => $this->error_recipient_is_undefined];
        }

        $dialog_id = $this->getDialogId($sender_id, $sender_role, $recipient_id, $recipient_role);
        $message_id = $this->internalMessageSendingAction($sender_id, $sender_role, $recipient_id, $recipient_role, $message, $cite_id, null, $dialog_id);
        return [
            "dialog_id" => $dialog_id,
            "result" => true,
            "error" => '',
            "message_id" => $message_id
        ];
    }

    /**
     * внутренний метод для отправки сообщения, без проверок
     */
    /**
     * @param $sender_id
     * @param $sender_role
     * @param $recipient_id
     * @param $recipient_role
     * @param $message
     * @param int $cite_id
     * @param int $room_id
     * @param int $dialog_id
     * @return mixed
     */
    private function internalMessageSendingAction($sender_id, $sender_role, $recipient_id, $recipient_role, $message, $cite_id = 0, $room_id = 0, $dialog_id = 0)
    {
        if ($cite_id == null) {
            $cite_id = 0;
        }
        $data = [
            'sender_id' => $sender_id,
            'sender_role' => $sender_role,
            'receiver_id' => $recipient_id,
            'receiver_role' => $recipient_role,
            'message' => $message,
            'cite_id' => $cite_id,
            'readed' => 'no',
            'room_id' => $room_id,
            'dialog_id' => $dialog_id,
        ];
        $this->Message->create();
        $this->Message->save($data);
        return $this->Message->id;
    }

    // получение истории сообщений комнаты
    public function getChatRoomMessages($room_id, $order, $count, $page, $user_id, $user_role, $last_message_id = null)
    {
        $show_count = $count >= 1 ? $count : $this->count_message_default;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($last_message_id != null) {
            $messages = $this->Message->find("all",
                array(
                    'conditions' =>
                        array(
                            'room_id' => $room_id,
                            'id >' => $last_message_id,
                            'or' => array(
                                array(
                                    'receiver_id' => $user_id,
                                    'receiver_role' => $user_role),
                                array(
                                    'sender_id' => $user_id,
                                    'sender_role' => $user_role),
                            ),
                        ),
                    'fields' => array(
                        'Message.*',
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Message.created) AS created_time'
                    ),
                    'order' => 'Message.created',
                    'limit' => $show_count,
                )
            );
        } else {
            $messages = $this->Message->find("all",
                array(
                    'conditions' =>
                        array(
                            'room_id' => $room_id,
                            'or' => array(
                                array(
                                    'receiver_id' => $user_id,
                                    'receiver_role' => $user_role),
                                array(
                                    'sender_id' => $user_id,
                                    'sender_role' => $user_role),
                            ),
                        ),
                    /*
                    'joins' => array(
                        array(
                            'table' => 'manufacturers',
                            'alias' => 'Manufacturer',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Product.manufacturer_id = Manufacturer.id'
                            )
                        ),
                    ),*/
                    'fields' => array(
                        'Message.*',
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Message.created) AS created_time'
                    ),
                    'order' => 'Message.created',
                    'limit' => $show_count,
                    'offset' => $limit_page,
                )
            );
        }

        $messages_arr = [];
        foreach ($messages as $message) {
            $message_data = $message['Message'];
            if ($message_data['receiver_id'] == $user_id AND $message_data['receiver_role'] == $user_role) {
                $dir = "chat-user";
                $last_user_id = $user_id;
                $last_user_role = $user_role;
            } else {
                $dir = "chat-me";
                $last_user_id = $message['Message']['sender_id'];
                $last_user_role = $message['Message']['sender_role'];
            }

            $user_name = $this->getUserName($last_user_id, $last_user_role);
            $messages_arr[] = array(
                'dir' => $dir,
                'id' => $message['Message']['id'],
                "user_id" => $last_user_id,
                "user_role" => $last_user_role,
                "user_name" => $user_name,
                "user_role_name" => $this->getRoleNameByRole($last_user_role),
                "message_text" => $message['Message']['message'],
                "readed" => $message['Message']['readed'],
                "cite_id" => $message['Message']['cite_id'],
                "message_time" => $this->formatMessageTime($message[0]['created_time']),
            );
        }

        return $messages_arr;
    }

    /**
     * @param $room_id
     * @return mixed
     */
    public function getChatRoomMessageCount($room_id)
    {
        $messages = $this->Message->find("count",
            array('conditions' =>
                array(
                    'room_id' => $room_id,
                )
            )
        );
        return $messages;
    }

    /**
     * @param $room_id
     * @return mixed
     */
    public function getDialogMessageCount($dialog_id)
    {
        $messages = $this->Message->find("count",
            array('conditions' =>
                array(
                    'dialog_id' => $dialog_id,
                )
            )
        );
        return $messages;
    }
    // получение истории сообщений диалога
    public function getDialogMessages($dialog_id, $sort_order, $count, $page = 1, $user_id, $user_role, $last_message_id = null)
    {
        $show_count = $count >= 1 ? $count : $this->count_message_default;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);


        if ($last_message_id != null) {
            $messages = $this->Message->find("all",
                array(
                    'conditions' =>
                        array(
                            'dialog_id' => $dialog_id,
                            'id >' => $last_message_id
                        ),

                    "fields" => array("Message.*", 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Message.created) AS created_time'),
                    'order' => 'Message.created',
                    'limit' => $show_count,
                )
            );
        } else {
            $messages = $this->Message->find("all",
                array(
                    'conditions' =>
                        array(
                            'dialog_id' => $dialog_id,
                        ),
                    /*
                    'joins' => array(
                        array(
                            'table' => 'manufacturers',
                            'alias' => 'Manufacturer',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Product.manufacturer_id = Manufacturer.id'
                            )
                        ),
                    ),
                    'fields' => array(
                        'Manufacturer.*',
                        'Product.*',
                    ),*/
                    "fields" => array("Message.*", 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Message.created) AS created_time'),
                    'order' => 'Message.created',
                    'limit' => $show_count,
                    'offset' => $limit_page,
                )
            );
        }

        $messages_arr = [];
        foreach ($messages as $message) {
            $message_data = $message['Message'];
            if ($message_data['receiver_id'] == $user_id AND $message_data['receiver_role'] == $user_role) {
                $dir = "chat-user";
                $last_user_id = $user_id;
                $last_user_role = $user_role;
            } else {
                $dir = "chat-me";
                $last_user_id = $message['Message']['sender_id'];
                $last_user_role = $message['Message']['sender_role'];
            }

            $user_name = $this->getUserName($last_user_id, $last_user_role);
            $messages_arr[] = array(
                'id' => $message['Message']['id'],
                'dir' => $dir,
                "user_id" => $last_user_id,
                "user_role" => $last_user_role,
                "user_name" => $user_name,
                "user_role_name" => $this->getRoleNameByRole($last_user_role),
                "message_text" => $message['Message']['message'],
                "readed" => $message['Message']['readed'],
                "cite_id" => $message['Message']['cite_id'],
                "message_time" => $this->formatMessageTime($message[0]['created_time']),
            );
        }

        return $messages_arr;
    }

    // поиск в истории переписки
    public function searchInHistory($room_id, $dialog_id)
    {

    }

    /**
     * @param array $messages
     * @return int
     */
    public function lastMessageId(array $messages)
    {
        $new_last_message_id = 0;
        foreach ($messages as $message) {
            $message_data = key_exists("Message", $message) ? $message['Message'] : $message;
            $message_id = $message_data['id'];
            if ($message_id > $new_last_message_id) {
                $new_last_message_id = $message_id;
            }
        }
        return $new_last_message_id;
    }

        /**
     * @param $chat_id
     * @param $chat_type
     * @return array|null
     */
    public function getLastMessageByChatIdOrDialogId($chat_id, $chat_type){
        $this->setupModels();
        if($chat_type == "room"){
            $field_type = 'room_id';
        } else {
            $field_type = 'dialog_id';
        }
        $message = $this->Message->find("first",
            array('conditions' =>
                array(
                    $field_type => $chat_id,
                ),
                'order' => array("id DESC")
            )
        );
        if (count($message) == 0) {
            return null;
        }
        return $message['Message'];
    }


    //последние сообщения для чата
    public function lastChatMessages($user_role, $user_id, $chat_type, $chat_id, $last_message_id)
    {
        $count = 30;
        if ($chat_type == "dialog") {
            $messages = $this->getDialogMessages($chat_id, null, $count, 1, $user_id, $user_role, $last_message_id);
        } else {
            $messages = $this->getChatRoomMessages($chat_id, null, $count, 1, $user_id, $user_role, $last_message_id);
        }
        $new_last_message_id = 0;
        foreach ($messages as $message) {
            $message_id = $message['id'];
            if ($message_id > $new_last_message_id) {
                $new_last_message_id = $message_id;
            }
        }
        return ["messages" => $messages, "last_message_id" => $new_last_message_id];
    }

    //последние сообщения в диалоге
    public function lastDialogMessages($user_role, $user_id, $recipient_role, $recipient_id, $last_message_id)
    {
        $count = 30;
        $dialog_id = $this->getDialogId($user_id, $user_role, $recipient_id, $recipient_role);
        $messages = $this->getDialogMessages($dialog_id, null, $count, 1, $user_id, $user_role, $last_message_id);

        $new_last_message_id = 0;
        foreach ($messages as $message) {
            $message_id = $message['id'];
            if ($message_id > $new_last_message_id) {
                $new_last_message_id = $message_id;
            }
        }
        return ["messages" => $messages, "last_message_id" => $new_last_message_id];
    }

    /**
     * @param $user_role
     * @param $user_id
     * @param $string
     */
    public function searchRecipient($user_role, $user_id, $string)
    {
        $recipient_and_dialogs = [];
        $recipients = $this->findRecipientByName($user_role, $user_id, $string);
        $chat_rooms = $this->findChatRoomByName($user_role, $user_id, $string);
    }

    /**
     * @param $sender_role
     * @param $recipient_role
     * @return array
     */
    private function chatPermissionSenderRecipient($sender_role, $recipient_role)
    {
        return array_keys($this->role_names);
    }

    /* поиск диалогов (собеседников) по чату */
    private function findRecipientByName($user_role, $user_id, $string)
    {
        $dialog_users = [];
        if ($this->chatPermissionSenderRecipient($user_role, "admin")) {
            $dialog_users[] = $this->findManagerByName($string, "admin");
        }
        if ($this->chatPermissionSenderRecipient($user_role, "orderer")) {
            $dialog_users[] = $this->findOrdererByName($string);
        }
        if ($this->chatPermissionSenderRecipient($user_role, "logist")) {
            $dialog_users[] = $this->findManagerByName($string, "logist");
        }
        if ($this->chatPermissionSenderRecipient($user_role, "driver")) {
            $dialog_users[] = $this->findDriverByName($string);
        }
        if ($this->chatPermissionSenderRecipient($user_role, "company")) {
            $dialog_users[] = $this->findCompanyAgentByName($string);
        }
        if ($this->chatPermissionSenderRecipient($user_role, "manager")) {
            $dialog_users[] = $this->findManagerByName($string, "operator");
        }
        return $dialog_users;
    }

    /* поиск диалогов (собеседников) по чату */
    public function findChatRoomByName($user_role, $user_id, $string)
    {
        $user_chat_rooms = $this->getUserChatRooms($user_role, $user_id);
        $found_user_chat_rooms = $this->Message_Room->find("all",
            array(
                'conditions' =>
                    array(
                        'id' => $user_chat_rooms,
                        'name' => $string
                    ),
                'order' => array('id ASC')
            )
        );
        $found_user_chat_rooms_arr = [];
        foreach ($found_user_chat_rooms as $found_user_chat_room) {
            $room_id = $found_user_chat_room['Message_Room']['id'];
            $room_name = $found_user_chat_room['Message_Room']['room_name'];
            $found_user_chat_rooms_arr[] = ['room_id' => $room_id, 'name' => $room_name, 'type' => 'chat'];
        }
        return $found_user_chat_rooms_arr;
    }

    /**
     * @param $string
     * @return array
     */
    private function findOrdererByName($string)
    {
        $modelName = 'Orderer';
        $this->Orderer = ClassRegistry::init($modelName);
        $found_users = $this->Orderer->find('all',
            array(
                'conditions' => array(
                    'or' => array(
                        array(
                            'firstname LIKE' => "%$string%",
                        ),
                        array(
                            'lastname LIKE' => "%$string%",
                        ),
                    ),
                )
            )
        );
        $found_users_arr = [];
        foreach ($found_users as $found_user) {
            $user_id = $found_user['Orderer']['id'];
            $user_name = prepare_fio($found_user['Orderer']['firstname'], $found_user['Orderer']['lastname'], "");
            //TODO добавить прикрепления dialog_id
            $dialog_id = 0;
            $found_users_arr[] = ['user_id' => $user_id, 'name' => $user_name];
        }
        return $found_users_arr;
    }

    /**
     * @param $string
     * @return array
     */
    private function findManagerByName($string, $role)
    {
        $modelName = 'Manager';
        $this->Manager = ClassRegistry::init($modelName);

        $found_users = $this->Manager->find('first',
            array(
                'conditions' => array(
                    'role' => $role,
                    'or' => array(
                        array(
                            'firstname LIKE' => "%$string%",
                        ),
                        array(
                            'lastname LIKE' => "%$string%",
                        ),
                    ),
                )
            )
        );
        $found_users_arr = [];
        foreach ($found_users as $found_user) {
            $user_id = $found_user['Manager']['id'];
            $user_name = prepare_fio($found_user['Manager']['firstname'], $found_user['Manager']['lastname'], "");
            $found_users_arr[] = ['user_id' => $user_id, 'name' => $user_name];
        }
        return $found_users_arr;
    }

    /**
     * @param $string
     * @return array
     */
    private function findDriverByName($string)
    {
        $modelName = 'Driver';
        $this->Driver = ClassRegistry::init($modelName);

        $found_users = $this->Driver->find('first',
            array(
                'conditions' => array(
                    'or' => array(
                        array(
                            'firstname LIKE' => "%$string%",
                        ),
                        array(
                            'lastname LIKE' => "%$string%",
                        ),
                    ),
                )
            )
        );
        $found_users_arr = [];
        foreach ($found_users as $found_user) {
            $user_id = $found_user['Driver']['id'];
            $user_name = prepare_fio($found_user['Driver']['firstname'], $found_user['Driver']['lastname'], "");
            $found_users_arr[] = ['user_id' => $user_id, 'name' => $user_name];
        }
        return $found_users_arr;
    }


    /**
     * @param $string
     * @return array
     */
    private function findCompanyAgentByName($string)
    {
        $modelName = 'Company_Agent';
        $this->Company_Agent = ClassRegistry::init($modelName);

        $found_users = $this->Company_Agent->find('all',
            array(
                'conditions' => array(
                    'or' => array(
                        array(
                            'firstname LIKE' => "%$string%",
                        ),
                        array(
                            'lastname LIKE' => "%$string%",
                        ),
                    ),
                )
            )
        );
        $found_users_arr = [];
        foreach ($found_users as $found_user) {
            $user_id = $found_user['Company_Agent']['id'];
            $user_name = prepare_fio($found_user['Company_Agent']['firstname'], $found_user['Company_Agent']['lastname'], "");
            $found_users_arr[] = ['user_id' => $user_id, 'name' => $user_name];
        }
        return $found_users_arr;
    }


    /**
     * @param $recipient_role
     * @param $recipient_id
     * @param $sender_id
     * @param $sender_role
     * @return mixed
     */
    private function createDialog($recipient_role, $recipient_id, $sender_id, $sender_role)
    {
        $this->setupModels();
        $dialog = array(
            'sender_id' => $sender_id,
            'sender_role' => $sender_role,
            'recipient_id' => $recipient_id,
            'recipient_role' => $recipient_role,
        );
        $this->Message_Dialog->create();
        $this->Message_Dialog->save($dialog);
        return $this->Message_Dialog->id;
    }

    /**
     * @param $recipient_role
     * @param $recipient_id
     * @param $sender_id
     * @param $sender_role
     * @return mixed
     */
    // диалог id - идентификатор диалога уникальных пользователей
    public function getDialogId($sender_id, $sender_role, $recipient_id, $recipient_role)
    {
        $this->setupModels();
        $dialog_data = array(
            'sender_id' => $sender_id,
            'sender_role' => $sender_role,
            'recipient_id' => $recipient_id,
            'recipient_role' => $recipient_role,
        );

        $dialog = $this->Message_Dialog->find("first",
            array(
                'conditions' => $dialog_data,
                'order' => array('created ASC')
            )
        );

        if (count($dialog) == 0) {
            return $this->createDialog($recipient_role, $recipient_id, $sender_id, $sender_role);
        } else {
            return $dialog['Message_Dialog']['id'];
        }
    }

    public function uploadAttachment($room_id, $dialog_id, $owner_role, $owner_id)
    {

    }

    public function removeAttachment($room_id, $dialog_id, $owner_role, $owner_id)
    {

    }

    // получение непрочитанных сообщений для пользователя в диалоге
    private function getUnreadUserMessageByDialog($dialog_id, $role, $user_id)
    {
        return MockComponent::ARR;
    }

    // получение непрочитанных сообщений для пользователя группового чата
    private function getUnreadUserMessageByChatRoom($room_id, $role, $user_id)
    {
        return MockComponent::ARR;
    }

    /**
     * @param $role
     * @param $user_id
     * @return array
     */
    public function getUserDialogs($role, $user_id)
    {
        $this->setupModels();
        $dialogs = $this->Message_Dialog->find("all",
            array(
                'conditions' =>
                    array(
                        'or' => array(
                            array(
                                'receiver_id' => $user_id,
                                'receiver_role' => $role),
                            array(
                                'sender_id' => $user_id,
                                'sender_role' => $role),
                        ),
                    ),
                'order' => array('id ASC')
            )
        );
        $dialogs_arr = [];
        foreach ($dialogs as $dialog) {
            $dialog_id = $dialog['Message_Dialog']['id'];
            if (!in_array($dialog_id, $dialogs_arr)) {
                $dialogs_arr[] = $dialog_id;
            }
        }
        return $dialogs_arr;
    }

    /**
     * @param $role
     * @param $user_id
     * @return array
     */
    public function getUserChatRooms($role, $user_id)
    {
        $this->setupModels();
        $chat_rooms = $this->Message_Room_User->find("all",
            array(
                'conditions' =>
                    array(
                        'user_id' => $user_id,
                        'role' => $role
                    ),
                'order' => array('id ASC')
            )
        );
        $chat_rooms_arr = [];
        foreach ($chat_rooms as $chat_room) {
            $chat_room_id = $chat_room['Message_Room_User']['room_id'];
            if (!in_array($chat_room_id, $chat_rooms_arr)) {
                $chat_rooms_arr[] = $chat_room_id;
            }
        }
        return $chat_rooms_arr;
    }

    /**
     * @return int
     */
    public function getNewMessagesCount()
    {
        // получение новых сообщений из диалогов
        $new_dialog_messages_count = 0;
        $user_dialogs = $this->getUserDialogs($this->getCurrentUserRole(), $this->getCurrentUserId());
        foreach ($user_dialogs as $user_dialog) {
            $dialog_id = $user_dialog['Message_Dialog']['id'];
            $unread_messages = $this->getUnreadUserMessageByDialog($dialog_id, $this->getCurrentUserRole(), $this->getCurrentUserId());
            $unread_messages_count = count($unread_messages);
            $new_dialog_messages_count += $unread_messages_count;
        }

        // получение новых сообщений из групповых чатов
        $new_chatroom_messages_count = 0;
        $user_chat_rooms = $this->getUserChatRooms($this->getCurrentUserRole(), $this->getCurrentUserId());
        foreach ($user_chat_rooms as $user_chat_room) {
            $chat_room_id = $user_chat_room['Message_Room_User']['id'];
            $unread_messages = $this->getUnreadUserMessageByDialog($chat_room_id, $this->getCurrentUserRole(), $this->getCurrentUserId());
            $unread_messages_count = count($unread_messages);
            $new_chatroom_messages_count += $unread_messages_count;
        }
        return $new_dialog_messages_count + $new_chatroom_messages_count;
    }

    public function setMessageAsRead($message_id)
    {
        $update = array(
            'readed' => 'yes',
        );
        $this->Message->id = $message_id;
        $this->Message->save($update);
        return true;
    }

    /**
     * @param $sender_id
     * @param $sender_role
     * @return bool
     */
    public function checkSendingAvailable($sender_id, $sender_role)
    {
        if (count($this->getMessagesBySender($sender_id, $sender_role)) >= $this->max_messages_limit) {
            return false;
        }
        return true;
    }

    /**
     * @param $sender_id
     * @param $sender_role
     * @param null $time_seconds_interval
     * @return |null
     */
    private function getMessagesBySender($sender_id, $sender_role, $time_seconds_interval = null)
    {
        $this->setupModels();

        $created_interval = "";
        if ($time_seconds_interval != null) {
            $created_interval = "UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Message.created) - " . $this->interval . "<=0";
        }

        $messages = $this->Message->find("first",
            array('conditions' =>
                array(
                    'sender_id' => $sender_id,
                    'sender_role' => $sender_role,
                    $created_interval
                )
            )
        );
        if (count($messages) == 0) {
            return null;
        }
        return $messages;
    }

    // получить по выбранному сообщению цитату
    public function get_message_cite($message_id)
    {
        $this->setupModels();
        $message = $this->Message->find("first",
            array('conditions' =>
                array(
                    'id' => $message_id
                )
            )
        );
        if (count($message) == 0) {
            return null;
        }
        $cite_id = $message['Message']['cite_id'];
        if ($cite_id == 0) {
            return null;
        }
        $cite_message = $this->Message->find("first",
            array('conditions' =>
                array(
                    'id' => $cite_id
                )
            )
        );
        if (count($cite_message) == 0) {
            return null;
        }
        return $cite_message;

    }

    public function getNewMessages($recipient_role, $recipient_id)
    {
        $new_messages = $this->Message->find("all",
            array(
                'conditions' =>
                    array(
                        'receiver_id' => $recipient_id,
                        'receiver_role' => $recipient_role,
                        'readed' => 'no'
                    ),
                "order" => array("Message.created DESC"),
                'fields' => array(
                    'DISTINCT Message.sender_id, Message.sender_role', 'Message.*, UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Message.created) AS created_time',
                ),
            )
        );
        return $new_messages;
    }

    /**
     * @param $recipient_role
     * @param $recipient_id
     * @return mixed
     */
    public function getNewMessagesForNotify($recipient_role, $recipient_id)
    {
        $all_messages = $this->getNewMessages($recipient_role, $recipient_id);
        $messages = [];
        foreach ($all_messages as $dialog) {
            $dialog_id = $dialog['Message']['id'];
            $dialog_type = $this->getDialogType($dialog);
            $chat_id = $dialog['Message']['room_id'] > 0 ? $dialog['Message']['room_id'] : $dialog['Message']['dialog_id'];
            if (!in_array($dialog_id, array_keys($messages))) {
                if ('receiver_id' == $recipient_id AND 'receiver_role' == $recipient_role) {
                    $last_user_id = $recipient_id;
                    $last_user_role = $recipient_role;
                } else {
                    $last_user_id = $dialog['Message']['sender_id'];
                    $last_user_role = $dialog['Message']['sender_role'];
                }

                $user_name = $this->getUserName($last_user_id, $last_user_role);
                $messages[] = array(
                    "chat_id" => $chat_id,
                    "type" => $dialog_type,
                    "user_id" => $last_user_id,
                    "user_role" => $last_user_role,
                    "user_name" => $user_name,
                    "user_role_name" => $this->getRoleNameByRole($last_user_role),
                    "message_text" => $dialog['Message']['message'],
                    "message_time" => $this->formatMessageTime($dialog[0]['created_time']),
                    "data" => $dialog['Message']
                );
            }
        }
        return $messages;
    }

    /**
     * @param $user_id
     * @param $role
     * @return array
     */
    public function getChatDialogHistory($user_id, $role)
    {
        $this->setupModels();
        $dialogs = $this->Message->find("all",
            array(
                'conditions' =>
                    array(
                        'or' => array(
                            array(
                                'receiver_id' => $user_id,
                                'receiver_role' => $role),
                            array(
                                'sender_id' => $user_id,
                                'sender_role' => $role),
                        ),
                    ),
                "order" => array("Message.created DESC"),
                'fields' => array(
                    //'DISTINCT Message.sender_id, Message.sender_role',
                    'Message.*',
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(Message.created) AS created_time'
                ),
            )
        );
        $history_arr = [];
        foreach ($dialogs as $dialog) {
            $message_id = $dialog['Message']['id'];
            $dialog_type = $this->getDialogType($dialog);
            $chat_id = $dialog['Message']['room_id'] > 0 ? $dialog['Message']['room_id'] : $dialog['Message']['dialog_id'];

            if ('receiver_id' == $user_id AND 'receiver_role' == $role) {
                $last_user_id = $user_id;
                $last_user_role = $role;
            } else {
                $last_user_id = $dialog['Message']['sender_id'];
                $last_user_role = $dialog['Message']['sender_role'];
            }

            $found = false;
            foreach ($history_arr as $history_item){
                if($history_item["chat_id"] == $chat_id AND $dialog_type == $history_item["type"]){
                    $found = true;
                }
            }
            if(!$found){

                $user_name = $this->getUserName($last_user_id, $last_user_role);
                $room_name = $user_name;
                if($dialog_type == "room"){
                    $room_name = $this->getChatRoomNameBuId($chat_id);
                }
                $history_arr[] = [
                    'id' => $message_id,
                    "chat_id" => $chat_id,
                    "type" => $dialog_type,
                    "user_id" => $last_user_id,
                    "user_role" => $last_user_role,
                    "user_name" => $user_name,
                    "name" => $room_name,
                    "user_role_name" => $this->getRoleNameByRole($last_user_role),
                    "message_text" => $dialog['Message']['message'],
                    "message_time" => $this->formatMessageTime($dialog[0]['created_time']),
                    "data" => $dialog['Message']
                ];

            }


        }
        return $history_arr;
    }

    /**
     * @param $dialog
     * @return string
     */
    private function getDialogType($dialog)
    {
        if ($dialog['Message']['room_id'] > 0) {
            return "room";
        }
        return "dialog";
    }

    /**
     * @param $message_created
     * @return mixed
     */
    private function formatMessageTime($message_created)
    {
        if($this->Validator->valid_date($message_created)){
            if(substr($message_created, 0, 2) == "20") {
                $message_created = time() - strtotime($message_created);
            }
        }
        return $this->DateTime->days_later($message_created);
    }

    /**
     * @param $counter
     * @return string
     */
    public function messageCounter($counter)
    {
        return $counter . " " . word_count_format($counter, array("сообщение", "сообщения", "сообщений"));
    }


    //поиск пользователей и комнат чата по названию
    public function searchRecipientWrapper($current_user_role, $current_user_id, $string){
        // 1. - список собеседников 2. - список чатов 3 - список пользователей, чье фио совпадает
        $recipients_by_name = $this->searchRecipientByName($current_user_role, $current_user_id, $string);
        $chat_room_by_name = $this->searchChatRoomByName($current_user_role, $current_user_id, $string);
        $users_by_name = $this->searchUsersByName($current_user_role, $current_user_id, $string);
        $recipient_ids = [];
        if(count($recipients_by_name)>0){
            foreach ($recipients_by_name as $recipient){
                $rec_id  = $recipient['id'];
                if(!in_array($rec_id, $recipient_ids)){
                    $recipient_ids[] = $rec_id;
                }
            }
        }
        $filtered_users = [];
        if(count($users_by_name)>0) {
            foreach ($users_by_name as $user) {
                $id = key_exists("id", $user) ? $user['id'] : null;
                if($id == null) continue;
                if (!in_array($id, $recipient_ids)) {
                    $filtered_users[] = $user;
                }
            }
        }

        foreach ($recipients_by_name as &$recipient){
            $user_id = $recipient['id'];
            $user_role = $recipient['role'];
            $dialog_id = $this->getDialogId($current_user_id, $current_user_role, $user_id, $user_role);
            $last_message = $this->getLastMessageByChatIdOrDialogId($dialog_id, "dialog");
            $recipient['last_message_id'] = $last_message['id'];
            $recipient['message_text'] = $last_message['message'];
            $recipient['message_time'] = $this->formatMessageTime($last_message['created']);
            $recipient['user_role_name'] = $this->role_names[$user_role];
            $recipient['name'] = $this->getUserName($user_id, $user_role);
        }

        foreach ($chat_room_by_name as &$chat_room){
            $chat_id = $chat_room['chat_id'];
            $last_message = $this->getLastMessageByChatIdOrDialogId($chat_id, "room");
            $chat_room['last_message_id'] = $last_message['id'];
            $chat_room['message_text'] = $last_message['message'];
            $chat_room['message_time'] = $this->formatMessageTime($last_message['created']);
        }

        foreach ($filtered_users as &$filtered_user){
            $user_id = $filtered_user['id'];
            $user_role = $filtered_user['role'];
            $dialog_id = $this->getDialogId($current_user_id, $current_user_role, $user_id, $user_role);
            $last_message = $this->getLastMessageByChatIdOrDialogId($dialog_id, "dialog");
            $filtered_user['last_message_id'] = $last_message['id'];
            $filtered_user['message_text'] = $last_message['message'];
            $filtered_user['message_time'] = $this->formatMessageTime($last_message['created']);
            $filtered_user['user_role_name'] = $this->role_names[$user_role];
            $filtered_user['name'] = $this->getUserName($user_id, $user_role);
        }

        $result_users = array_merge($recipients_by_name, $chat_room_by_name, $filtered_users);
        return $result_users;
    }


    /**
     * @param $string
     * @param $role
     * @param $user_id
     * @return array
     */
    public function searchRecipientByName($role, $user_id, $string)
    {
        $this->setupModels();
        $all_user_messages = $this->Message->find("all",
            array(
                'conditions' =>
                    array(
                        'or' => array(
                            array(
                                'receiver_id' => $user_id,
                                'receiver_role' => $role),
                            array(
                                'sender_id' => $user_id,
                                'sender_role' => $role),
                        ),
                    ),
            )
        );
        $users_arr = [];
        foreach ($all_user_messages as $user_message) {
            $sender_role = $user_message['Message']['sender_role'];
            $receiver_role = $user_message['Message']['receiver_role'];
            $sender_id = $user_message['Message']['sender_id'];
            $receiver_id = $user_message['Message']['receiver_id'];
            if ($sender_role == $role AND $sender_id == $user_id) {
                $user = ['role' => $receiver_role, 'id' => $receiver_id];
            } else {
                $user = ['role' => $sender_role, 'id' => $sender_id];
            }
            if (!in_array($user, $users_arr)) {
                if ($user['role'] == "company") {
                    $company_agents = $this->findCompanyAgentByName($string);
                    foreach ($company_agents as $company_agent) {
                        $user_id = $company_agent['user_id'];
                        if ($user_id == $user['id']) {
                            $users_arr[] = $user;
                        }
                    }
                } else if ($user['role'] == "admin") {
                    $admins = $this->findManagerByName($string, "admin");
                    foreach ($admins as $admin) {
                        $user_id = $admin['user_id'];
                        if ($user_id == $user['id']) {
                            $users_arr[] = $user;
                        }
                    }
                } else if ($user['role'] == "orderer") {
                    $orderers = $this->findOrdererByName($string);
                    foreach ($orderers as $orderer) {
                        $user_id = $orderer['user_id'];
                        if ($user_id == $user['id']) {
                            $users_arr[] = $user;
                        }
                    }
                } else if ($user['role'] == "driver") {
                    $drivers = $this->findDriverByName($string);
                    foreach ($drivers as $driver) {
                        $user_id = $driver['user_id'];
                        if ($user_id == $user['id']) {
                            $users_arr[] = $user;
                        }
                    }
                }

            }
        }
        return $users_arr;
    }

    /**
     * @param $string
     * @param $user_id
     * @param $user_role
     * @return array
     */
    public function searchChatRoomByName($user_role, $user_id, $string)
    {
        $this->setupModels();
        $user_message_rooms = $this->getUserChatRooms($user_role, $user_id);
        $found_chat_rooms = $this->Message_Room->find('all',
            array(
                'conditions' => array(
                    'id' => $user_message_rooms,
                    'room_name LIKE' => "%$string%",
                )
            )
        );
        $found_rooms_arr = [];
        foreach ($found_chat_rooms as $chat_room) {
            $chat_id = $chat_room['Message_Room']['id'];
            $room_name = prepare_fio($chat_room['Message_Room']['room_name'], $chat_room['Message_Room']['room_name'], "");
            $found_rooms_arr[] = ['chat_id' => $chat_id, 'name' => $room_name];
        }
        return $found_rooms_arr;
    }

    /**
     * @param $string
     * @param $user_role
     * @param $user_id
     * @return array
     */
    public function searchUsersByName($user_role, $user_id, $string)
    {
        return $this->findRecipientByName($user_role, $user_id, $string);
    }
}