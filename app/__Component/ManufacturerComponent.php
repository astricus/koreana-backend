<?php
App::uses(
    'AppController', 'Controller'
);
App::uses('L10n', 'L10n');
App::uses('Model', 'Model');

/**
 * Компонент Разработчик
 */
class ManufacturerComponent extends Component
{
	public $uses = array(
	    "Manufacturer"
    );

	public $controller;

	public $components = array(
	    'Session',
        'Error'
    );

	function setupModel(){
        $modelName = "Manufacturer";
        $this->Manufacturer = ClassRegistry::init($modelName);

        $modelName = "Brand";
        $this->Brand = ClassRegistry::init($modelName);
    }

	function initialize(Controller $controller)
	{
		$this->controller = $controller;
		$this->setupModel();
	}

    private function checkManufacturerExists($name)
    {
        $this->setupModel();
        if ($name != null) {
            $checkManufacturerExists = $this->Manufacturer->find("all",
                array('conditions' =>
                    array(
                        'name' => $name
                    )
                )
            );
            if (count($checkManufacturerExists) == 0) {
                return false;
            }
            return $checkManufacturerExists[0]['Manufacturer']['id'];
        }
    }

    public function checkBrandExists($name)
    {
        $this->setupModel();
        if ($name != null) {
            $checkBrandExists = $this->Brand->find("all",
                array('conditions' =>
                    array(
                        'name' => $name
                    )
                )
            );
            if (count($checkBrandExists) == 0) {
                return false;
            }
            return $checkBrandExists[0]['Brand']['id'];
        }
    }

    private function saveManufacturer($manufacturer)
    {
        $this->setupModel();
        $saveManufacturer = array(
            'name' => $manufacturer['name'],
            'url' => $manufacturer['url'],
        );
        $this->Manufacturer->create();
        $this->Manufacturer->save($saveManufacturer);
        return $this->Manufacturer->id;
    }

    public function saveBrand($brand)
    {
        $this->setupModel();
        $saveBrand = array(
            'name' => $brand,
        );
        $this->Brand->create();
        $this->Brand->save($saveBrand);
        return $this->Brand->id;
    }

    public function checkManufacturer($manufacturer)
    {
        $this->setupModel();
        $name = $manufacturer['name'];
        $url = $manufacturer['homepage_url'];

        $check = $this->checkManufacturerExists($name);
        if (!$check) {
            return $this->saveManufacturer(
                array(
                    'url' => $url ?? "",
                    'name' => $name,
                )
            );
        } else return $check;
    }

    public function getBrandNameById($id)
    {
        $this->setupModel();
        if ($id>0) {
            $checkBrandExists = $this->Brand->find("first",
                array('conditions' =>
                    array(
                        'id' => $id
                    )
                )
            );
            if (count($checkBrandExists) == 0) {
                return false;
            }
            return $checkBrandExists['Brand']['name'];
        }
    }

    public function getManufacturerNameById($id)
    {
        $this->setupModel();
        if ($id>0) {
            $checkExists = $this->Manufacturer->find("first",
                array('conditions' =>
                    array(
                        'id' => $id
                    )
                )
            );
            if (count($checkExists) == 0) {
                return false;
            }
            return $checkExists['Manufacturer']['name'];
        }
    }

    public function getAllManufacturers()
    {
        $this->setupModel();
        $list = $this->Manufacturer->find("all",
            array('conditions' =>
                array(

                ),
                'order' => 'name ASC'
            )
        );
        if (count($list) == 0) {
            return false;
        }
        return $list;
    }

}