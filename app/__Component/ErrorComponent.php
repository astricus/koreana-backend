<?php

App::uses('L10n', 'L10n');

/*
 * Компонент обработки ошибок
 * */

class ErrorComponent extends Component
{

	public $uses = array('Error');
	public $components = array('Session', 'Email');
	public $error;


	function initialize(Controller $controller)
	{
		$this->controller = $controller;
	}

	public function index()
	{
		echo "Контроллер ошибок";
		exit;
	}

	public function _app_exit($error_text)
	{
		$error_text = "Произошла критическая ошибка! Приложение пркаращает свою работу. <b> Администратор будет уведомлен об ошибке! Код ошибки: " . $error_text;
		die($error_text);
	}

	public function _ajax_error($error_content)
	{
		response_ajax(array('data' => $error_content), "error");

	}

	public function _is_log_active()
	{
		return Configure::read('ERROR_LOG_ACTIVE');
	}

	public function _database_error_log($type_id, $text, $ua, $os, $ip, $user_id = 0)
	{
		$error_data = array('Error' => array(
			'type_id' => $type_id,
			'text' => $text,
			'ua' => $ua,
			'ip' => $ip,
			'os' => $os,
			'user_id' => $user_id,
		));
		$model = ClassRegistry::init('Error');
		$model->save($error_data);
	}

	public function setError($error)
	{
		//получение массива конфига ошибки
		$this->error = Configure::read($error);

		if ($this->error == NULL) {
			//Вызвана неопознанная ошибка
			$this->setError('ERROR_1');
		} else {

			//получение ID ошибки
			$this->error_id = $this->error["ID"];

			//получение типа ошибки
			$this->error_type = $this->error["TYPE"];

			//получение типа ответа пользоватею
			$this->error_respone = $this->error["RESPONSE"];

			//получение статуса информирования админа
			$this->error_info = $this->error["INFO"];

			//получение текста ошибки
			$this->error_text = $this->error["TEXT"];

			//получение содержимого ответа при ошибке
			$this->error_respone_content = $this->error["RESPONSE_CONTENT"];

			//если ошибка - предупреждение, логирование не производится
			if ($this->_is_log_active() AND $this->error_type !== "WARNING") {
				$os = get_os();
				$ip = get_ip();
				$ua = get_ua();
				$user_id = $this->Session->read('User.id');
				//запись в БД
				$this->_database_error_log($this->error_id, $this->error_type, $ua, $os, $ip, $user_id);
			}

			//если тип ошибки не Стандартная (есть ответ)
			if ($this->error_type !== "STD") {
				//ответ аяксом
				if ($this->error_respone == "AJAX") {
					$this->_ajax_error($this->error_respone_content);


					//редирект на страницу
				} else if ($this->error_respone == "HTML") {
					$this->redirect(array('controller' => 'error', 'action' => 'index'));
				} //404 страница
				else if ($this->error_respone == "404") {
					throw new NotFoundException();
				} //выход
				else if ($this->error_respone == "DIE") {
					$this->_app_exit($this->error_respone_content);
				}
			}


			//сообщение срочное и критичное => Отправляется администратору незамедлительно
			if ($this->error_info == "YES") {

				$this->Email->from = Configure::read('SITE_MAIL');
				$this->Email->to = Configure::read('ADMIN_MAIL');
				$this->Email->subject = "Уведомление об ошибке";
				$this->Email->message = "Ошибка";
				$this->Email->send();
			}

			//в случае критичной ошибки выходим из приложения
			if ($this->error_type == "CR") {
				$this->_app_exit($this->error_respone_content);
			}
		}

	}

}