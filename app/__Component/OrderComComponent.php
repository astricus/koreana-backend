<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Заказ
 */
class OrderComComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'CompanyCom',
        'Validator',
        'DateTime'
    );

    // время ожидания на подтверждение позиций в новом заказе
    public $order_wait_time = 60 * 60 * 2;

    public $controller;
    public $user_data;
    public $user_id;

    public $last_error_message;

    public $error_unknown_order_status = "Неопределенный статус заказа";
    public $error_empty_order_id_or_null = "Пустой или некорректный идентификатор заказа";

    public $payment_types = [
        'site' => 'Оплата на сайте',
        'provider_cash' => 'Оплата курьеру наличными',
        'provider_card' => 'Оплата курьеру картой',
    ];

    public $payment_status = [
        'true' => 'Заказ оплачен',
        'false' => 'Заказ не оплачен',
    ];

    public function orderEventList()
    {
        return [
            'updateOrderData' => 'Были обновлены пользовательские данные заказа',
            'updateOrderList' => 'Были изменен состав заказа',
            'updateOrderCost' => 'Были изменена стоимость заказа',
            'updateOrderStatus' => 'Был изменен статус заказа',
            'updateOrderAddress' => 'Был изменен адрес доставки заказа',
            'updateOrderDelivery' => 'Были изменения в доставке заказа',
            'setOrderStatusNew' => 'Заказ создан и ждет обработки',
            'setOrderStatusProceed' => 'Заказ в обработке (ожидает подтверждение позиций)',
            'setOrderStatusCollected' => 'Заказ собран и готов к отгрузке',
            'setOrderStatusComplete' => 'Заказ отгружен и завершен',
            'setOrderStatusProblem' => 'Заказ помечен как проблемный',
            'setOrderStatusPartialConfirm' => 'Заказ частично подтвержден',
            'setOrderStatusConfirm' => 'Заказ полностью подтвержден',
            'setOrderStatusAbortByOrderer' => 'Заказ отменен заказчиком',
            'setOrderStatusAbortByShop' => 'Заказ отменен поставщиком',
            'setOrderStatusPickup' => 'Заказ готов к отгрузке, будет забран самостоятельно',
            'setOrderStatusDelivery' => 'Заказ забран транспортной компанией и доставляется',
        ];
    }

    public function orderEventTypes()
    {
        return [
            'updateOrderData' => 'default',
            'updateOrderList' => 'default',
            'updateOrderCost' => 'default',
            'updateOrderStatus' => 'default',
            'updateOrderAddress' => 'default',
            'updateOrderDelivery' => 'default',
            'setOrderStatusNew' => 'primary',
            'setOrderStatusProceed' => 'info',
            'setOrderStatusCollected' => 'mint',
            'setOrderStatusComplete' => 'success',
            'setOrderStatusProblem' => 'danger',
            'setOrderStatusPartialConfirm' => 'warning',
            'setOrderStatusConfirm' => 'warning',
            'setOrderStatusAbortByOrderer' => 'danger',
            'setOrderStatusAbortByShop' => 'danger',
            'setOrderStatusPickup' => 'purple',
            'setOrderStatusDelivery' => 'purple',
        ];
    }

    public function actionAgentList()
    {
        return [
            'company',
            'provider',
            'orderer',
            'manager'
        ];
    }

    /* статусы заказа*/
    public $valid_order_status_list = array(
        'new' => 'новый заказ',
        'proceed' => 'в обработке (собирается)',
        'start_confirm' => "Начат процесс подтверждения",
        'confirm' => "Подтвержден",
        'collected' => 'заказ забран и готов к отгрузке',
        'pickup' => 'заказ забран со склада поставщика',
        'partial_confirm' => 'частично подтвержден',
        'delivery' => 'доставляется',
        'complete' => 'обработан',
        'abort_by_orderer' => 'отменен заказчиком',
        'abort_by_shop' => 'отменен магазином',
        'problem' => "Проблема/Нестандартная ситуация",
    );

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    private function validateStatus($status)
    {
        $status_names = array_keys($this->valid_order_status_list);
        if (in_array($status, $status_names)) {
            return true;
        }
        return false;
    }

    /*сумма полной стоимости товаров в заказе*/
    public function shop_order_sum($order_id)
    {
        $order_products = $this->shop_order_products($order_id);
        $order_total_cost = 0;
        foreach ($order_products as $order_product) {
            $order_product = $order_product['Order_Product'];
            $product_amount = $order_product['amount'];
            $order_price = $order_product['order_price'];
            $product_sum_price = $product_amount * $order_price;
            $order_total_cost += $product_sum_price;
        }
        return $order_total_cost;
    }

    /*сумма полной стоимости товаров в заказе*/
    public function getOrderTotalCost($order_id)
    {
        $order_products = $this->getOrderOffers($order_id);
        $order_total_cost = 0;
        foreach ($order_products as $order_product) {
            $order_product = $order_product['Order_Product'];
            $product_amount = $order_product['amount'];
            $order_price = $order_product['order_price'];
            $product_sum_price = $product_amount * $order_price;
            $order_total_cost += $product_sum_price;
        }
        return $order_total_cost;
    }

    /*наименований в заказе*/
    public function shop_order_amount($order_id)
    {
        $order_products = $this->shop_order_products($order_id);
        return count($order_products);
    }

    /*список товаров заказа */
    public function shop_order_products($order_id)
    {
        $modelName = 'Order_Product';
        $this->Order_Product = ClassRegistry::init($modelName);
        $order_products = $this->Order_Product->find("all",
            array('conditions' =>
                array(
                    'order_id' => $order_id,
                    'company_id' => $this->CompanyCom->company_id()
                ),
            )
        );
        return $order_products;
    }

    public function getOrderOffers($order_id)
    {
        $modelName = 'Order_Product';
        $this->Order_Product = ClassRegistry::init($modelName);
        $order_products = $this->Order_Product->find("all",
            array('conditions' =>
                array(
                    'order_id' => $order_id,
                ),
            )
        );
        return $order_products;
    }

    public function getOrderEvents($order_id)
    {
        $modelName = 'Order_Event';
        $this->Order_Event = ClassRegistry::init($modelName);
        $order_events = $this->Order_Event->find("all",
            array('conditions' =>
                array(
                    'order_id' => $order_id,
                    'order_type' => 'order'
                ),
                'fields' => array('Order_Event.*',
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS time_ago',
                    //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(last_activity) AS last_act',
                ),
                'order' => array('created DESC')
            )
        );

        return $order_events;
    }

    // тестовый запрос набивки событиями заказа
    public function testFillOrderEvents($order_id)
    {

        $test_order_events_list = [];
        /*
         *  'updateOrderData' => 'Были обновлены пользовательские данные заказа',
            'updateOrderList' => 'Были изменен состав заказа',
            'updateOrderCost' => 'Были изменена стоимость заказа',
            'updateOrderStatus' => 'Был изменен статус заказа',
            'updateOrderAddress' => 'Был изменен адрес доставки заказа',
            'updateOrderDelivery' => 'Были изменения в доставке заказа',

            'setOrderStatusNew' => 'Заказ создан и ждет обработки',
            'setOrderStatusProceed' => 'Заказ в обработке (ожидает подтверждение позиций)',

            'setOrderStatusComplete' => 'Заказ отгружен и завершен',
            'setOrderStatusProblem' => 'Заказ помечен как проблемный',
            'setOrderStatusPartialConfirm' => 'Заказ частично подтвержден',
            'setOrderStatusCollected' => 'Заказ собран и готов к отгрузке',
            'setOrderStatusConfirm' => 'Заказ полностью подтвержден',
            'setOrderStatusAbortByOrderer' => 'Заказ отменен заказчиком',
            'setOrderStatusAbortByShop' => 'Заказ отменен поставщиком',
            'setOrderStatusPickup' => 'Заказ готов к отгрузке, будет забран самостоятельно',
            'setOrderStatusDelivery' => 'Заказ забран транспортной компанией и доставляется',
         * */
        $orderer_id = 1;
        $admin_id = 1;
        $company_id = 1;
        $provider_id = 1;
        //новый заказ
        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "orderer",
            'action_agent_id' => $orderer_id,
            'event_name' => "setOrderStatusNew",
            "event_data" => null
        ];
        // обработка
        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "company",
            'action_agent_id' => $company_id,
            'event_name' => "setOrderStatusProceed",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "company",
            'action_agent_id' => $company_id,
            'event_name' => "setOrderStatusPartialConfirm",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "company",
            'action_agent_id' => $company_id,
            'event_name' => "setOrderStatusPartialConfirm",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "company",
            'action_agent_id' => $company_id,
            'event_name' => "setOrderStatusProblem",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "company",
            'action_agent_id' => $company_id,
            'event_name' => "setOrderStatusCollected",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "orderer",
            'action_agent_id' => $orderer_id,
            'event_name' => "setOrderStatusAbortByOrderer",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "company",
            'action_agent_id' => $company_id,
            'event_name' => "setOrderStatusAbortByShop",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "admin",
            'action_agent_id' => $admin_id,
            'event_name' => "setOrderStatusPickup",
            "event_data" => null
        ];

        $test_order_events_list[] = [
            'order_id' => $order_id,
            'action_agent_type' => "provider",
            'action_agent_id' => $provider_id,
            'event_name' => "setOrderStatusDelivery",
            "event_data" => null
        ];

        foreach ($test_order_events_list as $test_item) {

            $this->addOrderEvent(
                $test_item['order_id'],
                $test_item['action_agent_type'],
                $test_item['action_agent_id'],
                $test_item['event_name'],
                $test_item['event_data']
            );
        }

    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function adminOrderProducts($order_id)
    {
        $modelName = 'Order_Product';
        $this->Order_Product = ClassRegistry::init($modelName);
        $order_products = $this->Order_Product->find("all",
            array('conditions' =>
                array(
                    'Order_Product.order_id' => $order_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'delivery_offers',
                        'alias' => 'Delivery_Offers',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Delivery_Offers.order_position_id = Order_Product.id'
                        )
                    ),
                    array(
                        'table' => 'stores',
                        'alias' => 'Store',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Store.id = Order_Product.store_id'
                        )
                    ),
                    array(
                        'table' => 'addresses',
                        'alias' => 'Address',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Address.id = Store.address_id'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'City.id = Address.city_id'
                        )
                    ),
                    array(
                        'table' => 'streets',
                        'alias' => 'Street',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Street.id = Address.street_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Order_Product.*',
                    'Delivery_Offers.*',
                    'Store.*',
                    'Address.*',
                    'City.*',
                    'Street.*',
                ),
            )
        );
        return $order_products;
    }

    /* список всех заказчиков заданного магазина */
    public function shop_orderers_list($shop_id = null)
    {
        $company_id = (int)$this->CompanyCom->company_id();
        if($company_id = null OR $company_id<=0){
            $company_id_arr = [];
        } else {
            $company_id_arr = ['Order.company_id' => $company_id];
        }
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        $orderers = $this->Order->find("all",
            array('conditions' =>
                array(
                    $company_id_arr
                ),
                'joins' => array(
                    array(
                        'table' => 'orderers',
                        'alias' => 'Orderer',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Order.orderer_id = Orderer.id'
                        )
                    ),
                ),
                'fields' => array(
                    'DISTINCT Orderer.*',
                ),
                'order' => array('Orderer.id DESC')
            )
        );
        return $orderers;
    }

    /* список всех городов заказчиков для заданного магазина или магазинов*/
    public function shop_orderers_city_list($shop_id = null)
    {
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        $cities = $this->Order->find("all",
            array('conditions' =>
                array(
                    'company_id' => $this->CompanyCom->company_id()
                ),
                'joins' => array(
                    array(
                        'table' => 'client_orders',
                        'alias' => 'Client_Order',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Client_Order.id = Order.client_order_id'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Client_Order.order_city = City.id'
                        )
                    ),
                ),
                'fields' => array(
                    'DISTINCT City.*',
                ),
                'order' => array('City.name ASC')
            )
        );
        return $cities;
    }

    /*список заказов по заказчику */
    public function get_orders_by_orderer($orderer_id)
    {
        $company_id = $this->CompanyCom->company_id();
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        $orderer_orders = $this->Order->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $orderer_id,
                    'company_id' => $company_id
                ),
            )
        );
        return $orderer_orders;
    }

    /*последний заказ заказчика */
    public function get_last_order_by_orderer($orderer_id)
    {
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        $last_order = $this->Order->find("first",
            array(
                'conditions' =>
                    array(
                        'orderer_id' => $orderer_id,
                        'company_id' => $this->CompanyCom->company_id()
                    ),
                'order' => array('id DESC')
            )
        );
        $last_order_sum = $this->shop_order_sum($last_order['Order']['id']);
        $last_order['Order']['sum'] = $last_order_sum;
        return $last_order;
    }

    /*список новых заказов по всем магазинам данного поставщика */
    public function get_new_orders_by_company_id()
    {
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        $new_orders = $this->Order->find("all",
            array('conditions' =>
                array(
                    'company_id' => $this->CompanyCom->company_id(),
                    'status' => 'new'
                ),
            )
        );
        $sum = 0;
        foreach ($new_orders as $new_order) {
            $order_id = $new_order['Order']['id'];
            $sum += $this->shop_order_sum($order_id);
        }
        return ['sum' => $sum, 'count' => count($new_orders)];
    }

    /**
     * @param $user_id
     * @param $order_data
     * @param $products_list
     * @param $company_id
     * @return bool
     */
    public function makeOrder($user_id, $order_data, $products_list, $company_id)
    {
        $modelName = "Order";
        $this->Order = ClassRegistry::init($modelName);

        $modelName = "Order_Product";
        $this->Order_Product = ClassRegistry::init($modelName);
        $order_id = $this->makeOrderInCompany($user_id, $order_data, $company_id);

        foreach ($products_list as $products_list_item) {
            $products_list_item = $products_list_item[0];

            if ($products_list_item['company_id'] == $company_id) {
                $this->addOrderProduct($user_id, $company_id, $products_list_item['price'], $products_list_item['amount'], $products_list_item['product_id'], $order_id);
            }
        }
        return true;
    }

    /**
     * @param $order_id
     * @param $order_data
     * @param $admin_id
     * @return bool
     * сохранение заказа администратором
     */
    public function updateOrderByAdmin($order_id, $order_data, $admin_id)
    {
        $result_validate = $this->validateOrderData($order_data, $order_id);
        if ($result_validate) {
            $this->saveClientOrder($order_id, $order_data);
            // данные заказа сохранены, необходимо это внести в историю заказа
            // отбор изменившихся полей
            $changed_fields = $this->compareOrderData($order_id, $order_data);
            $this->addOrderEvent($order_id, "manager", $admin_id, "updateOrderData", serialize($changed_fields));
            return true;
        } else {
            return $result_validate;
        }
    }

    private function compareOrderData($id, $new_data)
    {
        $compare_fields = [
            "order_email", "order_phone", "order_lastname", "order_firstname", "order_middlename", "order_address",
        ];
        $modelName = 'Client_Order';
        $this->Client_Order = ClassRegistry::init($modelName);
        $client_order = $this->Client_Order->find("first",
            array('conditions' =>
                array(
                    'id' => $id,
                ),
            )
        );
        $changed_fields = [];
        foreach ($client_order as $client_order_item) {
            foreach ($compare_fields as $compare_field) {
                $old_value = $client_order_item[$compare_field];
                $new_value = $new_data[$compare_field] ?? null;
                if ($old_value !== $new_value) {
                    $changed_fields[] = [$compare_field => $new_data[$compare_field] ?? null];
                }
            }
        }
        return $changed_fields;
    }

    /**
     * @param $order_data
     * @param $order_id
     * @return array|bool
     */
    private function validateOrderData($order_data, $order_id)
    {
        $errors = [];
        $flash_message = "";
        $result_status = true;
        if ($order_id == null OR intval($order_id) < 0) {
            $flash_message = $this->error_empty_order_id_or_null;
            $errors[] = 'error_empty_order_id_or_null';
        }
        // валидация пользовательских полей
        $validate_fields = [
            'order_phone' => "valid_phone_number",
            'order_firstname' => "valid_firstname",
            'order_lastname' => "valid_lastname",
            'order_middlename' => "valid_middlename",
        ];

        //запуск методов валидации полей
        foreach ($validate_fields as $key => $value) {
            if (method_exists($this->Validator, $value)) {
                $valid_result = $this->Validator->$value($key);
                if ($valid_result['result'] === false) {
                    $result_status = false;
                    $flash_message = $valid_result['message'];
                    $errors[] = $valid_result['error'];
                }
            }
        }
        if (!$this->validateStatus($order_data['status'])) {
            $result_status = false;
            $flash_message = $this->error_unknown_order_status;
            $errors[] = "error_unknown_order_status";
        }
        if ($result_status) {
            return true;
        } else {
            return [
                'flash_message' => $flash_message,
                'errors' => $errors
            ];
        }

    }

    // общий метод для сохранения/обновления заказа, может вызываться из нескольких надметодов

    /**
     * @param $order_id
     * @param $order_data
     */
    public function saveClientOrder($order_id, $order_data)
    {
        $modelName = 'Client_Order';
        $this->Client_Order = ClassRegistry::init($modelName);
        $this->Client_Order->id = $order_id;
        $this->Client_Order->save($order_data);
    }

    /**
     * @param $user_id
     * @param $order_data
     * @param $company_id
     * @return mixed
     */
    public function makeOrderInCompany($user_id, $order_data, $company_id)
    {
        $order_data['orderer_id'] = $user_id;
        $order_data['company_id'] = $company_id;
        $this->Order->create();
        $this->Order->save($order_data);
        return $this->Order->id;

    }

    /**
     * @param $user_id
     * @param $company_id
     * @param $price
     * @param $amount
     * @param $product_id
     * @param $order_id
     * @return mixed
     */
    public function addOrderProduct($user_id, $company_id, $price, $amount, $product_id, $order_id)
    {
        $product_data['orderer_id'] = $user_id;
        $product_data['company_id'] = $company_id;
        $product_data['order_price'] = $price;
        $product_data['amount'] = $amount;
        $product_data['product_id'] = $product_id;
        $product_data['order_id'] = $order_id;
        $this->Order_Product->create();
        $this->Order_Product->save($product_data);
        return $this->Order_Product->id;

    }

    /**
     * @param $order_id
     * @return array|bool
     */
    public function getClientOrderById($order_id)
    {
        //$this->setup();
        $modelName = 'Client_Order';
        $this->Client_Order = ClassRegistry::init($modelName);

        $modelName = 'Order_Product';
        $this->Order_Product = ClassRegistry::init($modelName);

        // Выборка клиентски данных и адреса доставки
        $client_order = $this->Client_Order->find("first",
            array('conditions' =>
                array(
                    'Client_Order.id' => $order_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'addresses',
                        'alias' => 'Address',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Address.id = Client_Order.order_address'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'City.id = Address.city_id'
                        )
                    ),
                    array(
                        'table' => 'streets',
                        'alias' => 'Street',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Street.id = Address.street_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Client_Order.*',
                    'Address.*',
                    'City.*',
                    'Street.*',
                ),
            )
        );

        if ($client_order) {
            $data = [
                'client_personal_data' => [
                    'firstname' => $client_order['Client_Order']['order_firstname'],
                    'lastname' => $client_order['Client_Order']['order_lastname'],
                    'middlename' => $client_order['Client_Order']['order_middlename'],
                    'email' => $client_order['Client_Order']['order_email'],
                    'phone' => $client_order['Client_Order']['order_phone'],
                ],
                'client_delivery_address' => [
                    'city_id' => $client_order['Address']['city_id'],
                    'city' => $client_order['City']['name'],
                    'street_type' => $client_order['Street']['type'],
                    'street' => $client_order['Street']['name'],
                    'building' => $client_order['Address']['building'],
                    'house' => $client_order['Address']['house'],
                ],
                'payment_status' => $client_order['Client_Order']['payment_status'] ? 1 : 0,
                'payment_type' => $client_order['Client_Order']['payment_type'],
            ];
        } else {
            return false;
        }

        // Выборка составляющих заказа
        $order_products = $this->Order_Product->find("all",
            array('conditions' =>
                array(
                    'Order_Product.client_order_id' => $order_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Product.id = Order_Product.product_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Order_Product.*',
                    'Product.*',
                ),
            )
        );

        $total_price = 0; // Полная стоимость заказа
        $weight = 0; // Полный вес доставки
        $total_volume = 0; // суммарные габариты
        foreach ($order_products as $val) {
            $total_price += $val['Order_Product']['amount'] * $val['Order_Product']['order_price'];
            $weight += $val['Product']['weight'];
            $volume = $val['Product']['volume'];
            if($volume == 0){
                $volume = $val['Product']['height']
                    *$val['Product']['width']
                    *$val['Product']['length'];
            }
            $total_volume += $volume;

            $product[$val['Order_Product']['order_id']][] = [
                'id' => $val['Order_Product']['product_id'],
                'name' => $val['Product']['product_name'],
                'amount' => $val['Order_Product']['amount'],
                'price' => $val['Order_Product']['order_price'],
                'barcode' => $val['Product']['system_barcode'],
                'weight' => $val['Product']['weight'],
            ];

        }

        $data['total_price'] = $total_price;
        $data['total_volume'] = $total_volume;
        $data['total_weight'] = $weight;
        $data['products'] = $product;
        $data['places'] = count($order_products);


        //pr($data);

        return $data;
    }

    public function getOrderById($order_id)
    {
         $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);

        $modelName = 'Order_Product';
        $this->Order_Product = ClassRegistry::init($modelName);

        // Выборка клиентски данных и адреса доставки
        $order = $this->Order->find("first",
            array('conditions' =>
                array(
                    'Client_Order.id' => $order_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'client_orders',
                        'alias' => 'Client_Order',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Order.client_order_id = Client_Order.id'
                        )
                    ),
                    array(
                        'table' => 'addresses',
                        'alias' => 'Address',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Address.id = Client_Order.order_address'
                        )
                    ),
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'City.id = Address.city_id'
                        )
                    ),
                    array(
                        'table' => 'streets',
                        'alias' => 'Street',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Street.id = Address.street_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Client_Order.*',
                    'Address.*',
                    'City.*',
                    'Street.*',
                    'Order.*'
                ),
            )
        );

        if ($order) {
            $data = [
                'client_personal_data' => [
                    'firstname' => $order['Client_Order']['order_firstname'],
                    'lastname' => $order['Client_Order']['order_lastname'],
                    'middlename' => $order['Client_Order']['order_middlename'],
                    'email' => $order['Client_Order']['order_email'],
                    'phone' => $order['Client_Order']['order_phone'],
                ],
                'client_delivery_address' => [
                    'city_id' => $order['Address']['city_id'],
                    'city' => $order['City']['name'],
                    'street_type' => $order['Street']['type'],
                    'street' => $order['Street']['name'],
                    'building' => $order['Address']['building'],
                    'house' => $order['Address']['house'],
                ],
                'payment_status' => $order['Order']['payment_status'] ? 1 : 0,
                'payment_type' => $order['Order']['payment_type'],
            ];
        } else {
            return false;
        }

        // Выборка составляющих заказа
        $order_products = $this->Order_Product->find("all",
            array('conditions' =>
                array(
                    'Order_Product.order_id' => $order_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Product.id = Order_Product.product_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Order_Product.*',
                    'Product.*',
                ),
            )
        );

        $total_price = 0; // Полная стоимость заказа
        $weight = 0; // Полный вес доставки
        $total_volume = 0; // суммарные габариты
        foreach ($order_products as $val) {
            $total_price += $val['Order_Product']['amount'] * $val['Order_Product']['order_price'];
            $weight += $val['Product']['weight'];
            $volume = $val['Product']['volume'];
            if($volume == 0){
                $volume = $val['Product']['height']
                    *$val['Product']['width']
                    *$val['Product']['length'];
            }
            $total_volume += $volume;

            $product[$val['Order_Product']['order_id']][] = [
                'id' => $val['Order_Product']['product_id'],
                'name' => $val['Product']['product_name'],
                'amount' => $val['Order_Product']['amount'],
                'price' => $val['Order_Product']['order_price'],
                'barcode' => $val['Product']['system_barcode'],
                'weight' => $val['Product']['weight'],
            ];
        }

        $data['total_price'] = $total_price;
        $data['total_volume'] = $total_volume;
        $data['total_weight'] = $weight;
        $data['products'] = $product;
        $data['order_cost'] = $this->shop_order_sum($order_id);

        $data['places'] = count($order_products);
        return $data;
    }


    public function getCompanyTransportDateAndPrice($company_id)
    {
        if ($company_id == null) {
            $company_id = $this->CompanyCom->company_id();
        }
        //HARDCODE
        if (date("h") < 14) {
            $transport_date = only_date(now_date());
        } else {
            $transport_date = only_date(mktime(0, 0, 0, date("m"), date("d") + 2, date("Y")));
        }
        return ['date' => $transport_date, 'price' => 1210];
    }

    public function getCompanyPickupDateAndPrice($company_id)
    {
        if ($company_id == null) {
            $company_id = $this->CompanyCom->company_id();
        }
        //HARDCODE
        if (date("h") < 14) {
            $transport_date = only_date(now_date());
        } else {
            $transport_date = only_date(mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
        }
        return ['date' => $transport_date, 'price' => 1310];
    }

    private function addOrderEvent($order_id, $action_agent_type, $action_agent_id, $event_name, $event_data)
    {
        if (!$this->validateOrderId($order_id)) {
            return false;
        }
        if (!$this->validateActionAgent($action_agent_type)) {
            return false;
        }
        $modelName = 'Order_Event';
        $this->Order_Event = ClassRegistry::init($modelName);
        $event = [
            'order_id' => $order_id,
            'order_type' => 'order',
            'action_agent_type' => $action_agent_type,
            'action_agent_id' => $action_agent_id,
            'event_name' => $event_name,
            'event_data' => $event_data,
        ];
        $this->Order_Event->create();
        $this->Order_Event->save($event);
        return $this->Order_Event->id;
    }

    private function validateOrderId($id)
    {
        if (intval($id) <= 0) {
            return false;
        }
        return true;
    }

    private function validateActionAgent($action_agent)
    {
        if (!in_array($action_agent, $this->actionAgentList())) {
            return false;
        }
        return true;
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function adaptiveDeliveryOrderStatus($order_id)
    {
        /* адаптивный статус за конкретного заказа
         * Если заказ находится в доставке - delivery, если нет - то прочерк, если complete - до доставлен или забран
         * */
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        $order = $this->Order->find("first",
            array('conditions' =>
                array(
                    'id' => $order_id,
                ),
            )
        );

        $order_status = $order['Order']['status'];
        return $order_status;
    }

    /**
     * @param $order_id
     * @return array
     */
    public function getAllDeliveriesInOrder($order_id)
    {
        /* Список всех доставок, которыми доставляется данный подзаказ
         * */
        $modelName = 'Delivery_Offer';
        $this->Delivery_Offer = ClassRegistry::init($modelName);
        $deliveries_items = $this->Delivery_Offer->find("all",
            array('conditions' =>
                array(
                    'order_id' => $order_id,
                ),
            )
        );
        $all_devs = [];
        foreach ($deliveries_items as $deliveries_item) {
            $delivery_id = $deliveries_item['Delivery_Offer']['id'];
            if (!in_array($delivery_id, $all_devs)) {
                $all_devs[] = $delivery_id;
            }
        }

        return $all_devs;
    }

    // помечание заказа как успешно оплаченного
    public function orderPaidSuccess($order_id, $method)
    {
        if ($method != null) {
            $valid_methods = array_keys($this->payment_types);
            if (in_array($method, $valid_methods)) {
                $order_data['method'] = $method;
            }
        }
        $modelName = 'Order';
        $this->Order->id = $order_id;
        $this->Order = ClassRegistry::init($modelName);
        $order_data['payment_status'] = 'true';
        $this->Order->save($order_data);
    }

    public function checkOrderExists($order_id)
    {
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        return $this->Order->find("count",
            array('conditions' =>
                array(
                    'id' => $order_id,
                ),
            )
        );
    }

    //изменение кол-ва позиций в заказе - не актуально для MVP версии - функционал отсутствует

    // методы связанные с истечением срока подтверждения заказа поставиком
    public function timeBeforeExpired($order_id)
    {
        $order = $this->Order->find("first",
            array('conditions' =>
                array(
                    'id' => $order_id,
                ),
                'fields' => array(
                    'Order.*',
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS time_ago',
                ),
            )
        );

        $order_status = $order['Order']['status'];
        if ($order_status == "new") {
            $order_time = $order[0]['time_ago'];
            return abs($order_time - $this->order_wait_time);
        }
        return 0;
    }

    /**
     * @param $company_id
     * @return array
     */
    public function getWaitingOrdersByCompanyId($company_id)
    {
        $orders = $this->Order->find("all",
            array('conditions' =>
                array(
                    'company_id' => $company_id,
                ),
                'fields' => array(
                    'Order.*',
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS time_ago',
                ),
            )
        );
        $order_list = [];
        foreach ($orders as $order){
            $order_id = $order['Order']['id'];
            $order_status = $order['Order']['status'];
            if ($order_status == "new") {
                $order_time = $order[0]['time_ago'];
                if($this->order_wait_time > $order_time){
                    $order_list[] = ['id' => $order_id, 'wait_time' => $this->DateTime->day_separator($order_time)];
                }
            }
        }

        return $order_list;
    }

    //фоновая задача - переводить заказы в статус неподтвержденный в случае, если прошло время ожидания заказа
    //CONSOLE
    public function setOrderExpired()
    {
        $orders = $this->Order->find("all",
            array('conditions' =>
                array(
                    'Order.status' => 'new'
                ),

                'fields' => array(
                    'Order.*',
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) AS time_ago',
                ),
                'order' => array('order.id DESC')
            )
        );
        foreach ($orders as $order) {
            $order_id = $order['Order']['id'];
            $order_time = $order[0]['time_ago'];
            if ($order_time > $this->order_wait_time) {
                $this->setOrderStatus($order_id, "start_confirm");
            }
        }
    }

    /**
     * @param $order_id
     * @param $order_status
     */
    private function setOrderStatus($order_id, $order_status)
    {
        $modelName = 'Order';
        $this->Order = ClassRegistry::init($modelName);
        $this->Order->id = $order_id;
        $order_data['status'] = $order_status;
        $this->Order->save($order_data);
    }


}