<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Грузчик
 */
class LoaderComponent extends Component
{
    public $components = array(
        'Log',
        'Validator',
        'Session',
        'Flash'
    );

    public $loader_data_is_incorrect = "Данные грузчика переданы некорректно";
    public $incorrect_firstname = "Некорректное имя грузчика";
    public $incorrect_lastname = "Некорректная фамилия грузчика";
    public $incorrect_phone_number = "Некорректный номер телефона грузчика";
    public $unknown_status = "Передан неизвестный статус";
    public $loader_not_found = "Грузчик не найден";

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    public function setup()
    {
        $modelName = "Loader";
        $this->Loader = ClassRegistry::init($modelName);
    }

    /**
     * @param $name
     * @return array|null
     */
    public function findLoadersByName($name){
        $this->setup();
        $loaders = $this->Loader->find("first",
            array('conditions' =>
                array(
                    'or' => array(
                        array(
                            'lastname LIKE' => "%$name%",
                        ),
                        array(
                            'firstname LIKE' => "%$name%",
                        ),
                    ),
                ),
            )
        );
        if (count($loaders) == 0) {
            return null;
        }
        return $loaders;
    }

    /**
     * @param $id
     * @return array|null
     */
    public function getLoaderById($id)
    {
        $this->setup();
        $loader = $this->Loader->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                ),
            )
        );
        if (count($loader) == 0) {
            return null;
        }
        return $loader;

    }

    /**
     * @return array|null
     */
    public function getActiveLoaders()
    {
        $this->setup();
        $loaders = $this->Loader->find("all",
            array('conditions' =>
                array(
                    'status' => 'active'
                ),
                'order' => array('id DESC')
            )
        );
        if (count($loaders) == 0) {
            return null;
        }
        return $loaders;
    }

    /**
     * @param $id
     * @param $data
     * @return array|bool
     */
    public function setLoaderData($id, $data)
    {
        $validate = $this->validateLoader($data, "update");
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->loader_data_is_incorrect . " " . $validate["error"] ];
        }
        $this->setup();
        $this->Loader->id = $id;
        $this->Loader->save($data);
        return true;
    }

    /**
     * @param $data
     * @param $action
     * @return array
     */
    private function validateLoader($data, $action)
    {
        $valid_status = ['active', 'blocked'];
        if($action == "create"){
            if (empty($data['firstname']) OR !$this->Validator->valid_firstname($data['firstname'])) {
                return ["status" => "error", "error" => $this->incorrect_firstname];
            }
            if (empty($data['lastname']) OR !$this->Validator->valid_firstname($data['lastname'])) {
                return ["status" => "error", "error" => $this->incorrect_lastname];
            }
            if (empty($data['phone_number']) OR !$this->Validator->valid_phone_number($data['phone_number'])) {
                return ["status" => "error", "error" => $this->incorrect_phone_number];
            }
        } else {
            if (!empty($data['firstname']) AND !$this->Validator->valid_firstname($data['firstname'])) {
                return ["status" => "error", "error" => $this->incorrect_firstname];
            }
            if (!empty($data['lastname']) AND !$this->Validator->valid_firstname($data['lastname'])) {
                return ["status" => "error", "error" => $this->incorrect_lastname];
            }
            if (!empty($data['phone_number']) OR !$this->Validator->valid_phone_number($data['phone_number'])) {
                return ["status" => "error", "error" => $this->incorrect_phone_number];
            }
            if (!empty($data['status']) AND !in_array($data['status'], $valid_status)){
                return ["status" => "error", "error" => $this->unknown_status];
            }
        }
        return ["status" => "success"];
    }

    /**
     * @param $id
     * @return bool
     */
    public function setLoaderActive($id)
    {
        $fields = ['status' => 'active'];
        $this->setup();
        $this->Loader->id = $id;
        $this->Loader->save($fields);
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function setLoaderBlocked($id)
    {
        $fields = ['status' => 'blocked'];
        $this->setup();
        $this->Loader->id = $id;
        $this->Loader->save($fields);
        return true;
    }

    /**
     * @param $date
     * @return array
     */
    public function getWorkingLoaderByDate($date)
    {
        return MockComponent::ARR;
        /*
        $this->setup();
        if (!$this->Validator->valid_date($date)) {
            return false;
        }
        $working_loaders = $this->Delivery_Route->find("all",
            array('conditions' =>
                array(
                    'route_date' => $date
                ),
                'joins' => array(
                    array(
                        'table' => 'loaders',
                        'alias' => 'Loader',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Delivery_Route.loader_id = Loader.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Loader.*',
                    'Delivery_Route.*'
                ),
            )
        );
        if (count($working_loaders) == 0) {
            return null;
        }
        return $working_loaders['Loader'];*/
    }

    /**
     * @param int $loader_id
     * @return bool|null|mixed
     */
    public function getWorkingDateByLoaderId($loader_id)
    {
        return MockComponent::ARR;
        $this->setup();
        /*
        if ($this->getLoaderById($loader_id)) {
            return false;

        }
        $working_dates = $this->Delivery_Route->find("all",
            array('conditions' =>
                array(
                    'loader_id' => $loader_id
                )
            )
        );
        if (count($working_dates) == 0) {
            return null;
        }
        return $working_dates['Delivery_Route'];
        */
    }

    /**
     * @param $data
     * @return array|null
     */
    public function createLoader($data)
    {
        $validate = $this->validateLoader($data, "create");
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->loader_data_is_incorrect . " " . $validate["error"] ];
        }
        $this->setup();

        $data['auth_code'] = "";
        $data['status'] = 'active';
        $this->Loader->create();
        $this->Loader->save($data);
        if($this->Loader->id>0){
            return $this->Loader->id;
        }
        return null;
    }

    public function getAllLoaders()
    {
        $this->setup();
        $loaders = $this->Loader->find("all",
            array('conditions' =>
                array(
                    //'status' => 'active'
                ),
                'order' => array('id DESC')
            )
        );
        if (count($loaders) == 0) {
            return null;
        }
        return $loaders;
    }

    /**
     * @param $loader_id
     * @param $data
     * @return array|null
     */
    public function saveLoader($loader_id, $data)
    {
        $this->setup();
        if ($this->getLoaderById($loader_id) == null) {
            return ["status" => "error", "error" => $this->loader_not_found ];
        }
        $validate = $this->validateSaveLoader($data);
        if ($validate['status' == "error"]) {
            return ["status" => "error", "error" => $this->loader_data_is_incorrect . " " . $validate["error"] ];
        }

        $this->Loader->id = $loader_id;
        $this->Loader->save($data);
        if($this->Loader->id>0){
            return $this->Loader->id;
        }
        return null;
    }

    /**
     * @param $data
     * @return array
     */
    private function validateSaveLoader($data)
    {
        if (!$this->Validator->valid_phone_number($data['phone_number'])){
                return ["status" => "error", "error" => $this->incorrect_phone_number];
        }
        if (!$this->Validator->valid_firstname($data['firstname'])){
            return ["status" => "error", "error" => $this->incorrect_firstname];
        }

        if (!$this->Validator->valid_lastname($data['lastname'])){
            return ["status" => "error", "error" => $this->incorrect_lastname];
        }
        return ["status" => "success"];
    }

    public function deleteLoader($loader_id)
    {
        $this->setup();
        if ($this->getLoaderById($loader_id) == null) {
            return ["status" => "error", "error" => $this->loader_not_found];
        }
        $this->Loader->id = $loader_id;
        $this->Loader->delete();
        return true;
    }


}