<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Адрес
 */
class AddressComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Mock'
    );

    private $address_fields = [
        'region_id',
        'city_id',
        'street_id',
        'building',
        'house'
    ];

    public $controller;


    /** Получить координаты адреса
     * @param array $address - массив с данными адреса доставки
     * @return array|bool
     */
    public function getCoordAddress(array $address)
    {
        $yaApiKey = Configure::read('YA_API_KEY');
        $address_string = $address['city'] . ' , ' . $address['street_type'] . ' ' . $address['street'] . ' , д.' . $address['building'];
        $url = 'https://geocode-maps.yandex.ru/1.x?geocode=' . urlencode($address_string) . '&apikey=' . $this->$yaApiKey . '&format=json';
        $res = json_decode(file_get_contents($url));
        if (count($res->response->GeoObjectCollection->featureMember) > 0) {
            $coord_arr = explode(' ', $res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
            return [
                'lat' => $coord_arr[1],
                'lng' => $coord_arr[0],
            ];
        } else {
            return false;
        }
    }

    /** Добавление / Обновление зон доставки
     * @param $file_path - путь до файла .kml
     * @return bool
     */
    public function updateZoneDelivery($file_path)
    {
        @$xml = simplexml_load_file($file_path);
        if ($xml) {
            foreach ($xml->Document->Folder->Placemark as $val) {
                $polygon_coords = '';
                $zone_code = trim($val->name);
                $a = explode(' ', $val->Polygon->outerBoundaryIs->LinearRing->coordinates);
                array_pop($a);
                foreach ($a as $v) {
                    $b = explode(',', $v);
                    $polygon_coords .= $b[0] . ' ' . $b[1] . ',';
                }
                $polygon_coords = substr($polygon_coords, 0, -1);
                $modelName = "Marketing_Zone";
                $this->Marketing_Zone = ClassRegistry::init($modelName);
                $zone = $this->Marketing_Zone->find('first',
                    array(
                        'conditions' => array(
                            'code' => $zone_code,
                        )
                    )
                );
                if ($zone) {
                    $p = 'POLYGON( (' . $polygon_coords . '))';
                    $this->Marketing_Zone->query("UPDATE delivery_marketing_zones SET polygon=PolygonFromText('" . $p . "') WHERE code='$zone_code'");
                } else {
                    $p = 'POLYGON( (' . $polygon_coords . '))';
                    $this->Marketing_Zone->query("INSERT INTO delivery_marketing_zones (`name`,`code`,`polygon`) 
                                                    VALUES ('Зона $zone_code','" . $zone_code . "',PolygonFromText('" . $p . "'))");
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /** Получить зону доставки
     * @param $coord - координаты в формате 30.405110 59.910472 (долгота широта)
     * @return array | bool
     */
    public function getZoneDelivery($coord)
    {
        $modelName = "Marketing_Zone";
        $this->Marketing_Zone = ClassRegistry::init($modelName);

        $res = $this->Marketing_Zone->query("SELECT `id`,`name`,`code`
                    FROM delivery_marketing_zones 
                    WHERE Contains(polygon,GeomFromText('POINT(" . $coord . ")')) > 0
                    ORDER BY code ASC LIMIT 1");
        if ($res) {
            return $res[0]['delivery_marketing_zones'];
        } else {
            return false;
        }
    }

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Address";
        $this->Address = ClassRegistry::init($modelName);

        $modelName = "Street";
        $this->Street = ClassRegistry::init($modelName);
    }

    /**
     * @param $address
     * @return bool
     */
    private function isValidAddress($address)
    {
        foreach ($this->address_fields as $item) {
            if (!key_exists($item, $address)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $address
     * @param $owner_type
     * @param $owner_id
     * @return bool
     */
    public function addAddress($address, $owner_type, $owner_id)
    {
        $this->setup();
        if (!$this->isValidAddress($address)) {
            return false;
        }
        $address['owner_type'] = $owner_type;
        $address['owner_id'] = $owner_id;
        $this->Address->save($address);
        return $this->Address->id;
    }

    /**
     * @param $address
     * @param $owner_type
     * @param $address_id
     * @return bool
     */
    public function saveAddress($address, $owner_type, $address_id)
    {
        $this->setup();
        if (!$this->isValidAddress($address)) {
            return false;
        }

        $address['owner_type'] = $owner_type;
        $this->Address->id = $address_id;
        $this->Address->save($address);
        return true;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAddressById($id)
    {
        $this->setup();
        return $this->Address->find('first',
            array(
                'conditions' => array(
                    'id' => $id,
                )
            )
        );
    }

    /**
     * @param $owner_type
     * @param $owner_id
     * @return mixed
     */
    public function getAdressListByOwnerTypeAndId($owner_type, $owner_id)
    {
        $this->setup();
        $address_list = $this->Address->find('all',
            array(
                'conditions' => array(
                    'owner_type' => $owner_type,
                    'owner_id' => $owner_id,
                )
            )
        );
        return $address_list;
    }

    /**
     * @param $region_id
     * @return mixed
     */
    public function getAdressListByRegionId($region_id)
    {
        $this->setup();
        $address_list = $this->Address->find('all',
            array(
                'conditions' => array(
                    'region_id' => $region_id,
                ),
                'order' => array('city_id ASC')
            )
        );
        return $address_list;
    }

    /**
     * @param $city_id
     * @return mixed
     */
    public function getAdressListByCityId($city_id)
    {
        $this->setup();
        $address_list = $this->Address->find('all',
            array(
                'conditions' => array(
                    'city_id' => $city_id,
                ),
                'order' => array('street_id ASC')
            )
        );
        return $address_list;
    }

    /**
     * @param $provider_store_id
     * @return mixed
     */
    public function getProviderStoreAddress($provider_store_id)
    {
        return $this->getAdressListByOwnerTypeAndId("provider_store", $provider_store_id);
    }

    /**
     * @param $company_store_id
     * @return mixed
     */
    public function getCompanyStoreAddress($company_store_id)
    {
        return $this->getAdressListByOwnerTypeAndId("company_store", $company_store_id);
    }

    /**
     * @param $orderer_id
     * @return mixed
     */
    public function getOrdererAddressList($orderer_id)
    {
        return $this->getAdressListByOwnerTypeAndId("orderer", $orderer_id);
    }

    /**
     * @param $street_id
     * @return mixed
     */
    public function getStreetNameById($street_id)
    {
        $this->setup();
        $street = $this->Street->find('first',
            array(
                'conditions' => array(
                    'id' => $street_id,
                ),
            )
        );
        $street = $street['Street'];
        return $street['type'] . " " . $street['name'];
    }

    /**
     * @param $address
     * @return string
     */
    public function simpleAddress($address)
    {
        $this->setup();
        return $this->getStreetNameById($address['street_id']) . ", дом " . $address['building'] . " кв/офис" . $address['house'];
    }


}