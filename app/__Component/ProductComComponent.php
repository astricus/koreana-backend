<?php

App::uses(
    'AppController', 'Controller'
);
App::uses('Model', 'Model');
App::uses('L10n', 'L10n');

/**
 * Компонент Товар
 */
class ProductComComponent extends Component
{
    public $components = array(
        'CompanyCom',
        'Country',
        'Error',
        'Log',
        'Manufacturer',
        'Moderate',
        'Session',
        'UserCom',
        'Region'
    );

    public $size_values = array(
        'm' => 1000,
        'M' => 1000,
        "м" => 1000,
        "М" => 1000,
        "Метр" => 1000,
        "метр" => 1000,
        "Метра" => 1000,
        "м-р" => 1000,
        'cm' => 10,
        'CM' => 10,
        "см" => 10,
        "СМ" => 10,
        "с-м" => 10,
        "сантиметр" => 10,
        "сантиметра" => 10,
        "Сантиметр" => 10,
        'dm' => 100,
        'DM' => 100,
        "дм" => 100,
        "ДМ" => 100,
        "дециметр" => 100,
        "Дециметр" => 100,
        "дециметра" => 100
    );

    public $weight_values = array(
        'g' => 1,
        'G' => 1,
        "г" => 1,
        "Г" => 1,
        "грамм" => 1,
        "Грамм" => 1,
        "Гр" => 1,
        "кг" => 1000,
        'КГ' => 1000,
        'килограмм' => 1000,
        'Килограмм' => 1000,
        "kg" => 1000,
        "KG" => 1000,
        "т" => 1000000,
        "тон" => 1000000,
        "тонны" => 1000000,
        "тонна" => 1000000,
        't' => 1000000,
        'ton' => 1000000,
    );

    public $required_price_parser_source = [
        '1',//petrovich
        'lerua'
    ];

    public $optional_product_offer_fields = [
        'sales_notes',
        'manufacturer_warranty',
        'package_count',
        'package_width',
        'package_height',
        'package_length',
        'package_weight',
        'barcode',
        'shop_url'
    ];

    public $optional_product_offer_field_names = [
        'sales_notes' => 'Комментарий к предложению',
        'manufacturer_warranty' => 'Гарантия производителя',
        'package_count' => 'Количество в упаковке, шт',
        'package_width' => 'Ширина упаковка, м',
        'package_height' => 'Высота упаковки, м',
        'package_length' => 'Длина упаковки, м',
        'package_weight' => 'Вес упаковки, кг',
        'barcode' => 'Артикул поставщика',
        'shop_url' => 'Ссылка на предложение на сайте поставщика'
    ];

    function setupModels()
    {
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);

        $modelName = "ProductCategoryShopSetting";
        $this->ProductCategoryShopSetting = ClassRegistry::init($modelName);

        $modelName = "ProductCategory";
        $this->ProductCategory = ClassRegistry::init($modelName);

        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);

        $modelName = "Shop_Import";
        $this->Shop_Import = ClassRegistry::init($modelName);

        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);

        $modelName = "Product_Image";
        $this->Product_Image = ClassRegistry::init($modelName);

        $modelName = "Product_Param";
        $this->Product_Param = ClassRegistry::init($modelName);
    }

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
        $this->setupModels();
    }

    const element_not_found = "в импортируемом файле отсутствует элемент ";

    const undefined_import_type_error = "Не указан механизм импортирования товара, возможные значения -> {'new', 'exists}";
    const undefined_import_field_model = "Не указано поле модель для товара";
    const undefined_import_field_brand = "Не указано поле бренд для товара";
    const company_not_set = "Ошибка. Что-то пошло не так, не установлен идентификатор поставщика";
    const not_found_active_shops = "Ошибка. У вас нет активных магазинов для импортирования товаров";

    public $debug_mode = false;

    public $importErrors = array(
        array('empty_import_file' => 'Отсутствует импортируемый файл'),
        array('no_catalog_element' => self::element_not_found . ' catalog'),
        array('no_shop_element' => self::element_not_found . ' shop'),
    );

    public function getProductOfferFields()
    {
        return $this->optional_product_offer_fields;
    }

    public function getOfferFieldsNames()
    {
        return $this->optional_product_offer_field_names;
    }

    // построение составного названия для товара
    public function buildProductNameByCategories($category_id, $render = "text")
    {
        if ((!isset($category_id)) OR $category_id <= 0) {
            die("ERROR: given false category_id");
        }
        $buildedCatName = "";
        $c = 0;
        do {
            $c++;
            if ($category_id == 0) {
                return array('name' => $buildedCatName, 'level' => 0);
                break;
            }
            $getCatSettings = $this->ProductCategoryShopSetting->find("first",
                array('conditions' =>
                    array(
                        'category_id' => $category_id
                    )
                )
            );

            $getCat = $this->ProductCategory->find("first",
                array('conditions' =>
                    array(
                        'id' => $category_id
                    )
                )
            );

            $parent_id = $getCat['ProductCategory']['parent_id'];
            $cat_name = mb_ucfirst($getCat['ProductCategory']['name']);
            if ($render == "text") {
                $buildedCatName = $cat_name . " / " . mb_strtolower($buildedCatName);
            } else {
                $buildedCatName = '<a class="btn-link" href="/category/' . $getCat['ProductCategory']['id'] . '">' . $cat_name . "</a> / " . mb_strtolower($buildedCatName);
            }

            if (count($getCatSettings) > 0 && $getCatSettings['ProductCategoryShopSetting']['use_build_name'] == 'true') {
                //return $buildedCatName;
                return array(
                    'name' => $buildedCatName,
                    'level' => $c
                );
                break;
            }


            $category_id = $parent_id;
        } while (true);
    }

    // построение составного названия для товара
    public function systemBarcode($product_id, $category_id = null)
    {
        if ($category_id == null) {
            $modelName = "ProductCategory";
            $this->ProductCategory = ClassRegistry::init($modelName);
            $getCat = $this->ProductCategory->find("first",
                array('conditions' =>
                    array(
                        'id' => $category_id
                    )
                )
            );

            $category_id = $getCat['ProductCategory']['id'];
        }
        $cat_zero_counter = strlen($category_id);
        $product_zero_counter = strlen($product_id);
        $category_barcode = str_pad("", 5 - $cat_zero_counter, "0") . $category_id;
        $product_barcode = str_pad("", 8 - $product_zero_counter, "0") . $product_id;
        return "T-" . $category_barcode . "-" . $product_barcode;
    }

    // получить по выбранной категории товаров привязанные характеристики товаров
    public function getProductCategoryParams($category_id)
    {
        $modelName = "ProductCategoryParam";
        $this->ProductCategoryParam = ClassRegistry::init($modelName);
        $params = $this->ProductCategoryParam->find("all",
            array('conditions' =>
                array(
                    'category_id' => $category_id
                )
            )
        );
        pr($params);
        exit;
    }

    // получить по выбранной категории товаров привязанные характеристики товаров
    public function getCategoryById($category_id)
    {
        $modelName = "ProductCategory";
        $this->ProductCategory = ClassRegistry::init($modelName);
        $ProductCategory = $this->ProductCategory->find("first",
            array('conditions' =>
                array(
                    'id' => $category_id
                )
            )
        );
        if(count($ProductCategory)>0){
            return $ProductCategory['ProductCategory']['name'];
        }
        return null;

    }

    // поиск категории по названию
    public function findCategoryByName($category_name)
    {
        $modelName = "ProductCategory";
        $this->ProductCategory = ClassRegistry::init($modelName);

        $getExactlyCat = $this->ProductCategory->find("first",
            array('conditions' =>
                array(
                    'name' => $category_name
                )
            )
        );
        if (count($getExactlyCat) > 0) {
            return $getExactlyCat['ProductCategory']['id'];
        }
        $getCat = $this->ProductCategory->find("first",
            array('conditions' =>
                array(
                    'name LIKE ' => "'%" . $category_name . "%'"
                )
            )
        );

        if (count($getCat) > 0) {
            $category_id = $getCat['ProductCategory']['id'];
            return $category_id;
        }
        return 0;
    }

    // поиск категории по названию
    public function addCategory($category_name, $parent_id = null, $import_id, $status = "moderate")
    {
        $modelName = "ProductCategory";
        $this->ProductCategory = ClassRegistry::init($modelName);
        $new_cat = array(
            'name' => $category_name,
            'parent_id' => $parent_id,
            'status' => $status
        );
        $this->ProductCategory->create();
        $this->ProductCategory->save($new_cat);
        $product_category_id = $this->ProductCategory->id;
        $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "category", $product_category_id);
        return $product_category_id;
    }

    private function prepareDataForRestoring($data, $row_id)
    {
        $saving_data = serialize($data);
        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);
        $this->Shop_Import_Item->id = $row_id;
        $this->Shop_Import_Item->save(array('restore_data' => $saving_data));
    }

    private function restoringData($modelName, $restore_data, $row_id)
    {
        $this->$modelName = ClassRegistry::init($modelName);
        $this->$modelName->create();
        $data_for_save = $restore_data;
        $this->$modelName->save($data_for_save);
        $new_value_id = $this->$modelName->id;

        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);
        $this->Shop_Import_Item->id = $row_id;
        $this->Shop_Import_Item->save(array('value_id' => $new_value_id));
    }


    // IMPORT

    // TODO сделать проверку привязки импорта и поставщика
    public function rollbackImport($import_id)
    {
        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);
        $import_list = $this->Shop_Import_Item->find("all",
            array('conditions' =>
                array(
                    'import_id' => $import_id
                )
            )
        );
        if (count($import_list) === 0) return;
        $product_rollback_count = $offer_rollback_count = $image_rollback_count =
        $category_rollback_count = $brand_rollback_count = $param_rollback_count =
        $param_value_rollback_count = 0;
        foreach ($import_list as $list_item) {
            $list_item = $list_item['Shop_Import_Item'];
            $value_type = $list_item['value_type'];
            $import_element_id = $list_item['id'];
            $value_id = $list_item['value_id'];
            if ($value_type == "product") {
                $product_rollback_count++;
                $modelName = "Product";
                $this->Product = ClassRegistry::init($modelName);
                $found_product = $this->Product->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_product) > 0) {
                    // save data for restoring
                    $this->prepareDataForRestoring($found_product, $import_element_id);
                    $this->Product->id = $value_id;
                    $this->Product->delete();
                }
            } else if ($value_type == "offer") {
                $offer_rollback_count++;
                $modelName = "Shop_Product";
                $this->Shop_Product = ClassRegistry::init($modelName);
                $found_offer = $this->Shop_Product->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_offer) > 0) {
                    // save data for restoring
                    $this->prepareDataForRestoring($found_offer, $import_element_id);
                    $this->Shop_Product->id = $value_id;
                    $this->Shop_Product->delete();
                }
            } else if ($value_type == "image") {
                // с картинками отсутствует возможность отката - слишком много неоднозначностей
                /*
                $image_rollback_count++;
                $modelName = "Product_Image";
                $this->Product_Image = ClassRegistry::init($modelName);
                $found_image = $this->Product_Image->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_image) > 0) {
                    // save data for restoring
                    $this->prepareDataForRestoring($found_image, $import_element_id);
                    $this->Product_Image->image_id = $value_id;
                    $this->Product_Image->delete();
                }*/
            } else if ($value_type == "brand") {
                $brand_rollback_count++;
                $modelName = "Brand";
                $this->Brand = ClassRegistry::init($modelName);
                $found_brand = $this->Brand->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_brand) > 0) {
                    // save data for restoring
                    $this->prepareDataForRestoring($found_brand, $import_element_id);
                    $this->Brand->id = $value_id;
                    $this->Brand->delete();
                }
            } else if ($value_type == "category") {
                $category_rollback_count++;
                $modelName = "ProductCategory";
                $this->ProductCategory = ClassRegistry::init($modelName);
                $found_cat = $this->ProductCategory->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_cat) > 0) {
                    // save data for restoring
                    $this->prepareDataForRestoring($found_cat, $import_element_id);
                    $this->ProductCategory->id = $value_id;
                    $this->ProductCategory->delete();
                }
            } else if ($value_type == "param") {
                $param_rollback_count++;
                $modelName = "Category_Product_Param";
                $this->Category_Product_Param = ClassRegistry::init($modelName);
                $found_param = $this->Category_Product_Param->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_param) > 0) {
                    // save data for restoring
                    $this->prepareDataForRestoring($found_param, $import_element_id);
                    $this->Category_Product_Param->id = $value_id;
                    $this->Category_Product_Param->delete();
                }
            } else if ($value_type == "param_value") {
                $param_value_rollback_count++;
                $modelName = "Category_Product_Param_Value";
                $this->Category_Product_Param_Value = ClassRegistry::init($modelName);
                $found_param_value = $this->Category_Product_Param_Value->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_param_value) > 0) {
                    // save data for restoring
                    $this->prepareDataForRestoring($found_param_value, $import_element_id);
                    $this->Category_Product_Param_Value->id = $value_id;
                    $this->Category_Product_Param_Value->delete();
                }
            }
        }
        $rollback_import = array(
            'active_status' => 'rollback'
        );
        $modelName = "Shop_Import";
        $this->Shop_Import = ClassRegistry::init($modelName);
        $this->Shop_Import->id = $import_id;
        $this->Shop_Import->save($rollback_import);
        $result_message = "В результате отката были удалены следующие данные: ";
        $result_message .= " Товаров - " . $product_rollback_count . " ";
        $result_message .= " Изображений - " . $image_rollback_count . " ";
        $result_message .= " Брендов - " . $brand_rollback_count . " ";
        $result_message .= " Предложений - " . $offer_rollback_count . " ";
        $result_message .= " Категорий - " . $category_rollback_count . " ";
        $result_message .= " Параметров - " . $param_rollback_count . " ";
        $result_message .= " Значений параметров - " . $param_value_rollback_count . " ";
        return $result_message;
    }

    // TODO сделать проверку привязки импорта и поставщика
    public function acceptImport($import_id)
    {
        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);
        $import_list = $this->Shop_Import_Item->find("all",
            array('conditions' =>
                array(
                    'import_id' => $import_id
                )
            )
        );
        if (count($import_list) === 0) return;
        $product_rollback_count = $offer_rollback_count = $image_rollback_count =
        $category_rollback_count = $brand_rollback_count = $param_rollback_count = $param_value_rollback_count = 0;
        foreach ($import_list as $list_item) {
            $list_item = $list_item['Shop_Import_Item'];
            $value_type = $list_item['value_type'];
            $row_id = $list_item['id'];

            $restore_data = $list_item['restore_data'];
            if (!empty($restore_data)) {
                $restore_data = unserialize($restore_data);
            }
            if ($value_type == "product") {
                $product_rollback_count++;
                $modelName = "Product";
                $this->restoringData($modelName, $restore_data, $row_id);
            } else if ($value_type == "offer") {
                $offer_rollback_count++;
                $modelName = "Shop_Product";
                $this->restoringData($modelName, $restore_data, $row_id);
            } else if ($value_type == "image") {
                //с картинками отсутствует возможность отката - слишком много неоднозначностей

                /*
                $image_rollback_count++;
                $modelName = "Product_Image";
                $this->restoringData($modelName, $restore_data, $row_id);
                */
            } else if ($value_type == "brand") {
                $brand_rollback_count++;
                $modelName = "Brand";
                $this->restoringData($modelName, $restore_data, $row_id);
            } else if ($value_type == "category") {
                $category_rollback_count++;
                $modelName = "ProductCategory";
                $this->restoringData($modelName, $restore_data, $row_id);
            } else if ($value_type == "param") {
                $param_rollback_count++;
                $modelName = "Category_Product_Param";
                $this->restoringData($modelName, $restore_data, $row_id);
            } else if ($value_type == "param_value") {
                $param_value_rollback_count++;
                $modelName = "Category_Product_Param_Value";
                $this->restoringData($modelName, $restore_data, $row_id);
            }
        }
        $rollback_import = array(
            'active_status' => 'import'
        );
        $modelName = "Shop_Import";
        $this->Shop_Import = ClassRegistry::init($modelName);
        $this->Shop_Import->id = $import_id;
        $this->Shop_Import->save($rollback_import);
        $result_message = "В результате принятия импорта были добавлены следующие данные: ";
        $result_message .= " Товаров - " . $product_rollback_count . " ";
        $result_message .= " Изображений - " . $image_rollback_count . " ";
        $result_message .= " Брендов - " . $brand_rollback_count . " ";
        $result_message .= " Предложений - " . $offer_rollback_count . " ";
        $result_message .= " Категорий - " . $category_rollback_count . " ";
        $result_message .= " Параметров - " . $param_rollback_count . " ";
        $result_message .= " Значений параметров - " . $param_value_rollback_count . " ";
        return $result_message;
    }

    private function removeImportById($id)
    {
        $modelName = "Shop_Import";
        $this->Shop_Import = ClassRegistry::init($modelName);
        $this->Shop_Import->id = $id;
        $this->Shop_Import->delete();
    }

    // TODO сделать проверку привязки импорта и поставщика
    public function removeImport($import_id)
    {
        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);
        $import_list = $this->Shop_Import_Item->find("all",
            array('conditions' =>
                array(
                    'import_id' => $import_id
                )
            )
        );
        if (count($import_list) == 0) {
            $this->removeImportById($import_id);
            return true;
        }//return false;
        $product_rollback_count = $offer_rollback_count = $image_rollback_count =
        $category_rollback_count = $brand_rollback_count = 0;
        foreach ($import_list as $list_item) {

            $list_item = $list_item['Shop_Import_Item'];
            $import_item_id = $list_item['id'];
            $value_type = $list_item['value_type'];
            $value_id = $list_item['value_id'];
            if ($value_type == "product") {
                $product_rollback_count++;
                $modelName = "Product";
                $this->Product = ClassRegistry::init($modelName);
                $found_product = $this->Product->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_product) > 0) {
                    $this->Product->id = $value_id;
                    $this->Product->delete();
                }
            } else if ($value_type == "offer") {
                $offer_rollback_count++;
                $modelName = "Shop_Product";
                $this->Shop_Product = ClassRegistry::init($modelName);
                $found_offer = $this->Shop_Product->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_offer) > 0) {
                    $this->Shop_Product->id = $value_id;
                    $this->Shop_Product->delete();
                }
            } else if ($value_type == "image") {
                $image_rollback_count++;
                $modelName = "Product_Image";
                $this->Product_Image = ClassRegistry::init($modelName);
                $found_images = $this->Product_Image->find("all",
                    array('conditions' =>
                        array(
                            'image_id' => $value_id
                        )
                    )
                );
                if (count($found_images) > 0) {
                    foreach ($found_images as $found_image) {
                        $i_id = $found_image['Product_Image']['id'];
                        $p_id = $found_image['Product_Image']['product_id'];
                        $file_link = $found_image['Product_Image']['file'];
                        $file_link_complete = Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR_RELATIVE') . DS . "p" . $p_id . DS . $file_link;
                        if (file_exists($file_link_complete)) {
                            unlink($file_link_complete);
                        }
                        $this->Product_Image->id = $i_id;
                        $this->Product_Image->delete();
                    }

                }
            } else if ($value_type == "brand") {
                $brand_rollback_count++;
                $modelName = "Brand";
                $this->Brand = ClassRegistry::init($modelName);
                $found_brand = $this->Brand->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_brand) > 0) {
                    $this->Brand->id = $value_id;
                    $this->Brand->delete();
                }
            } else if ($value_type == "category") {
                $category_rollback_count++;
                $modelName = "ProductCategory";
                $this->ProductCategory = ClassRegistry::init($modelName);
                $found_cat = $this->ProductCategory->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_cat) > 0) {
                    $this->ProductCategory->id = $value_id;
                    $this->ProductCategory->delete();
                }
            } else if ($value_type == "param") {
                $category_rollback_count++;
                $modelName = "Category_Product_Param";
                $this->Category_Product_Param = ClassRegistry::init($modelName);
                $found_param = $this->Category_Product_Param->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_param) > 0) {
                    $this->Category_Product_Param->id = $value_id;
                    $this->Category_Product_Param->delete();
                }
            } else if ($value_type == "param_value") {
                $category_rollback_count++;
                $modelName = "Category_Product_Param_Value";
                $this->Category_Product_Param_Value = ClassRegistry::init($modelName);
                $found_param = $this->Category_Product_Param_Value->find("first",
                    array('conditions' =>
                        array(
                            'id' => $value_id
                        )
                    )
                );
                if (count($found_param) > 0) {
                    $this->Category_Product_Param_Value->id = $value_id;
                    $this->Category_Product_Param_Value->delete();
                }
            }
            $modelName = "Shop_Import_Item";
            $this->Shop_Import_Item = ClassRegistry::init($modelName);
            $this->Shop_Import_Item->id = $import_item_id;
            $this->Shop_Import_Item->delete();
        }
        $this->removeImportById($import_id);
        $result_message = "Импорт полностью удален из системы.";
        return $result_message;
    }

    public function importXML($xml, $source_file, $shop_id = 0)
    {
        // проверка наличия хедера xml для осуществления корректного парсинга файла
        $xml_header = '<?xml version="1.0" encoding="UTF-8"?>';
        if (substr_count($xml, $xml_header) == 0) {
            $xml = $xml_header . $xml;
        }
        // список магазинов, куда производится импорт
        /* ИМПОРТ ПРОИЗВОДИТСЯ БЕЗ УКАЗАНИЯ МАГАЗИНА
        $shop_array = [];
        if ($shop_id == 0) {
            $shop_lists = $this->CompanyCom->shop_list_full();
            foreach ($shop_lists as $shop_list_item) {
                $shop_status = $shop_list_item['Shop']['status'];
                if ($shop_status == "active") {
                    $shop_array[] = $shop_list_item['Shop']['id'];
                }
            }
        } else {
            $shop_array[] = $shop_id;
        }
        if (count($shop_array) == 0) {
            $error_message = self::not_found_active_shops;
            response_ajax($error_message, "error");
            exit;
        }
        */
        /*$xmlString = '<?xml version="1.0"?><root><child>value</child></root>'; */
        $xmlObject = Xml::build($xml);
        $xmlArray = Xml::toArray($xmlObject);

        $result_status = "";

        if (!key_exists('yml_catalog', $xmlArray)) {
            $error_message = $this->importErrors['no_catalog_element'];
            response_ajax($error_message, "error");
            exit;
        }

        //все готово для импорта, начинается импорт, создается запись об импорте
        $company_id = $this->CompanyCom->company_id();
        $this->Shop_Import->create();
        $new_import = array(
            'company_id' => $company_id,
            'file' => $source_file,
            'offer_count' => 0,
            'status' => "init",
            'active_status' => "init"
        );
        $this->Shop_Import->save($new_import);
        $import_id = $this->Shop_Import->id;

        /* обход массива магазинов и их добавление */
        /*
         * Этот код скорее всего не нужен и с высокой долей вероятности будет удален, так как добавление магазинов производится в ЛКП в ручном режиме,
         * но на всякий случай пока пусть будет
         * */
        if (key_exists('shop', $xmlArray['yml_catalog'])/* AND key_exists('shop', $xmlArray['yml_catalog']['shops'])*/) {
            foreach ($xmlArray['yml_catalog']['shop'] as $xmlArrayShop) {
                $shop_name = $xmlArrayShop['name'] ?? null;
                $address = $xmlArrayShop['address'] ?? "";
                $city = $xmlArrayShop['city'] ?? null;
                $region = $xmlArrayShop['region'] ?? null;
                $phone = $xmlArrayShop['phone'] ?? null;
                $shop_open = $xmlArrayShop['shop_open'] ?? null;
                $shop_close = $xmlArrayShop['shop_close'] ?? null;
                if ($shop_name !== null AND
                    $address !== null AND
                    $city !== null AND
                    $region !== null
                )
                    $this->CompanyCom->import_shop($shop_name, $city, $region, $address, $phone, $shop_open, $shop_close);
            }
        }


        // обход категорий
        $import_categories = [];
        if (key_exists('categories', $xmlArray['yml_catalog']['shop'])) {
            foreach ($xmlArray['yml_catalog']['shop']['categories']['category'] as $xmlArrayCategory) {
                if (!in_array($xmlArrayCategory['@id'], $import_categories)) {
                    $cat_id = $xmlArrayCategory['@id'];
                    $parent_id = $xmlArrayCategory['@parentId'] ?? 0;
                    $cur_name = $xmlArrayCategory['@'];
                    $import_categories[$cat_id] = array(
                        'id' => $cat_id,
                        'name' => prepare_import_field($cur_name),
                        'parent_id' => $parent_id
                    );
                }
            }
        }

        /*
         * МЕХАНИЗМ ИМПОРТА КАТЕГОРИЙ
         * 1. Поиск самых верхних категорий, у которых parent_id = 0
         * Эти категории будут проверятся на наличие в базе, и при наличии совпадения по ним, импорт будет вестись в них
         * Если данной категории нет в базе, соответственно, она будет добавляться в корень, а все остальные подкатегории будут добавляться как ее потомки
         *
         * */

        /* присланные категории */
        foreach ($import_categories as &$item) {
            $parent_id = $item['parent_id'];
            $category_id = $this->findCategoryByName($item['name']);
            if ($category_id > 0) {
                $childrens = $this->getChildrens($import_categories, $item['id']);
                // нашли корневую категорию в БД
                foreach ($childrens as $children) {
                    // проверка, существует ли потомок в БД
                    $children_category_id = $this->findCategoryByName($import_categories[$children]['name']);
                    if ($children_category_id == 0) {//} OR $children_category_id!=$this->getParentCategory($children_category_id)){
                        // добавляемую подкатегорию не нашли - требуется ее добавить
                        //$import_categories[$children['real_category_id']] = $this->addCategory($import_categories[$children['name']], $category_id, "moderate");
                        $real_category_id = $this->addCategory($import_categories[$children]['name'], $category_id, $import_id, "moderate");
                        $this->recursive_insert_categories($import_categories, $children, $real_category_id, $import_id, "moderate");
                    } else {
                        $import_categories[$children]["real_category_id"] = $children_category_id;
                    }
                }
            } else {
                // если не нашли подкатегорию в базе
                // надо понять есть ли родительская категория в базе или нет
                $real_parent_category_id = null;
                if (key_exists($parent_id, $import_categories) AND key_exists("real_category_id", $import_categories[$parent_id])) {
                    $real_parent_category_id = $import_categories[$parent_id]['real_category_id'];
                }
                if ($real_parent_category_id != null) {
                    $real_parent_id = $real_parent_category_id;
                } else {
                    $real_parent_id = 0;
                }
                if ($this->findCategoryByName($item['name']) == 0) {
                    $item['real_category_id'] = $this->addCategory($item['name'], $real_parent_id, $import_id, "moderate");
                    $this->recursive_insert_categories($import_categories, $item['id'], $item['real_category_id'], $import_id, "moderate");
                }
            }
        }

        $import_errors = 0;
        //импорт торговых предложений
        if (key_exists('offers', $xmlArray['yml_catalog']['shop'])) {
            foreach ($xmlArray['yml_catalog']['shop']['offers']['offer'] as $offer) {
                //pr($offer);
                // Массогабаритные показатели товара
                $product_weight_size = [];
                if (@count($offer['param']) > 0) {
                    $product_weight_size = $this->paramWeightAndSize($offer['param']);
                }
                if (!empty($offer['weight'])) {
                    $product_weight_size['weight'] = $offer['weight'];
                }
                //pr($product_weight_size);

                $identified_product = false;
                /*способы сравнения:
                1. Название + Вендор + Артикул вендора*/
                // попытка идентификации товара

                if (key_exists("name", $offer)) {
                    $brand_name = prepare_import_field($offer["name"]);
                } else {
                    $brand_name = "";
                }

                if (key_exists("vendor", $offer)) {
                    $vendor_name = prepare_import_field($offer["vendor"]);
                } else {
                    $vendor_name = "";
                }

                if (key_exists("vendorCode", $offer)) {
                    $vendor_code = prepare_import_field($offer["vendorCode"]);
                } else {
                    $vendor_code = "";
                }

                if (key_exists("typePrefix", $offer)) {
                    $type_prefix = prepare_import_field($offer["typePrefix"]);
                } else {
                    $type_prefix = "";
                }

                if (key_exists("model", $offer)) {
                    $product_model = prepare_import_field($offer["model"]);
                } else {
                    $product_model = "";
                }

                if (key_exists("country_of_origin", $offer)) {
                    $country_of_origin = prepare_import_field($offer["country_of_origin"]);
                } else {
                    $country_of_origin = 0;
                }

                $description = prepare_import_field($offer['description']) ?? null;

                // проверка по венору и модели
                $check_product_by_brand_prefix_model = 0;
                $check_product_by_name_brand_barcode = 0;
                if (!empty($type_prefix) AND !empty($product_model)) {
                    $check_product_by_brand_prefix_model = $this->findProductByBrandPrefixModel(
                        $vendor_name,
                        $type_prefix,
                        $product_model
                    );
                }
                $product_id = null;
                // проверка по венору и модели
                if ($check_product_by_brand_prefix_model > 0) {
                    $product_id = $check_product_by_brand_prefix_model;
                    $identified_product = true;
                } else {
                    if (!empty($vendor_code) AND !empty($vendor_name)) {
                        $check_product_by_name_brand_barcode = $this->findProductByBrandAndBarcode(
                            $brand_name,
                            $vendor_code
                        );

                        if ($check_product_by_name_brand_barcode > 0) {
                            $identified_product = true;
                            $product_id = $check_product_by_brand_prefix_model;
                        }
                    }
                }

                if (!$identified_product && $product_id == null) {

                    //чистка полей для импортирования
                    if (empty($brand_name)) {
                        $product_name = $vendor_name . " " . $product_model;
                        if (!empty($type_prefix)) {
                            $product_name = $type_prefix . " " . $product_name;
                        }
                    } else {
                        $product_name = $brand_name;
                    }

                    // проверка, существует ли вендор, если нет - добавляем его
                    $brand_id = $this->Manufacturer->checkBrandExists($vendor_name);
                    if ($brand_id == 0) {
                        $brand_id = $this->Manufacturer->saveBrand($vendor_name);
                    }

                    // определение id страны
                    $country_id = $this->Country->getCountryIdByName($country_of_origin);
                    if ($country_id == 0) {
                        //$brand_id = $this->Country->saveBrand($vendor_name);
                    }

                    // определение категории
                    $cat_id = $import_categories[$offer['categoryId']]['real_category_id'];
                    if ($cat_id == 0) {
                        pr($import_categories);
                        echo $offer['categoryId'];
                        echo "cat_id = " . $cat_id;
                        exit;
                    }

                    $new_product = [
                        'description' => $description,
                        'product_name' => $product_name,
                        'weight' => $product_weight_size['weight'] ?? null,
                        'width' => $product_weight_size['width'] ?? null,
                        'height' => $product_weight_size['height'] ?? null,
                        'length' => $product_weight_size['length'] ?? null,
                        'volume' => $product_weight_size['volume'] ?? null,
                        'product_model' => $product_model,
                        'prefix' => $type_prefix,
                        'status' => 'hidden',
                        'brand_id' => $brand_id,
                        'brand_barcode' => $vendor_code,
                        'category_id' => $cat_id,
                        'country_id' => $country_id
                    ];

                    $additional_data['vendor'] = $vendor_name;
                    $additional_data['country_name'] = $country_of_origin;

                    $product_id = $this->import_company_product($import_id, $new_product, null, $additional_data);
                    $system_barcode = $this->systemBarcode($product_id, $cat_id);
                    $this->Product->id = $product_id;
                    $this->Product->save(
                        array('system_barcode' => $system_barcode)
                    );

                    //добавление записи в Shop Import Items
                    $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "product", $product_id);

                    //загрузка изображений по ссылке, если требуется
                    if (!empty($offer['picture']) AND filter_var($offer['picture'], FILTER_VALIDATE_URL) != FALSE) {
                        // проверка, не загружалось ли это изображение ранее
                        if (!$this->find_image_by_hash($offer['picture'], $product_id)) {
                            $image_id = $this->load_image_by_url($offer['picture'], $system_barcode, $product_id, $import_id);
                            $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "image", $image_id);
                        }
                    }

                } else {
                    echo "товар найден " . $product_id;
                    //чистка полей для импортирования
                    if (empty($brand_name)) {
                        $product_name = $vendor_name . " " . $product_model;
                        if (!empty($type_prefix)) {
                            $product_name = $type_prefix . " " . $product_name;
                        }
                    } else {
                        $product_name = $brand_name;
                    }

                    // проверка, существует ли вендор, если нет - добавляем его
                    $brand_id = $this->Manufacturer->checkBrandExists($vendor_name);
                    if ($brand_id == 0) {
                        $brand_id = $this->Manufacturer->saveBrand($vendor_name);
                        $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "brand", $brand_id);
                    }

                    $product = null;
                    $category_id = $this->getProductField("category_id", $product_id, $product);

                    $country_id = $this->Country->getCountryIdByName($country_of_origin);

                    $new_product = [
                        'description' => $description,
                        'product_name' => $product_name,
                        'weight' => $product_weight_size['weight'] ?? null,
                        'width' => $product_weight_size['width'] ?? null,
                        'height' => $product_weight_size['height'] ?? null,
                        'length' => $product_weight_size['length'] ?? null,
                        'volume' => $product_weight_size['volume'] ?? null,
                        'prefix' => $type_prefix,
                        'product_model' => $product_model,
                        'status' => 'hidden',
                        'brand_id' => $brand_id,
                        'brand_barcode' => $vendor_code,
                        'category_id' => $category_id,
                        'country_id' => $country_id
                    ];

                    $additional_data['vendor'] = $vendor_name;
                    $additional_data['country_name'] = $country_of_origin;

                    $this->import_company_product($import_id, $new_product, $product_id, $additional_data);
                    $system_barcode = $this->systemBarcode($product_id, $category_id);

                    //загрузка изображений по ссылке, если требуется
                    if (!empty($offer['picture']) AND filter_var($offer['picture'], FILTER_VALIDATE_URL) != FALSE) {
                        // проверка, не загружалось ли это изображение ранее
                        if (!$this->find_image_by_hash($offer['picture'], $product_id)) {
                            $image_id = $this->load_image_by_url($offer['picture'], $system_barcode, $product_id, $import_id);
                            $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "image", $image_id);
                        }
                    }

                }

                // Добавление новых параметров товара
                if (key_exists("param", $offer)) {
                    foreach ($offer["param"] as $offer_param) {
                        $name = $offer_param['@name'];
                        $unit = $offer_param['@unit'] ?? null;
                        $value = $offer_param['@'] ?? null;

                        $unit = trim(str_replace(".", "", $unit));
                        $unit = str_replace(",", "", $unit);

                        $name = trim(str_replace(".", "", $name));
                        $name = mb_ucfirst(mb_strtolower($name));
                        // остальные параметры, которые добавляются в отдельную таблицу
                        $filter_id = $this->find_filter_by_name($name, $category_id);
                        if ($filter_id == 0) {
                            // добавление нового параметра товара и отправка записи на регистрацию импорта
                            $filter_id = $this->add_new_param_by_import($name, $category_id, $unit);
                            //$filter_name = "Фильтр #" . $filter_id . " $name";
                            $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "param", $filter_id);
                        }
                        $filter_value_exists = $this->check_filter_value($filter_id, $value);
                        if (!$filter_value_exists) {
                            $value_id = $this->add_filter_value($filter_id, $value, $product_id);
                            //$value_name = "Фильтр #" . $filter_id . " ( $name ) Значение #" . $value_id . " " . $value;
                            $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "param_value", $value_id);
                        } else {
                            $this->add_product_param_value_force($filter_id, $value, $product_id);
                        }

                    }
                }

                // TODO HARDCODE Region_id = 1 SPB

                $base_price = intval($offer['price']);
                $offer = [
                    'region_id' => 1,
                    'base_price' => $base_price,
                    'oldprice' => intval($offer['oldprice']),
                    'shop_url' => $offer['url'],
                    'preorder_only' => $offer['store'],
                    'delivery' => $offer['delivery'],
                    'pickup' => $offer['pickup'],
                    'delivery_cost' => $offer['delivery-options']['option']['@cost'],
                    'delivery_days' => $offer['delivery-options']['option']['@days'],
                    'delivery_order_before' => $offer['delivery-options']['option']['@order-before'],
                    'sales_notes' => $offer['sales_notes'],
                    'barcode' => $offer['barcode'],
                    'product_id' => $product_id,
                    'actual' => "false",
                    'company_id' => $this->CompanyCom->company_id(),
                ];
                // добавляем товарное предложение в каждый магазин
                $this->Shop_Product->create();
                $this->Shop_Product->save($offer);
                $offer_id = $this->Shop_Product->id;
                $this->shop_import_register($this->CompanyCom->company_id(), $import_id, "offer", $offer_id);

                // TODO - непонятно, требуется ли отправлять предложение на модерацию при изменении цены в предложении
                /*
                if ($base_price !== 0 OR $base_price < 0) {
                    $this->add_moderation_task($this->current_moderation_id($product_id), "base_price", "", $base_price, "", $base_price);
                }
                */
            }
            if ($import_errors == 0) {
                $this->import_error_register($import_id, null, "success", $message = "");
            }
        }
        return true;
    }

    /**Выбирает массогабаритные параметры товара
     * @param $arr - массив с параметрами для товара
     * @return array - массогабаритные показатели
     */
    private function paramWeightAndSize($arr): array
    {
        $return_res = [
            'weight' => null,
            'length' => null,
            'width' => null,
            'height' => null,
            'volume' => null,
        ];
        $modelName = "Shop_Import_Params";
        $this->ShopImportParams = ClassRegistry::init($modelName);
        $p = $this->ShopImportParams->find("all");
        foreach ($p as $v) {
            $params_all[$v['Shop_Import_Params']['name']] = [
                'id' => $v['Shop_Import_Params']['id'],
                'type' => $v['Shop_Import_Params']['type'],
            ];
        }
        foreach ($arr as $v) {
            if (array_key_exists($v['@name'], $params_all)) {
                switch ($params_all[$v['@name']]['type']) {
                    case 'weight':
                        $return_res['weight'] = $v['@'];
                        break;
                    case 'length':
                        $return_res['length'] = $v['@'];
                        break;
                    case 'width':
                        $return_res['width'] = $v['@'];
                        break;
                    case 'height':
                        $return_res['height'] = $v['@'];
                        break;
                    case 'volume':
                        $return_res['volume'] = $v['@'];
                        break;
                }
            } else {
                $this->ShopImportParams->create();
                $this->ShopImportParams->save(['name' => $v['@name']]);
            }
        }
        return $return_res;
    }

    public function getBrandById($brand_id)
    {
        $modelName = "Brand";
        $this->Brand = ClassRegistry::init($modelName);

        $getBrand = $this->Brand->find("first",
            array('conditions' =>
                array(
                    'id' => $brand_id
                )
            )
        );
        if (count($getBrand) > 0) {
            return $getBrand;
        }
        return null;
    }

    public function getBrandIdByName($brand_name)
    {
        $modelName = "Brand";
        $this->Brand = ClassRegistry::init($modelName);

        $getBrand = $this->Brand->find("first",
            array('conditions' =>
                array(
                    'name' => $brand_name
                )
            )
        );
        if (count($getBrand) > 0) {
            return $getBrand['Brand']['id'];
        }
        return 0;
    }

    public function findProductByBrandPrefixModel($brand, $prefix, $model)
    {
        $search_params = $prefix . " " . $brand . " " . $model;
        $debug_info = "поиск товара по связке Модель, Вендор, Префикс/группа товаров " . $search_params;

        $brand_id = $this->getBrandIdByName($brand);
        if (!$brand_id) {
            $brand_array = array('brand_id' => $brand_id);
        } else {
            $brand_array = array();
        }
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);


        $findProduct = $this->Product->find("first",
            array('conditions' =>
                array(
                    $brand_array,
                    'prefix' => $prefix,
                    'product_model' => $model
                )
            )
        );
        if ($this->debug_mode) {
            echo $prefix . " " . $model;
            pr($brand_array);
            echo $debug_info;
            echo serialize($findProduct);
        }
        if (count($findProduct) > 0) {
            return $findProduct['Product']['id'];
        }

        return null;
    }

    public function findProductByBrandAndBarcode($brand_name, $brand_barcode)
    {
        $brand_id = $this->getBrandIdByName($brand_name);
        if (!$brand_id) {
            $brand_array = array('brand_id' => $brand_id);
        } else {
            $brand_array = array();
        }
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);
        $findProduct = $this->Product->find("first",
            array('conditions' =>
                array(
                    $brand_array,
                    'brand_barcode' => $brand_barcode
                )
            )
        );
        if (count($findProduct) > 0) {
            return $findProduct['Product']['id'];
        }
        return null;
    }

    public function findProductByName($name)
    {
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);
        $findProduct = $this->Product->find("first",
            array('conditions' =>
                array(
                    'product_name' => $name
                )
            )
        );
        if (count($findProduct) > 0) {
            return $findProduct['Product']['id'];
        }
        return null;
    }

    private function shop_import_register($company_id, $import_id, $value_type, $value_id)
    {
        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);
        $new_item = array(
            'company_id' => $company_id,
            'import_id' => $import_id,
            'value_type' => $value_type,
            'value_id' => $value_id,
        );
        $this->Shop_Import_Item->create();
        $this->Shop_Import_Item->save($new_item);
        return;
    }

    public function getParentCategory($category_id)
    {
        $modelName = "ProductCategory";
        $this->ProductCategory = ClassRegistry::init($modelName);
        $cat = $this->ProductCategory->find("first",
            array('conditions' =>
                array(
                    'id' => $category_id
                )
            )
        );
        if (count($cat) > 0) {
            return $cat['ProductCategory']['parent_id'];
        }
        return 0;
    }

    public function setParentCategory($category_id, $parent_id)
    {
        $save_data = array(
            'parent_id' => $parent_id,
        );
        $this->ProductCategory->id = $category_id;
        $this->ProductCategory->save($save_data);
    }

    private function getChildrens($array, $category_id, $parent_id = "parent_id")
    {
        $cat_ids = [];
        foreach ($array as $key => $value) {
            if ($value[$parent_id] == $category_id) {
                $cat_ids[] = $key;
            }
        }
        return $cat_ids;
    }

    public function recursive_insert_categories(&$import_categories, $item, $real_category_id, $import_id, $status = "moderate")
    {
        $childrens = $this->getChildrens($import_categories, $item);
        if (count($childrens) === 0) {
            return;
        }
        foreach ($childrens as &$children) {
            if ($this->findCategoryByName($import_categories[$children]['name']) == 0) {
                $children_real_category_id = $this->addCategory($import_categories[$children]['name'], $real_category_id, $import_id, $status);
                $import_categories[$children]['real_category_id'] = $children_real_category_id;
                $this->recursive_insert_categories($import_categories, $children, $children_real_category_id, $import_id, $status);
            }
        }
    }

    public function getImportData($import_id, $data_type)
    {
        $valid_data_types = ["image", "product", "category", "brand", "param", "offer"];
        if (!in_array($data_type, $valid_data_types)) {
            $this->Log->add("Не определенный тип данных в списке импортов - " . $data_type, "warning");
            return false;
        }
        if (intval($import_id) <= 0) {
            die("wrong shop import identifier");
        }

        $modelName = "Shop_Import_Item";
        $this->Shop_Import_Item = ClassRegistry::init($modelName);
        $result = $this->Shop_Import_Item->find("count",
            array('conditions' =>
                array(
                    'import_id' => $import_id,
                    'value_type' => $data_type
                )
            )
        );
        return $result;

    }

    public function find_image_by_hash($url, $product_id)
    {
        $md5_file = md5_file($url);
        $modelName = "Product_Image";
        $this->Product_Image = ClassRegistry::init($modelName);
        $hash_count = $this->Product_Image->find("count",
            array('conditions' =>
                array(
                    'product_id' => $product_id,
                    'hash' => $md5_file
                )
            )
        );
        if ($hash_count == 0) {
            return false;
        }
        return true;
    }

    public function checkImageByHash($hash, $product_id)
    {
        $modelName = "Product_Image";
        $this->Product_Image = ClassRegistry::init($modelName);
        $product_images = $this->Product_Image->find("count",
            array('conditions' =>
                array(
                    'product_id' => $product_id,
                    'hash' => $hash
                )
            )
        );
        return $product_images;
    }

    public function load_image_by_url($url, $system_barcode, $product_id, $import_id)
    {
        $path_to_images = Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR_RELATIVE');
        $product_image_dir = Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR') . DS . 'p' . $product_id;
        if (!is_dir($product_image_dir)) {
            mkdir($product_image_dir, 0777);
        }
        $uploaded_image = $path_to_images . "/" . "p" . $product_id . DS . "/" . basename($url);
        $loaded_image = file_get_contents($url);
        if (!$loaded_image) {
            $this->import_error_register($import_id, "", "warning", "не удалось загрузить изображение по ссылке " . $url);
        }
        $new_file = $product_image_dir . DS . basename($url);
        file_put_contents($new_file, $loaded_image);
        $md5_file = md5_file($new_file);
        if ($this->checkImageByHash($md5_file, $product_id) > 0) {
            return;
        }

        $image_id = $this->product_image_saver(basename($url), $system_barcode, $product_id);
        $this->Moderate->add_moderation_task(
            $this->Moderate->current_moderation_id($product_id), "product_image", "", $uploaded_image, "", $uploaded_image
        );
        return $image_id;
    }

    private function import_error_register($import_id, $new_product, $error = "warning", $message = "")
    {
        if ($error == "error") {
            // остановка импорта
            $status = "error";
            $import_result = array(
                'status' => $status,
                'active_status' => "import",
                'comment' => $message . " " . serialize($new_product),
            );
            $this->Shop_Import->id = $import_id;
            $this->Shop_Import->save($import_result);
            exit;
        } else if ($error == "warning") {
            $status = "warning";
            $import_result = array(
                'status' => $status,
                'comment' => $message . " " . serialize($new_product),
            );
            $this->Shop_Import->id = $import_id;
            $this->Shop_Import->save($import_result);
        } else if ($error == "success") {
            $status = "success";
            $import_result = array(
                'status' => $status,
                'active_status' => "import",
                'comment' => "Все товары были успешно импортированы",
            );
            $this->Shop_Import->id = $import_id;
            $this->Shop_Import->save($import_result);
        }
        return;
    }

    public function import_company_product($import_id, $product, $product_id = null, $additional_data = null)
    {
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);
        //echo
        if ($product_id == null) {
            $this->Product->create();
            $this->Product->save($product);
            $product_id = $this->Product->id;
        }
        $moderation_id = $this->Moderate->current_moderation_id($product_id);
        if (empty($product['product_model'])) {
            $this->import_error_register($import_id, $product, "warning", self::undefined_import_field_model);
        }
        if (empty($product['brand_id'])) {
            $this->import_error_register($import_id, $product, "warning", self::undefined_import_field_brand);
        }
        // если добавляется новый товар - новая карточка
        $moderate_fields_list = [
            'product_model',
            'description',
            'country_id',
            'brand_id',
            'prefix',
            'brand_barcode',
            'manufacturer_id',
        ];
        if ($product_id == null) {
            foreach ($moderate_fields_list as $moderate_field_item) {
                if (!empty($product[$moderate_field_item])) {
                    $update_product_fields[$moderate_field_item] = $product[$moderate_field_item];
                    $new_visual = $moderate_field_item;
                    if ($moderate_field_item == "country_id") {
                        $new_visual = $this->Country->getCountryNameById($product[$moderate_field_item]);
                    }
                    if ($moderate_field_item == "brand_id") {
                        $new_visual = $this->Manufacturer->getBrandNameById($product[$moderate_field_item]);
                    }
                    if ($moderate_field_item == "manufacturer_id") {
                        $new_visual = $this->Manufacturer->getManufacturerNameById($product[$moderate_field_item]);
                    }
                    echo "moderate " . $moderate_field_item . " " . $product[$moderate_field_item] . " " . $new_visual;
                    $this->Moderate->add_moderation_task($moderation_id, $moderate_field_item, "", $product[$moderate_field_item], "", $new_visual);
                }
            }
        } // если добавляется уже существующий товар
        else {
            $update_product_fields = array();
            foreach ($moderate_fields_list as $moderate_field_item) {
                $current_field = $this->getProductField($moderate_field_item, $product_id, $product);
                if (!key_exists($moderate_field_item, $product)) {
                    continue;
                }
                if ($current_field != $product[$moderate_field_item]) {
                    if (empty($current_field)) {
                        $update_product_fields[$moderate_field_item] = $product[$moderate_field_item];
                    }
                }
            }

            if (count($update_product_fields) > 0) {
                foreach ($update_product_fields as $key => $update_product_field) {
                    $null_product = null;
                    $old_field_value = $this->getProductField($key, $product_id, $null_product);
                    $old_visual = $key;
                    $new_visual = $additional_data[$key] ?? "";
                    if ($key == "country_id") {
                        $old_visual = $this->Country->getCountryNameById($update_product_field);
                    }
                    if ($key == "brand_id") {
                        $old_visual = $this->Manufacturer->getBrandNameById($update_product_field);
                    }
                    if ($key == "manufacturer_id") {
                        $old_visual = $this->Manufacturer->getManufacturerNameById($update_product_field);
                    }
                    echo "moderate " . $key . " " . $old_field_value . " " . $update_product_field . " " . $old_visual . " " . $new_visual;
                    $this->Moderate->add_moderation_task($moderation_id, $key, $old_field_value, $update_product_field, $old_visual, $new_visual);
                }
            }
        }

        // если все хорошо - идет сохранение товара, добавление полей на модерацию
        return $product_id;
    }


    /**
     * @param $field
     * @param $shop_product_id
     * @return |null
     */
    public function getShopProductField($field, $shop_product_id)
    {
        $this->Shop_Product = ClassRegistry::init("Shop_Product");
        $shop_product = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'id' => $shop_product_id,
                )
            )
        );

        if (isset($shop_product['Shop_Product'])) {
            $shop_product = $shop_product['Shop_Product'];
        }
        if (key_exists($field, $shop_product)) {
            return $shop_product[$field];
        }
        return null;

    }

    /**
     * @param $field
     * @param $product_id
     * @param null $product
     * @return |null
     */
    public function getProductField($field, $product_id, &$product = null)
    {
        if ($product == null) {
            $modelName = "Product";
            $this->Product = ClassRegistry::init($modelName);
            $product = $this->Product->find("first",
                array('conditions' =>
                    array(
                        'id' => $product_id,
                    )
                )
            );
        }
        if (isset($product['Product'])) {
            $product = $product['Product'];
        }
        if (key_exists($field, $product)) {
            return $product[$field];
        }
        return null;

    }

    /**
     * @param $field
     * @param $value
     * @param $product_id
     */
    public function set_product_field($field, $value, $product_id)
    {
        $modelName = "Product";
        $this->Product = ClassRegistry::init($modelName);
        $update_product = array(
            $field => $value
        );
        $this->Product->id = $product_id;
        $this->Product->save($update_product);
        return;
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getProductImages($product_id)
    {
        $modelName = "Product_Image";
        $this->Product_Image = ClassRegistry::init($modelName);
        return $this->Product_Image->find("all",
            array(
                'conditions' =>
                    array(
                        'product_id' => $product_id,
                    ),
                'order' => array('file ASC')
            )
        );
    }

    public function product_image_saver($uploaded_image, $system_barcode, $id)
    {
        //идентификатор изображения
        $image_id = md5(time() . " " . $uploaded_image);
        // конфигуратор создаваемых изображений
        $product_images_generator_param_list = array();
        $product_images_generator_param_list[] = ['big', [800, 600]];
        $product_images_generator_param_list[] = ['normal', [250, 250]];
        $product_images_generator_param_list[] = ['small', [60, 60]];

        foreach ($product_images_generator_param_list as $param_list) {
            $type_name = $param_list[0];
            $type_width = $param_list[1][0];
            $type_height = $param_list[1][1];
            $resizeded_image = resize_image(Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR') . DS . "p" . $id . DS . $uploaded_image, $type_width, $type_height);
            $image_generate_prefix = substr(md5(time() . $system_barcode), 0, 8) . "-" . $system_barcode;
            $result_file_name = $image_generate_prefix . "_" . $type_name . ".jpg";
            $resized_image = Configure::read('PRODUCT_IMAGE_FILE_UPLOAD_DIR') . DS . "p" . $id . DS . $result_file_name;
            $result_resized_name = $result_file_name;
            if (!file_exists($resized_image)) {
                imagejpeg($resizeded_image, $resized_image, 100);
            }

            $image_for_save = array(
                'type' => $type_name,
                "file" => $result_resized_name,
                'image_id' => $image_id,
                'product_id' => $id,
                'hash' => md5_file($resized_image),
            );
            $modelName = "Product_Image";
            $this->Product_Image = ClassRegistry::init($modelName);
            $this->Product_Image->create();
            $this->Product_Image->save($image_for_save);
        }
        return $image_id;
    }

    /* получение характеристик фильтров из импортного файла по названию*/
    public function find_filter_by_name($filter_name, $category_id)
    {
        $modelName = "CategoryProductParam";
        $this->CategoryProductParam = ClassRegistry::init($modelName);
        $filter_name = prepare_import_field($filter_name);
        $found_param = $this->CategoryProductParam->find("first",
            array('conditions' =>
                array(
                    'param_name' => $filter_name,
                    'category_id' => $category_id
                )
            )
        );
        if (count($found_param) > 0) {
            return $found_param['CategoryProductParam']['id'];
        } else return 0;
    }

    public function add_new_param_by_import($filter_name, $category_id, $unit = null)
    {
        $modelName = "ProductCategoryParam";
        $this->ProductCategoryParam = ClassRegistry::init($modelName);
        if ($unit == null) {
            $unit = "";
        }
        $saved_data = array(
            'param_name' => $filter_name,
            'param_description' => "",
            'default_value' => "",
            'status' => "hidden",
            'value_interval' => 1,
            'min_size' => 0,
            'max_size' => 0,
            'param_type' => 'list',
            'category_id' => $category_id,
            'unit' => $unit
        );
        $this->ProductCategoryParam->create();
        $this->ProductCategoryParam->save($saved_data);
        return $this->ProductCategoryParam->id;
    }

    public function check_filter_value($filter_id, $value)
    {
        $modelName = "CategoryProductParamValue";
        $this->CategoryProductParamValue = ClassRegistry::init($modelName);
        return $this->CategoryProductParamValue->find("count",
            array('conditions' =>
                array(
                    'param_id' => $filter_id,
                    'value' => $value
                )
            )
        );
    }

    public function add_filter_value($filter_id, $value, $product_id = null)
    {
        $modelName = "CategoryProductParamValue";
        $this->CategoryProductParamValue = ClassRegistry::init($modelName);
        $new_value = array(
            'param_id' => $filter_id,
            'value' => $value
        );
        $this->CategoryProductParamValue->create();
        $this->CategoryProductParamValue->save($new_value);
        $value_id = $this->CategoryProductParamValue->id;

        $modelName = "ProductParam";
        $this->ProductParam = ClassRegistry::init($modelName);

        if ($product_id > 0) {
            $new_product_param_value = array(
                'param_id' => $filter_id,
                'value' => $value_id,
                'product_id' => $product_id
            );
            $this->ProductParam->create();
            $this->ProductParam->save($new_product_param_value);
        }
        return $value_id;
    }

    public function add_product_param_value_force($filter_id, $value, $product_id)
    {
        $modelName = "CategoryProductParamValue";
        $this->CategoryProductParamValue = ClassRegistry::init($modelName);
        $value_id = $this->CategoryProductParamValue->find("first",
            array('conditions' =>
                array(
                    'param_id' => $filter_id,
                    'value' => $value
                )
            )
        );
        $value_id = $value_id['CategoryProductParamValue']['id'];
        $modelName = "ProductParam";
        $this->ProductParam = ClassRegistry::init($modelName);

        if ($product_id > 0) {
            $new_product_param_value = array(
                'param_id' => $filter_id,
                'value' => $value_id,
                'product_id' => $product_id
            );
            $this->ProductParam->create();
            $this->ProductParam->save($new_product_param_value);
        }
        return $value_id;
    }

    public function addParamValueToProduct($value_id, $filter_id, $product_id)
    {
        if ($value_id <= 0) {
            return null;
        }
        if ($product_id > 0) {
            $new_product_param_value = array(
                'param_id' => $filter_id,
                'value' => $value_id,
                'product_id' => $product_id
            );
            $modelName = "ProductParam";
            $this->ProductParam = ClassRegistry::init($modelName);
            $this->ProductParam->create();
            $this->ProductParam->save($new_product_param_value);
        }
        return $value_id;
    }

    /* получение id значения характеристики фильтра по имени*/
    public function findParamValueByName($filter_id, $value_name)
    {
        $modelName = "CategoryProductParamValue";
        $this->CategoryProductParamValue = ClassRegistry::init($modelName);
        $value_name = prepare_import_field($value_name);
        $found_param_value = $this->CategoryProductParamValue->find("first",
            array('conditions' =>
                array(
                    'param_id' => $filter_id,
                    'value' => $value_name
                )
            )
        );
        if (count($found_param_value) > 0) {
            return $found_param_value['CategoryProductParamValue']['id'];
        } else return 0;
    }

    /* получение id значения характеристики фильтра по имени*/
    public function filteringParamValue($value_name)
    {
        $units = [
            " мм",
            " м",
            " кг",
            " литр",
            " л",
            " шт",
            " об/мин",
            " Вт",
            " В",
            " Нм",
            " с",
            " с",
            " мин",
            " мин",
            " °",
            " Гц",

        ];
        $clear_value = "";
        $unit = "";
        $strlen = mb_strlen($value_name, "UTF-8");
        foreach ($units as $find_unit) {
            $find_unit_length = mb_strlen($find_unit, "UTF-8");
            if (mb_substr($value_name, $strlen - $find_unit_length, $find_unit_length, "UTF-8") == $find_unit) {
                $clear_value = str_replace($find_unit, "", $value_name);
                $unit = trim($find_unit);
            }
        }
        if (empty($clear_value)) {
            $clear_value = $value_name;
        }
        return ['clear_value' => $clear_value, 'unit' => $unit];

    }

    /**
     * @param $product_id
     * @param $company_id
     * @return mixed
     */
    public function ShopProductData($product_id, $company_id)
    {
        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);
        $ShopProductData = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'product_id' => $product_id,
                    'company_id' => $company_id
                )
            )
        );
        return $ShopProductData;
    }

    /**
     * @param $shop_product_id
     * @return mixed
     */
    public function ShopProduct2Data($shop_product_id)
    {
        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);
        $ShopProductData = $this->Shop_Product->find("first",
            array('conditions' =>
                array(
                    'id' => $shop_product_id
                )
            )
        );
        return $ShopProductData;
    }

    //TODO Добавить рассчет цены для индивидуального пользователя

    /**
     * @param $user_id
     * @param $shop_product_id
     * @param $base_price
     * @return mixed
     */
    public function calculateProductPriceByUser($user_id, $shop_product_id, $base_price)
    {
        return $base_price;
    }

    public function getProductOfferInAllCompanies($product_id)
    {
        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);
        $ShopProductOffers = $this->Shop_Product->find("all",
            array('conditions' =>
                array(
                    'product_id' => $product_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'companies',
                        'alias' => 'Company',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop_Product.company_id = Company.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Shop_Product.*',
                    'Company.*',
                ),
                'order' => array('Company.id ASC')
            )
        );
        return $ShopProductOffers;
    }

    public function getFiltersAndValuesByProductId($product_id)
    {
        $modelName = "Product_Param";
        $this->Product_Param = ClassRegistry::init($modelName);
        $product_param_data = $this->Product_Param->find("all",
            array('conditions' =>
                array(
                    'product_id' => $product_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'category_product_params',
                        'alias' => 'Category_Product_Param',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Product_Param.param_id = Category_Product_Param.id'
                        )
                    ),
                    array(
                        'table' => 'category_product_param_values',
                        'alias' => 'Category_Product_Param_Value',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Product_Param.value = Category_Product_Param_Value.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Category_Product_Param.*',
                    'Category_Product_Param_Value.*',
                    'Product_Param.*',
                ),
                'order' => array('Product_Param.id ASC')
            )
        );
        return $product_param_data;
    }

    /**
     * @param $product_id
     * @param $rate
     * @param $text
     * @param $experience
     * @array $photos
     * @param $parent_id
     * @return bool
     */
    public function userAddProductComment($user_id, $product_id, $rate, $text, $experience, $use_photos, $parent_id)
    {
        $modelName = "Product_Rating";
        $this->Product_Rating = ClassRegistry::init($modelName);
        $valid_rates = [1, 2, 3, 4, 5];
        if (!in_array($rate, $valid_rates)) {
            return false;
        }
        if (intval($product_id) <= 0) {
            return false;
        }
        $new_rate = [
            'user_id' => $user_id,
            'product_id' => $product_id,
            'text' => $text,
            'rating' => $rate,
            'experience' => $experience
        ];
        $this->Product_Rating->save($new_rate);
        $rate_id = $this->Product_Rating->id;
        if ($rate_id > 0) {
            $this->recalcProductRating($product_id);

            if (count($use_photos) > 0) {
                foreach ($use_photos as $use_photo) {
                    $photo_image = [
                        'file' => $use_photo,
                        'rate_id' => $rate_id,
                        'product_id' => $product_id
                    ];
                    $this->Product_Rating_Image->create();
                    $this->Product_Rating_Image->save($photo_image);
                }
            }
        }
        return $this->Product_Rating->id;
    }

    public function recalcProductRating($product_id)
    {
        $modelName = "Product_Rating";
        $this->Product_Rating = ClassRegistry::init($modelName);
        $ratings = $this->Product_Rating->find("all",
            array('conditions' =>
                array(
                    'product_id' => $product_id,
                )
            )
        );

        $rating_values = [];
        foreach ($ratings as $rating) {
            $rating_data = $rating['Product_Rating'];
            $rating_values[] = $rating_data['rating'];
        }

        $cur_average_rating = array_average($rating_values);
        $cur_average_rating = number_format($cur_average_rating, 2, '.', '');
        $this->Product->id = $product_id;
        $new_rate = [
            'stat__current_rating' => $cur_average_rating
        ];
        $this->Product->save($new_rate);
    }

    // получение технических параметров товара
    public function getProductParams($product_id, $source = null)
    {
        $modelName = "Product_Param";
        $this->Product_Param = ClassRegistry::init($modelName);
        if ($product_id != null) {

            if ($source !== null) {
                $source_array = ['source' => $source];
            } else {
                $source_array = [];
            }

            $product_params = $this->Product_Param->find("all",
                array('conditions' =>
                    array(
                        'product_id' => $product_id,
                        $source_array
                    )
                )
            );
            if (count($product_params) == 0) {
                return false;
            }
            return $product_params['Product_Param'];
        }
    }

    /**
     * @param $string
     * @return mixed
     */
    private function findParamValuesByString($string)
    {
        $modelName = "Category_Product_Param_Value";
        $this->Category_Product_Param_Value = ClassRegistry::init($modelName);
        $product_ids_by_string = $this->Category_Product_Param_Value->find("all",
            array('conditions' =>
                array(
                    'Category_Product_Param_Value.value LIKE' => "%$string%"
                ),
                'joins' => array(
                    array(
                        'table' => 'category_product_params',
                        'alias' => 'Category_Product_Param',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Category_Product_Param_Value.param_id = Category_Product_Param.id'
                        )
                    ),
                    array(
                        'table' => 'product_params',
                        'alias' => 'Product_Param',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Product_Param.param_id = Category_Product_Param.id'
                        )
                    ),

                ),
                'fields' => array(
                    'DISTINCT Product_Param.product_id',
                ),
                'order' => array('Product_Param.id ASC')
            )
        );
        $product_ids_arr = [];
        foreach ($product_ids_by_string as $product_ids_item) {
            $product_ids_arr[] = $product_ids_item['Product_Param']['product_id'];
        }
        return $product_ids_arr;

    }

    /**
     * @param $string
     * @return array
     */
    private function findProductIdsByFieldString($string)
    {
        $found_products_by_product_data_fields = $this->Product->find("all",
            array('conditions' =>
                array(
                    'or' => array(
                        array(
                            'product_name LIKE' => "%$string%",
                        ),
                        array(
                            'product_model LIKE' => "%$string%",
                        ),
                        array(
                            'prefix LIKE' => "%$string%",
                        ),
                        array(
                            'description LIKE' => "%$string%",
                        ),
                    ),
                ),
                'fields' => array(
                    'Product.id',
                ),
            )
        );
        $product_ids_arr = [];
        foreach ($found_products_by_product_data_fields as $product_ids_item) {
            $product_ids_arr[] = $product_ids_item['Product']['id'];
        }
        return $product_ids_arr;
    }

    public function searchProductsByString($string)
    {
        $found_product_ids_by_param_values = $this->findParamValuesByString($string);
        pr($found_product_ids_by_param_values);


        return array();
    }

    /**
     * @param $shop_product_id
     * @param $new_price
     * @return bool
     */
    public function updateCartProductPrice($shop_product_id, $new_price)
    {

        $this->Cart = ClassRegistry::init("Cart");
        $product_in_cart = $this->Cart->find("all",
            array('conditions' =>
                array(
                    'shop_product_id' => $shop_product_id
                ),
                'order' => array('Cart.id ASC')
            )
        );
        if (count($product_in_cart) == 0) {
            return false;
        }
        foreach ($product_in_cart as $product_in_cart_item) {
            $cart_id = $product_in_cart_item['Cart']['id'];
            $this->Cart->id = $cart_id;
            $this->Cart->save(['price' => $new_price]);
        }
    }

    public function productListWithRequiredUpdatePriceParse($company_id)
    {
        // TODO HARDCODE
        // приоритет для необходимости обновления цены методом парсинга - товары, у которых цена не обновлялась более суток
        // товары с самым большим количеством просмотров, то есть, наиболее востреюбованные
        $this->Shop_Product = ClassRegistry::init("Shop_Product");
        $products = $this->Shop_Product->find("all",
            array('conditions' =>
                array(
                    'company_id' => $company_id,
                    //'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(price_updated) >=' => '180',
                ),
                'order' => array('price_updated ASC'),
                'limit' => 10
            )
        );
        return $products;

    }

    // ELASTIC SEARCH
    public function findElasticBrandName()
    {
        pr($this->Product->query('SELECT LEVENSHTEIN(`name`, "BOSH") as differrence, name, id FROM Brands ORDER BY differrence ASC LIMIT 10'));
        exit;
    }

    public function getProductById($id)
    {
        $product = $this->Product->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );

        return $product;
    }

    /**
     * @param $user_id
     * @param $product_id
     * @param $datetime
     * @return mixed
     */
    public function addProductView($user_id, $product_id)
    {
        // интервал обновления записей об просмотре товара - раз в 10 минут
        $modelName = "Product_View";
        $this->Product_View = ClassRegistry::init($modelName);
        // интервал добавления записи о просмотре - 5 минут
        $get_last_user_view = $this->Product_View->find("count",
            array('conditions' =>
                array(
                    'product_id' => $product_id,
                    'UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(created) <=' => '600'
                )
            )
        );
        if ($get_last_user_view > 0) {
            return true;
        }

        $new_view = [
            'user_id' => $user_id,
            'product_id' => $product_id,
        ];
        $this->Product_View->save($new_view);
        return $this->Product_View->id;
    }

    /** Просмотренные пользователем товары товары
     * @param $user_id - ИД пользователя
     * @param $count - количество товаров
     * @return array|bool
     */
    public function getLastProductViewForUser($user_id, $count)
    {
        $modelName = "Product_View";
        $this->Product_View = ClassRegistry::init($modelName);

        $res = $this->Product_View->find("all",
            array('conditions' =>
                array(
                    'user_id' => $user_id,
                ),
                'limit' => $count,
                'order' => array('created DESC'),
            )
        );
        if ($res) {
            $data = [];
            foreach ($res as $v) {
                $data[] = $v['Product_View']['id'];
            }
            return $data;
        } else {
            return false;
        }
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getTotalProductViews($product_id)
    {
        $modelName = "Product_View";
        $this->Product_View = ClassRegistry::init($modelName);
        return $this->Product_View->find("count",
            array('conditions' =>
                array(
                    'product_id' => $product_id
                )
            )
        );
    }

    /**
     * @param $company_id
     * @return mixed
     */
    public function created_product_offers_by_company($company_id)
    {
        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);
        $ShopProductCount = $this->Shop_Product->find("count",
            array('conditions' =>
                array(
                    'company_id' => $company_id
                )
            )
        );
        return $ShopProductCount;
    }

    /*
        public function searchProductsByString($string)
        {
            $found_product_ids_by_param_values = $this->findParamValuesByString($string);
            pr($found_product_ids_by_param_values);


            return array();
        }*/

    /**
     * @param $company_id
     * @return array|null
     */
    public function getRegionListByCompanyOffers($company_id)
    {
        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);
        $ShopProducts = $this->Shop_Product->find("all",
            array('conditions' =>
                array(
                    'company_id' => $company_id
                )
            )
        );
        if (count($ShopProducts) == 0) {
            return null;
        }
        $modelName = "Shop_Region_Product";
        $this->Shop_Region_Product = ClassRegistry::init($modelName);
        $uniqueOfferRegions = [];

        foreach ($ShopProducts as $ShopProduct) {

            $offer_id = $ShopProduct['Shop_Product']['id'];
            $region_offers = $this->Shop_Region_Product->find("all",
                array('conditions' =>
                    array(
                        'shop_product_id' => $offer_id
                    )
                )
            );

            if (count($region_offers) > 0) {
                foreach ($region_offers as $region_offer) {
                    $region_id = $region_offer['Shop_Region_Product']['region_id'];
                    $region_name = $this->Region->getRegionNameById($region_id);
                    $region = array('id' => $region_id, 'name' => $region_name);
                    if (!in_array($region, $uniqueOfferRegions)) {
                        $uniqueOfferRegions[] = $region;
                    }
                }
            }
        }
        return $uniqueOfferRegions;
    }

    /**
     * @param $field
     * @param $value
     * @param int $shop_product_id
     * @return mixed
     */
    public function addShopProductField($field, $value, $shop_product_id)
    {
        $modelName = "Shop_Product_Field";
        $this->Shop_Product_Field = ClassRegistry::init($modelName);
        $new_field = [
            'field' => $field,
            'value' => $value,
            'shop_product_id' => $shop_product_id
        ];
        $this->Shop_Product_Field->create();
        $this->Shop_Product_Field->save($new_field);
        return $this->Shop_Product_Field->id;
    }

    /**
     * @param $field
     * @param $value
     * @param $shop_product_id
     * @return bool
     */
    public function updateShopProductField($field, $value, $shop_product_id)
    {
        $modelName = "Shop_Product_Field";
        $this->Shop_Product_Field = ClassRegistry::init($modelName);

        $shop_product_field_id = $this->Shop_Product_Field->find("first",
            array('conditions' =>
                array(
                    'shop_product_id' => $shop_product_id,
                    'field' => $field
                )
            )
        );
        if (count($shop_product_field_id) == 0) {
            return false;
        }

        $field = [
            'value' => $value,
        ];
        $this->Shop_Product_Field->id = $shop_product_field_id['Shop_Product_Field']['id'];
        $this->Shop_Product_Field->save($field);
        return true;
    }

    /**
     * @param int $region_id
     * @param $price
     * @param $old_price
     * @param int $shop_product_id
     * @param bool $abort
     * @return mixed
     */
    public function addShopProductRegionData($region_id, $price, $old_price, $shop_product_id, $abort = false)
    {
        $modelName = "Shop_Region_Product";
        $this->Shop_Region_Product = ClassRegistry::init($modelName);
        $new_region_product = [
            'region_id' => $region_id,
            'price' => $price,
            'old_price' => $old_price,
            'shop_product_id' => $shop_product_id,
            'abort' => $abort
        ];
        $this->Shop_Region_Product->create();
        $this->Shop_Region_Product->save($new_region_product);
        return $this->Shop_Region_Product->id;
    }

    /**
     * @param $shop_product_id
     * @return |null
     */
    public function ShopProductOfferFields($shop_product_id)
    {
        $modelName = "Shop_Product_Field";
        $this->Shop_Product_Field = ClassRegistry::init($modelName);
        $ShopProductFields = $this->Shop_Product_Field->find("all",
            array('conditions' =>
                array(
                    'shop_product_id' => $shop_product_id
                )
            )
        );
        if (count($ShopProductFields) == 0) {
            return null;
        }
        return $ShopProductFields;
    }

    /**
     * @param $product_id
     * @param $type_price
     * @return float|int
     */
    public function calcProductOfferPrice($product_id, $type_price)
    {
        $offers = $this->getProductOfferInAllCompanies($product_id);
        if ($type_price == "average") {
            $count = count($offers);
            if ($count == 0) {
                return 0;
            }
            $sum = 0;
            foreach ($offers as $offer) {
                $base_price = $offer['Shop_Product']['base_price'];
                $sum += $base_price;
            }
            $average_price = $sum / $count;
            return $average_price;
        } else if ($type_price == "min") {
            $min_price = 0;
            foreach ($offers as $offer) {
                $base_price = $offer['Shop_Product']['base_price'];
                if ($base_price < $min_price OR $min_price == 0) {
                    $min_price = $base_price;
                }
            }
            return $min_price;
        } else if ($type_price == "max") {
            $max_price = 0;
            foreach ($offers as $offer) {
                $base_price = $offer['Shop_Product']['base_price'];
                if ($base_price > $max_price) {
                    $max_price = $base_price;
                }
            }
            return $max_price;
        }
    }

    /**
     * @param $shop_product_id
     * @return bool
     */
    public function deleteShopProduct($shop_product_id)
    {
        $this->deleteShopRegionProductFields($shop_product_id);
        $this->deleteShopProductFields($shop_product_id);
        $modelName = "Shop_Product";
        $this->Shop_Product = ClassRegistry::init($modelName);
        $this->Shop_Product->id = $shop_product_id;
        $this->Shop_Product->delete();
        return true;
    }

    private function deleteShopRegionProductFields($shop_product_id)
    {
        // удаление полей регионов
        $modelName = "Shop_Region_Product";
        $this->Shop_Region_Product = ClassRegistry::init($modelName);
        $region_fields = $this->Shop_Region_Product->find("all",
            array(
                'conditions' =>
                    array(
                        'shop_product_id' => $shop_product_id
                    ),
            )
        );
        if (count($region_fields) > 0) {
            foreach ($region_fields as $region_field) {
                $this->Shop_Region_Product->id = $region_field['Shop_Region_Product']['id'];
                $this->Shop_Region_Product->delete();
            }
        }
    }

    private function deleteShopProductFields($shop_product_id)
    {
        // удаление полей регионов
        $modelName = "Shop_Product_Field";
        $this->Shop_Product_Field = ClassRegistry::init($modelName);
        $product_fields = $this->Shop_Product_Field->find("all",
            array(
                'conditions' =>
                    array(
                        'shop_product_id' => $shop_product_id
                    ),
            )
        );
        if (count($product_fields) > 0) {
            foreach ($product_fields as $product_field) {
                $this->Shop_Product_Field->id = $product_field['Shop_Product_Field']['id'];
                $this->Shop_Product_Field->delete();
            }
        }
    }

    /** Получить данные о товарах и предложения по ним
     * @param $data
     *          'page' - пагинация
     * @return array|bool
     */
    public function getProductList($data)
    {
        $arr = [
            'conditions' => [
                'Product.id IN' => $data['ids'],
            ],
            'fields' => [
                'Product.*',
            ],
            'order' => 'Product.id ASC',
            'limit' => $data['count'],
            'page' => $data['page'],
        ];

        if ($data['sort_field']) {
            switch ($data['sort_field']) {
                case 'price':
                    $sort = 'Shop_Product.base_price';
                    break;
                default:
                    $sort = 'Shop_Product.base_price';
                    break;
            }

            if ($data['sort_dir'] == 'desc') {
                $sort .= ' DESC';
            } else {
                $sort .= ' ASC';
            }

            $arr['order'] = $sort;
            $arr['joins'] = [
                [
                    'table' => 'shop_products',
                    'alias' => 'Shop_Product',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Shop_Product.product_id = Product.id'
                    ],
                ],
            ];
        }

        $products_res = $this->Product->find("all", $arr);

        if (!$products_res) {
            return false;
        }

        foreach ($products_res as $v) {
            $response_product_id[] = $v['Product']['id'];
            $products[$v['Product']['id']]['id'] = $v['Product']['id'];
            $products[$v['Product']['id']]['barcode'] = $v['Product']['system_barcode'];
            $products[$v['Product']['id']]['name'] = $v['Product']['product_name'];
            $products[$v['Product']['id']]['rating'] = $v[0]['stat__current_rating'];
            $products[$v['Product']['id']]['weight'] = $v['Product']['weight'];
            $products[$v['Product']['id']]['width'] = $v['Product']['width'];
            $products[$v['Product']['id']]['height'] = $v['Product']['height'];
            $products[$v['Product']['id']]['length'] = $v['Product']['length'];
            $products[$v['Product']['id']]['volume'] = $v['Product']['volume'];
        }
        return $return = [
            'products' => $products,
            'ids' => $response_product_id,
        ];
    }

    /** Получить картинки для товаров
     * @param $ids array - нумерованный массив ИД тваров
     * @return bool
     */
    public function getImagesProductList($ids)
    {
        $images_res = $this->Product_Image->find("all",
            array(
                'conditions' =>
                    array(
                        'Product_Image.product_id IN' => $ids,
                        'Product_Image.type' => 'normal',
                    ),
                'group' => 'Product_Image.file',
            )
        );
        if ($images_res) {
            foreach ($images_res as $v) {
                $im[$v['Product_Image']['product_id']][] = [
                    'url' => $v['Product_Image']['file'],
                    'alt' => '',
                ];
            }
            return $im;
        } else {
            return false;
        }
    }

    /** Получить параметры товаров по их ИД
     * @param $ids array - нумерованный массив ИД тваров
     * @return bool
     */
    public function getParamsProductList($ids)
    {
        $params_res = $this->Product_Param->find("all",
            array(
                'conditions' =>
                    array(
                        'Product_Param.product_id IN' => $ids,
                    ),
                'joins' => array(
                    array(
                        'table' => 'category_product_params',
                        'alias' => 'Param',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Param.id = Product_Param.param_id'
                        )
                    ),
                    array(
                        'table' => 'category_product_param_values',
                        'alias' => 'Param_Value',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Param_Value.id = Product_Param.value'
                        )
                    ),
                ),
                'fields' => array(
                    'Product_Param.*',
                    'Param.*',
                    'Param_Value.*',
                ),
            )
        );
        if ($params_res) {
            foreach ($params_res as $v) {
                $params[$v['Product_Param']['product_id']][] = [
                    'title' => $v['Param']['param_name'],
                    'value' => $v['Param_Value']['value'],
                    'unit' => $v['Param']['unit'],
                    'type' => $v['Param']['param_type'],
                    'isImportant' => $v['Param']['is_important'],
                ];
            }
            return $params;
        } else {
            return false;
        }
    }

    /** Получить предложения по товарам
     * @param $data
     * @return bool
     */
    public function getOffersProductList($data)
    {
        $offers_res = $this->Shop_Product->find("all",
            array(
                'conditions' => array(
                    'Shop_Product.product_id IN' => $data['ids'],
                ),
                'joins' => array(
                    array(
                        'table' => 'companies',
                        'alias' => 'Company',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company.id = Shop_Product.company_id'
                        )
                    ),
                ),
                'fields' => array(
                    'Shop_Product.*',
                    'Company.*',
                ),
            )
        );

        if ($offers_res) {
            foreach ($offers_res as $v) {
                $available = ($v['Shop_Product']['status'] == 'active') ? true : false;
                $offers[$v['Shop_Product']['product_id']][] = [
                    'offer_id'      => $v['Shop_Product']['id'],
                    'company_offer' => [
                        'id'     => $v['Company']['id'],
                        'name'   => $v['Company']['company_name'],
                        'rating' => 0,
                    ],
                    'price' => $v['Shop_Product']['base_price'],
                    'old_price' => $v['Shop_Product']['old_price'],
                    'is_available' => $available,
                    'delivery' => [
                        'transport' => [],
                        'pickup' => [],
                    ],
                    'amount_available' => 10,
                    'has_chat' => false,
                ];
            }
            return $offers;
        } else {
            return false;
        }
    }

    /** Получить ИД предложений товаров по фильтрам
     * @param $data
     * @return array|bool
     */
    public function getOffersFilterId($data)
    {
        $conditions['Shop_Product.product_id IN'] = $data['ids'];
        if (!empty($data['price_min'])) {
            $conditions['Shop_Product.base_price >= '] = $data['price_min'];
        }
        if (!empty($data['price_max'])) {
            $conditions['Shop_Product.base_price <= '] = $data['price_max'];
        }
        if (!empty($data['active_offers'])) {
            $conditions['Shop_Product.status'] = 'active';
        }

        $offers_res = $this->Shop_Product->find("all",
            array(
                'conditions' => $conditions,
                'fields' => array(
                    'Shop_Product.product_id',
                ),
                'group' => 'Shop_Product.product_id',
            )
        );
        if ($offers_res) {
            $count = 0;
            foreach ($offers_res as $v) {
                $count++;
                $offers[] = $v['Shop_Product']['product_id'];
            }
            return $offers = [
                'products' => $offers,
                'count' => $count,
            ];
        } else {
            return false;
        }
    }

    /** Поиск товаров в категории с учетом фильтрав категории
     * @param $data
     * @return array|bool
     */
    public function getProductsInCatId($data)
    {
        $query = [
            'conditions' => [
                'Product.category_id' => $data['cat_id'],
            ],
            'fields' => [
                'Product.id',
            ]
        ];
        // обираю запрос с учетом фильтров категории
        if (!empty($data['filters'])) {
            $query['joins'] = [
                [
                    'table' => 'product_params',
                    'alias' => 'Product_Params',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Product_Params.product_id = Product.id',
                    ],
                ],
            ];

            foreach($data['filters'] as $k => $v){
                $f[] = "Product_Params.param_id=".$k;
                $str = 'Product_Params.value IN (';
                foreach($v as $val){
                    $str .= $val.',';
                }
                $str = substr($str,0,-1);
                $str .= ')';
                $f[] = $str;
            }
            $query['conditions'] = array_merge($query['conditions'], $f);
        }

        $products_res = $this->Product->find("all",$query);

        if (!$products_res) {
            return false;
        }

        $count = 0;
        foreach ($products_res as $v) {
            $count++;
            $products[] = $v['Product']['id'];
        }
        return $return = [
            'products' => $products,
            'count' => $count,
        ];
    }

    public function getFiltersFromRequest($request)
    {
        $filter_array = [];
        $filtering_substring = "filter_";
        foreach ($request->query as $key => $value) {
            if (substr_count($key, $filtering_substring) > 0) {
                $filter_data = explode("_", $key);
                $filter_id = intval($filter_data[1]);
                $filter_values = $request->query($filtering_substring . $filter_id) ?? null;
                if ($filter_values != null) {
                    if (substr_count($filter_values, ",") == 0) {
                        $filter_values = str_replace("[", "", $request->query($filtering_substring . $filter_id));
                        $filter_values = str_replace("]", "", $filter_values);
                        if(trim($filter_values) == ''){
                            continue;
                        }
                        else{
                            $filter_array[$filter_id][] = $filter_values;
                        }
                    } else {
                        $filter_values = str_replace("[", "", $filter_values);
                        $filter_values = str_replace("]", "", $filter_values);
                        $filter_values_list = explode(",", $filter_values);
                        $filter_array[$filter_id] = $filter_values_list;
                    }
                }
            }
        }
        if(count($filter_array) > 0){
            return $filter_array;
        }
        else{
            return false;
        }
    }

}