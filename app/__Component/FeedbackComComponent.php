<?php

App::uses(
	'AppController', 'Controller'
);
App::uses('Model', 'Model');
App::uses('L10n', 'L10n');

/**
 * Компонент Обратная связь
 */
class FeedbackComComponent extends Component
{
	public $components = array(
	    'Session',
        'Error'
    );

    function setupModels(){
        $modelName = "Feedback";
        $this->Feedback = ClassRegistry::init($modelName);
    }

	function initialize(Controller $controller)
	{
		$this->controller = $controller;
        $this->setupModels();
	}

	public function get_feedbacks($status = null){
        $this->setupModels();
        if($status!=null){
            $status_list = ['status' => $status];
        } else if($status == "active") {
            $status_list = array('status !=' => array('new', 'active'));
        } else {
            $status_list = "";
        }
        $feedbacks = $this->Feedback->find("all",
            array('conditions' =>
                array(
                    $status_list
                ),
            )
        );
        return $feedbacks;
    }

}