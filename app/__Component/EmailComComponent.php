<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Email
 */
class EmailComComponent extends Component
{
    public $components        = [
        'Session',
        'Error',
        'Log',
    ];

    public $critical_priority = 3;

    public $high_priority     = 2;

    public $normal_priority   = 1;

    public $senders           = [
        'main'      => ['name' => "Terpikka", "email" => "mail@terpikka.ru"],
        'support'   => ['name' => "Terpikka Support", "email" => "support@terpikka.ru"],
        'finance'   => ['name' => "Terpikka Finance", "email" => "finance@terpikka.ru"],
        'no-replay' => ['name' => "Terpikka Advertisement", "email" => "a.mat@terpikka.ru"],
    ];

    public $controller;

    public $email_debug       = false;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param $receiver
     * @param $sender
     * @param $subject
     * @param $layout
     * @param $template
     * @param $message_data
     */
    private function sendEmail($receiver, $sender, $subject, $layout, $template, $message_data, $attachment)
    {
        if ($sender == "null" or $sender == null) {
            $sender = Configure::read('SITE_MAIL');
        }

        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail();
        $email->config("gmail");
        $email->emailFormat('html');
        $email->template($template, $layout);
        $email->from($sender);
        $email->to($receiver);
        $email->subject($subject);
        if ($attachment != null) {
            $email->attachments($attachment);
        }
        $email->viewVars(['sended_data' => $message_data]);
        $email->send();
        exit;
    }

    /**
     * @param        $receiver
     * @param        $sender
     * @param        $subject
     * @param        $layout
     * @param        $template
     * @param        $message_data
     * @param string $attachment
     * @param string $start_sending
     */
    public function sendEmailNow(
        $receiver,
        $sender,
        $subject,
        $layout,
        $template,
        $message_data,
        $attachment = "",
        $start_sending = ""
    ) {
        $this->sendEmail($receiver, $sender, $subject, $layout, $template, $message_data, $attachment);
    }

    /**
     * @param        $receiver
     * @param        $sender
     * @param        $subject
     * @param        $layout
     * @param        $template
     * @param        $message_data
     * @param int    $importance
     * @param string $attachment
     * @param string $start_sending
     * @param int    $query_id
     *
     * @return mixed
     */
    public function sendEmailLater(
        $receiver,
        $sender,
        $subject,
        $layout,
        $template,
        $message_data,
        $importance = 1,
        $attachment = "",
        $start_sending = "",
        $query_id = 0
    ) {
        $this->setupModel();
        $new_mail = [
            'receiver'      => $receiver,
            'sender'        => $sender,
            'query_id'      => $query_id,
            'subject'       => $subject,
            'layout'        => $layout,
            'template'      => $template,
            'message_data'  => $message_data,
            'status'        => 'new',
            'attachment'    => $attachment,
            'importance'    => $importance,
            'start_sending' => $start_sending,

        ];
        $this->Mail_Item->save($new_mail);

        return $this->Mail_Item->id;
    }

    public function setupModel()
    {
        $modelName       = "MailQuery";
        $this->MailQuery = ClassRegistry::init($modelName);

        $modelName       = "Mail_Item";
        $this->Mail_Item = ClassRegistry::init($modelName);
    }

    /**
     * @param int $amount
     */
    public function sendQuery($amount = 20)
    {
        $this->setupModel();
        $mailing_query = $this->Mail_Item->find(
            "all",
            [
                'conditions' =>
                    [
                        'status !=' => 'success',
                        'UNIX_TIMESTAMP(start_sending) > UNIX_TIMESTAMP(NOW())',
                    ],
                'order'      => ['importance DESC'],
                'limit'      => $amount,
            ]
        );
        foreach ($mailing_query as $mailing_item) {
            $mailing_item = $mailing_item['Mail_Item'];
            $this->sendEmail(
                $mailing_item['receiver'],
                $mailing_item['sender'],
                $mailing_item['subject'],
                $mailing_item['layout'],
                $mailing_item['template'],
                $mailing_item['message_data'],
                $mailing_item['attachment']
            );
            $this->Mail_Item->id = $mailing_item['id'];
            $this->Mail_Item->save(['status' => 'success']);
        }
    }


    //MAILJET

    /**
     * @param $recipient_name
     * @param $recipient_email
     * @param $subject
     * @param $HTML
     *
     * @return bool
     */
    public function sendRegisterOrdererEmail($recipient_name, $recipient_email, $subject, $HTML)
    {
        $sender       = $this->senders['no-replay'];
        $sender_name  = $sender['name'];
        $sender_email = $sender['email'];

        return $this->sendViaMailJet($recipient_name, $recipient_email, $sender_name, $sender_email, $subject, $HTML);
    }

    /**
     * @param $recipient_name
     * @param $recipient_email
     * @param $subject
     * @param $HTML
     *
     * @return bool
     */
    public function sendMakeOrderByOrdererEmail($recipient_name, $recipient_email, $subject, $HTML)
    {
        $sender       = $this->senders['no-replay'];
        $sender_name  = $sender['name'];
        $sender_email = $sender['email'];

        return $this->sendViaMailJet($recipient_name, $recipient_email, $sender_name, $sender_email, $subject, $HTML);
    }

    /**
     * @param $recipient_name
     * @param $recipient_email
     * @param $subject
     * @param $HTML
     *
     * @return bool
     */
    public function sendBaseEmail($recipient_name, $recipient_email, $subject, $HTML)
    {
        $sender       = $this->senders['no-replay'];
        $sender_name  = $sender['name'];
        $sender_email = $sender['email'];

        return $this->sendViaMailJet($recipient_name, $recipient_email, $sender_name, $sender_email, $subject, $HTML);
    }

    /**
     * @param $recipient_name
     * @param $recipient_email
     * @param $sender_name
     * @param $sender_email
     * @param $subject
     * @param $HTML
     *
     * @return bool
     */
    private function sendViaMailJet($recipient_name, $recipient_email, $sender_name, $sender_email, $subject, $HTML)
    {
        /*
        $data = [
            "Messages" => [
                [
                    "From" => [
                        "Email" => "a.mat@terpikka.ru",
                        "Name" => "artur"
                    ],
                    "To" => [
                        [
                            "Email" => "a.mat@terpikka.ru",
                            "Name" => "artur"
                        ]
                    ],
                    "Subject" => "My first Mailjet email",
                    "TextPart" => "Greetings from Mailjet.",
                    "HTMLPart" => "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
                    "CustomID" => "AppGettingStartedTest"
                ]
            ]
        ];*/
        $data        = [
            "Messages" => [
                [
                    "From"     => [
                        "Email" => $sender_email,
                        "Name"  => $sender_name,
                    ],
                    "To"       => [
                        [
                            "Email" => $recipient_email,
                            "Name"  => $recipient_name,
                        ],
                    ],
                    "Subject"  => $subject,
                    "TextPart" => "Greetings from Mailjet.",
                    "HTMLPart" => $HTML,
                    "CustomID" => "AppGettingStartedTest",
                ],
            ],
        ];
        $data_format = json_encode($data);

        $auth_login    = Configure::read('MAILJET_API_LOGIN');
        $auth_password = Configure::read('MAILJET_API_PASS');
        $send_url      = Configure::read('MAILJET_API_URL');

        $curl = curl_init($send_url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_format);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "$auth_login:$auth_password");
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $postResult = curl_exec($curl);
        if (curl_errno($curl)) {
            $errors = print_r(curl_error($curl), true);
            $this->Log->add("Sending Mail Error - " . $errors, "email_component");
            $this->Log->add(serialize(curl_error($curl)), "email_component");
        }
        $this->Log->add(serialize($postResult), "email_component");

        $res = json_decode($postResult);
        if ($res->Messages[0]->Status == 'success') {
            return true;
        } else {
            return false;
        }
    }

    public function sendTestMailJet($email)
    {

        $data = [
            "Messages" => [
                [
                    "From"     => [
                        "Email" => "a.mat@terpikka.ru",
                        "Name"  => "artur",
                    ],
                    "To"       => [
                        [
                            "Email" => $email,
                            "Name"  => "recipient",
                        ],
                    ],
                    "Subject"  => "TEST Mailjet email",
                    "TextPart" => "Greetings from Mailjet.",
                    "HTMLPart" => "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
                    "CustomID" => "AppGettingStartedTest",
                ],
            ],
        ];

        $data_format = json_encode($data);

        $auth_login    = Configure::read('MAILJET_API_LOGIN');
        $auth_password = Configure::read('MAILJET_API_PASS');
        $send_url      = Configure::read('MAILJET_API_URL');

        $curl = curl_init($send_url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_format);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "$auth_login:$auth_password");
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $postResult = curl_exec($curl);
        if (curl_errno($curl)) {
            print_r(curl_error($curl), true);
            $errors = print_r(curl_error($curl), true);
            $this->Log->add("Sending Mail Error - " . $errors, "email_component");
            $this->Log->add(serialize(curl_error($curl)), "email_component");
        }
        $this->Log->add(serialize($postResult), "email_component");

        $res = json_decode($postResult);
        if ($res->Messages[0]->Status == 'success') {
            return true;
        } else {
            return false;
        }
    }
}