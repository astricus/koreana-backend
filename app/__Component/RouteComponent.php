<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Маршруты доставки
 */
class RouteComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Orderer',
        'OrderCom',
        'Car',
        'Loader',
        'Driver',
        'DateTime'
    );

    public $point_types = [
        'load_products_from_company' => 'Забор заказа от поставщика',
        'load_products_to_orderer' => 'Доставка заказа покупателю',
        'return_products_from_orderer' => 'Забор возврата от покупателя',
        'return_products_to_company' => 'Доставка возврата поставщику',
        'organization' => 'Орг вопрос',
    ];

    public $valid_point_type = [
        'company_store' => 'Склад поставщика',
        'orderer' => 'Адрес покупателя',
        'office' => 'Офис/Склад',
    ];

    public $unknown_delivery_route_point_type = "Неизвестный тип точки марщрута доставки";
    public $empty_delivery_route_point_type = "Пуст тип точки марщрута доставки";

    public $delivery_status = [
        'new' => 'Новая доставка',
        'transit_to_store' => 'В пути на склад ТК',
        'transit_to_client' => 'В пути к клиенту',
        'complete' => 'Доставлен',
        'abort' => 'Отменен',
    ];

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Delivery";
        $this->Delivery = ClassRegistry::init($modelName);

        $modelName = "Route_Point";
        $this->Route_Point = ClassRegistry::init($modelName);

        $modelName = "Delivery_Route";
        $this->Delivery_Route = ClassRegistry::init($modelName);

        $modelName = "Delivery_Offer";
        $this->Delivery_Offer = ClassRegistry::init($modelName);

        $modelName = "Order";
        $this->Order = ClassRegistry::init($modelName);

        $modelName = "Client_Order";
        $this->Client_Order = ClassRegistry::init($modelName);

        $modelName = "Order_Product";
        $this->Order_Product = ClassRegistry::init($modelName);
    }


//ФУНКЦИОНАЛ MVP ДЛЯ FULLFILLMENT

    /*методы:
    1. Создание (редактирование, удаление) маршрутной точки (обязательные поля: - id заказчика, id заказа, id поставщика (если тип заказа organization - то эти три поля не нужны), id )
    2. Создание (редактирование, удаление) нового маршрута (обязательные поля: car_id, driver_id)
    3. Получить точки по маршруту
    4. Получить расхождение между запланированным временем и фактическим
    5. Проверка минимального интервала времени (показывать предупреждение, если время между прибытием на точку меньше некоего стандартного, например, полчаса)
    */

    /**
     * @param $date
     * @return bool
     */
    private function checkRouteDateExists($date)
    {
        $this->setup();
        $check = $this->Delivery_Route->find("count",
            array(
                'conditions' => array(
                    'route_date' => $date,
                ),
            )
        );
        if ($check > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param int $car_id
     * @return bool
     */
    private function checkCarIsActive($car_id)
    {
        $car = $this->Car->getCarById($car_id);
        if ($car == null) {
            return false;
        }
        if ($car['Car']['status'] == 'blocked') {
            return false;
        }
        return true;
    }

    /**
     * @param int $driver_id
     * @return bool
     */
    private function checkDriverIsActive($driver_id)
    {
        $driver_id = $this->Driver->getCarById($driver_id);
        if ($driver_id == null) {
            return false;
        }
        if ($driver_id['Driver']['status'] == 'blocked') {
            return false;
        }
        return true;
    }

    /**
     * @param int $car_id
     * @param int $driver_id
     * @param DateTime $route_date
     * @return array
     */
    private function validateRoute($car_id, $driver_id, $route_date)
    {
        $car_id = (int)$car_id;
        if (empty($car_id) OR !is_int($car_id)) {
            return ["status" => "error", "error" => $this->required_car_id_integer_field];
        }
        if (empty($driver_id) OR !is_int($driver_id)) {
            return ["status" => "error", "error" => $this->required_driver_id_integer_field];
        }

        if (!$this->Validator->valid_date($route_date)) {
            return ["status" => "error", "error" => $this->required_route_date_as_date_field];
        }
        return ["status" => "success"];
    }

    /**
     * @param $car_id
     * @param $driver_id
     * @param $date
     * @param $comment
     * @return array
     */
    public function createRoute($car_id, $driver_id, $date, $comment)
    {
        $validation = $this->validateRoute($car_id, $driver_id, $date);
        if ($validation['status'] == "error") {
            return ["id" => null, "status" => "error", "error" => $validation["error"]];
        }
        if ($this->checkRouteDateExists($date)) {
            return ["id" => null, "status" => "error", "error" => $this->date_route_already_exists];
        }

        if ($this->checkCarIsActive($car_id)) {
            return ["id" => null, "status" => "error", "error" => $this->car_is_not_active_or_not_found];
        }
        if ($this->checkDriverIsActive($driver_id)) {
            return ["id" => null, "status" => "error", "error" => $this->driver_is_not_active_or_not_found];
        }

        // все хорошо, проверки пройдены, создем маршрут
        $route_point_id = $this->internalCreateRoute($car_id, $driver_id, $date, $comment);
        return ["id" => $route_point_id, "status" => "success"];
    }

    /**
     * @param $car_id
     * @param $driver_id
     * @param $route_date
     * @param $comment
     * @return mixed
     */
    private function internalCreateRoute($car_id, $driver_id, $route_date, $comment)
    {
        $this->setup();
        $data['car_id'] = $car_id;
        $data['driver_id'] = $driver_id;
        $data['route_date'] = $route_date;
        $data['comment'] = $comment;
        $this->Delivery_Route->create();
        $this->Delivery_Route->save($data);
        return $this->Delivery_Route->id;
    }

    /**
     * @param $type
     * @return bool
     */
    private function checkValidRoutePoint($type)
    {
        if (in_array($type, $this->valid_point_type)) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function validateRoutePointData($data)
    {
        if (empty($data['type'])) {
            return ["status" => "error", "error" => $this->unknown_delivery_route_point_type];
        }
        if (!$this->checkValidRoutePoint($data['type'])) {
            return ["status" => "error", "error" => $this->empty_delivery_route_point_type];
        }
        return ["status" => "success"];
    }

    /**
     * @param $route_id
     * @param $data
     * @return array
     */
    public function createRoutePoint($route_id, $data)
    {
        $validation = $this->validateRoutePointData($data);
        if ($validation['status'] == "error") {
            return ["id" => null, "status" => "error", "error" => $validation["error"]];
        }
        $new_point = [];
        $new_point['point_type'] = $data['type'];
        $new_point['plan_time '] = $data['plan_time'];
        $new_point['route_id'] = $route_id;
        $route_point_id = $this->internalCreateRoutePoint(null, $new_point);
        return ["id" => $route_point_id, "status" => "success"];
    }

    /**
     * @param $route_id
     * @param $data
     * @return array
     */
    public function saveRoutePoint($route_id, $data)
    {
        $validation = $this->validateRoutePointData($data);
        if ($validation['status'] == "error") {
            return ["id" => null, "status" => "error", "error" => $validation["error"]];
        }
        $route_point_id = $this->internalCreateRoutePoint($route_id, $data);
        return ["id" => $route_point_id, "status" => "success"];
    }

    /**
     * @param $route_id
     * @param $data
     * @return mixed
     */
    private function internalCreateRoutePoint($route_id, $data)
    {
        $this->setup();
        $data['route_id'] = $route_id;
        $this->Route_Point->create();
        $this->Route_Point->save($data);
        return $this->Route_Point->id;
    }

    // получение точек машрута
    // route_id, дата, order_status, route_type
    /**
     * @param int $count
     * @param array $filter
     * @return |null
     */
    public function getRoutePoints($count = 50, $filter = null)
    {
        $this->setup();
        if ($filter["route_id"] == null) {
            $route_id_array = [];
        } else {
            $route_id_array = ['Route_Point.route_id' => $filter['route_id']];
        }
        if (!key_exists("point_type", $filter) OR $filter["point_type"] == null) {
            $point_type_array = [];
        } else {
            $point_type_array = ['Route_Point.route_id' => $filter['point_type']];
        }
        if ($filter["date"] !== null) {
            $date_array = ['DATE(Route_Point.created)' => $filter['date']];
        } else {
            $date_array = ['DATE(Route_Point.created)' => $this->DateTime->now_date()];
        }
        if ($filter["address_id"] == null) {
            $address_array = [];
        } else {
            $address_array = ['Route_Point.address_id' => $filter['address_id']];
        }
        if ($filter["order_id"] == null) {
            $order_id_array = [];
        } else {
            $order_id_array = ['Route_Point.order_id' => $filter['order_id']];
        }

        $points = $this->Route_Point->find("all",
            array('conditions' =>
                array(
                    $order_id_array,
                    $route_id_array,
                    $point_type_array,
                    $date_array,
                    $address_array
                ),
                'joins' => array(
                    array(
                        'table' => 'delivery_routes',
                        'alias' => 'Delivery_Route',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Route_Point.route_id = Delivery_Route.id'
                        )
                    ),
                    array(
                        'table' => 'cars',
                        'alias' => 'Car',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Car.id = Delivery_Route.car_id'
                        )
                    ),
                    array(
                        'table' => 'drivers',
                        'alias' => 'Driver',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Driver.id = Delivery_Route.driver_id'
                        )
                    ),
                    array(
                        'table' => 'addresses',
                        'alias' => 'Address',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Address.id = Route_Point.address_id'
                        )
                    ),
                    array(
                        'table' => 'streets',
                        'alias' => 'Street',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Street.id = Address.street_id'
                        )
                    ),
                    array(
                        'table' => 'orderers',
                        'alias' => 'Orderer',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Orderer.id = Route_Point.orderer_id'
                        )
                    ),
                    array(
                        'table' => 'orders',
                        'alias' => 'Order',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Order.id = Route_Point.order_id'
                        )
                    ),
                    array(
                        'table' => 'stores',
                        'alias' => 'Company_Store',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Company_Store.id = Route_Point.company_store_id',
                        )
                    ),
                ),
                'fields' => array(
                    'Car.*',
                    'Company_Store.*',
                    'Route_Point.*',
                    'Order.*',
                    'Orderer.*',
                    'Address.*',
                    'Driver.*',
                    'Route_Point.*',
                    'Delivery_Route.*',
                    'Street.*'
                ),
                'order' => array('Route_Point.created ASC')
            )
        );
        if (count($points) == 0) {
            return null;
        }
        return $points;
    }

    /**
     * @param $route_id
     * @return string|null
     */
    public function getCourierById($route_id)
    {
        $this->setup();
        $route = $this->Delivery_Route->find("all",
            array(
                'conditions' => array(
                    'id' => $route_id,
                ),
            )
        );
        if ($route == 0) {
            return null;
        }
        foreach ($route as $route_data) {
            $route_item = $route_data['Delivery_Route'];
            $car_id = $route_item['car_id'];
            $car = $this->Car->getCarById($car_id);
            $driver_id = $route_item['driver_id'];
            $driver = $this->Driver->getDriverById($driver_id);
            $car_data = $car['Car'];
            $car_type_data = $car['Car_Type'];
            $driver_data = $driver['Driver'];

        }
        return $car_type_data['name'] . $car_data['max_weight'] . " кг (" . $car_data['car_number'] . " )" . $driver_data['lastname'] . mb_ucfirst($driver_data['firstname']) . " " . $driver_data['phone_number'];
    }

    /**
     * @param $date
     * @return array|null
     */
    public function getCouriersByDate($date)
    {
        $this->setup();
        $routes = $this->getRoutesByDate($date);
        $couriers = [];
        foreach ($routes as $route_data) {
            $route_item = $route_data['Delivery_Route'];
            $car_id = $route_item['car_id'];
            $car = $this->Car->getCarById($car_id);
            $driver_id = $route_item['driver_id'];
            $driver = $this->Driver->getDriverById($driver_id);
            $car_data = $car['Car'];
            $car_type_data = $car['Car_Type'];
            $driver_data = $driver['Driver'];
            $couriers[] = [
                'id' => $route_item['id'],
                'name' => $this->courierTitle(
                    $car_type_data['name'],
                    $car_data['max_weight'],
                    $car_data['car_number'],
                    $driver_data['lastname'],
                    $driver_data['firstname'],
                    $driver_data['phone_number']
                ),
            ];
        }
        return $couriers;
    }

    /**
     * @param $car_type_name
     * @param $car_max_weight
     * @param $car_number
     * @param $driver_firstname
     * @param $driver_lastname
     * @param $driver_phone
     * @return string
     */
    public function courierTitle($car_type_name, $car_max_weight, $car_number, $driver_firstname, $driver_lastname, $driver_phone)
    {
        return $car_type_name . " " . $car_max_weight . " кг (" . $car_number . " ) " . $driver_firstname . " " . mb_ucfirst($driver_lastname) . " " . $driver_phone;
    }

    /**
     * @param $date
     * @return |null
     */
    public function getRoutesByDate($date)
    {
        $this->setup();
        $routes = $this->Delivery_Route->find("all",
            array(
                'conditions' => array(
                    'route_date' => $date,
                ),
                'order' => array('id ASC')
            )
        );
        if ($routes == 0) {
            return null;
        }
        return $routes;
    }

    /**
     * @return |null
     */
    public function getAllRoutes()
    {
        $this->setup();
        $routes = $this->Delivery_Route->find("all",
            array(
                'conditions' => array(//'route_date' => $date,
                ),
                'order' => array('id ASC')
            )
        );
        if ($routes == 0) {
            return null;
        }
        return $routes;
    }


    /**
     * @param $point_id
     * @return array|null
     */
    public function getPointsByRouteId($route_id)
    {
        $this->setup();
        $points = $this->Point->find("all",
            array(
                'conditions' => array(
                    'route_id' => $route_id,
                ),
                'order' => array('id ASC')
            )
        );
        if ($points == 0) {
            return null;
        }
        return $points;
    }

    /**
     * @param null|$date
     * @return array
     */
    public function getOrdersByRouteDate($date = null)
    {
        $this->setup();
        if ($date !== null) {
            $routes = $this->getRoutesByDate($date);
        } else {
            $routes = $this->getAllRoutes();
        }
        $unique_orders = [];
        foreach ($routes as $route_data) {
            $route_id = $route_data['Delivery_Route']['id'];
            $points = $this->getPointsByRouteId($route_id);
            foreach ($points as $point) {
                $order_id = $point['Point']['order_id'];
                if ($order_id != 0 AND !in_array($order_id, $unique_orders)) {
                    $unique_orders[] = $order_id;
                }
            }
        }
        return $unique_orders;
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function getOrderLogisticParams($order_id){
        return $this->OrderCom->getOrderById($order_id);
        //return $params["places"] . " мест(а) " . $params["total_weight"] . "кг " . $params["total_volume"] . " куб.м";
    }

    /**
     * @param $data
     * @return string
     */
    public function getOrderLogisticData($data){
        return $data["places"] . " мест(а) " . $data["total_weight"] . "кг " . $data["total_volume"] . " куб.м";
    }

    /**
     * @param $data
     * @return string
     */
    public function getOrderLogisticPayment($data){
        if($data['payment_type'] == "site"){
            $payment_type = "оплачено";
        }
        elseif($data['payment_type'] == "provider_cash"){
            $payment_type = "нал.";
        } else {
            $payment_type = "по карте";
        }
        if($data['payment_status'] == "false"){
            return $data['order_cost'] . " (" . $payment_type . ")";
        } else {
            return $payment_type;
        }
    }
}
