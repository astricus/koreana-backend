<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Сервис коротких ссылок
 */
class ShortLinkComponent extends Component
{
    public $components = array(
        'Log'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    /**
     * @param $link
     * @return bool
     */
    public function isTerpikkaUrl($link){
        if(substr_count($link, "terpikka")>0){
            return true;
        }
        return false;
    }

    /**
     * @param $hash
     * @return mixed
     */
    private function checkHashExists($hash)
    {
        $modelName = "Shortlink";
        $this->Shortlink = ClassRegistry::init($modelName);
        return $this->Shortlink->find("count",
            array('conditions' =>
                array(
                    'hash' => $hash,
                ),
            )
        );

    }

    /**
     * @param $link
     * @return bool
     */
    private function saveShortlink($link)
    {
        $modelName = "Shortlink";
        $this->Shortlink = ClassRegistry::init($modelName);
        $try_hash = md5($link);
        $length = 8;
        while($this->checkHashExists($try_hash) >0) {
            $length++;
            $try_hash = substr($try_hash, 0, $length);

        }
        $new_link = ['link' => $link, "hash" => $try_hash];
        $this->Shortlink->save($new_link);
        if($this->Shortlink->id>0){
            return $try_hash;
        }
        return null;
    }

    /**
     * @param $link
     * @return bool|string
     */
    private function checkLinkExists($link)
    {
        $modelName = "Shortlink";
        $this->Shortlink = ClassRegistry::init($modelName);
        $link_data = $this->Shortlink->find("first",
            array('conditions' =>
                array(
                    'link' => $link,
                ),
            )
        );
        if (count($link_data) > 0) {
            return $link_data['Shortlink']['hash'];
        }
        return false;
    }

    /**
     * @param $hash
     * @return null|string
     */
    public function getShortlink($hash)
    {
        $modelName = "Shortlink";
        $this->Shortlink = ClassRegistry::init($modelName);
        if (!$this->checkHashExists($hash)) {
            return false;
        }
        $link_data = $this->Shortlink->find("first",
            array(
                'conditions' =>
                    array(
                        'hash' => $hash,
                    ),
            )
        );
        if (count($link_data) > 0) {
            return $link_data['Shortlink']['link'];
        }
        return null;
    }

    /**
     * @param $link
     * @return string
     */
    public function makeShortlink($link)
    {
        $shortlink = $this->checkLinkExists($link);
        if ($shortlink) {
            return $shortlink;
        }
        return $this->saveShortlink($link);
    }
}