<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');
App::uses('ApiSmsPilot', 'Lib'); // API smspilot.ru

class SmsComponent extends Component
{
    private $template = [
        'user_phone_confirm' => [
            'msg' => 'Ваш код подтверждения - %code_confirm%',
            'search' => ['%code_confirm%'],
            'replace' => ['code_confirm'],
        ],
    ];

    private $providers = [
        1 => 'ApiSmsPilot',
    ];

    public $SMS;

    public function __construct(ComponentCollection $collection, $settings = array())
    {
        parent::__construct($collection, $settings);
        $this->SMS = $this->connectApiSmsProvider(1);
    }

    public function sendSms($phone_number, $template, $data = '')
    {
        $text = $this->getMsg($template, $data);
        $res = $this->SMS->sendSms($phone_number, $text);
        return $res;
    }

    public function checkSms($number)
    {
        $this->SMS->checkSms($number);
    }

    public function getBalance()
    {
        return $this->SMS->getBalance();
    }

    public function connectApiSmsProvider($provider_id)
    {
        $api_class = $this->providers[$provider_id];
        if (class_exists($api_class)) {
            return new $this->providers[$provider_id];
        }
        return false;
    }

    private function getMsg($template, $data)
    {
        $template_data = $this->getTemplate($template);
        if ($template_data) {
            $str = $template_data['msg'];
            $i = 0;
            foreach ($template_data['replace'] as $v) {
                if (!empty($data[$v])) {
                    $str = str_replace($template_data['search'][$i], $data[$v], $str);
                } else {
                    return false;
                }
                $i++;
            }
            return $str;
        } else {
            return false;
        }
    }

    private function getTemplate($template)
    {
        if (!empty($this->template[$template])) {
            return $this->template[$template];
        } else {
            return false;
        }
    }

    public function send_sms($phone_number, $content){
        $sms_api_key = Configure::read('SMS_PILOT_API_KEY');
        //file_get_contents("http://smspilot.ru/api.php?send=test&to=79237854880&apikey=$sms_api_key");

        $sender = 'INFORM';//'Личный Кабинет Поставщика TERPIKKA'; //  имя отправителя из списка https://smspilot.ru/my-sender.php
        $url = 'https://smspilot.ru/api.php' .
            '?send=' . urlencode($content)
            . '&to=' . urlencode($phone_number)
            . '&from=' . $sender
            . '&apikey=' . $sms_api_key
            . '&format=json';
        $json = file_get_contents($url);
        //echo $json . '<br/>';
        // {"send":[{"server_id":"10000","phone":"79037672215","price":"1.68","status":"0"}],"balance":"11908.50","cost":"1.68"}
        $j = json_decode($json);
        if (!isset($j->error)) {
            $this->Log->add("SMS успешно отправлено, server_id=" . $j->send[0]->server_id . " Пользователь: " . $phone_number . " Содержимое: "  . $content, "warning");
            return true;
        } else {
            $this->Log->add("SMS не отправлено!" . " Пользователь: " . $phone_number . " Содержимое: " . $content, "warning");
            return false;
        }
    }
}