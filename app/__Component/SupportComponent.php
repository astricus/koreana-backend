<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Поддержка
 */
class SupportComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
      );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return mixed
     */
    public function getCompanyThemes()
    {
        $modelName = "Company_Ticket_Theme";
        $this->Company_Ticket_Theme = ClassRegistry::init($modelName);

        $themes = $this->Company_Ticket_Theme->find("all",
            array('conditions' =>
                array(
                    //'id' => $id
                ),
                'order' => array('name Asc')
            )
        );
        return $themes;
    }

    /**
     * @return mixed
     */
    public function getOrdererThemes()
    {
        $modelName = "Orderer_Ticket_Theme";
        $this->Orderer_Ticket_Theme = ClassRegistry::init($modelName);

        $themes = $this->Orderer_Ticket_Theme->find("all",
            array('conditions' =>
                array(
                    //'id' => $id
                ),
                'order' => array('name Asc')
            )
        );
        return $themes;
    }

    /**
     * @param $company_id
     * @return mixed
     */
    public function admin_answered_tickets_by_company($company_id)
    {
        $modelName = "Company_Ticket";
        $this->Company_Ticket = ClassRegistry::init($modelName);
        $tickets = $this->Company_Ticket->find("count",
            array('conditions' =>
                array(
                    'author_id' => $company_id,
                    'ticket_status' => 'admin_answered'
                ),
            )
        );
        return $tickets;
    }

    public function get_new_company_tickets()
    {
        $modelName = "Company_Ticket";
        $this->Company_Ticket = ClassRegistry::init($modelName);
        $tickets = $this->Company_Ticket->find("count",
            array('conditions' =>
                array(
                    'or' => array(
                        array(
                            'ticket_status' => "new",
                        ),
                        array(
                            'ticket_status' => "user_answered",
                        ),
                    ),
                )
            )
        );
        return $tickets;
    }


    /**
     * @param $orderer_id
     * @return mixed
     */
    public function admin_answered_tickets_by_orderer($orderer_id)
    {
        $modelName = "Orderer_Ticket";
        $this->Orderer_Ticket = ClassRegistry::init($modelName);
        $tickets = $this->Orderer_Ticket->find("count",
            array('conditions' =>
                array(
                    'author_id' => $orderer_id,
                    'ticket_status' => 'admin_answered'
                ),
            )
        );
        return $tickets;
    }

}