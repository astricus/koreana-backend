<?php

App::uses(
    'AppController', 'Controller'
);
App::uses('Model', 'Model');
App::uses('L10n', 'L10n');

/**
 * Компонент Поиск
 */
class SearchComponent extends Component
{
    public $components = array(
        'CompanyCom',
        'Country',
        'Error',
        'Log',
        'Manufacturer',
        'Moderate',
        'ProductCom',
        'Session',
        'UserCom',
        'Cacher',
    );

    public $symbol_switch_array = array(
        "а" => "f", "А" => "F",
        "б" => ",", "Б" => "<",
        "в" => "d", "В" => "D",
        "г" => "u", "Г" => "D",
        "д" => "l", "Д" => "L",
        "е" => "t", "Е" => "T",
        "ё" => "`", "Ё" => "~",
        "ж" => ";", "Ж" => ":",
        "з" => "p", "З" => "P",
        "и" => "b", "И" => "B",
        "й" => "q", "Й" => "Q",
        "к" => "r", "К" => "R",
        "л" => "k", "Л" => "K",
        "м" => "v", "М" => "V",
        "н" => "y", "Н" => "Y",
        "о" => "j", "О" => "J",
        "п" => "g", "П" => "G",
        "р" => "h", "Р" => "H",
        "с" => "c", "С" => "C",
        "т" => "n", "Т" => "N",
        "у" => "e", "У" => "E",
        "ф" => "a", "Ф" => "A",
        "х" => "[", "Х" => "{",
        "ц" => "w", "Ц" => "W",
        "ч" => "x", "Ч" => "X",
        "ш" => "i", "Ш" => "I",
        "щ" => "o", "Щ" => "O",
        "ъ" => "]", "Ъ" => "}",
        "ы" => "s", "Ы" => "S",
        "ь" => "m", "Ь" => "M",
        "э" => "'", "Э" => "\"",
        "ю" => ".", "Ю" => ">",
        "я" => "z", "Я" => "Z",
        "," => "?", "." => "/"
    );

    function setupModels()
    {
        $this->Product = ClassRegistry::init("Product");
        $this->ProductCategory = ClassRegistry::init("ProductCategory");
        $this->Shop_Product = ClassRegistry::init("Shop_Product");
        $this->Search = ClassRegistry::init("Search");
        $this->Search_Prefer = ClassRegistry::init("Search_Prefer");
        $this->Search_User = ClassRegistry::init("Search_User");
        $this->Search_Deep = ClassRegistry::init("Search_Deep");
    }

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
        $this->setupModels();
    }

    /**
     * @param $string
     * @return string
     */
    public function switchLanguageString($string)
    {
        $query_reverse = "";
        $query_reverse_arr = str_split_utf8($string);
        foreach ($query_reverse_arr as $sym) {
            if (!key_exists($sym, $this->symbol_switch_array)) {
                $key_rev = array_flip($this->symbol_switch_array);
                $query_reverse .= $key_rev[$sym];
            } else {
                $query_reverse .= $this->symbol_switch_array[$sym];
            }
        }
        return $query_reverse;
    }

    public function searchProductsByString($string)
    {
        $found_product_ids_by_param_values = $this->findParamValuesByString($string);
        pr($found_product_ids_by_param_values);
        return MockComponent::ARR;
    }

    /**
     * @param $file
     * @param $count
     * @param $search_id
     */
    private function saveSearchResultCount($file, $count, $search_id)
    {
        $this->setupModels();
        $update_record = [
            'cache_file' => $file,
            'found_elements' => $count,
            'cache_update' => now_date(),
        ];
        $this->Search->id = $search_id;
        $this->Search->save($update_record);
    }

    /**
     * @param $query
     * @return array
     */
    public function baseSearch($query)
    {
        $query = trim($query);
        $query_search_id = $this->createSearchQuery($query);
        $search_user_id = $this->createSearchUser($this->UserCom->getUserSessionId(), $this->UserCom->getuserId(), $query_search_id);

        //проверка наличия кеша глубокого поиска
        $deep_search_results = $this->checkDeepSearchData($query_search_id);
        if (is_array($deep_search_results)) {
            $cached_json_search_filename = $deep_search_results['Search_Deep']['cache_file'];
            $cached_json_search_data = $this->cache_filename($cached_json_search_filename);
            if (file_exists($cached_json_search_data)) {
                return json_decode(file_get_contents($cached_json_search_data));
            }
        }
        //в случае, если отсутствует файл с кешем поиска, запускается прямой поиск в БД
        $direct_products_found_by_product_data = $this->findProductIdsByFieldString($query);
        $direct_products_found_by_param_data = $this->findProductIdsByParamValue($query);

        $direct_brands_found_by_brand_name = $this->findBrandsByName($query);

        $direct_categories_found_by_name = $this->findCategoriesByName($query);

        $direct_products = array_merge($direct_products_found_by_product_data, $direct_products_found_by_param_data);
        /*
        foreach ($direct_products as $direct_product){
            pr($direct_product);
        }
        exit;*/

        $found_data = [];
        $found_data['products'] = $direct_products;
        $found_data['brands'] = $direct_brands_found_by_brand_name;
        $found_data['categories'] = $direct_categories_found_by_name;
        $found_data_json = json_encode($found_data);
        $found_elements = count($direct_products) + count($direct_brands_found_by_brand_name) + count($direct_categories_found_by_name);
        $this->saveSearchResultCount($found_data_json, $found_elements, $query_search_id);

        return $found_data;
    }

    // ELASTIC SEARCH
    public function findElasticBrandName($query)
    {
        $this->Brand = ClassRegistry::init("Brand");
        $founded_brands = $this->Brand->find("all",
            array('conditions' =>
                array(//'string' => $string
                ),
                'order' => array('difference ASC'),
                'fields' => array(
                    'LEVENSHTEIN (name, "' . $query . '") AS difference',
                    'id',
                    'name'
                ),
                'limit' => 10
            )
        );

        $founded_brands_arr = [];
        $min_dif_direct = 99;
        $min_dif_reverse = 99;
        foreach ($founded_brands as $founded_brand) {
            $difference = $founded_brand[0]['difference'];
            $founded_brand = $founded_brand['Brand'];
            $brand_id = $founded_brand['id'];
            $brand_name = $founded_brand['name'];

            if ($difference < $min_dif_direct) {
                $min_dif_direct = $difference;
            }
            if ($difference <= $this->percent_difference($query)) {
                $founded_brands_arr[] = [
                    'brand_id' => $brand_id,
                    'brand_name' => $brand_name,
                    'difference' => $difference
                ];
            }
        }

        // поиск при неправильной раскладе
        $query_reverse = $this->switchLanguageString($query);

        $founded_brands_reverse = $this->Brand->find("all",
            array('conditions' =>
                array(//'string' => $string
                ),
                'order' => array('difference ASC'),
                'fields' => array(
                    'LEVENSHTEIN (name, "' . $query_reverse . '") AS difference',
                    'id',
                    'name'
                ),
                'limit' => 10
            )
        );

        $founded_brands_rev_arr = [];

        foreach ($founded_brands_reverse as $founded_brand_reverse) {
            $difference = $founded_brand_reverse[0]['difference'];
            $founded_brand = $founded_brand_reverse['Brand'];
            $brand_id = $founded_brand['id'];
            $brand_name = $founded_brand['name'];
            if ($difference < $min_dif_reverse) {
                $min_dif_reverse = $difference;
            }
            if ($difference <= $this->percent_difference($query)) {
                $founded_brands_rev_arr[] = [
                    'brand_id' => $brand_id,
                    'brand_name' => $brand_name,
                    'difference' => $difference
                ];
            }
        }
        if ($min_dif_reverse < $min_dif_direct) {
            return $founded_brands_rev_arr;
        } else {
            return $founded_brands_arr;
        }
    }

    public function findElasticCategoryName($query)
    {
        $this->ProductCategory = ClassRegistry::init("ProductCategory");
        $founded_brands = $this->ProductCategory->find("all",
            array('conditions' =>
                array(//'string' => $string
                ),
                'order' => array('difference ASC'),
                'fields' => array(
                    'LEVENSHTEIN (name, "' . $query . '") AS difference',
                    'id',
                    'name'
                ),
                'limit' => 10
            )
        );

        $founded_brands_arr = [];
        $min_dif_direct = 99;
        $min_dif_reverse = 99;
        foreach ($founded_brands as $founded_brand) {
            $difference = $founded_brand[0]['difference'];
            $founded_brand = $founded_brand['ProductCategory'];
            $brand_id = $founded_brand['id'];
            $brand_name = $founded_brand['name'];

            if ($difference < $min_dif_direct) {
                $min_dif_direct = $difference;
            }
            if ($difference <= $this->percent_difference($query)) {
                $founded_brands_arr[] = [
                    'category_id' => $brand_id,
                    'category_name' => $brand_name,
                    'difference' => $difference
                ];
            }
        }

        // поиск при неправильной раскладе
        $query_reverse = $this->switchLanguageString($query);

        $founded_brands_reverse = $this->ProductCategory->find("all",
            array('conditions' =>
                array(//'string' => $string
                ),
                'order' => array('difference ASC'),
                'fields' => array(
                    'LEVENSHTEIN (name, "' . $query_reverse . '") AS difference',
                    'id',
                    'name'
                ),
                'limit' => 10
            )
        );

        $founded_brands_rev_arr = [];

        foreach ($founded_brands_reverse as $founded_brand_reverse) {
            $difference = $founded_brand_reverse[0]['difference'];
            $founded_brand = $founded_brand_reverse['ProductCategory'];
            $brand_id = $founded_brand['id'];
            $brand_name = $founded_brand['name'];
            if ($difference < $min_dif_reverse) {
                $min_dif_reverse = $difference;
            }
            if ($difference <= $this->percent_difference($query)) {
                $founded_brands_rev_arr[] = [
                    'category_id' => $brand_id,
                    'category_name' => $brand_name,
                    'difference' => $difference
                ];
            }
        }
        if ($min_dif_reverse < $min_dif_direct) {
            return $founded_brands_rev_arr;
        } else {
            return $founded_brands_arr;
        }
    }

    public function findElasticProductParamValue($query)
    {

        $this->Product_Category_Param_Value = ClassRegistry::init("ProductCategoryParamValues");
        $founded_items = $this->Product_Category_Param_Value->find("all",
            array('conditions' =>
                array(//'string' => $string
                ),
                'order' => array('difference ASC'),
                'fields' => array(
                    'LEVENSHTEIN (value, "' . $query . '") AS difference',
                    'id',
                    'value'
                ),
                'limit' => 10
            )
        );

        $founded_items_arr = [];
        $min_dif_direct = 99;
        $min_dif_reverse = 99;
        foreach ($founded_items as $founded_item) {
            $difference = $founded_item[0]['difference'];
            pr($founded_item);
            $founded_item = $founded_item['ProductCategoryParamValues'];
            $param_id = $founded_item['id'];
            $param_value = $founded_item['value'];

            if ($difference < $min_dif_direct) {
                $min_dif_direct = $difference;
            }
            if ($difference <= $this->percent_difference($query)) {
                $founded_items_arr[] = [
                    'param_id' => $param_id,
                    'param_value' => $param_value,
                    'difference' => $difference
                ];
            }
        }

        // поиск при неправильной раскладе
        $query_reverse = $this->switchLanguageString($query);

        $founded_items_reverse = $this->Product_Category_Param_Value->find("all",
            array('conditions' =>
                array(//'string' => $string
                ),
                'order' => array('difference ASC'),
                'fields' => array(
                    'LEVENSHTEIN (value, "' . $query_reverse . '") AS difference',
                    'id',
                    'value'
                ),
                'limit' => 10
            )
        );

        $founded_items_rev_arr = [];

        foreach ($founded_items_reverse as $founded_item_reverse) {
            $difference = $founded_item_reverse[0]['difference'];
            $founded_item = $founded_item_reverse['ProductCategoryParamValues'];
            $param_id = $founded_item['id'];
            $param_value = $founded_item['value'];
            if ($difference < $min_dif_reverse) {
                $min_dif_reverse = $difference;
            }
            if ($difference <= $this->percent_difference($query)) {
                $founded_items_rev_arr[] = [
                    'param_id' => $param_id,
                    'param_value' => $param_value,
                    'difference' => $difference
                ];
            }
        }
        if ($min_dif_reverse < $min_dif_direct) {
            return $founded_items_rev_arr;
        } else {
            return $founded_items_arr;
        }
    }



    //DIRECT SEARCH

    /**
     * @param $string
     * @return array
     */
    private function findProductIdsByFieldString($string)
    {
        $found_products_by_product_data_fields = $this->Product->find("all",
            array('conditions' =>
                array(
                    'or' => array(
                        array(
                            'product_name LIKE' => "%$string%",
                        ),
                        array(
                            'product_model LIKE' => "%$string%",
                        ),
                        array(
                            'prefix LIKE' => "%$string%",
                        ),
                        array(
                            'description LIKE' => "%$string%",
                        ),
                    ),
                ),
                'fields' => array(
                    'Product.id',
                ),
            )
        );
        $product_ids_arr = [];
        foreach ($found_products_by_product_data_fields as $product_ids_item) {
            $product_ids_arr[] = $product_ids_item['Product']['id'];
        }
        return $product_ids_arr;
    }

    /**
     * @param $query
     * @return array
     */
    private function findProductIdsByParamValue($query)
    {
        $this->Category_Product_Param_Value = ClassRegistry::init("Category_Product_Param_Value");
        $founded_items = $this->Category_Product_Param_Value->find("all",
            array('conditions' =>
                array(
                    'Category_Product_Param_Value.value LIKE' => "%$query%",
                ),
                'joins' => array(
                    array(
                        'table' => 'product_params',
                        'alias' => 'Product_Param',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Category_Product_Param_Value.id = Product_Param.value'
                        )
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Product_Param.product_id = Product.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Product.id',
                ),
            )
        );
        $product_ids_arr = [];
        foreach ($founded_items as $product_ids_item) {
            $product_ids_arr[] = $product_ids_item['Product']['id'];
        }
        return $product_ids_arr;
    }

    /**
     * @param $query
     * @return array
     */
    public function findBrandsByName($query)
    {
        $this->Brand = ClassRegistry::init("Brand");
        $founded_items = $this->Brand->find("all",
            array('conditions' =>
                array(
                    'name LIKE' => "%$query%",
                ),
                'fields' => array(
                    'id', 'name'
                ),
            )
        );
        $founded_items_arr = [];
        foreach ($founded_items as $founded_item) {
            $founded_items_arr[] = array('id' => $founded_item['Brand']['id'], 'name' => $founded_item['Brand']['name']);
        }
        return $founded_items_arr;
    }

    /**
     * @param $query
     * @return array
     */
    public function findCategoriesByName($query)
    {
        $this->ProductCategory = ClassRegistry::init("ProductCategory");
        $founded_items = $this->ProductCategory->find("all",
            array('conditions' =>
                array(
                    'name LIKE' => "%$query%",
                ),
                'fields' => array(
                    'id', 'name'
                ),
            )
        );
        $founded_items_arr = [];
        foreach ($founded_items as $founded_item) {
            $founded_items_arr[] = array('id' => $founded_item['ProductCategory']['id'], 'name' => $founded_item['ProductCategory']['name']);
        }
        return $founded_items_arr;
    }



    //MECHANIC
    /** Создание поисковой записи о пользователе */
    /**
     * @param $user_id
     * @param $orderer_id
     * @param $search_id
     * @return int
     */
    private function createSearchUser($user_id, $orderer_id, $search_id)
    {
        if ($orderer_id == null) {
            $orderer_id = 0;
        }
        $this->setupModels();
        $new_record = [
            'user_id' => $user_id,
            'orderer_id' => $orderer_id,
            'search_id' => $search_id,
        ];
        $this->Search_User->save($new_record);
        return $this->Search_User->id;
    }

    /** Создание поисковой записи о запросе
     * @param $user_id
     * @param $orderer_id
     * @return boolean
     */
    public function checkSearch($orderer_id, $user_id)
    {
        $this->setupModels();
        if ($orderer_id > 0) {
            $search_user_arr = array('orderer_id' => $orderer_id);
        } else {
            $search_user_arr = array('user_id' => $user_id);
        }
        $query = $this->Search_User->find("count",
            array('conditions' =>
                array(
                    $search_user_arr,
                    'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) <10'
                ),
                'fields' => array(
                    'Search_User.*',
                ),
            )
        );
        if ($query > 2) {
            return false;
        }
        return true;
    }

    /**
     * @param $query
     * @return integer|null
     */
    private function getQueryInSearchRecord($query)
    {
        $query = $this->Search->find("first",
            array('conditions' =>
                array(
                    'string' => $query
                ),
            )
        );
        return $query['Search']['id'] ?? null;
    }

    /** Создание поисковой записи о пользователе
     * @param $query
     * @return mixed
     */
    private function createSearchQuery($query)
    {
        // TODO query correct add
        $this->setupModels();
        $query_count = $this->Search->find("first",
            array('conditions' =>
                array(
                    'query' => $query
                ),
            )
        );
        if (count($query_count) > 0) {
            return $query_count['Search']['id'];
        }

        // проверка запроса на неправильность
        $query_correct_count = $this->Search->find("first",
            array('conditions' =>
                array(
                    'query_correct' => $query
                ),
            )
        );
        if (count($query_correct_count) > 0) {
            return $query_correct_count['Search']['id'];
        }

        $new_record = [
            'cache_file' => "",
            'query_correct' => "",
            'cache_update' => now_date(),
            'query' => $query,
            'found_elements' => 0,
        ];
        $this->Search->save($new_record);
        return $this->Search->id;
    }


    //DEEP SEARCH
    public function searchBigData()
    {
        $this->Search_User = ClassRegistry::init("Search_User");
        $most_wanted_search_id_list = [];
        $most_wanted_queries = $this->Search_User->find("all",
            array('conditions' =>
                array(//$search_user_arr
                ),
                'fields' => array(
                    'search_id'
                ),
                'group' => 'search_id',
                'order' => array('count(search_id) DESC'),
                'limit' => 5
            )
        );
        $this->Search_Deep = ClassRegistry::init("Search_Deep");
        foreach ($most_wanted_queries as $most_wanted_query) {
            $most_wanted_search_id = $most_wanted_query['Search_User']['search_id'];
            /*
             * если существует search_deep - проверяем актуальность кеша и перезапускаем поиск, если не существует - создаем и запускаем поиск
             *
             * */

            $check_search_deep = $this->Search_Deep->find("first",
                array('conditions' =>
                    array(
                        'search_id' => $most_wanted_search_id,
                        "status !=" => 'active'
                    ),
                )
            );

            if (count($check_search_deep) == 0) {
                // отсутствует задание на глубокий поиск - создаем и добавляем
                $new_deep_search = [
                    'search_id' => $most_wanted_search_id,
                    'cache_file' => "",
                    'cache_update' => now_date(),
                    'found_elements' => 0,
                    'status' => 'init'
                ];
                $this->Search_Deep->create();
                $this->Search_Deep->save($new_deep_search);
                $deep_search_id = $this->Search_Deep->id;
                $this->initDeepSpace($deep_search_id);
            } else {
                $update_timestamp = strtotime($check_search_deep['Search_Deep']['cache_update']);
                // если ничего не найдено - запускаем глубокий поиск
                if ($check_search_deep['Search_Deep']['found_elements'] == 0
                    && $check_search_deep['Search_Deep']['status'] == 'success'
                    && time() - $update_timestamp > 86400
                ) {
                    $this->initDeepSpace($most_wanted_search_id);
                }
                // если результаты устарели (пока по умолчанию - срок кеширования месяц)
                $update_cache_interval = 30 * 86400;
                if (time() - $update_timestamp > $update_cache_interval) {
                    $this->initDeepSpace($check_search_deep['Search_Deep']['id']);
                }
            }
        }
        pr($most_wanted_queries);
        exit;
    }

    /**
     * @param $deep_search_id
     * @return mixed
     */
    /*глубокий поиск по БД для данного поискового запроса */
    private function initDeepSpace($deep_search_id)
    {
        $this->setupModels();
        //поиск активирован, защита от повторной активации в процессе поиска
        $this->updateDeepSearch("active", $deep_search_id);

        $search_id = $this->getSearchIdByDeepSearchId($deep_search_id);
        $query = $this->getQueryBySearchId($search_id);
        $elastic_brands_found = $this->findElasticBrandName($query);
        $elastic_category_found = $this->findElasticCategoryName($query);
        $elastic_product_param = $this->findElasticProductParamValue($query);
        $found_data = [];
        $found_data['products'] = $elastic_product_param;
        $found_data['brands'] = $elastic_brands_found;
        $found_data['categories'] = $elastic_category_found;
        $found_data_json = json_encode($found_data);
        $found_elements = count($elastic_product_param) + count($elastic_brands_found) + count($elastic_category_found);
        // сохранение результатов поиска
        $this->saveDeepSearchResult($found_data_json, $found_elements, $deep_search_id, $search_id);
    }

    /**
     * @param $status
     * @param $deep_search_id
     */
    private function updateDeepSearch($status, $deep_search_id)
    {
        $update_record = [
            'status' => $status
        ];
        $this->Search_Deep->id = $deep_search_id;
        $this->Search_Deep->save($update_record);
    }

    /**
     * @param $search_id
     * @return mixed
     */
    private function getQueryBySearchId($search_id)
    {
        $getQueryField = $this->Search->find("first",
            array('conditions' =>
                array(
                    'id' => $search_id
                ),
            )
        );
        if (count($getQueryField) > 0) {
            return $getQueryField['Search']['query'];
        } else {
            die("unknown search identifier");
        }
    }

    /**
     * @param $search_id
     * @return mixed
     */
    private function getDeepSearchIdBySearchId($search_id)
    {
        $getData = $this->Search_Deep->find("first",
            array('conditions' =>
                array(
                    'search_id' => $search_id
                ),
            )
        );
        return $getData['Search_Deep']['id'];
    }

    /**
     * @param $deep_search_id
     * @return mixed
     */
    private function getSearchIdByDeepSearchId($deep_search_id)
    {
        $getData = $this->Search_Deep->find("first",
            array('conditions' =>
                array(
                    'id' => $deep_search_id
                ),
            )
        );
        return $getData['Search_Deep']['search_id'];
    }

    /**
     * @param $file
     * @param $count
     * @param $search_id
     */
    private function saveDeepSearchResult($file, $count, $deep_search_id, $search_id)
    {
        $hash_cash_file = $this->deepSearchRoute($this->getQueryBySearchId($search_id));
        $update_record = [
            'cache_file' => $hash_cash_file,
            'found_elements' => $count,
            'search_id' => $search_id,
            'cache_update' => now_date(),
            'status' => 'success'
        ];

        $this->Cacher->set_json_cache($hash_cash_file, $file);
        $this->Search_Deep->id = $deep_search_id;
        $this->Search_Deep->save($update_record);
    }


    private function deepSearchRoute($query)
    {
        return md5("DEEP_SEARCH" . $query);
    }

    public function cache_filename($hash)
    {
        $cache_dir = Configure::read("JSON_CACHE_DIR");
        if (!is_dir($cache_dir)) {
            mkdir($cache_dir, 0777);
        }
        return $cache_dir . DS . $hash . ".json";
    }

    private function percent_difference($query)
    {
        $query_len = mb_strlen($query);
        if ($query_len > 10) {
            return 4;
        }
        if ($query_len <= 10 && $query_len > 7) {
            return 3;
        }
        if ($query_len < 7) {
            return 2;
        }
    }

    private function checkDeepSearchData($search_id)
    {
        $results = $this->Search_Deep->find("first",
            array('conditions' =>
                array(
                    'search_id' => $search_id
                ),
            )
        );
        if (count($results) == 0) {
            return false;
        }
        return $results;
    }


    //Collect search prefers

    /**
     * @param $search_id
     * @return bool
     */
    private function checkSearchIdExists($search_id)
    {
        $search_id_count = $this->Search->find("count",
            array('conditions' =>
                array(
                    'id' => $search_id
                ),
            )
        );
        if ($search_id_count > 0) {
            return true;
        }
        return false;
    }

    public function collectSearchPrefer($search_id, $user_id, $element_type, $element_id)
    {
        if (!$this->checkSearchIdExists($search_id)) {
            return null;
        }
        if ($element_type == "product") {
            $product_by_id = $this->ProductCom->getProductById($element_id);
            if ($product_by_id == null) {
                return null;
            }
        }

        if ($element_type == "category") {
            $category_by_id = $this->ProductCom->getCategoryById($element_id);
            if ($category_by_id == null) {
                return null;
            }
        }

        if ($element_type == "brand") {
            $brand_by_id = $this->ProductCom->getBrandById($element_id);
            if ($brand_by_id == null) {
                return null;
            }
        }
        //проверка наличия данного предпочтения для данного пользователя по данному поисковому запросу, чтобы не было
        //случайных или умышленных накруток
        $this->setupModels();
        $found_prefers = $this->Search_Prefer->find("count",
            array('conditions' =>
                array(
                    'search_id' => $search_id,
                    'user_id' => $user_id,
                    'element_type' => $element_type,
                    'element_id' => $element_id,
                ),
            )
        );

        if ($found_prefers > 0) {
            return null;
        }
        $prefer = [
            'search_id' => $search_id,
            'user_id' => $user_id,
            'element_type' => $element_type,
            'element_id' => $element_id,
        ];

        $this->Search_Prefer->save($prefer);
        return $this->Search_Prefer->id;
    }

    /*
        public function searchProductsByString($string)
        {
            $found_product_ids_by_param_values = $this->findParamValuesByString($string);
            pr($found_product_ids_by_param_values);


            return array();
        }*/
    //pr($this->Product->query('SELECT LEVENSHTEIN(`name`, "BOSH") as differrence, name, id FROM Brands ORDER BY differrence ASC LIMIT 10'));

}