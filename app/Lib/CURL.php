<?php
class CURL
{
    private $ch;

    public function __construct()
    {
        $this->ch =  curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_VERBOSE, 1);
        curl_setopt($this->ch, CURLOPT_HEADER, 1);
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2');
    }

    public function getDataCURL($url, $data = [], $method = 'get')
    {
        if($method == 'get'){
            $url = $url.'?'.urldecode(http_build_query($data));
            curl_setopt($this->ch, CURLOPT_URL, $url);
            return $this->execute();
        }
        else{

        }

    }

    private function execute()
    {
        $result = curl_exec($this->ch);
        $header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
        //$header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);
        $httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        if($httpCode == 200){
            return $body;
        }
        else{
            return false;
        }
        curl_close($this->ch);
    }
}