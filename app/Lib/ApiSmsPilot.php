<?php
App::uses('CURL', 'Lib');

class ApiSmsPilot extends CURL
{
    private $apiKey;
    private $url = 'http://smspilot.ru/api.php';

    public function __construct()
    {
        $this->apiKey = Configure::read('SMS_PILOT_API_KEY');
        parent::__construct();
    }

    public function sendSms($phone_number, $text)
    {
        $data = [
            'send' => $text,
            'to' => $phone_number,
            'apikey' => $this->apiKey,
            'format' => 'json',
        ];

        $res = $this->getDataCURL($this->url, $data);

        if($res){
            $res = json_decode($res, true);

            if(!empty($res['error'])){
                return [
                    'status' => 'error',
                    'msg'    => $res['error']['description_ru'],
                ];
            }
            elseif(!empty($res['send'])){
                return [
                    'status' => 'ok',
                    'sms_id' => $res['send'][0]['server_id'],
                    'phone'  => $res['send'][0]['phone'],
                    'price'  => $res['send'][0]['price'],
                    'status_sms' => $res['send'][0]['status'],
                ];
            }
            else{
                return [
                    'status' => 'error',
                    'msg'    => 'Неизвестная ошибка',
                ];
            }
        }
        else{
            return [
                'status' => 'error',
                'msg'    => 'Оператор не отдал информацию о отправки sms',
            ];
        }
        //{"send":[{"server_id":"153147149","phone":"79081234567","price":"1.68","status":"0"}],"balance":"11908.50","cost":"1.68"}
        //{"error":{"code":"223","description":"Spam protection","description_ru":"Защита от спама (исп. шаблоны или добавьте контакт в белый список или подкл. как бизнес-клиент)"}}
    }

    public function checkSms($id)
    {
        $data = [
            'check' => $id,
            'apikey' => $this->apiKey,
            'format' => 'json',
        ];

        $res = $this->getDataCURL($this->url, $data);
        pr($res);
    }

    /**  Возвращает баланс аккаунта
     * @return bool | float
     */
    public function getBalance()
    {
        $data = [
            'balance' => date('T',time()),
            'apikey' => $this->apiKey,
            'format' => 'json',
        ];

        $res = $this->getDataCURL($this->url, $data);
        $res = json_decode($res, true);

        if(!empty($res['balance'])){
            return $res['balance'];
        }
        else{
            return false;
        }
    }
}