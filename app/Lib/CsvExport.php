<?php

class CsvExport
{
    public $delimiter = ',';
    public $enclosure = '"';
    public $filename = 'Export.csv';
    public $line = array();
    public $buffer;

    public function __construct()
    {
        $this->clear();
    }

    /**
     * @return void
     */
    function clear()
    {
        $this->line = array();
        $this->buffer = fopen('php://temp/maxmemory:' . (5 * 1024 * 1024), 'r+');
    }

    /**
     * @param $value
     * @return void
     */
    public function addField($value)
    {
        $this->line[] = $value;
    }

    /**
     * @return void
     */
    public function endRow()
    {
        $this->addRow($this->line);
        $this->line = array();
    }

    /**
     * @param $row
     * @return void
     */
    public function addRow($row)
    {
        fputcsv($this->buffer, $row, $this->delimiter, $this->enclosure);
    }

    function renderHeaders()
    {
        //ob_start();
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=UTF-8');
        header("Content-type:application/vnd.ms-excel");
        header('Content-Disposition: attachment; filename="' . $this->filename . '"');
        header("Pragma: no-cache");
        header("Expires: 0");
        //ob_end_clean();
    }

    /**
     * @param $filename
     * @return void
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        if (strtolower(substr($this->filename, -4)) != '.csv') {
            $this->filename .= '.csv';
        }
    }

    /**
     * @param bool $outputHeaders
     * @param $to_encoding
     * @param $from_encoding
     * @return array|false|string
     */
    public function render(bool $outputHeaders = true, $to_encoding = null, $from_encoding = "auto")
    {
        if ($outputHeaders) {
            if (is_string($outputHeaders)) {
                $this->setFilename($outputHeaders);
            }
            $this->renderHeaders();
        }
        rewind($this->buffer);
        $output = stream_get_contents($this->buffer);

        if ($to_encoding) {
            $output = mb_convert_encoding($output, $to_encoding, $from_encoding);
        }

        return $output;
    }
}