<?php
App::uses('CURL', 'Lib');

class ApiPek extends CURL
{
    public $IS_TEST; // флаг тестирования (0 - работа, 1 - тест)

    public function __construct()
    {
        $this->IS_TEST = 0;
        parent::__construct();
    }

    public function getCities()
    {
        $url = 'https://pecom.ru/ru/calc/towns.php';
        return json_decode($this->getDataCURL($url), JSON_OBJECT_AS_ARRAY);
    }

    public function getCostDeliveryCompanyToStore($d)
    {
        $url = 'http://calc.pecom.ru/bitrix/components/pecom/calc/ajax.php';
        $places = [];
        foreach ($d['products'] as $val) {
            $weight = 0;
            foreach ($val as $v) {
                $weight += $v['amount'] * $v['weight'];
            }
            $places[] = [0, 0, 0, 0, $weight];
        }
        $data = [
            'places' => $places,
            'take' => [
                'town' => '-463',
            ],
            'deliver' => [
                'town' => '-463',//-446 Москва, -58740 НН, -466 Воронеж
            ],
            'strah' => $d['total_price'],
        ];
        $res = json_decode($this->getDataCURL($url, $data), JSON_OBJECT_AS_ARRAY);

        return $this->dataCostDeliveryStructure($res);
    }

    public function getCostDelivery($d)
    {
        $url = 'http://calc.pecom.ru/bitrix/components/pecom/calc/ajax.php';
        $data = [
            'places' => [
                [
                    isset($d['width']) ? $d['width'] : 0, // Ширина, м
                    isset($d['length']) ? $d['length'] : 0, // Длина, м
                    isset($d['height']) ? $d['height'] : 0, // Высота, м
                    isset($d['volume']) ? $d['volume'] : 0, // Объем, м3
                    isset($d['weight']) ? $d['weight'] : 1, // Вес, кг
                    //0, // Признак негабаритности груза
                    //0, // Признак ЗУ
                ],
            ],
            'take' => [
                'town' => $d['sender']['location']['cityID'], // ID города забора
                //'tent'   => 0, // требуется растентровка при заборе
                //'gidro'  => 0, // требуется гидролифт при заборе
                //'manip'  => 0, // требуется манипулятор при заборе
                //'speed'  => 0, // Срочный забор (только для Москвы)
                //'moscow' => 0, // Без въезда, МОЖД, ТТК, Садовое.
            ],
            'deliver' => [
                'town' => $d['recipient']['location']['cityID'], // ID города доставки
                //'tent'   => 0, // Требуется растентровка при доставке
                //'gidro'  => 0, // Требуется гидролифт при доставке
                //'manip'  => 0, // Требуется манипулятор при доставке
                //'speed'  => 0, // Срочная доставка (только для Москвы)
                //'moscow' => 0, // Без въезда, МОЖД, ТТК, Садовое.
            ],
            //'plombir' => 0, // Количество пломб
            'strah' => isset($d['insurance']) ? $d['insurance'] : 0, // Величина страховки
            //'ashan'   => 0, // Доставка в Ашан
            //'night'   => 0, // Забор в ночное время
            //'pal'     => 0, // Требуется запаллечивание груза (0 - не требуется, значение больше нуля - количество паллет)
            //'pallets' => 0, // Кол-во паллет для расчет услуги паллетной перевозки (только там, где эта услуга предоставляется)
        ];

        $res = json_decode($this->getDataCURL($url, $data), JSON_OBJECT_AS_ARRAY);
        return $this->dataCostDeliveryStructure($res);
    }

    private function dataCostDeliveryStructure($data)
    {
        //pr($data);
        $cost = 0;
        $zg = 0; // забор гуза
        $dg = 0; // доствака до клиента
        $insurance = 0;
        $detail = [];
        // обработка на складе или магистральная перевозка
        if (isset($data['auto'][2])) {
            $cost += $data['auto'][2];
            $detail[] = [
                'name' => trim(strip_tags($data['auto'][0])),
                'cost' => $data['auto'][2],
            ];
        }
        // стоимость забора груза
        if (isset($data['take'][2])) {
            $zg = $data['take'][2];
            $cost += $data['take'][2];
            $detail[] = [
                'name' => trim($data['take'][0]),
                'cost' => $data['take'][2],
            ];
        }
        // стоимость доставки груза
        if (isset($data['deliver'][2])) {
            $dg = $data['deliver'][2];
            $cost += $data['deliver'][2];
            $detail[] = [
                'name' => trim($data['deliver'][0]),
                'cost' => $data['deliver'][2],
            ];
        }
        // страховка
        if (isset($data['ADD_3'][2])) {
            $insurance = $data['ADD_3'][2];
            $cost += $data['ADD_3'][2];
            $detail[] = [
                'name' => trim($data['ADD_3'][1]),
                'cost' => $data['ADD_3'][2],
            ];
        }

        $result = [
            /*'zg'            => $zg,
            'dg'            => $dg,*/
            'cost_delivery' => $zg + $dg,
            'insurance'     => $insurance,
            'total_cost'    => $cost,
            'detail_cost'   => $detail,
            'period' => [
                'min' => isset($data['periods_days']) ? $data['periods_days'] : 1,
                'max' => isset($data['periods_days']) ? $data['periods_days'] : 1,
            ],
        ];
        //pr($result);
        return $result;
    }
}