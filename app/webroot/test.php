<?PHP
function sendMessage()
{
    $content      = [
        "en" => 'English Message',
    ];
    $hashes_array = [];
    array_push(
        $hashes_array,
        [
            "id"   => "like-button",
            "text" => "Like",
            "icon" => "http://i.imgur.com/N8SN8ZS.png",
            "url"  => "https://yoursite.com",
        ]
    );
    array_push(
        $hashes_array,
        [
            "id"   => "like-button-2",
            "text" => "Like2",
            "icon" => "http://i.imgur.com/N8SN8ZS.png",
            "url"  => "https://yoursite.com",
        ]
    );
    $fields = [
        'app_id'            => "5eb5a37e-b458-11e3-ac11-000c2940e62c",
        'included_segments' => [
            'Subscribed Users',
        ],
        'data'              => [
            "foo" => "bar",
        ],
        'contents'          => $content,
        'web_buttons'       => $hashes_array,
    ];

    $fields = json_encode($fields);
    print("\nJSON sent:\n");
    print($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        [
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj',
        ]
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}

$response               = sendMessage();
$return["allresponses"] = $response;
$return                 = json_encode($return);

$data = json_decode($response, true);
print_r($data);
$id = $data['id'];
print_r($id);

print("\n\nJSON received:\n");
print($return);
print("\n");
?>

