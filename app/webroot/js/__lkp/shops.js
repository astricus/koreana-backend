$(document).ready(function(){
    var city_container = $("#city_id");
    $(document).on("change", "#select_region_id", function () {
        $(".cities_container_box").hide();
        var selected_region_id = $(this).val();
        var transfer_data = "region_id=" + selected_region_id;
        $.ajax({
            url: "/search/find_cities_by_region/"+ selected_region_id,
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data.data);
                city_container.html("");
                $(".cities_container_box").show();

                if (data.data.status == "error") {
                }
                else {
                    city_container.append('<option value="">город магазина</option>');
                    for (var i = 0; i < data.data.length; i++) {
                        console.log(data.data[i].City.name);

                        var new_elem = $('<option value="' + data.data[i].City.id + '">' + data.data[i].City.name + '</option>');
                        city_container.append(new_elem);
                    }
                }
            }
        });
    });
});