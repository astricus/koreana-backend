$(document).ready(function(){

    if($(".order_container").length>0) {

        /* город */
        $(document).on("change", ".dropdownOrdererCitySearch", function () {
            var city_id = $('.dropdownOrdererCitySearch option:selected').val(), new_url;
            if (city_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
            }
            redirect(new_url);
        });

        /* магазин */
        $(document).on("change", ".dropdownShopSearch", function () {
            var selected_shop_id = $('.dropdownShopSearch option:selected').val(), new_url;
            if (selected_shop_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'shop_id', selected_shop_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'shop_id', '');
            }
            redirect(new_url);
        });

        /* заказчик */
        $(document).on("change", ".dropdownOrdererSearch", function () {
            var selected_shop_orderer_id = $('.dropdownOrdererSearch option:selected').val(), new_url;
            if (selected_shop_orderer_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'orderer_id', selected_shop_orderer_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'orderer_id', '');
            }
            redirect(new_url);
        });

        /* статус*/
        $(document).on("change", ".dropdownOrderStatus", function (evt) {
            evt.preventDefault();
            var selected_order_status = $('.dropdownOrderStatus option:selected').val(), new_url;
            if (selected_order_status != undefined) {
                new_url = updateQueryStringParameter(window.location.href, 'status', selected_order_status);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'status', '');
            }
            redirect(new_url);
        });

        /* обработчик смены статуса доставки заказа*/
        var delivery_resiult;

        if ($("#sw-sz-lg").length > 0) {
            var changeCheckbox = document.getElementById('sw-sz-lg');
            new Switchery(changeCheckbox, {size: 'large'});
            changeCheckbox.onchange = function () {
                if (changeCheckbox.checked) {
                    $(".delivery_status_accept_text").show();
                    $(".delivery_status_reject_text").hide();
                } else {
                    delivery_resiult = 'доставка не требуется';
                    $(".delivery_status_accept_text").hide();
                    $(".delivery_status_reject_text").show();
                }
            };
        }
    }
});