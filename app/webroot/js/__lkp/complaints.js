$(document).ready(function () {

    if($(".complaint_container").length>0) {

        $(".use_select2").select2();

        $(document).on("change", ".type_filter", function () {
            var state = $(this).is(':checked') ? 'on' : '';

            var type_filter = $(this).attr('data-type_filter');
            var active_only, new_only, new_url, type_filter_value;
            if (type_filter == "new_only") {
                type_filter_value = "new_only";
                new_url = updateQueryStringParameter(window.location.href, type_filter_value, state);
            } else if (type_filter == "active_only") {
                type_filter_value = "active_only";
                new_url = updateQueryStringParameter(window.location.href, type_filter_value, state);
            }

            var stateObj = {foo: "bar"};
            history.pushState(stateObj, "", new_url);
            // стартовая загрузка страницы клиентов

            active_only = getParameterByName("active_only");
            new_only = getParameterByName("new_only");

            var sort_type = getParameterByName("sort_type");
            var sort_dir = getParameterByName("sort_dir");
            var page = getParameterByName("page");
            var shop_id = getParameterByName("shop_id");
            var orderer_id = getParameterByName("orderer_id");

            var params = {
                'sort_type': sort_type,
                'sort_dir': sort_dir,
                'page': page,
                'orderer_id': orderer_id,
                'shop_id': shop_id,
                'active_only': active_only,
                'new_only': new_only,
            };

            var query_string = $.param(params);
            reload_complaint_container(query_string);
            return false;

        });

        $(document).on("click", ".sort_complaints_list", function (e) {
            e.preventDefault();
            var current_sorter = $(this);
            var icon_down = $(this).attr("data-down-icon");
            var icon_up = $(this).attr("data-up-icon");
            var sort_type = $(this).attr("data-sort");
            var sort_status = $(this).attr("data-sort_status");
            var sort_direction = $(this).attr("data-dir");

            if (sort_status == "on") {
                if (sort_direction == "asc") {
                    $(this).attr('data-dir', "desc");
                    $(this).find(".sort_icon").removeClass(icon_up).addClass(icon_down);
                } else if (sort_direction == "desc") {
                    $(this).attr('data-dir', "asc");
                    $(this).find(".sort_icon").removeClass(icon_down).addClass(icon_up);
                }
            }

            $(".sort_complaints_list").each(function () {
                if ($(this).attr('data-sort') == sort_type) {
                    $(this).removeClass("text-muted");
                    $(this).attr('data-sort_status', "on");
                } else {
                    $(this).addClass("text-muted");
                    $(this).attr('data-sort_status', "off");
                }
            });

            showComplaintTableLoader();

            var new_url = updateQueryStringParameter(window.location.href, 'sort_dir', sort_direction);
            new_url = updateQueryStringParameter(new_url, 'sort_type', sort_type);

            var stateObj = {foo: "bar"};
            history.pushState(stateObj, "", new_url);

            var params = {
                'sort_type': sort_type,
                'sort_dir': sort_direction,
                'page': page,
                'orderer_id': orderer_id,
                'shop_id': shop_id,
            };

            var query_string = $.param(params);
            reload_complaint_container(query_string);
            return false;
        });

        var loader = $('<div class="load5 product_loader"><div class="loader"></div></div>');

        var notFoundBlock = $('<div class="well">По вашему запросу ничего не удалось найти.</div>');

        var complaint_container = $(".complaint_container");

        var showComplaintTableLoader = function () {
            complaint_container.html(loader);
        };

        var showComplaintData = function (content) {
            complaint_container.html(content);
        };

        var showComplaintTableNotFound = function () {
            complaint_container.html(notFoundBlock);
        };

        var reload_complaint_container = function (query_string) {

            var reload_timer;
            var transfer_data = query_string;
            $.ajax({
                url: "/complaint/ajax/",
                type: "post",
                dataType: 'html',
                data: transfer_data,
                beforeSend: function () {
                    showComplaintTableLoader();
                    clearInterval(reload_timer);
                },
                success: function (data) {
                    reload_timer = setInterval(showComplaintData(data), 4000);
                }
            });
        };
        // стартовая загрузка страницы клиентов
        var sort_type = getParameterByName("sort_type");
        var sort_dir = getParameterByName("sort_dir");
        var page = getParameterByName("page");
        var shop_id = getParameterByName("shop_id");
        var orderer_id = getParameterByName("orderer_id");
        var active_only = getParameterByName("active_only");
        var new_only = getParameterByName("new_only");

        var params = {
            'sort_type': sort_type,
            'sort_dir': sort_dir,
            'page': page,
            'shop_id': shop_id,
            'orderer_id': orderer_id,
            'active_only': active_only,
            'new_only': new_only,
        };

        var query_string = $.param(params);
        setTimeout(reload_complaint_container(query_string), 50);

        /* магазин */
        $(document).on("change", ".dropdownOrdererShopSearch", function () {
            var shop_id = $('.dropdownOrdererShopSearch option:selected').val(), new_url;
            if (shop_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'shop_id', shop_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'shop_id', '');
            }
            redirect(new_url);
        });

        /* фильтр по клиенту */
        $(document).on("change", ".dropdownOrdererSelect", function () {
            var orderer_id = $('.dropdownOrdererSelect option:selected').val(), new_url;
            if (orderer_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'orderer_id', orderer_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'orderer_id', '');
            }
            redirect(new_url);
        });
    }

});