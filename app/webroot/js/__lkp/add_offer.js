$(document).ready(function () {

    var add_region_price_element = $('.add_region_item');
    $(document).on("click", ".add_region_action", function () {
        console.log('add_region_action');
        var element_content = add_region_price_element.html();
        $(".add_region_price_container").append("<div class='margin-top: 10px'>" + element_content + "</div>");
    });

    var handleOutsideClick = function (e) {
        var $target = $(e.target);
        if ($target.hasClass("found_product_field") === true || $target.closest(".found_product_container") ===true ) {
            e.preventDefault();
            e.stopPropagation();
            return;
        }
        hideSearchResult();
    };

    var showSearchResult = function() {
        var container = $(".found_product_container");
        container.show();
        $(document).on("click", handleOutsideClick);
    };

    var hideSearchResult = function() {
        var container = $(".found_product_container");
        container.hide();
        $(document).off("click", handleOutsideClick);
    };

    var lastSearchStr = null;

    var product_found_delay = function (string) {
        if (lastSearchStr == string) {
            return false;
        }
        var transfer_data = "product_name=" + string;
        $.ajax({
            url: "/product/find_product_by_name",
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function (){
                $(".found_product_container").hide();
            },
            success: function (data) {
                $(".found_product_container").html('');

                if (data.data.status == "error") {
                } else {
                    if (data.data.length > 0) {
                        for (var i = 0; i < data.data.length; i++) {
                            //found_product_field
                            var new_elem = $('<a class="found_product_item" data-id="' + data.data[i].Product.id + '" data-image ="' + data.data[i].image + '"' +
                                '  data-name = "' + data.data[i].Product.product_name + '"><img height="50px" src="' + data.data[i].image + '" ' +
                                'class="found_product_item_image" alt="' + data.data[i].Product.product_name + '"/>&nbsp;&nbsp;<span class="text">' + data.data[i].Product.product_name + '</span></a>');
                            $(".found_product_container").append(new_elem);
                        }

                        showSearchResult();
                    } else {
                        hideSearchResult();
                    }
                }

            }
        });
        lastSearchStr = string;
    };

    var timeoutId = null;

    $(document).on("keyup", ".found_product_field", function () {
        var last_string = "";
        var string = $(this).val();

        if (string.length <= 2) {
            hideSearchResult();
            return false;
        }

        showSearchResult();

        if (timeoutId != null) {
            clearTimeout(timeoutId);
        }

        timeoutId = setTimeout(product_found_delay(string, last_string), 500);
    });

    $(document).on("focus", ".found_product_field", function (e) {
        var str = $(".found_product_field").val();

        if (str.length > 2) {
            showSearchResult();
        }
    });

    $(document).on("click", ".found_product_item", function (e) {
        var product_name = $(this).attr('data-name');
        var product_id = $(this).attr('data-id');
        var product_image = $(this).attr('data-image');
        selectProductCardItem(product_id, product_name, product_image);
        $(".found_product_field").val('');
        hideSearchResult();
    });

    var product_card = $(".selected_product_card");

    var productCardUrl = function(id){
        return '/product/' +   id;
    };

    var selectProductCardItem = function(product_id, product_name, product_image){
        product_card.show(200);
        $(".selected_product_card a").attr('href', productCardUrl(product_id));
        $(".product_card_image").attr('src', product_image);
        $(".product_card_name").text(product_name);
        $("input[name='product_id']").val(product_id);
        $(".offer_toggle_data").slideDown();
    };

    $(document).on("change", ".delivery_check_action", function (e) {

        var checked_value = $('.delivery_check_action:checked').val();
        console.log(checked_value);
        if(checked_value=="true"){
            $(".delivery_cost_field").show();
        } else {
            $(".delivery_cost_field").hide();
        }
    });
});