$(document).ready(function () {

    if($(".company_rating_container").length>0) {


        $(".use_select2").select2();

        $(".user_objects_use_select2").select2();

        $(document).on("change", "#search_ajax_new_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'new_only', state);
            redirect(new_url);
        });

        $(document).on("change", "#search_ajax_answered_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'answered_only', state);
            redirect(new_url);
        });

        $(document).on("change", ".selected_user_list_select", function () {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            $(".selected_user_option").html("<option value='" + id + "'>" + name + "</option>");
        });

        $(document).on("click", ".sort_product_list", function (e) {
            e.preventDefault();
            var current_sorter = $(this);
            var icon_down = $(this).attr("data-down-icon");
            var icon_up = $(this).attr("data-up-icon");
            var sort_type = $(this).attr("data-sort");
            var sort_status = $(this).attr("data-sort_status");
            var sort_direction = $(this).attr("data-dir");

            if (sort_status == "on") {
                if (sort_direction == "asc") {
                    $(this).attr('data-dir', "desc");
                    $(this).find(".sort_icon").removeClass(icon_up).addClass(icon_down);
                } else if (sort_direction == "desc") {
                    $(this).attr('data-dir', "asc");
                    $(this).find(".sort_icon").removeClass(icon_down).addClass(icon_up);
                }
            }

            $(".sort_product_list").each(function () {
                if ($(this).attr('data-sort') == sort_type) {
                    $(this).removeClass("text-muted");
                    $(this).attr('data-sort_status', "on");
                } else {
                    $(this).addClass("text-muted");
                    $(this).attr('data-sort_status', "off");
                }
            });

            showRatingTableLoader()

            var new_url = updateQueryStringParameter(window.location.href, 'sort_dir', sort_direction);
            new_url = updateQueryStringParameter(new_url, 'sort_type', sort_type);

            var stateObj = {foo: "bar"};
            history.pushState(stateObj, "", new_url);

            var params = {
                'sort_type': sort_type,
                'sort_dir': sort_direction,
                'page': page,
                'user_id': user_id,
            };

            var query_string = $.param(params);
            reload_rating_container(query_string);
            return false;
        });

        var loader = $('<div class="load5 product_loader"><div class="loader"></div></div>');

        var notFoundBlock = $('<div class="well">По вашему запросу ничего не удалось найти.</div>');

        var company_rating_container = $(".company_rating_container");

        var showRatingTableLoader = function () {
            company_rating_container.html(loader);
        };

        var showRatingData = function (content) {
            company_rating_container.html(content);
        };

        var showRatingTableNotFound = function () {
            company_rating_container.html(notFoundBlock);
        };

        var reload_rating_container = function (query_string) {

            var reload_timer;
            var transfer_data = query_string;
            $.ajax({
                url: "/company_ratings/ajax",
                type: "post",
                dataType: 'html',
                data: transfer_data,
                beforeSend: function () {
                    showRatingTableLoader();
                    clearInterval(reload_timer);
                },
                success: function (data) {
                    reload_timer = setInterval(showRatingData(data), 4000);
                }
            });
        };
        // стартовая загрузка страницы отзывов о компании
        var sort_type = getParameterByName("sort_type");
        var sort_dir = getParameterByName("sort_dir");
        var page = getParameterByName("page");
        var user_id = getParameterByName("user_id");

        var params = {
            'sort_type': sort_type,
            'sort_dir': sort_dir,
            'page': page,
            'user_id': user_id,
        };

        var query_string = $.param(params);
        setTimeout(reload_rating_container(query_string), 50);

        /* Автор отзыва */
        $(document).on("change", ".dropdownUserSearch", function () {
            var user_id = $('.dropdownUserSearch option:selected').val(), new_url;
            if (user_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'user_id', user_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'user_id', '');
            }
            redirect(new_url);
        });

    }
});