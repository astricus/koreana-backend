$(document).ready(function () {

    if($(".product_container").length>0) {

        var color_picker_param_box = function (default_value, param_name, param_id) {
            var content = $("<div class='product_param_form_element'>" +
                "<label for=name='" + param_id + "'>" + param_name + "</label>" +
                "<input type='color' id='param_" + param_id + "' name='params[" + param_id + "]' value='" + default_value + "' /></div>");
            return content;
        };


        var number_range_param_box = function (default_value, param_name, param_id, value_interval, min_value, max_value) {
            var content =
                $("<div class='product_param_form_element'>" +
                    "<label for=name='" + param_id + "'>" + param_name + "</label>" +
                    "<div class='super_slider_box'>" +
                    "<input name='params[" + param_id + "]' id='param_" + param_id + "' type='range' step='" + value_interval + "' min='" + min_value + "' max='" + max_value + "' value='" + default_value + "' class='super_slider'>" +
                    "<input class='slider_viewer' value='' type='text' />" +
                    "</div>" +
                    "</div>");
            return content;
        };

        var checkbox_param_box = function (default_value, param_name, param_id, value_list) {
            var cur_checkbox = "<label>" + param_name + " </label>";
            for (var i = 0; i < value_list.length; i++) {
                var cur_param_value = value_list[i].value;
                var isChecked = (cur_param_value == default_value) ? "checked" : "";
                cur_checkbox = cur_checkbox + "<div class='checkbox'>" +
                    "<input id='param_value_" + value_list[i].id + "' type='checkbox' name='params[" + param_id + "]' class='magic-checkbox' " + isChecked + "/>" +
                    "<label for='param_value_" + value_list[i].id + "'>" + cur_param_value + " </label></div>";
            }
            return $("<div class='product_param_form_element'>" + cur_checkbox + "</div>");
        };

        // поиск подкатегорий в категории
        $(document).on("change", ".productCategoryParamsSearch", function () {
            var optionSelected = $("option:selected", this);
            var category_id = this.value;
            var category_name = $(".productCategoryParamsSearch option:selected").text();
            var transfer_data = "category_id=" + category_id;
            $.ajax({
                url: "/product_category/params_search_ajax/" + category_id,
                type: "post",
                dataType: 'json',
                data: transfer_data,
                beforeSend: function () {
                },
                success: function (data) {
                    $(".category_product_param_box").html('');

                    // указывается название выбраной категории товара
                    $(".selected_category_name").text(category_name);

                    if (data.data.status == "error") {
                    } else {

                        var param_data = data.data;

                        for (var i = 0; i < param_data.length; i++) {

                            var cur_param_data = param_data[i];
                            var new_elem;
                            if (cur_param_data.param.param_type == "color") {
                                new_elem = color_picker_param_box(
                                    cur_param_data.param.default_value,
                                    cur_param_data.param.param_name,
                                    cur_param_data.param.id
                                );
                            } else if (cur_param_data.param.param_type == "number") {
                                new_elem = number_range_param_box(
                                    cur_param_data.param.default_value,
                                    cur_param_data.param.param_name,
                                    cur_param_data.param.id,
                                    cur_param_data.param.value_interval,
                                    cur_param_data.param.min_value,
                                    cur_param_data.param.max_value
                                );
                            } else if (cur_param_data.param.param_type == "checkbox") {
                                new_elem = checkbox_param_box(
                                    cur_param_data.param.default_value,
                                    cur_param_data.param.param_name,
                                    cur_param_data.param.id,
                                    cur_param_data.param_values
                                );
                            } else {
                                new_elem = "";
                            }
                            $(".category_product_param_box").append(new_elem);
                        }
                    }
                }
            });
        });

        $(document).on("keyup", "#dropdownUserSearchConnector", function () {

            var last_string = "";
            var string = $(this).val();

            if (string.length <= 2) {
                return false;
            }

            var search_delay = function (string, last_string) {

                if (last_string == string) {
                    return false;
                }
                var transfer_data = "string=" + string;
                $.ajax({
                    url: "/user/search_ajax",
                    type: "post",
                    dataType: 'json',
                    data: transfer_data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        $(".search_ajax_user_box").html('');
                        if (data.data.status == "error") {
                        } else {
                            for (var i = 0; i < data.data.data.length; i++) {
                                var new_elem = $('<li data-original-index="0" class="selected"><a tabindex="0" data-tokens="null" role="option" aria-disabled="false" ' +
                                    'aria-selected="true" data-id="' + data.data.data[i].id + '" data-name="' + data.data.data[i].company_name + '" ' +
                                    'class="selected_user_list_select"><span class="text">' + data.data.data[i].company_name + '</span></a></li>');
                                $(".search_ajax_user_box").append(new_elem);
                            }
                        }
                    }
                });
                last_string = string;
            };

            setTimeout(search_delay(string, last_string), 500);
        });

        $(".use_select2").select2();

        $(".user_objects_use_select2").select2();

        $(document).on("click", ".selected_manager_list_item", function () {
            var selected_manager_id = $(this).attr('data-id');
            if (selected_manager_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'manager_id', selected_manager_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'manager_id', '');
            }
            redirect(new_url);
        });

        $(document).on("click", ".selected_user_list_item", function () {
            var selected_user_id = $(this).attr('data-id');
            if (selected_user_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'user_id', selected_user_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'user_id', '');
            }
            redirect(new_url);
        });

        $(document).on("click", ".selected_user_list_item", function (evt) {
            evt.preventDefault();
            var selected_user_id = $(this).attr('data-id');
            var selected_user_name = $(this).attr('data-name');
            //$("#dropdownUserSearch").val(selected_user_name);
            new_url = updateQueryStringParameter(window.location.href, 'user_id', selected_user_id);
            redirect(new_url);
        });

        $(document).on("change", "#search_ajax_open_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'open_only', state);
            redirect(new_url);
        });

        $(document).on("change", "#search_ajax_active_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'active_only', state);
            redirect(new_url);
        });

        $(document).on("change", "#search_ajax_my_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'stopped_offers', state);
            redirect(new_url);
        });

        $(document).on("change", ".selected_user_list_select", function () {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            $(".selected_user_option").html("<option value='" + id + "'>" + name + "</option>");
        });

        $(document).on("click", ".sort_product_list", function (e) {
            e.preventDefault();
            var current_sorter = $(this);
            var icon_down = $(this).attr("data-down-icon");
            var icon_up = $(this).attr("data-up-icon");
            var sort_type = $(this).attr("data-sort");
            var sort_status = $(this).attr("data-sort_status");
            var sort_direction = $(this).attr("data-dir");

            if (sort_status == "on") {
                if (sort_direction == "asc") {
                    $(this).attr('data-dir', "desc");
                    $(this).find(".sort_icon").removeClass(icon_up).addClass(icon_down);
                } else if (sort_direction == "desc") {
                    $(this).attr('data-dir', "asc");
                    $(this).find(".sort_icon").removeClass(icon_down).addClass(icon_up);
                }
            }

            $(".sort_product_list").each(function () {
                if ($(this).attr('data-sort') == sort_type) {
                    $(this).removeClass("text-muted");
                    $(this).attr('data-sort_status', "on");
                } else {
                    $(this).addClass("text-muted");
                    $(this).attr('data-sort_status', "off");
                }
            });

            showProductTableLoader();

            var new_url = updateQueryStringParameter(window.location.href, 'sort_dir', sort_direction);
            new_url = updateQueryStringParameter(new_url, 'sort_type', sort_type);

            var stateObj = {foo: "bar"};
            history.pushState(stateObj, "", new_url);

            var params = {
                'sort_type': sort_type,
                'sort_dir': sort_direction,
                'page': page,
                'category_id': category_id,
                'shop_id': shop_id,
            };

            var query_string = $.param(params);
            reload_product_container(query_string);
            return false;
        });

        var loader = $('<div class="load5 product_loader"><div class="loader"></div></div>');

        var notFoundBlock = $('<div class="well">По вашему запросу ничего не удалось найти.</div>');

        var product_container = $(".product_container");

        var showProductTableLoader = function () {
            product_container.html(loader);
        };

        var showProductData = function (content) {
            product_container.html(content);
        };

        var showProductTableNotFound = function () {
            product_container.html(notFoundBlock);
        };

        var reload_product_container = function (query_string) {

            var reload_timer;
            var transfer_data = query_string;
            $.ajax({
                url: "/products/ajax",
                type: "post",
                dataType: 'html',
                data: transfer_data,
                beforeSend: function () {
                    showProductTableLoader();
                    clearInterval(reload_timer);
                },
                success: function (data) {
                    reload_timer = setInterval(showProductData(data), 4000);
                }
            });
        };
        // стартовая загрузка страницы товаров
        var sort_type = getParameterByName("sort_type");
        var sort_dir = getParameterByName("sort_dir");
        var page = getParameterByName("page");
        var category_id = getParameterByName("category_id");
        var shop_id = getParameterByName("shop_id");
        var product_id = getParameterByName("product_id");

        var params = {
            'sort_type': sort_type,
            'sort_dir': sort_dir,
            'page': page,
            'category_id': category_id,
            'shop_id': shop_id,
            'product_id': product_id,
        };

        var query_string = $.param(params);
        setTimeout(reload_product_container(query_string), 50);

        /* категория */
        $(document).on("change", ".dropdownCategorySearch", function () {
            var category_id = $('.dropdownCategorySearch option:selected').val(), new_url;
            if (category_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'category_id', category_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'category_id', '');
            }
            redirect(new_url);
        });

        /* магазин */
        $(document).on("change", ".dropdownShopSelect", function () {
            var shop_id = $('.dropdownShopSelect option:selected').val(), new_url;
            if (shop_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'shop_id', shop_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'shop_id', '');
            }
            redirect(new_url);
        });

        /* товар */
        $(document).on("change", ".dropdownProduct", function () {
            var product_id = $('.dropdownProduct option:selected').val(), new_url;
            if (product_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'product_id', product_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'product_id', '');
            }
            redirect(new_url);
        });
    }
});