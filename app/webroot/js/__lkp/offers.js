$(document).ready(function () {

    if($(".offer_container").length>0) {

        // поиск подкатегорий в категории
        $(document).on("change", ".productCategoryParamsSearch", function () {
            var optionSelected = $("option:selected", this);
            var category_id = this.value;
            var category_name = $(".productCategoryParamsSearch option:selected").text();
            var transfer_data = "category_id=" + category_id;
            $.ajax({
                url: "/product_category/params_search_ajax/" + category_id,
                type: "post",
                dataType: 'json',
                data: transfer_data,
                beforeSend: function () {
                },
                success: function (data) {
                    $(".category_product_param_box").html('');

                    // указывается название выбраной категории товара
                    $(".selected_category_name").text(category_name);

                    if (data.data.status == "error") {
                    } else {

                        var param_data = data.data;

                        for (var i = 0; i < param_data.length; i++) {

                            var cur_param_data = param_data[i];
                            var new_elem;
                            if (cur_param_data.param.param_type == "color") {
                                new_elem = color_picker_param_box(
                                    cur_param_data.param.default_value,
                                    cur_param_data.param.param_name,
                                    cur_param_data.param.id
                                );
                            } else if (cur_param_data.param.param_type == "number") {
                                new_elem = number_range_param_box(
                                    cur_param_data.param.default_value,
                                    cur_param_data.param.param_name,
                                    cur_param_data.param.id,
                                    cur_param_data.param.value_interval,
                                    cur_param_data.param.min_value,
                                    cur_param_data.param.max_value
                                );
                            } else if (cur_param_data.param.param_type == "checkbox") {
                                new_elem = checkbox_param_box(
                                    cur_param_data.param.default_value,
                                    cur_param_data.param.param_name,
                                    cur_param_data.param.id,
                                    cur_param_data.param_values
                                );
                            } else {
                                new_elem = "";
                            }
                            $(".category_product_param_box").append(new_elem);
                        }
                    }
                }
            });
        });

        $(document).on("keyup", "#dropdownUserSearchConnector", function () {

            var last_string = "";
            var string = $(this).val();

            if (string.length <= 2) {
                return false;
            }

            var search_delay = function (string, last_string) {

                if (last_string == string) {
                    return false;
                }
                var transfer_data = "string=" + string;
                $.ajax({
                    url: "/user/search_ajax",
                    type: "post",
                    dataType: 'json',
                    data: transfer_data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        $(".search_ajax_user_box").html('');
                        if (data.data.status == "error") {
                        } else {
                            for (var i = 0; i < data.data.data.length; i++) {
                                var new_elem = $('<li data-original-index="0" class="selected"><a tabindex="0" data-tokens="null" role="option" aria-disabled="false" ' +
                                    'aria-selected="true" data-id="' + data.data.data[i].id + '" data-name="' + data.data.data[i].company_name + '" ' +
                                    'class="selected_user_list_select"><span class="text">' + data.data.data[i].company_name + '</span></a></li>');
                                $(".search_ajax_user_box").append(new_elem);
                            }
                        }
                    }
                });
                last_string = string;
            };

            setTimeout(search_delay(string, last_string), 500);
        });

        $(".use_select2").select2();

        $(".user_objects_use_select2").select2();

        $(document).on("click", ".selected_manager_list_item", function () {
            var selected_manager_id = $(this).attr('data-id');
            if (selected_manager_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'manager_id', selected_manager_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'manager_id', '');
            }
            redirect(new_url);
        });

        $(document).on("click", ".selected_user_list_item", function () {
            var selected_user_id = $(this).attr('data-id');
            if (selected_user_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'user_id', selected_user_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'user_id', '');
            }
            redirect(new_url);
        });

        $(document).on("click", ".selected_user_list_item", function (evt) {
            evt.preventDefault();
            var selected_user_id = $(this).attr('data-id');
            var selected_user_name = $(this).attr('data-name');
            //$("#dropdownUserSearch").val(selected_user_name);
            new_url = updateQueryStringParameter(window.location.href, 'user_id', selected_user_id);
            redirect(new_url);
        });

        $(document).on("change", "#search_ajax_open_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'open_only', state);
            redirect(new_url);
        });

        $(document).on("change", "#search_ajax_active_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'active_only', state);
            redirect(new_url);
        });

        $(document).on("change", "#search_ajax_my_only", function () {
            var state = $(this).is(':checked') ? 'on' : '';
            new_url = updateQueryStringParameter(window.location.href, 'stopped_offers', state);
            redirect(new_url);
        });

        $(document).on("change", ".selected_user_list_select", function () {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            $(".selected_user_option").html("<option value='" + id + "'>" + name + "</option>");
        });

        $(document).on("click", ".sort_product_list", function (e) {
            e.preventDefault();
            var current_sorter = $(this);
            var icon_down = $(this).attr("data-down-icon");
            var icon_up = $(this).attr("data-up-icon");
            var sort_type = $(this).attr("data-sort");
            var sort_status = $(this).attr("data-sort_status");
            var sort_direction = $(this).attr("data-dir");

            if (sort_status == "on") {
                if (sort_direction == "asc") {
                    $(this).attr('data-dir', "desc");
                    $(this).find(".sort_icon").removeClass(icon_up).addClass(icon_down);
                } else if (sort_direction == "desc") {
                    $(this).attr('data-dir', "asc");
                    $(this).find(".sort_icon").removeClass(icon_down).addClass(icon_up);
                }
            }

            $(".sort_product_list").each(function () {
                if ($(this).attr('data-sort') == sort_type) {
                    $(this).removeClass("text-muted");
                    $(this).attr('data-sort_status', "on");
                } else {
                    $(this).addClass("text-muted");
                    $(this).attr('data-sort_status', "off");
                }
            });

            showOfferTableLoader();

            var new_url = updateQueryStringParameter(window.location.href, 'sort_dir', sort_direction);
            new_url = updateQueryStringParameter(new_url, 'sort_type', sort_type);

            var stateObj = {foo: "bar"};
            history.pushState(stateObj, "", new_url);

            var params = {
                'sort_type': sort_type,
                'sort_dir': sort_direction,
                'page': page,
                'category_id': category_id,
                'region_id': region_id,
                'brand_id': brand_id,
            };

            var query_string = $.param(params);
            reload_offer_container(query_string);
            return false;
        });

        var loader = $('<div class="load5 product_loader"><div class="loader"></div></div>');

        var notFoundBlock = $('<div class="well">По вашему запросу ничего не удалось найти.</div>');

        var offer_container = $(".offer_container");

        var showOfferTableLoader = function () {
            offer_container.html(loader);
        };

        var showOfferData = function (content) {
            offer_container.html(content);
        };

        var showOfferTableNotFound = function () {
            offer_container.html(notFoundBlock);
        };

        var reload_offer_container = function (query_string) {

            var reload_timer;
            var transfer_data = query_string;
            $.ajax({
                url: "/offer/ajax",
                type: "post",
                dataType: 'html',
                data: transfer_data,
                beforeSend: function () {
                    showOfferTableLoader();
                    clearInterval(reload_timer);
                },
                success: function (data) {
                    reload_timer = setInterval(showOfferData(data), 4000);
                }
            });
        };
        // стартовая загрузка страницы предложений
        var sort_type = getParameterByName("sort_type");
        var sort_dir = getParameterByName("sort_dir");
        var page = getParameterByName("page");
        var category_id = getParameterByName("category_id");
        var region_id = getParameterByName("region_id");
        var product_id = getParameterByName("product_id");
        var brand_id = getParameterByName("brand_id");

        var params = {
            'sort_type': sort_type,
            'sort_dir': sort_dir,
            'page': page,
            'category_id': category_id,
            'region_id': region_id,
            'product_id': product_id,
            'brand_id': brand_id,
        };

        var query_string = $.param(params);
        setTimeout(reload_offer_container(query_string), 50);

        /* категория */
        $(document).on("change", ".dropdownOfferRegion", function () {
            var region_id = $('.dropdownOfferRegion option:selected').val(), new_url;
            if (region_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'region_id', region_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'region_id', '');
            }
            redirect(new_url);
        });

        /* Бренд */
        $(document).on("change", ".dropdownBrandSelect", function () {
            var brand_id = $('.dropdownBrandSelect option:selected').val(), new_url;
            if (brand_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'brand_id', brand_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'brand_id', '');
            }
            redirect(new_url);
        });

        /* товар */
        $(document).on("change", ".dropdownProduct", function () {
            var product_id = $('.dropdownProduct option:selected').val(), new_url;
            if (product_id > 0) {
                new_url = updateQueryStringParameter(window.location.href, 'product_id', product_id);
            } else {
                new_url = updateQueryStringParameter(window.location.href, 'product_id', '');
            }
            redirect(new_url);
        });
    }
});