$(document).on("click", ".is_work_day", function () {
    var day = $(this).attr('data-day');
    if ($(this).hasClass("work_day")) {
        $("select[data-day=" + day + "]").attr('disabled', false);
        $(".is_work_day.no_work_day[data-day=" + day + "]").removeClass('btn-success').addClass('btn-default');
        $(".is_work_day.work_day[data-day=" + day + "]").addClass('btn-success').removeClass('btn-default');
        $("input[name='work_time[" + day + "][is_work_day]']").val(1);
    } else {
        $("select[data-day=" + day + "]").attr('disabled', true);
        $(".is_work_day.no_work_day[data-day=" + day + "]").addClass('btn-success').removeClass('btn-default');
        $(".is_work_day.work_day[data-day=" + day + "]").addClass('btn-default').removeClass('btn-success');
        $("input[name='work_time[" + day + "][is_work_day]']").val(0);
    }
    return false;
});
