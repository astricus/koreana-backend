$(document).ready(function () {
    var updateShopCoords = function(coords, id){
        var result = {
            shop_latitude: coords[0],
            shop_longitude: coords[1],
        };
        $.ajax({
            url: "/shop/update_coords/" + id,
            type: "post",
            dataType: 'json',
            data: result,
            beforeSend: function () {
            },
            success: function (data) {
                //console.log(data);
                if (data.data.status == "error") {
                }
                else {
                    window.location.reload(true);
                }
            }
        });
    };
});