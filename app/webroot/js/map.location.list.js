ymaps.ready(init);
var myMap;

let def_map_lat = 57.5262;
let def_map_lon = 38.3061;


function init() {

    myMap = new ymaps.Map("map", {
        center: [lat, lon],
        zoom: 10
    }, {
        balloonMaxWidth: 200,
        searchControlProvider: 'yandex#search'
    });

    // Обработка события, возникающего при щелчке
    // левой кнопкой мыши в любой точке карты.
    // При возникновении такого события откроем балун.
    myMap.events.add('click', function (e) {
        if (!myMap.balloon.isOpen()) {
            var coords = e.get('coords');
            console.log(e.get('coords'));
            getAddress(coords);
            myMap.balloon.open(coords, {
                contentHeader: 'Событие!',
                contentBody: '<p>Вы выбрали местоположение на карте.</p>' +
                    '<p>Координаты места: ' + [
                        coords[0].toPrecision(6),
                        coords[1].toPrecision(6)
                    ].join(', ') + '</p>',
                contentFooter: '<sup>Щелкните еще раз</sup>'
            });
        } else {
            myMap.balloon.close();
        }
    });

    // Обработка события, возникающего при щелчке
    // правой кнопки мыши в любой точке карты.
    // При возникновении такого события покажем всплывающую подсказку
    // в точке щелчка.
    myMap.events.add('contextmenu', function (e) {
        myMap.hint.open(e.get('coords'), 'Кто-то щелкнул правой кнопкой');
        console.log(e.get('coords'));
    });

    // Скрываем хинт при открытии балуна.
    myMap.events.add('balloonopen', function (e) {
        myMap.hint.close();
    });

    function getAddress(coords) {
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);
            console.log('city: ' + firstGeoObject.properties.get('name'));
            console.log('text: ' + firstGeoObject.properties.get('text'));
            console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
            console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
            console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
            console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));
            $("#address").val(firstGeoObject.properties.get('text'));
        });
    }


    /**
     * Created by astricus on 05.11.2016.
     */
    var myMap;

    function parseUrlQuery() {
        var data = {};
        if (location.search) {
            var pair = (location.search.substr(1)).split('&');
            for (var i = 0; i < pair.length; i++) {
                var param = pair[i].split('=');
                data[param[0]] = param[1];
            }
        }
        return data;
    }

    var searchGeoData;

    view_map();

    function view_map() {
        $("#map").show();
        var urlData = parseUrlQuery();
        var url = '/location/map_ajax/';
        var params = 'type=all';
        var myPlacemark = null;
        ymaps.ready(init).done(function (ym) {
        });

// Инициализация и уничтожение карты при нажатии на кнопку.


        var isSearching = false;

        function searchItems(bound_area) {
            sended_data = bound_area;
            $.ajax({
                url: url,
                type: "post",
                dataType: 'json',
                data: sended_data,
                beforeSend: function () {
                    myMap.geoObjects.removeAll();
                },
                success: function (data) {
                    searchGeoData = data.data;
                    //ymaps.geoQuery(searchGeoData).searchInside(myMap).addToMap(myMap).applyBoundsToMap(myMap, {checkZoomRange: true});
                    //var result = ymaps.geoQuery(searchGeoData).applyBoundsToMap(myMap, {checkZoomRange: true});
                    // Откластеризуем полученные объекты и добавим кластеризатор на карту.
                    // Обратите внимание, что кластеризатор будет создан сразу, а объекты добавлены в него
                    // только после того, как будет получен ответ от сервера.
                    var visibleObjects = ymaps.geoQuery(searchGeoData).searchInside(myMap).addToMap(myMap);//.applyBoundsToMap(myMap, {checkZoomRange: true});
                    //ymaps.geoQuery(searchGeoData).remove(visibleObjects).removeFromMap(myMap);
                    $(".map_active_items_counter").text(ymaps.geoQuery(searchGeoData)._objects.length);
                    //$(".map_filtered_items_counter").text(data.data.filtered_items_count);
                    myMap.geoObjects.add(visibleObjects.clusterize());
                    //console.log(ymaps.geoQuery(searchGeoData));

                    setTimeout(function () {
                        isSearching = false;
                    }, 200);

                    /*

                     myMap.events.add('boundschange', function () {
                     // После каждого сдвига карты будем смотреть, какие объекты попадают в видимую область.
                     var visibleObjects = ymaps.geoQuery(searchGeoData).searchInside(myMap).addToMap(myMap);
                     // Оставшиеся объекты будем удалять с карты.
                     ymaps.geoQuery(searchGeoData).remove(visibleObjects).removeFromMap(myMap);
                     });*/

                }, error: function (data) {
                    console.log('error');
                    console.log(data);
                }
            });
        }

        map_coods = myMap.getBounds();
        bound_area = {
            's_x': map_coods[0][0],
            's_y': map_coods[0][1],
            'e_x': map_coods[1][0],
            'e_y': map_coods[1][1]
        };
        searchItems(bound_area);


        // экспериментальное кодирование

        myMap.events.add('boundschange', function (boundschange) {
            if (!isSearching) {
                setTimeout(function () {
                    //console.log(myMap.getBounds());
                    map_coods = myMap.getBounds();
                    bound_area = {
                        's_x': map_coods[0][0],
                        's_y': map_coods[0][1],
                        'e_x': map_coods[1][0],
                        'e_y': map_coods[1][1]
                    };

                    // рассчет смены процента покрытия карты, производится в случае если существенно перемещается карта либо производится смена
                    // масштаба карты
                    if (boundschange.get('oldZoom') != myMap.getZoom()) {
                        isSearching = true;
                        searchItems(bound_area);
                    } else {
                        var oldBound = boundschange.get('oldBounds');
                        // выравнивание координат
                        if (oldBound[0][0] < 0) oldBound[0][0] = 180 - oldBound[0][0];
                        if (oldBound[0][1] < 0) oldBound[0][1] = 180 - oldBound[0][1];
                        if (oldBound[1][0] < 0) oldBound[1][0] = 180 - oldBound[1][0];
                        if (oldBound[1][1] < 0) oldBound[1][1] = 180 - oldBound[1][1];
                        if (map_coods[0][0] < 0) map_coods[0][0] = 180 - map_coods[0][0];
                        if (map_coods[0][1] < 0) map_coods[0][1] = 180 - map_coods[0][1];
                        if (map_coods[1][0] < 0) map_coods[1][0] = 180 - map_coods[1][0];
                        if (map_coods[1][1] < 0) map_coods[1][1] = 180 - map_coods[1][1];
                        //определение относительного сдвига координат, независимо от масштаба карты
                        var shiftBounds = (Math.abs(map_coods[0][0] - oldBound[0][0]) * Math.abs(map_coods[0][1] - oldBound[0][1])) /
                            (Math.abs(map_coods[0][0] - map_coods[1][0]) * Math.abs(map_coods[0][1] - map_coods[1][1]));
                        if ($(document).width() > 400) {
                            var initRenderParam = 0.0007;
                        } else {
                            var initRenderParam = 0.15;
                        }
                        console.log('shift ' + shiftBounds);
                        if (shiftBounds > initRenderParam) {
                            console.log('рендеринг карты, смещение ' + shiftBounds);
                            isSearching = true;
                            searchItems(bound_area);
                        } else {
                            console.log('слишком маленькое смещение ' + shiftBounds);
                        }
                    }
                }, 150);
            }


            // После каждого сдвига карты будем смотреть, какие объекты попадают в видимую область.
            ////var visibleObjects = ymaps.geoQuery(searchGeoData).searchInside(myMap).addToMap(myMap);
            // Оставшиеся объекты будем удалять с карты.
            ////ymaps.geoQuery(searchGeoData).remove(visibleObjects).removeFromMap(myMap);
        });
        //ymaps.geoQuery(searchGeoData).addToMap(myMap);

        //$.getJSON('http://invite.loc/js/mapdata/data.json', function (json) {
        //	ymaps.geoQuery(json)
        //	.addToMap(myMap);
        //});

        //$(".map_loader_link").text('Скрыть карту');


        // Определяем адрес по координатам (обратное геокодирование).

        function getAddress(coords) {
            myPlacemark.properties.set('iconCaption', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                console.log('city: ' + firstGeoObject.properties.get('name'));
                console.log('text: ' + firstGeoObject.properties.get('text'));
                console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
                console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
                console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
                console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));
                // декодирование города / области


                var DecodeData = 'string=' + firstGeoObject.properties.get('description');
                console.log('adress ' + firstGeoObject.properties.get('text'));
                load_map_popup([coords[0].toPrecision(6), coords[1].toPrecision(6)], 0, firstGeoObject.properties.get('text'));

                $.ajax({
                    url: "/cities/decode/",
                    type: "get",
                    dataType: 'json',
                    data: DecodeData,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status == "error") {
                            console.log('error');
                        } else {
                            if (data.status == "success") {

                                /*
                                 console.log('founded ' + data.data.data.id);
                                 $("input[name='city_name']").val(data.data.data.city_name);
                                 $("input[name='city_id']").val(data.data.data.id);
                                 */
                            }
                        }
                    }
                });

                myPlacemark.properties
                    .set({
                            iconCaption: 'Мое приглашение - ' + firstGeoObject.properties.get('name'),
                            balloonContent: firstGeoObject.properties.get('text'),
                            preset: 'islands#blackStretchyIcon'
                        }
                    );
            });
        }

        // Слушаем клик на карте.
        myMap.events.add('click', function (e) {
            var coords = e.get('coords');
            // Если метка уже создана – просто передвигаем ее.
            if (myPlacemark) {
                myPlacemark.geometry.setCoordinates(coords);

            }
            // Если нет – создаем.
            else {
                //загрузка формы добавления элемента карты
                myPlacemark = createPlacemark(coords);
                myMap.geoObjects.add(myPlacemark);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemark.events.add('dragend', function (e) {

                    getAddress(myPlacemark.geometry.getCoordinates());
                    /*
                    var thisPlacemark = e.get('target');
                    // Определение координат метки
                    var coords = thisPlacemark.geometry.getCoordinates();
                    // и вывод их при щелчке на метке
                    thisPlacemark.properties.set(coords);
                    console.log(coords);
                    myMap.balloon.open(coords, {
                        contentHeader: 'Готово!',
                        contentBody: '<p>Вы успешно указали ваше месторасположение на карте</p>' +
                        '<p>Координаты: ' + [
                            coords[0].toPrecision(6),
                            coords[1].toPrecision(6)
                        ].join(', ') + '</p>',
                        contentFooter: '<sup>Вы успешно указали ваше месторасположение на карте</sup>'
                    });
                    */

                });
            }
            getAddress(myPlacemark.geometry.getCoordinates());
        });

        // Создание метки.
        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconCaption: 'поиск...'
            }, {
                preset: 'islands#violetDotIconWithCaption',
                draggable: true
            });
        }

        // обработка перехода на карту при клике на метках
        $(document).on("click", ".map_active_linker", function () {
            var lat = $(this).attr('data-lat');
            var lon = $(this).attr('data-lon');
            $('html,body').animate({
                scrollTop: $("#map").offset().top + 30
            }, 1000);
            myMap.setCenter([lat, lon], 16);

        });



    }


}

function load_map_popup(map_center, item_id, item_address) {
    $("#map_popup").slideDown(400, function () {
        $(".map_popup_wrapper").show(200);
        if (isNaN(item_id) || item_id==0) {
            $(".map_popup_wrapper").load("/map_action/add_to_map_popup/", function () {
                var set_lat_coord = map_center[0];
                var set_lon_coord = map_center[1];
                $(".set_item_lat").val(set_lat_coord);
                $(".set_item_lon").val(set_lon_coord);
                console.log('address item' + item_address);
                if(item_address.length>0){
                    $(".item_address").val(item_address);
                }

            });
        } else {
            $(".map_popup_wrapper").load("/map_action/edit_map_item_popup/" + item_id, function () {

            });
        }
    });
    var ScrollPopupTo = $("#map").offset().top*1 - $("#main_nav").height()*1 - 5;
    console.log($("#map").offset().top + " " + $("#main_nav").height());
    $('html,body').animate({
        scrollTop: ScrollPopupTo
    }, 500);
}