let LOG = function (data) {
    console.log(data);
};
let query_string = null;
let reloadChatInterval = null;
let select_recipient_title = null;
let chat_container = null;
let room_recipients = null;
let room_recipient_container = null;



let chat_current_recipient = null;
let getRecipientData = function (elem) {
};
let setRecipientData = function (elem) {
};
let updateRecipientList = function () {
};
let updateChatRoomUserList = function () {
};
let updateChatMessages = function (data) {
};
let chat_recipient_name = "";
let loading_messages_title = "идет загузка сообщений, подождите";
let sending_message_time = "только что";
let reload_chat = null;
let searchPopup = null;
let last_chat_message_id = 0;
let site_name = window.location.protocol + '//' + window.location.host;


let send_chat_message_form = null;


//общие методы, переменные
$(document).ready(function () {
    chat_container = $("#chat_container");
    select_recipient_title = $(".select_recipient_title");
    chat_current_recipient = $(".chat_recipient");
    chat_recipient_name = $(".chat_recipient_name");
    send_chat_message_form = $("#send_chat_message");
    room_recipients = $(".room_recipients");
    room_recipient_container = $(".room_recipient_container");

});
let active_recipient = [];


//обновление чата, перегрузка сообщений, прокрутка чата
let scrollToBottom = function (chat_container) {
    document.getElementById(chat_container).scrollIntoView(false);
};

let chat_message_html = '<a href="#" class="chat-unread chat_message_item">' +
    '<div class="media-left">' +
    '<img class="img-circle img-xs" src="images/chat/CHAT_AVATAR" alt="Profile Picture">' +
    '<i class="badge badge-success badge-stat badge-icon pull-left"></i>' +
    '</div>' +
    '<div class="media-body">' +
    '<span class="chat-info">' +
    '<span class="text-xs">MSG_TIME</span>' +
    '<span class="badge badge-success">9</span>' +
    '</span>' +
    '<div class="chat-text">' +
    '<p class="chat-username chat_recipient" data-user_id="USER_ID" data-user_role="USER_ROLE" data-chat_type="CHAT_TYPE" data-last_msg="LAST_MSG_ID" data-chat_id="CHAT_ID">USER_NAME</p>' +
    '<p class="chat-userrole text-bold">USER_ROLE_NAME</p>' +
    '<p>LAST_MSG_TXT</p>' +
    '</div>' +
    '</div>' +
    '</a>';

var setSendingParams = function () {
};

// Список диалогов чата
$(document).ready(function () {

    getRecipientData = function (elem) {
        let recipient_data = elem.find(".chat_recipient");
        let user_id = recipient_data.attr('data-user_id');
        let user_role = recipient_data.attr('data-user_role');
        let chat_type = recipient_data.attr('data-chat_type');
        let chat_id = recipient_data.attr('data-chat_id');
        let last_chat_message_id = recipient_data.attr('data-last_msg');
        let user_name = recipient_data.text();
        let user = {};
        user.name = user_name;
        user.user_role = user_role;
        user.user_id = user_id;
        user.chat_type = chat_type;
        user.chat_id = chat_id;
        //window.last_chat_message_id = last_chat_message_id;
        return user;
    };

    setRecipientData = function (user) {
        chat_recipient_name.text(user.name);
        chat_current_recipient.attr('data-user_id', user.user_id);
        chat_current_recipient.attr('data-user_role', user.user_role);
        chat_current_recipient.attr('data-chat_type', user.chat_type);
        chat_current_recipient.attr('data-chat_id', user.chat_id);
        send_chat_message_form.find("[name='chat_id']").val(user.chat_id);
        send_chat_message_form.find("[name='recipient_role']").val(user.user_role);
        send_chat_message_form.find("[name='recipient_id']").val(user.user_id);
        send_chat_message_form.find("[name='type']").val(user.chat_type);
        return true;
    };

    updateRecipientList = function (data, chat_message_html) {
        for (var item in data) {
            const message = data[item];
            let format_html = formatRecipientListHtml(chat_message_html, message);
            $(".chat-user-list").append(format_html);
        }
    };

    updateChatRoomUserList = function (data, chat_room_user_html) {
        //for (var item in data) {
            //const room_user = data[item];
        for (let i = 0; i<data.length; i++) {
            const room_user = data[i];

            let format_html = formatChatUserListHtml(chat_room_user_html, room_user);
            LOG(format_html);
            room_recipient_container.append(format_html);
        }
        let you_room_user = {};
        you_room_user.user_id = null;
        you_room_user.user_role_name = "";
        you_room_user.user_name = "Вы";
        let format_html = formatChatUserListHtml(chat_room_user_html, you_room_user);
        room_recipient_container.append(format_html);
    };

    //обновить окно сообщений переписки
    updateChatMessages = function (messages) {
        let messages_count = messages.length;
        if (messages_count > 0) {
            for (let i = 0; i < messages_count; i++) {
                let raw_message = messages[i];

                let html_chat_message = formatChatMessageContentHtml(chat_message_content_html, raw_message);
                chat_container.append(html_chat_message);
            }
        }
    };

});

let formatRecipientListHtml = function (chat_message_html, data) {
    format_html = chat_message_html.replace("MSG_TIME", data.message_time);
    format_html = format_html.replace("USER_ID", data.user_id);
    format_html = format_html.replace("USER_ROLE", data.user_role);
    //format_html = format_html.replace("USER_ROLE_NAME", data.user_role_name);

    format_html = format_html.replace("LAST_MSG_TXT", data.message_text);

    if (data.type == "dialog") {
        format_html = format_html.replace("CHAT_AVATAR", "dialog.png");
        format_html = format_html.replace("USER_NAME", data.user_name);
        format_html = format_html.replace("USER_ROLE_NAME", data.user_role_name);
    } else {
        format_html = format_html.replace("CHAT_AVATAR", "chat_room.svg");
        format_html = format_html.replace("USER_NAME", data.name);
        format_html = format_html.replace("USER_ROLE_NAME", data.user_role_name + " " + data.user_name + " " + data.name);

    }
    format_html = format_html.replace("CHAT_TYPE", data.type);
    format_html = format_html.replace("CHAT_ID", data.chat_id);
    format_html = format_html.replace("LAST_MSG_ID", data.id);
    return format_html;
};

let formatChatUserListHtml = function (room_user_html, data) {
    LOG(data);
    format_html = room_user_html.replace("USER_ID", data.user_id);
    //format_html = format_html.replace("USER_ROLE", data.user_role);
    format_html = format_html.replace("USER_NAME_AND_ROLE", "<span class='text-bold'>" + data.user_role_name + "</span> " +  data.user_name);
    return format_html;
};

/*
*         let user_role_name = currentItem.attr("data-user_role_name");
        let last_message_id = currentItem.attr("data-last_message_id");
        let message_text = currentItem.attr("data-message_text");
        let message_time = currentItem.attr("data-message_time");
* */

let search_popup_item = '<a href="#" class="chat-unread chat_popup_item">' +
    '<div class="media-left">' +
    '<img class="img-circle img-xs" src="images/chat/CHAT_AVATAR" alt="Profile Picture">' +
    '</div>' +
    '<div class="media-body">' +
    '<div class="chat-text">' +
    '<p class="chat-username chat_recipient" data-user_id="USER_ID" data-user_role="USER_ROLE" data-last_message_id="LAST_MSG_ID" data-message_time="MSG_TIME" data-message_text="MSG_TEXT" data-user_role_name="USER_ROLE_NAME" data-chat_type="room" data-chat_id="CHAT_ID">ELEM_NAME</p>' +
    '</div>' +
    '</div>' +
    '</a>';

let formatSearchItemHtml = function (search_popup_item, data) {
    //LOG(data);
    let format_html = search_popup_item;
    if (data.id !== undefined) {
        format_html = format_html.replace("USER_ID", data.id);
    }
    if (data.chat_id !== undefined) {
        format_html = format_html.replace("CHAT_ID", data.chat_id);
    }
    if (data.role !== undefined) {
        format_html = format_html.replace("USER_ROLE", data.role);
    }
    if (data.user_role_name !== undefined) {
        format_html = format_html.replace("USER_ROLE_NAME", data.user_role_name);
    }
    if (data.type == "dialog") {
        format_html = format_html.replace("CHAT_AVATAR", "dialog.png");
    } else {
        format_html = format_html.replace("CHAT_AVATAR", "chat_room.svg");
    }

    if (data.chat_id !== undefined) {
        format_html = format_html.replace("CHAT_ID", data.chat_id);
    }
    if (data.message_time !== undefined) {
        format_html = format_html.replace("MSG_TIME", data.message_time);
    }
    if (data.message_text !== undefined) {
        format_html = format_html.replace("MSG_TEXT", data.message_text);
    }
    if (data.name !== undefined) {
        format_html = format_html.replace("ELEM_NAME", data.name);
    }
    if (data.last_message_id !== undefined) {
        format_html = format_html.replace("LAST_MSG_ID", data.last_message_id);
    }
      return format_html;
};


let chat_message_content_html = '<a href="#" class="CHAT_MESS_OWNER chat_message" data-message_id="MESS_ID">' +
    '<div class="media-left">' +
        '<img src="AVATAR" class="img-circle img-sm" alt="Profile Picture">' +
        '<p class="chat_message_user_name text-bold">USER_NAME_AND_ROLE</p>' +
    '</div>' +
    '<div class="media-body">' +
    '<div>' +
    '<p>MESS_TXT' +
    '<small>MSG_TIME</small>' +
    '</p>' +
    '</div>' +
    '</div>' +
    '</a>';

let chat_room_user_html = '<div class="chat_room_user_message btn btn-sm btn-success btn-active-success" data-user_id="USER_ID">' +
    '<span class="room_user_name">USER_NAME_AND_ROLE</span>' +
    '</div> ';

//шаблон сообщения
//chat-me / chat-user

let default_me_avatar = "img/profile-photos/3.png";
let default_user_avatar = "img/profile-photos/3.png";

let formatChatMessageContentHtml = function (chat_message_content_html, data) {
    if (data.dir == "chat-me") {
        format_html = chat_message_content_html.replace("CHAT_MESS_OWNER", "chat-me");
    } else {
        format_html = chat_message_content_html.replace("CHAT_MESS_OWNER", "chat-user");
    }
    format_html = format_html.replace("AVATAR", default_me_avatar);
    format_html = format_html.replace("MSG_TIME", data.message_time);
    format_html = format_html.replace("USER_NAME_AND_ROLE", data.user_role_name + " <br> " + data.user_name);
    format_html = format_html.replace("USER_ID", data.user_id);
    format_html = format_html.replace("USER_ROLE", data.user_role);
    format_html = format_html.replace("USER_ROLE_NAME", data.user_role_name);
    format_html = format_html.replace("USER_NAME", data.user_name);
    format_html = format_html.replace("MESS_TXT", data.message_text);
    format_html = format_html.replace("MESS_ID", data.id);
    return format_html;
};


let getChatType = function (chat_message_html) {
    return chat_message_html.chat_type;
};

let getRecipientRole = function (chat_message_html) {
    return chat_message_html.user_role;
};


let getRecipientId = function (chat_message_html) {
    return chat_message_html.user_id;
};


let getChatId = function (chat_message_html) {
    return chat_message_html.chat_id;
};
// Окно переписки чата - заголовки


//окно переписки чата - сообщения
let add_new_chat_message = function (message, date) {
    let chat_container = $("#chat_container");
    let new_message = $('<div class="chat-me animated fadeInUp">' +
        '<div class="media-left">' +
        '<img src="img/profile-photos/8.png" class="img-circle img-sm" alt="Profile Picture">' +
        '</div><div class="media-body">' +
        '<div><p>' + message +
        '<small>' + date + '</small>' +
        '</p></div></div></div>');
    chat_container.append(new_message);
    scrollToBottom("chat_container");
};

//окно переписки чата - форма набора сообщения и отправки
let clearChatTextarea = function () {
    $("#chat_textarea").val('');
};

let clearChatMessagesArea = function () {
    chat_container.html('');
};

let clearChatRoomUserList = function () {
    room_recipient_container.html('');
};



let updateChatMessageCounter = function (count) {
    $(".message_counter").html(count);
};

//окно переписки чата - отправка файла


//обновить список собеседников


// выбор активного собеседника/группового чата
let setActiveRecipient = function () {

};