let data_chat_notifier = null;
var notifyNewMessage = function(){
    $.ajax({
        url: "/chat/notify",
        type: "post",
        dataType: 'json',
        data: data_chat_notifier,
        beforeSend: function () {
            //clearInterval(reload_timer);
        },
        success: function (data) {
            for(var c=0; c<data.data.length; c++){
                var user_role_sender = data.data[c].user_role_name + " " + data.data[c].user_name + " написал вам ";
                var user_message = data.data[c].message_text;
                if(user_message !== undefined) {
                    showChatNotify(
                        user_role_sender,
                        user_message,
                        '<i class="fa fa-cogs icon-lg"></i>',
                        'success',
                        data.data[c].data.id,
                        data.data[c].chat_id,
                        data.data[c].type
                    );
                }
            }
        }

    });
    return true;
};

let messageIsRead = function(id){
    LOG("is read " + id);
    $.ajax({
        url: "/chat/is_read/" + id,
        type: "post",
        dataType: 'json',
        data: data_chat_notifier,
        success: function (data) {
        }

    });
    return true;
};

notifyNewMessage();

var showChatNotify = function(message_text, message_title, message_icon, style, id,  chat_id, chat_type){
    var url = "/chat/" + chat_type + "/" + chat_id;
    var link = "<a href='" + url + "'>" + message_text + "</a>";
    var message_content = message_icon + " " + link;
    $.niftyNoty({
        type: style,
        container : 'floating',
        title : message_content ,
        message : message_title,
        closeBtn : false,
        timer : 10000,
        onHide:function(){
            messageIsRead(id);
        },
    });
};
//setInterval(notifyNewMessage, 7000);