$(document).ready(function () {

// отправка сообщения в чате
    // загрузка списка собеседников
    setTimeout(function () {
        $.ajax({
            url: "/chat/history",
            type: "post",
            dataType: 'json',
            data: null,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status === "success") {
                    updateRecipientList(data.data, chat_message_html);
                }
            }
        });
    }, 500);

    var setActiveChat = function (type, chat_id) {
        select_recipient_title.hide();
        $(".chat_window").show();

        let currentChatMessageItem = $(this);
        $(".chat_message_item").removeClass("active_chat_item");
        $(this).addClass("active_chat_item");

        let user = getRecipientData(currentChatMessageItem);
        setRecipientData(user);
        // получение истории переписки
        let transfer_data = null;
        $.ajax({
            url: "/chat/messages/" + type + "/" + chat_id,
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
                clearChatMessagesArea();
                updateChatMessageCounter(loading_messages_title);
            },
            success: function (data) {
                updateChatMessages(data.data.messages);
                updateChatMessageCounter(data.data.count);
            }
        });
    };

    if (chat_id !== "" && chat_type !== "") {
        setActiveChat(chat_type, chat_id);
    }

    let getLastMessageId = function (data) {
        test_msg = data.find(".chat_recipient").attr('data-last_msg');
        return test_msg;
    };

    $(document).on("click", ".chat_message", function (e) {
        let message_id = $(this).attr('data-message_id');
        let message_content = $(this).html();
        $(".chat_message_cite").html(message_content);
        $(".chat_message_cite").show();
        $("input[name='cite_id']").val(message_id);
        e.preventDefault();
        return false;
    });

    let setChatMessageItem = function(currentChatMessageItem){
        select_recipient_title.hide();
        $(".chat_window").show();

        $(".chat_message_item").removeClass("active_chat_item");
        currentChatMessageItem.addClass("active_chat_item");

        let user = getRecipientData(currentChatMessageItem);
        let chat_type = getChatType(user);
        let chat_id = getChatId(user);
        window.chat_type = chat_type;
        window.chat_id = chat_id;

        //идентификация по роли и пользователю
        let recipient_role = getRecipientRole(user);
        let recipient_id = getRecipientId(user);
        window.recipient_role = recipient_role;
        window.recipient_id = recipient_id;

        window.last_chat_message_id = getLastMessageId(currentChatMessageItem);

        // изменение урл чата
        let stateObj = {index: "index"};
        let new_url = null;

        if(isNaN(chat_id)){
            new_url = site_name + "/chat/recipient/" + recipient_role + "/" + recipient_id;
        } else {
            new_url =  site_name + "/chat/" + chat_type + "/" + chat_id;
            if(chat_type == "dialog"){
                $(".chat_room_container").hide();
            } else {
                $(".chat_room_container").show();
            }
        }
        history.pushState(stateObj, "", new_url);

        setRecipientData(user);
        // получение истории переписки
        let transfer_data = null;
        $.ajax({
            url: "/chat/messages/" + chat_type + "/" + chat_id,
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
                clearChatMessagesArea();
                updateChatMessageCounter(loading_messages_title);
            },
            success: function (data) {
                updateChatMessages(data.data.messages);
                updateChatMessageCounter(data.data.count);
                window.last_chat_message_id = data.data.last_message_id;
            }
        });

        if(chat_type == "room") {
            //загрузка списка собеседников для чата
            $.ajax({
                url: "/chat/recipients/" + chat_id,
                type: "post",
                dataType: 'json',
                data: transfer_data,
                beforeSend: function () {
                    clearChatRoomUserList();
                },
                success: function (data) {
                    updateChatRoomUserList(data.data, chat_room_user_html);
                }
            });
        }

    };

    $(document).on("click", ".chat_message_item", function (e) {

        let currentChatMessageItem = $(this);
        setChatMessageItem(currentChatMessageItem);
        e.preventDefault();
        return false;
    });

    $(document).on("submit", "#send_chat_message", function (e) {
        e.preventDefault();
        let $data = {};

        let form = $(this);
        let dataUrl = form.attr('action');
        form.find('input, textarea, select').each(function () {
            $data[this.name] = $(this).val();
        });
        let new_message = $data.message;
        sendMessage(dataUrl, $data, new_message);
    });

    let sendMessage = function (url, data, message) {
        $.ajax({
            url: url,
            type: "post",
            dataType: 'json',
            data: data,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status === "success") {
                    add_new_chat_message(message, sending_message_time);
                    if(data.data.dialog_id!=undefined){
                        let dialog_id = data.data.dialog_id;
                        window.chat_id = dialog_id;
                        window.chat_type= "dialog";
                        let new_url =  site_name + "/chat/dialog/" + dialog_id;
                        history.pushState({}, "", new_url);
                    }
                    clearChatTextarea();
                }
            }
        });
        $(".chat_message_cite").hide();
        $("input[name='cite_id']").val(null);
    };

    $(document).on("keyup", "#chat_textarea", function (e) {
        e.preventDefault();
        if ((e.keyCode == 10 || e.keyCode == 13) && e.ctrlKey) {

            let $data = {};
            let form = $("#send_chat_message");
            let dataUrl = form.attr('action');
            form.find('input, textarea, select').each(function () {
                $data[this.name] = $(this).val();
            });
            let new_message = $data.message;
            sendMessage(dataUrl, $data, new_message);
        }
    });

    reload_chat = function () {
        chat_id = window.chat_id;
        if(chat_id == "CHAT_ID") {
            chat_id = undefined;
        }
        if(chat_type == "undefined") {
            chat_type = undefined;
        }
        chat_type = window.chat_type;
        recipient_id = window.recipient_id;
        recipient_role = window.recipient_role;

        last_chat_message_id = window.last_chat_message_id;

        if ((chat_id == undefined || chat_type == undefined) && recipient_id == undefined || recipient_role == undefined) {
            return;
        }

        let reload_timer;
        let dialog_reload_route = "";

        if(chat_id == undefined && chat_type == undefined){
            dialog_reload_route =  "/chat/reload/recipient/" + recipient_role + "/" + recipient_id;
        } else {
            dialog_reload_route = "/chat/reload/" + chat_type + "/" + chat_id;
        }

        let transfer_data = "last_msg=" + last_chat_message_id;
        $.ajax({
            url: dialog_reload_route,
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
                clearInterval(reload_timer);
            },
            success: function (data) {
                let res = data.data;
                if (res.messages !== undefined && res.length > 0) {
                    updateChatMessages(res.messages);
                }

                updateChatMessageCounter(res.count);
                //updateRecipientList();
            }
        });
    };
    setInterval(reload_chat, 4000);

    let search_chat_user = function (query_string) {

        let reload_timer;
        let transfer_data = "q=" + query_string;
        $.ajax({
            url: "/chat/search/recipient",
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
                clearInterval(reload_timer);
            },
            success: function (data) {
                showSearchPopup(data.data.users);
                //updateChatUserPopup(data.messages);
                /*
                for (var i = 0; i < data.data.data.length; i++) {
                    var new_elem = $('<li data-original-index="0" class="selected"><a tabindex="0" data-tokens="null" role="option" aria-disabled="false" ' +
                        'aria-selected="true" data-id="' + data.data.data[i].id + '" data-name="' + data.data.data[i].company_name + '" ' +
                        'class="selected_user_list_select"><span class="text">' + data.data.data[i].company_name + '</span></a></li>');
                    $(".search_ajax_user_box").append(new_elem);

                }*/
            }
        });
    };

    $(document).on("click", ".chat_popup_item", function (e) {
        let currentItem = $(this).find(".chat_recipient");

        let chat_id = currentItem.attr("data-chat_id");
        let user_id = currentItem.attr("data-user_id");
        let user_role = currentItem.attr("data-user_role");
        let user_role_name = currentItem.attr("data-user_role_name");
        let last_message_id = currentItem.attr("data-last_message_id");
        let message_text = currentItem.attr("data-message_text");
        let message_time = currentItem.attr("data-message_time");
        let elem_name = currentItem.text();
        window.recipient_id = user_id;
        window.recipient_role = user_role;


        /*
        * если выбранный диалог или чат есть в списке, то переходит фокус на него,
        * если нет - добавляется в начало списка диалогов,
        * попап в любом случае закрывается
        * */
        let checkChatMessageExist = false;
        if(checkChatMessageExist){

        } else {
            let message = {};
            message.chat_id = chat_id;
            message.user_id = user_id;
            message.user_role = user_role;
            message.last_message_id = last_message_id;
            if(message_text=="null") {
                message.message_text = "<span class='small'>Переписка отсутствует</span>";
            } else {
                message.message_text = message_text;
            }
            message.message_time = message_time;
            message.user_role_name = user_role_name;
            message.user_name = elem_name;


            let format_html = formatRecipientListHtml(chat_message_html, message);
            $(".chat-user-list").prepend(format_html);
            $(".chat_message_item")[0].click();

        }



        hideSearchPopup();
        e.preventDefault();
        return false;
    });

    let purgeSearchPopupHtml = function(){
        searchPopup.html('');
    };

    searchPopup = $(".chat_search_popup");

    let showSearchPopup = function(data){
        purgeSearchPopupHtml();
        if(data.length>0){
            for(let i = 0; i<data.length; i++){
                let search_elem = data[i];
                let format_html = formatSearchItemHtml(search_popup_item, search_elem);
                searchPopup.append(format_html);
            }
        }
        searchPopup.show();
    };

    let hideSearchPopup = function(){
        purgeSearchPopupHtml();
        searchPopup.hide();
    };

    $(document).on("keyup", ".chat_search_user", function () {
        var last_string = "";
        var string = $(this).val();
        if (string.length <= 2) {
            return false;
        }
        var search_delay = function (string, last_string) {
            if (last_string == string) {
                return false;
            }
            search_chat_user(string);
            last_string = string;
        };
        setTimeout(search_delay(string, last_string), 500);
    });
});