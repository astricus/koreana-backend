$(document).ready(function () {

    $("#real_number_find_form").on("change", function () {
        $(".filter_call_form").submit();
    });

    $("#from_date_dp").on("change", function () {
        $(".filter_call_form").submit();
    });

    $("#to_date_dp").on("change", function () {
        $(".filter_call_form").submit();
    });

    $("#show_all").on("change", function () {
        $(".filter_call_form").submit();
    });

    $("#show_orders").on("change", function () {
        $(".filter_call_form").submit();
    });

    $("#object_id_find_form").on("change", function () {
        $(".filter_call_form").submit();
    });

    $(document).on("click", ".abort_reason_type", function () {
        var val = $(this).attr('data-id');
        if (val == "other") {
            $(".abort_reason_textarea").show(200).css({'display': 'block'});
        } else {
            $(".abort_reason_textarea").hide(200);
        }
    });

    $(document).on("click", ".abort_booking_status", function () {
        var val = $(this).attr('data-id');
        if (val == "0") {
            $(".abort_booking_container").show(200);
            $(".order_sum_container").hide(200);
        } else {
            $(".abort_booking_container").hide(200);
            $(".order_sum_container").show(200);
            $(".abort_reason_type").removeClass('sim_radio_elem_checked').addClass('sim_radio_elem');
        }
        $(".abort_reason_textarea").hide();
    });

    $(document).on("click", ".play_sound_button", function () {
        $(".audio_box").hide();
        var id = $(this).attr('data-id').trim();
        $("tr[audio_box=" + id + "]").show();
        $("audio").trigger('pause');
        $("tr[audio_box=" + id + "] audio").trigger('play');
    });

    $(document).on("click", ".sim_radio_button", function () {
        var id = $(this).attr('data-id');
        var elem = $(this).attr('data-elem');
        $("input[name='" + elem + "']").removeAttr('checked');
        var selector = 'input[name="' + elem + '"][value="' + id + '"]';
        $(selector).attr('checked', 'checked');
        $('div[data-elem="' + elem + '"][data-id!="' + id + '"]').removeClass('sim_radio_elem_checked').addClass('sim_radio_elem');
        $('div[data-elem="' + elem + '"][data-id="' + id + '"]').removeClass('sim_radio_elem').addClass('sim_radio_elem_checked');
    });

    $(document).on("submit", ".edit_order_form", function (x) {
        if (errorsOrder()) {
            console.log("form validate errors");
            x.preventDefault();
            return false;
        }

        if ($(this).hasClass("edit_call_order")) {
            var order_data = $('form[name="edit_order_form"]').serialise();
            $.ajax({
                url: "/call/edit_order_form/",
                type: "post",
                dataType: 'json',
                data: order_data,
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.status == "success") {
                        $("#save_object_user").removeClass('btn-warning');
                        $("#save_object_user").addClass('btn-success');
                    }

                    else if (data.status == "error") {
                        $("#save_object_user").removeClass('btn-warning');
                        $("#save_object_user").addClass('btn-danger');
                    }
                    else {
                        console.log(data.status);
                    }
                }
            });
        }
    });

    $(document).on("submit", ".save_order_form", function (x) {
        if (errorsOrder()) {
            console.log("form validate errors");
            x.preventDefault();
            return false;
        }
    });

    var errorsOrder = function () {
        $('.sim_radio_elem').removeClass("field_not_checked");
        var prev_name = "";
        var errorFound = false;
        $('.modal-body input[type="radio"]').each(function () {
            var name = $(this).attr('name');
            if (name !== prev_name && name !== "abort_reason") {
                if (!$('.modal-body input[name="' + prev_name + '"]').is(":checked") &&
                    !$('.modal-body input[name="' + name + '"]').is(":checked")
                ) {
                    console.log("field: " + name);
                    errorFound = 1;
                    $('.modal-body .sim_radio_elem[data-elem="' + name + '"]').addClass("field_not_checked");
                    return;
                }
            }
            var prev_name = $(this).attr('name');
        });
        if (errorFound) {
            console.log("errors found");
            return true;
        }
        console.log("no errors ");
        return false;
    };
});

$(document).ready(function () {

    $(document).on("click", ".call_order", function (event) {
        var call_order_form_content = $(".call_order_form").html();
        var id = $(this).attr('data-id');
        var form_call_date = $(this).attr('data-datetime');
        var phone_number = $(this).attr('data-phone');
        if (id > 0) {
            $(".modal-body").html(call_order_form_content);
            $(".modal-body form").append("<input type='hidden'  name='zadarma_call_id' value='" + id + "'>");
            $(".call_datetime").text(form_call_date);
            $(".modal-body form .form_call_number").text(phone_number);
            $(".modal-title").text('Анкета звонка');
        }
    });

    $(document).on("click", ".close_popup", function (event) {
        clear_calls_order();
    });

    $(document).on("click", "button.close", function (event) {
        clear_calls_order();
    });
});

var clear_calls_order = function () {
    setTimeout(function () {
        $(".modal-body").html('');
    }, 500);
};

$(document).on("click", ".call_order_view", function (event) {
    var call_order_result = $(this).next(".call_order_result").html();
    $(".modal-body").html(call_order_result);
    $(".modal-title").text('Анкета звонка');
});

var order_progress = function () {
    var progress = 0.0;

    $('input[type="radio"]').each(function () {
        if ($(this).is(":checked") && ($(this).attr('name') !== "abort_reason") && ($(this).attr('name') !== "booking_status")) {
            progress = progress + 8;
        }

        if ($(this).attr('value') == "other" && $(this).attr('name') == "abort_reason" && $(this).is(":checked")) {
            progress = progress + 4;
        } else if ($(this).attr('value') !== "other" && $(this).is(":checked") && $(this).attr('name') == "abort_reason") {
            progress = progress + 8;
        }

        if ($(this).is(":checked") && $(this).attr('name') == "booking_status") {
            if ($(this).attr('value') == "0") {
                progress = progress + 4;
            } else {
                progress = progress + 12;
            }
        }
    });

    if ($('input[name="abort_reason"][value="other"]').is(":checked")) {
        var comment = $('textarea[name="abort_reason_message"]').val();
        if (comment.length >= 3) {
            progress = progress + 4;
        }
    }

    $("form .progress-bar").css("width", progress + "%");
    if (progress >= 100) {
        $("form .progress").removeClass('progress-striped').removeClass('active');
        $("form .progress-bar").text("ГОТОВО!").addClass("progress-bar-success").removeClass("progress-bar-warning");
        $(".manage_order").removeAttr('disabled').addClass("btn-mint");
    } else {
        $("form .progress").addClass('progress-striped').addClass('active');
        $("form .progress-bar").text("").removeClass("progress-bar-success").addClass("progress-bar-warning");
        $(".manage_order").attr('disabled', true).removeClass("btn-mint");
    }
};

$(document).on("click", ".stop_manage", function () {
    $('.stop_manage').val("yes");
    $('form[name="call_manage_form"]').submit();
});

$(document).on("click", ".manage_status_set", function (e) {
    if (confirm('Вы уверены, что невозможно оценить звонок?')) {
        $('.manage_status').val("pass");
        $('form[name="call_manage_form"]').submit();
        e.preventDefault();
        return false;
    } else {
        e.preventDefault();
        return false;
    }
});

setInterval(function () {
    order_progress();
}, 200);


$(document).ready(function () {

    var dataSource;
    var getCallOrdersAjax = function () {
        var manager_id = $("input[name='manager_id']").val();
        var to_date = $("input[name='to_date']").val();
        var from_date = $("input[name='from_date']").val();
        var minute_price = $("input[name='minute_price']").val();
        var minimum_call_cost = $("input[name='minimum_call_cost']").val();
        var order_data = 'from_date=' + from_date + "&to_date=" + to_date + "&manager_id=" + manager_id +
        "&minute_price=" + minute_price + "&minimum_call_cost=" + minimum_call_cost;
        $.ajax({
            url: "/calls/stat/ajax",
            type: "post",
            dataType: 'json',
            data: order_data,
            beforeSend: function () {
            },
            success: function (data) {
                console.log(data);
                dataSource = data.data.results;
                var salary = data.data.salary.toFixed(2);
                var salary_log = data.data.salary_log;
                $(".salary_value").text(salary);
                $(".salary_log").html(salary_log);
                var chart = $("#chart").dxChart({
                    adaptiveLayout: {
                        width: 500
                    },
                    dataSource: dataSource,
                    commonSeriesSettings: {
                        type: "bar",
                        hoverMode: "allArgumentPoints",
                        selectionMode: "allArgumentPoints",
                    },
                    customizePoint: function () {
                        if (this.value > 0) {
                            return {color: "#ff7c7c", hoverStyle: {color: "#ff7c7c"}};
                        } else {
                            return {color: "#8c8cff", hoverStyle: {color: "#8c8cff"}};
                        }
                    },
                    series: [{
                        argumentField: "date",
                        valueField: "delta",
                        selectionStyle: {
                            color: "#10ac0d",
                        },
                        label: {
                            backgroundColor: "#ffffff",
                            font: {
                                color: "#333",
                                family: "Helvetica Neue",
                            },
                            visible: true,
                            customizeText: function () {
                                return this.total;
                            },
                            position: "outside",
                        }
                    }],
                    tooltip: {
                        enabled: true,
                        location: "edge",
                        customizeTooltip: function (arg) {

                            //if (arg.seriesName == "Неудачные") {
                                return {
                                    html: "<div class='call_abort_tooltip' style='z-index: 100000'>" +
                                    "звонок длился, сек: " + arg.point.data.seconds_talk +
                                    "<br>обрабатывался, сек: " + arg.point.data.process_time +
                                    "<br>дата: " + arg.point.data.date +
                                    "</div>"
                                };
                            //}
                        }
                    },
                    argumentAxis: {
                        grid: {
                            visible: false
                        },
                        tickInterval: 5,
                        minorGrid: {
                            visible: true
                        },
                        label: {
                            visible: false,
                        },
                    },
                    valueAxis: {
                        tickInterval: 50
                    },
                    legend: {
                        visible: false
                    },
                    commonPaneSettings: {
                        border: {
                            visible: true
                        }
                    }
                }).dxChart("instance");
            }
        });
    };
    getCallOrdersAjax();

    // обработка скрипта просмотра статистики звонков
    $("#manager_stat_find_form").on("change", function () {
        $(".filter_call_form").submit();
    });

    $("#from_date_dp").on("change", function () {
        $(".filter_call_form").submit();
    });

    $("#to_date_dp").on("change", function () {
        $(".filter_call_form").submit();
    });
});


