$(document).ready(function () {

    $(document).on("click", ".call_order", function (event) {
        console.log("init");
        var call_order_form_content = $(".call_order_form").html();
        var id = $(this).attr('data-id');
        var form_call_date = $(this).attr('data-datetime');
        var phone_number = $(this).attr('data-phone');
        if (id > 0) {
            $(".modal-body").html(call_order_form_content);
            $(".modal-body form").append("<input type='hidden'  name='zadarma_call_id' value='" + id + "'>");
            //$(".modal-body form input[name='form_call_date']").val(form_call_date);
            $(".call_datetime").text(form_call_date);
            console.log("7" + form_call_date);
            $(".modal-body form .form_call_number").text(phone_number);
            $(".modal-title").text('Анкета звонка');
        }
    });

    $(document).on("click", ".close_popup", function (event) {
        clear_calls_order();
    });

    $(document).on("click", "button.close", function (event) {
        clear_calls_order();
    });
});

var clear_calls_order = function () {
    setTimeout(function(){
        $(".modal-body").html('');
    }, 500);
};

$(document).on("click", ".call_order_view", function (event) {
    var call_order_result = $(this).next(".call_order_result").html();
    $(".modal-body").html(call_order_result);
    $(".modal-title").text('Анкета звонка');
});

$(document).on("click", "body", function (event) {
    $(".manager_popup_menu").slideUp(180);
});

$(document).on("click", ".manager_box", function (event) {
    event.stopPropagation();
    $(".manager_popup_menu").slideDown(180);
});

$(document).on("click", ".manager_popup_menu", function (event) {
    event.stopPropagation();
});