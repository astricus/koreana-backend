$(document).ready(function(){

    $(document).on("keyup", "#dropdownUserByManagerSearch", function () {

        var last_string = "";
        var string = $(this).val();

        if (string.length <= 2) {
            return false;
        }

        var search_delay = function (string, last_string) {

            if(last_string==string) {
                return false;
            }
            var transfer_data = "string=" + string;
            $.ajax({
                url: "/user/search_by_manager_ajax",
                type: "post",
                dataType: 'json',
                data: transfer_data,
                beforeSend: function () {
                },
                success: function (data) {
                        console.log(data);
                    $(".search_ajax_user_by_manager_box").html('');

                    if (data.data.status == "error") {
                    }
                    else {

                        for (var i = 0; i < data.data.data.length; i++) {

                            var new_elem = $('<li data-original-index="0" class="selected"><a tabindex="0" data-tokens="null" role="option" aria-disabled="false" ' +
                                'aria-selected="true" data-id="' + data.data.data[i].id + '" data-name="' + data.data.data[i].company_name + '" ' +
                                'class="selected_user_list_item"><span class="text">' + data.data.data[i].company_name + '</span></a></li>');
                            $(".search_ajax_user_by_manager_box").append(new_elem);
                        }
                    }
                }
            });
            last_string = string;
        };

        setTimeout(search_delay(string, last_string), 1000);
    });


    $(document).on("change", "#set_user_id", function () {
        $("#save_object_user").removeClass('btn-default');
        $("#save_object_user").addClass('btn-warning');
    });

    // сохранение связи клиент - объект
    $(document).on("click", "#save_object_user", function () {

        var user_id = $("#set_user_id option:selected").val();
        var object_id = $(this).attr('data-object_id');
        var transfer_data = "user_id=" + user_id + "&object_id=" + object_id;
        $.ajax({
            url: "/set_user_object/",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == "success") {
                    $("#save_object_user").removeClass('btn-warning');
                    $("#save_object_user").addClass('btn-success');
                }

                else if (data.status == "error") {
                    $("#save_object_user").removeClass('btn-warning');
                    $("#save_object_user").addClass('btn-danger');
                }
                else {
                    console.log(data.status);
                }
            }
        });
    });

    /* возможно потребуется сделать аякс проверку свободы вводимого email*/
    var check_login = function(login){
        var transfer_data = "login=" + login;
        $.ajax({
            url: "/user/check_ajax_login/",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.data.count == "1") {
                    $(".login_result").addClass('has-error').removeClass('has-success');
                    $(".check_ajax_login").attr('disabled', true);
                }
                else if (data.data.count == "0") {
                    $(".login_result").removeClass('has-error').addClass('has-success');
                    $(".check_ajax_login").removeAttr('disabled');
                }
                else {
                    console.log(data.status);
                }
            }
        });
    };

    // сохранение связи объект - менеджер
    $(document).on("click", "#save_object_manager", function () {

        var manager_id = $("#set_object_manager_id option:selected").val();
        var object_id = $(this).attr('data-object_id');
        var transfer_data = "object_id=" + object_id + "&manager_id=" + manager_id;
        $.ajax({
            url: "/set_manager_object/",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == "success") {
                    $("#save_object_manager").removeClass('btn-warning');
                    $("#save_object_manager").addClass('btn-success');
                }

                else if (data.status == "error") {
                    $("#save_object_manager").removeClass('btn-warning');
                    $("#save_object_manager").addClass('btn-danger');
                }
                else {
                    console.log(data.status);
                }
            }
        });
    });

    $(document).on("change", "#set_user_id", function () {
        /*TODO * навесить правильные кнопки в темплейте */
        $("#save_object_user").removeClass('btn-default');
        $("#save_object_user").addClass('btn-warning');
    });

    // сохранение связи клиент - объект
    $(document).on("click", "#save_user_object_item", function () {

        var object_id = $("select[name='set_user_object_id'] option:selected").val();
        var user_id = $(this).attr('data-user_id');
        var transfer_data = "object_id=" + object_id + "&user_id=" + user_id;
        $.ajax({
            /*TODO * экшин пока отсутствиет */
            url: "/set_user_current_user_object/",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == "success") {
                    $("#save_user_object_item").removeClass('btn-warning');
                    $("#save_user_object_item").addClass('btn-success');
                    console.log("eee");
                }

                else if (data.status == "error") {
                    $("#save_user_object_item").removeClass('btn-warning');
                    $("#save_user_object_item").addClass('btn-danger');
                }
                else {
                    console.log(data.status);
                }
            }
        });
    });

});