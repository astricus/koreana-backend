$(document).ready(function(){

    $(document).on("keyup", "#dropdownObjectSearch", function () {

        var last_string = "";
        var string = $(this).val();

        if (string.length <= 2) {
            return false;
        }

        var search_delay = function (string, last_string) {

            if(last_string==string) {
                return false;
            }
            var transfer_data = "string=" + string;
            $.ajax({
                url: "/object/search_ajax",
                type: "post",
                dataType: 'json',
                data: transfer_data,
                beforeSend: function () {

                },
                success: function (data) {
                    console.log(data);

                    $(".search_ajax_object_box").html('');

                    if (data.data.status == "error") {
                    }
                    else {

                        for (var i = 0; i < data.data.data.length; i++) {
                            var new_elem = $('<li data-original-index="0" class="selected"><a ' +
                                //'tabindex="0" ' +
                                //'data-tokens="null" role="option" aria-disabled="false" ' +
                                //'aria-selected="true" ' +
                                'href="/object/' + data.data.data[i].id + '?x=1">' +
                                //'data-id="' + data.data.data[i].id + '" data-name="' + data.data.data[i].name + '" ' +
                                //'class="selected_user_list_item"
                                '<span class="text">' + data.data.data[i].name + '</span></a></li>');
                            $(".search_ajax_object_box").append(new_elem);
                        }
                    }
                }
            });
            last_string = string;
        };

        setTimeout(search_delay(string, last_string), 500);
    });

    $(document).on("keyup", "#dropdownUserSearchConnector", function () {

        var last_string = "";
        var string = $(this).val();

        if (string.length <= 2) {
            return false;
        }

        var search_delay = function (string, last_string) {

            if(last_string==string) {
                return false;
            }
            var transfer_data = "string=" + string;
            $.ajax({
                url: "/user/search_ajax",
                type: "post",
                dataType: 'json',
                data: transfer_data,
                beforeSend: function () {

                },
                success: function (data) {
                    console.log(data);

                    $(".search_ajax_user_box").html('');

                    if (data.data.status == "error") {
                    }
                    else {

                        for (var i = 0; i < data.data.data.length; i++) {
                            var new_elem = $('<li data-original-index="0" class="selected"><a tabindex="0" data-tokens="null" role="option" aria-disabled="false" ' +
                                'aria-selected="true" data-id="' + data.data.data[i].id + '" data-name="' + data.data.data[i].company_name + '" ' +
                                'class="selected_user_list_select"><span class="text">' + data.data.data[i].company_name + '</span></a></li>');
                            $(".search_ajax_user_box").append(new_elem);
                        }
                    }
                }
            });
            last_string = string;
        };

        setTimeout(search_delay(string, last_string), 500);
    });



    $(document).on("click", ".selected_manager_list_item", function () {
        var selected_manager_id = $(this).attr('data-id');
        if(selected_manager_id>0) {
            new_url = updateQueryStringParameter(window.location.href, 'manager_id', selected_manager_id);
        } else {
            new_url = updateQueryStringParameter(window.location.href, 'manager_id', '');
        }
        redirect(new_url);
    });

    $(document).on("click", ".selected_user_list_item", function () {
        var selected_user_id = $(this).attr('data-id');
        if(selected_user_id>0) {
            new_url = updateQueryStringParameter(window.location.href, 'user_id', selected_user_id);
        } else {
            new_url = updateQueryStringParameter(window.location.href, 'user_id', '');
        }
        redirect(new_url);
    });

    $(document).on("click", ".selected_user_list_item", function (evt) {
        evt.preventDefault();
        var selected_user_id = $(this).attr('data-id');
        var selected_user_name = $(this).attr('data-name');
        //$("#dropdownUserSearch").val(selected_user_name);
        new_url = updateQueryStringParameter(window.location.href, 'user_id', selected_user_id);
        redirect(new_url);
    });

    $(document).on("change", "#search_ajax_open_only", function () {
        var state = $(this).is(':checked') ? 'on' : '';
        new_url = updateQueryStringParameter(window.location.href, 'open_only', state);
        redirect(new_url);
    });

    $(document).on("change", "#search_ajax_active_only", function () {
        var state = $(this).is(':checked') ? 'on' : '';
        new_url = updateQueryStringParameter(window.location.href, 'active_only', state);
        redirect(new_url);
    });

    $(document).on("change", "#search_ajax_my_only", function () {
        var state = $(this).is(':checked') ? 'on' : '';
        new_url = updateQueryStringParameter(window.location.href, 'stopped_offers', state);
        redirect(new_url);
    });

    $(document).on("change", ".selected_user_list_select", function () {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        console.log(id + " " + name);
        $(".selected_user_option").html("<option value='" + id + "'>" + name + "</option>");
    });

    $(document).ready(function() { $(".use_select2").select2(); });

    $(document).ready(function() {
        $(".user_objects_use_select2").select2();
    });


});