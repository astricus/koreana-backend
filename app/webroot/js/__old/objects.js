$(document).ready(function(){

    // сохранение связи урл - объект
    $(document).on("click", "#add_url_to_object", function () {

        var url = $("#add_url").val();
        var object_id = $(this).attr('data-object_id');
        var transfer_data = "url=" + url + "&object_id=" + object_id;
        if(url==""){
            $("#add_url").addClass("input_error");
            return;
        }

        $.ajax({
            url: "/set_url_object/",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == "success") {
                    $("#add_url").val('');
                    $("#add_url").removeClass("input_error");
                    $("#add_url_to_object").removeClass('btn-warning');
                    $("#add_url_to_object").addClass('btn-success');
                    $(".object_link_container").append('<div class="A_phone_container">' +
                        '<a href="' + url + '" class="A_phone" target="_blank">' + url + '</a>' +
                    '<a href="#" class="btn btn-sm btn-warning" title="удалить">' +
                        '<i class="pli-cross"></i></a>' +
                    '</div>');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }

                else if (data.status == "error") {
                    $("#add_url_to_object").removeClass('btn-warning');
                    $("#add_url_to_object").addClass('btn-danger');
                }
                else {
                    console.log(data.status);
                }
            }
        });
    });

    // сохранение связи телефон - объект
    $(document).on("click", "#add_phone_to_object", function () {

        var phone = $("#add_real_phone option:selected").val();
        var object_id = $(this).attr('data-object_id');
        var transfer_data = "phone=" + phone + "&object_id=" + object_id;
        if(phone == undefined) {
            return;
        }
        $.ajax({
            url: "/object/connect_real_phone/",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status == "success") {
                    $("#add_phone_to_object").removeClass('btn-warning');
                    $("#add_phone_to_object").addClass('btn-success');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }

                else if (data.status == "error") {
                    $("#add_phone_to_object").removeClass('btn-warning');
                    $("#add_phone_to_object").addClass('btn-danger');
                }
                else {
                    console.log(data.status);
                }
            }
        });
    });

    $(document).on("click", "#add_url_field", function () {
        var new_link = $('<br><input placeholder="url сайта" class="form-control small_input" type="text" value="" name="url[]">');
        $(".url_holder").append(new_link);
    });

    $(document).on("click", ".add_phone_button", function () {
        $(".add_phone_field").show();
    });



});

