let form_type_fields;
$(document).on('nifty.ready', function () {

    $('.summernote').summernote({
        'height': '230px',
        callbacks: {
            onImageUpload: function (files, editor, welEditable) {

                for (let i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });

    function sendFile(file, el) {
        let form_data = new FormData();
        form_data.append('file', file);
        $.ajax({
            data: form_data,
            dataType: 'json',
            type: "POST",
            url: '/welcome_screen_uploader/',
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status === "success") {
                    $(el).summernote('editor.insertImage', data.data.url);
                } else {
                    alert(data.data.message);
                }

            }
        });
    }

    setInterval(function () {
        $("#page_content").val($(".summernote").summernote('code'));
    }, 120);

    $('#save-text').on('click', function () {
        $('#summernote-edit').summernote('destroy');
    });

    $(".datepicker").datepicker();

});