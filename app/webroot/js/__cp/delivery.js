//Запрос стоимости доставки
$(document).on("click", "#getCostDeliveryLink", function () {
    var deliveryId = $(this).data('orderId');
    var TkId = $("#getCostDeliverySelect option:selected").val();
    $.ajax({
        url: "/delivery/ajax/getCostDelivery",
        type: "post",
        dataType: 'json',
        data: {'deliveryId': deliveryId, 'TkId': TkId},
        beforeSend: function () {
            $('#setCostDelivery').empty();
            $('#setCostDelivery').html("<h5>Обработка события...</h5>");
        },
        success: function (data) {
            $('#setCostDelivery').empty();
            var html = '';
            $.each(data, function (index, value) {
                html += "<h5>" + value.provider_title + "</h5>";
                html += "<p>Полная стоимость: " + value.delivery_cost_data.total_cost + " р.</p>";
                html += "<p>Время в пути: " + value.delivery_cost_data.period.min + " - " + value.delivery_cost_data.period.max + " д.</p>";
                html += "<p>Подробно:</p>";
                html += "<ul>";
                $.each(value.delivery_cost_data.detail_cost, function (i, v) {
                    html += "<li>" + v.cost + " р. (" + v.name + ")</li>";
                });
                html += "</ul>";
            });
            $('#setCostDelivery').append( html );
        }
    });
});
//бновление статуса доставки
$(document).on("click", "#getStatusDeliveryLink", function () {
    var deliveryId = $(this).data('orderId');
    var status = $("#getStatusDeliverySelect option:selected").val();
    $.ajax({
        url: "/delivery/ajax/setStatusDelivery",
        type: "post",
        dataType: 'json',
        data: {'deliveryId': deliveryId, 'status': status},
        beforeSend: function () {
            $('#setStatusDelivery').empty();
            $('#setStatusDelivery').html("<h5>Обработка события...</h5>");
        },
        success: function (data) {
            $('#setStatusDelivery').empty();
            if (data.status == 'OK') {
                $('#setStatusDelivery').append(data.message);
            }
        }
    });
});

//создать новую доставку
var companyStore = '';
var providerStore = '';
var clientAddress = '';

$(document).on("change", "#selectTypeDelivery", function () {
    $('#routeDeliveryTo').empty();
    $('#routeDeliveryFrom').empty();
    $('#newDelivery').attr('disabled', false);
    var typeDelivery = $("#selectTypeDelivery").val();
    if(typeDelivery != '0'){
        switch(typeDelivery){
            case 'from_company_to_provider_store':
                $('#routeDeliveryTo').append(companyStore);
                $('#routeDeliveryFrom').append(providerStore);
                break;
            case 'from_provider_store_to_orderer':
                $('#routeDeliveryTo').append(providerStore);
                $('#routeDeliveryFrom').append(clientAddress);
                break;
            case 'from_company_to_orderer':
                $('#routeDeliveryTo').append(companyStore);
                $('#routeDeliveryFrom').append(clientAddress);
                break;
            case 'from_orderer_to_provider_store':
                $('#routeDeliveryTo').append(clientAddress);
                $('#routeDeliveryFrom').append(providerStore);
                break;
            case 'from_provider_store_to_company':
                $('#routeDeliveryTo').append(providerStore);
                $('#routeDeliveryFrom').append(companyStore);
                break;
            case 'from_orderer_to_company':
                $('#routeDeliveryTo').append(clientAddress);
                $('#routeDeliveryFrom').append(companyStore);
                break;
        }
    }
    else{
        $('#newDelivery').attr('disabled', true);
        $('#routeDeliveryTo').empty();
        $('#routeDeliveryFrom').empty();
    }
});


$(document).on("click", "#buttonOpenDelivery", function () {
    getClientAddress();
    getCompanyStores();
    getProviderStores();
});

function getCompanyStores(){
    if(companyStore == ''){
        $.get( "/delivery/ajax/getCompanyStores", function( data ) {
            companyStore = '<select name="companyStore">';
            $.each( JSON.parse(data), function( i, v ){
                companyStore += '<option value="'+v.id+'">'+v.name+'</option>';
            });
            companyStore += '</select>';
        });
    }
}
function getProviderStores(){
    if(providerStore == ''){
        $.get( "/delivery/ajax/getProviderStores", function( data ) {
            providerStore = '<select name="providerStore">';
            $.each( JSON.parse(data), function( i, v ){
                providerStore += '<option value="'+v.id+'">'+v.name+'</option>';
            });
            providerStore += '</select>';
        });
    }
}
function getClientAddress(){
    if(clientAddress == ''){
        $.get( "/delivery/ajax/getClientDataAll", function( data ) {
            clientAddress = '<select name="clientOrder">';
            $.each( JSON.parse(data), function( i, v ){
                clientAddress += '<option value="'+v.id+'">'+v.name+'</option>';
            });
            clientAddress += '</select>';
        });
    }
}

var setPriceFieldStatus = function (elem, status) {
    elem.removeClass('wait_save_field');
    elem.removeClass('success_save_field');
    elem.removeClass('error_save_field');
    elem.addClass(status);
};

$(document).on("keyup", ".update_price_field", function () {

    var last_price = "";
    var limit = $(this).attr("data-limit");
    var zone = $(this).attr("data-zone");
    var price = $(this).val();
    var price_field = $(this);

    var action_delay = function (price, last_price) {

        if (last_price == price || price.length <= 1) {
            return false;
        }
        setPriceFieldStatus(price_field, 'wait_save_field');
        var transfer_data = "limit=" + limit + "&zone=" + zone + "&price=" + price;
        $.ajax({
            url: "/delivery_marketing/save_price/",
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
                setPriceFieldStatus(price_field, 'wait_save_field');
            },
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    setPriceFieldStatus(price_field, 'success_save_field');
                    showNotify(
                        ' Маркетинговая цена была успешно обновлена',
                        'Новое событие в системе',
                        '<i class="fa fa-cogs icon-lg"></i>',
                        'success');

                } else {
                    setPriceFieldStatus(price_field, 'error_save_field');
                    showNotify(
                        ' Маркетинговая цена некорректна',
                        'Новое событие в системе',
                        '<i class="fa fa-cogs icon-lg"></i>',
                        'danger');
                }


            }
        });
        last_price = price;
    };

    setTimeout(action_delay(price, last_price), 500);
});