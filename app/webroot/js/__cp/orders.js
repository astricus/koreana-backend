$(document).ready(function(){

    /* город */
    $(document).on("change", ".dropdownOrdererCitySearch", function () {
        var city_id = $('.dropdownOrdererCitySearch option:selected').val(), new_url;
        if(city_id>0) {
            new_url = updateQueryStringParameter(window.location.href, 'city_id', city_id);
        } else {
            new_url = updateQueryStringParameter(window.location.href, 'city_id', '');
        }
        redirect(new_url);
    });

    /* магазин */
    $(document).on("change", ".dropdownShopSearch", function () {
        var selected_shop_id = $('.dropdownShopSearch option:selected').val(), new_url;
        if(selected_shop_id>0) {
            new_url = updateQueryStringParameter(window.location.href, 'shop_id', selected_shop_id);
        } else {
            new_url = updateQueryStringParameter(window.location.href, 'shop_id', '');
        }
        redirect(new_url);
    });

    /* заказчик */
    $(document).on("change", ".dropdownOrdererSearch", function () {
        var selected_shop_orderer_id = $('.dropdownOrdererSearch option:selected').val(), new_url;
        if(selected_shop_orderer_id>0) {
            new_url = updateQueryStringParameter(window.location.href, 'orderer_id', selected_shop_orderer_id);
        } else {
            new_url = updateQueryStringParameter(window.location.href, 'orderer_id', '');
        }
        redirect(new_url);
    });

    /* статус*/
    $(document).on("change", ".dropdownOrderStatus", function (evt) {
        evt.preventDefault();
        var selected_order_status = $('.dropdownOrderStatus option:selected').val(), new_url;
        if(selected_order_status!=undefined) {
            new_url = updateQueryStringParameter(window.location.href, 'status', selected_order_status);
        } else {
            new_url = updateQueryStringParameter(window.location.href, 'status', '');
        }
        redirect(new_url);
    });

    /* обработчик смены статуса доставки заказа*/
    var delivery_result;

    if($("#sw-sz-lg").length>0) {
        var changeCheckbox = document.getElementById('sw-sz-lg');
        new Switchery(changeCheckbox, {size: 'large'});
        changeCheckbox.onchange = function () {
            if (changeCheckbox.checked) {
                $(".delivery_status_accept_text").show();
                $(".delivery_status_reject_text").hide();
            } else {
                delivery_result = 'доставка не требуется';
                $(".delivery_status_accept_text").hide();
                $(".delivery_status_reject_text").show();
            }
        };
    }

    $(document).on("click", ".remove_or_return_product_position", function () {
        var optionSelected = $("option:selected", this);
        var position_id = this.value;
        var category_name = $(".productCategoryParamsSearch option:selected").text();
        var transfer_data = "category_id=" + category_id;
        $.ajax({
            url: "/product_category/params_search_ajax/" + category_id,
            type: "post",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
            },
            success: function (data) {
                //console.log(data);
                $(".category_product_param_box").html('');

                // указывается название выбраной категории товара
                $(".selected_category_name").text(category_name);

                if (data.data.status == "error") {
                } else {

                    var param_data = data.data;

                    for (var i = 0; i < param_data.length; i++) {

                        var cur_param_data = param_data[i];
                        console.log(cur_param_data);
                        var new_elem;
                        if (cur_param_data.param.param_type == "color") {
                            new_elem = color_picker_param_box(
                                cur_param_data.param.default_value,
                                cur_param_data.param.param_name,
                                cur_param_data.param.id
                            );
                        } else if (cur_param_data.param.param_type == "number") {
                            new_elem = number_range_param_box(
                                cur_param_data.param.default_value,
                                cur_param_data.param.param_name,
                                cur_param_data.param.id,
                                cur_param_data.param.value_interval,
                                cur_param_data.param.min_value,
                                cur_param_data.param.max_value
                            );
                        } else if (cur_param_data.param.param_type == "checkbox") {
                            new_elem = checkbox_param_box(
                                cur_param_data.param.default_value,
                                cur_param_data.param.param_name,
                                cur_param_data.param.id,
                                cur_param_data.param_values
                            );
                        } else {
                            new_elem = "";
                        } /*
                        var new_elem = $('<li data-original-index="0" class="selected"><a ' +
                            //'tabindex="0" ' +
                            //'data-tokens="null" role="option" aria-disabled="false" ' +
                            //'aria-selected="true" ' +
                            'href="/object/' + data.data[i].id + '?x=1">' +
                            //'data-id="' + data.data.data[i].id + '" data-name="' + data.data.data[i].name + '" ' +
                            //'class="selected_user_list_item"
                            '<span class="text">' + data.data[i].param_name + '</span></a></li>');
                            */
                        $(".category_product_param_box").append(new_elem);
                    }
                }
            }
        });
    });

});