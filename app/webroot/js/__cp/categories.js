/* обработчик смены статуса доставки заказа*/
var delivery_result;

$(document).ready(function () {
    if ($("#category_status_switcher").length > 0) {
        console.log(1);
        var changeCheckbox = document.getElementById('category_status_switcher');
        new Switchery(changeCheckbox);
        changeCheckbox.onchange = function () {
            if (changeCheckbox.checked) {

                $(".category_status_accept_text").hide();
                $(".category_status_reject_text").show();
            } else {
                $(".category_status_accept_text").show();
                $(".category_status_reject_text").hide();
            }
        };
    }
});

$(document).on("change", ".dropdownCategoryMove", function () {
    console.log("now move");
    let new_parent_category_id = $(this).find('option:selected').val();
    let category_id = $(this).attr('data-id');
    let transfer_data = null;
    $.ajax({
        url: "/category/move/" + category_id + "?parent_id=" + new_parent_category_id,
        type: "post",
        dataType: 'json',
        data: transfer_data,
        beforeSend: function () {
        },
        success: function (data) {
            if (data.data.status == "error") {
                setTimeout(showNotify(
                data.data.message,
                data.data.message,
                '<i class="fa fa-cogs icon-lg"></i>',
                'error'),
                1000);
            } else {
                setTimeout(showNotify(
                "категория успешно перенесена",
                data.data.message,
                '<i class="fa fa-cogs icon-lg"></i>',
                'success'),
                1000);
            }
        }
    });
});


$(document).on("click", "#action_save_new_category_filter", function () {
    var $data = {};

    var form = $('form[name="add_category_filter"]');
    var dataUrl = form.attr('action');
    form.find('input, textarea, select').each(function () {
        // добавим новое свойство к объекту $data
        // имя свойства – значение атрибута name элемента
        // значение свойства – значение свойство value элемента
        $data[this.name] = $(this).val();
    });


    $("input[name='param_values[]'").each(function (i,e) {
        console.log($(this).val());
        // добавим новое свойство к объекту $data
        // имя свойства – значение атрибута name элемента
        // значение свойства – значение свойство value элемента
        $data['param_values[' + i + ']'] = $(this).val();
    });


    //var transfer_data = "query_text=" + string;
    $.ajax({
        url: dataUrl,
        type: "post",
        dataType: 'json',
        data: $data,
        beforeSend: function () {

        },
        success: function (data) {

            if (data.data.status == "error") {

            } else {

                $('#myModal').hide();
                $('.modal-backdrop').hide();
                setTimeout(showNotify(
                    data.data.message,
                    data.data.message,
                    '<i class="fa fa-cogs icon-lg"></i>',
                    'success'),
                    1000);
                //$("#prb_query_result").val(data.data.result);
            }
        }
    });
});

$(document).on("click", ".load_category_filter_data", function () {
    var filter_id =  $(this).attr('data-filter_id');
    var dataUrl =  $(this).attr('href');
    $.ajax({
        url: dataUrl,
        type: "post",
        dataType: 'json',
        data: null,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
            if (data.data.status == "error") {

            } else {

                $('#categoryFilterModal').hide();
                $('.modal-backdrop').hide();
                setTimeout(showNotify(
                    data.data.message,
                    data.data.message,
                    '<i class="fa fa-cogs icon-lg"></i>',
                    'success'),
                    1000);
            }
        }
    });
});


$(document).on("click", ".param_value_add_item", function (e) {
    e.preventDefault();
    var inputParamValueBlock = $(".param_value_item").eq(0).clone();

    $(".param_value_container").append(inputParamValueBlock);
    $(".param_value_container .param_value_item").show();
    $(".param_value_container .param_value_item").css({'display' : 'block'});
    return false;

});

$(document).on("click", "#action_add_category", function () {
    var form = $('form[name="add_category"]');
    var dataUrl = form.attr('action');
    var category_id = $('#category_id option:selected').val();
    var category_name = $('#category_name').val();

    var transfer_data = "category_id=" + category_id + "&category_name=" + category_name;
    $.ajax({
        url: dataUrl,
        type: "post",
        dataType: "json",
        data: transfer_data,
        beforeSend: function () {
        },
        success: function (data) {
            console.log("res");
            console.log(data);
            if (data.data.status == "error") {
                showNotify(
                    data.data.message,
                    data.data.message,
                    '<i class="fa fa-cogs icon-lg"></i>',
                    'danger'
                );
            } else {
                $('#addCategoryModal').hide();
                $('.modal-backdrop').hide();
                showNotify(
                    data.data.message,
                    data.data.message,
                    '<i class="fa fa-cogs icon-lg"></i>',
                    'warning'
                );
            }
        }
    });
});