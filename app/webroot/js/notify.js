var showNotify = function(message_text, message_title, message_icon, style){
    var message_content = message_icon + " " + message_text;

    $.niftyNoty({
        type: style,
        container : 'floating',
        title : message_title,
        message : message_content,
        closeBtn : false,
        timer : 2800,
        onHide:function(){
            //alert("onHide Callback");
        }
    });
};