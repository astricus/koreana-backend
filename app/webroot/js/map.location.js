ymaps.ready(init);
var myMap;

let def_map_lat = 57.5262;
let def_map_lon = 38.3061;

if(lat==undefined){
    lat = def_map_lat;
}
if(lon==undefined){
    lon = def_map_lon;
}
function init() {

    myMap = new ymaps.Map("map", {
        center: [lat, lon],
        zoom: 11
    }, {
        balloonMaxWidth: 200,
        searchControlProvider: 'yandex#search'
    });

    // Обработка события, возникающего при щелчке
    // левой кнопкой мыши в любой точке карты.
    // При возникновении такого события откроем балун.
    myMap.events.add('click', function (e) {
        if (!myMap.balloon.isOpen()) {
            var coords = e.get('coords');
            console.log(e.get('coords'));
            $("input[name='lat']").val(coords[0].toPrecision(6));
            $("input[name='lon']").val(coords[1].toPrecision(6));
            getAddress(coords);
            myMap.balloon.open(coords, {
                contentHeader: 'Событие!',
                contentBody: '<p>Вы выбрали местоположение на карте.</p>' +
                    '<p>Координаты места: ' + [
                        coords[0].toPrecision(6),
                        coords[1].toPrecision(6)
                    ].join(', ') + '</p>',
                contentFooter: '<sup>Щелкните еще раз</sup>'
            });
        } else {
            myMap.balloon.close();
        }
    });

    // Обработка события, возникающего при щелчке
    // правой кнопки мыши в любой точке карты.
    // При возникновении такого события покажем всплывающую подсказку
    // в точке щелчка.
    myMap.events.add('contextmenu', function (e) {
        myMap.hint.open(e.get('coords'), 'Кто-то щелкнул правой кнопкой');
        // console.log(e.get('coords'));
    });

    // Скрываем хинт при открытии балуна.
    myMap.events.add('balloonopen', function (e) {
        myMap.hint.close();
    });

    function getAddress(coords) {
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);
            console.log('city: ' + firstGeoObject.properties.get('name'));
            console.log('text: ' + firstGeoObject.properties.get('text'));
            console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
            console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
            console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
            console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));
            $("#address").val(firstGeoObject.properties.get('text'));
        });
    }
}