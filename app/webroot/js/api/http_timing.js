/**
 * Created by user on 18.02.2020.
 */

/*
*  TCP Connection = Pre Transfer (95 ms) - DNS Lookup (29 ms) =  66 millseconds
 Content Generation = Start Transfer (166 ms) - Pre Transfer (95 ms) =  71 milliseconds
 Content Transfer = Total (530 ms) - Start Transfer (166 ms) = 364 milliseconds
* */
//        total_time, dns_time, connect_time, start_transfer_time
//STARTTRANSFER_TIME
var timing_width, view_http_timing_function, calculate_timings_width;
$(document).ready(function () {
    timing_width = $(".timings").width()*1;
    calculate_timings_width = function (timing_width, total_time) {
        return timing_width/total_time;
    };

    view_http_timing_function = function (timing_width, total_time, dns_time, connect_time, pre_transfer_time, start_transfer_time) {
        console.log(
            timing_width +
        " " + total_time +
        " " + dns_time + " "
        +  connect_time + " "
        +  pre_transfer_time + " "
        +  start_transfer_time
        );
        //var px_per_ms = calculate_timings_width(timing_width, total_time, dns_time, connect_time, pre_transfer_time, start_transfer_time);
        dns_time = dns_time*1;
        var DNS_Lookup_Time = dns_time.toFixed(4);//ms
        if(DNS_Lookup_Time<=0){
            DNS_Lookup_Time = 0.001;
        }
        var TCP_Connection_Time = (pre_transfer_time-dns_time).toFixed(4);//ms
        if(TCP_Connection_Time<=0){
            TCP_Connection_Time = 0.001;
        }
        var TCP_Content_Generation_Time = (start_transfer_time-pre_transfer_time).toFixed(4);//s
        if(TCP_Content_Generation_Time<=0){
            TCP_Content_Generation_Time = 0.001;
        }
        var TCP_Content_Transfer_Time = (total_time-start_transfer_time).toFixed(4);//s
        if(TCP_Content_Transfer_Time<=0){
            TCP_Content_Transfer_Time = 0.001;
        }

        var DNS_TIME_PERCENT = dns_time/total_time*100;
        var TCP_Connection_Time_PERCENT = TCP_Connection_Time/total_time*100;
        var TCP_Content_Generation_Time_PERCENT = TCP_Content_Generation_Time/total_time*100;
        var TCP_Content_Transfer_Time_PERCENT = TCP_Content_Transfer_Time/total_time*100;

        $(".timing_dns_lookup_time .progress-bar").css({"width":  DNS_TIME_PERCENT + "%"});
        $(".timing_dns_lookup_time .progress-bar .sr-only").text(DNS_TIME_PERCENT + "%");
        $(".timing_dns_lookup_time small").text(DNS_Lookup_Time+"s");

        $(".timing_tcp_connection_time .progress-bar").css({"width":  TCP_Connection_Time_PERCENT + "%"});
        $(".timing_tcp_connection_time .progress-bar .sr-only").text(TCP_Connection_Time_PERCENT + "%");
        $(".timing_tcp_connection_time small").text(TCP_Connection_Time+"s");

        $(".timing_content_generation_time .progress-bar").css({"width":  TCP_Content_Generation_Time_PERCENT + "%"});
        $(".timing_content_generation_time .progress-bar .sr-only").text(TCP_Content_Generation_Time_PERCENT + "%");
        $(".timing_content_generation_time small").text(TCP_Content_Generation_Time+"s");

        $(".timing_content_transfer_time .progress-bar").css({"width":  TCP_Content_Transfer_Time_PERCENT + "%"});
        $(".timing_content_transfer_time .progress-bar .sr-only").text(TCP_Content_Transfer_Time_PERCENT + "%");
        $(".timing_content_transfer_time small").text(TCP_Content_Transfer_Time+"s");

    };
});