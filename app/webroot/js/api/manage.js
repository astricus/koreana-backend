

$(document).ready(function(){
    $(document).on("click", ".run_api_request", function (e) {
        e.preventDefault();
        var form = $(".run_api_request_form");
        var api_method_name = form.attr("name");
        var api_action_url = form.attr("action");
        var $data = {};

        var $params = {};

        form.find('input, select').each(function () {
            // добавим новое свойство к объекту $data
            // имя свойства – значение атрибута name элемента
            // значение свойства – значение свойство value элемента
            $params[this.name] = $(this).val();
        });

        form.find('textarea').each(function () {
            // добавим новое свойство к объекту $data
            // имя свойства – значение атрибута name элемента
            // значение свойства – значение свойство value элемента
            $data[this.name] = $(this).val();
        });

        $("input[name='param_values[]'").each(function (i,e) {
            // добавим новое свойство к объекту $data
            // имя свойства – значение атрибута name элемента
            // значение свойства – значение свойство value элемента
            $data['param_values[' + i + ']'] = $(this).val();
        });

        var complete_data = {};
        complete_data['data'] = $data;
        complete_data['params'] = $params;

        $.ajax({
            url: api_action_url,
            type: "post",
            dataType: 'json',
            data: complete_data,
            beforeSend: function () {
                //preparingApiMethod();
            },
            success: function (data) {
                $(".result_api__status").html(data.data.status);
                $(".result_api__time").html(data.data.time);
                $(".result_api__size").html(data.data.size);
                $(".result_api__body").text(data.data.data);
                $(".result_api__headers").text(data.data.headers.HTTP);
                hljs.initHighlighting();

                 $(".preview_api_result_html").attr("srcdoc", data.data.data);
                $(".result_api__container").removeClass("hidden");

                /*
                *         $headers_info['TOTAL_TIME'] = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
                 $headers_info['NAMELOOKUP_TIME'] = curl_getinfo($ch, CURLINFO_NAMELOOKUP_TIME);
                 $headers_info['CONNECT_TIME'] = curl_getinfo($ch, CURLINFO_CONNECT_TIME);
                 $headers_info['STARTTRANSFER_TIME'] = curl_getinfo($ch, CURLINFO_STARTTRANSFER_TIME);
                 $headers_info['PRETRANSFER_TIME'] = curl_getinfo($ch, CURLINFO_PRETRANSFER_TIME);
                * */
                var total_time = data.data.headers.TOTAL_TIME;
                var dns_time = data.data.headers.NAMELOOKUP_TIME;
                var connect_time = data.data.headers.CONNECT_TIME;
                var start_transfer_time = data.data.headers.STARTTRANSFER_TIME;
                var pre_transfer_time = data.data.headers.PRETRANSFER_TIME;
                view_http_timing_function(timing_width, total_time, dns_time, connect_time, pre_transfer_time, start_transfer_time);

/*

                if (data.data.status == "error") {
                } else {

                    var param_data = data.data;

                    for (var i = 0; i < param_data.length; i++) {

                        var cur_param_data = param_data[i];
                        console.log(cur_param_data);
                        var new_elem;
                        if (cur_param_data.param.param_type == "color") {
                            new_elem = color_picker_param_box(
                                cur_param_data.param.default_value,
                                cur_param_data.param.param_name,
                                cur_param_data.param.id
                            );
                        } else if (cur_param_data.param.param_type == "number") {
                            new_elem = number_range_param_box(
                                cur_param_data.param.default_value,
                                cur_param_data.param.param_name,
                                cur_param_data.param.id,
                                cur_param_data.param.value_interval,
                                cur_param_data.param.min_value,
                                cur_param_data.param.max_value
                            );
                        } else if (cur_param_data.param.param_type == "checkbox") {
                            new_elem = checkbox_param_box(
                                cur_param_data.param.default_value,
                                cur_param_data.param.param_name,
                                cur_param_data.param.id,
                                cur_param_data.param_values
                            );
                        } else {
                            new_elem = "";
                        }
                        $(".category_product_param_box").append(new_elem);
                    }
                }
                */
            }
        });
        return false;
    });

});