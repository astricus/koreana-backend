let form_type_fields;
$(document).on('nifty.ready', function () {

    $(".masked_phone").mask('+7(999)999-99-99');
    $('.datepicker').datepicker();

    $('.summernote').summernote({
        'height': '230px',
        callbacks: {
            onImageUpload: function (files, editor, welEditable) {

                for (let i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });

    function sendFile(file, el) {
        let form_data = new FormData();
        form_data.append('file', file);
        $.ajax({
            data: form_data,
            dataType: 'json',
            type: "POST",
            url: '/static_uploader/',
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status === "success") {
                    $(el).summernote('editor.insertImage', data.data.url);
                } else {
                    alert(data.data.message);
                }

            }
        });
    }

    setInterval(function () {
        $("#page_content").val($(".summernote").summernote('code'));
    }, 120);

    $('#save-text').on('click', function () {
        $('#summernote-edit').summernote('destroy');
    });

    $(".datepicker").datepicker();

    $(document).on("change", "#title", function () {
        let val = $(this).val();
        $("#page_url").val(translit(val));
    });

    let form_fields_header = $(".form_fields_header");
    $(document).on("change", ".new_field_type", function () {
        let field_type = $('.new_field_type option:selected').val();
        if(field_type=="-"){
            return;
        }
        let field_label = $("<label class='col-md-2 control-label'>" + form_type_fields[field_type] + "</label>" +
            "<input type='hidden' name='webform[id][]' value=''/>");
        let field_name = $("<div class='col-md-2'><input type='text' name='webform[name][]' value='' placeholder='Название поля формы' class='form-control'/></div>");
        let field_code = $("<div class='col-md-2'><input type='text' name='webform[code][]' value='' placeholder='Код поля формы' class='form-control'/></div>");
        //let field_utm = $("<div class='col-md-2'><input type='text' name='webform[utm][]' value='' placeholder='utm-метка поля' class='form-control'/></div>");
        let field_type_name = $("<input name='webform[type][]' type='hidden' value='" + field_type + "'/>");

        let field_data = "";
        //alert(field_type);
        if (field_type === "select") {
            let provider_list_html = $(".data_providers").html();
            field_data = $("<div class='col-md-2'><select name='webform[data_provider_id][]' class='form-control'>" + provider_list_html + "" +
                "</select></div>" +
                "<input type='hidden' name='webform[placeholder][]' value='' />");

        } else if (field_type == "input_radio") {
            field_data = $("<div class='col-md-2'>" +
                "<input type='text' name='webform[input_radio][]' value='' placeholder='значение для поля radio' class='form-control' /></div>");
        }
        else if (field_type == "input_checkbox") {
            field_data = $("<div class='col-md-2'>" +
                "<input type='text' name='webform[input_checkbox][]' value='' placeholder='значение для поля checkbox'  class='form-control' /></div>");
        }
        else {
            field_data = $("<input name='webform[data_provider_id][]' type='hidden' value=''/>" +
                "<div class='col-md-2'><input type='text' name='webform[placeholder][]' value='' placeholder='Подсказка поля формы' class='form-control'/></div>");
        }

        let field_controls = $("<div class='col-md-3'>" +
            "<a class='btn btn-warning btn-bock delete_form_field' href='#' title='Удалить'><span class='fa fa-trash'></span></a>" +
            "<a class='btn btn-default btn-bock up_form_field' href='#' title='Перенести вверх'><span class='fa fa-arrow-up'></span></a>" +
            "<a class='btn btn-default btn-bock down_form_field' href='#' title='Перенести вниз'><span class='fa fa-arrow-down'></span></a>" +
            "</div>"
        );


        $("<div class='form_fields_container row sortable_field'></div>").insertAfter(form_fields_header);
        let new_field_box = $(".form_fields_container").first();
        new_field_box.append(field_label);
        new_field_box.append(field_name);
        new_field_box.append(field_code);
        new_field_box.append(field_type_name);
        //new_field_box.append(field_placeholder);
        new_field_box.append(field_data);
        new_field_box.append(field_controls);
        new_field_box.addClass("form_group");
    });


    $(document).on("click", ".delete_form_field", function (e) {
        let elem = $(this).parents('.form_fields_container');
        console.log(elem);
        elem.remove();

        e.preventDefault();
        return false;
    });

    $(document).on("click", ".down_form_field", function (e) {
        let elem = $(this).parents('.form_fields_container');
        console.log(elem);
        let next_elem = elem.next();
        if (next_elem.hasClass("form_fields_container")) {
            //elem.insertAfter(next_elem);
            elem.swapWith(next_elem);
        }
        console.log("moved down");
        e.preventDefault();
        return false;
    });

    $(document).on("click", ".up_form_field", function (e) {
        let elem = $(this).parents('.form_fields_container');
        console.log(elem);
        let prev_elem = elem.prev();
        if (prev_elem.hasClass("form_fields_container")) {
            elem.swapWith(prev_elem);
        }
        console.log("moved up");
        e.preventDefault();
        return false;
    });


    // источники данных
    $(document).on("click", ".new_provider_element", function (e) {

        let new_elem = $('<div class="form-group provider_element">'+
            '<label class="col-lg-3 control-label" for="">Название элемента списка</label>'+
            '<div class="col-sm-6">'+
                '<input type="text" name="provider[name][]" class="form-control"/>'+
            '</div>'+
            '<div class="col-sm-2">'+
            '<a class="btn btn-warning btn-bock delete_provider_element" href="#" title="Удалить элемент списка"><span class="fa fa-trash"></span></a></div>'+
        '</div>');
        $(".provider_elements").append(new_elem);
        e.preventDefault();
        return false;
    });


    $(document).on("click", ".delete_provider_element", function (e) {
        let elem = $(this).parents('.provider_element');
        elem.remove();
        e.preventDefault();
        return false;
    });

    $(document).on("click", ".delete_provider_list_element", function (e) {
        let elem_id = $(this).attr('data-value');
        e.preventDefault();
        $("input[data-value=" + elem_id + "]").attr('type', 'hidden');
        $("input[data-value=" + elem_id + "]").val("--");
        $("input[data-value=" + elem_id + "]").parents(".form-group").hide();
        return false;
    });


});