$(document).ready(function(){
    $(document).on("change", ".super_slider", function () {
        var cur_value = $(this).val();
        var slider_viewer = $(this).next('.slider_viewer');
        slider_viewer.val(cur_value);
    });
});