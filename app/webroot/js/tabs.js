var clickTab = function (tabName) {
    var stateObj = {'ancor': tabName};
    var sourceIUrl = new URL(window.location.href);
    var cleanIUrl = sourceIUrl.origin+sourceIUrl.pathname+sourceIUrl.search;
    history.pushState(stateObj, "", cleanIUrl+tabName);
};

$(document).ready(function () {

    var sourceIUrl = new URL(window.location.href);
    var hash = sourceIUrl.hash;

    var curTab = $('a[data-toggle="tab"][href="' + hash + '"]');
    if(curTab.length>0){
        curTab.click();
    }

    $(document).on('click', 'a[data-toggle="tab"]', function (e) {
        var tabName = $(this).attr('href');
        clickTab(tabName);

    });
});
