<div class="row">
    <div class="col-sm-12">
        <a href="/doc/translate/index" class="btn btn-primary"><span class="fa fa-book"></span> Документация Translate Api </a>
        <a href="/translate/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/translate/sandbox/" class="btn btn-primary"><span class="fa fa-code"></span> Песочница Translate Api</a>
        <a href="/translate/monitoring/" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг</a>
      </div>
</div>
<hr>