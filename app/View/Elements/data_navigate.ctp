<div class="row">
    <div class="col-sm-12">
        <a href="/doc/data/index" class="btn btn-danger"><span class="fa fa-book"></span> Документация Data Api </a>
        <a href="/data/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/data/sandbox/" class="btn btn-primary"><span class="fa fa-code"></span> Песочница Data Api</a>
        <a href="/data/projects/" class="btn btn-primary"><span class="fa fa-list"></span> Проекты</a>
        <a href="/data/monitoring/" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг</a>
      </div>
</div>
<hr>