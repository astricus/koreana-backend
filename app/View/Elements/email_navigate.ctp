<div class="row">
    <div class="col-sm-12">
        <a href="/doc/email/index" class="btn btn-info"><span class="fa fa-book"></span> Документация Email Api </a>
        <a href="/email/list" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/email/sandbox/" class="btn btn-primary"><span class="fa fa-code"></span> Отправить Email</a>
        <a href="/email/query/" class="btn btn-primary"><span class="fa fa-code"></span> Очереди рассылок</a>
        <a href="/email/query/" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг</a>
      </div>
</div>
<hr>