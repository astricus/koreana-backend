<script src="<?= site_url(); ?>/js/chat.lib.js"></script>
<script src="<?= site_url(); ?>/js/chat.js"></script>
<div class="page-fixedbar-container chat_user_list">
    <div class="page-fixedbar-content">
        <div class="nano has-scrollbar">
            <div class="nano-content" tabindex="0">
                <div class="pad-all bord-btm">
                    <input type="text" placeholder="Найдите собеседника или начните новый чат" class="form-control chat_search_user">
                    <div class="chat_search_popup"></div>
                </div>

                <div class="chat-user-list">
                    <a href="#" class="chat-unread chat_message_item">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/2.png" alt="Profile Picture">
                            <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info">
                                <span class="text-xs">11:39</span>
                                <span class="badge badge-success">9</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username chat_recipient" data-user_id="1" data-user_role="driver">Андрей Пригожин</p>
                                <p class="chat-userrole text-bold">Водитель</p>
                                <p>Я подьезжаю к клиенту, скоро буду.</p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="chat_message_item">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/3.png" alt="Profile Picture">
                            <i class="badge badge-danger badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">11:09</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username chat_recipient" data-user_id="1" data-user_role="driver">Максим Тухлов</p>
                                <p class="chat-userrole text-bold">Поставщик - Петрович</p>
                               <p>I hear the buzz of the little world among the stalks</p>
                            </div>
                        </div>
                    </a>
                    <? /*
                    <a href="#" class="chat-unread">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/1.png" alt="Profile Picture">
                            <i class="badge badge-info badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">10:12</span>
                                <span class="badge badge-success">1</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Jack George</p>
                                <p>I should be incapable of drawing a single stroke at the present moment</p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="chat-unread">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/4.png" alt="Profile Picture">
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">08:52</span>
                                <span class="badge badge-success">5</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Donald Brown</p>
                                <p>I must explain to you how all this mistaken idea</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/8.png" alt="Profile Picture">
                            <i class="badge badge-warning badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">7:05</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Betty Murphy</p>
                                <p>For science, music, sport, etc</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/9.png" alt="Profile Picture">
                            <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Yesterday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Samantha Reid</p>
                                <p>Great explorer of the truth, the master-builder of human happiness.</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/10.png" alt="Profile Picture">
                            <i class="badge badge-danger badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Yesterday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Brenda Fuller</p>
                                <p>On the other hand</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/4.png" alt="Profile Picture">
                            <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Yesterday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Howard Rios</p>
                                <p>Everyone realizes why a new common language would be desirable</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/6.png" alt="Profile Picture">
                            <i class="badge badge-danger badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Yesterday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Lucy Moon</p>
                                <p>I should be incapable of drawing</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/7.png" alt="Profile Picture">
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Friday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Brittany Meyer</p>
                                <p>I enjoy with my whole heart. I enjoy with my whole heart.</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/6.png" alt="Profile Picture">
                            <i class="badge badge-info badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Friday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Kathryn Obrien</p>
                                <p>We are gradually adding new functionality</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/8.png" alt="Profile Picture">
                            <i class="badge badge-warning badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Friday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Betty Murphy</p>
                                <p>For science, music, sport, etc</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/6.png" alt="Profile Picture">
                            <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Friday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Lucy Moon</p>
                                <p>I should be incapable of drawing</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/8.png" alt="Profile Picture">
                            <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">Friday</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Brenda Fuller</p>
                                <p>On the other hand</p>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="media-left">
                            <img class="img-circle img-xs" src="img/profile-photos/4.png" alt="Profile Picture">
                            <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                        </div>
                        <div class="media-body">
                            <span class="chat-info pull-right">
                                <span class="text-xs">12/11/17</span>
                            </span>
                            <div class="chat-text">
                                <p class="chat-username">Howard Rios</p>
                                <p>Everyone realizes why a new common language would be desirable</p>
                            </div>
                        </div>
                    </a>*/?>
                </div>


            </div>
            <div class="nano-pane" style="display: none;">
                <div class="nano-slider" style="height: 1204px; transform: translate(0px);"></div>
            </div>
        </div>
    </div>
</div>