<div class="row">
    <div class="col-sm-12">
        <a href="/manage/sandbox/script/base_script" class="btn btn-primary"><span class="fa fa-code"></span> Создать веб-скрипт</a>
        <a href="/manage/sandbox/script/list" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/manage/sandbox/http/get" class="btn btn-primary"><span class="fa fa-code"></span> Http Get запрос</a>
        <a href="/manage/sandbox/http/post" class="btn btn-primary"><span class="fa fa-code"></span> Http Post запрос</a>
        <a href="/doc/http/index" class="btn btn-primary"><span class="fa fa-code"></span> Документация Http Api </a>
     </div>
</div>
<hr>


