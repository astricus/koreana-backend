<div class="row">
    <div class="col-sm-12">
        <a href="/admin/create" class="btn btn-success"><span class="fa fa-plus-circle"></span> Создать администратора</a>
        <a href="/admins/list" class="btn btn-primary"><span class="fa fa-list"></span> Все администраторы</a>
        <a href="/admins/monitoring" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг</a>
      </div>
</div>
<hr>