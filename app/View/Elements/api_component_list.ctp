<hr>
<h4>Api Компонент <?= ucfirst($api_component) ?></h4>
<div class="row">
    <div class="col-sm-12">
        <? foreach ($method_list as $method_item) {
            if ($method_item == $current_method) {
                $link = 'disabled="disabled" onclick="return:false"';
            } else {
                $link = 'href="/manage/sandbox/' . $api_component . '/' . $method_item . '"';
            }
            ?>
            <a <?=$link?> class="btn btn-success"><span
                    class="fa fa-code"></span> <?= ucfirst($api_component) ?> <?= $method_item ?></a>
        <? } ?>

    </div>
</div>
<hr>