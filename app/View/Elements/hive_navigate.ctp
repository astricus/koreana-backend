<div class="row">
    <div class="col-sm-12">
        <a href="/restapi/doc/index" class="btn btn-success"><span class="fa fa-book"></span> Документация Koreana Backend Api </a>
        <a href="/restapi/test" class="btn btn-primary"><span class="fa fa-code"></span> Тестирование API</a>
        <a href="/restapi/request/add" class="btn btn-primary"><span class="fa fa-code"></span> Создать API-запрос</a>
        <a href="/restapi/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/restapi/monitoring" class="btn btn-primary"><span class="fa fa-eye"></span> Подключения и активность</a>

      </div>
</div>
<hr>