<div class="row">
    <div class="col-sm-12">

        <a href="/doc/file/index" class="btn btn-warning"><span class="fa fa-book"></span> Документация File Api </a>
        <a href="/file/list" class="btn btn-primary"><span class="fa fa-list"></span> Файловый менеджер</a>
        <a href="/file/sandbox" class="btn btn-primary"><span class="fa fa-code"></span> Песочница File Api</a>
        <a href="/file/monitoring" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг</a>
      </div>
</div>
<hr>