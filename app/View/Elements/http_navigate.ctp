<div class="row">
    <div class="col-sm-12">
        <a href="/doc/http/index" class="btn btn-dark"><span class="fa fa-book"></span> Документация Http+Curl Api </a>
        <a href="/http/base_script" class="btn btn-primary"><span class="fa fa-code"></span> Создать веб-скрипт</a>
        <a href="/http/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/http/get" class="btn btn-primary"><span class="fa fa-code"></span> Http Get запрос</a>
        <a href="/http/post" class="btn btn-primary"><span class="fa fa-code"></span> Http Post запрос</a>
        <a href="/http/curl_get" class="btn btn-primary"><span class="fa fa-code"></span> Curl Get запрос</a>
        <a href="/http/curl_post" class="btn btn-primary"><span class="fa fa-code"></span> Curl Post запрос</a>
        <a href="/http/curl_put" class="btn btn-primary"><span class="fa fa-code"></span> Curl Put запрос</a>
        <a href="/http/curl_delete" class="btn btn-primary"><span class="fa fa-code"></span> Curl Delete запрос</a>
        <a href="/http/curl_browser" class="btn btn-primary"><span class="fa fa-code"></span> Curl Имитация браузера</a>
        <a href="/http//curl_basic_auth" class="btn btn-primary"><span class="fa fa-code"></span> Curl Http авторизация</a>
        <a href="/http/curl_load_file" class="btn btn-primary"><span class="fa fa-code"></span> Curl cкачивание файла</a>
        <a href="/http/curl_send_file" class="btn btn-primary"><span class="fa fa-code"></span> Curl закачивание файла</a>
      </div>
</div>
<hr>