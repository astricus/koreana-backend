<div class="row">
    <div class="col-sm-12">
        <a href="/doc/sms/index" class="btn btn-success"><span class="fa fa-book"></span> Документация Sms Api </a>
        <a href="/sms/base_script" class="btn btn-primary"><span class="fa fa-code"></span> Создать веб-скрипт</a>
        <a href="/sms/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/sms/send" class="btn btn-primary"><span class="fa fa-code"></span> Отправка смс</a>
        <a href="/sms/monitoring" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг</a>
    </div>
</div>
<hr>