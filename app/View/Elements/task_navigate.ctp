<div class="row">
    <div class="col-sm-12">
        <a href="/doc/task/index" class="btn btn-purple"><span class="fa fa-book"></span> Документация Task Api </a>
        <a href="/task/base_script" class="btn btn-primary"><span class="fa fa-code"></span> Создать задачу</a>
        <a href="/task/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры задач</a>
        <a href="/task/list" class="btn btn-primary"><span class="fa fa-list"></span> Список задач</a>
        <a href="/task/monitoring" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг задач</a>
      </div>
</div>
<hr>