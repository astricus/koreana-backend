<div class="row">
    <div class="col-sm-12">
        <a href="/doc/forms/index" class="btn btn-mint"><span class="fa fa-book"></span> Документация Forms Api </a>
        <a href="/forms/web_form" class="btn btn-primary"><span class="fa fa-code"></span> Создать веб-форму</a>
        <a href="/forms/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/forms/list" class="btn btn-primary"><span class="fa fa-list"></span> Формы</a>
        <a href="/forms/monitoring" class="btn btn-primary"><span class="fa fa-eye"></span> Мониторинг</a>
      </div>
</div>
<hr>