<div class="row">
    <div class="col-sm-12">
        <a href="/doc/socket/index" class="btn btn-pink"><span class="fa fa-book"></span> Документация Web Socket Api </a>
        <a href="/socket/examples" class="btn btn-primary"><span class="fa fa-code"></span> Шаблоны и примеры </a>
        <a href="/socket/create" class="btn btn-primary"><span class="fa fa-code"></span> Запуск сокет сервера</a>
        <a href="/socket/list" class="btn btn-primary"><span class="fa fa-list"></span> Список серверов</a>
        <a href="/socket/monitoring" class="btn btn-primary"><span class="fa fa-code"></span> Мониторинг</a>
      </div>
</div>
<hr>