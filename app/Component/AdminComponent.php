<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');
/**
 * Компонент Администратор
 */
class AdminComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;
    public $user_data;
    public $user_id;

    public $uses = array('Error');

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

     public function manager_data()
    {
        //общие данные администратора
        $user_id = $this->Session->read('manager_id');
        $modelName = 'Manager';
        $this->Manager = ClassRegistry::init($modelName);

        $user_data = $this->Manager->find('first',
            array(
                'conditions' => array(
                    'id' => $user_id
                )
            )
        );
        $this->user_data = $user_data;
        $this->controller->set('manager_data', $user_data);
        return $user_data;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getManagerById($id)
    {
        if ($id <= 0) {
            return null;
        }
        $modelName = 'Manager';
        $this->Manager = ClassRegistry::init($modelName);
        $manager = $this->Manager->find('first',
            array(
                'conditions' => array(
                    'id' => $id
                )
            )
        );
        return $manager['Manager'];
    }

    public function manager_id()
    {
        return $this->Session->read('manager_id');
    }

    public function get_managers()
    {
        $modelName = 'Manager';
        $this->Manager = ClassRegistry::init($modelName);
        $users = $this->Manager->find('all',
            array(
                'fields' => array(
                    'id',
                    'firstname',
                    'lastname',
                )
            )
        );
        return $users;
    }

    public function checkEmailExists($email)
    {
        $modelName = 'Manager';
        $this->Manager = ClassRegistry::init($modelName);
        return $this->Manager->find("count",
            array(
                'conditions' =>
                    array(
                        'email' => $email
                    ),
            )
        );
    }

}