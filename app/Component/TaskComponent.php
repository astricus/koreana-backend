<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Фоновая задача
 */
class TaskComponent extends Component
{

    public $components = [
        'Session',
        'Validator',
        'User',
    ];

    public $task_table = [
        'task_test' => ['class' => 'Task', 'action' => 'taskTest'],
        'create_dump' => ['class' => 'Task', 'action' => 'taskDump'],
        'update_car_base' => ['class' => 'UpdateCarBase', 'action' => 'updateBase'],

    ];

    public $valid_statuses = [
        'wait' => 'ожидает',
        'active' => 'запущена',
        'finished' => 'остановлена',
        'unknown' => 'неизвестно',
    ];

    public $valid_result_statuses = [
        'unknown' => 'Неизвестно',
        'success' => 'Успешно',
        'failure' => 'Не успешно',
    ];

    public $errors = [];

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Task";
        $this->Task = ClassRegistry::init($modelName);
    }

    public function logTask($task_name, $code, $task_result, $payload, $task_timer)
    {
        $data = [
            'name' => $task_name,
            'code' => $code,
            'timer' => $task_timer,
            'status' => 'finished',
            'args' => '',
            'result_status' => $task_result,
            'result_payload' => $payload,
            'start_date' => date('y-m-d'),
            'start_hour' => date('H'),
            'start_minute' => date('i'),
        ];
        $this->setup();
        $this->Task->create();
        if ($this->Task->save($data)) {
            return $this->Task->id;
        }

        return false;
    }

    /**
     * @param $task_id
     * @return bool
     * @throws Exception
     */
    public function startTask($task_id)
    {
        // перевести задачу в статус в работе
        $data = [
            'status' => 'active'
        ];
        $this->setup();
        $this->Task->id = $task_id;
        if ($this->Task->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return mixed
     */
    public function taskList($show_count, $page, $sort, $sort_dir, $filter)
    {
        $this->setup();

        $start_date_ar = [];
        if (isset($filter["start_date"]) && $filter["start_date"] == true) {
            $st_date = $filter["start_date"];
            $start_date_ar = ["DATEDIFF('$st_date', start_date)" => 0];
        }

        $status_ar = [];
        if (isset($filter["status"])) {
            $status_ar = ['status' => $filter["status"]];
        }

        $result_status_ar = [];
        if (isset($filter["result_status"])) {
            $result_status_ar = ['result_status' => $filter["result_status"]];
        }

        $list = $this->Task->find(
            "all",
            [
                'conditions' =>
                    [
                        $start_date_ar,
                        $status_ar,
                        $result_status_ar
                    ],

                'limit' => $show_count,
                'offset' => $page,
                'order' => ["created DESC"]
            ]
        );

        return $list;
    }

    /**
     * @param $status
     * @param $start_date
     * @param $result_status
     * @return array|int|null
     */
    public function totalTaskCount($status, $start_date, $result_status)
    {
        $this->setup();

        $view_start_date = null;
        if (isset($start_date)) {
            $view_start_date['UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(start_date)'] = 0;
        }
        $view_status = null;
        if (!empty($status)) {
            $view_status['status'] = $status;
        }
        $view_result_status = null;
        if (!empty($result_status)) {
            $view_result_status['result_status'] = $result_status;
        }

        return $this->Task->find(
            "count",
            [
                'conditions' =>
                    [
                        $view_start_date,
                        $view_status,
                        $view_result_status
                    ]
            ]
        );
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function getTaskById($id)
    {
        $this->setup();
        $task = $this->Task->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id
                    ]
            ]
        );
        if (count($task) > 0) {
            return $task['Task'];
        }
        return null;
    }

    /**
     * @param $start_date
     * @param $start_hour
     * @param $start_minute
     * @return bool
     */
    private function checkLaunchDatetime($start_date, $start_hour, $start_minute){
        if(date('y-m-d') < $start_date) {
            $this->errors[] = 'Дата запуска не должна быть в прошедшем времени';
            return false;
        }
        if($start_date == date('y-m-d') && date('H')<$start_hour) {
            $this->errors[] = 'Час запуска приложения для сегодня уже пройден';
            return false;
        }
        if($start_date == date('y-m-d') && date('H') == $start_hour && date('i')<=$start_minute) {
            $this->errors[] = 'Минута запуска приложения текущего часа уже наступила';
            return false;
        }
        return true;
    }

    /**
     * @param $task_name
     * @return bool
     */
    private function validateTaskName($task_name){
        if(mb_strlen($task_name)<=3){
            $this->errors[] = 'Слишком короткное название для задачи';
            return false;
        }
        return true;
    }

    /**
     * @param $task_id
     * @return bool
     */
    public function deleteTask($task_id)
    {
        $this->setup();
        $task_check = $this->getTaskById($task_id);
        if($task_check == null){
            $this->errors[] = 'Задача не найдена';
            return false;
        }
        if( $task_check['status'] == 'active'){
            $this->errors[] = 'Нельзя удалить запущенную задачу';
            return false;
        }

        $this->Task->id = $task_id;
        if ($this->Task->delete()) {
            return true;
        }
        $this->errors[] = 'Не удалось удалить задачу';
        return false;
    }

    public function taskToDo()
    {
        $this->setup();

        return $this->Task->find(
            "all",
            [
                'conditions' =>
                    [
                        'status' => 'wait',
                        'DATE(NOW()) - DATE(start_date) >=' => 0,
                        'HOUR(NOW()) - HOUR(start_date) >=' => 0,
                        'MINUTE(NOW()) - MINUTE(start_date) >=' => 0,
                    ],
                'order' => ["created DESC"]
            ]
        );
    }

    /**
     * @param $task_id
     * @return bool
     * @throws Exception
     */
    public function stopTask($task_id)
    {
        // остановить задачу, перевести в статус завершена
        // перевести задачу в статус в работе
        $data = [
            'status' => 'finished'
        ];
        $this->setup();
        $this->Task->id = $task_id;
        if ($this->Task->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $task_id
     * @return bool
     * @throws Exception
     */
    public function restartTask($task_id)
    {
        // перевести задачу в статус в работе
        $data = [
            'status' => 'wait'
        ];
        $this->setup();
        $this->Task->id = $task_id;
        if ($this->Task->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $task_name
     * @param $args
     * @param $code
     * @param $date
     * @param $hour
     * @param $minute
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function addTaskToQueue($task_name, $args, $code, $date, $hour, $minute)
    {
        $data = [
            'name' => $task_name,
            'code' => $code,
            'args' => $args,
            'result_status' => 'unknown',
            'result_payload' => '',
            'timer' => 0,
            'status' => 'wait',
            'start_date' => $date,
            'start_hour' => $hour,
            'start_minute' => $minute,
        ];
        if(!$this->validateTaskName($task_name)){
            return false;
        }
        if(!$this->checkLaunchDatetime($date, $hour, $minute)){
            return false;
        }
        $this->setup();
        $this->Task->create();
        if ($this->Task->save($data)) {
            return $this->Task->id;
        }

        return false;
    }

    /**
     * @param $task_id
     * @param $arr
     * @return false
     * @throws Exception
     */
    public function updateTask($task_id, $arr)
    {
        $valid_fields = [
            'name', 'code', 'args',
            'start_date',
            'start_hour',
            'start_minute'
        ];
        $vals = [
            'result_status' => '',
            'result_payload' => '',
            'timer' => 0,
            'status' => 'wait',
        ];

        if(key_exists('name', $arr)){
            if(!$this->validateTaskName($arr['name'])){
                return false;
            }
        }

        if(key_exists('start_date', $arr)) {
            if (!$this->checkLaunchDatetime($arr['start_date'], $arr['start_hour'], $arr['start_minute'])) {
                return false;
            }
        }

        foreach ($arr as $k => $v) {
            if (in_array($k, $valid_fields)) {
                $vals[$k] = $v;
            }
        }

        if (count($vals) > 0) {
            $this->setup();
            $this->Task->id = $task_id;
            $this->Task->save($vals);
        }
        return false;
    }

    /**
     * @param $id
     * @param $result_status
     * @param $payload
     * @param $timer
     * @return bool
     * @throws Exception
     */
    public function resultTask($id, $result_status, $payload, $timer)
    {
        $data = [
            'result_status' => $result_status,
            'result_payload' => $payload,
            'timer' => $timer,
            //'status' => 'wait',

        ];
        $this->setup();
        $this->Task->id = $id;
        $this->Task->save($data);

        return true;
    }

    public function taskTest()
    {
        return ['status' => 'success', 'payload' => 'success'];
    }

    public function taskDump(){
        $this->start_time = microtime(true);
        $dump_name = date("Y-m-d") . "--.sql";
        $path = TMP;
        $final_file = $path . $dump_name;
        exec("mysqldump --user=root --password=Cosmos_888 --host=127.0.0.1 --ignore-table=koreana.api_log koreana > $final_file");
        sleep(7);
        if(file_exists($final_file)){
            return ['status' => 'success', 'payload' => 'success'];
        }
        return ['status' => 'failure', 'payload' => ''];
    }


}