<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Источник Данных
 */
class DataProviderComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Static',
    );

    public $default_sort_field = "created";

    public $default_show_count = "10";

    public $controller;

    public $error_text = [
        'data_provider_is_not_exists' => 'Источник данных не существует',
    ];

    public $params = [
        'min_title_length' => 2,
        'max_title_length' => 256,
        'min_url_length' => 2,
        'max_url_length' => 512
    ];

    public $errors = [];

    public $error_fields = [];

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Data_Provider";
        $this->Data_Provider = ClassRegistry::init($modelName);
        $modelName = "Data_List_Item";
        $this->Data_List_Item = ClassRegistry::init($modelName);
        $modelName = "Webform_Field";
        $this->Webform_Field = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function checkDataProviderExists($id)
    {
        $this->setup();
        return $this->Data_Provider->find("count",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }

    //---- PUBLIC

    //---- PAGES

    /**
     * @param $id
     * @param $name
     * @param null $status
     * @return bool
     */
    public function updateDataProvider($id, $name, $status = null)
    {
        $this->setup();
        if ($status == null) {
            $status_list = [];
        } else {
            $status_list = ["status" => $status];
        }
        $data = [
            "name" => $name,
            $status_list
        ];
        $this->Data_Provider->id = $id;
        if ($this->Data_Provider->save($data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $name
     * @return bool
     */
    public function createDataProvider($name)
    {
        $this->setup();
        $data = [
            "name" => $name,
            "enabled" => "1"
        ];

        $this->Data_Provider->create();
        if ($this->Data_Provider->save($data)) {
            return $this->Data_Provider->id;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function totalDataProviderCount()
    {
        $this->setup();
        return $this->Data_Provider->find("count",
            array('conditions' =>
                array(),
            )
        );
    }

    /**
     * @param $show_count
     * @param $limit_page
     * @param $sort
     * @param $sort_dir
     * @return mixed
     */
    public function pageList($show_count, $limit_page, $sort, $sort_dir)
    {
        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }
        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }
        $this->setup();
        $page_list = $this->Data_Provider->find("all",
            array('conditions' =>
                array(),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array($sort . " " . $sort_dir),
            )
        );
        return $page_list;
    }


    /**
     * @param $id
     * @return bool
     */
    public function deleteProvider($id)
    {
        if (!$this->checkDataProviderExists($id)) {
            $this->returnError("General", $this->errors['data_provider_is_not_exists']);
            return false;
        }
        $this->Data_Provider->id = $id;
        $this->Data_Provider->delete();
        return true;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDataProviderById($id)
    {
        $this->setup();
        return $this->Data_Provider->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
    }

    public function getAllDataProviders()
    {
        $status = [];
        return $this->dataProviders($status);
    }

    public function getActiveDataProviders()
    {
        $status = ['enabled' => 1];
        return $this->dataProviders($status);
    }

    private function dataProviders($status)
    {
        $this->setup();
        return $this->Data_Provider->find("all",
            array('conditions' =>
                array(
                    $status
                )
            )
        );
    }

    /**
     * @param $provider_id
     * @param $name
     * @return bool
     */
    public function addDataItem($provider_id, $name)
    {
        $this->setup();

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "provider_id" => $provider_id,
            "name" => $name,
        ];

        $this->Data_List_Item->create();
        if ($this->Data_List_Item->save($data)) {
            return $this->Data_List_Item->id;
        }
        return false;
    }

    /**
     * @param $item_id
     * @param $name
     * @return false
     */
    public function updateDataItem($item_id, $name)
    {
        $this->setup();

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "name" => $name,
        ];

        $this->Data_List_Item->id = $item_id;
        if ($this->Data_List_Item->save($data)) {
            return $this->Data_List_Item->id;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function removeDataItem($id)
    {
        $this->setup();
        $this->Data_List_Item->id = $id;
        $this->Data_List_Item->delete();
        return true;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProviderList($id)
    {
        $this->setup();
        return $this->Data_List_Item->find("all",
            array('conditions' =>
                array(
                    'provider_id' => $id,
                ),
                'order' => 'id ASC'
            )
        );
    }

    /**
     * @param $id
     * @return int
     */
    public function getProviderListElementCount($id)
    {
        return count($this->getProviderList($id));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDataProviderFormUsed($id)
    {
        $this->setup();
        return $this->Webform_Field->find("count",
            array('conditions' =>
                array(
                    'field_value_list_id' => $id,
                ),
            )
        );
    }

    /**
     * @param $id
     * @return bool
     */
    public function block($id)
    {
        $this->setup();
        if (!$this->checkDataProviderExists($id)) {
            $this->returnError("General", $this->errors['data_provider_is_not_exists']);
            return false;
        }
        $this->setStatus($id, 0);
    }

    /**
     * @param $id
     * @return bool
     */
    public function unblock($id)
    {
        $this->setup();
        if (!$this->checkDataProviderExists($id)) {
            $this->returnError("General", $this->errors['data_provider_is_not_exists']);
            return false;
        }
        $this->setStatus($id, 1);
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     */
    private function setStatus($id, $status)
    {
        $this->setup();
        $update_data = array('enabled' => $status);
        $this->Data_Provider->id = $id;
        if ($this->Data_Provider->save($update_data)) {
            return true;
        }
        return false;
    }
}