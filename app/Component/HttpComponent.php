<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент HTTP
 */
class HttpComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Log'
    );

    public $controller;

    public $uses = array('Error');

    public $method_list = array(
        'get',
        'post',
        'ping_domain',
        'put',
        'option',
        'delete',
    );

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function apiDomain(){
        $scheme = "http://";
        if( isset($_SERVER['HTTPS'] ) ) {
            $scheme = "https://";
        }
        return $scheme . $_SERVER["SERVER_NAME"] . "/api/";
    }

    /**
     * @param $method
     * @param $data
     * @param $params
     * @return mixed
     */
    public function runMethod($method, $data, $params)
    {
        if (method_exists(__CLASS__, $method)) {
            if ($method == "post") {
                if (key_exists("url", $params)) {
                    $url = $params['url'] ?? null;
                    $result = $this->post($url, $params, $data);
                    return $result;
                }
            } else if ($method == "get") {
                if (key_exists("url", $params)) {
                    $url = $params['url'] ?? null;
                    $result = $this->get($url, $params);
                    return $result;
                }
            }
        } else {
            die("UNDEFINED API COMPONENT METHOD");
        }
    }

    // TEST API Route handler
    public function testApiHandler($route, $params, $data)
    {
        $url = $this->apiDomain() . $route;
        $starttime = microtime(true);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        // ЗАГЛУШКА
        curl_setopt($ch, CURLINFO_HEADER_OUT, true); // enable tracking
        $headers_array = [];
        $headers_array[] = $this->apiHttpHeadersSignature();
        //default http headers
        $headers_array[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp';
        $headers_array[] = 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3';
        $headers_array[] = 'Connection: keep-alive';
        $headers_array[] = 'Cache-Control: max-age=0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($data != null) {
            if(is_array($data)) {
                $query = http_build_query(urlencode($data));
            } else {
                $query = urlencode($data);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        }
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

//        pr(curl_getinfo($ch, CURLINFO_HEADER_OUT));
//        exit;
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $data_size = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
        // Retudn headers seperatly from the Response Body
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        $headers_info = [];
        $headers_info['HTTP'] = $headers;

        header("Content-Type:text/html; charset=UTF-8");
        curl_close($ch);

        $stoptime = microtime(true);
        $operation_timer = ($stoptime - $starttime) * 1000;
        if ($data_size / 1000 > 1) {
            $data_size_final = sprintf("%.2f", $data_size / 1000) . "k";
        } else {
            $data_size_final = sprintf("%.2f", $data_size);
        }
        if ($operation_timer / 1000 > 1) {
            $timer_final = sprintf("%.2f", $operation_timer / 1000) . "c";
        } else {
            $timer_final = sprintf("%.2f", $operation_timer) . " мс";
        }
        return [
            "request_data" => $query,
            "response_data" => $body,
            "url" => $url,
            "status" => $httpcode,
            "size" => $data_size_final,
            "time" => $timer_final,
            "headers" => $headers_info['HTTP']
        ];
    }

    /**
     * @param $url
     * @param array $params
     * @param array $data
     * @return mixed
     */
    public function post($url, $params, $data)
    {
        $starttime = microtime(true);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        // ЗАГЛУШКА
        curl_setopt($ch, CURLINFO_HEADER_OUT, true); // enable tracking
        $headers_array = [];
        $headers_array[] = $this->apiHttpHeadersSignature();
        $headers_array[] = $this->apiHttpHeadersUserAgent($params["user-agent"]);
        //default http headers
        $headers_array[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp';
        $headers_array[] = 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3';
        $headers_array[] = 'Accept-Encoding: gzip, deflate';
        $headers_array[] = 'Connection: keep-alive';
        $headers_array[] = 'Cookie: ' . $params["cookie"];
        $headers_array[] = 'Upgrade-Insecure-Requests: 1';
        $headers_array[] = 'Cache-Control: max-age=0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);

        $temp_dir = Configure::read('FILE_TEMP_DIR');
        $cookie_jar = tempnam($temp_dir, "__cookie_");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_jar);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($data != null) {
            $query = http_build_query($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        }
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

//        pr(curl_getinfo($ch, CURLINFO_HEADER_OUT));
//        exit;
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $data_size = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);


        // Retudn headers seperatly from the Response Body
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($response, 0, $header_size);
        //die(pr($headers));
        $body = substr($response, $header_size);
        $cookies = curl_getinfo($ch, CURLINFO_COOKIELIST);

        // get cookies
//        $cookies = array();
//        preg_match_all('/Set-Cookie:(?<cookie>\s{0,}.*)$/im', $response, $cookies);
//
//        pr($cookies['cookie']); // show harvested cookies
//
//        $cookieParts = array();
//        preg_match_all('/Set-Cookie:\s{0,}(?P<name>[^=]*)=(?P<value>[^;]*).*?expires=(?P<expires>[^;]*).*?path=(?P<path>[^;]*).*?domain=(?P<domain>[^\s;]*).*?$/im', $response, $cookieParts);
//        pr($cookieParts);
//
//        exit;
        $headers_info = $cookies;
        $headers_info['HTTP'] = $headers;

        $CURLINFO_TOTAL_TIME = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        $CURLINFO_NAMELOOKUP_TIME = curl_getinfo($ch, CURLINFO_NAMELOOKUP_TIME);
        $CURLINFO_CONNECT_TIME = curl_getinfo($ch, CURLINFO_CONNECT_TIME);
        $CURLINFO_STARTTRANSFER_TIME = curl_getinfo($ch, CURLINFO_STARTTRANSFER_TIME);
        $CURLINFO_PRETRANSFER_TIME = curl_getinfo($ch, CURLINFO_PRETRANSFER_TIME);

        $CURLINFO_TOTAL_TIME = sprintf("%.4f", $CURLINFO_TOTAL_TIME);
        $headers_info['TOTAL_TIME'] = $CURLINFO_TOTAL_TIME;

        $CURLINFO_NAMELOOKUP_TIME = sprintf("%.4f", $CURLINFO_NAMELOOKUP_TIME);
        $headers_info['NAMELOOKUP_TIME'] = $CURLINFO_NAMELOOKUP_TIME;

        $CURLINFO_CONNECT_TIME = sprintf("%.4f", $CURLINFO_CONNECT_TIME);
        $headers_info['CONNECT_TIME'] = $CURLINFO_CONNECT_TIME;

        $CURLINFO_STARTTRANSFER_TIME = sprintf("%.4f", $CURLINFO_STARTTRANSFER_TIME);
        $headers_info['STARTTRANSFER_TIME'] = $CURLINFO_STARTTRANSFER_TIME;

        $CURLINFO_PRETRANSFER_TIME = sprintf("%.4f", $CURLINFO_PRETRANSFER_TIME);
        $headers_info['PRETRANSFER_TIME'] = $CURLINFO_PRETRANSFER_TIME;

        //- Total number of bytes uploaded
        $headers_info['CURLINFO_SIZE_UPLOAD'] = curl_getinfo($ch, CURLINFO_SIZE_UPLOAD);
        //Total number of bytes downloaded
        $headers_info['CURLINFO_SIZE_DOWNLOAD'] = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
        //Average download speed
        $headers_info['CURLINFO_SPEED_DOWNLOAD'] = curl_getinfo($ch, CURLINFO_SPEED_DOWNLOAD);
        //Average upload speed
        $headers_info['CURLINFO_SPEED_UPLOAD'] = curl_getinfo($ch, CURLINFO_SPEED_UPLOAD);

        header("Content-Type:text/html; charset=UTF-8");
        curl_close($ch);
//        $headers = explode("\r\n", $headers); // The seperator used in the Response Header is CRLF (Aka. \r\n)
//        $headers = array_filter($headers);
//        $headers_info = '';
//        foreach ($headers as &$value) {
//            $headers_info .= '<li>' . $value . '</li>';
//        }
//        $headers_info = '<ul>' . $headers_info . '</ul>';

        $stoptime = microtime(true);
        $operation_timer = ($stoptime - $starttime) * 1000;
        if ($data_size / 1000 > 1) {
            $data_size_final = sprintf("%.2f", $data_size / 1000) . "k";
        } else {
            $data_size_final = sprintf("%.2f", $data_size);
        }
        if ($operation_timer / 1000 > 1) {
            $timer_final = sprintf("%.2f", $operation_timer / 1000) . "c";
        } else {
            $timer_final = sprintf("%.2f", $operation_timer) . " мс";
        }


        /*
        CURLINFO_TOTAL_TIME - Общее время выполнения транзакции в секундах последней передачи
CURLINFO_NAMELOOKUP_TIME - Время разрешения имени сервера в секундах
CURLINFO_CONNECT_TIME - Время в секундах, затраченное на установку соединения
CURLINFO_PRETRANSFER_TIME - Время в секундах, прошедшее от начала операции до готовности к фактической передаче данных
CURLINFO_STARTTRANSFER_TIME - Время в секундах до передачи первого байта данных
    */

        return [
            "data" => $body,
            "status" => $httpcode,
            "size" => $data_size_final,
            "time" => $timer_final,
            "headers" => $headers_info
        ];
    }

    /**
     * @param $url
     * @param array $params
     * @return mixed
     */
    public function get($url, $params)
    {
        $starttime = microtime(true);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        // ЗАГЛУШКА
        curl_setopt($ch, CURLINFO_HEADER_OUT, true); // enable tracking
        $headers_array = [];

        $headers_array[] = $this->apiHttpHeadersSignature();
        $headers_array[] = $this->apiHttpHeadersUserAgent($params["user-agent"]);
        //default http headers

        $headers_array[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
        $headers_array[] = 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3';

        $headers_array[] = 'Accept-Encoding: gzip';


        $headers_array[] = 'Connection: keep-alive';
        if(key_exists("cookie", $params)){
            $headers_array[] = 'Cookie: ' . $params["cookie"];
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
        curl_setopt($ch, CURLOPT_ENCODING,  'gzip');
        $headers_array[] = 'Upgrade-Insecure-Requests: 1';
        $headers_array[] = 'Cache-Control: max-age=0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);

        $temp_dir = Configure::read('FILE_TEMP_DIR');
        $cookie_jar = tempnam($temp_dir, "__cookie_");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_jar);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $data_size = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        $cookies = curl_getinfo($ch, CURLINFO_COOKIELIST);

        $headers_info = $cookies;
        $headers_info['HTTP'] = $headers;

        $CURLINFO_TOTAL_TIME = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        $CURLINFO_NAMELOOKUP_TIME = curl_getinfo($ch, CURLINFO_NAMELOOKUP_TIME);
        $CURLINFO_CONNECT_TIME = curl_getinfo($ch, CURLINFO_CONNECT_TIME);
        $CURLINFO_STARTTRANSFER_TIME = curl_getinfo($ch, CURLINFO_STARTTRANSFER_TIME);
        $CURLINFO_PRETRANSFER_TIME = curl_getinfo($ch, CURLINFO_PRETRANSFER_TIME);

        $CURLINFO_TOTAL_TIME = sprintf("%.4f", $CURLINFO_TOTAL_TIME);
        $headers_info['TOTAL_TIME'] = $CURLINFO_TOTAL_TIME;

        $CURLINFO_NAMELOOKUP_TIME = sprintf("%.4f", $CURLINFO_NAMELOOKUP_TIME);
        $headers_info['NAMELOOKUP_TIME'] = $CURLINFO_NAMELOOKUP_TIME;

        $CURLINFO_CONNECT_TIME = sprintf("%.4f", $CURLINFO_CONNECT_TIME);
        $headers_info['CONNECT_TIME'] = $CURLINFO_CONNECT_TIME;

        $CURLINFO_STARTTRANSFER_TIME = sprintf("%.4f", $CURLINFO_STARTTRANSFER_TIME);
        $headers_info['STARTTRANSFER_TIME'] = $CURLINFO_STARTTRANSFER_TIME;

        $CURLINFO_PRETRANSFER_TIME = sprintf("%.4f", $CURLINFO_PRETRANSFER_TIME);
        $headers_info['PRETRANSFER_TIME'] = $CURLINFO_PRETRANSFER_TIME;

        //- Total number of bytes uploaded
        $headers_info['CURLINFO_SIZE_UPLOAD'] = curl_getinfo($ch, CURLINFO_SIZE_UPLOAD);
        //Total number of bytes downloaded
        $headers_info['CURLINFO_SIZE_DOWNLOAD'] = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
        //Average download speed
        $headers_info['CURLINFO_SPEED_DOWNLOAD'] = curl_getinfo($ch, CURLINFO_SPEED_DOWNLOAD);
        //Average upload speed
        $headers_info['CURLINFO_SPEED_UPLOAD'] = curl_getinfo($ch, CURLINFO_SPEED_UPLOAD);

        header("Content-Type:text/html; charset=UTF-8");
        curl_close($ch);
//        $headers = explode("\r\n", $headers); // The seperator used in the Response Header is CRLF (Aka. \r\n)
//        $headers = array_filter($headers);
//        $headers_info = '';
//        foreach ($headers as &$value) {
//            $headers_info .= '<li>' . $value . '</li>';
//        }
//        $headers_info = '<ul>' . $headers_info . '</ul>';

        $stoptime = microtime(true);
        $operation_timer = ($stoptime - $starttime) * 1000;
        if ($data_size / 1000 > 1) {
            $data_size_final = sprintf("%.2f", $data_size / 1000) . "k";
        } else {
            $data_size_final = sprintf("%.2f", $data_size);
        }
        if ($operation_timer / 1000 > 1) {
            $timer_final = sprintf("%.2f", $operation_timer / 1000) . "c";
        } else {
            $timer_final = sprintf("%.2f", $operation_timer) . " мс";
        }
        return [
            "data" => $body,
            "status" => $httpcode,
            "size" => $data_size_final,
            "time" => $timer_final,
            "headers" => $headers_info
        ];
    }

    /**
     * @param $domain
     * @return mixed
     */
    public function ping_domain($url)
    {
        $domain = $this->getHostByUrl($url);
        $starttime = microtime(true);
        $file = fsockopen($domain, 80, $errno, $errstr, 10);
        $stoptime = microtime(true);
        $status = 0;
        if (!$file) $status = -1;  // Site is down
        else {
            fclose($file);
            $status = ($stoptime - $starttime) * 1000;
            $status = floor($status);
        }
        return $status;
    }

    /**
     * @param $url
     * @return mixed
     */
    private function getHostByUrl($url)
    {
        $sourceUrl = parse_url($url);
        return $sourceUrl['host'];
    }

    /**
     * @return string
     */
    private function apiHttpHeadersSignature()
    {
        return 'Api-Signature: ApiHive';
    }

    /**
     * @param $user_agent
     * @return string
     */
    private function apiHttpHeadersUserAgent($user_agent)
    {
        $default_user_agent = $_SERVER['HTTP_USER_AGENT'] ?? null;
        if($default_user_agent==null){
            $default_user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0";
        }
        $api_user_agent = $user_agent ? $user_agent : $default_user_agent;
        return "User-Agent: $api_user_agent";
    }

}