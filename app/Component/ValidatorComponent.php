<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Валидатор
 */
class ValidatorComponent extends Component
{
    public $components = array(
        'Log'
    );

    public $controller;

    public $field_errors = [
        'required_numeric_field' => 'Ожидается тип данных Число',
        'required_numeric_string' => 'Ожидается тип данных Строка',
        'value_min_length' => 'Переданное значение меньше (короче) минимально возможного',
        'value_max_length' => 'Переданное значение больше (длиннее) максимально возможного',
        'required_field_is_empty' => 'Значение обязательного поля пусто или отсутствует',
    ];

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    /**
     * @param $error
     * @param $field
     * @return string
     */
    private function returnValidateError($error, $field)
    {
        return "Ошибка валидации значения: " . $error . " в поле " . $field;
    }

    /**
     * @param $fields
     * @param $data
     * @return array|bool
     */
    public function validateData($fields, $data)
    {
        $errors = [];
        foreach ($data as $key => $item) {
            if(!in_array($key, $fields)){
                // не валидировать поля, которых нет в структуре fields для данной модели, то есть они пропускаются
                continue;
            }
            $valid__type = $fields[$key]['type'];
            $valid__min_length = $fields[$key]['min_length'];
            $valid__max_length = $fields[$key]['max_length'];
            $valid__required = $fields[$key]['required'];
            if ($valid__type == "numeric" and !is_numeric($item[$key])) {
                $errors[] = $this->returnValidateError($this->field_errors['required_numeric_field'], $key);
            }
            if ($valid__type == "text" and !is_string($item[$key])) {
                $errors[] = $this->returnValidateError($this->field_errors['required_numeric_string'], $key);
            }
            if (mb_strlen($item[$key]) < $valid__min_length) {
                $errors[] = $this->returnValidateError($this->field_errors['value_min_length'], $key);
            }
            if (mb_strlen($item[$key]) > $valid__max_length) {
                $errors[] = $this->returnValidateError($this->field_errors['value_max_length'], $key);
            }
            if (empty($item[$key]) && $valid__required == true) {
                $errors[] = $this->returnValidateError($this->field_errors['required_field_is_empty'], $key);
            }
        }
        foreach ($fields as $f_key => $f_value) {
            $valid__required = $fields[$f_key]['required'];
            if ((!key_exists($f_key, $data) OR empty($data[$f_key])) && $valid__required == true) {
                $errors[] = $this->returnValidateError($this->field_errors['required_field_is_empty'], $f_key);
            }
        }
        if (count($errors) == 0) {
            return true;
        }
        return $errors;
    }

    public function valid_hash($hash)
    {
        if (strlen(trim($hash)) != 32) {
            return false;
        } else return true;
    }

    public function valid_password($pass)
    {
        if (preg_match("/^[a-zA-Z0-9_!@#$%^&*]{8,32}$/i", $pass)) {
            return true;
        } else return false;
    }

    public function valid_directory($name)
    {
        if (preg_match("/^(\w+\.?)*\w+$/i", $name)) {
            return true;
        } else return false;
    }


    public function valid_mail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else return false;
    }

    public function valid_phone_number($number)
    {
        if (preg_match("/^[0-9]{11}$/", $number)) {
            return true;
        }
        return false;
    }

    public function valid_minute($int)
    {
        if ($int >= 0 and $int <= 59) {
            return true;
        } else return false;
    }

    public function valid_hour($int)
    {
        if ($int >= 0 and $int <= 23) {
            return true;
        } else return false;
    }

    public function valid_date($date)
    {
        return (bool)strtotime($date);
    }

    private function is_valid_name($string, $min, $max)
    {
        if ((!empty($string)) and (mb_strlen($string) <= $max) and (mb_strlen($string) >= $min)
            and (preg_match('/^[a-zA-ZабвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ\s\- ]+$/u', $string))
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function valid_firstname($string)
    {
        return $this->is_valid_name($string, 2, 32);
    }

    public function valid_lastname($string)
    {
        return $this->is_valid_name($string, 2, 32);
    }

    public function valid_middlename($string)
    {
        return $this->is_valid_name($string, 2, 32);
    }

    public function valid_sex($string)
    {
        if ($string == 'male' or $string == 'female') {
            return true;
        } else {
            return false;
        }
    }

    public function valid_building($string)
    {
        if ((!empty($string)) and (mb_strlen($string) <= 1) and (mb_strlen($string) >= 32)
            and (preg_match('/^[_a-zA-Z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ\. ]+$/u', $string))
        ) {

            return true;
        } else {
            return false;
        }
    }

    public function valid_house($string)
    {
        if ((!empty($string)) and (mb_strlen($string) <= 1) and (mb_strlen($string) >= 32)
            and (preg_match('/^[_a-zA-Z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ\. ]+$/u', $string))
        ) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $int
     * @return bool
     */
    public function valid_int($int)
    {
        if (preg_match('/^\d+$/', $int)) {
            return true;
        }
        return false;
    }
}