<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент  Аккаунт (Пользователь API)
 */
class ClientapiComponent extends Component
{
    public $components = [
        'Session',
        'Error',
        'User'
    ];

    public $undefined_client_api_method_error = "Error! Request of undefined client api method";

    public $controller;

    public $request_id, $app_secret_key, $app_name, $request_date, $client_api_url, $token;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /*
     * методы:
     * 1. генерация crsf-токена +
     * 2. Добавление нового клиента
     * 3. Общий API метод
     * 4. Получение данных по клиенту из КБ
     * 5. Получение апдейтов по клиенту из КБ
     * 6. Обновление данных клиента в КБ
     * 7. Проверка наличия клиента по номеру телефона в КБ
     * 8. Проверка того, что ответ базы был корректный
     * */

    /**
     * @param $request_type
     * @param $client_id
     * @param $request_data
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    private function generateRequest($request_type, $client_id, $request_data)
    {
        $modelName = "Client_Api";
        $this->Client_Api = ClassRegistry::init($modelName);
        $this->request_date = gmdate('D, d M Y H:i:s T');
        $request_data = json_encode($request_data);
        $new_request = [
            "type" => $request_type,
            "data" => $request_data,
            "origin_id" => $client_id,
            "date" => $this->request_date,
            "token" => "",
            "result" => null
        ];
        $this->Client_Api->save($new_request);
        $this->request_id = $this->Client_Api->id;

        $this->app_secret_key = Configure::read('MOBILE_APP_KEY');
        $this->app_name = Configure::read('CLIENT_BASE_APP_NAME');

        $this->client_api_url = Configure::read('CLIENT_BASE_URL');

        $this->token = $this->generateCsrfToken($this->request_id, $this->app_secret_key, $this->request_date);
        $this->Client_Api->save(["token" => $this->token]);
        return $this->request_id;
    }

    /**
     * @param $request_id
     * @param $app_secret_key
     * @param $request_date
     * @return string
     */
    private function generateCsrfToken($request_id, $app_secret_key, $request_date)
    {
        return hash('sha256', $request_id . $app_secret_key . $request_date);
    }

    /**
     * @param $string
     * @return bool
     */
    private function isJson($string)
    {
        if(is_array($string)){
            return false;
        }
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * @param $request_type
     * @param $client_id
     * @param $request_data
     * @return false|mixed|string
     */
    private function clientApiMethod($request_type, $client_id, $request_data)
    {
        $api_method_name = "";
        if ($request_type == "create_client") {
            $api_method_name = "user/create/";
        } else if ($request_type == "get_client_data") {
            $api_method_name = "user/$client_id/get/";
        } else if ($request_type == "update_client_data") {
            $api_method_name = "user/$client_id/update/";
        } else if ($request_type == "get_client_updates") {
            $api_method_name = "user/$client_id/updates/";
        } else if ($request_type == "client_check") {
            $api_method_name = "user/check/";
        } else {
            die($this->undefined_client_api_method_error . " " . $request_type);
        }

        $url = $this->client_api_url . $api_method_name;

        if(!$this->isJson($request_data)){
            $request_data = json_encode($request_data);
        }
        $this->app_name = ucfirst(strtolower($this->app_name));
        $options = [
            'http' => [
                'method' => 'POST',
                'content' => $request_data,
                'header' =>
                    "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n" .
                    "Date: " . $this->request_date . "\r\n" .
                    "Content-ID: " . $this->request_id . "\r\n" .
                    "Request_ID: " . $this->request_id . "\r\n" .
                    "X-Csrf-Token: " . $this->token . "\r\n" .
                    "Access_Key: " . $this->token . "\r\n" .
                    "Title: " . $this->app_name . "\r\n" .
                    "App_Name: " . $this->app_name . "\r\n",
            ],
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === false) { /* Handle error */
        }
        return $result;
    }

    /**
     * @param $status
     * @return void
     * @throws Exception
     */
    private function saveRequestStatus($status)
    {
        $modelName = "Client_Api";
        $this->Client_Api = ClassRegistry::init($modelName);
        $this->Client_Api->id = $this->request_id;
        $this->Client_Api->save(["result" => $status]);
    }

    /**
     * @param $request_data
     * @return bool
     * @throws Exception
     */
    public function apiCreateClient($request_data)
    {
        $this->generateRequest("create_client", null, $request_data);
        $result = $this->clientApiMethod("create_client", null, $request_data);
        if (!$result) {
            $this->saveRequestStatus("error");
            return false;
        }
        $this->saveRequestStatus($result);
        return true;
    }

    /**
     * @param $client_id
     * @return bool
     * @throws Exception
     */
    public function apiGetClientData($client_id)
    {
        $this->generateRequest("get_client_data", $client_id, null);
        $result = $this->clientApiMethod("get_client_data", $client_id, null);
        if (!$result) {
            $this->saveRequestStatus("error");
            return false;
        }
        $this->saveRequestStatus($result);
        return true;
    }

    public function apiUpdateClientData($client_id, $request_data)
    {
        $this->generateRequest("update_client_data", $client_id, $request_data);
        $result = $this->clientApiMethod("update_client_data", $client_id, $request_data);
        if (!$result) {
            $this->saveRequestStatus("error");
            return false;
        }
        $this->saveRequestStatus($result);
        return true;
    }

    /**
     * @param $client_id
     * @param $updates_from
     * @return bool
     * @throws Exception
     */
    public function apiGetClientUpdates($client_id, $updates_from)
    {
        $this->generateRequest("get_client_updates", $client_id, $updates_from);
        $result = $this->clientApiMethod("get_client_updates", $client_id, ["updates_from" => $updates_from]);
        if (!$result) {
            $this->saveRequestStatus("error");
            return false;
        }
        $this->saveRequestStatus($result);
        return true;
    }

    /**
     * @param $phone
     * @return bool
     * @throws Exception
     */
    public function apiCheckClientByPhone($phone)
    {
        $this->generateRequest("client_check", null, $phone);
        $result = $this->clientApiMethod("client_check", null, ["phone" => $phone]);
        if (!$result) {
            $this->saveRequestStatus("error");
            return false;
        }
        $this->saveRequestStatus($result);
        return true;
    }

    /**
     * @param $user_id
     *
     * @return mixed
     */
    public function getUserData($user_id)
    {
        $modelName = "User";
        $this->User = ClassRegistry::init($modelName);
        $user = $this->User->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $user_id,
                    ],
            ]
        );

        return $user;
    }

    public function apiRecreateClients(){
        $users = $this->User->userSimpleList();

        foreach ($users as $user){
            $user_data = $user['User'];
            //if( $user_data['id']>=7) continue;
            $user_data['origin_id'] = $user_data['id'];
            $this->apiCreateClient($user_data);
        }
    }

}