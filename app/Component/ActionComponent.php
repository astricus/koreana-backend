<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Акция
 */
class ActionComponent extends Component
{
    public $components         = [
        'City',
        'Session',
        'Error',
        'Admin',
        'Validator',
        'Platform',
        'Uploader',
        'Content',
        'Api',
    ];

    public $fields             = [
        "title"     => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => true,],
        "preview"   => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => false,],
        "content"   => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => true,],
        "author_id" => ["type" => "numeric", "min_length" => "1", "max_length" => "", "required" => false,],
        "enabled"   => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => false,],
    ];

    public $errors             = [
        "invalid_action_status"    => "Передан некорректный статус акции",
        "action_with_id_not_found" => "Акции с данным идентификатором не существует",
    ];

    public $statuses           = [
        'enabled'  => 1,
        'disabled' => 0,
    ];

    public $api_methods        = [
        "list" => "actionListByAPi",
        "get"  => "getActionById",
    ];

    public $default_show_count = 9999;

    public $default_sort_field = "created";

    const AUTH_REQUIRES_MESSAGE = "Error! This action requires user authorization. Please, authorize with your credential.";

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName         = "Action";
        $this->Action      = ClassRegistry::init($modelName);
        $modelName         = "City_Action";
        $this->City_Action = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function checkActionExists($id)
    {
        $this->setup();

        return $this->Action->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function getActionById($id)
    {
        $this->setup();
        $action = $this->Action->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($action) > 0) {
            $image = $action['Action']['image'];
            if ($image == "") {
                $action['Action']['image_url'] = null;
            } else {
                $action['Action']['image_url'] = $this->Content->getContentImageUrl($image);
            }
            if ($this->Api->is_API()) {
                $action['Action']['content_text'] = htmlspecialchars($action['Action']['content']);
            }
            $action['Action']['cities'] = $this->City->getCityIdByActionId($action['Action']['id']);

            return $action['Action'];
        }
    }

    public function getActionName($id)
    {
        $this->setup();
        $action = $this->Action->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($action) > 0) {
            return $action['Action']['title'];
        }

        return null;

    }

    /**
     * @param $id
     *
     * @return string
     */
    public function getAuthorByAuthorId($id)
    {
        $author = $this->Admin->getManagerById($id);

        return prepare_fio($author['firstname'], $author['lastname'], "");
    }

    public function totalActionCount()
    {
        $this->setup();
        $action_count = $this->Action->find(
            "count",
            [
                'conditions' =>
                    [],
            ]
        );

        return $action_count;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function createAction($data)
    {
        $this->setup();
        $validate_result = $this->validateActionFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        if (empty($data['start_date']) || empty($data['start_time']) || $data['start_now'] !== "on") {
            $data['start_datetime'] = now_date();
        } else {
            if(empty($data['start_time'])){
                $data['start_time'] = "00:00";
            }
            $data['start_datetime'] = $data['start_date'] . " " . $data['start_time'] . ":00";
        }

        if(empty($data['stop_time'])){
            $data['stop_time'] = "00:00";
        }
        if(empty($data['stop_date'])){
            $data['stop_date'] = "2032-01-01";
        }
        $data['stop_datetime'] = $data['stop_date'] . " " . $data['stop_time'] . ":00";

        $data["author_id"] = $this->Admin->manager_id();

        //сохранение платформы
        $data['platforms'] = $this->Platform->platformsToString($data['platform']);
        if(empty($data['platform'])){
            $data['platform'] = "main";
        }

        $this->Action->create();
        if ($this->Action->save($data)) {
            return $this->Action->id;
        }

        return false;
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return array|null
     */
    public function actionList($show_count, $page, $sort, $sort_dir, $filter)
    {

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $view_active  = "";
        $not_finished = "";
        if (isset($filter["show_active"]) && $filter["show_active"] == true) {
            $view_active  = 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(start_datetime)>0';
            $not_finished = 'UNIX_TIMESTAMP(stop_datetime)-UNIX_TIMESTAMP(NOW())>0';
        }

        $this->setup();

        $platform_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "platform") {
                $platform_filter = ['Action.platforms LIKE' => "%$f_value%"];
            }
        }

        $city_id_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "city_id") {
                $city_id_filter = ['City_Action.city_id' => $f_value];
            }
        }

        $start_datetime_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "start_datetime") {
                $start_datetime_filter = ['Action.start_datetime >' => $f_value];
            }
        }

        $enabled = [];
        if ($this->Api->is_API()) {
            $enabled = ['Action.enabled' => 1];
        }

        $action_list = $this->Action->find(
            "all",
            [
                'conditions' =>
                    [
                        $enabled,
                        $view_active,
                        $platform_filter,
                        $start_datetime_filter,
                        $city_id_filter,
                        $not_finished,
                    ],
                'joins'      => [
                    [
                        'table'      => 'city_actions',
                        'alias'      => 'City_Action',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'City_Action.action_id = Action.id',
                        ],
                    ],
                ],
                'limit'      => $show_count,
                'offset'     => $limit_page,
                'order'      => [$sort . " " . $sort_dir],
                'fields'     => ['DISTINCT Action.id, Action.*'],
            ]
        );

        if (count($action_list) > 0) {
            foreach ($action_list as &$action) {
                $author_id             = $action['Action']['author_id'];
                $action['author_name'] = $this->getAuthorByAuthorId($author_id);
                $action['platforms']   = $this->Platform->platformsToArray($action['Action']['platforms']);

                $image = $action['Action']['image'];
                if ($image == "") {
                    $action['image_url'] = null;
                } else {
                    $action['image_url'] = $this->Content->getContentImageUrl($image);
                }
                if ($this->Api->is_API()) {
                    $action['Action']['content_text'] = htmlspecialchars($action['Action']['content']);
                    $action['Action']['cities']       = $this->City->getCityIdByActionId($action['Action']['id']);
                }
            }

            return $action_list;
        }

        return [];
    }

    /**
     * @param $params
     *
     * @return array|null
     */
    public function actionListByAPi($params)
    {
        $show_count = $params['show_count'] ?? $this->default_show_count;
        $page       = $params['page'] ?? 1;
        $sort       = $params['sort'] ?? $this->default_sort_field;
        $sort_dir   = $params['sort_dir'] ?? 'desc';
        $filter     = $params['filter'] ?? null;

        // показ акций, у которых время показа наступило - специально для API
        $filter['show_active'] = true;

        return $this->actionList($show_count, $page, $sort, $sort_dir, $filter);
    }

    /**
     * @param $id
     * @param $data
     *
     * @return bool
     */
    public function updateAction($id, $data)
    {
        if (!$this->checkActionExists($id)) {
            $this->returnError("General", $this->errors['action_with_id_not_found']);

            return false;
        }
        $validate_result = $this->validateActionFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        if (empty($data['start_date']) || empty($data['start_time'])) {
            $data['start_datetime'] = now_date();
        } else {
            $data['start_datetime'] = $data['start_date'] . " " . $data['start_time'] . ":00";
        }
        $data['stop_datetime'] = $data['stop_date'] . " " . $data['stop_time'] . ":00";

        //сохранение платформы
        $data['platforms'] = $this->Platform->platformsToString($data['platform']);
        $this->Action->id  = $id;
        if ($this->Action->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @param $action_id
     *
     * @return bool
     * @throws Exception
     */
    public function saveActionImage($file, $width, $height, $action_id)
    {
        $saving = $this->Content->saveContentImage($file, $width, $height);
        if ($saving['status']) {
            $modelName        = "Action";
            $this->Action     = ClassRegistry::init($modelName);
            $this->Action->id = $action_id;
            $this->Action->save(['image' => $saving['name']]);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function validateActionFields($data)
    {
        return $this->Validator->validateData($this->fields, $data);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        if (!$this->checkActionExists($id)) {
            $this->returnError("General", $this->errors['action_with_id_not_found']);

            return false;
        }
        $this->Action->id = $id;
        $this->Action->delete();

        return true;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    private function validateStatus($status)
    {
        if ($status !== 1 and $status !== 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockAction($id)
    {
        $this->setup();
        if (!$this->checkActionExists($id)) {
            $this->returnError("General", $this->errors['action_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockAction($id)
    {
        $this->setup();
        if (!$this->checkActionExists($id)) {
            $this->returnError("General", $this->errors['action_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    private function setStatus($id, $status)
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->errors['invalid_action_status']);

            return false;
        }
        $update_data      = ['enabled' => $status];
        $this->Action->id = $id;
        if ($this->Action->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }
}