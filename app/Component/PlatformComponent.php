<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Платформа
 */
class PlatformComponent extends Component
{
    public $components = array();

    public $platforms = [
        "main",
        "japan",
        "korean",
        "china",
        "mobile"
    ];

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param $string
     * @return array
     */
    public function platformsToArray($string)
    {
        $arr=[];
        foreach ($this->platforms as $item){
            if(substr_count($string, $item)>0){
                $arr[] = $item;
            }
        }
        return $arr;
    }

    public function platforms(){
        return $this->platforms;
    }

    /**
     * @param $arr
     * @return string
     */
    public function platformsToString($arr)
    {
        return implode(" ", $arr);
    }
}