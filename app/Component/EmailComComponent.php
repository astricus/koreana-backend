<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Email
 */
class EmailComComponent extends Component
{
    public $components        = [
        'Session',
        'Error',
        'Log',
    ];

    public $critical_priority = 3;

    public $high_priority     = 2;

    public $normal_priority   = 1;

    public $controller;

    public $email_debug       = false;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param $receiver
     * @param $sender
     * @param $subject
     * @param $layout
     * @param $template
     * @param $message_data
     */
    private function sendEmail($receiver, $sender, $subject, $layout, $template, $message_data, $attachment)
    {
        if ($sender == "null" or $sender == null) {
            $sender = Configure::read('SITE_MAIL');
        }

        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail();
        $email->config("koreana");
        $email->emailFormat('html');
        $email->template($template, $layout);
        $email->from($sender);
        $email->to($receiver);
        $email->subject($subject);
        if ($attachment != null) {
            $email->attachments($attachment);
        }
        $email->viewVars(['sended_data' => $message_data]);
        $email->send();
    }

    /**
     * @param        $receiver
     * @param        $sender
     * @param        $subject
     * @param        $layout
     * @param        $template
     * @param        $message_data
     * @param string $attachment
     * @param string $start_sending
     */
    public function sendEmailNow(
        $receiver,
        $sender,
        $subject,
        $layout,
        $template,
        $message_data,
        $attachment = "",
        $start_sending = ""
    ) {
        $this->sendEmail($receiver, $sender, $subject, $layout, $template, $message_data, $attachment);
    }

    /**
     * @param        $receiver
     * @param        $sender
     * @param        $subject
     * @param        $layout
     * @param        $template
     * @param        $message_data
     * @param int    $importance
     * @param string $attachment
     * @param string $start_sending
     * @param int    $query_id
     *
     * @return mixed
     */
    public function sendEmailLater(
        $receiver,
        $sender,
        $subject,
        $layout,
        $template,
        $message_data,
        $importance = 1,
        $attachment = "",
        $start_sending = "",
        $query_id = 0
    ) {
        $this->setupModel();
        $new_mail = [
            'receiver'      => $receiver,
            'sender'        => $sender,
            'query_id'      => $query_id,
            'subject'       => $subject,
            'layout'        => $layout,
            'template'      => $template,
            'message_data'  => $message_data,
            'status'        => 'new',
            'attachment'    => $attachment,
            'importance'    => $importance,
            'start_sending' => $start_sending,

        ];
        $this->Mail_Item->save($new_mail);

        return $this->Mail_Item->id;
    }

    public function setupModel()
    {
        $modelName       = "MailQuery";
        $this->MailQuery = ClassRegistry::init($modelName);

        $modelName       = "Mail_Item";
        $this->Mail_Item = ClassRegistry::init($modelName);
    }

    /**
     * @param int $amount
     */
    public function sendQuery($amount = 20)
    {
        $this->setupModel();
        $mailing_query = $this->Mail_Item->find(
            "all",
            [
                'conditions' =>
                    [
                        'status !=' => 'success',
                        'UNIX_TIMESTAMP(start_sending) > UNIX_TIMESTAMP(NOW())',
                    ],
                'order'      => ['importance DESC'],
                'limit'      => $amount,
            ]
        );
        foreach ($mailing_query as $mailing_item) {
            $mailing_item = $mailing_item['Mail_Item'];
            $this->sendEmail(
                $mailing_item['receiver'],
                $mailing_item['sender'],
                $mailing_item['subject'],
                $mailing_item['layout'],
                $mailing_item['template'],
                $mailing_item['message_data'],
                $mailing_item['attachment']
            );
            $this->Mail_Item->id = $mailing_item['id'];
            $this->Mail_Item->save(['status' => 'success']);
        }
    }

    //MAILJET

    /**
     * @param $recipient_name
     * @param $recipient_email
     * @param $subject
     * @param $HTML
     *
     * @return bool
     */
    public function sendTestKoreana()
    {
        $receiver     = 'homoastricus2011@gmail.com';
        $subject      = "1234";
        $message_data = ['1234'];
        $attachment   = null;
        $sender       = null;
        $template     = "sto_service_template";
        $layout       = "sto_service_layout";
        $this->sendEmail($receiver, $sender, $subject, $layout, $template, $message_data, $attachment);

        return true;
    }
}