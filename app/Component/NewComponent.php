<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Новость
 */
class NewComponent extends Component
{
    public $components = array(
        'Admin',
        'Api',
        'City',
        'Content',
        'Error',
        'Platform',
        'Session',
        'Uploader',
        'Validator',
    );

    public $fields = [
        "title" => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => true,],
        "category" => ["type" => "text", "min_length" => "3", "max_length" => "255", "required" => true,],
        "preview" => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => false,],
        "content" => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => true,],
        "author_id" => ["type" => "numeric", "min_length" => "1", "max_length" => "", "required" => false,],
        "enabled" => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => false,],
    ];

    public $errors = [
        "invalid_news_status" => "Передан некорректный статус новости",
        "news_with_id_not_found" => "Новость с данным идентификатором не существует",
    ];

    public $statuses = [
        'enabled' => 1,
        'disabled' => 0
    ];

    public $api_methods = [
        "list" => "newsListByAPi",
        "get" => "getNewById",
    ];

    public $default_show_count = 9999;
    public $default_sort_field = "created";

    const AUTH_REQUIRES_MESSAGE = "Error! This action requires user authorization. Please, authorize with your credential.";

    public $controller;

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "News";
        $this->News = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function checkNewsExists($id)
    {
        $this->setup();
        return $this->News->find("count",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
    }

    /**
     * @param $id
     * @return mixed
     *
     *
     */
    public function getNewById($id)
    {
        $this->setup();
        $new = $this->News->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
        if (count($new) > 0) {
            $image = $new['News']['image'];
            if($image == ""){
                $new['News']['image_url'] = null;
            } else {
                $new['News']['image_url'] = $this->Content->getContentImageUrl($image);
            }
            if($this->Api->is_API()){
                $new['News']['content_text'] = htmlspecialchars($new['News']['content']);
                $new['News']['cities'] = $this->City->getCityIdByNewId($new['News']['id']);
            }
            return $new['News'];
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function getAuthorByAuthorId($id)
    {
        $author = $this->Admin->getManagerById($id);
        return prepare_fio($author['firstname'], $author['lastname'], "");
    }

    public function totalNewsCount()
    {
        $this->setup();
        $news_count = $this->News->find("count",
            array('conditions' =>
                array(),
            )
        );
        return $news_count;
    }

    /**
     * @param $data
     * @return bool
     */
    public function createNew($data)
    {
        $this->setup();
        $validate_result = $this->validateNewsFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));
            return false;
        }

        if (empty($data['start_date']) || empty($data['start_time']) || $data['start_now'] !== "on") {
            $data['start_datetime'] = now_date();
        } else {
            $data['start_datetime'] = $data['start_date'] . " " . $data['start_time'] . ":00";
        }

        $data['stop_datetime'] = $data['stop_date'] . " " . $data['stop_time'] . ":00";

        $data["author_id"] = $this->Admin->manager_id();
        //сохранение платформы
        $data['platforms'] = $this->Platform->platformsToString($data['platform']);

        $this->News->create();
        if ($this->News->save($data)) {
            return $this->News->id;
        }
        return false;
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     * @return |null
     */
    public function newsList($show_count, $page, $sort, $sort_dir, $filter)
    {

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $view_active = "";
        if (isset($filter["show_active"]) && $filter["show_active"] == true) {
            $view_active = 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(start_datetime)>0';
        }

        $platform_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "platform") {
                $platform_filter = array('News.platforms LIKE' => "%$f_value%");
            }
        }

        $city_id_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "city_id") {
                $city_id_filter = array('City_News.city_id' => $f_value);
            }
        }

        $view_category = "";
        if (isset($filter["category"]) && !empty($filter["category"])) {
            $view_category = array('News.category' => $filter["category"]);
        }

        $start_datetime_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "start_datetime") {
                $start_datetime_filter = array('News.start_datetime >' => $f_value);
            }
        }

        $this->setup();
        $news_list = $this->News->find("all",
            array('conditions' =>
                array(
                    //'id' => $id
                    $platform_filter,
                    $view_active,
                    $start_datetime_filter,
                    $city_id_filter,
                    $view_category
                ),
                'joins' => array(
                    array(
                        'table' => 'city_news',
                        'alias' => 'City_News',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'City_News.news_id = News.id'
                        )
                    ),
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array($sort . " " . $sort_dir),
                'fields' => array('DISTINCT News.id, News.*')
            )
        );
        if (count($news_list) > 0) {
            foreach ($news_list as &$news_item){
                $image = $news_item['News']['image'];
                if($image == ""){
                    $news_item['image_url'] = null;
                } else {
                    $news_item['image_url'] = $this->Content->getContentImageUrl($image);
                }
                if($this->Api->is_API()){
                    $news_item['News']['content_text'] = htmlspecialchars($news_item['News']['content']);
                    $news_item['News']['cities'] = $this->City->getCityIdByNewId($news_item['News']['id']);
                }

            }
            return $news_list;
        }
        return null;
    }

    /**
     * @param $id
     * @param $data
     * @param $params
     * @return array|int|null
     */
    public function newsListByAPi($id, $data, $params)
    {
        $show_count = $data['show_count'] ?? $this->default_show_count;
        $page = $data['page'] ?? 1;
        $sort = $data['sort'] ?? $this->default_sort_field;
        $sort_dir = $data['sort_dir'] ?? 'desc';
        $filter = [];
        // показ новостей, у которых время показа наступило - специально для API
        $filter['show_active'] = true;
        $filter['category'] = $data['category'] ?? null;
        return $this->newsList($show_count, $page, $sort, $sort_dir, $filter);
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateNews($id, $data)
    {
        if (!$this->checkNewsExists($id)) {
            $this->returnError("General", $this->errors['news_with_id_not_found']);
            return false;
        }
        $validate_result = $this->validateNewsFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));
            return false;
        }

        if (empty($data['start_date']) || empty($data['start_time'])) {
            $data['start_datetime'] = now_date();
        } else {
            $data['start_datetime'] = $data['start_date'] . " " . $data['start_time'] . ":00";
        }

        $data['stop_datetime'] = $data['stop_date'] . " " . $data['stop_time'] . ":00";

        $this->News->id = $id;
        //сохранение платформы
        $data['platforms'] = $this->Platform->platformsToString($data['platform']);
        if ($this->News->save($data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @param $news_id
     * @return boolean
     * @throws Exception
     */
    public function saveNewImage($file, $width, $height, $news_id)
    {
        $saving = $this->Content->saveContentImage($file, $width, $height);
        if ($saving['status']) {
            $modelName = "News";
            $this->News = ClassRegistry::init($modelName);
            $this->News->id = $news_id;
            $this->News->save(['image' => $saving['name']]);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    private function validateNewsFields($data)
    {
        return $this->Validator->validateData($this->fields, $data);
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteNew($id)
    {
        if (!$this->checkNewsExists($id)) {
            $this->returnError("General", $this->errors['news_with_id_not_found']);
            return false;
        }
        $this->News->id = $id;
        $this->News->delete();
        return true;
    }

    /**
     * @param $status
     * @return bool
     */
    private function validateStatus($status)
    {
        if ($status !== 1 and $status !== 0) {
            return false;
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function blockNew($id)
    {
        $this->setup();
        if (!$this->checkNewsExists($id)) {
            $this->returnError("General", $this->errors['news_with_id_not_found']);
            return false;
        }
        $this->setStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     * @return bool
     */
    public function unblockNew($id)
    {
        $this->setup();
        if (!$this->checkNewsExists($id)) {
            $this->returnError("General", $this->errors['news_with_id_not_found']);
            return false;
        }
        $this->setStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     */
    private function setStatus($id, $status)
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->errors['invalid_news_status']);
            return false;
        }
        $update_data = array('enabled' => $status);
        $this->News->id = $id;
        if ($this->News->save($update_data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }


}