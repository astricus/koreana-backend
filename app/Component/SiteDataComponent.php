<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Данные Сайта
 */
class SiteDataComponent extends Component
{
    public $components   = [
        'Session',
        'Error',
    ];

    public $controller;

    public $error_text   = [
        'screen_with_id_is_not_found'  => 'экран с переданным идентификатором не найден',
        'screen_title_is_empty'        => 'Заголовок пуст',
        'screen_title_is_too_short'    => 'Заголовок слишком короткий',
        'screen_title_is_too_long'     => 'Заголовок слишком длинный',
        'screen_name_is_empty'         => 'Название пусто',
        'screen_name_is_too_short'     => 'Название слишком короткий',
        'screen_name_is_too_long'      => 'Название слишком длинный',
        'invalid_screen_status'        => 'Неопределенный статус экрана',
        'data_field_with_id_not_found' => 'Поле данных с переданным идентификатором не существует',
    ];

    public $params       = [
        'min_title_length' => 3,
        'max_title_length' => 1024,
        'min_name_length'  => 3,
        'max_name_length'  => 256,
    ];

    public $errors       = [];

    public $error_fields = [];

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName       = "Site_Data";
        $this->Site_Data = ClassRegistry::init($modelName);
        $modelName       = "Screen";
        $this->Screen    = ClassRegistry::init($modelName);
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function getSiteData($key)
    {
        $this->setup();

        return $this->Site_Data->find(
            "first",
            [
                'conditions' =>
                    [
                        'name' => $key,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getDataFieldById($id)
    {
        $this->setup();

        return $this->Site_Data->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @return mixed
     */
    public function getSiteDataList()
    {
        $this->setup();

        return $this->Site_Data->find(
            "all",
            [
                'conditions' =>
                    [
                        //'name' => $key,
                    ],
                'order'      => [
                    'id DESC',
                ],
            ]
        );
    }

    /**
     * @param $title
     * @param $name
     * @param $content
     *
     * @return false
     */
    public function createDataField($title, $name, $content)
    {
        $this->setup();
        $this->validateScreenName($name);
        $this->validateScreenTitle($title);

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "content" => $content,
            "name"    => $name,
            "title"   => $title,
        ];

        $this->Site_Data->create();
        if ($this->Site_Data->save($data)) {
            return $this->Site_Data->id;
        }

        return false;
    }

    /**
     * @param $id
     * @param $title
     * @param $name
     * @param $content
     *
     * @return bool
     */
    public function updateDataField($id, $title, $name, $content)
    {
        $this->setup();
        $this->validateScreenName($name);
        $this->validateScreenTitle($title);

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "content" => $content,
            "name"    => $name,
            "title"   => $title,
        ];

        $this->Site_Data->id = $id;
        if ($this->Site_Data->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteDataField($id)
    {
        if (!$this->checkDataFieldExists($id)) {
            $this->returnError("General", $this->errors['data_field_with_id_not_found']);

            return false;
        }
        $this->Site_Data->id = $id;
        $this->Site_Data->delete();

        return true;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function checkDataFieldExists($id)
    {
        $this->setup();

        return $this->Site_Data->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    // ЭКРАНЫ ПРИВЕТСТВИЙ

    private function checkScreenExists($id)
    {
        $this->setup();

        return $this->Screen->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    public function getScreenList()
    {
        $this->setup();

        return $this->Screen->find(
            "all",
            [
                'conditions' =>
                    [
                        //'name' => $key,
                    ],
                'order'      => [
                    'id DESC',
                ],
            ]
        );
    }

    public function getScreensApi()
    {
        $this->setup();

        return $this->Screen->find(
            "all",
            [
                'conditions' =>
                    [
                        'enabled' => 1,
                    ],
                'order'      => [
                    'id DESC',
                ],
            ]
        );
    }

    public function getScreenById($id)
    {
        $this->setup();

        return $this->Screen->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockScreen($id)
    {
        $this->setup();
        if (!$this->checkScreenExists($id)) {
            $this->returnError("General", $this->error_text['screen_with_id_is_not_found']);

            return false;
        }
        $this->setStatus($id, 0);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockScreen($id)
    {
        $this->setup();
        if (!$this->checkScreenExists($id)) {
            $this->returnError("General", $this->error_text['screen_with_id_is_not_found']);

            return false;
        }
        $this->setStatus($id, 1);
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    private function setStatus($id, $status)
    {
        $update_data      = ['enabled' => $status];
        $this->Screen->id = $id;
        if ($this->Screen->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $title
     * @param $name
     * @param $content
     *
     * @return false
     */
    public function createScreen($title, $name, $content)
    {
        $this->setup();
        $this->validateScreenName($name);
        $this->validateScreenTitle($title);

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "content" => $content,
            "name"    => $name,
            "title"   => $title,
            'enabled' => 1,
        ];

        $this->Screen->create();
        if ($this->Screen->save($data)) {
            return $this->Screen->id;
        }

        return false;
    }

    /**
     * @param $id
     * @param $title
     * @param $name
     * @param $content
     *
     * @return bool
     */
    public function updateScreen($id, $title, $name, $content)
    {
        $this->setup();
        $this->validateScreenName($name);
        $this->validateScreenTitle($title);

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "content" => $content,
            "name"    => $name,
            "title"   => $title,
        ];

        $this->Screen->id = $id;
        if ($this->Screen->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    private function validateScreenName($name)
    {
        if (empty($name)) {
            $this->errors["name"] = $this->error_text['screen_name_is_empty'];
            $this->error_fields[] = "name";

            return false;
        }
        if (mb_strlen($name) < $this->params['min_name_length']) {
            $this->errors["screen"] = $this->error_text['screen_name_is_too_short'];
            $this->error_fields[]   = "name";

            return false;
        }
        if (mb_strlen($name) > $this->params['max_name_length']) {
            $this->errors["screen"] = $this->error_text['screen_name_is_too_long'];
            $this->error_fields[]   = "name";

            return false;
        }

        return true;
    }

    /**
     * @param $title
     *
     * @return bool
     */
    private function validateScreenTitle($title)
    {
        if (empty($title)) {
            $this->errors["title"] = $this->error_text['screen_title_is_empty'];
            $this->error_fields[]  = "title";

            return false;
        }
        if (mb_strlen($title) < $this->params['min_title_length']) {
            $this->errors["screen"] = $this->error_text['screen_title_is_too_short'];
            $this->error_fields[]   = "title";

            return false;
        }
        if (mb_strlen($title) > $this->params['max_title_length']) {
            $this->errors["screen"] = $this->error_text['screen_title_is_too_long'];
            $this->error_fields[]   = "title";

            return false;
        }

        return true;
    }

    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }
}