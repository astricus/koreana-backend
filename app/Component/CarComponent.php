<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Модель(тип)Автомобиля
 */
class CarComponent extends Component
{
    public $components = [
        'Api',
        'Admin',
        'City',
        'Error',
        'Session',
        'Validator',
        'CarApi',
    ];

    public $fields = [
        "auto_service" => [
            "name" => ["type" => "text", "min_length" => "0", "max_length" => "255", "required" => false,],
            "comment" => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => true,],
            "enabled" => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => true,],
        ],
        "auto_service_record" => [
            "service_id" => ["type" => "id", "min_length" => "0", "max_length" => "255", "required" => false,],
            "comment" => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => false,],
            "datetime" => ["type" => "datetime", "min_length" => "", "max_length" => "", "required" => true,],
        ],
    ];

    public $errors = [
        "item_with_id_not_found" => "Услуга с данным идентификатором не существует",
        "auto_service_id_not_found" => "Услуга автосервиса не найдена",
        "brand_with_id_not_found" => "Марка машин с данным идентификатором не существует",
        "model_with_id_not_found" => "Модель машин с данным идентификатором не существует",
        "invalid_status" => "Неопределенный статус",
    ];

    public $statuses = [
        'enabled' => 1,
        'disabled' => 0,
    ];

    public $spec_transmission = [
        'automatic' => 'Автоматическая',
        'manual' => 'Механическая',
        'robot' => 'Робот',
        'variator' => 'Вариатор',
    ];

    public $car_release_min_year = 1979;

    public $car_vin_min_length = 6;

    public $car_vin_max_length = 17;

    public $car_vin_mismatch = "вин номер должен быть не менее 6 символов и не более 17 символов и может содержать только цифры и буквы английского языка";

    public $api_methods = [
        "user_cars" => "apiGetUserCars",
        "brands" => "getBrandsApi",
        "models" => "getModelsApi",
        "validate_car_number" => "validateCarNumberApi",
        //"car_generations" => "apiGetGenerations",
        //"car_configurations" => "apiGetConfigurations",
        "add_car" => "apiAddUserCar",
        "update_car" => "apiUpdateUserCar",
        "delete_car" => "apiDeleteUserCar",
        "specs" => "specParams",
    ];

    public $default_show_count = 9999;

    public $default_sort_field = "created";

    public $controller;

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "User_Car";
        $this->User_Car = ClassRegistry::init($modelName);
        $modelName = "Car_Mark";
        $this->Car_Mark = ClassRegistry::init($modelName);
        $modelName = "Car_Model";
        $this->Car_Model = ClassRegistry::init($modelName);
        $modelName = "Car_Specification";
        $this->Car_Specification = ClassRegistry::init($modelName);
        $modelName = "Car_Generation";
        $this->Car_Generation = ClassRegistry::init($modelName);
        $modelName = "Car_Configuration";
        $this->Car_Configuration = ClassRegistry::init($modelName);
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return mixed
     */
    public function brandList($show_count, $page, $sort, $sort_dir, $filter)
    {
        $this->setup();

        if ($sort !== "id" && $sort !== "created") {
            $sort = "Car_Mark.name";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $view_active = "";
        if (isset($filter["show_active"]) && $filter["show_active"] == true) {
            $view_active = 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(start_datetime)>0';
        }

        $list = $this->Car_Mark->find(
            "all",
            [
                'conditions' =>
                    [
                        $view_active,
                    ],

                'limit' => $show_count,
                'offset' => $page,
                'order' => ["Car_Mark.name ASC"],
                'fields' => ['Car_Mark.id, Car_Mark.name, Car_Mark.created, Car_Mark.popular'],
            ]
        );
        foreach ($list as &$list_item) {
            $brand_id = $list_item['Car_Mark']['id'];
            $model_counter = $this->getModelsByBrandId($brand_id);
            $model_counter = count($model_counter);
            $list_item['count'] = $model_counter;
        }

        return $list;
    }

    public function getUsersCars($show_count, $page, $sort, $sort_dir, $filter)
    {
        $this->setup();
        if ($sort !== "id" && $sort !== "created") {
            $sort = "Car_Mark.name";
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $view_active = "";
        if (isset($filter["show_active"]) && $filter["show_active"] == true) {
            $view_active = 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(start_datetime)>0';
        }
        $user_cars = $this->User_Car->find(
            "all",
            [
                'conditions' =>
                    [
                        //'User_Car.user_id' => $id,
                        //'User_Car.status' => 'active'
                    ],
                'joins' => [
                    [
                        'table' => 'car_marks',
                        'alias' => 'Car_Mark',
                        'type' => 'INNER',
                        'conditions' => [
                            'Car_Mark.id = User_Car.mark_id',
                        ],
                    ],
                    [
                        'table' => 'car_models',
                        'alias' => 'Car_Model',
                        'type' => 'INNER',
                        'conditions' => [
                            'Car_Model.id = User_Car.model_id',
                        ],
                    ],
                    [
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User.id = User_Car.user_id',
                        ],
                    ],
                ],
                'limit' => $show_count,
                'offset' => $page,
                'order' => ["User_Car.created ASC"],
                'fields' => [
                    'Car_Mark.name',
                    'Car_Mark.id',
                    'Car_Model.id',
                    'Car_Model.name',
                    'User_Car.*',
                    'User.*',
                ],
            ]
        );

        //pr($user_cars);
        return $user_cars;

    }

    public function totalBrandCount()
    {
        $this->setup();
        $list = $this->Car_Mark->find(
            "count",
            [
                'conditions' =>
                    [//$view_active,
                    ],
            ]
        );

        return $list;
    }

    public function totalUserCarCount()
    {
        $this->setup();
        $count = $this->User_Car->find(
            "count",
            [
                'conditions' =>
                    [//$view_active,
                    ],
            ]
        );

        return $count;
    }

    public function getBrands()
    {
        $this->setup();
        $brands = $this->Car_Mark->find(
            "all",
            [
                'conditions' =>
                    [],
                "order" => ["popular DESC, name ASC"],
                "fields" => ["Car_Mark.id, Car_Mark.name, Car_Mark.popular"],
            ]
        );
        $brands_arr = [];
        if (count($brands) > 0) {
            foreach ($brands as $brand) {
                $brand = $brand['Car_Mark'];
                $brands_arr[] = $brand;
            }
        }

        return $brands_arr;
    }

    public function getBrandsApi()
    {
        $result["brands"] = $this->getBrands();
        $this->Api->response_api($result, "success");
        exit;
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getModels($id)
    {
        $errors = [];
        if (empty($id) or !is_numeric($id)) {
            $errors[] = "empty mark_id";
            $this->Api->response_api($errors, "error");
            exit;
        }

        $this->setup();
        $models = $this->Car_Model->find(
            "all",
            [
                'conditions' =>
                    [
                        'mark_id' => $id,
                    ],
                "order" => ["popular DESC, name ASC"],
                "fields" => ["Car_Model.id, Car_Model.name, Car_Model.popular"],
            ]
        );
        $model_arr = [];
        if (count($models) > 0) {
            foreach ($models as $model) {
                $model = $model['Car_Model'];
                $model["popular"] = boolval($model["popular"]);
                $model_arr[] = $model;
            }
        }

        return $model_arr;
    }

    /**
     * @param $id
     */
    public function getModelsApi($id)
    {
        $result["model"] = $this->getModels($id);
        $this->Api->response_api($result, "success");
        exit;
    }

    public function apiGetUserCars($id, $params, $data)
    {
        $errors = [];
        if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
            $errors[] = "empty user_id";
            $this->Api->response_api($errors, "error");
            exit;
        }

        $user_cars = $this->getUserCars($data['user_id']);
        $user_cars_arr = [];
        if (count($user_cars) > 0) {
            foreach ($user_cars as $user_car) {
                if (empty($user_car['User_Car']['transmission']) or $user_car['User_Car']['transmission'] == "") {
                    $user_car['User_Car']['transmission'] = null;
                }
                if (empty($user_car['User_Car']['year']) or $user_car['User_Car']['year'] == 0) {
                    $user_car['User_Car']['year'] = null;
                }
                if (empty($user_car['User_Car']['engine_volume']) or $user_car['User_Car']['engine_volume'] == 0) {
                    $user_car['User_Car']['engine_volume'] = null;
                }
                if (empty($user_car['User_Car']['vin']) or $user_car['User_Car']['vin'] == "") {
                    $user_car['User_Car']['vin'] = null;
                }
                $user_cars_arr[] = [
                    'id' => $user_car['User_Car']['id'],
                    'model' => $user_car['Car_Model']['name'],
                    'mark' => $user_car['Car_Mark']['name'],
                    'model_id' => $user_car['Car_Model']['id'],
                    'mark_id' => $user_car['Car_Mark']['id'],
                    'car_number' => $user_car['User_Car']['car_number'],
                    'created' => $user_car['User_Car']['created'],
                    'transmission' => $user_car['User_Car']['transmission'] ?? null,
                    'year' => $user_car['User_Car']['year'] ?? null,
                    'engine_volume' => $user_car['User_Car']['engine_volume'] ?? null,
                    'vin' => $user_car['User_Car']['vin'],

                ];
            }
            $this->Api->response_api($user_cars_arr, "success");
            exit;
        } else {
            $this->Api->response_api([], "success");
            exit;
        }

    }

    public function apiGetGenerations($id)
    {
        return $this->CarApi->apiGetGenerations($id);
    }

    public function apiGetConfigurations($id)
    {
        return $this->CarApi->apiGetConfigurations($id);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function checkAutoServiceExists($id)
    {
        $this->setup();

        return $this->AutoService->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getCarById($id)
    {
        $this->setup();

        return $this->User_Car->find(
            "first",
            [
                'conditions' =>
                    [
                        'User_Car.id' => $id,
                    ],
                'joins' => [
                    [
                        'table' => 'car_marks',
                        'alias' => 'Car_Mark',
                        'type' => 'INNER',
                        'conditions' => [
                            'Car_Mark.id = User_Car.mark_id',
                        ],
                    ],
                    [
                        'table' => 'car_models',
                        'alias' => 'Car_Model',
                        'type' => 'INNER',
                        'conditions' => [
                            'Car_Model.id = User_Car.model_id',
                        ],
                    ],
                ],
                'fields' => ['Car_Mark.name', 'Car_Model.name', 'User_Car.*'],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getUserCars($id)
    {
        $this->setup();

        return $this->User_Car->find(
            "all",
            [
                'conditions' =>
                    [
                        'User_Car.user_id' => $id,
                        'User_Car.status' => 'active',
                    ],
                'joins' => [
                    [
                        'table' => 'car_marks',
                        'alias' => 'Car_Mark',
                        'type' => 'LEFT',
                        'conditions' => [
                            'Car_Mark.id = User_Car.mark_id',
                        ],
                    ],
                    [
                        'table' => 'car_models',
                        'alias' => 'Car_Model',
                        'type' => 'LEFT',
                        'conditions' => [
                            'Car_Model.id = User_Car.model_id',
                        ],
                    ],
                ],

                'order' => ["User_Car.created  ASC"],
                'fields' => ['Car_Mark.*', 'Car_Model.*', 'User_Car.*'],
            ]
        );

    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getUserCarsByModel($id)
    {
        $this->setup();

        return $this->User_Car->find(
            "count",
            [
                'conditions' =>
                    [
                        'User_Car.model_id' => $id,
                        //'User_Car.status' => 'active'
                    ],
            ]
        );

    }

    public function apiUpdateUserCar($id, $params, $data)
    {
        $errors = [];
        if (isset($data['user_id']) and isset($data['user_car_id']) and isset($data['car_number']) and isset($data['mark_id'])) {

            if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
                $errors[] = "empty user_id";
            }
            if (empty($data['mark_id']) or !is_numeric($data['mark_id'])) {
                $errors[] = "empty mark_id";
            }
            if (empty($data['model_id']) or !is_numeric($data['model_id'])) {
                $errors[] = "empty model_id";
            }
            if (empty($data['car_number'])) {
                $errors[] = "empty car_number";
            }
            if (empty($data['user_car_id']) or !is_numeric($data['user_car_id'])) {
                $errors[] = "empty user_car_id";
            }

            if (!$this->checkUserCarOwner($data['user_id'], $data['user_car_id'])) {
                $errors[] = "selected user is not owner of this car";
            }

            if (count($errors) > 0) {
                $this->Api->response_api($errors, "error");
                exit;
            }

            if (!empty($data['vin'])) {
                if (!$this->validateCarSpecParam("vin", $data['vin'])) {
                    $errors[] = "vin number is not valid";
                    $this->Api->response_api($errors, "error");
                    exit;
                }
                $save_data = ['vin' => $data['vin']];
                $this->updateCarSpecification($data['user_car_id'], $save_data);
            }
            if (!empty($data['transmission'])) {
                if (!$this->validateCarSpecParam("transmission", $data['transmission'])) {
                    $errors[] = "transmission value is not valid";
                    $this->Api->response_api($errors, "error");
                    exit;
                }
                $save_data = ['transmission' => $data['transmission']];
                $this->updateCarSpecification($data['user_car_id'], $save_data);
            }
            if (!empty($data['engine_volume'])) {
                if (!$this->validateCarSpecParam("engine_volume", $data['engine_volume'])) {
                    $errors[] = "engine_volume value is not valid";
                    $this->Api->response_api($errors, "error");
                    exit;
                }
                $save_data = ['engine_volume' => $data['engine_volume']];
                $this->updateCarSpecification($data['user_car_id'], $save_data);
            }
            if (!empty($data['year'])) {
                if (!$this->validateCarSpecParam("year", $data['year'])) {
                    $errors[] = "release date year is not valid, year can be in (" . $this->car_release_min_year . "-" . date(
                            "Y"
                        ) . ")";
                    $this->Api->response_api($errors, "error");
                    exit;
                }
                $save_data = ['year' => $data['year']];
                $this->updateCarSpecification($data['user_car_id'], $save_data);
            }

            $update = $this->updateUserCar(
                $data['user_car_id'],
                $data['car_number'],
                $data['mark_id'],
                $data['model_id']
            );
            if ($update) {
                $this->Api->response_api("true", "success");
            } else {
                $this->Api->response_api("false", "error");
            }
            exit;
        }

    }

    /**
     * @param $param
     * @param $params
     * @param $data
     *
     * @return bool
     */
    public function specParams()
    {
        $spec_params = [];
        $engine_vals = [];
        for ($x = 0.8; $x < 6.0; $x += 0.1) {
            $engine_vals[] = sprintf("%1.01f", $x);
        }
        $spec_params['transmission'] = $this->spec_transmission;
        $spec_params['vin'] = $this->car_vin_mismatch;
        $spec_params['volume_engine'] = $engine_vals;
        $years_vals = range(1979, date("Y"), 1);
        $spec_params['year'] = $years_vals;
        $this->Api->response_api($spec_params, "success");
        exit;
    }

    public function validateCarSpecParam($param, $value)
    {
        if ($param == "vin") {
            if (mb_strlen($value) < $this->car_vin_min_length OR
                mb_strlen($value) > $this->car_vin_max_length) {
                return false;
            }

            return true;
        } elseif ($param == "transmission") {
            if (!in_array($value, array_keys($this->spec_transmission))) {
                return false;
            }

            return true;

        } elseif ($param == "engine_volume") {
            if (preg_match("/^[0-9]{1}\.[0-9]{1}+$/u", $value) && $value > 0.1) {
                return true;
            }

            return false;
        } elseif ($param == "year") {
            if ($value >= $this->car_release_min_year && $value <= date("Y")) {
                return true;
            }

            return false;
        } else {
            return false;
        }
    }

    public function apiDeleteUserCar(
        $id,
        $params,
        $data
    )
    {
        $errors = [];
        if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
            $errors[] = "empty user_id";
        }
        if (empty($data['user_car_id']) or !is_numeric($data['user_car_id'])) {
            $errors[] = "empty user_car_id";
        }

        if (!$this->checkUserCarOwner($data['user_id'], $data['user_car_id'])) {
            $errors[] = "selected user is not owner of this car";
        }

        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $update = $this->deleteUserCar($data['user_car_id']);
        if ($update) {
            $this->Api->response_api($data['user_car_id'], "success");
        } else {
            $this->Api->response_api($data['user_car_id'], "error");
        }
        exit;

    }

    /**
     * @param $car_id
     * @param $car_number
     * @param $mark_id
     * @param $model_id
     *
     * @return false
     */
    public function updateUserCar(
        $car_id,
        $car_number,
        $mark_id,
        $model_id
    )
    {
        $this->setup();

        $save_data = [
            'car_number' => $car_number,
            'mark_id' => $mark_id,
            'model_id' => $model_id,
            'status' => 'active',
        ];
        $this->User_Car->id = $car_id;
        if ($this->User_Car->save($save_data)) {
            return $this->User_Car->id;
        }

        return false;
    }

    /**
     * @param $car_id
     *
     * @return bool
     */
    public
    function deleteUserCar(
        $car_id
    )
    {
        $this->setup();
        $this->User_Car->id = $car_id;
        $save_data = [
            'status' => 'hidden',
        ];
        if ($this->User_Car->save($save_data)) {
            return true;
        }

        return false;
    }

    // ГАРАЖ

    /**
     * @param $car_id
     * @param $specs
     *
     * @return bool
     */
    public
    function updateCarSpecification(
        $car_id,
        $specs
    )
    {
        $this->setup();
        $this->User_Car->id = $car_id;
        foreach ($specs as $key => $val) {
            $save_data = [
                $key => $val,
            ];
            $this->User_Car->save($save_data);
        }

        return true;
    }

    /**
     * @param $id
     * @param $params
     * @param $data
     */
    public
    function validateCarNumberApi(
        $id,
        $params,
        $data
    )
    {
        $errors = [];
        if (empty($data['car_number'])) {
            $errors[] = "empty car_number";
        }
        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $add_car = $this->validateCarNumber($data['car_number']);
        if ($add_car) {
            $this->Api->response_api($add_car, "success");
        } else {
            $this->Api->response_api($add_car, "error");
        }
        exit;
    }

    /**
     * @param $car_number
     *
     * @return bool
     */
    private
    function validateCarNumber(
        $car_number
    ): bool
    {
        $number_regexp = "/^[АВЕКМНОРСТУХ]\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\d{2,3}$/ui";
        if (preg_match($number_regexp, $car_number)) {
            return true;
        }

        return false;
    }

    /**
     * @param $data
     * @param $user_id
     */
    public
    function apiAddUserCar(
        $id,
        $params,
        $data
    )
    {
        $errors = [];
        if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
            $errors[] = "empty user_id";
        }
        if (empty($data['mark_id']) or !is_numeric($data['mark_id'])) {
            $errors[] = "empty mark_id";
        }
        if (empty($data['model_id']) or !is_numeric($data['model_id'])) {
            $errors[] = "empty model_id";
        }
        if (empty($data['car_number'])) {
            $errors[] = "empty car_number";
        }
        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $this->setup();
        $check_car_exists = $this->User_Car->find(
            "count",
            [
                'conditions' =>
                    [
                        'user_id' => $data['user_id'],
                        'car_number' => $data['car_number'],
                        'status' => 'active'
                    ],
            ]
        );
        if ($check_car_exists > 0) {
            $this->Api->response_api("машина с данным номером уже привязана к пользователю", "error");
            exit;
        }
        $add_car = $this->addUserCar($data['car_number'], $data['mark_id'], $data['model_id'], $data['user_id']);
        if ($add_car) {
            $this->Api->response_api($add_car, "success");
        } else {
            $this->Api->response_api($add_car, "error");
        }
        exit;

    }

    /**
     * @param $car_number
     * @param $brand_id
     * @param $model_id
     * @param $user_id
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public
    function addUserCar(
        $car_number,
        $brand_id,
        $model_id,
        $user_id
    )
    {
        $this->setup();
        $save_data = [
            'car_number' => $car_number,
            'mark_id' => $brand_id,
            'model_id' => $model_id,
            'user_id' => $user_id,
            'status' => 'active',
        ];
        $this->User_Car->create();
        if ($this->User_Car->save($save_data)) {
            return $this->User_Car->id;
        }

        return false;
    }

    private
    function validateCarFields(
        $data
    )
    {
        return $this->Validator->validateData($this->fields, $data);
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private
    function checkUserCarExists(
        $data
    )
    {
        $this->setup();
        $found = $this->Car->find(
            "count",
            [
                'conditions' =>
                    [
                        'brand_id' => $data['brand_id'],
                        'model_id' => $data['model_id'],
                        'configuration_id' => $data['configuration_id'],
                    ],
            ]
        );
        if ($found == true) {
            return true;
        }

        return false;
    }

    /**
     * @param $user_id
     * @param $user_car_id
     *
     * @return bool
     */
    public
    function checkUserCarOwner(
        $user_id,
        $user_car_id
    )
    {
        $this->setup();
        $found = $this->User_Car->find(
            "count",
            [
                'conditions' =>
                    [
                        'user_id' => $user_id,
                        'id' => $user_car_id,
                        'status' => 'active'
                    ],
            ]
        );
        if ($found == 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private
    function returnError(
        $error_type,
        $error_text
    )
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }

    public
    function getCarBrands()
    {
        $this->setup();

        return $this->Car_Mark->find(
            "all",
            [
                'conditions' =>
                    [//'user_id' => $id
                    ],
                'order' => ["name  ASC"],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public
    function getBrandById(
        $id
    )
    {
        $this->setup();

        return $this->Car_Mark->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
                //'order' => array("name  ASC"),
            ]
        );
    }

    public
    function getModelById(
        $id
    )
    {
        $this->setup();

        return $this->Car_Model->find(
            "all",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
                //'order' => array("name  ASC"),
            ]
        );
    }

    public
    function getModelsByBrandId(
        $brand_id
    )
    {
        $this->setup();
        $models = $this->Car_Model->find(
            "all",
            [
                'conditions' =>
                    [
                        'mark_id' => $brand_id,
                    ],
                //'order' => array("name  ASC"),
            ]
        );
        foreach ($models as &$model) {
            $id = $model['Car_Model']['id'];
            $car_count = $this->getUserCarsByModel($id);
            $model['count'] = $car_count;
        }

        return $models;
    }

    public
    function getCarModels()
    {
        $this->setup();

        return $this->Car_Model->find(
            "all",
            [
                'conditions' =>
                    [//'user_id' => $id
                    ],
                'order' => ["name  ASC"],
            ]
        );
    }

    public
    function getCarSpecification()
    {
        $this->setup();

        return $this->Car_Specification->find(
            "all",
            [
                'conditions' =>
                    [//'user_id' => $id
                    ],
                'order' => ["name  ASC"],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private
    function checkBrandExists(
        $id
    )
    {
        $this->setup();

        return $this->Car_Mark->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private
    function checkModelExists(
        $id
    )
    {
        $this->setup();

        return $this->Car_Model->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    // проверка сушествования машин данного бренда
    private
    function getModelCountUserCars(
        $model_id
    )
    {
        $this->setup();
        $found = $this->Car->find(
            "count",
            [
                'conditions' =>
                    [
                        'brand_id' => $data['brand_id'],
                        'model_id' => $data['model_id'],
                        'configuration_id' => $data['configuration_id'],
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public
    function deleteBrand(
        $id
    )
    {
        if (!$this->checkBrandExists($id)) {
            $this->returnError("General", $this->errors['brand_with_id_not_found']);

            return false;
        }
        $this->Car_Mark->id = $id;
        $this->Car_Mark->delete();

        return true;
    }

    public
    function deleteModel(
        $id
    )
    {
        if (!$this->checkModelExists($id)) {
            $this->returnError("General", $this->errors['model_with_id_not_found']);

            return false;
        }
        $this->Car_Model->id = $id;
        $this->Car_Model->delete();

        return true;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    private
    function validateStatus(
        $status
    )
    {
        if ($status !== 1 and $status !== 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public
    function blockBrand(
        $id
    )
    {
        $this->setup();
        if (!$this->checkBrandExists($id)) {
            $this->returnError("General", $this->errors['brand_with_id_not_found']);

            return false;
        }
        $this->setBrandStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public
    function unblockBrand(
        $id
    )
    {
        $this->setup();
        if (!$this->checkBrandExists($id)) {
            $this->returnError("General", $this->errors['brand_with_id_not_found']);

            return false;
        }
        $this->setBrandStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    private
    function setBrandStatus(
        $id,
        $status
    )
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->errors['invalid_status']);

            return false;
        }
        $update_data = ['enabled' => $status];
        $this->Car_Mark->id = $id;
        if ($this->Car_Mark->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public
    function blockModel(
        $id
    )
    {
        $this->setup();
        if (!$this->checkModelExists($id)) {
            $this->returnError("General", $this->errors['model_with_id_not_found']);

            return false;
        }
        $this->setModelStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public
    function unblockModel(
        $id
    )
    {
        $this->setup();
        if (!$this->checkModelExists($id)) {
            $this->returnError("General", $this->errors['model_with_id_not_found']);

            return false;
        }
        $this->setModelStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    private
    function setModelStatus(
        $id,
        $status
    )
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->errors['invalid_status']);

            return false;
        }
        $update_data = ['enabled' => $status];
        $this->Car_Model->id = $id;
        if ($this->Car_Model->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return false
     */
    public
    function setBrandPopular(
        $id
    )
    {
        $this->setup();
        if (!$this->checkBrandExists($id)) {
            $this->returnError("General", $this->errors['brand_with_id_not_found']);

            return false;
        }
        $save_data = [
            'popular' => 1,
        ];
        $this->Car_Mark->id = $id;
        if ($this->Car_Mark->save($save_data)) {
            return $this->Car_Mark->id;
        }
    }

    /**
     * @param $id
     *
     * @return false
     */
    public
    function setBrandNotPopular(
        $id
    )
    {
        $this->setup();
        if (!$this->checkBrandExists($id)) {
            $this->returnError("General", $this->errors['brand_with_id_not_found']);

            return false;
        }
        $save_data = [
            'popular' => 0,
        ];
        $this->Car_Mark->id = $id;
        if ($this->Car_Mark->save($save_data)) {
            return $this->Car_Mark->id;
        }
    }

    /**
     * @param $id
     *
     * @return false
     */
    public
    function setModelPopular(
        $id
    )
    {
        $this->setup();
        if (!$this->checkModelExists($id)) {
            $this->returnError("General", $this->errors['model_with_id_not_found']);

            return false;
        }
        $save_data = [
            'popular' => 1,
        ];
        $this->Car_Model->id = $id;
        if ($this->Car_Model->save($save_data)) {
            return $this->Car_Model->id;
        }
    }

    /**
     * @param $id
     *
     * @return false
     */
    public
    function setModelNotPopular(
        $id
    )
    {
        $this->setup();
        if (!$this->checkModelExists($id)) {
            $this->returnError("General", $this->errors['model_with_id_not_found']);

            return false;
        }
        $save_data = [
            'popular' => 0,
        ];
        $this->Car_Model->id = $id;
        if ($this->Car_Model->save($save_data)) {
            return $this->Car_Model->id;
        }
    }

    public function today_new_cars()
    {
        $this->setup();
        return $this->User_Car->find(
            "count",
            [
                'conditions' =>
                    [
                        'DATE(NOW()) - DATE(created)' => 0,
                    ],
            ]
        );

    }

    public function total_cars()
    {
        $this->setup();
        return $this->User_Car->find(
            "count",
            [
                'conditions' =>
                    [],
            ]
        );
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return array|int|null
     */
    public function new_cars_by_date($date_from, $date_to)
    {
        $this->setup();
        return $this->User_Car->find(
            "count",
            [
                'conditions' =>
                    [
                        "created >" => $date_from . " 00:00:00",
                        "created <" => $date_to . " 00:00:00",
                    ],
            ]
        );

    }

}