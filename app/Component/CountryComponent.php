<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Страна
 */
class CountryComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Country";
        $this->Country = ClassRegistry::init($modelName);
    }

    public function getCountryNameById($id)
    {
        $this->setup();
        $country = $this->Country->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
        return $country['Country']['name'];
    }

    public function getCountryIdByName($name)
    {
        $this->setup();
        $country = $this->Country->find("first",
            array('conditions' =>
                array(
                    'name' => $name
                )
            )
        );
        return $country['Country']['id'] ?? 0;
    }



}