<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Локации/Контакты
 */
class LocationComponent extends Component
{
    public $components = [
        'Admin',
        'Api',
        'City',
        'Content',
        'Error',
        'Session',
        'Uploader',
        'Validator',
    ];

    public $days_eng = [
        "pnd" => "monday",
        "vt" => "tuesday",
        "sr" => "wednesday",
        "cht" => "thursday",
        "pt" => "friday",
        "sb" => "saturday",
        "vs" => "sunday",
    ];

    public $fields = [
        "title" => ["type" => "text", "min_length" => "0", "max_length" => "255", "required" => false,],
        "address" => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => true,],
        "type" => ["type" => "text", "min_length" => "0", "max_length" => "12", "required" => false,],
        // shop, sto, dealer
        "address_desc" => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => false,],
        "view_on_map" => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => false,],
        "phone" => ["type" => "text", "min_length" => "0", "max_length" => "255", "required" => false,],
        "work_time" => ["type" => "text", "min_length" => "0", "max_length" => "255", "required" => false,],
        "email" => ["type" => "text", "min_length" => "0", "max_length" => "255", "required" => false,],
        "image" => ["type" => "text", "min_length" => "0", "max_length" => "255", "required" => false,],
        "lat" => ["type" => "float", "min_length" => "1", "max_length" => "", "required" => false,],
        "lon" => ["type" => "float", "min_length" => "1", "max_length" => "", "required" => false,],
        "enabled" => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => false,],
    ];

    public $errors = [
        "invalid_location_status" => "Передан некорректный статус локации",
        "item_with_id_not_found" => "Локация с данным идентификатором не существует",
        "cant_delete_location_type" => "Невозможно удалить тип локации в связи с тем, что существуют локации данного типа",
        "location_type_id_not_found" => "Тип локации не найден",
    ];

    public $statuses = [
        'enabled' => 1,
        'disabled' => 0,
    ];

    public $location_types = [
        "shop" => "Магазин",
        "sto" => "Автосервис",
        "dealer" => "Автосалон",
    ];

    public $api_methods = [
        "list" => "locationsByApi",
        "get" => "getLocationById",
        "cities" => "getCitiesByApi",
    ];

    public function getCitiesByApi()
    {
        return $this->City->getCityMock();
    }

    public $default_show_count = 9999;

    public $default_sort_field = "created";

    public $controller;

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Location";
        $this->Location = ClassRegistry::init($modelName);
        $modelName = "Location_Type";
        $this->Location_Type = ClassRegistry::init($modelName);
        $modelName = "Work_Time";
        $this->Work_Time = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function checkLocationExists($id)
    {
        $this->setup();

        return $this->Location->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function checkLocationTypeExists($id)
    {
        $this->setup();

        return $this->Location_Type->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @return mixed
     */
    public function getLocationTypes()
    {
        $this->setup();
        $types = $this->Location_Type->find(
            "all",
            [
                'conditions' =>
                    [],
            ]
        );

        return $types;

    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getLocationTypeNameById($id)
    {
        $this->setup();
        $type = $this->Location_Type->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );

        return $type['Location_Type']['name'];

    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function createLocation_Type($data)
    {
        $this->setup();
        $this->Location_Type->create();
        if ($this->validateLocation_Type($data)) {
            if ($this->Location_Type->save($data)) {
                return $this->Location_Type->id;
            }

            return false;
        }

        return false;
    }

    public function importLocation($data)
    {
        $this->setup();
        $this->Location_Type->create();
        if ($this->validateLocation_Type($data)) {
            if ($this->Location_Type->save($data)) {
                return $this->Location_Type->id;
            }

            return false;
        }

        return false;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function validateLocation_Type($data)
    {
        if (!key_exists("name", $data) or empty($data['name'])) {
            return false;
        }
        $check_name = $this->Location_Type->find(
            "count",
            [
                'conditions' =>
                    [
                        'name' => $data['name'],
                    ],
            ]
        );
        if ($check_name > 0) {
            return false;
        }

        if (!key_exists("type", $data) or empty($data['type'])) {
            return false;
        }
        $check_type = $this->Location_Type->find(
            "count",
            [
                'conditions' =>
                    [
                        'type' => $data['type'],
                    ],
            ]
        );
        if ($check_type > 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getLocationById($id)
    {
        $this->setup();
        $location = $this->Location->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($location) > 0) {
            $image = $location['Location']['image'];
            if ($image == "") {
                $location['Location']['image_url'] = null;
            } else {
                $location['Location']['image_url'] = $this->Content->getLocationImageUrl($image);
            }

            return $location['Location'];
        }
    }

    /**
     * @param $location_id
     * @param $work_time
     *
     * @return bool
     */
    public function saveWorkTime($location_id, $work_time)
    {
        if (!$this->checkLocationExists($location_id)) {
            return false;
        }
        foreach ($work_time as $day => $time) {

            $check_work_time = $this->Work_Time->find(
                "first",
                [
                    'conditions' =>
                        [
                            'day' => $day,
                            'location_id' => $location_id,
                        ],
                ]
            );
            if ($check_work_time == null) {
                $this->Work_Time->create();
            } else {
                $this->Work_Time->id = $check_work_time['Work_Time']['id'];
            }
            $save_data = [
                'location_id' => $location_id,
                'day' => $day,
                'start_time' => $time['start_time'] ?? 0,
                'end_time' => $time['end_time'] ?? 0,
                'is_work_day' => $time['is_work_day'],
            ];
            $this->Work_Time->save($save_data);
        }

        return true;
    }

    /**
     * @param $location_id
     *
     * @return mixed
     */
    public function getWorkTime($location_id)
    {
        $this->setup();
        $work_time = $this->Work_Time->find(
            "all",
            [
                'conditions' =>
                    [
                        'location_id' => $location_id,
                    ],
                'order' => ['id ASC'],
            ]
        );
        $wt = [];
        foreach ($work_time as $time_elem) {
            if ($this->Api->is_API()) {
                if ($time_elem['Work_Time']['is_work_day'] == 0) {
                    continue;
                }
                if ($time_elem['Work_Time']['end_time'] == 0) {
                    continue;
                }
                $start_h_m = $time_elem['Work_Time']['start_time'];
                $start_h_m_arr = explode(":", $start_h_m);

                $end_h_m = $time_elem['Work_Time']['end_time'];
                $end_h_m_arr = explode(":", $end_h_m);

                $wt[$this->days_eng[$time_elem['Work_Time']['day']]] =
                    sprintf('%02d', $start_h_m_arr[0]) . ":" . $start_h_m_arr[1] . "- " . sprintf(
                        '%02d',
                        $end_h_m_arr[0]) . ":" . $end_h_m_arr[1];
            } else {
                $wt[$time_elem['Work_Time']['day']] = [
                    'start_time' => $time_elem['Work_Time']['start_time'],
                    'end_time' => $time_elem['Work_Time']['end_time'],
                    'is_work_day' => $time_elem['Work_Time']['is_work_day'],
                ];
            }
        }

        return $wt;
    }

    public function totalCount($filter, $search)
    {
        $this->setup();
        $view_active = "";
        if (isset($filter["show_active"]) && $filter["show_active"] == true) {
            $view_active = ['enabled' => 1];
        }

        $city_id_filter = [];

        if (!empty($filter['city_id'])) {
            $city_id_filter = ['city_id' => $filter['city_id']];
        }

        $location_filter = [];
        if (!empty($filter['location_type_id'])) {
            $location_filter = ['location_type_id' => $filter['location_type_id']];
        }

        $location_type = [];
        if (!empty($filter['location_type'])) {
            $location_type = ['Location_Type.type' => $filter['location_type']];
        }
        $location_search = [];
        if (!empty($search)) {
            $location_search = ['OR' => array(
                array('address LIKE' => "%" . $search . "%"),
                array('phones LIKE ' => "%" . $search . "%"))
            ];
        }

        return $this->Location->find(
            "count",
            [
                'conditions' =>
                    [
                        $view_active,
                        $city_id_filter,
                        $location_filter,
                        $location_type,
                        $location_search
                    ],
            ]
        );

    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function createLocation($data)
    {
        $this->setup();
        $validate_result = $this->validateFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        /*
        if (empty($data['start_date']) || empty($data['start_time']) || $data['start_now'] !== "on") {
            $data['start_datetime'] = now_date();
        } else {
            $data['start_datetime'] = $data['start_date'] . " " . $data['start_time'] . ":00";
        }

        $data['stop_datetime'] = $data['stop_date'] . " " . $data['stop_time'] . ":00";
        */

        $this->Location->create();
        if ($this->Location->save($data)) {
            return $this->Location->id;
        }

        return false;
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return |null
     */
    public function locations($show_count, $page, $sort, $sort_dir, $filter, $search)
    {
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = (int)$show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $view_active = "";
        if (isset($filter["show_active"]) && $filter["show_active"] == true) {
            $view_active = ['enabled' => 1];
        }

        $city_id_filter = [];

        if (!empty($filter['city_id'])) {
            $city_id_filter = ['city_id' => $filter['city_id']];
        }

        $location_filter = [];
        if (!empty($filter['location_type_id'])) {
            $location_filter = ['location_type_id' => $filter['location_type_id']];
        }

        $location_type = [];
        if (!empty($filter['location_type'])) {
            $location_type = ['Location_Type.type' => $filter['location_type']];
        }
        $location_search = [];
        if (!empty($search)) {
            $location_search = ['OR' => array(
                array('address LIKE' => "%" . $search . "%"),
                array('phones LIKE ' => "%" . $search . "%"))
            ];
        }
        $location_enabled = [];
        if (!empty($filter['enabled'])) {
            $location_enabled = ['enabled' => $filter['enabled']];
        }

        $this->setup();
        $locations = $this->Location->find(
            "all",
            [
                'conditions' =>
                    [
                        $view_active,
                        $city_id_filter,
                        $location_filter,
                        $location_type,
                        $location_enabled,
                        $location_search
                    ],
                'joins' => [
                    [
                        'table' => 'location_types',
                        'alias' => 'Location_Type',
                        'type' => 'INNER',
                        'conditions' => [
                            'Location_Type.id = Location.location_type_id',
                        ],
                    ],
                ],
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => [$sort . " " . $sort_dir],
                'fields' => ['Location_Type.*', 'Location.*'],
            ]
        );
        if (count($locations) > 0) {
            foreach ($locations as &$location) {
                $image = $location['Location']['image'];
                if ($image == "") {
                    $location['image_url'] = null;
                } else {
                    $location['image_url'] = $this->Content->getLocationImageUrl($image);
                }
                if ($this->Api->is_API()) {
                    $address = $location['Location']['address'];
                    if (!empty($address)) {
                        $address_arr = explode(", ", $address);
                        $city_name = $address_arr[0];
                        $new_address_str = "";
                        foreach ($address_arr as $key => $elem) {
                            if ($key == 0) {
                                continue;
                            }
                            $new_address_str .= $elem;
                            if ($key < count($address_arr) - 1) {
                                $new_address_str .= ", ";
                            }
                        }
                    } else {
                        $new_address_str = $address;
                    }
                    $location['Location']['address'] = $new_address_str;
                    unset($location['Location']['created']);
                    unset($location['Location']['modified']);
                    unset($location['Location']['enabled']);
                    $location['location_type_rus'] = $this->getLocationTypeNameById(
                        $location['Location']['location_type_id']
                    );
                    $location['work_time'] = $this->getWorkTime($location['Location']['id']);
                }
            }

            return $locations;
        }

        return [];
    }

    /**
     * @param $filter
     *
     * @return |null
     */
    public function locationMarks($filter)
    {
        $location_filter = [];
        if (!empty($filter['location_type'])) {
            $location_filter = ['location_type' => $filter['location_type']];
        }
        $this->setup();
        $locations = $this->Location->find(
            "all",
            [
                'conditions' =>
                    [

                        $location_filter,
                    ],
            ]
        );
        if (count($locations) > 0) {
            foreach ($locations as &$location) {
                $image = $location['Location']['image'];
                if ($image == "") {
                    $location['image_url'] = null;
                } else {
                    $location['image_url'] = $this->Content->getLocationImageUrl($image);
                }
                if ($this->Api->is_API()) {
                    unset($location['Location']['created']);
                    unset($location['Location']['modified']);
                    unset($location['Location']['enabled']);
                    $location['location_type_rus'] = $this->location_types[$location['Location']['location_type']];
                }
            }

            return $locations;
        }

        return null;
    }

    /**
     * @param $data
     * @param $params
     *
     * @return |null
     */
    public function locationsByAPi($data, $params, $search)
    {
        $show_count = $params['show_count'] ?? $this->default_show_count;
        $page = $params['page'] ?? 1;
        $sort = $params['sort'] ?? $this->default_sort_field;
        $sort_dir = $params['sort_dir'] ?? 'desc';
        $filter = $params ?? [];
        $filter['enabled'] = 1;
        $filter['search'] = $search;

        return $this->locations($show_count, $page, $sort, $sort_dir, $filter, "");
    }

    /**
     * @param $id
     * @param $data
     *
     * @return bool
     */
    public function updateLocation($id, $data)
    {
        if (!$this->checkLocationExists($id)) {
            $this->returnError("General", $this->errors['item_with_id_not_found']);

            return false;
        }
        $validate_result = $this->validateFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        /*
        if (empty($data['start_date']) || empty($data['start_time'])) {
            $data['start_datetime'] = now_date();
        } else {
            $data['start_datetime'] = $data['start_date'] . " " . $data['start_time'] . ":00";
        }

        $data['stop_datetime'] = $data['stop_date'] . " " . $data['stop_time'] . ":00";

        */
        $this->Location->id = $id;
        if ($this->Location->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @param $id
     *
     * @return boolean
     * @throws Exception
     */
    public function saveLocationImage($file, $width, $height, $id)
    {
        $saving = $this->Content->saveLocationImage($file, $width, $height);
        if ($saving['status']) {
            $this->setup();
            $this->Location->id = $id;
            $this->Location->save(['image' => $saving['name']]);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function validateFields($data)
    {
        return $this->Validator->validateData($this->fields, $data);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteLocation($id)
    {
        if (!$this->checkLocationExists($id)) {
            $this->returnError("General", $this->errors['item_with_id_not_found']);

            return false;
        }
        $this->setup();
        $this->Location->id = $id;
        $this->Location->delete();
        // удаление рабочего времени локации
        $work_times = $this->Work_Time->find(
            "all",
            [
                'conditions' =>
                    [
                        'location_id' => $id
                    ],
            ]
        );
        if (count($work_times) > 0) {
            foreach ($work_times as $work_time) {
                $w_id = $work_time['Work_Time']['id'];
                $this->Work_Time->id = $w_id;
                $this->Work_Time->delete();
            }
        }

        return true;
    }

    /**
     * @param $location_type_id
     *
     * @return mixed
     */
    public function getLocationCountByType($location_type_id)
    {
        $this->setup();

        return $this->Location->find(
            "count",
            [
                'conditions' =>
                    [
                        'location_type_id' => $location_type_id,
                    ],
                'order' => ['id ASC'],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteLocationType($id)
    {
        if ($this->checkLocationTypeExists($id) == 0) {
            $this->returnError("General", $this->errors['location_type_id_not_found']);

            return false;
        }
        if ($this->getLocationCountByType($id) > 0) {
            $this->returnError("General", $this->errors['cant_delete_location_type']);

            return false;
        }
        $this->Location_Type->id = $id;
        $this->Location_Type->delete();

        return true;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    private function validateStatus($status)
    {
        if ($status !== 1 and $status !== 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockLocation($id)
    {
        $this->setup();
        if (!$this->checkLocationExists($id)) {
            $this->returnError("General", $this->errors['item_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockLocation($id)
    {
        $this->setup();
        if (!$this->checkLocationExists($id)) {
            $this->returnError("General", $this->errors['item_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    private function setStatus($id, $status)
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->errors['invalid_location_status']);

            return false;
        }
        $update_data = ['enabled' => $status];
        $this->Location->id = $id;
        if ($this->Location->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }

    /**
     * @param $city_id
     * @return array
     */
    public function getStoListByCityId($city_id){
        $this->setup();

        $ids = $this->Location->find(
            "all",
            [
                'conditions' =>
                    [
                        'city_id' => $city_id,
                    ],
                'order' => ['id ASC'],
            ]
        );
        $ids_ar = [];
        foreach ($ids as $l_id){
            $location_id = $l_id['Location']['id'];
            if(!in_array($location_id, $ids_ar)){
                $ids_ar[] = $location_id;
            }
        }
        return $ids_ar;
    }

}