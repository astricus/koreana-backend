<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Пользователь
 */
class UserComComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'ProductCom',
        'Address',
        'OrderCom',
        'CompanyCom',
        'Uploader',
    );

    public $api_methods = [
        "auth" => "auth",
        "saveProfile" => "saveProfile",
    ];

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "User";
        $this->User = ClassRegistry::init($modelName);
    }

    /*
     * методы:
     * 1. авторизация
     * 2. сохранение профиля
     * 3. Отправка смс на номер -
     * 4. Подтверждение - ввод смс -
     * 5. Сообщение об успешной авторизации и выдача user_id
     * */

    /*
     * список вопросов для уточнения:
     * 1. Можно ли посылать смс постоянно при неверных попытках входа или отправлять в бан?
     * */

    public function auth()
    {
        $errors = [];
        $data = $this->params['data'];

        // валидация фио, почты, телефона
        $phone_regexp = "/^[0-9]{11,12}$/iu";

        $data['phone'] = trim($data['phone']);
        $data['phone'] = str_replace("+", "", $data['phone']);

        if (!preg_match($phone_regexp, $data['phone'])) {
            $errors[] = "incorrect field: phone number";
        }


        if ($this->checkPhoneExists($data['phone'])) {
            $errors[] = "incorrect field: entered phone number " . $data['phone'] . " is busy";
        }

        if (count($errors) > 0) {
            return $errors;
        }

        $sms_code = mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9);
        // заглушка
        $sms_code = 7777;

        $auth_data = [
            'send_sms_time' => now_date(),
            'os' => get_os(),
            'ip' => get_ip(),
            'browser' => get_browser(),
            'status' => "new",
            'user_id' => null,
            'sms_code' => $sms_code,
            'phone' => $data['phone'],
        ];
        $this->Auth->save($auth_data);
        $auth_id = $this->Auth->id;

        $result['auth_id'] = $auth_id;
        return $result;
    }

    public function save_profile()
    {
        $errors = [];
        $data = $this->params['data'];

        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp = "/^[0-9]{11,12}$/iu";
        $name_regexp = "/^[a-zA_ZА-Яа-яёЁ-]{2,32}$/iu";

        if (empty($date['user_id']) OR !is_numeric($date['user_id'])) {
            $errors[] = "empty user_id";
            response_api($errors, "error");
            exit;
        }

        $user_id = $date['user_id'];
        $data['firstname'] = trim($data['firstname']);
        $data['lastname'] = trim($data['lastname']);
        $data['middlename'] = trim($data['middlename']);
        $data['email'] = trim($data['email']);
        $data['city_id'] = trim($data['city_id']);
        $data['date_of_birth'] = trim($data['date_of_birth']);
        $data['phone'] = str_replace("+", "", $data['phone']);

        if (!empty($data['email']) and !preg_match($mail_regexp, $data['email'])) {
            $errors[] = "incorrect field: email";
        }

        if (!empty($data['phone']) and !preg_match($phone_regexp, $data['phone'])) {
            $errors[] = "incorrect field: phone number";
        }

        if (!empty($data['lastname']) and !preg_match($name_regexp, $data['lastname'])) {
            $errors[] = "incorrect field: lastname";
        }

        if (!empty($data['firstname']) and !preg_match($name_regexp, $data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!empty($data['email']) AND filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if (!empty($data['email']) and $this->checkEmailExists($data['email'])) {
            $errors[] = "incorrect field: entered email " . $data['email'] . " is busy";
        }

        if (!empty($data['city_id']) and !is_numeric($data['city_id'])) {
            $errors[] = "incorrect field: entered city_id " . $data['city_id'] . " is not a number";
        }

        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }

        $this->User_Data->id =
            $this->User_Data->save($data);
        $this->User_Data->id = $user_id;

        $result = ['success' => true];
        response_api($result, "success");
        exit;
    }

    public function register()
    {
        $errors = [];
        $data = $this->params['data'];

        // валидация фио, почты, телефона
        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp = "/^[0-9]{11,12}$/iu";
        $name_regexp = "/^[a-zA_ZА-Яа-яёЁ-]{2,32}$/iu";
        $password_regexp = "/^[a-zA-Z0-9_]{8,32}$/iu";

        $data['firstname'] = trim($data['firstname']);
        $data['lastname'] = trim($data['lastname']);
        $data['company_name'] = trim($data['company_name']);
        $data['email'] = trim($data['email']);
        $data['password'] = trim($data['password']);
        $data['phone'] = trim($data['phone']);
        $data['phone'] = str_replace("+", "", $data['phone']);

        if (empty($data['company_name'])) {
            $errors[] = "incorrect field: empty company name";
        }

        if (!preg_match($mail_regexp, $data['email'])) {
            $errors[] = "incorrect field: email";
        }

        if (!preg_match($phone_regexp, $data['phone'])) {
            $errors[] = "incorrect field: phone number";
        }

        if (!preg_match($name_regexp, $data['lastname'])) {
            $errors[] = "incorrect field: lastname";
        }

        if (!preg_match($name_regexp, $data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!preg_match($password_regexp, $data['password'])) {
            $errors[] = "incorrect field: password";
        }

        if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if ($this->checkEmailExists($data['email'])) {
            $errors[] = "incorrect field: entered email " . $data['email'] . " is busy";
        }

        if ($this->checkPhoneExists($data['phone'])) {
            $errors[] = "incorrect field: entered phone number " . $data['phone'] . " is busy";
        }

        if (count($errors) > 0) {
            response_api($errors, "error");
            exit;
        }

        $data["mail_key"] = md5(time() . $data["email"]);
        //генерация пароля с солью
        $real_pwd = $data["password"];
        $data["password"] = get_hash(Configure::read('USER_AUTH_SALT'), $real_pwd);

        $company_data = [
            'company_name' => $data['company_name'],
            'status' => 'init',
            'phone_number' => $data['phone'],
            'stat__current_rating' => 0,
        ];

        $this->Company->save($company_data);
        $company_id = $this->Company->id;

        $company_agent_data = [
            'company_id' => $company_id,
            'firstname' => $data["firstname"],
            'lastname' => $data["lastname"],
            'role' => "owner",
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => $data["password"],
            'status' => 'new',
            'subscription' => 'true'

        ];
        $this->Company_Agent->save($company_agent_data);
        $company_agent_id = $this->Company_Agent->id;

        //ключ активации
        $mail_key_salt = Configure::read('MAIL_KEY_SALT');
        $mail_key = generate_mail_key($company_agent_id, $mail_key_salt);
        $this->Company_Agent->id = $company_agent_id;
        $this->Company_Agent->save(array('mail_key' => $mail_key));

        //запись номера телефона
        /*
        $phone_regexp = "/^\+[0-9]{11,12}$/";
        $user_phone = $user_data["User"]["phone"];
        if ($user_phone != null AND preg_match($phone_regexp, $user_phone)) {
            $data_to_save = array('user_id' => $user_id, 'phone' => $user_phone);
            $this->UserPhone->save($data_to_save);
        }
        */

        $message_data = "Вы только что зарегистрировались на проекте" . " " . site_url() . "<br>";
        $message_data .= "Ваши регистрационные данные<br> почтовый ящик: " . $data["email"];
        $message_data .= "Имя : " . $data["firstname"];
        $message_data .= "Пароль: " . $real_pwd . "<br>";
        $message_data .= "<a href='" . site_url() . "activate_account/user/" . $mail_key . "'>Ссылка активации аккаунта</a> <br><br>";
        $result = ['success' => true];
        response_api($result, "success");
        $this->EmailCom->sendEmailNow(
            $data["email"],
            "",
            "REGISTER_ON_PROJECT" . " " . site_url(),
            'user_register_mail_layout',
            'user_register_mail_template',
            $message_data,
            $attachment = "",
            $start_sending = ""
        );
        exit;
    }


















    //-- помойка --

    /**
     * @param $length
     * @param string $keyspace
     * @return string
     * @throws Exception
     */
    public function generateRandomPass($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$_')
    {
        $str = '';
        $max = mb_strlen($keyspace) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    public function is_auth()
    {
        if ($this->Session->check('token') or !empty($_COOKIE['token'])) {
            return true;
        } else {
            return false;
        }
    }

    public function auth_requires()
    {
        if (!$this->is_auth()) {
            $token = $_COOKIE['token'] ?? null;

            if (is_null($token)) {
                response_api(["error" => self::AUTH_REQUIRES_MESSAGE], "error");
                exit;
            }
            if (!$this->checkToken($token)) {
                response_api(["error" => self::AUTH_REQUIRES_MESSAGE], "error");
                exit;
            }
        }
    }

    /**
     * @param $token
     * @return bool
     */
    public function checkToken($token)
    {
        $this->Token = ClassRegistry::init("Token");
        $user = $this->Token->find("count",
            array('conditions' =>
                array(
                    'token' => $token
                )
            )
        );
        if ($user > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $token
     */
    public function logout($token)
    {
        $this->Token = ClassRegistry::init("Token");
        if ($this->checkToken($token)) {
            $token_id = $this->getTokenIdByToken();
            $this->Token->id = $token_id;
            $this->Token->delete();
            $this->Session->write('token', null);
            $this->Cookie->delete('token');
        }
    }

    /** Смена пароля покупателя
     * @param $user_id
     * @param $new_password
     * @return bool
     */
    public function changePassword($user_id, $new_password)
    {
        $modelName = "Orderer";
        $this->Orderer = ClassRegistry::init($modelName);

        $new = get_hash(Configure::read('USER_AUTH_SALT'), $new_password);
        $this->Orderer->id = $user_id;
        return $this->Orderer->save(['password' => $new]);
    }

    public function saveAvatarPhoto($file)
    {
        switch ($file['type']) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            default:
                return ['status' => 'error', 'msg' => 'Тип файла не поддерживается'];
        }

        $img = $this->Uploader->resizeImg($file['tmp_name'], 170, 170);
        if (!$img) {
            return ['status' => 'error', 'msg' => 'Проблема в создании нового изображения'];
        }

        $new_file_name = md5(rand(0, 9999)) . "." . $ext;

        // создать папку есть ее нет
        $user_dir = $this->getUserImagesDir($this->getUserId(), "absolute");
        // Configure::read('USER_AVATAR_UPLOAD_DIR'). DS . $this->getUserId();
        if (!is_dir($user_dir)) {
            mkdir($user_dir, 0777);
        }

        $save_file_path = $user_dir . DS . $new_file_name;
        $saving_img = $this->Uploader->saveImg($img, $save_file_path, $ext);
        if ($saving_img) {
            $modelName = "Orderer";
            $this->Orderer = ClassRegistry::init($modelName);

            $this->Orderer->id = $this->getUserId();
            $this->Orderer->save(['avatar' => $new_file_name]);

            $url = Router::url('/', true) . Configure::read('USER_AVATAR_UPLOAD_DIR_RELATIVE') . DS . $this->getUserId() . DS . $new_file_name;
            return ['status' => 'ok', 'msg' => 'Изображение сохранено', 'url' => $url];
        } else {
            return ['status' => 'error', 'msg' => 'Проблема в сохранении изображения'];
        }
    }

    /**
     * @param int $user_id
     * @param string $type
     * @return string
     */
    public function getUserImagesDir($user_id, $type = "relative")
    {
        if ($type == "relative") {
            return Router::url('/', true) . Configure::read('USER_AVATAR_UPLOAD_DIR_RELATIVE') . DS . $user_id;
        }
        return Configure::read('USER_AVATAR_UPLOAD_DIR') . DS . $user_id;
    }

    /**
     * Удаление фото-аватара пользователя
     */
    public function deleteAvatarPhoto()
    {
        $user_data = $this->user_data();

        $modelName = "Orderer";
        $this->Orderer = ClassRegistry::init($modelName);

        $this->Orderer->id = $user_data['Orderer']['id'];
        $this->Orderer->save(['avatar' => null]);

        if (!is_null($user_data['Orderer']['avatar'])) {
            $file = $this->getUserImagesDir($this->getUserId(), "absolute") . "/" . $user_data['Orderer']['avatar'];
            if (file_exists($file)) {
                unlink($file);
            }
            return [
                'status' => 'success',
                'msg' => 'Фото удалено',
            ];
        } else {
            return [
                'status' => 'error',
                'msg' => 'Фото не найдено',
            ];
        }
    }

    /**
     * @return mixed
     */
    private function getTokenIdByToken()
    {
        $this->Token = ClassRegistry::init("Token");
        $token = $this->getUserToken();
        $user_token = $this->Token->find("first",
            array('conditions' =>
                array(
                    'token' => $token
                )
            )
        );
        if (count($user_token) > 0) {
            return $user_token['Token']['id'];
        }
    }

    /**
     * @return mixed
     */
    private function getUserToken()
    {
        if ($this->Session->check('token')) {
            return $this->Session->read('token');

        } else if (!empty($_COOKIE['token'])) {
            $token = $_COOKIE['token'];
            if (is_array($token)) {
                $token = $token[0];
            }
            return $token;
        }
        return null;
    }

    public function getUserId()
    {
        $this->Token = ClassRegistry::init("Token");
        $token = $this->getUserToken();

        $user = $this->Token->find("first",
            array('conditions' =>
                array(
                    'token' => $token
                )
            )
        );
        if (count($user) > 0) {
            return $user['Token']['user_id'];
        }
        return null;
    }

    public function user_data()
    {
        if (!$this->is_auth()) {
            return false;
        } else {
            if (empty($this->user_data)) {
                $this->user_data = $this->getUserData($this->getUserId());
            }
            return $this->user_data;
        }
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getUserData($user_id)
    {
        $modelName = "Orderer";
        $this->Orderer = ClassRegistry::init($modelName);
        $user = $this->Orderer->find("first",
            array('conditions' =>
                array(
                    'id' => $user_id
                )
            )
        );
        return $user;
    }

    public function getUserDataByEmail($email)
    {
        $modelName = "Orderer";
        $this->Orderer = ClassRegistry::init($modelName);
        $user = $this->Orderer->find("first",
            array('conditions' =>
                array(
                    'email' => $email
                )
            )
        );

        if ($user) {
            return $user['Orderer'];
        } else {
            return false;
        }
    }

    /** Поиск подписок юзера
     * @param $user_id
     * @return array
     */
    public function getUserSubscriptions($user_id)
    {
        // Перечень всех подписок
        $subscriptions = [];
        $modelName = "Subscription";
        $this->Subscription = ClassRegistry::init($modelName);
        $data = $this->Subscription->find("all",
            array('conditions' =>
                array(
                    'user_id' => $user_id
                )
            )
        );
        if (count($data) > 0) {
            foreach ($data as $subscription) {
                $subscription_type = $subscription['Subscription']['type'];
                $subscription_value = $subscription['Subscription']['value'];
                if ($subscription_value == "true") {
                    $subscription_value_boolean = true;
                } else {
                    $subscription_value_boolean = false;
                }
                $subscriptions[$subscription_type] = $subscription_value_boolean;
            }
        }
        return $subscriptions;
    }

    /**
     * @param int $user_id
     * @param string $type
     * @return bool
     */
    public function saveUserSubscription($user_id, $type, $value = "true")
    {
        $modelName = "Subscription";
        $this->Subscription = ClassRegistry::init($modelName);
        $subscription = [
            'type' => $type,
            'user_id' => $user_id,
            'value' => $value,

        ];
        $checkSubscription = $this->Subscription->find("first",
            array('conditions' =>
                array(
                    'type' => $type,
                    'user_id' => $user_id
                )
            )
        );
        if (count($checkSubscription) == 0) {
            $this->Subscription->create();
        } else {
            $this->Subscription->id = $checkSubscription['Subscription']['id'];
        }
        $this->Subscription->save($subscription);
        if ($this->Subscription->id > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $user_id
     * @param null $type
     * @return mixed
     */
    public function getFavouriteList($user_id, $type = null)
    {
        $modelName = "Favourite";
        $this->Favourite = ClassRegistry::init($modelName);

        $favourites = $this->Favourite->find("all",
            array('conditions' =>
                array(
                    'user_id' => $user_id
                )
            )
        );
        $favourites_arr = [];
        foreach ($favourites as $favouritest_item) {
            $favourites_arr[] = $favouritest_item['Favourite'];
        }
        return $favourites_arr;
    }

    /**
     * @param $user_id
     * @param $element_type
     * @param $element_id
     * @return mixed
     */
    public function isElementInFavouriteList($user_id, $element_type, $element_id)
    {
        $modelName = "Favourite";
        $this->Favourite = ClassRegistry::init($modelName);
        return $this->Favourite->find("count",
            array('conditions' =>
                array(
                    'user_id' => $user_id,
                    'content_id' => $element_id,
                    'content' => $element_type,
                )
            )
        );
    }

    /**
     * @param $user_id
     * @param $element_type
     * @param $element_id
     * @return mixed
     */
    public function addElementToFavouriteList($user_id, $element_type, $element_id)
    {
        $modelName = "Favourite";
        $this->Favourite = ClassRegistry::init($modelName);

        $save_data = array(
            'user_id' => $user_id,
            'content' => $element_type,
            'content_id' => $element_id,
        );

        $this->Favourite->save($save_data);
        return $this->Favourite->id;
    }

    /**
     * @param $user_id
     * @param null $element_type
     */
    public function clearFavourite($user_id, $element_type = null)
    {
        $modelName = "Favourite";
        $this->Favourite = ClassRegistry::init($modelName);

        if ($element_type != null) {
            $element_type_array = ['content' => $element_type];
        } else {
            $element_type_array = [];
        }

        $element_list = $this->Favourite->find("all",
            array('conditions' =>
                array(
                    'user_id' => $user_id,
                    $element_type_array
                )
            )
        );

        if (count($element_list) > 0) {
            foreach ($element_list as $element_list_item) {
                $element_list_item = $element_list_item['Favourite'];
                $f_id = $element_list_item['id'];
                $this->Favourite->id = $f_id;
                $this->Favourite->delete();
            }
        }
    }

    /**
     * @param $user_id
     * @param null $element_type
     */
    public function clearProducts($user_id, $element_type = null)
    {
        return $this->clearFavourite($user_id, 'product');
    }

    /**
     * @param $user_id
     * @param $element_type
     * @param $element_id
     */
    public function deleteElementFromFavourite($user_id, $element_type, $element_id)
    {
        $modelName = "Favourite";
        $this->Favourite = ClassRegistry::init($modelName);

        if ($element_type != null) {
            $element_type_array = ['content' => $element_type];
        } else {
            $element_type_array = [];
        }

        $element_list = $this->Favourite->find("all",
            array('conditions' =>
                array(
                    'user_id' => $user_id,
                    'content_id' => $element_id,
                    $element_type_array
                )
            )
        );

        if (count($element_list) > 0) {
            foreach ($element_list as $element_list_item) {
                $element_list_item = $element_list_item['Favourite'];
                $f_id = $element_list_item['id'];
                $this->Favourite->id = $f_id;
                $this->Favourite->delete();
            }
        }
    }



    // ФУНКЦИОНАЛ КОРЗИНЫ

    /**
     * @param $user_id
     * @param null $type
     * @return mixed
     */
    public function getCartList($user_id)
    {
        return $this->getCartItems($user_id);
    }


    /**
     * @param $user_id
     * @param $product_id
     * @return mixed
     */
    public function isProductInCart($user_id, $shop_product_id)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);
        return $this->Cart->find("count",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                    'shop_product_id' => $shop_product_id,
                )
            )
        );
    }

    /**
     * @param $user_id
     * @param $product_id
     * @return mixed
     */
    public function addProductToCart($user_id, $shop_product_id, $amount, $product_id, $price)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);

        $save_data = array(
            'orderer_id' => $user_id,
            'shop_product_id' => $shop_product_id,
            'amount' => $amount,
            'product_id' => $product_id,
            'price' => $price,
        );

        $this->Cart->save($save_data);
        return $this->Cart->id;
    }

    /**
     * @param $user_id
     */
    public function clearCart($user_id)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);

        $element_list = $this->Cart->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                )
            )
        );

        if (count($element_list) > 0) {
            foreach ($element_list as $element_list_item) {
                $element_list_item = $element_list_item['Cart'];
                $f_id = $element_list_item['id'];
                $this->Cart->id = $f_id;
                $this->Cart->delete();
            }
        }
    }

    /**
     * @param $user_id
     * @param $offer_id
     */
    public function deleteProductFromCart($user_id, $offer_id)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);
        $element_list = $this->Cart->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                    'shop_product_id' => $offer_id,
                )
            )
        );

        if (count($element_list) > 0) {
            foreach ($element_list as $element_list_item) {
                $element_list_item = $element_list_item['Cart'];
                $cart_id = $element_list_item['id'];
                $this->Cart->id = $cart_id;
                $this->Cart->delete();
            }
        }
    }

    /**
     * @param $user_id
     * @return array
     */
    private function getCartItems($user_id)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);
        $cart_list = $this->Cart->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id
                )
            )
        );
        $cart_arr = [];
        foreach ($cart_list as $cart_item) {

            $cart_item = $cart_item['Cart'];
            $product_id = $cart_item['product_id'];
            $product_name = $this->ProductCom->getProductField("product_name", $product_id);
            $product_barcode = $this->ProductCom->getProductField("system_barcode", $product_id);

            $old_price = $this->ProductCom->getShopProductField("old_price", $cart_item['shop_product_id']);

            // поиск предложений по данному товару в других компаниях
            $offers_in_other_companies = $this->ProductCom->getProductOfferInAllCompanies($product_id);

            $alt_offers_arr = [];
            foreach ($offers_in_other_companies as $offers_company) {
                $offer_product = $offers_company['Shop_Product'];
                $offers_company = $offers_company['Company'];
                $cpm_id = $offers_company['id'];
                $cpm_name = $offers_company['company_name'];
                $alt_offers_arr[] = [
                    'offer_id' => intval($offer_product['id']),
                    'price' => intval($offer_product['base_price']),
                    "company_offer" => [
                        'id' => intval($cpm_id),
                        'name' => $cpm_name,
                    ],
                ];
            }

            $offer_company_id = $this->ProductCom->getShopProductField("company_id", $cart_item['shop_product_id']);
            $offer_status = $this->ProductCom->getShopProductField("status", $cart_item['shop_product_id']);
            $offer_company = $this->CompanyCom->getCompanyByid($offer_company_id);

            $offer_company_name = $offer_company[0]['Company']['company_name'];
            $product_in_cart = [
                'product' => [
                    'id' => intval($product_id),
                    'name' => $product_name,
                    'barcode' => $product_barcode
                ],
                'offer_id' => intval($cart_item['shop_product_id']),
                'amount' => intval($cart_item['amount']),
                'price' => intval($cart_item['price']),
                'old_price' => intval($old_price),
                'alternative_offers' => $alt_offers_arr,
                'company_offer' => ["id" => intval($offer_company_id), "name" => $offer_company_name],
                'is_available' => (bool)$offer_status,
            ];

            // изображения
            $product_images = $this->ProductCom->getProductImages($product_id);
            $image_types = [];
            $image_list = [];

            foreach ($product_images as $image_item) {

                $image_hash = substr($image_item['Product_Image']['file'], 0, 8);
                if (empty($old_hash)) {
                    $old_hash = $image_hash;
                    $image_types[] = $image_item['Product_Image']['type'];
                    continue;
                }
                if ($image_hash == $old_hash) {
                    if (!in_array($image_item['Product_Image']['type'], $image_types)) {
                        $image_types[] = $image_item['Product_Image']['type'];
                    }
                } else {
                    $image_list[] = array(
                        'alt' => '',
                        'hash' => $image_hash,
                        'types' => $image_types,
                        'image_id' => $image_item['Product_Image']['image_id'],
                    );
                    $old_hash = $image_hash;
                    $image_types = [];
                }
            }
            $image_list[] = array(
                'alt' => '',
                'hash' => $image_hash,
                'types' => $image_types,
                'image_id' => $image_item['Product_Image']['image_id'],
            );
            $product_in_cart['product']['images'] = $image_list;
            //delivery
            //transport and pickup
            $product_in_cart['delivery']['transport'] = $this->OrderCom->getCompanyTransportDateAndPrice(null);
            $product_in_cart['delivery']['pickup'] = $this->OrderCom->getCompanyPickupDateAndPrice(null);
            $cart_arr[] = $product_in_cart;
        }
        return $cart_arr;
    }

    public function getCartListFull($user_id)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);
        $cart_list = $this->Cart->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id
                ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Cart.shop_product_id = Shop_Product.id'
                        )
                    ),
                    array(
                        'table' => 'companies',
                        'alias' => 'Company',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Shop_Product.company_id = Company.id'
                        )
                    ),
                ),
                'fields' => array(
                    'Shop_Product.*',
                    'Company.*',
                    'Cart.*'
                ),
                'order' => array('Company.id ASC')
            )
        );
        $cart_arr = [];
        foreach ($cart_list as $cart_item) {
            $cart_arr[] = $cart_item;
        }
        return $cart_arr;
    }

    // Количество товарных позиций в корзине юзера
    public function getCartOffersCount($user_id)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);
        $cart_list = $this->Cart->find("count",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id
                ),
                'joins' => array(
                    array(
                        'table' => 'shop_products',
                        'alias' => 'Shop_Product',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Cart.shop_product_id = Shop_Product.id'
                        )
                    ),
                ),
            )
        );
        return $cart_list;
    }

    /**
     * @param $user_id
     * @param $product_id
     * @return int
     */
    public function findCartIdByProductAndUser($user_id, $product_id)
    {
        $cart_product = $this->Cart->find("first",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                    'product_id' => $product_id
                )
            )
        );
        if (count($cart_product) == 0) {
            return 0;
        }
        return $cart_product['Cart']['id'];
    }

    /**
     * @param $user_id
     * @param $product_id
     * @param $amount
     */
    public function UpdateProductCart($user_id, $product_id, $amount)
    {
        $modelName = "Cart";
        $this->Cart = ClassRegistry::init($modelName);
        $cart_id = $this->findCartIdByProductAndUser($user_id, $product_id);
        $new_data = [
            'amount' => $amount,
        ];
        $this->Cart->id = $cart_id;
        $this->Cart->save($new_data);
    }

    // СРАВНЕНИЕ ТОВАРОВ

    // Получить все товары добавленные в сравнение
    public function getCompareProductList($user_id)
    {
        $modelName = "Compared";
        $this->Compared = ClassRegistry::init($modelName);

        $compare_product = $this->Compared->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                ),
                'order' => array('created ASC')
            )
        );

        $arr = [];
        if ($compare_product) {
            foreach ($compare_product as $v) {
                $arr[] = $v['Compared']['product_id'];
            }
        }
        return $arr;
    }

    /**
     * @param $user_id
     * @param $category_id
     * @return array|bool
     */
    public function getCompareListByCategory($user_id, $category_id)
    {
        $modelName = "Compared";
        $this->Compared = ClassRegistry::init($modelName);
        $compared_product = $this->Compared->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                    'category_id' => $category_id
                ), 'order' => array('created ASC')
            )
        );
        if (count($compared_product) == 0) {
            return false;
        }
        $compared_product_arr = [];
        foreach ($compared_product as $compared_item) {
            $compared_category_product = $this->Compared->find("count",
                array('conditions' =>
                    array(
                        'orderer_id' => $user_id,
                        'category_id' => $compared_item['Compared']['category_id']
                    ),
                )
            );

            $compared_product_arr[] = array(
                'product_id' => $compared_item['Compared']['product_id'],
                'category_id' => $compared_item['Compared']['category_id'],
                'products_count' => $compared_category_product,
            );
        }
        return $compared_product_arr;
    }

    /**
     * @param $user_id
     * @return array|bool
     */
    public function getCompareCategories($user_id)
    {
        $modelName = "Compared";
        $this->Compared = ClassRegistry::init($modelName);
        $compared_product = $this->Compared->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                ),
                'joins' => array(
                    array(
                        'table' => 'product_categories',
                        'alias' => 'Product_Category',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Compared.category_id = Product_Category.id'
                        )
                    )
                ),
                'order' => array('Product_Category.name ASC'),
                'fields' => array('DISTINCT Compared.category_id, Product_Category.name')
            )
        );
        if (count($compared_product) == 0) {
            return null;
        }
        $compared_product_arr = [];
        foreach ($compared_product as $compared_item) {
            $compared_product_arr[] = array(
                'category_name' => $compared_item['Product_Category']['name'],
                'category_id' => $compared_item['Compared']['category_id']
            );
        }
        return $compared_product_arr;
    }

    /**
     * @param $user_id
     * @param $product_id
     * @return mixed
     */
    public function addProductToCompare($user_id, $product_id)
    {
        $modelName = "Compared";
        $this->Compare = ClassRegistry::init($modelName);

        $category_id = $this->ProductCom->getProductField("category_id", $product_id);

        $save_data = array(
            'orderer_id' => $user_id,
            'category_id' => $category_id,
            'product_id' => $product_id,
        );

        $this->Compare->save($save_data);
        return $this->Compare->id;
    }

    /**
     * @param $user_id
     * @param $product_id
     * @return mixed
     */
    public function deleteProductFromCompare($user_id, $product_id)
    {
        $modelName = "Compared";
        $this->Compared = ClassRegistry::init($modelName);

        $element_list = $this->Compared->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                    'product_id' => $product_id,
                )
            )
        );

        if (count($element_list) > 0) {
            foreach ($element_list as $element_list_item) {
                $element_list_item = $element_list_item['Compared'];
                $f_id = $element_list_item['id'];
                $this->Compared->id = $f_id;
                $this->Compared->delete();
            }
        }
    }

    /**
     * @param $user_id
     * @param $product_id
     * @return mixed
     */
    public function isProductInCompared($user_id, $product_id)
    {
        $modelName = "Compared";
        $this->Compare = ClassRegistry::init($modelName);
        return $this->Compare->find("count",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                    'product_id' => $product_id,
                )
            )
        );
    }

    /**
     * @param $user_id
     * @param $category_id
     */
    public function deleteCategoryFromCompare($user_id, $category_id)
    {
        $modelName = "Compared";
        $this->Compared = ClassRegistry::init($modelName);

        $element_list = $this->Compared->find("all",
            array('conditions' =>
                array(
                    'orderer_id' => $user_id,
                    'category_id' => $category_id,
                )
            )
        );

        if (count($element_list) > 0) {
            foreach ($element_list as $element_list_item) {
                $element_list_item = $element_list_item['Compared'];
                $f_id = $element_list_item['id'];
                $this->Compared->id = $f_id;
                $this->Compared->delete();
            }
        }
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getOrdererAddressList($user_id)
    {
        return $this->Address->getOrdererAddressList($user_id);
    }

    /**
     * @param $user_id
     * @param $address
     * @return mixed
     */
    public function addOrdererAddress($user_id, $address)
    {
        return $this->Address->addAddress($address, "orderer", $user_id);
    }

    /**
     * @param $address_id
     * @param $address
     * @return mixed
     */
    public function saveAddress($address_id, $address)
    {
        return $this->Address->saveAddress($address, "orderer", $address_id);
    }
}