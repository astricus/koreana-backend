<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Регион
 */
class RegionComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Region";
        $this->Region = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRegionNameById($id)
    {
        $this->setup();
        $region = $this->Region->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
        return $region['Region']['name'];
    }

    /**
     * @param $name
     * @return int
     */
    public function getRegionIdByName($name)
    {
        $this->setup();
        $region = $this->Region->find("first",
            array('conditions' =>
                array(
                    'name' => $name
                )
            )
        );
        return $region['Region']['id'] ?? 0;
    }

    /**
     * @param $country_id
     * @return |null
     */
    public function getRegionsByCountry($country_id){
        $this->setup();
        $regions = $this->Region->find("all",
            array('conditions' =>
                array(
                    'country_id' => $country_id
                ),
                'order' =>  array('name ASC')
            )
        );
        if(count($regions)>0){
            return $regions;
        }
        return null;
    }

    /**
     * @param $id
     * @return bool
     */
    public function isRegionExists($id){
        $this->setup();
        $regionCount = $this->Region->find("count",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
        if($regionCount>0){
            return true;
        }
        return false;
    }

}