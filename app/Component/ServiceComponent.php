<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Сервисы
 */
class ServiceComponent extends Component
{
    public $components         = [
        'City',
        'Session',
        'Error',
        'Admin',
        'Validator',
        'Platform',
        'Uploader',
        'Content',
        'Api',
    ];

    public $fields             = [
        "title"     => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => true,],
        "name"   => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => true,],
        "content"   => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => true,],
        "image" => ["type" => "text", "min_length" => "1", "max_length" => "", "required" => false,],
        "link" => ["type" => "text", "min_length" => "1", "max_length" => "", "required" => true,],
        "enabled"   => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => false,],
    ];

    public $errors             = [
        "invalid_service_status"    => "Передан некорректный статус сервиса",
        "service_with_id_not_found" => "Сервис с данным идентификатором не существует",
    ];

    public $statuses           = [
        'enabled'  => 1,
        'disabled' => 0,
    ];

    public $api_methods        = [
        "list" => "serviceListByAPi",
        "get"  => "getServiceById",
    ];

    public $default_show_count = 9999;

    public $default_sort_field = "created";

    const AUTH_REQUIRES_MESSAGE = "Error! This action requires user authorization. Please, authorize with your credential.";

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName         = "Service";
        $this->Service      = ClassRegistry::init($modelName);
        $modelName         = "Service_City";
        $this->Service_City = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function checkServiceExists($id)
    {
        $this->setup();

        return $this->Service->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function getServiceById($id)
    {
        $this->setup();
        $Service = $this->Service->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($Service) > 0) {
            $image = $Service['Service']['image'];
            if ($image == "") {
                $Service['Service']['image_url'] = null;
            } else {
                $Service['Service']['image_url'] = $this->Content->getContentImageUrl($image);
            }
            if ($this->Api->is_API()) {
                $Service['Service']['content'] = htmlspecialchars($Service['Service']['content']);
            }
            $Service['Service']['cities'] = $this->City->getCityIdByServiceid($Service['Service']['id']);

            return $Service['Service'];
        }
    }

    public function getServiceName($id)
    {
        $this->setup();
        $Service = $this->Service->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($Service) > 0) {
            return $Service['Service']['title'];
        }

        return null;

    }

    public function totalServiceCount()
    {
        $this->setup();
        $service_count = $this->Service->find(
            "count",
            [
                'conditions' =>
                    [],
            ]
        );

        return $service_count;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function createService($data)
    {
        $this->setup();
        $validate_result = $this->validateServiceFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        //сохранение платформы
        $data['platforms'] = $this->Platform->platformsToString($data['platform']);
        if(empty($data['platform'])){
            $data['platform'] = "main";
        }

        $this->Service->create();
        if ($this->Service->save($data)) {
            return $this->Service->id;
        }

        return false;
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return array|null
     */
    public function serviceList($show_count, $page, $sort, $sort_dir, $filter)
    {

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $view_active  = "";
        $not_finished = "";

        $this->setup();

        $platform_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "platform") {
                $platform_filter = ['Service.platforms LIKE' => "%$f_value%"];
            }
        }

        $city_id_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "city_id") {
                $city_id_filter = ['Service_City.city_id' => $f_value];
            }
        }

        $enabled = [];
        if ($this->Api->is_API()) {
            $enabled = ['Service.enabled' => 1];
        }

        $service_list = $this->Service->find(
            "all",
            [
                'conditions' =>
                    [
                        $enabled,
                        $view_active,
                        $platform_filter,
                        $city_id_filter,
                        $not_finished,
                    ],
                'joins'      => [
                    [
                        'table'      => 'service_cities',
                        'alias'      => 'Service_City',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'Service_City.service_id = Service.id',
                        ],
                    ],
                ],
                'limit'      => $show_count,
                'offset'     => $limit_page,
                'order'      => [$sort . " " . $sort_dir],
                'fields'     => ['DISTINCT Service.id, Service.*'],
            ]
        );

        if (count($service_list) > 0) {
            foreach ($service_list as &$Service) {
                $Service['platforms']   = $this->Platform->platformsToArray($Service['Service']['platforms']);

                $image = $Service['Service']['image'];
                if ($image == "") {
                    $Service['Service']['image_url'] = null;
                } else {
                    $Service['Service']['image_url'] = $this->Content->getContentImageUrl($image);
                }
                if ($this->Api->is_API()) {
                    $Service['Service']['content_text'] = htmlspecialchars($Service['Service']['content']);
                    $Service['Service']['cities']       = $this->City->getCityIdByServiceId($Service['Service']['id']);
                }
            }

            return $service_list;
        }

        return [];
    }

    /**
     * @param $params
     *
     * @return array|null
     */
    public function serviceListByAPi($params)
    {
        $show_count = $params['show_count'] ?? $this->default_show_count;
        $page       = $params['page'] ?? 1;
        $sort       = $params['sort'] ?? $this->default_sort_field;
        $sort_dir   = $params['sort_dir'] ?? 'desc';
        $filter     = $params['filter'] ?? null;

        // показ акций, у которых время показа наступило - специально для API
        $filter['show_active'] = true;

        return $this->serviceList($show_count, $page, $sort, $sort_dir, $filter);
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function updateService($id, $data)
    {
        if (!$this->checkServiceExists($id)) {
            $this->returnError("General", $this->errors['service_with_id_not_found']);

            return false;
        }
        $validate_result = $this->validateServiceFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        //сохранение платформы
        $data['platforms'] = $this->Platform->platformsToString($data['platform']);
        $this->Service->id  = $id;
        if ($this->Service->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @param $Service_id
     *
     * @return bool
     * @throws Exception
     */
    public function saveServiceImage($file, $width, $height, $Service_id)
    {
        $saving = $this->Content->saveContentImage($file, $width, $height);
        if ($saving['status']) {
            $modelName        = "Service";
            $this->Service     = ClassRegistry::init($modelName);
            $this->Service->id = $Service_id;
            $this->Service->save(['image' => $saving['name']]);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function validateServiceFields($data)
    {
        return $this->Validator->validateData($this->fields, $data);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteService($id)
    {
        if (!$this->checkServiceExists($id)) {
            $this->returnError("General", $this->errors['service_with_id_not_found']);

            return false;
        }
        $this->Service->id = $id;
        $this->Service->delete();

        return true;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    private function validateStatus($status)
    {
        if ($status !== 1 and $status !== 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockService($id)
    {
        $this->setup();
        if (!$this->checkServiceExists($id)) {
            $this->returnError("General", $this->errors['service_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockService($id)
    {
        $this->setup();
        if (!$this->checkServiceExists($id)) {
            $this->returnError("General", $this->errors['service_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    private function setStatus($id, $status)
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->errors['invalid_service_status']);

            return false;
        }
        $update_data      = ['enabled' => $status];
        $this->Service->id = $id;
        if ($this->Service->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }
}