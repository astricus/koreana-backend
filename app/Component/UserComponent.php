<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Пользователь
 */
class UserComponent extends Component
{

    public $components = [
        'Api',
        'Car',
        'City',
        'Clientapi',
        'Error',
        'Sms',
        'Session',
        'Uploader',
    ];

    public $api_methods = [
        "auth" => "authUserApi",
        "check_code" => "authCheckCodeApi",
        "save_profile" => "saveProfile",
        "profile" => "getUserDataApi",
        'add_device' => "addUserDevice",
    ];

    public $controller;

    public $default_sort_field = "User.created";

    const AUTH_REQUIRES_MESSAGE = "___";

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "User";
        $this->User = ClassRegistry::init($modelName);

        $modelName = "Client_Group";
        $this->Client_Group = ClassRegistry::init($modelName);

        $modelName = "Auth";
        $this->Auth = ClassRegistry::init($modelName);

        $modelName = "User_Code";
        $this->User_Code = ClassRegistry::init($modelName);
    }

    public function authUserApi($token, $params, $data)
    {
        $this->setup();
        $errors = [];
        // валидация фио, почты, телефона
        $phone_regexp = "/^[0-9]{11,12}$/iu";

        if (!isset($data['phone'])) {
            $errors[] = "empty field: phone number";
            $this->Api->response_api($errors, "error");
            exit;
        }

        $data['phone'] = trim($data['phone']);
        $data['phone'] = str_replace("+", "", $data['phone']);

        if (substr($data['phone'], 0, 1) == "7") {
            $data['phone'] = "8" . substr($data['phone'], 1);
        }

        if (!preg_match($phone_regexp, $data['phone'])) {
            $errors[] = "incorrect field: phone number";
        }

        if (!empty($data['phone']) and !preg_match($phone_regexp, $data['phone'])) {
            $errors[] = "incorrect field: phone number";
        }

        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }

        // есть ли привязка смс кода к номера
        $has_connection = null;
        if (is_test_app()) {
            $sms_code = "1234";
        } else if (is_prod_app()) {
            $has_connection = $this->getUserCodeByPhone($data['phone']);
            if ($has_connection == null) {
                $sms_code = mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9) . mt_rand(1, 9);
            } else {
                $sms_code = $has_connection;
            }
        }


        $def_platform = "android";
        if (key_exists("platform", $data)) {
            if ($data["platform"] != $def_platform && $data["platform"] != "ios") {
                $data["platform"] = $def_platform;
            }
        } else {
            $data["platform"] = $def_platform;
        }


        $auth_data = [
            'send_sms_time' => now_date(),
            'os' => $data["platform"] ?? null,
            'ip' => get_ip(),
            'browser' => "mobile",
            'status' => "new",
            'user_id' => 0,
            'sms_code' => $sms_code,
            'phone' => $data['phone'],
            "try_sms" => 0,
        ];
        $this->Auth->create();
        $this->Auth->save($auth_data);
        $auth_id = $this->Auth->id;

        $result['auth_id'] = $auth_id;
        $result['sms_code'] = $sms_code;
        $result['token'] = $token;

        $this->Api->response_api($result, "success");
        $sms_code_str = "Ваш проверочный код: $sms_code"; //M6RKRfoPu4A
        // СМС временно отключены для этапа тестирования
        if (is_prod_app() && $has_connection == null) {
            $send_phone = $data['phone'];
            $SmsApiKey = Configure::read('SMS_PILOT_API_KEY');
            $result = file_get_contents("https://smspilot.ru/api.php?send=$sms_code_str&to=$send_phone&apikey=$SmsApiKey&format=v");
            /*$this->Sms->sendSms($data['phone'], 'user_phone_confirm', $sms_code);*/
        }
        exit;
    }

    public function getApiToken()
    {
        return $this->_Collection->getController()->request->query("token") ?? null;
    }

    public function authCheckCodeApi($id, $params, $data)
    {
        $this->setup();
        $errors = [];

        if (!isset($data['code'])) {
            $errors[] = "empty field: code";
            $this->Api->response_api($errors, "error");
            exit;
        }
        if (!isset($data['auth_id'])) {
            $errors[] = "empty field: auth_id";
            $this->Api->response_api($errors, "error");
            exit;
        }

        if (!isset($data['auth_id'])) {
            $errors[] = "empty field: auth_id";
            $this->Api->response_api($errors, "error");
            exit;
        }

        $data['code'] = trim($data['code']);
        $real_code_check = $this->Auth->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $data['auth_id'],
                    ],
            ]
        );
        if (count($real_code_check) == 0) {
            $errors[] = "auth not found";
            $this->Api->response_api($errors, "error");
            exit;
        }
        $auth_id = $data['auth_id'];
        $real_code = $real_code_check['Auth']['sms_code'];
        $send_sms_time = $real_code_check['Auth']['send_sms_time'];
        $try_sms = $real_code_check['Auth']['try_sms'] ?? 0;

        $status = $real_code_check['Auth']['status'];
        if ($status == "failure") {
            $result['error'] = "error: authorization failed";
            $this->Api->response_api($result, "error");
            exit;
        }

        $left_time_seconds = 60 * 4 - (strtotime(now_date()) - strtotime($send_sms_time));
        $phone = $real_code_check['Auth']['phone'];
        $result['auth_id'] = $auth_id;
        // TODO - убрать заглушку на бою!
        if ($real_code == $data['code']) { //is_test_app()
            //init auth
            $find_user = $this->findUserByPhone($phone);
            if ($find_user == null) {
                $user_id = $this->createNewUser(['phone' => $phone]);
            } else {
                // при успешной авторизации запросить обновления данных пользователя из БД
                $user_id = $find_user;
            }

            $token = $this->getApiToken();
            $auth_data['status'] = "success";
            $auth_data['user_id'] = $user_id;
            $auth_data['token'] = $token;
            $this->Auth->id = $auth_id;
            $this->Auth->save($auth_data);

            $result["user_id"] = $user_id;
            $result["data"] = $this->getUserData($user_id);
            if (key_exists("city_id", $result["data"]) && $result["data"]['city_id'] !== 0) {
                $result["data"]['city_name'] = $this->City->getCityNameById($result["data"]['city_id']);
            } else {
                $result["data"]['city_name'] = "";
            }

            if (isset($data['private_agree'])) {
                if ($data['private_agree'] == 0 or $data['private_agree'] == 1) {
                    $this->User->id = $user_id;
                    $private_agree = [
                        'private_agree' => $data['private_agree'],
                        'agree_update' => date('Y-m-d H:i:s')
                    ];
                    $this->User->save($private_agree);
                }
            }
            $this->Api->response_api($result, "success");

        } else {
            // если кол-во попыток не исчерпано и период, с которого нужно отправлять смс повторно, прошел
            if ($left_time_seconds <= 0) {
                $this->Sms->sendSms($phone, 'user_phone_confirm', $real_code);
                $try_sms++;
                if ($try_sms >= 4) {
                    $auth_data['status'] = "failure";
                } else {
                    $auth_data['status'] = "new";
                    $auth_data['send_sms_time'] = now_date();
                }
                $auth_data['try_sms'] = $try_sms;
                $this->Auth->id = $auth_id;
                $this->Auth->save($auth_data);
            }
            $result['error'] = "your code is not correct";
            $result['left_time'] = $left_time_seconds;
            header("HTTP/1.0 402 Wrong Code");
            $this->Api->response_api($result, "error");
        }
        exit;
    }

    /**
     * @param $id
     * @param $params
     * @param $data
     */
    public function getUserDataApi($id, $params, $data)
    {
        $errors = [];
        if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
            $errors[] = "empty user_id";
            $this->Api->response_api($errors, "error");
            exit;
        }

        $this->Api->auth_requires($this->getApiToken());

        /*
        if (!$this->validateUserToken($data['user_id'], $this->getApiToken())) {
            $result["error"] = "invalid token or has exceed! please, authorize again";
            $result["need_auth"] = "true";
            header("HTTP/1.0 419 Authentication Timeout");
            $this->Api->response_api(["error" => $this::FALSE_TOKEN_MESSAGE], "error", null, null);
            $this->Api->response_api($result, "error");
            exit;
        }
        */

        $user = $this->getUserData($data['user_id']);
        if ($user == null) {
            $this->Api->response_api(["error" => "user_not_found"], "error");
        } else {
            $city_id = $user['city_id'] ?? null;
            if ($city_id != null) {
                $city_name = $this->City->getCityNameById($city_id);
                $user['city_name'] = $city_name;
            }
            $this->Api->response_api($user, "success");
        }

        exit;
    }

    public function getUserData($user_id)
    {
        $this->setup();
        $user = $this->User->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $user_id,
                    ],
            ]
        );
        if (isset($user['User'])) {
            if ($user['User']['date_of_birth'] == '') {
                $user['User']['date_of_birth'] = date("Y-m-d");
            }

            return $user['User'];
        }

        return null;
    }

    /**
     * @param $user_id
     * @param $token
     *
     * @return bool
     */
    public function validateUserToken($user_id, $token)
    {
        $this->setup();
        $result = $this->Auth->find(
            "count",
            [
                'conditions' =>
                    [
                        "status" => "success",
                        'token' => $token,
                        'user_id' => $user_id,
                    ],
            ]
        );
        if ($result > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $params
     * @param $data
     * @return array
     * @throws Exception
     */
    public function saveProfile($id, $params, $data)
    {
        $errors = [];
        if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
            $errors[] = "empty user_id";
            return ['status' => 'error', 'error' => $errors];
        }

        //проверка token
        $this->Api->auth_requires($this->getApiToken());
        /*
        if (!$this->validateUserToken($data['user_id'], $this->getApiToken())) {
            $result["error"] = "invalid token or has exceed! please, authorize again";
            $result["need_auth"] = "true";
            $this->Api->response_api($result, "error");
            exit;
        }
        */

        //ПРОВЕРКИ ПОЛЕЙ
        $mail_regexp = "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,10}$/i";
        $phone_regexp = "/^[0-9]{11,12}$/iu";
        $name_regexp = "/^[a-zA_ZА-Яа-яёЁ-]{2,32}$/iu";

        $user_id = $data['user_id'];
        if (isset($data['firstname'])) {
            $data['firstname'] = trim($data['firstname']);
        }

        if (isset($data['lastname'])) {
            $data['lastname'] = trim($data['lastname']);
        }

        if (isset($data['middlename'])) {
            $data['middlename'] = trim($data['middlename']);
        }

        if (isset($data['email'])) {
            $data['email'] = trim($data['email']);
        }

        if (isset($data['city_id'])) {
            $data['city_id'] = trim($data['city_id']);
        }
        if (isset($data['date_of_birth'])) {
            $data['date_of_birth'] = trim($data['date_of_birth']);
        }

        if (isset($data['phone'])) {
            $data['phone'] = str_replace("+", "", $data['phone']);
        }

        if (!empty($data['email']) and !preg_match($mail_regexp, $data['email'])) {
            $errors[] = "incorrect field: email";
        }

        if (!empty($data['phone']) and !preg_match($phone_regexp, $data['phone'])) {
            $errors[] = "incorrect field: phone number";
        }

        if (!empty($data['lastname']) and !preg_match($name_regexp, $data['lastname'])) {
            $errors[] = "incorrect field: lastname";
        }

        if (!empty($data['firstname']) and !preg_match($name_regexp, $data['firstname'])) {
            $errors[] = "incorrect field: firstname";
        }

        if (!empty($data['email']) and filter_var($data['email'], FILTER_VALIDATE_EMAIL) == false) {
            $errors[] = "incorrect field: email";
        }

        if (!empty($data['email']) and $this->checkEmailExists($data['email'])) {
            $errors[] = "incorrect field: entered email " . $data['email'] . " is busy";
        }

        if (!empty($data['city_id']) and !is_numeric($data['city_id'])) {
            $errors[] = "incorrect field: entered city_id " . $data['city_id'] . " is not a number";
        }

        if (count($errors) > 0) {
            return ['status' => 'error', 'error' => $errors];
        }

        $this->setup();
        $this->User->id = $user_id;
        $this->User->save($data);

        // обновление данных пользователя в клиентской базе
        $data['origin_id'] =  $this->User->id;
        $this->Clientapi->apiUpdateClientData($user_id, $data);
        return ['status' => 'success', 'error' => null];
    }

    /**
     * @param $phone
     *
     * @return |null
     */
    public function findUserByPhone($phone)
    {
        $this->setup();
        $result = $this->User->find(
            "first",
            [
                'conditions' =>
                    [
                        'phone' => $phone,
                    ],
            ]
        );
        if (count($result) > 0) {
            return $result['User']['id'];
        }

        return null;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function createNewUser($data)
    {
        $this->setup();
        $user_data = [
            'lastname' => $data['lastname'] ?? "",
            'firstname' => $data['firstname'] ?? "",
            'middlename' => $data['middlename'] ?? "",
            'city_id' => $data['city_id'] ?? 0,
            'email' => $data['email'] ?? "",
            'login' => $data['login'] ?? "",
            'status' => "active",
            'phone' => $data['phone'],
            'date_of_birth' => $data['date_of_birth'] ?? "",
        ];
        $this->User->create();
        $this->User->save($user_data);

        // сохранение пользователя в клиентской базе данных
        $user_data['origin_id'] =  $this->User->id;
        $this->Clientapi->apiCreateClient($user_data);

        return $this->User->id;
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    public function checkEmailExists($email)
    {
        $this->setup();

        return $this->User->find(
            "count",
            [
                'conditions' =>
                    [
                        'email' => $email,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function checkUserExists($id)
    {
        $this->setup();

        return $this->User->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getUserById($id)
    {
        $this->setup();
        $user = $this->User->find(
            "first",
            [
                'conditions' =>
                    [
                        'User.id' => $id,
                    ],
                'joins' => [
                    [
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User.city_id = City.id',
                        ],
                    ],
                ],
                'fields' => ['User.*, City.*'],
            ]
        );
        if (count($user) > 0) {
            return $user;
        }
    }

    public function userTotalCount($filter, $search)
    {
        $this->setup();
        $city_id_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "city_id") {
                $city_id_filter = ['User.city_id' => $f_value];
            }
        }

        $created_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "created") {
                $created_filter = ['User.created >' => $f_value];
            }
        }

        $record_search = [];
        if (!empty($search)) {
            $record_search = [
                'OR' => array(
                    array('User.firstname LIKE' => "%" . $search . "%"),
                    array('User.lastname LIKE ' => "%" . $search . "%"),
                    array('User.phone LIKE ' => "%" . $search . "%"),
                    array('Car_Model.name LIKE ' => "%" . $search . "%"),
                    array('Car_Mark.name LIKE ' => "%" . $search . "%"),
                    array('User_Car.car_number LIKE ' => "%" . $search . "%"),
                )
            ];
        }

        return $this->User->find(
            "count",
            [
                'conditions' =>
                    [
                        $created_filter,
                        $city_id_filter,
                        $record_search
                    ],
                'joins' => [
                    [
                        'table' => 'user_cars',
                        'alias' => 'User_Car',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User_Car.user_id = User.id',
                        ],
                    ],
                    [
                        'table' => 'car_marks',
                        'alias' => 'Car_Mark',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User_Car.mark_id = Car_Mark.id',
                        ],
                    ],
                    [
                        'table' => 'car_models',
                        'alias' => 'Car_Model',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User_Car.model_id = Car_Model.id',
                        ],
                    ],
                ],
                'group' => array('User.id'),
                'fields' => [
                    //'DISTINCT User.*',
                    'User.*',
                    'User_Car.*',
                    'Car_Mark.name',
                    'Car_Model.name',
                ],

            ]
        );

    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return |null
     */
    public function userList($show_count, $page, $sort, $sort_dir, $filter, $search)
    {

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        /*
                $platform_filter = [];
                foreach ($filter as $f_key => $f_value) {
                    if ($f_key == "platform") {
                        $platform_filter = array('News.platforms LIKE' => "%$f_value%");
                    }
                }
        */
        $city_id_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "city_id") {
                $city_id_filter = ['User.city_id' => $f_value];
            }
        }

        $created_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "created") {
                $created_filter = ['User.created >' => $f_value];
            }
        }

        $record_search = [];
        if (!empty($search)) {
            $record_search = [
                'OR' => array(
                    array('User.firstname LIKE' => "%" . $search . "%"),
                    array('User.lastname LIKE ' => "%" . $search . "%"),
                    array('User.phone LIKE ' => "%" . $search . "%"),
                    array('Car_Model.name LIKE ' => "%" . $search . "%"),
                    array('Car_Mark.name LIKE ' => "%" . $search . "%"),
                    array('User_Car.car_number LIKE ' => "%" . $search . "%"),
                )
            ];
        }

        $this->setup();
        $user_list = $this->User->find(
            "all",
            [
                'conditions' =>
                    [
                        $created_filter,
                        $city_id_filter,
                        $record_search
                    ],
                'joins' => [
                    [
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User.city_id = City.id',
                        ],
                    ],
                    [
                        'table' => 'user_cars',
                        'alias' => 'User_Car',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User_Car.user_id = User.id',
                        ],
                    ],
                    [
                        'table' => 'car_marks',
                        'alias' => 'Car_Mark',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User_Car.mark_id = Car_Mark.id',
                        ],
                    ],
                    [
                        'table' => 'car_models',
                        'alias' => 'Car_Model',
                        'type' => 'LEFT',
                        'conditions' => [
                            'User_Car.model_id = Car_Model.id',
                        ],
                    ],
                ],
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => [$sort . " " . $sort_dir],
                'group' => array('User.id'),
                'fields' => [
                    'User.*',
                    'User_Car.*',
                    'City.*',
                    'Car_Mark.name',
                    'Car_Model.name',
                ],
            ]
        );
        if (count($user_list) > 0) {
            foreach ($user_list as &$user) {
                $id = $user['User']['id'];
                $last_auth = $this->Auth->find(
                    "first",
                    [
                        'conditions' =>
                            [
                                'user_id' => $id,
                            ],
                        "order" => ["id DESC"],
                    ]
                );
                if (count($last_auth) > 0) {
                    $user['User']['last_auth'] = $last_auth['Auth']['created'];
                } else {
                    $user['User']['last_auth'] = "";
                }
                $user['cars'] = $this->Car->getUserCars($id);
            }

            return $user_list;
        }

        return [];
    }

    public function user_auths($id)
    {
        $this->setup();

        return $this->Auth->find(
            "all",
            [
                'conditions' =>
                    [
                        'user_id' => $id,
                    ],
                "order" => ["id DESC"],
            ]
        );
    }

    public function userSimpleList()
    {
        $this->setup();
        $user_list = $this->User->find(
            "all",
            [
                'order' => ["firstname asc"],
            ]
        );
        if (count($user_list) > 0) {
            return $user_list;
        }

        return [];
    }

    /**
     * @param $phone
     * @param $code
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function createUserCode($phone, $code)
    {
        $this->setup();
        $user_data = [
            'phone' => $phone,
            'code' => $code,
        ];
        $this->User_Code->create();
        $this->User_Code->save($user_data);

        return $this->User_Code->id;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteUserCode($id)
    {
        $this->setup();
        $this->User_Code->id = $id;
        $this->User_Code->delete();
        return true;
    }

    /**
     * @param int $id
     * @return array
     */
    public function getUserCode($id)
    {
        $this->setup();
        $user_code = $this->User_Code->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($user_code) > 0) {
            return $user_code;
        }

        return [];
    }

    public function getUserCodes()
    {
        $this->setup();
        $user_codes = $this->User_Code->find(
            "all",
            [
                'conditions' =>
                    [
                        //'id' => $id,
                    ],
                'order' => ['created DESC']
            ]
        );
        return $user_codes;
    }

    public function getUserPhones()
    {
        $this->setup();
        $user_phones = [];
        $users = $this->User->find(
            "all",
            [
                'conditions' =>
                    [
                        //'id' => $id,
                    ],
                'order' => ['created DESC']
            ]
        );
        foreach ($users as $user) {
            $phone = $user['User']['phone'];
            if (!in_array($phone, $user_phones)) {
                $user_phones[] = $phone;
            }
        }
        return $user_phones;
    }

    /**
     * @param $phone
     * @return mixed|null
     */
    public function getUserCodeByPhone($phone)
    {
        $this->setup();
        $user_code = $this->User_Code->find(
            "first",
            [
                'conditions' =>
                    [
                        'phone' => $phone,
                    ],
            ]
        );
        if (count($user_code) > 0) {
            return $user_code['User_Code']['code'];
        }

        return null;
    }

    //-- помойка --

    /**
     * @param        $length
     * @param string $keyspace
     *
     * @return string
     * @throws Exception
     */
    public function generateRandomPass(
        $length,
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$_'
    )
    {
        $str = '';
        $max = mb_strlen($keyspace) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }

        return $str;
    }

    public function is_auth()
    {
        if ($this->Session->check('token') or !empty($_COOKIE['token'])) {
            return true;
        } else {
            return false;
        }
    }

    public function auth_requires()
    {
        if (!$this->is_auth()) {
            $token = $_COOKIE['token'] ?? null;

            if (is_null($token)) {
                $this->Api->response_api(["error" => self::AUTH_REQUIRES_MESSAGE], "error");
                exit;
            }
            if (!$this->checkToken($token)) {
                $this->Api->response_api(["error" => self::AUTH_REQUIRES_MESSAGE], "error");
                exit;
            }
        }
    }

    /**
     * @param $token
     *
     * @return bool
     */
    public function checkToken($token)
    {
        $this->Token = ClassRegistry::init("Token");
        $user = $this->Token->find(
            "count",
            [
                'conditions' =>
                    [
                        'token' => $token,
                    ],
            ]
        );
        if ($user > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param $token
     */
    public function logout($token)
    {
        $this->Token = ClassRegistry::init("Token");
        if ($this->checkToken($token)) {
            $token_id = $this->getTokenIdByToken();
            $this->Token->id = $token_id;
            $this->Token->delete();
            $this->Session->write('token', null);
            $this->Cookie->delete('token');
        }
    }

    /** Смена пароля покупателя
     *
     * @param $user_id
     * @param $new_password
     *
     * @return bool
     */
    public function changePassword($user_id, $new_password)
    {
        $modelName = "Orderer";
        $this->Orderer = ClassRegistry::init($modelName);

        $new = get_hash(Configure::read('USER_AUTH_SALT'), $new_password);
        $this->Orderer->id = $user_id;

        return $this->Orderer->save(['password' => $new]);
    }

    public function saveAvatarPhoto($file)
    {
        switch ($file['type']) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            default:
                return ['status' => 'error', 'msg' => 'Тип файла не поддерживается'];
        }

        $img = $this->Uploader->resizeImg($file['tmp_name'], 170, 170);
        if (!$img) {
            return ['status' => 'error', 'msg' => 'Проблема в создании нового изображения'];
        }

        $new_file_name = md5(rand(0, 9999)) . "." . $ext;

        // создать папку есть ее нет
        $user_dir = $this->getUserImagesDir($this->getUserId(), "absolute");
        // Configure::read('USER_AVATAR_UPLOAD_DIR'). DS . $this->getUserId();
        if (!is_dir($user_dir)) {
            mkdir($user_dir, 0777);
        }

        $save_file_path = $user_dir . DS . $new_file_name;
        $saving_img = $this->Uploader->saveImg($img, $save_file_path, $ext);
        if ($saving_img) {
            $modelName = "Orderer";
            $this->Orderer = ClassRegistry::init($modelName);

            $this->Orderer->id = $this->getUserId();
            $this->Orderer->save(['avatar' => $new_file_name]);

            $url = Router::url('/', true) . Configure::read('USER_AVATAR_UPLOAD_DIR_RELATIVE') . DS . $this->getUserId() . DS . $new_file_name;

            return ['status' => 'ok', 'msg' => 'Изображение сохранено', 'url' => $url];
        } else {
            return ['status' => 'error', 'msg' => 'Проблема в сохранении изображения'];
        }
    }

    /**
     * @return mixed
     */
    private function getTokenIdByToken()
    {
        $this->Token = ClassRegistry::init("Token");
        $token = $this->getUserToken();
        $user_token = $this->Token->find(
            "first",
            [
                'conditions' =>
                    [
                        'token' => $token,
                    ],
            ]
        );
        if (count($user_token) > 0) {
            return $user_token['Token']['id'];
        }
    }

    /**
     * @return mixed
     */
    private function getUserToken()
    {
        if ($this->Session->check('token')) {
            return $this->Session->read('token');

        } else {
            if (!empty($_COOKIE['token'])) {
                $token = $_COOKIE['token'];
                if (is_array($token)) {
                    $token = $token[0];
                }

                return $token;
            }
        }

        return null;
    }

    public function getUserId()
    {
        $this->Token = ClassRegistry::init("Token");
        $token = $this->getUserToken();

        $user = $this->Token->find(
            "first",
            [
                'conditions' =>
                    [
                        'token' => $token,
                    ],
            ]
        );
        if (count($user) > 0) {
            return $user['Token']['user_id'];
        }

        return null;
    }

    public function user_data()
    {
        if (!$this->is_auth()) {
            return false;
        } else {
            if (empty($this->user_data)) {
                $this->user_data = $this->getUserData($this->getUserId());
            }

            return $this->user_data;
        }
    }

    /**
     * @param $user_id
     *
     * @return mixed
     */

    public function addUserDevice()
    {
        $fields = [
            'app_id' => Configure::read('ONESIGNAL_APP_ID'),// "ce8bec48-6590-4a74-94ca-5450c3914fe5",
            'identifier' => "ce777617da7f548fe7a9ab6febb56cf39fba6d382000c0395666288d961ee566",
            'language' => "en",
            'timezone' => "-28800",
            'game_version' => "1.0",
            'device_os' => "9.1.3",
            'device_type' => "0",
            'device_model' => "iPhone 8,2",
            'tags' => ["foo" => "bar"],
        ];

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);

        $return["allresponses"] = $response;
        $return = json_encode($return);

        print("\n\nJSON received:\n");
        print($return);
        print("\n");
        $result = ['success' => true, 'id' => $return['id']];
        $this->Api->response_api($result, "success");
        exit;

    }

    /**
     * @param $name
     * @param $filters
     *
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function createClientGroup($name, $filters)
    {
        $this->setup();
        $group_data = [
            'name' => $name,
            'registered_after' => $filters['registered'] ?? "",
            'city_id' => $filters['city_id'] ?? 0,
            'mark_id' => $filters['mark_id'] ?? 0,
            'model_id' => $filters['model_id'] ?? 0,
            'last_visit' => $filters['last_visit'] ?? "",
            'has_visit' => $filters['has_visit'] ?? "",
            'long_sleep' => $filters['long_sleep'],
        ];
        $this->Client_Group->create();
        $this->Client_Group->save($group_data);

        return $this->Client_Group->id;
    }

    /**
     * @param $id
     * @param $name
     * @param $filters
     *
     * @return bool
     * @throws Exception
     */
    public function editClientGroup($id, $name, $filters)
    {
        $this->setup();
        $group_data = [
            'name' => $name,
            'registered_after' => $filters['registered'] ?? "",
            'city_id' => $filters['city_id'] ?? 0,
            'mark_id' => $filters['mark_id'] ?? 0,
            'model_id' => $filters['model_id'] ?? 0,
            'last_visit' => $filters['last_visit'] ?? "",
            'has_visit' => $filters['has_visit'] ?? "",
            'long_sleep' => $filters['long_sleep'] ?? "",
        ];
        $this->Client_Group->id = $id;
        $this->Client_Group->save($group_data);

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteClientGroup($id)
    {
        $this->setup();
        $this->Client_Group->id = $id;
        $this->Client_Group->delete();

        return true;

    }

    /**
     * @param $id
     *
     * @return array|int|null
     */
    public function getClientGroupById($id)
    {
        $this->setup();
        $client_group = $this->Client_Group->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($client_group) > 0) {
            return $client_group;
        }

        return null;
    }

    public function getClientGroupList()
    {

    }

    public function today_new_users()
    {
        $this->setup();
        return $this->User->find(
            "count",
            [
                'conditions' =>
                    [
                        'DATE(NOW()) - DATE(created)' => 0,
                    ],
            ]
        );

    }

    public function total_users()
    {
        $this->setup();
        return $this->User->find(
            "count",
            [
                'conditions' =>
                    [],
            ]
        );
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return array|int|null
     */
    public function new_users_by_date($date_from, $date_to)
    {
        $this->setup();
        return $this->User->find(
            "count",
            [
                'conditions' =>
                    [
                        "created >" => $date_from . " 00:00:00",
                        "created <" => $date_to . " 00:00:00",
                    ],
            ]
        );

    }

}