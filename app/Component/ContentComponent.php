<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Контент (Акции, Новости)
 */
class ContentComponent extends Component {

    public $components = array(
        'Session',
        'Error',
        'Uploader'
    );

    public $image_params = [
        'max_size_mb' => 0.5,
        'default_image_height' => 800,
        'default_image_width' => 800,
    ];

    public $controller;

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "News";
        $this->News = ClassRegistry::init($modelName);
    }

    /**
     * @param $file
     * @return string
     */
    public function getContentImageUrl($file)
    {
        return $_SERVER['REQUEST_SCHEME'] . "://" . $this->_Collection->getController()->request->host() . "/" . Configure::read('CONTENT_UPLOAD_DIR_RELATIVE') . "/" . $file;
    }

    public function getLocationImageUrl($file)
    {
        return $_SERVER['REQUEST_SCHEME'] . "://" . $this->_Collection->getController()->request->host() . "/" . Configure::read('LOCATION_UPLOAD_DIR_RELATIVE') . "/" . $file;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @return string[]
     * @throws Exception
     */
    public function saveContentImage($file, $width, $height)
    {
        switch ($file['type']) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            default:
                return ['status' => 'error', 'msg' => 'Тип файла не поддерживается'];
        }

        $img = $this->Uploader->resizeImg($file['tmp_name'], $width, $height);
        if (!$img) {
            return ['status' => 'error', 'msg' => 'Проблема в создании нового изображения'];
        }

        $new_file_name = md5(uniqid()) . "." . $ext;

        // создать папку есть ее нет
        $upl_dir = Configure::read('CONTENT_UPLOAD_DIR');
        if (!is_dir($upl_dir)) {
            mkdir($upl_dir, 0777);
        }

        $save_file_path = $upl_dir . DS . $new_file_name;
        $saving_img = $this->Uploader->saveImg($img, $save_file_path, $ext);
        if ($saving_img) {
            $url = Router::url('/', true) . Configure::read('CONTENT_UPLOAD_DIR_RELATIVE') . "/" . $new_file_name;
            return ['status' => true, 'msg' => 'Изображение сохранено', 'url' => $url, 'name' => $new_file_name];
        } else {
            return ['status' => false, 'msg' => 'Проблема в сохранении изображения'];
        }
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @return array|string[]
     */
    public function saveLocationImage($file, $width, $height)
    {
        switch ($file['type']) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            default:
                return ['status' => 'error', 'msg' => 'Тип файла не поддерживается'];
        }

        $img = $this->Uploader->resizeImg($file['tmp_name'], $width, $height);
        if (!$img) {
            return ['status' => 'error', 'msg' => 'Проблема в создании нового изображения'];
        }

        $new_file_name = md5(uniqid()) . "." . $ext;

        // создать папку есть ее нет
        $upl_dir = Configure::read('LOCATION_UPLOAD_DIR');
        if (!is_dir($upl_dir)) {
            mkdir($upl_dir, 0777);
        }

        $save_file_path = $upl_dir . DS . $new_file_name;
        $saving_img = $this->Uploader->saveImg($img, $save_file_path, $ext);
        if ($saving_img) {
            $url = Router::url('/', true) . Configure::read('LOCATION_UPLOAD_DIR_RELATIVE') . $new_file_name;
            return ['status' => true, 'msg' => 'Изображение сохранено', 'url' => $url, 'name' => $new_file_name];
        } else {
            return ['status' => false, 'msg' => 'Проблема в сохранении изображения'];
        }
    }

    // TODO validate content image
    public function validateImage($file)
    {

    }

    /**
     * @param $link
     * @return string
     */
    public function saveLocationImageByLink($link)
    {
        $path_to_images = Configure::read('LOCATION_UPLOAD_DIR_RELATIVE');
        $path_to_images_dir = Configure::read('LOCATION_UPLOAD_DIR');
        if (!is_dir($path_to_images_dir)) {
            mkdir($path_to_images_dir, 0777);
        }
        $loaded_image = file_get_contents($link);
        if (!$loaded_image) {
            die("не удалось загрузить изображение по ссылке " . $link);
        }
        if (substr_count(basename($link), "?") > 0) {
            $new_file = explode("?", basename($link))[0];
        }
        $new_full_file = $path_to_images_dir . DS . $new_file;
        file_put_contents($new_full_file, $loaded_image);

        $img = $this->Uploader->resizeImg($new_full_file, $this->image_params['default_image_height'], $this->image_params['default_image_width']);
        if (!$img) {
            die('Проблема в создании нового изображения');
        }
        return basename($new_file);
    }


    public function getPushImageUrl($file)
    {
        if(empty($file)){
            return '';
        }
        return $_SERVER['REQUEST_SCHEME'] . "://" . $this->_Collection->getController()->request->host() . "/" . Configure::read('PUSH_UPLOAD_DIR_RELATIVE') . "/" . $file;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @return string[]
     * @throws Exception
     */
    public function savePushImage($file, $width, $height): array
    {
        switch ($file['type']) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            default:
                return ['status' => 'error', 'msg' => 'Тип файла не поддерживается'];
        }

        $img = $this->Uploader->resizeImg($file['tmp_name'], $width, $height);
        if (!$img) {
            return ['status' => 'error', 'msg' => 'Проблема в создании нового изображения'];
        }

        $new_file_name = md5(uniqid()) . "." . $ext;

        // создать папку есть ее нет
        $upl_dir = Configure::read('PUSH_UPLOAD_DIR');
        if (!is_dir($upl_dir)) {
            mkdir($upl_dir, 0777);
        }

        $save_file_path = $upl_dir . DS . $new_file_name;
        $saving_img = $this->Uploader->saveImg($img, $save_file_path, $ext);
        if ($saving_img) {
            $url = Router::url('/', true) . Configure::read('PUSH_UPLOAD_DIR_RELATIVE') . "/" . $new_file_name;
            return ['status' => 'ok', 'msg' => 'Изображение сохранено', 'url' => $url, 'name' => $new_file_name];
        } else {
            return ['status' => false, 'msg' => 'Проблема в сохранении изображения'];
        }
    }
}