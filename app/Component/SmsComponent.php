<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');
App::uses('ApiSmsPilot', 'Lib'); // API smspilot.ru
App::uses('ApiSms4b', 'Lib'); // API sms4b.ru

class SmsComponent extends Component
{
    private $template = [
        'user_phone_confirm' => [
            'msg' => 'Ваш код подтверждения - %code_confirm%',
            'search' => ['%code_confirm%'],
            'replace' => ['code_confirm'],
        ],
    ];

    private $providers = [
        1 => 'ApiSmsPilot',
    ];

    public $SMS;

    public function __construct(ComponentCollection $collection, $settings = array())
    {
        parent::__construct($collection, $settings);
        $this->SMS = $this->connectApiSmsProvider(1);
    }

    public function sendSms($phone_number, $template, $data = '')
    {
        $res = $this->SMS->sendSms($phone_number, $data);
        return $res;
    }

    public function sendViaSms4b($phone_number, $data){
        $ApiSms4b = new ApiSms4b();
        $res = $ApiSms4b->SendSMS($data, $phone_number, $sender = '');
        return $res;
    }




    public function checkSms($number)
    {
        $this->SMS->checkSms($number);
    }

    public function getBalance()
    {
        return $this->SMS->getBalance();
    }

    public function connectApiSmsProvider($provider_id)
    {
        $api_class = $this->providers[$provider_id];
        if (class_exists($api_class)) {
            return new $this->providers[$provider_id];
        }
        return false;
    }

    private function getMsg($template, $data)
    {
        $template_data = $this->getTemplate($template);
        if ($template_data) {
            $str = $template_data['msg'];
            $i = 0;
            foreach ($template_data['replace'] as $v) {
                if (!empty($data[$v])) {
                    $str = str_replace($template_data['search'][$i], $data[$v], $str);
                } else {
                    return false;
                }
                $i++;
            }
            return $str;
        } else {
            return false;
        }
    }

    private function getTemplate($template)
    {
        if (!empty($this->template[$template])) {
            return $this->template[$template];
        } else {
            return false;
        }
    }
}