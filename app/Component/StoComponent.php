<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Автосервис
 */
class StoComponent extends Component
{
    public $components         = [
        'Action',
        'Api',
        'City',
        'Error',
        'EmailCom',
        'Session',
        'Validator',
        'Car',
        "Location",
        "User",
        "Log",
    ];

    public $STO_NOTIFY_EMAILS  = [
        "homoastricus2011@gmail.com",
        "support@koreanagroup.ru",
        "r.shirokovsky@koreanagroup.ru",
        "sto-zayavki@koreanagroup.ru"
    ];

    public $fields             = [
        "auto_service"        => [
            "name"    => ["type" => "text", "min_length" => "0", "max_length" => "255", "required" => false,],
            "comment" => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => true,],
            "enabled" => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => true,],
        ],
        "auto_service_record" => [
            //"service_id" => ["type" => "id", "min_length" => "0", "max_length" => "255", "required" => true,],
            "comment"          => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => false,],
            "service_datetime" => ["type" => "datetime", "min_length" => "", "max_length" => "", "required" => true,],
            "user_id"          => ["type" => "id", "min_length" => "0", "max_length" => "255", "required" => true,],
            "sto_id"           => ["type" => "id", "min_length" => "0", "max_length" => "255", "required" => true,],
            "car_id"           => ["type" => "id", "min_length" => "0", "max_length" => "255", "required" => true,],
        ],
    ];

    public $errors             = [
        "item_with_id_not_found"    => "Услуга с данным идентификатором не существует",
        "auto_service_id_not_found" => "Услуга автосервиса не найдена",
    ];

    public $statuses           = [
        'enabled'  => 1,
        'disabled' => 0,
    ];

    public $api_methods        = [
        "add_service_record" => "addServiceRecord",
        "user_records"       => "getStoVisitsByUser",
        "record_details"     => "getStoDetails",
    ];

    public $default_show_count = 9999;

    //кеш 1с запросов с интервалом 1 час
    private $interval_1c_timer = 3600;

    public $default_sort_field = "created";

    public $controller;

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName                 = "Auto_Service_Record";
        $this->Auto_Service_Record = ClassRegistry::init($modelName);
        $modelName                 = "Sto_Visit";
        $this->Sto_Visit           = ClassRegistry::init($modelName);

        $modelName               = "Sto_Visit_Service";
        $this->Sto_Visit_Service = ClassRegistry::init($modelName);
        $modelName               = "Sto_Visit_Note";
        $this->Sto_Visit_Note    = ClassRegistry::init($modelName);
        $modelName               = "Auto_Service";
        $this->Auto_Service      = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function checkAutoServiceExists($id)
    {
        $this->setup();

        return $this->Auto_Service_Record->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getAutoServiceById($id)
    {
        $this->setup();

        return $this->Auto_Service->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $filter
     * @param $search
     * @return array|int|null
     */
    public function totalCount($filter, $search)
    {
        $this->setup();

        $car_id_filter = [];
        if (!empty($filter['car_id'])) {
            $car_id_filter = ['car_id' => $filter['car_id']];
        }

        $sto_filter = [];
        if (!empty($filter['location_id'])) {
            $sto_filter = ['sto_id' => $filter['location_id']];
        }

        $service_date = [];
        if (!empty($filter['service_date'])) {
            $service_date = ['DATE(service_datetime)' => $filter['service_date']];
        }

        $date_from = [];
        if (key_exists('date_from', $filter)) {
            $date_from = ['DATE(service_datetime) >=' => $filter['date_from']];
        }

        $date_to = [];
        if (key_exists('date_to', $filter)) {
            $date_to = ['DATE(service_datetime) <=' => $filter['date_to']];
        }

        $record_search = [];
        if (!empty($search)) {
            $record_search = [
                'OR' => array(
                    array('User.firstname LIKE' => "%" . $search . "%"),
                    array('User.lastname LIKE ' => "%" . $search . "%"),
                    array('User.phone LIKE ' => "%" . $search . "%"),
                    array('Action.preview LIKE ' => "%" . $search . "%"),
                    array('Location.address LIKE ' => "%" . $search . "%"),
                    array('Auto_Service_Record.comment LIKE ' => "%" . $search . "%"),
                    array('Car_Model.name LIKE ' => "%" . $search . "%"),
                    array('Car_Mark.name LIKE ' => "%" . $search . "%"),
                    array('User_Car.car_number LIKE ' => "%" . $search . "%"),
                )
            ];
        }

        return $this->Auto_Service_Record->find(
            "count",
            [
                'conditions' =>
                    [
                        $car_id_filter,
                        $sto_filter,
                        $service_date,
                        $record_search,
                        $date_from,
                        $date_to
                    ],
                'joins'      => [
                    [
                        'table'      => 'locations',
                        'alias'      => 'Location',
                        'type'       => 'INNER',
                        'conditions' => [
                            'Location.id = Auto_Service_Record.sto_id',
                        ],
                    ],
                    [
                        'table'      => 'user_cars',
                        'alias'      => 'User_Car',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.id = Auto_Service_Record.car_id',
                        ],
                    ],
                    [
                        'table'      => 'users',
                        'alias'      => 'User',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User.id = Auto_Service_Record.user_id',
                        ],
                    ],
                    [
                        'table'      => 'car_marks',
                        'alias'      => 'Car_Mark',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.mark_id = Car_Mark.id',
                        ],
                    ],
                    [
                        'table'      => 'car_models',
                        'alias'      => 'Car_Model',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.model_id = Car_Model.id',
                        ],
                    ],
                    [
                        'table'      => 'actions',
                        'alias'      => 'Action',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'Action.id = Auto_Service_Record.action_id',
                        ],
                    ],
                ],
                'fields'     => [
                    'Auto_Service_Record.*',
                    'Location.*',
                    'User_Car.*',
                    'User.*',
                    'Car_Mark.name',
                    'Car_Model.name',
                ],
            ]
        );

    }

    /**
     * @param $id
     * @param $params
     * @param $data
     *
     * @return false
     */
    public function addServiceRecord($id, $params, $data)
    {
        $this->setup();
        $validate_result = $this->validateServiceUserData($data);

        //проверка существования услуги/сервиса
        /*
        if(!$this->checkAutoServiceExists($data['service_id'])){
            $sid = $data['service_id'];
            $this->returnError("General", "Услуга с переданным идентификатором $sid не найдена");
            return false;
        }
        */
        if (empty($data['service_datetime'])) {
            $this->returnError("General", "Не указано желаемое время записи " . $data['service_datetime']);

            return false;
        }

        if (empty($data['sto_id'])) {
            $this->returnError("General", "Пуст идентификатор автосервиса " . $data['sto_id']);

            return false;
        }

        if (empty($data['user_id'])) {
            $this->returnError("General", "Пуст идентификатор пользователя " . $data['user_id']);

            return false;
        }

        if (empty($data['car_id'])) {
            $this->returnError("General", "Пуст идентификатор автомобиля " . $data['car_id']);

            return false;
        }

        if (!empty($data['action_id'])) {

            $check_action = $this->Action->checkActionExists($data['action_id']);
            if ($check_action == 0) {
                $this->returnError("General", "Акция не найдена! " . $data['action_id']);

                return false;
            }
        }

        //проверка, что переданная машина относится к данному пользователю
        if (!$this->Car->checkUserCarOwner($data['user_id'], $data['car_id'])) {
            $this->returnError(
                "General",
                "Машина с идентификатором " . $data['car_id'] . " не принадлежит пользователю с идентификатором " . $data['user_id'] . " не найдена"
            );

            return false;
        }

        $sto               = $this->Location->getLocationById($data['sto_id']);
        $sto_location_type = 2;
        if ($sto == null or $sto['location_type_id'] != $sto_location_type) {
            $this->returnError(
                "General",
                "Не найден автосервис с идентификатором " . $data['sto_id']
            );

            return false;
        }

        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        $client_data = $this->User->getUserById($data['user_id']);
        $client      = prepare_fio(
            $client_data['User']['firstname'],
            $client_data['User']['lastname'],
            $client_data['User']['middlename']
        );
        $user_phone  = $client_data['User']['phone'];
        $car_data    = $this->Car->getCarById($data['car_id']);
        $car         = "рег.номер <b>" . $car_data['User_Car']['car_number'] . "</b> марка <b>" . $car_data['Car_Mark']['name'] . "</b>, модель <b>" . $car_data['Car_Model']['name'] . "</b>";

        $this->Auto_Service_Record->create();

        $recs = null;
        if(key_exists('recommendations', $data)){
            $recs = $data['recommendations'];
        }

        $id = 0;
        if ($this->Auto_Service_Record->save($data)) {

            $id = $this->Auto_Service_Record->id;
            //response_api(['service_record_id' => $id], "success");
            if (key_exists("action_id", $data) && $data['action_id'] != null) {
                $action_name = $this->Action->getActionName($data['action_id']);
            } else {
                $action_name = "";
            }
            if (is_prod_app()) {
                foreach ($this->STO_NOTIFY_EMAILS as $send_email) {
                    @$this->sendStoNotify(
                        $send_email,
                        $sto['address'],
                        $data['service_datetime'],
                        $client,
                        $user_phone,
                        $car,
                        $data['comment'],
                        $action_name,
                        $recs
                    );
                }
                @$this->sendStoNotify(
                    $sto['email'],
                    $sto['address'],
                    $data['service_datetime'],
                    $client,
                    $user_phone,
                    $car,
                    $data['comment'],
                    $action_name,
                    $recs
                );
            }
        }

        return $id;
    }

    /**
     * @param $email
     * @param $address
     * @param $service_datetime
     * @param $client
     * @param $phone
     * @param $car
     * @param $comment
     * @param $action_name
     * @param $recs
     */
    public function sendStoNotify(
        $email,
        $address,
        $service_datetime,
        $client,
        $phone,
        $car,
        $comment = null,
        $action_name = null,
        $recs = null
    ) {
        //<br> желаемое дата/время записи - <b>$service_datetime</b>,
        $client = trim($client);
        if(mb_strlen($client)<=2){
            $client = "Имя не указано";
        }
        $subject   = "Уведомление о записи в автосервис";
        $mail_data = "Адрес СТО - <b>$address</b>, <br> клиент <b>$client</b>,<br> телефон <b>$phone</b>,<br> автомобиль <b>$car</b><br> комментарий к записи: <b>$comment</b><br>";
        if ($action_name != null) {
            $mail_data .= "акция: " . $action_name;
        }
        if ($recs != null) {
            $mail_data .= "Запись по рекомендации: " . $recs;
        }
        $template = "sto_service_template";
        $layout   = "sto_service_layout";
        $this->EmailCom->sendEmailNow(
            $email,
            "",
            $subject,
            $layout,
            $template,
            $mail_data,
            $attachment = "",
            $start_sending = ""
        );
    }

    private function validateServiceUserData($data)
    {
        return $this->Validator->validateData($this->fields["auto_service_record"], $data);
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     * @param $search
     *
     * @return |null
     */
    public function recordList($show_count, $page, $sort, $sort_dir, $filter, $search)
    {
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = (int)$show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $car_id_filter = [];
        if (!empty($filter['car_id'])) {
            $car_id_filter = ['car_id' => $filter['car_id']];
        }

        $sto_filter = [];
        if (!empty($filter['location_id'])) {
            $sto_filter = ['sto_id' => $filter['location_id']];
        }

        $sto_list_filter = [];
        if (!empty($filter['city_id'])) {
            $sto_list = $this->Location->getStoListByCityId($filter['city_id']);
            $sto_list_filter = ['sto_id' => $sto_list];
        }

        $service_date = [];
        if (!empty($filter['service_date'])) {
            $service_date = ['DATE(service_datetime)' => $filter['service_date']];
        }

        $date_from = [];
        if (key_exists('date_from', $filter)) {
            $date_from = ['DATE(service_datetime) >=' => $filter['date_from']];
        }

        $date_to = [];
        if (key_exists('date_to', $filter)) {
            $date_to = ['DATE(service_datetime) <=' => $filter['date_to']];
        }

        $record_search = [];
        if (!empty($search)) {
            $record_search = [
               'OR' => array(
                    array('User.firstname LIKE' => "%" . $search . "%"),
                    array('User.lastname LIKE ' => "%" . $search . "%"),
                    array('User.phone LIKE ' => "%" . $search . "%"),
                    array('Action.preview LIKE ' => "%" . $search . "%"),
                    array('Location.address LIKE ' => "%" . $search . "%"),
                    array('Auto_Service_Record.comment LIKE ' => "%" . $search . "%"),
                    array('Car_Model.name LIKE ' => "%" . $search . "%"),
                    array('Car_Mark.name LIKE ' => "%" . $search . "%"),
                    array('User_Car.car_number LIKE ' => "%" . $search . "%"),
                )
            ];
        }

        $this->setup();
        $locations = $this->Auto_Service_Record->find(
            "all",
            [
                'conditions' =>
                    [
                        $car_id_filter,
                        $sto_filter,
                        $sto_list_filter,
                        $service_date,
                        $record_search,
                        $date_from,
                        $date_to
                    ],
                'joins'      => [
                    [
                        'table'      => 'locations',
                        'alias'      => 'Location',
                        'type'       => 'INNER',
                        'conditions' => [
                            'Location.id = Auto_Service_Record.sto_id',
                        ],
                    ],
                    [
                        'table'      => 'user_cars',
                        'alias'      => 'User_Car',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.id = Auto_Service_Record.car_id',
                        ],
                    ],
                    [
                        'table'      => 'users',
                        'alias'      => 'User',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User.id = Auto_Service_Record.user_id',
                        ],
                    ],
                    [
                        'table'      => 'car_marks',
                        'alias'      => 'Car_Mark',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.mark_id = Car_Mark.id',
                        ],
                    ],
                    [
                        'table'      => 'car_models',
                        'alias'      => 'Car_Model',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.model_id = Car_Model.id',
                        ],
                    ],
                    [
                        'table'      => 'actions',
                        'alias'      => 'Action',
                        'type'       => 'LEFT',
                        'conditions' => [
                            'Action.id = Auto_Service_Record.action_id',
                        ],
                    ],
                ],
                'limit'      => $show_count,
                'offset'     => $limit_page,
                'order'      => ['Auto_Service_Record.' . $sort . " " . $sort_dir],
                'fields'     => [
                    'Auto_Service_Record.*',
                    'Location.*',
                    'User_Car.*',
                    'User.*',
                    'Car_Mark.name',
                    'Car_Model.name',
                ],
            ]
        );
        if (count($locations) > 0) {
            return $locations;
        }

        return [];
    }

    /**
     * @param $id
     *
     * @return |null
     */
    public function record($id)
    {
        $this->setup();
        $record = $this->Auto_Service_Record->find(
            "first",
            [
                'conditions' =>
                    [
                        'Auto_Service_Record.id' => $id,
                    ],
                'joins'      => [
                    [
                        'table'      => 'locations',
                        'alias'      => 'Location',
                        'type'       => 'INNER',
                        'conditions' => [
                            'Location.id = Auto_Service_Record.sto_id',
                        ],
                    ],
                    [
                        'table'      => 'user_cars',
                        'alias'      => 'User_Car',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.id = Auto_Service_Record.car_id',
                        ],
                    ],
                    [
                        'table'      => 'users',
                        'alias'      => 'User',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User.id = Auto_Service_Record.user_id',
                        ],
                    ],
                    [
                        'table'      => 'car_marks',
                        'alias'      => 'Car_Mark',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.mark_id = Car_Mark.id',
                        ],
                    ],
                    [
                        'table'      => 'car_models',
                        'alias'      => 'Car_Model',
                        'type'       => 'INNER',
                        'conditions' => [
                            'User_Car.model_id = Car_Model.id',
                        ],
                    ],
                ],
                'fields'     => [
                    'Auto_Service_Record.*',
                    'Location.*',
                    'User_Car.*',
                    'User.*',
                    'Car_Mark.name',
                    'Car_Model.name',
                ],
            ]
        );
        if (count($record) > 0) {
            return $record;
        }

        return null;
    }
    /*
     *
     *         $api_data['clientID'] = "1";
        $api_data['phone'] = "9112259120";
        //$api_data['OrderNumber '] = "ППКР-012357";
        $api_data['carNumber'] = "Х375ХТ152";
    */

    /**
     * @param $user_id
     * @param $url
     * @param $user_phone
     * @param $car_number
     * @param $result
     *
     * @return mixed
     */
    private function save1CRequest($user_id, $url, $user_phone, $car_number, $result)
    {
        $request_name = "get_user_sto";
        $data         = [
            "clientID"  => $user_id,
            "carNumber" => $car_number,
            "phone"     => $user_phone,
        ];

        return $this->Log->save1CRequestLog($request_name, $url, get_ip(), serialize($data), $result);
    }

    /**
     * @param $user_id
     * @param $user_phone
     * @param $car_number
     *
     * @return false|mixed|string
     */
    public function getUserStoData1C($user_id, $user_phone, $car_number)
    {

        $server_1c            = Configure::read('1C_GATE');
        $url                  = $server_1c . "GetLastOrderAndLinkCustomer";
        $api_data             = [];
        $api_data['clientID'] = $user_id;
        $api_data['phone']    = $user_phone;
        //$api_data['OrderNumber '] = "ППКР-012357";
        $api_data['carNumber'] = $car_number;

        $options = [
            'http' => [
                'method'  => 'POST',
                'content' => json_encode($api_data),
                'header'  => "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n",
            ],
        ];
        $context = stream_context_create($options);
        $result  = file_get_contents($url, false, $context);
        if ($result === false) { /* Handle error */
        }
        $this->save1CRequest($user_id, $url, $user_phone, $car_number, $result);

        return $result;
    }

    /**
     * @param $order_number
     * @param $car_number
     * @param $user_id
     * @param $user_phone
     *
     * @return false|mixed|string
     */
    public function getUserStoData1CDetails($order_number, $car_number, $user_id, $user_phone)
    {
        $server_1c               = Configure::read('1C_GATE');
        $url                     = $server_1c . "GetOrder";
        $api_data                = [];
        $api_data['OrderNumber'] = $order_number;
        $api_data['carNumber']   = $car_number;
        $api_data['ClientID']    = $user_id;

        $options = [
            'http' => [
                'method'  => 'POST',
                'content' => json_encode($api_data),
                'header'  => "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n",
            ],
        ];
        $context = stream_context_create($options);
        $result  = file_get_contents($url, false, $context);
        if ($result === false) { /* Handle error */
        }
        $this->save1CRequest($user_id, $url, $user_phone, $car_number, $result);

        return $result;
    }

    /**
     * @param $user_id
     * @param $car_number
     *
     * @return false|mixed|string
     */
    public function getSto1COrderList($user_id, $car_number)
    {
        $date_start  = "1990-01-01";
        $date_finish = date("Y-m-d");

        $api_data               = [];
        $api_data['clientID']   = $user_id;
        $api_data['dateStart']  = $date_start;
        $api_data['dateFinish'] = $date_finish;
        $api_data['carNumber']  = $car_number;

        $server_1c = Configure::read('1C_GATE');
        $url       = $server_1c . "GetOrdersList";

        $options = [
            'http' => [
                'method'  => 'POST',
                'content' => json_encode($api_data),
                'header'  => "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n",
            ],
        ];
        $context = stream_context_create($options);
        $result  = file_get_contents($url, false, $context);
        if ($result === false) { /* Handle error */
        }
        $this->save1CRequest($user_id, $url, "", $car_number, $result);

        return $result;
    }

    // О104РО177 chervole lacettI ЗН ВСКР-006802, ВСКР-009823, ТИКР-000555

    /**
     * @param $order_id
     * @param $services
     * @return bool
     * @throws Exception
     */
    public function saveStoServicesByOrderId($order_id, $services)
    {
        foreach ($services as $service) {
            $new_service = [
                'sto_visit_id' => $order_id,
                'service'      => $service['name'],
            ];

            $check_service = $this->Sto_Visit_Service->find(
                "count",
                [
                    'conditions' => $new_service,
                ]
            );
            if ($check_service == 0) {
                $this->Sto_Visit_Service->create();
                $this->Sto_Visit_Service->save($new_service);
            }
        }

        return true;
    }

    /**
     * @param $order_id
     * @param $notes
     *
     * @return bool
     */
    public function saveStoNotesByOrderId($order_id, $notes)
    {
        foreach ($notes as $note) {
            if (is_array($note)) {
                $note_name = $note['name'];
            } else {
                $note_name = $note;
            }
            if (!empty($note_name)) {
                $new_note   = [
                    'sto_visit_id' => $order_id,
                    'note'         => $note_name,
                ];
                $check_note = $this->Sto_Visit_Note->find(
                    "count",
                    [
                        'conditions' => $new_note,
                    ]
                );
                if ($check_note == 0) {
                    $this->Sto_Visit_Note->create();
                    $this->Sto_Visit_Note->save($new_note);
                }
            }
        }

        return true;
    }

    // метод получения истории посещений

    /**
     * @param $id
     * @param $params
     * @param $data
     *
     * @return array|null|bool
     */
    public function getStoVisitsByUser($id, $params, $data)
    {
        $car_id_filter = [];

        if (!key_exists("car_id", $data) or empty($data["car_id"])) {
            $this->returnError("General", "Не передан идентификатор автомобиля пользователя " . $data["car_id"]);
            return false;
        }
        if (!empty($data['car_id'])) {
            $car_id_filter['car_id'] = $data['car_id'];
        }

        if (!key_exists("user_id", $data) or empty($data["user_id"])) {
            $this->returnError("General", "Пуст идентификатор пользователя " . $data["user_id"]);
            return false;
        }
        if (!empty($data['user_id'])) {
            $car_id_filter['user_id'] = $data['user_id'];
        }

        if (!key_exists('car_id', $data) or empty($data['car_id'])) {
            $this->returnError("General", "Пуст идентификатор автомобиля car_id");

            return false;
        }

        if (!$this->Car->checkUserCarOwner($data['user_id'], $data['car_id'])) {
            $this->returnError(
                "General",
                "Машина с идентификатором " . $data['car_id'] . " не принадлежит пользователю с идентификатором " . $data['user_id'] . " не найдена"
            );

            return false;
        }

        $car = $this->Car->getCarById($data['car_id']);

        if (empty($car)) {
            $error_text = "Автомобиль не найден!";
            $this->returnError(
                "General",
                $error_text
            );

            return false;
        }

        $this->setup();

        $sto_visits = $this->Sto_Visit->find(
            "all",
            [
                'conditions' =>
                    [
                        $car_id_filter,
                    ],
                'order'      => ["id DESC"],
            ]
        );

        $interval_1c_timer = $this->interval_1c_timer;//3600 (час)

        if (count($sto_visits) == 0 or time() - strtotime($sto_visits[0]['Sto_Visit']['update_1c']) > $interval_1c_timer) {

            $this->Log->add("Делаем запрос в 1с по машине {$data['car_id']} и пользователю {$data['user_id']}", "1C");

            $car_number = $car['User_Car']['car_number'];
            $user       = $this->User->getUserById($data['user_id']);
            if (empty($user)) {
                $error = "Пользователь не найден!";
                $this->Api->response_api($error, "error");
                exit;
            }
            $user_phone = $user['User']['phone'];
            $user_phone = substr($user_phone, 1);

            // осуществляется привязка пользователя backend и 1с
            $this->getUserStoData1C($data['user_id'], $user_phone, $car_number);
            sleep(2);

            $visits_1c = $this->getSto1COrderList(
                $data['user_id'],
                $car_number
            );

            if ($visits_1c != null) {
                $visits_1c = (array)json_decode($visits_1c);

                if (key_exists("orders", $visits_1c) && count($visits_1c['orders']) > 0) {
                    foreach ($visits_1c['orders'] as $visit_order) {
                        $visit_order  = json_decode(json_encode($visit_order), true);
                        $order_number = $visit_order['number'];

                        $sto_visit_check = $this->Sto_Visit->find(
                            "count",
                            [
                                'conditions' =>
                                    [
                                        'order_number' => $order_number,
                                        'car_id'       => $data['car_id'],
                                    ],
                            ]
                        );

                        if ($sto_visit_check == 0) {

                            $visit_data = [
                                'order_number' => $order_number ?? "",
                                'order_date'   => $visit_order['date'] ?? "",
                                'order_summ'   => $visit_order['summ'] ?? "",
                                'distance'     => $visit_order['kilometer'] ?? "",
                                'car_id'       => $data['car_id'],
                                'user_id'      => $data['user_id'],
                                'car_number'   => $car_number,
                                'update_1c'    => now_date(),
                            ];

                            $this->Sto_Visit->create();
                            $this->Sto_Visit->save($visit_data);

                            $this->Log->add(
                                "Новая запись истории посещений СТО по ЗН 1с по №{$order_number} машина {$data['car_id']}, пользователь {$data['user_id']}",
                                "1C"
                            );
                        } else {
                            $this->Log->add(
                                "Запись истории посещений СТО по ЗН 1с по №{$order_number}, машина {$data['car_id']}, пользователь {$data['user_id']} уже добавлены",
                                "1C"
                            );
                        }
                    }

                } else {
                    $this->Log->add(
                        "В пришедшем ответе от 1с по машине {$data['car_id']} и пользователю {$data['user_id']} заказ-нарядов нет",
                        "1C"
                    );

                    return [];
                }

            } else {
                $this->Log->add(
                    "Записи запроса истории посещений СТО в 1с по машине {$data['car_id']} и пользователю {$data['user_id']} не были найдены",
                    "1C"
                );
            }

        }

        /*
        $this->Log->add(
            "запрос в 1с на получение записей истории посещений СТО в 1с по машине {$data['car_id']} и пользователю {$data['user_id']} не были найдены",
            "1C"
        );
        **/

        $sto_visits = $this->Sto_Visit->find(
            "all",
            [
                'conditions' =>
                    [
                        'car_id'  => $data['car_id'],
                        'user_id' => $data['user_id'],
                    ],
                "fields"     => ["Sto_Visit.id, Sto_Visit.distance, Sto_Visit.order_number, Sto_Visit.order_summ, Sto_Visit.order_date, Sto_Visit.car_number"],
                'order'      => ['id DESC'],
            ]
        );

        $sto_arr = [];
        foreach ($sto_visits as &$sto_visit) {
            $distance = $sto_visit['Sto_Visit']['distance'];
            if ($distance == '0') {
                continue;
            }
            /*
            $sto_visit_services = $this->Sto_Visit_Service->find(
                "all",
                [
                    'conditions' =>
                        [
                            'sto_visit_id' => $sto_visit['Sto_Visit']['order_id'],
                        ],
                    'order'      => ['created ASC'],
                ]
            );

            $sto_visit_notes = $this->Sto_Visit_Note->find(
                "all",
                [
                    'conditions' =>
                        [
                            'sto_visit_id' => $sto_visit['Sto_Visit']['order_id'],
                        ],
                    'order'      => ['created ASC'],
                ]
            );

            $sto_visit['Sto_Visit']['recs']     = $sto_visit_notes;
            $sto_visit['Sto_Visit']['services'] = $sto_visit_services;*/
            $sto_arr[] = $sto_visit['Sto_Visit'];
        }

        if (count($sto_arr) > 0) {

            $this->Log->add(
                "api-ответ на получение записей истории посещений СТО по машине {$data['car_id']} и пользователю {$data['user_id']} - найдено записей: " . count(
                    $sto_arr
                ),
                "1C"
            );

            return $sto_arr;
        }

        $this->Log->add(
            "api-ответ на получение записей истории посещений СТО по машине {$data['car_id']} и пользователю {$data['user_id']} - не найдены записи: ",
            "1C"
        );

        return [];
    }

    /**
     * @param $id
     * @param $params
     * @param $data
     *
     * @return false|array
     */
    public function getStoDetails($id, $params, $data)
    {
        if (!key_exists('order_id', $data) or empty($data['order_id'])) {
            $this->returnError("General", "Пуст идентификатор заказа order_id");

            return false;
        }

        $this->setup();
        $sto_visit_data = $this->Sto_Visit->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $data['order_id'],
                    ],
            ]
        );
        if (!key_exists("Sto_Visit", $sto_visit_data)) {
            $this->returnError("General", "Ошибка! Заказ не найден в базе");

            return false;
        }
        $sto_visit_data = $sto_visit_data['Sto_Visit'];
        // если запрос не найден - отдаем ошибку
        if (empty($sto_visit_data)) {
            $this->returnError("General", "Ошибка! Заказ не найден в базе");

            return false;
        }

        $sto_visit_services = $this->Sto_Visit_Service->find(
            "all",
            [
                'conditions' =>
                    [
                        'sto_visit_id' => $data['order_id'],
                    ],
                'order'      => ['created ASC'],
            ]
        );

        $sto_visit_services_arr = [];
        if (count($sto_visit_services) > 0) {
            foreach ($sto_visit_services as $sto_visit_service) {
                $sto_visit_services_arr[]['name'] = $sto_visit_service['Sto_Visit_Service']['service'];
            }
        }
        $sto_visit_services = $sto_visit_services_arr;
        $sto_visit_notes    = $this->Sto_Visit_Note->find(
            "all",
            [
                'conditions' =>
                    [
                        'sto_visit_id' => $data['order_id'],
                    ],
                'order'      => ['created ASC'],
            ]
        );

        if (count($sto_visit_services) == 0 and count($sto_visit_notes) == 0) {
            // данные еще не были загружены из 1С
            // получить список рекомендаций из последнего запроса
            $user_data  = $this->User->getUserData($sto_visit_data['user_id']);
            $user_phone = $user_data['phone'];

            if (substr($user_phone, 0, 1) == "8") {
                $user_phone = substr($user_phone, 1);
            }

            $visits_details = $this->getUserStoData1CDetails(
                $sto_visit_data['order_number'],
                $sto_visit_data['car_number'],
                $sto_visit_data['user_id'],
                $user_phone
            );

            if ($visits_details != null) {
                $visits_details = (array)json_decode($visits_details);
                if (!empty($visits_details['orderNotes'])) {
                    $visits_details['orderNotes'] = json_decode(json_encode($visits_details['orderNotes']), true);
                    $this->saveStoNotesByOrderId($data['order_id'], $visits_details['orderNotes']);

                    $sto_visit_notes = $this->Sto_Visit_Note->find(
                        "all",
                        [
                            'conditions' =>
                                [
                                    'sto_visit_id' => $data['order_id'],
                                ],
                            'order'      => ['created ASC'],
                        ]
                    );

                    //$sto_visit_notes = $visits_details['orderNotes'];
                } else {
                    $sto_visit_notes = [];
                }

                if (!empty($visits_details['services'])) {
                    $visits_details['services'] = json_decode(json_encode($visits_details['services']), true);
                    $this->saveStoServicesByOrderId($data['order_id'], $visits_details['services']);
                    $sto_visit_services = $visits_details['services'];
                } else {
                    $sto_visit_services = [];
                }
            }

        }

        $sto_arr = ['services' => $sto_visit_services, 'recs' => $sto_visit_notes];

        if (count($sto_arr) > 0) {
            return $sto_arr;
            //response_api($sto_arr, "success");
            //exit;
        }

        return [];
        //response_api([], "success");
        //exit;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function validateAutoService($data)
    {
        return $this->Validator->validateData($this->fields['auto_service'], $data);
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function validateAutoServiceRecord($data)
    {
        return $this->Validator->validateData($this->fields['auto_service_record'], $data);
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }

    public function today_new_sto_records()
    {
        $this->setup();
        return $this->Auto_Service_Record->find(
            "count",
            [
                'conditions' =>
                    [
                        'DATE(NOW()) - DATE(created)' => 0,
                    ],
            ]
        );

    }

    public function total_sto_records()
    {
        $this->setup();
        return $this->Auto_Service_Record->find(
            "count",
            [
                'conditions' =>
                    [],
            ]
        );
    }

    /*
     * 8.4.	В back должно генерироваться письмо на адрес (должен быть привязан к автосервису. Берем из адресов в контактах автосервисов).

Форма письма:
Автосервис :<выбранный адрес автосервиса>Имя:<Имя из профиля приложения> Ваш телефон для связи:<№ телефона, на котором зарегистрировано приложение>

Автомобиль: <Информация из раздела Ваши автомобили >


Комментарий:<информация из поля «Комментарий»>.Удобные для Вас дата и время посещения СТО< из раздела Выбрать желаемое время>
В формате:Дата:12.07.2021
     * */

}