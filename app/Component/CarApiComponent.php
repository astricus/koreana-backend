<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Апи БД Машин
 */
class CarApiComponent extends Component
{
    public $controller;

    public $base_sync_interval = 86400;

    public $test_mark_limit = 30;

    public $base_sync_success_message = "all marks and models synchronized successfully!";

    public $base_sync_api_error_message = "error while fetching car marks by API";

    public $server = "https://services.koreanaparts.ru";

    public $api_user = "koreanaAPI";

    public $api_password = "ktkp9Goi6a51Z5_~l1mMknQD61ppi09UIYIUysbnb";

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Car_Mark";
        $this->Car_Mark = ClassRegistry::init($modelName);
        $modelName = "Car_Model";
        $this->Car_Model = ClassRegistry::init($modelName);
        $modelName = "Car_Specification";
        $this->Car_Specification = ClassRegistry::init($modelName);
        $modelName = "Car_Generation";
        $this->Car_Generation = ClassRegistry::init($modelName);
        $modelName = "Car_Configuration";
        $this->Car_Configuration = ClassRegistry::init($modelName);
    }



    /**
     * @param $request
     * @param $id
     * @return false|string
     */
    public function apiRequest($request, $id)
    {
        $request = $this->server . $request;
        if ($id != null) {
            $request .= $id;
        }

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $request);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,60);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $headers = array(
            'Authorization: Basic '. base64_encode($this->api_user .':'.  $this->api_password),
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 0);
        $contents = curl_exec($ch);
        curl_close($ch);
        //var_dump($contents);
        return $contents;
    }

    public function apiGetBrands()
    {
        return $this->apiRequest("/cars/mark", null);
    }

    public function apiGetModels($id)
    {
        return $this->apiRequest("/cars/model?mark_id=", $id);
    }

    public function apiGetGenerations($id)
    {
        return $this->apiRequest("/cars/generation?model_id=", $id);
    }

    public function apiGetConfigurations($id)
    {
        return $this->apiRequest("/cars/configuration?generation_id=", $id);
    }

    /**
     * @param $id
     * @return int|false
     */
    public function checkingHasOuterMarkId($id)
    {
        $this->setup();
        if ($id == 0) return false;
        $get_mark = $this->Car_Mark->find(
            "first",
            [
                'conditions' =>
                    [
                        'outer_id' => $id,
                    ],
            ]
        );
        if ($get_mark == null) return false;
        return $get_mark['Car_Mark']['id'];
    }

    /**
     * @param $id
     * @return false|mixed
     */
    public function checkingHasOuterModelId($id)
    {
        $this->setup();
        if ($id == 0) return false;
        $get_model = $this->Car_Model->find(
            "first",
            [
                'conditions' =>
                    [
                        'outer_id' => $id,
                    ],
            ]
        );
        if ($get_model == null) return false;
        return $get_model['Car_Model']['id'];
    }

    /**
     * @param $mark_id
     * @param $name
     * @return false|int
     */
    public function checkingOuterModelByName($mark_id, $name)
    {
        $this->setup();
        if ($name == "") return false;
        $get_model = $this->Car_Model->find(
            "first",
            [
                'conditions' =>
                    [
                        'mark_id' => $mark_id,
                        'name' => $name,
                    ],
            ]
        );
        if ($get_model == null) return false;
        return $get_model['Car_Model']['id'];
    }

    /**
     * @param $name
     * @return false|mixed
     */
    public function checkingOuterMarkByName($name)
    {
        $this->setup();
        if ($name == "") return false;
        $get_mark = $this->Car_Mark->find(
            "first",
            [
                'conditions' =>
                    [
                        'name' => $name,
                    ],
            ]
        );
        if ($get_mark == null) return false;
        return $get_mark['Car_Mark']['id'];
    }

    /**
     * @param $outer_id
     * @param $name
     * @param $cyr_name
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function createNewMark($outer_id, $name, $cyr_name)
    {
        $this->setup();
        $new_mark = [
            'outer_id' => $outer_id,
            'name' => $name,
            'cyrillic_name' => $cyr_name,
            'ext_id' => null,
            'popular' => 0,
        ];
        $this->Car_Mark->create();
        if ($this->Car_Mark->save($new_mark)) {
            return $this->Car_Mark->id;
        }
        return false;
    }

    /**
     * @param $outer_id
     * @param $mark_id
     * @param $name
     * @param $cyr_name
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function createNewModel($outer_id, $mark_id, $name, $cyr_name)
    {
        $this->setup();
        // outer_id 	mark_id 	name 	cyrillic_name 	popular 	ext_id 	count 	status 	new
        $new_model = [
            'outer_id' => $outer_id,
            'mark_id' => $mark_id,
            'name' => $name,
            'cyrillic_name' => $cyr_name,
            'ext_id' => null,
            'status' => 0,
            'new' => 1,
            'popular' => 0,
            'count' => 0
        ];
        $this->Car_Model->create();
        if ($this->Car_Model->save($new_model)) {
            return $this->Car_Model->id;
        }
        return false;
    }

    /**
     * @param $outer_model_id
     * @param $id
     * @return bool
     * @throws Exception
     */
    private function connectToOuterModel($outer_model_id, $id)
    {
        $update_model = [
            'outer_id' => $outer_model_id,
        ];
        $this->Car_Model->id = $id;
        if ($this->Car_Model->save($update_model)) {
            return true;
        }
        return false;
    }

    /**
     * @param $outer_id
     * @param $id
     * @return bool
     * @throws Exception
     */
    private function connectToOuterMark($outer_id, $id){
        $update_mark = [
            'outer_id' => $outer_id,
        ];
        $this->Car_Mark->id = $id;
        if ($this->Car_Mark->save($update_mark)) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return array|int|null
     */
    public function getMarkById($id)
    {
        $this->setup();
        return $this->Car_Mark->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /*
     * метод синхронизации обновлений локальной базы автомобилей из базы справочника
     * */
    public function updateBase()
    {
        // забор всех брендов по API
        $all_marks = $this->apiGetBrands();
        if ($all_marks == null) die($this->base_sync_api_error_message);
        $all_marks = json_decode($all_marks, true);
        $this->setup();
        $updates_mark_list = [];
        $updates_model_list = [];
        $tml = 0;
        foreach ($all_marks as $mark) {
            // защитная заглушка
            if($tml>=$this->test_mark_limit) break;
            $outer_id = $mark['id'];
            if ($outer_id == 0) continue;
            $mark_id = $this->checkingHasOuterMarkId($outer_id);
            // если марка точно существует в базе и привязана, то смотрим на дату обновления, если прошло меньше суток - ничего не делаем
            if ($mark_id) {
                $mark_data = $this->getMarkById($mark_id);
                $mark_updated = $mark_data['Car_Mark']['modified'];
                if (strtotime(now_date()) - strtotime($mark_updated) < $this->base_sync_interval) {
                    continue;
                }
            } else {
                //в случае, если требуется обновление марки и модели, увеличиваем (до достижения лимита) количество обновляемых марок
                $tml++;
                $mark['name'] = trim($mark['name']);
                //если модель не найдена по внешнему идентификатору, ищем по названию модели
                $found_mark_by_outer_name = $this->checkingOuterMarkByName($mark['name']);
                if ($found_mark_by_outer_name) {
                    // если смогли распознать марку по названию
                    $this->connectToOuterMark($outer_id, $found_mark_by_outer_name);
                    $mark_id = $found_mark_by_outer_name;
                } else {
                    $mark_id = $this->createNewMark($outer_id, $mark['name'], $mark['cyrillic_name']);
                }
            }
            $updates_mark_list[] = $mark_id;
            $all_models = $this->apiGetModels($outer_id);
            $all_models = json_decode($all_models, true);
            if (!is_array($all_models) OR count($all_models) == 0) continue;
            // проходимся в цикле по всем моделям для данной марки из базы-справочника и проверяем
            // есть ли модель в нашей базе, сначала проверка ведется по outer_model_id, повторная проверка
            // в случае если по outer_model_id модель не найдена, ведется уже по имени (поле name)
            foreach ($all_models as $model) {
                $outer_model_id = $model['id'];
                if ($outer_model_id == 0) continue;
                $model_id = $this->checkingHasOuterModelId($outer_model_id);
                if (!$model_id) {
                    $model['name'] = trim($model['name']);
                    //если модель не найдена по внешнему идентификатору, ищем по названию модели
                    $found_by_outer_name = $this->checkingOuterModelByName($mark_id, $model['name']);
                    if ($found_by_outer_name) {
                        // если смогли распознать модель по названию
                        $this->connectToOuterModel($outer_model_id, $found_by_outer_name);
                    } else {
                        // если не найдена модель по названию - создаем в базе новую модель
                        $model_id = $this->createNewModel($outer_model_id, $mark_id, $model['name'], $model['cyrillic_name']);
                        $updates_model_list[] = $model_id;
                    }
                }
            }
            //$tml++;
        }
        $result = "updated marks: [" . join(" ", $updates_mark_list) . " ] models: [" .  join(" ", $updates_model_list) . " ] \r\n";
        $result .= $this->base_sync_success_message;
        return $result;
    }

}