<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент  Аккаунт (Пользователь API)
 */
class AccountComponent extends Component
{
    public $components = [
        'Api',
        'Session',
        'Error',
    ];

    const AUTH_REQUIRES_MESSAGE = "Error! This action requires user authorization. Please, authorize with your credential.";

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName  = "User";
        $this->User = ClassRegistry::init($modelName);

        $modelName       = "Auth_Code";
        $this->Auth_Code = ClassRegistry::init($modelName);

        $modelName   = "Token";
        $this->Token = ClassRegistry::init($modelName);

    }

    public function getUserId()
    {
        $this->Token = ClassRegistry::init("Token");
        $token       = $this->getUserToken();

        $user = $this->Token->find(
            "first",
            [
                'conditions' =>
                    [
                        'token' => $token,
                    ],
            ]
        );
        if (count($user) > 0) {
            return $user['Token']['user_id'];
        }

        return null;
    }

    public function getTokens($id)
    {
        $this->Token = ClassRegistry::init("Token");
        $user_tokens = $this->Token->find(
            "all",
            [
                'conditions' =>
                    [
                        'user_id' => $id,
                    ],
                "order"      => ["id ASC"],
            ]
        );
        if (count($user_tokens) > 0) {
            return $user_tokens;
        }

        return null;
    }

    public function terminateToken($token)
    {
        $this->Token = ClassRegistry::init("Token");
        if ($this->checkToken($token)) {
            $token_id        = $this->getTokenIdByToken();
            $this->Token->id = $token_id;
            $this->Token->delete();
        }
    }

    /**
     * @param        $length
     * @param string $keyspace
     *
     * @return string
     * @throws Exception
     */
    public function generateRandomPass(
        $length,
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$_'
    ) {
        $str = '';
        $max = mb_strlen($keyspace) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }

        return $str;
    }

    public function is_auth()
    {
        if ($this->Session->check('token') or !empty($_COOKIE['token'])) {
            return true;
        } else {
            return false;
        }
    }

    public function auth_requires()
    {
        if (!$this->is_auth()) {
            $token = $_COOKIE['token'] ?? null;

            if (is_null($token)) {
                $this->Api->response_api(["error" => self::AUTH_REQUIRES_MESSAGE], "error");
                exit;
            }
            if (!$this->checkToken($token)) {
                $this->Api->response_api(["error" => self::AUTH_REQUIRES_MESSAGE], "error");
                exit;
            }
        }
    }

    /**
     * @param $token
     *
     * @return bool
     */
    public function checkToken($token)
    {
        $this->Token = ClassRegistry::init("Token");
        $user        = $this->Token->find(
            "count",
            [
                'conditions' =>
                    [
                        'token' => $token,
                    ],
            ]
        );
        if ($user > 0) {
            return true;
        }

        return false;
    }

    /** Смена пароля покупателя
     *
     * @param $user_id
     * @param $new_password
     *
     * @return bool
     */
    public function changePassword($user_id, $new_password)
    {
        $modelName     = "Orderer";
        $this->Orderer = ClassRegistry::init($modelName);

        $new               = get_hash(Configure::read('USER_AUTH_SALT'), $new_password);
        $this->Orderer->id = $user_id;

        return $this->Orderer->save(['password' => $new]);
    }

    /**
     * @param int    $user_id
     * @param string $type
     *
     * @return string
     */
    public function getUserImagesDir($user_id, $type = "relative")
    {
        if ($type == "relative") {
            return Router::url('/', true) . Configure::read('USER_AVATAR_UPLOAD_DIR_RELATIVE') . DS . $user_id;
        }

        return Configure::read('USER_AVATAR_UPLOAD_DIR') . DS . $user_id;
    }

    /**
     * @return mixed
     */
    private function getTokenIdByToken()
    {
        $this->Token = ClassRegistry::init("Token");
        $token       = $this->getUserToken();
        $user_token  = $this->Token->find(
            "first",
            [
                'conditions' =>
                    [
                        'token' => $token,
                    ],
            ]
        );
        if (count($user_token) > 0) {
            return $user_token['Token']['id'];
        }
    }

    /**
     * @return mixed
     */
    private function getUserToken()
    {
        if ($this->Session->check('token')) {
            return $this->Session->read('token');

        } else {
            if (!empty($_COOKIE['token'])) {
                $token = $_COOKIE['token'];
                if (is_array($token)) {
                    $token = $token[0];
                }

                return $token;
            }
        }

        return null;
    }

    public function user_data()
    {
        if (!$this->is_auth()) {
            return false;
        } else {
            if (empty($this->user_data)) {
                $this->user_data = $this->getUserData($this->getUserId());
            }

            return $this->user_data;
        }
    }

    /**
     * @param $user_id
     *
     * @return mixed
     */
    public function getUserData($user_id)
    {
        $modelName  = "User";
        $this->User = ClassRegistry::init($modelName);
        $user       = $this->User->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $user_id,
                    ],
            ]
        );

        return $user;
    }

    public function getUserSessionId()
    {
        $ip           = get_ip();
        $ua           = get_ua();
        $os           = get_os();
        $user_session = $this->checkUserSession($ip, $ua, $os);
        if ($user_session) {
            return $user_session;
        } else {
            return $this->addUserSession($ip, $ua, $os);
        }
    }

    /**
     * @param $ip
     * @param $ua
     * @param $os
     *
     * @return mixed
     */
    private function checkUserSession($ip, $ua, $os)
    {
        $modelName     = "FdbUser";
        $this->FdbUser = ClassRegistry::init($modelName);

        return $this->FdbUser->find(
            'count',
            [
                'conditions' =>
                    [
                        'ip'      => $ip,
                        'browser' => $ua,
                        'os'      => $os,
                    ],
            ]
        );
    }

    // генерация токена в админ панели
    public function createNewToken($user_id)
    {
        $this->setup();
        $token = $this->generateToken($user_id);
        $this->saveToken($user_id, $token);

        return $token;
    }

    /**
     * @param $user_id
     *
     * @return string
     */
    private function generateToken($user_id)
    {
        return md5(time() . uniqid() . $user_id);
    }

    /**
     * @param $user_id
     * @param $token
     *
     * @return string
     */
    private function saveToken($user_id, $token)
    {
        $token_data = [
            'token'   => $token,
            'user_id' => $user_id,
            'expired' => date('Y-m-d', strtotime('+1 year')),
        ];
        $this->Token->save($token_data);
    }

}