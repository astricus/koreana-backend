<?php
App::uses('Component', 'Controller');
App::uses('AppController', 'Controller');

class BreadcrumbsComponent extends Component
{
    private $breadcrumbs = array();

    public function initialize(Controller $controller)
    {
        $ctrl_init = strtolower($controller->name);
        if (APP_MODE == "LKP") {
            $this->add(_('Личный кабинет поставщика'), Router::url('/'));
        } else if ($ctrl_init == "LKZ") {
            $this->add(_('Личный кабинет заказчика'), Router::url('/'));
        } else if ($ctrl_init == "CP") {
            $this->add(_('Административная панель'), Router::url('/'));
        }
    }

    public function beforeRender(Controller $controller)
    {
        $controller->set('breadcrumbs', $this->breadcrumbs);
    }

    public function add($title, $url = '', $icon = '')
    {
        if (is_array($title)) {
            return $this->_add($title);
        }
        if (empty($this->breadcrumbs) || ($this->breadcrumbs[count($this->breadcrumbs) - 1]['url'] != $url)) {
            $this->breadcrumbs[] = array(
                'title' => $title,
                'url' => $url,
                'icon' => $icon
            );
        }
        return true;
    }

    public function clear()
    {
        $this->breadcrumbs = array();
        return true;
    }

    private function _add($params)
    {
        if (!is_array($params)) return false;
        foreach ($params as $param) {
            $this->add($param['title'], $param['url']);
        }
        return true;
    }
}