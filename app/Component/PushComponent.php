<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Push
 */
class PushComponent extends Component
{

    private $android_channel = "koreana_android_notification_channel";

    public $valid_message_types = [
        'default' => 'Без типа (по умолчанию)',
        'service' => 'Сервисное',
        'update' => 'Обновление',
    ];

    public $components = [
        'Admin',
        'Api',
        'City',
        'Content',
        'Error',
        'Session',
        'Validator',
        'User',
    ];

    public $fields = [
        "name" => ["type" => "text", "min_length" => "4", "max_length" => "255", "required" => true,],
        "push_title" => ["type" => "text", "min_length" => "4", "max_length" => "", "required" => true,],
        "preview" => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => false,],
        "push_message" => ["type" => "text", "min_length" => "0", "max_length" => "", "required" => true,],
        "image" => ["type" => "text", "min_length" => "1", "max_length" => "", "required" => false,],
        "enabled" => ["type" => "numeric", "min_length" => "0", "max_length" => "1", "required" => false,],
    ];

    public $errors = [
        "invalid_action_status" => "Передан некорректный статус сообщения",
        "push_with_id_not_found" => "Сообщения с данным идентификатором не существует",
        "user_with_id_not_found" => 'Получать сообщения с данным идентификатором не найден',
        "device_with_id_not_found" => "Не найдено данное устройство",
        "error_while_sending_push" => "Уведомление не удалось отправить",
    ];

    public $firebase_error = "";

    public $statuses = [
        'enabled' => 1,
        'disabled' => 0,
    ];

    public $push_sent_statuses = [
        'new' => 'ожидает',
        'sent' => 'отправлено',
        'received' => 'доставлено',
        'error' => 'ошибка',
    ];

    public $error = "";

    public $api_methods = [
        "add_device" => "addDeviceApi",
        "get" => "getPushById",
        "delete_device" => "deleteDeviceApi",
        "messages_by_user" => "messagesByUserDeviceApi",
        "message_by_user" => "messageByUserDeviceApi",
        "message_received" => "messageReceivedApi",
    ];

    public $default_show_count = 9999;

    public $default_sort_field = "created";

    const AUTH_REQUIRES_MESSAGE = "Error! This action requires user authorization. Please, authorize with your credential.";

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Push_Message";
        $this->Push_Message = ClassRegistry::init($modelName);
        $modelName = "User_Device";
        $this->User_Device = ClassRegistry::init($modelName);
        $modelName = "Push_Sent";
        $this->Push_Sent = ClassRegistry::init($modelName);
        $modelName = "Push_Message_Context";
        $this->Push_Message_Context = ClassRegistry::init($modelName);

    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function checkPushExists($id)
    {
        $this->setup();

        return $this->Push_Message->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     * @return array|int|null
     */
    public function checkPushSentExists($id)
    {
        $this->setup();

        return $this->Push_Sent->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getPushById($id)
    {
        $this->setup();
        $push = $this->Push_Message->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
        if (count($push) > 0) {
            return $push['Push_Message'];
        }

        return null;
    }

    public function totalPushCount()
    {
        $this->setup();
        $push_count = $this->Push_Message->find(
            "count",
            [
                'conditions' => [],
            ]
        );

        return $push_count;
    }

    /**
     * @param $data
     * @return array|bool|int|mixed|string
     * @throws Exception
     */
    public function createPushMessage($data)
    {
        $this->setup();
        $validate_result = $this->validatePushFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        $this->Push_Message->create();
        if ($this->Push_Message->save($data)) {
            return $this->Push_Message->id;
        }

        return false;
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return array|null
     */
    public function pushList($show_count, $page, $sort, $sort_dir, $filter)
    {
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $view_active = "";
        $not_finished = "";
        if (isset($filter["show_active"]) && $filter["show_active"] == true) {
            $view_active = 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(start_datetime)>0';
            $not_finished = 'UNIX_TIMESTAMP(stop_datetime)-UNIX_TIMESTAMP(NOW())>0';
        }

        $this->setup();

        $start_datetime_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "start_datetime") {
                $start_datetime_filter = ['Push_Message.start_datetime >' => $f_value];
            }
        }

        $push_list = $this->Push_Message->find(
            "all",
            [
                'conditions' =>
                    [
                        $view_active,
                        $start_datetime_filter,
                        $not_finished,
                    ],
                'joins' => [],
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => [$sort . " " . $sort_dir],
                //'fields'     => ['DISTINCT Action.id, Action.*'],
            ]
        );

        if (count($push_list) > 0) {
            // push_sent
            foreach ($push_list as &$push_item) {
                $p_id = $push_item['Push_Message']['id'];
                $p_sent = $this->Push_Sent->find(
                    "count",
                    [
                        'conditions' =>
                            [
                                'push_id' => $p_id,
                                'status' => 'sent',
                            ],
                    ]
                );
                $p_new = $this->Push_Sent->find(
                    "count",
                    [
                        'conditions' =>
                            [
                                'push_id' => $p_id,
                                'status' => 'new',
                            ],
                    ]
                );
                $p_received = $this->Push_Sent->find(
                    "count",
                    [
                        'conditions' =>
                            [
                                'push_id' => $p_id,
                                'status' => 'received',
                            ],
                    ]
                );
                $p_error = $this->Push_Sent->find(
                    "count",
                    [
                        'conditions' =>
                            [
                                'push_id' => $p_id,
                                'status' => 'error',
                            ],
                    ]
                );
                $push_item['sent'] = $p_sent;
                $push_item['new'] = $p_new;
                $push_item['received'] = $p_received;
                $push_item['error'] = $p_error;
            }

            return $push_list;
        }

        return null;
    }

    /**
     * @param $push_id
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @return array|int|null
     */
    public function pushSentList($push_id, $show_count, $page, $sort, $sort_dir)
    {
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $this->setup();

        $user_list = $this->Push_Sent->find(
            "all",
            [
                'conditions' => [
                    'push_id' => $push_id
                ],
                'joins' => [
                    [
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'INNER',
                        'conditions' => [
                            'Push_Sent.user_id = User.id',
                        ],
                    ],
                    [
                        'table' => 'user_devices',
                        'alias' => 'User_Device',
                        'type' => 'LEFT',
                        'conditions' => [
                            'Push_Sent.device_id = User_Device.device_id',
                        ],
                    ],
                ],
                'fields' => ["User.*, Push_Sent.*, User_Device.*"],
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => ["Push_Sent." . $sort . " " . $sort_dir],
            ]
        );

        if (count($user_list) > 0) {
            return $user_list;
        }

        return [];
    }

    /**
     * @param $push_id
     * @return array|int|null
     */
    public function totalPushSentCount($push_id)
    {
        $this->setup();
        return $this->Push_Sent->find(
            "count",
            [
                'conditions' =>
                    [
                        'push_id' => $push_id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     * @param $data
     *
     * @return bool
     */
    public function updatePushMessage($id, $data)
    {
        if (!$this->checkPushExists($id)) {
            $this->returnError("General", $this->errors['push_with_id_not_found']);

            return false;
        }
        $validate_result = $this->validatePushFields($data);
        if (is_array($validate_result)) {
            $this->returnError("General", implode($validate_result));

            return false;
        }

        $this->Push_Message->id = $id;
        if ($this->Push_Message->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @param $message_id
     *
     * @return array
     * @throws Exception
     */
    public function savePushImage($file, $width, $height, $message_id): array
    {
        $saving = $this->Content->savePushImage($file, $width, $height);
        if ($saving['status']) {
            $this->setup();
            $this->Push_Message->id = $message_id;
            $this->Push_Message->save(['push_image' => $saving['name']]);
        }
        return $saving;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function validatePushFields($data)
    {
        return $this->Validator->validateData($this->fields, $data);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deletePush($id): bool
    {
        if (!$this->checkPushExists($id)) {
            $this->returnError("General", $this->errors['push_with_id_not_found']);

            return false;
        }

        //очистка копий пуша
        $user_sent_list = $this->Push_Sent->find(
            "all",
            [
                'conditions' => [
                    'push_id' => $id
                ],
            ]
        );
        if (count($user_sent_list) > 0) {
            foreach ($user_sent_list as $user_sent_item) {
                $this->Push_Sent->id = $user_sent_item['Push_Sent']['id'];
                $this->Push_Sent->delete();
            }
        }

        $this->Push_Message->id = $id;
        $this->Push_Message->delete();
        return true;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    private function validateStatus($status)
    {
        if ($status !== 1 and $status !== 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockPush($id)
    {
        $this->setup();
        if (!$this->checkPushExists($id)) {
            $this->returnError("General", $this->errors['push_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockPush($id)
    {
        $this->setup();
        if (!$this->checkPushExists($id)) {
            $this->returnError("General", $this->errors['push_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     * @throws Exception
     */
    private function setStatus($id, $status)
    {
        $this->setup();
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->errors['invalid_action_status']);

            return false;
        }
        $update_data = ['enabled' => $status];
        $this->Push_Message->id = $id;
        if ($this->Push_Message->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }

    /**
     * @param array $user_ids
     * @param int $push_id
     * @return bool
     * @throws Exception
     */
    public function sendPush(array $user_ids, int $push_id): bool
    {
        $push = $this->getPushById($push_id);
        if (is_null($push)) {
            $this->returnError("General", $this->errors['push_with_id_not_found']);

            return false;
        }
        foreach ($user_ids as $user_id) {
            $unread_messages = $this->userUnreadMessages($user_id);
            $devices = $this->getUserDevices($user_id);
            if (count($devices) > 0) {
                foreach ($devices as $device) {

                    $push_sent_id = $this->getPushSentId($device, $push_id);
                    $cur_status = $this->getPushSentStatus($push_sent_id);
                    if ($cur_status == "new" or $cur_status == "error") {

                        $push_context = $this->Push_Message_Context->find(
                            "first",
                            [
                                'conditions' =>
                                    [
                                        'message_id' => $push_id,
                                    ],
                            ]
                        );
                        $payload = [];
                        foreach ($push_context as $push_context_item) {
                            $payload[$push_context_item['Push_Message_Context']['title']] = $push_context_item['Push_Message_Context']['value'];
                        }

                        $send_result = $this->sendNativePush($push_id, $device, $push['push_title'], $push['push_message'], $push['created'],
                            $unread_messages, $push["push_image"], $push["push_type"], $payload);
                        $push_sent_id = $this->getPushSentId($device, $push_id);
                        $status = $send_result ? "sent" : "error";
                        $this->updatePushSentStatus($push_sent_id, $status, $this->firebase_error);

                    }
                }
            }
        }
        $user = $this->User->getUserData($user_id);
        if (is_null($user)) {
            $this->returnError("General", $this->errors['user_with_id_not_found']);

            return false;
        }

        return true;
    }

    /**
     * @param $sent_user_id
     * @return bool
     * @throws Exception
     */
    public function sendPushToUser($sent_user_id): bool
    {
        $this->setup();
        $sent_data = $this->Push_Sent->find(
            "first",
            [
                'conditions' => [
                    'id' => $sent_user_id
                ],
            ]
        );

        if (count($sent_data) == 0) {
            $this->returnError("General", $this->errors['push_with_id_not_found']);
            return false;
        }

        $push = $this->getPushById($sent_data['Push_Sent']['push_id']);
        if (is_null($push)) {

            $this->returnError("General", $this->errors['push_with_id_not_found']);

            return false;
        }

        $cur_status = $this->getPushSentStatus($sent_user_id);
        $status = "";
        if ($cur_status == "new" or $cur_status == "error") {

            $push_context = $this->Push_Message_Context->find(
                "all",
                [
                    'conditions' =>
                        [
                            'message_id' => $sent_data['Push_Sent']['push_id'],
                        ],
                ]
            );
            $payload = [];
            foreach ($push_context as $push_context_item) {
                $payload[$push_context_item['Push_Message_Context']['title']] = $push_context_item['Push_Message_Context']['value'];
            }

            $unread_messages = $this->userUnreadMessages($sent_data['Push_Sent']['user_id']);
            $send_result = $this->sendNativePush($sent_data['Push_Sent']['push_id'], $sent_data['Push_Sent']['device_id'],
                $push['push_title'], $push['push_message'], $push['created'], $unread_messages,
                $push["push_image"], $push["push_type"], $payload);
            $push_sent_id = $this->getPushSentId($sent_data['Push_Sent']['device_id'], $sent_data['Push_Sent']['push_id']);
            $status = $send_result ? "sent" : "error";
            $this->updatePushSentStatus($push_sent_id, $status, $this->firebase_error);
        }
        if ($status == "error") {
            $this->error = $this->errors['error_while_sending_push'];
            return false;
        }
        return true;
    }

    /**
     * @param $id
     * @param $status
     * @param $error
     * @return bool
     * @throws Exception
     */
    private function updatePushSentStatus($id, $status, $error = "")
    {
        $this->setup();
        $update_data = ['status' => $status, 'error' => $error];
        $this->Push_Sent->id = $id;
        if ($this->Push_Sent->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private
    function getPushSentStatus(
        $id
    )
    {
        $this->setup();
        $push_sent = $this->Push_Sent->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );

        return $push_sent['Push_Sent']['status'];
    }

    /**
     * @param $device_id
     * @param $push_id
     *
     * @return mixed
     */
    private
    function getPushSentId(
        $device_id,
        $push_id
    )
    {
        $this->setup();
        $push_sent = $this->Push_Sent->find(
            "first",
            [
                'conditions' =>
                    [
                        'device_id' => $device_id,
                        'push_id' => $push_id,
                    ],
            ]
        );

        return $push_sent['Push_Sent']['id'];
    }

    /**
     * @param $device_id
     * @return mixed|null
     */
    private function getOSByDeviceId($device_id)
    {
        $device = $this->User_Device->find(
            "first",
            [
                'conditions' =>
                    [
                        'device_id' => $device_id,
                    ],
            ]
        );
        if (count($device) > 0) {
            return $device['User_Device']['os'];
        }
        return null;
    }

    /**
     * @param $user_id
     *
     * @return array
     */
    private function getUserDevices($user_id)
    {
        $this->setup();
        $device_ids = $this->User_Device->find(
            "all",
            [
                'conditions' =>
                    [
                        'user_id' => $user_id,
                    ],
            ]
        );
        $arr = [];
        foreach ($device_ids as $device_id) {
            $arr[] = $device_id['User_Device']['device_id'];
        }

        return $arr;
    }

    /**
     * @param $user_id
     * @return int|null
     */
    private function userUnreadMessages($user_id)
    {
        $this->setup();
        return $this->Push_Sent->find(
            "count",
            [
                'conditions' =>
                    [
                        'Push_Sent.user_id' => $user_id,
                    ],
                'group' => array('Push_Sent.push_id'),
                'fields' => ["DISTINCT Push_Sent.push_id"]
            ]
        );
    }

    /**
     * @param $push_id
     * @param $device_id
     * @param $title
     * @param $message
     * @param $date
     * @param $unread_messages_count
     * @param $image
     * @param string $type
     * @param $payload
     * @return bool
     */
    public function sendNativePush($push_id, $device_id, $title, $message, $date, $unread_messages_count,
                                   $image = null, string $type = "", $payload = null): bool
    {
        $payload_data = [];
        $payload_data['notificationId'] = $push_id;
        if (count($payload) > 0) {
            foreach ($payload as $p_key => $p_val) {
                $payload_data[$p_key] = $p_val;
            }
        } else {
            $payload_data['notificationType'] = 'default';
            $payload_data['notificationDate'] = $date;
        }

        /*
         * {
    "message": {
        "data": {
            "notificationId": "2",
            "notificationType": "service",
            "notificationDate": "2022-02-22 12:51:35"
        },
        "notification": {
            "title": "21 декабря нужно будет сделать техническое облуживание KIA Rio 2008",
            "body": "Замена свечей зажигания; Замена масла в КПП; Замена передних...",
            "image": "https://s.car.info/image_files/1920/porsche-taycan-front-side-1-1106393.jpg"
            },
        "android": {
            "priority": "HIGH",
            "notification": {
            "channel_id": "koreana_android_notification_channel",
            "sticky": true,
            "notification_priority": "PRIORITY_MAX"
            }
        },
        "token": "cMzP7aveTUEQitYweGOirs:APA91bE6FxL29gz7UfmiCQWXOwxa5tuvk1D4qU7Wg-kIYdtZzXF-oZi2FxGgwDL_THMJN3AmRAs2CLqObwM3uxiMZM3TGwn9x6rZcuFcvRrkKkl4-GX4ykl_x2eCoSuS7ad1iBYb4tri"
    }
}
         * */

        $message = [
            'title' => $title,
            'body' => $message,
            "image" => $this->getPushImageUrl($image),
            "sound" => 'default',
        ];

        $arrayToSend = [
            'to' => $device_id,
            'notification' => $message,
            "data" => $payload_data,
            "android" => [
                "priority" => "HIGH",
                'notification' => [
                    "sound" => "default",
                    'channel_id' => $this->android_channel,
                    "sticky" => true,
                    "notification_priority" => "PRIORITY_MAX"
                ]
            ],
            "apns" => [
                "payload" => [
                    "aps" => [
                        "sound" => "default",
                        "badge" => $unread_messages_count,
                    ]
                ]
            ],
            "token" => "$device_id"
        ];

        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $YOUR_KEY = Configure::read('FIREBASE_API_KEY');

        $headers[] = "Authorization: key=$YOUR_KEY";
        $api_endpoint = "https://fcm.googleapis.com/fcm/send";

        $json = json_encode($arrayToSend);
        $ch = curl_init($api_endpoint);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);

        if (key_exists("success", $response) && $response["success"] == 1) {
            return true;
        } else {
            $this->firebase_error = $response["results"][0]["error"];
            return false;
        }
    }

    /**
     * @param $id
     * @param $params
     * @param $data
     *
     * @return void
     * @throws Exception
     */
    public function addDeviceApi($id, $params, $data)
    {
        $errors = [];
        if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
            $errors[] = "empty user_id";
        }
        if (empty($data['device_id'])) {
            $errors[] = "empty device_id";
        }
        if (empty($data['os'])) {
            $errors[] = "empty operation system name";
        }
        if (!empty($this->hasDeviceId($data['device_id']))) {
            $this->Api->response_api($data['device_id'], "success");
            exit;
        }

        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $this->setup();
        $this->User_Device->create();
        $data = [
            'device_id' => $data['device_id'],
            'user_id' => $data['user_id'],
            'os' => $data['os'],
        ];
        if ($this->User_Device->save($data)) {
            $this->Api->response_api($this->User_Device->id, "success");
        } else {
            $this->Api->response_api(null, "error");
        }
        exit;

    }

    /**
     * @param $id
     * @param $params
     * @param $data
     *
     * @return void
     */
    public function deleteDeviceApi($id, $params, $data)
    {
        $errors = [];
        if (empty($data['device_id'])) {
            $errors[] = "empty device_id";
        }
        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $this->setup();
        $device_id = $data['device_id'];
        if ($this->hasDeviceId($device_id)) {
            $device = $this->User_Device->find(
                "first",
                [
                    'conditions' =>
                        [
                            'device_id' => $device_id,
                        ],
                ]
            );
            $this->User_Device->id = $device['User_Device']['id'];
            $this->User_Device->delete();
        }
        $this->Api->response_api("", "success");
        exit;

    }

    /**
     * @param $device_id
     *
     * @return bool
     */
    private function hasDeviceId($device_id)
    {
        $this->setup();

        return (bool)$this->User_Device->find(
            "count",
            [
                'conditions' =>
                    [
                        'device_id' => $device_id,
                    ],
            ]
        );
    }

    public function messagesByUserDeviceApi($id, $params, $data)
    {
        $errors = [];
        if (empty($data['user_id'])) {
            $errors[] = "empty user_id";
        }
        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $this->setup();
        $push_messages = $this->Push_Sent->find(
            "all",
            [
                'conditions' =>
                    [
                        'Push_Sent.user_id' => $data['user_id'],
                    ],
                'joins' => [
                    [
                        'table' => 'push_messages',
                        'alias' => 'Push_Message',
                        'type' => 'INNER',
                        'conditions' => [
                            'Push_Sent.push_id = Push_Message.id',
                        ],
                    ],
                ],
                'group' => array('Push_Message.id'),
                'order' => ["Push_Sent.created DESC"],
                'fields' => ["DISTINCT Push_Message.id, Push_Message.*, Push_Sent.*"],
            ]
        );
        if (count($push_messages) === 0) {
            $this->Api->response_api([], "success");
            exit;
        }
        $msgs = [];
        foreach ($push_messages as $push_message) {
            $msg = [
                'notificationId' => $push_message['Push_Message']['id'],
                'notificationType' => $push_message['Push_Message']['push_type'],
                'title' => $push_message['Push_Message']['push_title'],
                'text' => $push_message['Push_Message']['push_message'],
                'imageURL' => $this->getPushImageUrl($push_message['Push_Message']['push_image']),
                'notificationDate' => $push_message['Push_Sent']['created'],
                'is_seen' => $push_message['Push_Sent']['status'] == "received" ? true : false,
            ];
            $msgs[] = $msg;
        }
        $this->Api->response_api($msgs, "success");
        exit;
    }

    public function messageByUserDeviceApi(
        $id,
        $params,
        $data
    )
    {
        $errors = [];
        if (empty($data['user_id'])) {
            $errors[] = "empty user_id";
        }
        if (empty($data['message_id'])) {
            $errors[] = "empty message_id";
        }
        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $this->setup();
        $push_message = $this->Push_Sent->find(
            "first",
            [
                'conditions' =>
                    [
                        'Push_Sent.user_id' => $data['user_id'],
                        'Push_Sent.push_id' => $data['message_id'],
                    ],
                'joins' => [
                    [
                        'table' => 'push_messages',
                        'alias' => 'Push_Message',
                        'type' => 'INNER',
                        'conditions' => [
                            'Push_Sent.push_id = Push_Message.id',
                        ],
                    ],
                    [
                        'table' => 'push_message_context',
                        'alias' => 'Push_Message_Context',
                        'type' => 'LEFT',
                        'conditions' => [
                            'Push_Message_Context.message_id = Push_Message.id',
                        ],
                    ],
                ],
                'order' => ["Push_Sent.created DESC"],
                'fields' => ["Push_Message.*, Push_Sent.*, Push_Message_Context.*"],
            ]
        );
        if ($push_message == null) {
            $this->Api->response_api([], "success");
            exit;
        }
        $update_data = ['status' => 'received'];
        $this->Push_Sent->id = $push_message['Push_Sent']['id'];
        $this->Push_Sent->save($update_data);
        $msg = [
            'notificationId' => $push_message['Push_Message']['id'],
            'notificationType' => $push_message['Push_Message']['push_type'],
            'title' => $push_message['Push_Message']['push_title'],
            'text' => $push_message['Push_Message']['push_message'],
            'is_seen' => $push_message['Push_Sent']['status'] == "received" ? true : false,
            'imageURL' => $this->getPushImageUrl($push_message['Push_Message']['push_image']),
            'notificationDate' => $push_message['Push_Sent']['created'],
            'payload' => $push_message['Push_Message_Context'],
        ];

        if (!empty($push_message['Push_Message']['push_type'])) {

            $push_context = $this->Push_Message_Context->find(
                "all",
                [
                    'conditions' =>
                        [
                            'message_id' => $data['message_id'],
                        ],
                ]
            );
            $contx = [];
            foreach ($push_context as $push_context_item) {
                $contx[$push_context_item['Push_Message_Context']['title']] = $push_context_item['Push_Message_Context']['value'];
            }
            $msg['payload'] = $contx;
        }

        $this->Api->response_api($msg, "success");
        exit;
    }

    public function messageReceivedApi(
        $id,
        $params,
        $data
    )
    {
        $errors = [];
        if (empty($data['user_id'])) {
            $errors[] = "empty user_id";
        }
        if (count($errors) > 0) {
            $this->Api->response_api($errors, "error");
            exit;
        }
        $this->setup();
        $push_messages = $this->Push_Sent->find(
            "all",
            [
                'conditions' =>
                    [
                        'Push_Sent.user_id' => $data['user_id'],
                    ],
            ]
        );
        if ($push_messages == null) {
            $this->Api->response_api('messages not found', "error");
            exit;
        }

        $update_data = ['status' => 'received'];
        foreach ($push_messages as $push_message) {
            $this->Push_Sent->id = $push_message['Push_Sent']['id'];
            $this->Push_Sent->save($update_data);
        }

        $this->Api->response_api('push messages received', "success");
        exit;
    }

    public function getPushImageUrl($file)
    {
        return $this->Content->getPushImageUrl($file);
    }

    /**
     * @param $push_id
     * @param $user_id
     * @return bool
     * @throws Exception
     */
    public function addPushReceiver($push_id, $user_id)
    {
        $this->setup();
        $user_devices = $this->getUserDevices($user_id);
        if (count($user_devices) == 0) {
            return false;
        }

        foreach ($user_devices as $user_device) {
            // проверка защиты от дублей
            $data = [
                'status' => 'new',
                'push_id' => $push_id,
                'user_id' => $user_id,
                'device_id' => $user_device,
                'os' => $this->getOSByDeviceId($user_device)
            ];

            $check_doubles = $this->Push_Sent->find(
                "count",
                [
                    'conditions' =>
                        [
                            'push_id' => $push_id,
                            'user_id' => $user_id,
                            'device_id' => $user_device
                        ],
                ]
            );
            if ($check_doubles == 0) {
                $this->Push_Sent->create();
                $this->Push_Sent->save($data);
            }
        }

        return true;
    }

    /**
     * @param $push_sent_id
     * @return bool
     */
    public function deleteReceiver($push_sent_id)
    {

        if (!$this->checkPushSentExists($push_sent_id)) {
            $this->returnError("General", $this->errors['push_with_id_not_found']);

            return false;
        }
        $this->Push_Sent->id = $push_sent_id;
        $this->Push_Sent->delete();

        return true;
    }

    public function today_new_push_stat()
    {
        $this->setup();
        return $this->Push_Sent->find(
            "count",
            [
                'conditions' =>
                    [
                        'DATE(NOW()) - DATE(created)' => 0,
                    ],
            ]
        );

    }

    public function total_push_stat()
    {
        $this->setup();
        return $this->Push_Sent->find(
            "count",
            [
                'conditions' =>
                    [],
            ]
        );
    }

    public function getPushRecipients($push_id)
    {
        $this->setup();
        $recipients = $this->Push_Sent->find(
            "all",
            [
                'conditions' => [
                    'push_id' => $push_id,
                    'OR' => array(
                        array('status ' => "new"),
                        array('status ' => "error")
                    )
                ],
                'fields' => ['DISTINCT Push_Sent.user_id'],
            ]
        );
        $rs = [];
        foreach ($recipients as $recipient) {
            $us_id = $recipient['Push_Sent']['user_id'];
            if (!in_array($us_id, $rs)) {
                $rs[] = $us_id;
            }
        }
        return $rs;
    }

    public function clearOldPush()
    {
        $this->setup();
        $old_push_messages = $this->Push_Sent->find(
            "all",
            [
                'conditions' =>
                    [
                        //'Push_Sent.user_id' => $data['user_id'],
                        'DATE(NOW()) - DATE(created) >=' => 30,
                    ],
            ]
        );
        $deleted_count = count($old_push_messages);
        if ($deleted_count > 0) {
            foreach ($old_push_messages as $old_push_message) {
                $p_id = $old_push_message['Push_Sent']['id'];
                $this->Push_Sent->id = $p_id;
                $this->Push_Sent->delete();
            }
        }
        return "было удалено старых уведомлений - $deleted_count";
    }

    public function clearOldDevice()
    {
        $this->setup();
        $old_devices = $this->User_Device->find(
            "all",
            [
                'conditions' =>
                    [
                        'DATE(NOW()) - DATE(created) >=' => 30,
                    ],
            ]
        );
        $deleted_count = count($old_devices);
        if ($deleted_count > 0) {
            foreach ($old_devices as $old_device) {
                $p_id = $old_device['User_Device']['id'];
                $this->User_Device->id = $p_id;
                $this->User_Device->delete();
            }
        }
        return "было удалено устаревших токенов - $deleted_count";
    }

}