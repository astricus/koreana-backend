<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Страница
 */
class StaticComponent extends Component
{
    public $components         = [
        'Session',
        'Error',
        'Uploader',
        'Platform',
        'Admin',
        'Api',
    ];

    public $default_sort_field = "created";

    public $default_show_count = "2";

    public $api_methods        = [
        "get_pages"          => "pagesByApi",
        "get_page"           => "pageByApi",
        "get_categories"     => "categoriesByApi",
        "get_category_pages" => "pageByCategoryApi",
        "get_subcategory"    => "subcategoriesByApi",
    ];

    public $controller;

    public $error_text         = [
        'page_with_id_is_not_found'    => 'Cтраница с переданным идентификатором не найдеена',
        'category_id_is_not_set'       => 'Категория страницы не установлена',
        'category_id_is_not_found'     => 'Категория страницы не найдеена',
        'page_title_is_empty'          => 'Название страницы пусто',
        'page_title_is_too_short'      => 'Название страницы слишком короткое',
        'page_title_is_too_long'       => 'Название страницы слишком длинное',
        'page_url_is_empty'            => 'Url страницы пусто',
        'page_url_is_too_short'        => 'Url страницы слишком короткий',
        'page_url_is_too_long'         => 'Url страницы слишком длинный',
        'invalid_page_status'          => 'Неопределенный статус страницы',
        'webform_name_is_too_short'    => 'Название веб-формы слишком короткое',
        'webform_name_is_too_long'     => 'Название веб-формы слишком длинное',
        'webform_with_id_is_not_found' => 'Веб форма с указанным идентификатором не найдена',
    ];

    public $params             = [
        'min_title_length' => 2,
        'max_title_length' => 256,
        'min_url_length'   => 2,
        'max_url_length'   => 512,
    ];

    public $errors             = [];

    public $error_fields       = [];

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName             = "Static_Page_Log";
        $this->Static_Page_Log = ClassRegistry::init($modelName);
        $modelName             = "Static_Page";
        $this->Static_Page     = ClassRegistry::init($modelName);
        $modelName             = "Static_Category";
        $this->Static_Category = ClassRegistry::init($modelName);
        $modelName             = "Webform";
        $this->Webform         = ClassRegistry::init($modelName);
        $modelName             = "Webform_Link";
        $this->Webform_Link    = ClassRegistry::init($modelName);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    private function validateCategoryId($id)
    {
        $id = intval($id);
        if (!is_int($id)) {
            $this->errors["category_id"] = $this->error_text['category_id_is_not_set'];
            $this->error_fields[]        = "category_id";

            return false;
        }
        if ($this->getCategoryById($id) == null) {
            $this->errors["category_id"] = $this->error_text['category_id_is_not_found'];
            $this->error_fields[]        = "category_id";

            return false;
        }

        return true;
    }

    /**
     * @param $title
     *
     * @return bool
     */
    private function validatePageTitle($title)
    {
        if (empty($title)) {
            $this->errors["title"] = $this->error_text['page_title_is_empty'];
            $this->error_fields[]  = "title";

            return false;
        }
        if (mb_strlen($title) < $this->params['min_title_length']) {
            $this->errors["title"] = $this->error_text['page_title_is_too_short'];
            $this->error_fields[]  = "title";

            return false;
        }
        if (mb_strlen($title) > $this->params['max_title_length']) {
            $this->errors["title"] = $this->error_text['page_title_is_too_long'];
            $this->error_fields[]  = "title";

            return false;
        }

        return true;
    }

    /**
     * @param $title
     *
     * @return bool
     */
    private function validateCategoryName($title)
    {
        if (empty($title)) {
            $this->errors["title"] = $this->error_text['page_title_is_empty'];
            $this->error_fields[]  = "title";

            return false;
        }
        if (mb_strlen($title) < $this->params['min_title_length']) {
            $this->errors["category"] = $this->error_text['page_title_is_too_short'];
            $this->error_fields[]     = "category";

            return false;
        }
        if (mb_strlen($title) > $this->params['max_title_length']) {
            $this->errors["category"] = $this->error_text['page_title_is_too_long'];
            $this->error_fields[]     = "category";

            return false;
        }

        return true;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    private function validateWebformName($name)
    {
        if (empty($name)) {
            $this->errors["title"] = $this->error_text['page_title_is_empty'];
            $this->error_fields[]  = "title";

            return false;
        }
        if (mb_strlen($name) < $this->params['min_title_length']) {
            $this->errors["webform"] = $this->error_text['webform_name_is_too_short'];
            $this->error_fields[]    = "webform";

            return false;
        }
        if (mb_strlen($name) > $this->params['max_title_length']) {
            $this->errors["webform"] = $this->error_text['webform_name_is_too_long'];
            $this->error_fields[]    = "webform";

            return false;
        }

        return true;
    }

    /**
     * @param $url
     *
     * @return bool
     */
    private function validatePageUrl($url)
    {
        if (empty($url)) {
            $this->errors["url"]  = $this->errors['page_url_is_empty'];
            $this->error_fields[] = "url";

            return false;
        }
        if (mb_strlen($url) < $this->params['min_url_length']) {
            $this->errors["url"]  = $this->errors['page_url_is_too_short'];
            $this->error_fields[] = "url";

            return false;
        }
        if (mb_strlen($url) > $this->params['max_url_length']) {
            $this->errors["url"]  = $this->errors['page_url_is_too_long'];
            $this->error_fields[] = "url";

            return false;
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function checkPageExists($id)
    {
        $this->setup();

        return $this->Static_Page->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    private function setStatus($id, $status)
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->error_text['invalid_page_status']);

            return false;
        }
        $update_data           = ['enabled' => $status];
        $this->Static_Page->id = $id;
        if ($this->Static_Page->save($update_data)) {
            return true;
        }

        return false;
    }

    private function setStatus($id, $status)
    {
        if (!$this->validateStatus($status)) {
            $this->returnError("General", $this->error_text['invalid_page_status']);

            return false;
        }
        $update_data           = ['enabled' => $status];
        $this->Static_Page->id = $id;
        if ($this->Static_Page->save($update_data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $error_type
     * @param $error_text
     */
    private function returnError($error_type, $error_text)
    {
        die("ERROR!" . $error_type . " " . $error_text);
        //TODO передавать впоследстие ошибку в единый компонент ERROR
    }

    /**
     * @param $page_id
     * @param $action
     *
     * @return false
     */
    private function updateStaticPageLog($page_id, $action)
    {
        /*
         * action = create, delete, hide, show, update
         * */
        $page_log = [
            'page_id'   => $page_id,
            "author_id" => $this->Admin->manager_id(),
            "action"    => $action,
        ];
        $this->Static_Page_Log->create();
        if ($this->Static_Page_Log->save($page_log)) {
            return $this->Static_Page_Log->id;
        }

        return false;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function validateStaticFields($data)
    {
        return $this->Validator->validateData($this->fields, $data);
    }

    /**
     * @param $status
     *
     * @return bool
     */
    private function validateStatus($status)
    {
        if ($status !== 1 and $status !== 0) {
            return false;
        }

        return true;
    }

    //---- PUBLIC

    //---- PAGES

    /**
     * @param $id
     * @param $content
     * @param $category_id
     * @param $title
     * @param $url
     * @param $platforms
     * @param $meta_keywords
     * @param $meta_title
     * @param $meta_description
     * @param $meta_custom
     *
     * @return bool
     */
    public function updateStaticPage(
        $id,
        $content,
        $category_id,
        $title,
        $url,
        $platforms,
        $meta_keywords,
        $meta_title,
        $meta_description,
        $meta_custom
    ) {
        $this->setup();

        $this->validateCategoryId($category_id);
        $this->validatePageTitle($title);
        $this->validatePageUrl($url);

        if (count($this->errors) > 0) {
            return false;
        }

        $platforms             = $this->Platform->platformsToString($platforms);
        $data                  = [
            "content"          => $content,
            "category_id"      => $category_id,
            "title"            => $title,
            "meta-keywords"    => $meta_keywords,
            "meta-description" => $meta_description,
            "meta-title"       => $meta_title,
            "meta-custom"      => $meta_custom,
            "url"              => $url,
            "platform"         => $platforms,
        ];
        $this->Static_Page->id = $id;
        if ($this->Static_Page->save($data)) {
            $this->updateStaticPageLog($id, "update");

            return true;
        }

        return false;
    }

    /**
     * @param $content
     * @param $category_id
     * @param $title
     * @param $keywords
     * @param $url
     * @param $platforms
     *
     * @return bool
     */
    public function createStaticPage(
        $content,
        $category_id,
        $title,
        $url,
        $platforms,
        $meta_keywords,
        $meta_title,
        $meta_description,
        $meta_custom
    ) {
        $this->setup();
        $this->validateCategoryId($category_id);
        $this->validatePageTitle($title);
        $this->validatePageUrl($url);

        if (count($this->errors) > 0) {
            return false;
        }

        $platforms = $this->Platform->platformsToString($platforms);
        $data      = [
            "content"          => $content,
            "category_id"      => $category_id,
            "title"            => $title,
            "meta-keywords"    => $meta_keywords,
            "meta-description" => $meta_description,
            "meta-title"       => $meta_title,
            "meta-custom"      => $meta_custom,
            "url"              => $url,
            "platforms"        => $platforms,
        ];

        $this->Static_Page_Log->create();
        if ($this->Static_Page_Log->save($data)) {
            $page_id = $this->Static_Page_Log->id;
        }
        $this->updateStaticPageLog($page_id, "create");

        $this->Static_Page->create();
        if ($this->Static_Page->save($data)) {
            return $this->Static_Page->id;
        }

        return false;
    }

    /**
     * @param $platform
     * @param $category_id
     * @param $enabled
     *
     * @return mixed
     */
    public function totalPageCount($platform, $category_id, $enabled)
    {
        $this->setup();

        $filter = [];
        if ($platform != null) {
            $filter["platform"] = $platform;
        }
        if ($category_id != null) {
            $filter["category_id"] = $category_id;
        }
        if ($enabled != null) {
            $filter["enabled"] = $enabled;
        }

        return $this->Static_Page->find(
            "count",
            [
                'conditions' =>
                    [
                        $filter,
                    ],
            ]
        );
    }

    /**
     * @param $show_count
     * @param $limit_page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return mixed
     */
    public function pageList($show_count, $limit_page, $sort, $sort_dir, $filter)
    {

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $platform_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "platform") {
                $platform_filter = ['News.platforms LIKE' => "%$f_value%"];
            }
        }

        $city_id_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "category_id") {
                $city_id_filter = ['category_id' => $f_value];
            }
        }

        $find_filter = [];
        if (!empty($filter['find_by_name'])) {
            $string      = $filter['find_by_name'];
            $find_filter = ['title LIKE ' => "%$string%"];
        }

        $this->setup();
        $page_list = $this->Static_Page->find(
            "all",
            [
                'conditions' =>
                    [
                        $platform_filter,
                        $find_filter,
                        $city_id_filter,
                    ],
                'limit'      => $show_count,
                'offset'     => $limit_page,
                'order'      => [$sort . " " . $sort_dir],
                //'fields' => array('DISTINCT News.id, News.*')
            ]
        );

        return $page_list;

    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deletePage($id)
    {
        if (!$this->checkPageExists($id)) {
            $this->returnError("General", $this->errors['page_with_id_is_not_found']);

            return false;
        }
        $this->Static_Page->id = $id;
        $this->Static_Page->delete();

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockStatic($id)
    {
        $this->setup();
        if (!$this->checkPageExists($id)) {
            $this->returnError("General", $this->errors['page_with_id_is_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['disabled']);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockStatic($id)
    {
        $this->setup();
        if (!$this->checkPageExists($id)) {
            $this->returnError("General", $this->errors['page_with_id_is_not_found']);

            return false;
        }
        $this->setStatus($id, $this->statuses['enabled']);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getPageById($id)
    {
        $this->setup();

        return $this->Static_Page->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getPageLog($id)
    {
        $this->setup();

        return $this->Static_Page_Log->find(
            "all",
            [
                'conditions' =>
                    [
                        'page_id' => $id,
                    ],
                'order'      => ['id desc'],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockPage($id)
    {
        $this->setup();
        if (!$this->checkPageExists($id)) {
            $this->returnError("General", $this->error_text['page_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, 0);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockPage($id)
    {
        $this->setup();
        if (!$this->checkPageExists($id)) {
            $this->returnError("General", $this->error_text['news_with_id_not_found']);

            return false;
        }
        $this->setStatus($id, 1);
    }

    /**
     * @param $id
     * @param $params
     * @param $data
     *
     * @return bool|null
     */
    public function pagesByApi($id, $params, $data)
    {
        $sort = $data['sort'] ?? null;
        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        $count = $data['count'] ?? null;
        if (empty($count) or $count <= 1) {
            $count = $this->default_show_count;
        }

        $page = $data['page'] ?? null;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = (int)$count * ($page - 1);

        $sort_dir = $data['dir'] ?? null;
        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $platform           = $this->Api->platform;
        $platform_filter    = ['Static_Page.platform LIKE' => "%$platform%"];
        $category_id_filter = [];
        if (!empty($data['category_id'])) {
            $category_id_filter['category_id'] = $data['category_id'];
        }

        $this->setup();
        $page_list = $this->Static_Page->find(
            "all",
            [
                'conditions' =>
                    [
                        $platform_filter,
                        'enabled' => 1,
                        $category_id_filter,
                    ],
                'joins'      => [
                    [
                        'table'      => 'static_categories',
                        'alias'      => 'Static_Category',
                        'type'       => 'INNER',
                        'conditions' => [
                            'Static_Page.category_id = Static_Category.id',
                        ],
                    ],
                ],
                'limit'      => $count,
                'offset'     => $limit_page,
                'order'      => ['Static_Page.' . $sort . " " . $sort_dir],
                'fields'     => [
                    'Static_Category.*',
                    'Static_Page.content',
                    'Static_Page.url',
                    'Static_Page.id',
                    'Static_Page.title',
                    'Static_Page.meta-title',
                    'Static_Page.meta-description',
                    'Static_Page.meta-keywords',
                    'Static_Page.meta-custom',
                ],
            ]
        );

        $st_arr = [];
        foreach ($page_list as $page_item) {
            $arr             = [];
            $arr['page']     = $page_item['Static_Page'];
            $arr['category'] = [
                'name' => $page_item['Static_Category']['name'],
                'id'   => $page_item['Static_Category']['id'],
            ];
            $st_arr[]        = $arr;
        }

        if (count($st_arr) > 0) {
            $this->Api->response_api($st_arr, "success");
            exit;
        }
        $this->Api->response_api(null, "error");
        exit;

    }

    /**
     * @param $id
     * @param $params
     * @param $data
     */
    public function pageByApi($id, $params, $data)
    {
        if (empty($id) or intval($id) <= 0) {
            $this->Api->response_api("Некорректный идентификатор статичной страницы " . $id, "error");
            exit;
        }

        $platform        = $this->Api->platform;
        $platform_filter = ['Static_Page.platform LIKE' => "%$platform%"];

        $this->setup();
        $st_page = $this->Static_Page->find(
            "first",
            [
                'conditions' =>
                    [
                        'Static_Page.id' => $id,
                        $platform_filter,
                        'enabled'        => 1,
                    ],
                'joins'      => [
                    [
                        'table'      => 'static_categories',
                        'alias'      => 'Static_Category',
                        'type'       => 'INNER',
                        'conditions' => [
                            'Static_Page.category_id = Static_Category.id',
                        ],
                    ],
                ],
                'fields'     => [
                    'Static_Category.id',
                    'Static_Category.name',
                    'Static_Page.content',
                    'Static_Page.id',
                    'Static_Page.title',
                    'Static_Page.meta-title',
                    'Static_Page.meta-description',
                    'Static_Page.meta-keywords',
                    'Static_Page.meta-custom',
                    'Static_Page.url',
                ],
            ]
        );
        if (count($st_page) > 0) {
            $arr             = [];
            $arr['page']     = $st_page['Static_Page'];
            $arr['category'] = [
                'name' => $st_page['Static_Category']['name'],
                'id'   => $st_page['Static_Category']['id'],
            ];
            $this->Api->response_api($arr, "success");
            exit;
        } else {
            $this->Api->response_api(null, "error");
            exit;
        }

    }

    public function pageByCategoryApi($id, $params, $data)
    {
        $sort = $data['sort'] ?? null;
        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        $count = $data['count'] ?? null;
        if (empty($count) or $count <= 1) {
            $count = $this->default_show_count;
        }

        $page = $data['page'] ?? null;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) {
            $page = 1;
        }
        $limit_page = (int)$count * ($page - 1);

        $sort_dir = $data['dir'] ?? null;
        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        if (empty($id) or intval($id) <= 0) {
            $this->Api->response_api("Некорректный идентификатор категории страниц " . $id, "error");
            exit;
        }

        $platform        = $this->Api->platform;
        $platform_filter = ['Static_Page.platform LIKE' => "%$platform%"];

        $this->setup();
        $page_list = $this->Static_Page->find(
            "all",
            [
                'conditions' => [
                    $platform_filter,
                    'enabled'     => 1,
                    'category_id' => $id,
                ],
                'limit'      => $count,
                'offset'     => $limit_page,
                'order'      => ['Static_Page.' . $sort . " " . $sort_dir],
                'fields'     => [
                    'Static_Page.content',
                    'Static_Page.url',
                    'Static_Page.id',
                    'Static_Page.title',
                    'Static_Page.meta-title',
                    'Static_Page.meta-description',
                    'Static_Page.meta-keywords',
                    'Static_Page.meta-custom',
                ],
            ]
        );

        $st_arr = [];
        if (count($page_list) > 0) {
            foreach ($page_list as $page_item) {
                $st_arr[] = $page_item['Static_Page'];
            }
        }
        if (count($st_arr) > 0) {
            $this->Api->response_api($st_arr, "success");
            exit;
        } else {
            $this->Api->response_api(null, "error");
            exit;
        }

    }

    //---- category

    /**
     * @return mixed
     */
    public function categoryList()
    {
        $this->setup();

        return $this->Static_Category->find(
            "all",
            [
                'conditions' =>
                    [//$car_id_filter,
                    ],
                'order'      => ["id DESC "],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getCategoryById($id)
    {
        $this->setup();

        return $this->Static_Category->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getSubCategories($id)
    {
        return $this->Static_Category->find(
            "all",
            [
                'conditions' =>
                    [
                        'parent_id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $name
     * @param $parent_id
     *
     * @return false
     */
    public function createCategory($name, $parent_id)
    {
        $this->setup();
        if ($parent_id > 0) {
            $this->validateCategoryId($parent_id);
        }

        if (!$this->validateCategoryName($name)) {
            return false;
        }

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "parent_id" => $parent_id,
            "name"      => $name,
        ];

        $this->Static_Category->create();
        if ($this->Static_Category->save($data)) {
            return $this->Static_Category->id;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getPagesByCategory($id)
    {
        return $this->Static_Page->find(
            "all",
            [
                'conditions' =>
                    [
                        'category_id' => $id,
                    ],
                'order'      => ['id DESC'],
            ]
        );
    }

    /**
     * @param $id
     * @param $parent_id
     *
     * @return bool
     */
    public function removeCategory($id, $parent_id)
    {
        $data = [
            "parent_id" => $parent_id,
        ];

        $this->setup();
        $this->Static_Category->id = $id;
        if ($this->Static_Category->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $name
     *
     * @return bool
     */
    public function renameCategory($id, $name)
    {
        $validate = $this->validateCategoryName($name);
        if (!$validate) {
            return false;
        }
        $data = [
            "name" => $name,
        ];
        $this->setup();

        $this->Static_Category->id = $id;
        if ($this->Static_Category->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $params
     * @param $data
     */
    public function categoriesByApi($id, $params, $data)
    {
        $this->setup();
        $cats     = $this->Static_Category->find(
            "all",
            [
                'conditions' =>
                    [],
                'order'      => ["id DESC "],
                'fields'     => ['id', 'name', 'parent_id'],
            ]
        );
        $cats_arr = [];
        foreach ($cats as $cat) {
            $cats_arr[] = $cat['Static_Category'];
        }
        if (count($cats_arr) > 0) {
            $this->Api->response_api($cats_arr, "success");
            exit;
        }
        $this->Api->response_api(null, "error");
        exit;

    }

    /**
     * @param $id
     * @param $params
     * @param $data
     */
    public function subcategoriesByApi($id, $params, $data)
    {
        if (empty($id) or intval($id) <= 0) {
            $this->Api->response_api("Некорректный идентификатор категории страниц " . $id, "error");
            exit;
        }
        $this->setup();
        $cats_arr = $this->categoryTree($id);
        if (count($cats_arr) > 0) {
            $this->Api->response_api($cats_arr, "success");
            exit;
        }
        $this->Api->response_api(null, "error");
        exit;

    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function getCategoryPageCount($id)
    {
        return $this->Static_Page->find(
            "count",
            [
                'conditions' =>
                    [
                        'category_id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function categoryTree($id)
    {
        $this->setup();
        $max_steps = 10;
        $parents   = [$id];
        $tree      = [];
        for ($step = 0; $step < $max_steps; $step++) {
            $cat_step = $this->Static_Category->find(
                "all",
                [
                    'conditions' =>
                        [
                            'parent_id' => $parents,
                        ],
                    'order'      => ["id DESC "],
                ]
            );
            if (count($cat_step) > 0) {
                $parents = [];
                foreach ($cat_step as $cat_step_item) {
                    $cat = $cat_step_item['Static_Category'];
                    if (!in_array($cat['id'], $parents)) {
                        $parents[] = $cat['id'];
                    }
                    if (!in_array($cat['id'], $tree)) {
                        $tree[$cat['id']] = [
                            'name'      => $cat['name'],
                            'parent_id' => $cat['parent_id'],
                            'children'  => [],
                            'id'        => $cat['id'],
                            'step'      => $step,
                            'pages'     => $this->getCategoryPageCount($cat['id']),
                        ];
                    }
                    if (key_exists($cat['parent_id'], $tree) &&
                        !in_array($cat['id'], $tree[$cat['parent_id']]['children'])
                    ) {
                        $tree[$cat['parent_id']]['children'][] = $cat['id'];
                    }

                }
            } else {
                ksort($tree);

                return $tree;
            }

        }

        return $tree;

    }

    //---- CUSTOM

    /**
     * @param $id
     *
     * @return string
     */
    public function getAuthorByAuthorId($id)
    {
        $author = $this->Admin->getManagerById($id);

        return prepare_fio($author['firstname'], $author['lastname'], "");
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function getStaticPageAuthor($id)
    {
        $this->setup();
        $log_action = $this->Static_Page_Log->find(
            "first",
            [
                'conditions' =>
                    [
                        'page_id' => $id,
                        'action'  => "create",
                    ],
            ]
        );
        if ($log_action == null) {
            return null;
        }
        $author_id = $log_action['Static_Page_Log']['author_id'];
        $author    = $this->Admin->getManagerById($author_id);

        return prepare_fio($author['firstname'], $author['lastname'], $author['middlename'] ?? null);
    }

    //-------- WEB FORMS

    /**
     * @param $id
     * @param $content
     * @param $name
     *
     * @return bool
     */
    public function updateWebform($id, $content, $name, $utm)
    {
        $this->setup();
        //$this->validateWebformId($id);
        $this->validateWebformName($name);

        if (count($this->errors) > 0) {
            return false;
        }

        $data              = [
            "content" => $content,
            "name"    => $name,
            "utm"     => $utm,
        ];
        $this->Webform->id = $id;
        if ($this->Webform->save($data)) {
            return true;
        }

        return false;
    }

    /**
     * @param $content
     * @param $name
     * @param $utm
     *
     * @return bool
     */
    public function createWebform($content, $name, $utm)
    {
        $this->setup();
        $this->validateWebformName($name);

        if (count($this->errors) > 0) {
            return false;
        }

        $data = [
            "content" => $content,
            "name"    => $name,
            "utm"     => $utm,
        ];

        $this->Webform->create();
        if ($this->Webform->save($data)) {
            return $this->Webform->id;
        }

        return false;
    }

    /**
     * @param $show_count
     * @param $limit_page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     *
     * @return mixed
     */
    public function webformList($show_count, $limit_page, $sort, $sort_dir, $filter)
    {

        if ($sort !== "id" && $sort !== "created") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $enabled_filter = [];
        foreach ($filter as $f_key => $f_value) {
            if ($f_key == "enabled") {
                $enabled_filter = ['enabled' => $f_value];
            }
        }

        $find_filter = [];
        if (!empty($filter['find_by_name'])) {
            $string      = $filter['find_by_name'];
            $find_filter = ['name LIKE ' => "%$string%"];
        }

        $this->setup();
        $webform_list = $this->Webform->find(
            "all",
            [
                'conditions' =>
                    [
                        $find_filter,
                        $enabled_filter,
                    ],
                'limit'      => $show_count,
                'offset'     => $limit_page,
                'order'      => [$sort . " " . $sort_dir],
            ]
        );

        return $webform_list;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function checkWebformExists($id)
    {
        $this->setup();

        return $this->Webform->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteWebform($id)
    {
        if (!$this->checkWebformExists($id)) {
            $this->returnError("General", $this->errors['webform_with_id_is_not_found']);

            return false;
        }
        $this->Webform->id = $id;
        $this->Webform->delete();

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function blockWebform($id)
    {
        $this->setup();
        if (!$this->checkWebformExists($id)) {
            $this->returnError("General", $this->errors['webform_with_id_is_not_found']);

            return false;
        }
        $this->setWebformStatus($id, 0);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function unblockWebform($id)
    {
        $this->setup();
        if (!$this->checkWebformExists($id)) {
            $this->returnError("General", $this->errors['webform_with_id_is_not_found']);

            return false;
        }
        $this->setWebformStatus($id, 1);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getConnectedWFPage($id)
    {
        $this->setup();

        return $this->Webform_Link->find(
            "all",
            [
                'conditions' =>
                    [
                        'webform_id'   => $id,
                        'connect_type' => 'connect_page',
                    ],
                'order'      => ["id DESC"],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getConnectedWFCategory($id)
    {
        $this->setup();

        return $this->Webform_Link->find(
            "all",
            [
                'conditions' =>
                    [
                        'webform_id'   => $id,
                        'connect_type' => 'connect_category',
                    ],
                'order'      => ["id DESC"],
            ]
        );

    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getDisconnectedWFPage($id)
    {
        $this->setup();

        return $this->Webform_Link->find(
            "all",
            [
                'conditions' =>
                    [
                        'webform_id'   => $id,
                        'connect_type' => 'disconnect_page',
                    ],
                'order'      => ["id DESC"],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getDisConnectedWFCategory($id)
    {
        $this->setup();

        return $this->Webform_Link->find(
            "all",
            [
                'conditions' =>
                    [
                        'webform_id'   => $id,
                        'connect_type' => 'disconnect_category',
                    ],
                'order'      => ["id DESC"],
            ]
        );
    }

    /**
     * @param $webform_id
     * @param $connect_id
     * @param $connect_type
     *
     * @return false
     */
    public function connectWF($webform_id, $connect_id, $connect_type)
    {
        $this->setup();

        $data = [
            "webform_id"   => $webform_id,
            "connect_id"   => $connect_id,
            'connect_type' => $connect_type,
        ];

        $this->Webform_Link->create();
        if ($this->Webform_Link->save($data)) {
            return $this->Webform_Link->id;
        }

        return false;
    }

    /**
     * @param $webform_id
     * @param $connect_id
     * @param $connect_type
     *
     * @return mixed
     */
    public function getWebformConnectId($webform_id, $connect_id, $connect_type)
    {
        $this->setup();

        return $this->Webform_Link->find(
            "first",
            [
                'conditions' =>
                    [
                        'webform_id'   => $webform_id,
                        'connect_id'   => $connect_id,
                        'connect_type' => $connect_type,
                    ],
            ]
        );
    }

    /**
     * @param $webform_id
     * @param $connect_id
     * @param $connect_type
     *
     * @return bool
     */
    public function removeConnectWF($webform_id, $connect_id, $connect_type)
    {
        if (!$this->checkWebformExists($webform_id)) {
            $this->returnError("General", $this->errors['webform_with_id_is_not_found']);

            return false;
        }
        $connect = $this->getWebformConnectId($webform_id, $connect_id, $connect_type);
        if ($connect != null) {
            $id                     = $connect['Webform_Link']['id'];
            $this->Webform_Link->id = $id;
            $this->Webform_Link->delete();

            return true;
        }

        return false;
    }

    //список всех страниц - нужен для привязки веб-форм
    public function pageNameList()
    {
        $this->setup();

        return $this->Static_Page->find(
            "all",
            [
                'conditions' =>
                    [
                        'enabled' => 1,
                    ],
                'order'      => ["id DESC"],
                'fields'     => ["id", "title"],
            ]
        );
    }

}