<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Город
 */
class CityComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "City";
        $this->City = ClassRegistry::init($modelName);
        $modelName = "City_News";
        $this->City_News = ClassRegistry::init($modelName);
        $modelName = "City_Action";
        $this->City_Action = ClassRegistry::init($modelName);
        $modelName = "Service_City";
        $this->Service_City = ClassRegistry::init($modelName);
    }

    public function getCityList()
    {
        $this->setup();
        $cities = $this->City->find("all",
            array('conditions' =>
                array(
                    //'region_id' => $id
                ),
                'order' => array("name ASC")
            )
        );
        return $cities;
    }

    public function getCityMock()
    {
        $city_msk = 5;
        $city_spb = 1;
        $this->setup();
        $cities = $this->City->find("all",
            array('conditions' =>
                array(
                    'OR' => array(array('id' => $city_msk), array('id' => $city_spb))
                ),
                'order' => array("name ASC")
            )
        );
        return $cities;
    }

    /**
     * @param $news_id
     * @return mixed
     */
    public function getCityByNews($news_id)
    {
        $this->setup();
        $cities = $this->City_News->find("all",
            array('conditions' =>
                array(
                    'news_id' => $news_id
                ),
                'joins' => array(
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'City_News.city_id = City.id'
                        )
                    ),
                ),
                'fields' => array('DISTINCT City_News.city_id, City.*'),
                'order' => array("id DESC")
            )
        );
        return $cities;
    }

    public function getCityByAction($action_id)
    {
        $this->setup();
        $cities = $this->City_Action->find("all",
            array('conditions' =>
                array(
                    'action_id' => $action_id
                ),
                'joins' => array(
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'City_Action.city_id = City.id'
                        )
                    ),
                ),
                'fields' => array('DISTINCT City_Action.city_id, City.*'),
                'order' => array("id DESC")
            )
        );
        return $cities;
    }

    /**
     * @param $service_id
     * @return array
     */
    public function getCityByService($service_id)
    {
        $this->setup();
        $cities = $this->Service_City->find("all",
            array('conditions' =>
                array(
                    'service_id' => $service_id
                ),
                'joins' => array(
                    array(
                        'table' => 'cities',
                        'alias' => 'City',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Service_City.city_id = City.id'
                        )
                    ),
                ),
                'fields' => array('DISTINCT Service_City.city_id, City.*'),
                'order' => array("id DESC")
            )
        );
        return $cities;
    }

    /**
     * @param $news_id
     * @return mixed
     */
    public function getCityIdByNewId($news_id)
    {
        $this->setup();
        $new_cities = $this->City_News->find("all",
            array('conditions' =>
                array(
                    'news_id' => $news_id
                ),
                'order' => array('city_id ASC')
            )
        );
        $arr = [];
        foreach ($new_cities as $new_city){
            $arr[] = $new_city['City_News']['city_id'];
        }
        return $arr;
    }

    /**
     * @param $action_id
     * @return array
     */
    public function getCityIdByActionId($action_id)
    {
        $this->setup();
        $cities = $this->City_Action->find("all",
            array('conditions' =>
                array(
                    'action_id' => $action_id
                ),
                'order' => array('city_id ASC')
            )
        );
        $arr = [];
        foreach ($cities as $city){
            $arr[] = $city['City_Action']['city_id'];
        }
        return $arr;
    }

    /**
     * @param $service_id
     * @return array
     */
    public function getCityIdByServiceId($service_id)
    {
        $this->setup();
        $cities = $this->Service_City->find("all",
            array('conditions' =>
                array(
                    'service_id' => $service_id
                ),
                'order' => array('city_id ASC')
            )
        );
        $arr = [];
        foreach ($cities as $city){
            $arr[] = $city['Service_City']['city_id'];
        }
        return $arr;
    }

    /**
     * @param $news_id
     * @param $cities
     */
    public function updateCityByNews($news_id, $cities)
    {
        $this->setup();
        $new_cities = $this->getCityIdByNewId($news_id);
        if(count($cities)>0){
            asort($cities);
        }

        if($cities == $new_cities) return;
        //старый список
        foreach ($new_cities as $elem){
            if(!in_array($elem, $cities)){
                // удаление города из списка
                $this->City_News->deleteAll(['city_id' => $elem, 'news_id' => $news_id]);
            }
        }
        //новый список
        if(count($cities)>0) {
            foreach ($cities as $elem) {
                if (!in_array($elem, $new_cities)) {
                    $data = [
                        'news_id' => $news_id,
                        'city_id' => $elem
                    ];
                    $this->City_News->create();
                    $this->City_News->save($data);
                }
            }
        }
    }

    /**
     * @param $action_id
     * @param $cities
     * @return bool|void
     * @throws Exception
     */
    public function updateCityByAction($action_id, $cities)
    {
        $this->setup();
        $action_cities = $this->getCityIdByActionId($action_id);
        if($action_cities == null && $cities == null) {
            return true;
        }
        if(count($cities)>0){
            asort($cities);
        }

        if($cities == $action_cities) return;
        //старый список
        foreach ($action_cities as $elem){
            if(!in_array($elem, $cities)){
                // удаление города из списка
                $this->City_Action->deleteAll(['city_id' => $elem, 'action_id' => $action_id]);
            }
        }
        //новый список
        if(count($cities)>0) {
            foreach ($cities as $elem) {
                if(!intval($elem)){
                    continue;
                }
                if (!in_array($elem, $action_cities)) {
                    $data = [
                        'action_id' => $action_id,
                        'city_id' => $elem
                    ];
                    $this->City_Action->create();
                    $this->City_Action->save($data);
                }
            }
        }
        return true;
    }

    /**
     * @param $service_id
     * @param $cities
     * @return bool|void
     * @throws Exception
     */
    public function updateCityByService($service_id, $cities)
    {
        $this->setup();
        $service_cities = $this->getCityIdByServiceId($service_id);
        if($service_cities == null && $cities == null) {
            return true;
        }
        if(count($cities)>0){
            asort($cities);
        }

        if($cities == $service_cities) return;
        //старый список
        foreach ($service_cities as $elem){
            if(!in_array($elem, $cities)){
                // удаление города из списка
                $this->Service_City->deleteAll(['city_id' => $elem, 'service_id' => $service_id]);
            }
        }
        //новый список
        if(count($cities)>0) {
            foreach ($cities as $elem) {
                if(!intval($elem)){
                    continue;
                }
                if (!in_array($elem, $service_cities)) {
                    $data = [
                        'service_id' => $service_id,
                        'city_id' => $elem
                    ];
                    $this->Service_City->create();
                    $this->Service_City->save($data);
                }
            }
        }
        return true;
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getCityNameById($id)
    {
        $this->setup();
        $city = $this->City->find("first",
            array('conditions' =>
                array(
                    'id' => $id
                )
            )
        );
        if(count($city)>0){
            return $city['City']['name'];
        }
        return null;
    }

    /**
     * @param $name
     * @return int
     */
    public function getCityIdByName($name)
    {
        $this->setup();
        $city = $this->City->find("first",
            array('conditions' =>
                array(
                    'name' => $name
                )
            )
        );
        return $city['City']['id'] ?? 0;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function createCity($name)
    {
        $this->setup();
        $this->City->create();
        $this->City->save(['name' => $name]);
        return $this->City->id;
    }




    /** Сервис Геокодирования
     * @param $address
     * @return array
     */
    public function getCoordByAddress($address){
        $address = urlencode($address);
        $url = "http://geocode-maps.yandex.ru/1.x/?geocode={$address}";
        $content = file_get_contents($url);
        preg_match("/<pos>(.*?)<\/pos>/", $content, $point);
        //$coords = str_replace(' ', ', ', trim(strip_tags($point[1])));
        if(!empty($point[1])) {
            return explode(" ", $point[1]);
        }
        return null;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCitiesByRegionId($id)
    {
        $this->setup();
        $cities = $this->City->find("all",
            array('conditions' =>
                array(
                    'region_id' => $id
                )
            )
        );
        return $cities;
    }

}