<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Допуск к функционалу и данным
 */
class AccessComponent extends Component
{
    public $components = array(
        'Admin',
        'Session',
        'Error',
        'Flash'
    );

    public $access_list = [
        'god_mode' => 'Глобальные настройки приложения',
        'backups' => 'работа с бекапами',
        'banners' => 'размещение и удаление баннеров',
        'all_contents' => 'неограниченный доступ для работы с контентом сайта',
        'navigation' => 'редактирование разделов и подразделов меню сайта',
        'web_form_stat' => 'доступ к статистике веб-форм',
        'clients' => 'редактирование клиентской базы',
        'email_sends' => 'осуществление е-mail рассылок по клиентской базе средствами сайта',
        'files' => 'возможность размещения и редактирования файлов в корневом каталоге',
        'create_new_pages' => 'создание новых страниц сайта',
        'meta_tags' => 'изменения мета-тегов страниц',
        'news' => 'управление новостями и объявлениями',
        'actions' => 'управление акциями',
        'web_forms' => 'управление веб-формами',
        'contacts' => 'управление контактными данными',
        'vacancies' => 'управление вакансиями',
        'admins' => 'управление аккаунтами администраторов',
    ];

    public $roles = [
        'super_admin' => 'супер администратор',
        'admin' => 'администратор',
        'super_editor' => 'супер редактор',
        'editor' => 'редактор',
        'user' => 'пользователь',
    ];

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function setup()
    {
        $modelName = "Access";
        $this->Access = ClassRegistry::init($modelName);
        $modelName = "Access_Role";
        $this->Access_Role = ClassRegistry::init($modelName);
    }

    /**
     * @param $functional
     * @return bool
     */
    public function checkControlAccess($functional)
    {
        $role = $this->Admin->manager_data()['Manager']['role'];
        $list = $this->getAccessListByRole($role);
        $functional_title = $this->getAccessTitleByName($functional);
        if (!in_array($functional, $list)) {
            return $this->messageAccessError($role, $functional_title);
        }
    }

    /**
     * @param $role
     * @param $functional
     * @return mixed
     */
    public function messageAccessError($role, $functional)
    {
        return $this->controller->redirect("/access_forbidden?functional=$functional&role=$role");
    }

    /**
     * @param $role
     * @return array
     */
    public function getAccessListByRole($role)
    {
        if (!in_array($role, array_keys($this->roles))) {
            return null;
        }
        $this->setup();
        $access_list = $this->Access_Role->find('all',
            array(
                'conditions' => array(
                    'role' => $role,
                    'status' => 1
                )
            )
        );
        $list = [];
        foreach ($access_list as $access_list_item) {
            $list[] = $access_list_item['Access_Role']['name'];
        }
        return $list;
    }

    /** Стартовая запись полей доступа в БД
     * @return bool
     */
    public function preProcessAccessList()
    {
        $this->setup();
        $access_list = $this->getAccessList();
        foreach (array_keys($this->roles) as $role) {
            foreach ($this->access_list as $name => $title) {
                $update_arr = ['name' => $name, 'role' => $role, 'status' => 0];
                $this->Access_Role->create();
                $this->Access_Role->save($update_arr);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getAccessList()
    {
        $this->setup();
        $access_list = $this->Access->find('all',
            array(
                'conditions' => array(//'id' => $user_id
                )
            )
        );
        return $access_list;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getAccessTitleByName($name)
    {
        $this->setup();
        $access_list = $this->Access->find('first',
            array(
                'conditions' => array(
                    'name' => $name
                )
            )
        );
        return $access_list['Access']['title'];
    }

    public function getAccessListByName()
    {
        $this->setup();
        $access_list = $this->Access->find('all',
            array(
                'conditions' => array(//'id' => $user_id
                )
            )
        );
        $access_list_arr = [];
        foreach ($access_list as $list_item) {
            $name = $list_item['Access']['name'];
            $title = $list_item['Access']['title'];
            $access_list_arr[$name] = $title;
        }
        return $access_list_arr;
    }

    /**
     * @param $role
     * @return mixed
     */
    public function getAccessByRole($role)
    {
        $this->setup();
        $access_list = $this->Access_Role->find('all',
            array(
                'conditions' => array(
                    'role' => $role
                ),
                'joins' => array(
                    array(
                        'table' => 'access',
                        'alias' => 'Access',
                        'type' => 'INNER',
                        'conditions' => array(
                            'Access_Role.name = Access.name'
                        )
                    ),
                ),
                'fields' => array(
                    'Access_Role.*',
                    'Access.*',
                ),
            )
        );
        return $access_list;
    }

    /**
     * @param $name
     * @param $role
     * @return mixed
     */
    public
    function getAccessByNameAndRole($name, $role)
    {
        $this->setup();
        $access_list = $this->Access_Role->find('all',
            array(
                'conditions' => array(
                    'role' => $role,
                    'name' => $name
                ),
                'fields' => array(
                    'Access_Role.status',
                ),
            )
        );
        return $access_list;
    }

    /**
     * @param $role
     * @param $name
     * @return int
     */
    public
    function checkAccessByRole($role, $name)
    {
        $this->setup();
        $check = $this->Access_Role->find("first",
            array('conditions' =>
                array(
                    'name' => $name,
                    'role' => $role
                )
            )
        );
        if ($check == null) {
            return 0;
        }
        return $check['Access_Role']['value'];
    }

    /**
     * @param $access_name
     * @param $role
     * @return mixed
     */
    private
    function checkAccessRoleExists($access_name, $role)
    {
        $this->setup();
        $check = $this->Access_Role->find("first",
            array('conditions' =>
                array(
                    'name' => $access_name,
                    'role' => $role
                )
            )
        );
        if ($check == null) {
            return 0;
        }
        return $check['Access_Role']['id'];
    }

    /**
     * @param $data
     * @return bool
     */
    public function updateAccessList($data)
    {
        $this->setup();
        if (count($data) > 0) {
            foreach ($data as $value) {
                $access_name = $value['name'];
                $role = $value['role'];
                $status = $value['status'];
                $check_id = $this->checkAccessRoleExists($access_name, $role);
                if ($check_id == 0) {
                    $this->Access_Role->create();
                    $check_id = $this->Access_Role->id;
                } else {
                    $this->Access_Role->id = $check_id;
                }
                $update_arr = ['name' => $access_name, 'role' => $role, 'status' => $status];
                $this->Access_Role->id = $check_id;
                $this->Access_Role->save($update_arr);
            }
            return true;
        }
        return false;
    }

}