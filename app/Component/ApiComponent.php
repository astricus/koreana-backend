<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент APi
 */
class ApiComponent extends Component
{
    public $components = [
        "Account",
        "Action",
        "Car",
        'Error',
        'Http',
        "Log",
        'Location',
        'Mock',
        "New",
        'Session',
        'Service',
        "Sto",
        'Static',
        "User",
        "SiteData",
        "Push",
    ];

    public $blocked_ips;

    /** @var string[] IP которые не блокировать - ip адреса тестировщиков */
    public $notBlockIps = [
        '37.18.255.125',
        '84.52.110.169',
        '94.19.94.9',
        '171.25.164.99',
        '10.157.13.26',
        '84.204.148.116',
        '84.52.97.225',
        '95.161.154.218',
        '10.180.201.225',
        '84.52.116.131',
        '79.134.197.198',
        '84.52.115.158',
        '195.177.121.50',
        '84.52.100.123',
        '95.161.160.194',
        '109.167.221.238',
        '84.52.83.22',
        '84.52.83.29',
        '84.52.92.75',
        '79.174.189.246',
        '81.23.125.102',
        '83.243.67.119',
        '81.23.111.82',
        '84.52.84.103',
        '91.215.226.54',
        '62.141.122.130',
        '5.17.1.232',
        '10.146.4.89',
        '185.202.215.162',
        '46.175.215.222',
        '62.118.138.175',
        '94.230.164.243',
        '37.18.255.125',
        '192.192.168.99'
    ];

    public $block_rules = [
        ['name' => 'minute_limit', 'block_period' => 3600, 'limit' => 120, 'check_limit' => 60],
        ['name' => 'hour_limit', 'block_period' => 360000, 'limit' => 1000, 'check_limit' => 3600]
    ];

    public $METHOD_NOT_FOUND_ERROR = "Метод API не существует";

    public $METHOD_NOT_FOUND_IN_PROJECT_ERROR = "Метод API не найден в данном проекте";

    public $API_URL_REQUIRED = "Требуется непустой url для API запроса";

    public $API_NAME_REQUIRED = "Требуется непустое название для API запроса";

    public $API_RESPONSE_DATA_REQUIRED = "Требуется заполнить ответ API запроса";

    const FALSE_TOKEN_MESSAGE = "Ошибка! Api token не найден или истек срок действия. Требуется авторизоваться в API";

    const NO_TOKEN_MESSAGE = "Ошибка! Требуется Api token, авторизуйтесь и предоставьте актуальный token";

    const TOO_MATCH_TOKEN_MESSAGE = "Ошибка! Вы слишком часто авторизуетесь в системе! Попробуйте делать это реже.";

    public $API_RESPONSE_DATA_NOT_FOUND = "Данные не найдены";

    public $count_per_second_token = "3";

    public $count_per_hour_token = "30";

    public $controller;

    public $entity, $id, $method, $token, $data, $params, $result;

    public function user_id()
    {
        return $this->Session->read('manager_id');
    }

    public function is_API()
    {
        if ($this->_Collection->getController()->name == "Api") {
            return true;
        }

        return false;
    }

    public $start_time;

    public $default_sort_field = "created";

    public function auth_requires($token)
    {
        if (is_null($token)) {
            $this->response_api(["error" => $this::NO_TOKEN_MESSAGE], "error", null, null);
            exit;
        }
        if (!$this->checkToken($token)) {
            $this->saveApiRequest(
                "unknown",
                "unknown",
                "unknown",
                "unknown",
                "unknown",
                $token,
                "unknown",
                "unknown",
                "unknown",
                $this::FALSE_TOKEN_MESSAGE
            );
            header("HTTP/1.0 419 Authentication Timeout");
            $this->response_api(["error" => $this::FALSE_TOKEN_MESSAGE], "error", null, null);
            exit;
        }
    }

    /**
     * @param $token
     */
    public function preToken($token)
    {
        $this->token = $token;
    }

    /**
     * @param $api_id
     *
     * @return array|null
     */
    private function getApiMethodById($api_id)
    {
        $modelName = "Api_Method";
        $this->Api_Method = ClassRegistry::init($modelName);
        $method = $this->Api_Method->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $api_id,
                    ],
            ]
        );

        if (count($method) == 0) {
            return null;
        }

        return $method;
    }

    /**
     * @param $component
     *
     * @return string
     */
    public function getApiComponent($component)
    {
        $component_name = ucfirst(strtolower($component));

        return $component_name;
    }

    /**
     * @param $api_component
     *
     * @return mixed
     */
    public function getApiComponentActiveMethods($api_component)
    {
        $api_component = $this->getApiComponent($api_component);

        return $this->$api_component->method_list;
    }

    /**
     * @param $component
     * @param $method_type
     *
     * @return bool
     */
    public function checkApiMethodTypeExists($component, $method_type)
    {
        $api_component = $this->getApiComponent($component);
        $api_class = $this->{$api_component};
        if (!in_array($method_type, array_keys($api_class->api_methods))) {
            return false;

        }
        $real_class_method = $api_class->api_methods[$method_type];
        if (!method_exists($this->$api_component, $real_class_method)) {
            return false;
        }

        return $real_class_method;
    }

    /**
     * @param $error
     * @param $http_error_code
     * @param $platform
     * @param $version_api
     */
    public function apiErrorReturn($error, $http_error_code, $platform, $version_api)
    {
        if ($http_error_code == "500") {
            header("HTTP/1.0 500 SERVER API ERROR!");
        } else {
            if ($http_error_code == "400") {
                header("HTTP/1.0 400 Not Found");
            }
        }
        $error_str = "API ERROR RESPONSE: " . $error;
        $this->saveApiRequest(
            $version_api,
            $platform,
            $this->entity,
            $this->id,
            $this->method,
            $this->token,
            $this->data,
            $this->params,
            $this->result,
            $error_str
        );
        $this->response_api(["error" => $error_str], "error", $platform, $version_api);
        exit;
    }

    /**
     * @param $build
     * @return void
     */
    public function checkBuildVersion($build)
    {
        $modelName = "Setting";
        $this->Setting = ClassRegistry::init($modelName);
        $settings = $this->Setting->find(
            "first",
            [
                'conditions' =>
                    [
                        'name' => 'min_build_version',
                    ],
            ]
        );
        $min_build_version = 0;
        if (count($settings) > 0) {
            $min_build_version = $settings['Setting']['value'];
        }
        if ($build < $min_build_version) {
            $server_time = date("Y-m-d H:i:s");
            $api_name = Configure::read('API_NAME');
            $data = [
                'api' => $api_name,
                "server_time" => $server_time,
            ];
            header("Access-Control-Allow-Origin: *");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Allow-Headers: X-Requested-With');
            header('Access-Control-Allow-Methods: GET, POST');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
            header("HTTP/1.0 426 UPGRADE REQUIRED!");
            echo json_encode($data);
            exit;
        }

    }

    /**
     * @param $component
     * @param $method_type
     * @param $name
     * @param $alias
     * @param $description
     * @param $settings
     */
    private function internalApiMethodCreate($component, $method_type, $name, $alias, $description, $settings)
    {

    }

    /**
     * @param $component
     * @param $method_type
     * @param $project_id
     * @param $settings
     *
     * @return mixed|void
     */
    public function addApiMethod($component, $method_type, $project_id, $settings)
    {
        $check_exists = $this->checkApiMethodTypeExists($component, $method_type);
        if (!$check_exists["status"]) {
            return $check_exists["error"];
        }
        $this->Log->add("run Api method " . $method_type, __CLASS__);
        $validator = $this->Validator->validateMethod($method_type, $settings);
        if ($validator["status"]) {
            return $this->internalApiMethodCreate($method_type, $project_id, $settings);
        } else {
            return $validator["error"];
        }
    }

    /**
     * @param string $component
     * @param string $method_type
     * @param mixed $settings
     * @param string $name
     * @param string $alias
     * @param string $description
     *
     * @return mixed
     */
    public function saveNewApiMethod($component, $method_type, $settings, $name, $alias, $description)
    {
        $check_component_available = $this->checkComponentAvailable($component);
        if (!$check_component_available["status"]) {
            return $check_component_available["error"];
        }

        $check_exists = $this->checkApiMethodTypeExists($component, $method_type);
        if (!$check_exists["status"]) {
            return $check_exists["error"];
        }

        $validator = $this->Validator->validateMethod($component, $method_type, $settings);
        if (!$validator["status"]) {
            return $validator["error"];
        }

        $alias_validator = $this->Validator->validateMethodAlias($alias);
        if (!$alias_validator["status"]) {
            return $alias_validator["error"];
        }

        return $this->internalApiMethodCreate($component, $method_type, $name, $alias, $description, $settings);

    }

    /**
     * @param $component
     * @param $method_type
     * @param $token
     * @param $id
     * @param $params
     * @param $platform
     * @param $version
     * @param $data
     *
     * @return mixed
     */
    public function runApiBaseMethod($component, $method_type, $token, $id, $params, $platform, $version, $data)
    {
        $this->Log->add("run Api method " . $method_type . " in component $component . ", __CLASS__);
        $real_method = $this->checkApiMethodTypeExists($component, $method_type);
        if ($real_method == false) {
            $err_str = "method '$method_type' not found in component $component";
            $this->Log->add($this->METHOD_NOT_FOUND_ERROR, __CLASS__);
            $this->component = $component;
            $this->token = $token;
            $this->method_type = $method_type;
            $this->params = $params;
            $this->id = $id;
            $this->data = $data;
            $this->apiErrorReturn($err_str, "400", $platform, $version);
        }
        $this->platform = $platform;
        $this->version = $version;

        $api_component = $this->getApiComponent($component);

        return $this->$api_component->$real_method($id, $params, $data);
    }

    /**
     * @param $version
     * @param $platform
     * @param $entity
     * @param $id
     * @param $method
     * @param $token
     * @param $data
     * @param $params
     * @param $result
     * @param $error
     * @return bool
     * @throws Exception
     */
    public function saveApiRequest($version, $platform, $entity, $id, $method, $token, $data, $params, $result, $error = "")
    {
        if (is_null($platform)) {
            $platform = "undefined";
        }
        if (is_null($token)) {
            $token = "undefined";
        }

        $entity = $entity ?? $this->component;
        if (is_null($entity)) {
            $entity = "undefined";
        }
        $id = $id ?? "undefined";
        $method = $method ?? "undefined";
        $modelName = "Api_Log";
        $this->Api_Log = ClassRegistry::init($modelName);
        if (!empty($result)) {
            $result = json_encode($result);
        }
        if (!empty($data)) {
            $data = json_encode($data);
        }
        if (!empty($params)) {
            $params = json_encode($params);
        }
        $execution_time = (microtime(true) - $this->start_time);
        $api_request = [
            'version' => $version,
            'platform' => $platform,
            'entity' => $entity,
            'entity_id' => $id,
            'method' => $method,
            'token' => $token,
            'data' => $data ?? " ",
            'params' => $params ?? " ",
            'result' => $result ?? " ",
            'error' => $error,
            'ip' => get_ip(),
            'time' => number_format($execution_time, 2)
        ];
        $this->Api_Log->create();
        $this->Api_Log->save($api_request);

        return true;
    }

    /**
     * @param $component
     * @param $method_type
     * @param $data
     * @param $params
     *
     * @return array
     */
    public function runApiMethodInSandbox($component, $method_type, $data, $params)
    {
        $api_component = $this->getApiComponent($component);
        $access_control = $this->checkApiMethodTypeExists($api_component, $method_type);
        if (!$access_control["status"]) {
            return $access_control;
        }
        $this->Log->add("run Api method in sandbox " . $method_type, $component);

        return $this->$api_component->runMethod($method_type, $data, $params);
    }

    // генерация API токена
    private function securityToken()
    {
        $this->Api_Token = ClassRegistry::init("Api_Token");
        $count_per_second_token = $this->Api_Token->find(
            "count",
            [
                'conditions' =>
                    [
                        'ip' => get_ip(),
                        'ua' => get_ua(),
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) <=' => 3,
                    ],
            ]
        );

        if ($count_per_second_token >= $this->count_per_second_token) {
            $this->response_api(["error" => $this::TOO_MATCH_TOKEN_MESSAGE], "error", null, null);
            exit;
        }

        $count_per_hour_token = $this->Api_Token->find(
            "count",
            [
                'conditions' =>
                    [
                        'ip' => get_ip(),
                        'ua' => get_ua(),
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) <=' => 3600,
                    ],
            ]
        );

        if ($count_per_hour_token >= $this->count_per_hour_token) {
            $this->response_api(["error" => $this::TOO_MATCH_TOKEN_MESSAGE], "error", null, null);
            exit;
        }
    }

    public function createNewToken()
    {
        $this->securityToken();
        $token = $this->generateToken();
        $this->saveToken($token);

        return $token;
    }

    /**
     * @return string
     */
    private function generateToken()
    {
        return md5(time() . uniqid() . get_ip());
    }

    /**
     * @param $token
     *
     * @return bool
     * @throws Exception
     */
    private function saveToken($token)
    {
        $token_data = [
            'ip' => get_ip(),
            'ua' => get_ua(),
            'token' => $token,
            'expired' => date('Y-m-d', strtotime('+1 year')),
        ];
        $this->Api_Token = ClassRegistry::init("Api_Token");
        $this->Api_Token->save($token_data);

        return true;
    }

    /**
     * @param $token
     * @return bool
     * @throws Exception
     */
    public function checkToken($token)
    {
        $this->Api_Token = ClassRegistry::init("Api_Token");
        $check_token = $this->Api_Token->find(
            "count",
            [
                'conditions' =>
                    [
                        'token' => $token,
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(expired) <' => 0,
                    ],
            ]
        );
        if ($check_token > 0) {
            return true;
        }
        // token устарел или иная проблема
        $this->saveApiRequest(
            "unknown",
            "unknown",
            "unknown",
            "unknown",
            "unknown",
            $token,
            "unknown",
            "unknown",
            "token error",
            $error = ""
        );

        return false;
    }

    /**
     * API response функции
     */
    /**
     * @param        $array_data
     * @param string $status
     * @param        $platform
     * @param        $version_api
     */
    public function response_api($array_data, $status = "success", $platform = "mobile", $version_api = 1)
    {
        $server_time = date("Y-m-d H:i:s");
        $api_name = Configure::read('API_NAME');
        $data = [
            'api' => $api_name,
            'data' => $array_data,
            "server_time" => $server_time,
            "platform" => $platform,
            "version_api" => $version_api,
            'status' => $status,
        ];
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: X-Requested-With');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
        echo json_encode($data);
    }

    /**
     * @param $show_count
     * @param $page
     * @param $sort
     * @param $sort_dir
     * @param $filter
     * @return array
     */
    public function getAPIRequests($show_count, $page, $sort, $sort_dir, $filter)
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        if ($sort !== "id" && $sort !== "created" && $sort !== "time") {
            $sort = $this->default_sort_field;
        }

        if ($sort_dir !== "asc" && $sort_dir !== "desc") {
            $sort_dir = "desc";
        }

        $filter_component = [];
        if (key_exists("component", $filter)) {
            $filter_component[] = ['entity' => $filter['component']];
        }
        if (key_exists("request_date", $filter)) {
            $filter_component[] = ['DATE(created) =' => $filter['request_date']];
        }

        $ct = [];
        if (key_exists("client_id", $filter) && $filter['client_id'] > 0) {

            $this->Auth = ClassRegistry::init("Auth");
            $client_tokens = $this->Auth->find(
                "all",
                [
                    'conditions' =>
                        [
                            'user_id' => $filter['client_id'],
                            'status' => 'success'
                        ],
                ]
            );

            if (count($client_tokens) > 0) {
                foreach ($client_tokens as $client_token) {
                    $token_test = $client_token['Auth']['token'];
                    if (!in_array($token_test, $ct)) {
                        $ct[] = $token_test;
                    }
                }
                $filter_component[] = ['token IN' => $ct];
            }

        }

        $requests = $this->Api_Log->find(
            "all",
            [
                'conditions' =>
                    [
                        $filter_component
                    ],
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array($sort . " " . $sort_dir),
            ]
        );
        return $requests;
    }

    public function uniqueComponents()
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        $comps = $this->Api_Log->find(
            "all",
            [
                'conditions' =>
                    [

                    ],
                'fields' => ['DISTINCT Api_Log.entity'],
                'order' => array("Api_Log.entity ASC"),
            ]
        );
        $cps = [];
        foreach ($comps as $comp) {
            $cps[] = $comp['Api_Log']['entity'];
        }
        return $cps;
    }

    public function uniqueRequestTokens()
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        $u_tokens = $this->Api_Log->find(
            "all",
            [
                'fields' => ['DISTINCT Api_Log.token'],
                // 'order' => array("Api_Log.token ASC"),
            ]
        );
        $u_t = [];
        if (count($u_tokens) > 0) {
            foreach ($u_tokens as $u_token) {
                $u_t[] = $u_token['Api_Log']['token'];
            }
        }
        return $u_t;
    }

    public function uniqueRequestClients()
    {
        $unique_users = [];
        $this->Auth = ClassRegistry::init("Auth");
        $tokens = $this->uniqueRequestTokens();
        if (count($tokens) > 0) {
            foreach ($tokens as $unique_token) {
                $client = $this->Auth->find(
                    "first",
                    [
                        'conditions' =>
                            [
                                'token' => $unique_token
                            ],
                    ]
                );
                if ($client != null) {
                    $u_id = $client['Auth']['user_id'];
                    if (count($client) > 0 && $u_id != 0) {
                        if (!in_array($u_id, $unique_users)) {
                            $unique_users[] = $u_id;
                        }
                    }
                }
            }
        }
        $users = [];
        if (count($unique_users) > 0) {

            foreach ($unique_users as $unique_user) {
                $user = $this->User->getUserData($unique_user);
                if ($user != null) {
                    $users[] = $user;
                }
            }
        }
        return $users;

    }

    public function getRequestsByIp($ip, $show_count, $page)
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        $requests = $this->Api_Log->find(
            "all",
            [
                'conditions' =>
                    [
                        'ip' => $ip
                    ],
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array("created DESC"),
            ]
        );
        return $requests;
    }

    /**
     * @param $id
     * @return array|int|null
     */
    public function getAPIRequest($id)
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        $request = $this->Api_Log->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id
                    ],
            ]
        );
        return $request;
    }

    public function totalAPICount($filter)
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        $filter_component = [];
        if (key_exists("component", $filter)) {
            $filter_component[] = ['entity' => $filter['component']];
        }
        if (key_exists("request_date", $filter)) {
            $filter_component[] = ['DATE(created) =' => $filter['request_date']];
        }

        $ct = [];
        if (key_exists("client_id", $filter) && $filter['client_id'] > 0) {

            $this->Auth = ClassRegistry::init("Auth");
            $client_tokens = $this->Auth->find(
                "all",
                [
                    'conditions' =>
                        [
                            'user_id' => $filter['client_id'],
                            'status' => 'success'
                        ],
                ]
            );

            if (count($client_tokens) > 0) {
                foreach ($client_tokens as $client_token) {
                    $token_test = $client_token['Auth']['token'];
                    if (!in_array($token_test, $ct)) {
                        $ct[] = $token_test;
                    }
                }
                $filter_component[] = ['token IN' => $ct];
            }

        }
        $api_count = $this->Api_Log->find("count",
            array('conditions' =>
                array(
                    $filter_component
                ),
            )
        );
        return $api_count;
    }

    public function totalAPICountByIp($ip)
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        $api_count = $this->Api_Log->find("count",
            array('conditions' =>
                array(
                    'ip' => $ip
                ),
            )
        );
        return $api_count;
    }

    public function checkApiRequestExists($id)
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        $api_count = $this->Api_Log->find("count",
            array('conditions' =>
                array(
                    'id' => $id
                ),
            )
        );
        return $api_count;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteApiRequest($id)
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        if (!$this->checkApiRequestExists($id)) {
            return false;
        }
        $this->Api_Log->id = $id;
        $this->Api_Log->delete();
        return true;
    }

    public function today_new_api_reqs()
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        return $this->Api_Log->find(
            "count",
            [
                'conditions' =>
                    [
                        'DATE(NOW()) - DATE(created)' => 0,
                    ],
            ]
        );

    }

    public function total_api_reqs()
    {
        $this->Api_Log = ClassRegistry::init("Api_Log");
        return $this->Api_Log->find(
            "count",
            [
                'conditions' =>
                    [],
            ]
        );
    }

    /**
     * @param $ip
     * @return bool
     * @throws Exception
     */
    public function manualBlockIp($ip)
    {
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");
        $manual_block_check = $this->Blocked_Ip->find(
            "first",
            [
                'conditions' =>
                    [
                        'ip' => $ip,
                        'rule' => 'manual'
                    ],
            ]
        );
        if (count($manual_block_check) > 0) {
            $id = $manual_block_check['Blocked_Ip']['id'];
            $this->Blocked_Ip->id = $id;
        } else {
            $this->Blocked_Ip->create();
        }
        $block = [
            'ip' => $ip,
            'rule' => 'manual',
            'status' => 'blocked',
            'expired' => '2100-12-12 00:00:00'
        ];

        if ($this->Blocked_Ip->save($block)) {
            return true;
        }
        return false;
    }

    /**
     * @param $ip
     * @return bool
     * @throws Exception
     */
    public function manualUnBlockIp($ip)
    {
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");
        $manual_block_check = $this->Blocked_Ip->find(
            "first",
            [
                'conditions' =>
                    [
                        'ip' => $ip,
                        'rule' => 'manual'
                    ],
            ]
        );
        if (count($manual_block_check) > 0) {
            $id = $manual_block_check['Blocked_Ip']['id'];
            $this->Blocked_Ip->id = $id;
        } else {
            $this->Blocked_Ip->create();
        }
        $block = [
            'ip' => $ip,
            'rule' => 'manual',
            'status' => 'active',
            'expired' => '2100-12-12 00:00:00'
        ];

        if ($this->Blocked_Ip->save($block)) {
            return true;
        }
        return false;
    }

    public function addIpToBlock($ip, $period, $rule)
    {
        $expired = date("Y-m-d H:i:s", strtotime("+$period sec"));
        $block = [
            'ip' => $ip,
            'rule' => $rule,
            'status' => 'blocked',
            'expired' => $expired
        ];
        $modelName = "Blocked_Ip";
        $this->Blocked_Ip = ClassRegistry::init($modelName);
        $this->Blocked_Ip->create();
        $this->Blocked_Ip->save($block);
        return true;
    }

    public function checkUnblock($ip)
    {
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");
        $needs_unblock_list = $this->Blocked_Ip->find(
            "all",
            [
                'conditions' =>
                    [
                        'status' => 'blocked',
                        'rule !=' => 'manual',
                        'ip' => $ip,
                        'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(expired) >' => 0,
                    ],
            ]
        );
        if (count($needs_unblock_list) > 0) {
            foreach ($needs_unblock_list as $needs_unblock) {
                $id = $needs_unblock['Blocked_Ip']['id'];
                $this->Blocked_Ip->id = $id;
                $this->Blocked_Ip->save(['status' => 'active']);
            }
        }
    }

    /**
     * @param $ip
     * @return bool
     */
    public function checkBlockIp($ip)
    {
        // проверка разблокировки прежних банов
        $this->checkUnblock($ip);

        // проверка существующих блок-вок
        if ($this->filterIp($ip)) {
            return true;
        }

        $block_result = false;

        if (in_array($ip, $this->notBlockIps)) {
            return $block_result;
        }

        // проверка новых блокировок
        $block_rules = $this->block_rules;
        $modelName = "Api_Log";
        $this->Api_Log = ClassRegistry::init($modelName);
        foreach ($block_rules as $block_rule) {
            $check_limit = $block_rule['check_limit'];
            $limit = $block_rule['limit'];
            $block_period = $block_rule['block_period'];
            $requests = $this->Api_Log->find(
                "count",
                [
                    'conditions' =>
                        [
                            'ip' => $ip,
                            'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(created) <=' => $check_limit,
                        ],
                ]
            );
            if ($requests >= $limit) {
                $this->addIpToBlock($ip, $block_period, $block_rule['name']);
                $block_result = true;
                break;
            }
        }
        return $block_result;
    }

    /**
     * @return array
     */
    public function getBlockedIp()
    {
        if (!empty($this->blocked_ips)) {
            return $this->blocked_ips;
        }
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");
        $blockeds = $this->Blocked_Ip->find(
            "all",
            [
                'conditions' =>
                    [
                        'status' => 'blocked',
                    ],
            ]
        );
        $block_list = [];
        foreach ($blockeds as $blocked) {
            $block_list[] = $blocked['Blocked_Ip']['ip'];
        }
        $this->blocked_ips = $block_list;
        return $this->blocked_ips;
    }

    /**
     * @param $ip
     * @return bool
     */
    public function filterIp($ip)
    {
        if (in_array($ip, $this->getBlockedIp())) {
            return true;
        }
        return false;
    }

    public function getBlocksByIp($ip, $show_count, $page)
    {
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");

        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 or !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        $blocks = $this->Blocked_Ip->find(
            "all",
            [
                'conditions' =>
                    [
                        'ip' => $ip
                    ],
                'limit' => $show_count,
                'offset' => $limit_page,
                'order' => array("created DESC"),
            ]
        );
        return $blocks;
    }


    /**
     * @param $ip
     * @return array|int|null
     */
    public function totalIpBlocksCountByIp($ip)
    {
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");
        return $this->Blocked_Ip->find(
            "count",
            [
                'conditions' =>
                    [
                        'ip' => $ip
                    ],
            ]
        );
    }

    public function today_ip_blocked()
    {
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");
        return $this->Blocked_Ip->find(
            "count",
            [
                'conditions' =>
                    [
                        // 'status' => 'blocked',
                        'DATE(NOW()) - DATE(created)' => 0,
                    ],
            ]
        );
    }

    public function total_ip_blocked()
    {
        $this->Blocked_Ip = ClassRegistry::init("Blocked_Ip");
        return $this->Blocked_Ip->find(
            "count",
            [
                'conditions' =>
                    [
                        //'status' => 'blocked',
                    ],
            ]
        );
    }


}