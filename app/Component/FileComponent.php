<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент File
 */
class FileComponent extends Component
{
    public $components = array(
        'Account',
        'Session',
        'Error',
        'Log',
        'Validator',
        'Mock'
    );

    public $DIRECTORY_NOT_EXISTS_ERROR = "Директория не существует";
    public $FILE_NOT_EXISTS_ERROR = "Файл не существует";
    public $FILE_THIS_NAME_ALREADY_EXISTS_ERROR = "Файл с таким именем уже существует";
    public $INCORRECT_DIR_NAME_ERROR = "Некорректное название директории";

    public $SUB_DIR_THIS_NAME_ALREADY_EXISTS_ERROR = "Директория с таким именем уже существует";

    public $base_user_dir = "file_storage";
    public $remote_base_user_dir = "/var/file_storage/";

    public $CANT_REMOVE_INIT_DIR_ERROR = "Невозможно удалить начальную директорию аккаунта";

    private $archive_file_type = [
        'zip',
        'rar',
        'gz',
        'tar',
    ];

    private $html_file_type = [
        'html',
    ];

    private $video_file_type = [
        'avi',
        'mov',
        'mp4',
        'mpeg',
        'flv'
    ];

    private $text_file_type = [
        'txt',
        'test'
    ];

    private $doc_file_type = [
        'doc',
        'docx',
        'ppt'
    ];

    private $image_file_type = [
        'jpg',
        'png',
        'gif'
    ];

    private $music_file_type = [
        'mp3',
        'ogg',
    ];

    private $program_file_type = [
        'exe',
        'com',
        'bin',
    ];

    public $controller;

    public $uses = array('Error');

    public $method_list = array(
        'get',
        'post',
        'ping_domain',
        'put',
        'option',
        'delete',
    );

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    private function setupFDB()
    {
        $this->Account->loadRemoteDataSource();
        $modelName = "FdbFile";
        $this->FdbFile = ClassRegistry::init($modelName);
        $modelName = "FdbFileSession";
        $this->FdbFileSession = ClassRegistry::init($modelName);
        $modelName = "FdbDirectory";
        $this->FdbDirectory = ClassRegistry::init($modelName);
    }

    /**
     * @param $dir_name
     * @return array
     */
    public function canDeleteDirectory($dir_name)
    {
        $dir_id = $this->getDirectoryIdByName($dir_name);
        if ($dir_id <= 0) {
            return ["status" => false, "error" => $this->DIRECTORY_NOT_EXISTS_ERROR];
        }
        if ($this->getInitDirectory() == $dir_name) {
            return ["status" => false, "error" => $this->CANT_REMOVE_INIT_DIR_ERROR];
        }
        return ["status" => true, "error" => ""];
    }

    private function getInitDirectory()
    {
        return $this->base_user_dir;
    }

    public function getDirectoryIdByName($dir_name)
    {

    }

    public function moveDirectoryToDirectory($dir_name_moved, $dir_name_source)
    {

    }

    public function getDirectoryNameById($id)
    {

    }

    /**
     * @param $id
     * @return null|string
     */
    public function getPathByDirectoryId($id)
    {
        $this->setupFDB();
        $dir_path = "";
        $new_parent_id = $id;
        while ($new_parent_id !== "0") {
            $dir = $this->FdbDirectory->find("first",
                array('conditions' =>
                    array(
                        'id' => $new_parent_id,
                    ),
                )
            );
            if (count($dir) == 0) {
                return null;
            }

            $new_parent_id = $dir['FdbDirectory']['parent_id'];
            $dir_name = $dir['FdbDirectory']['name'];
            if ($dir_path !== "") {
                $dir_path = $dir_name . UDS . $dir_path;
            } else {
                $dir_path = $dir_name;
            }
        }
        return $dir_path;
    }

    /**
     * @param $id
     * @return array|null
     */
    public function getPathIdsByDirId($id)
    {
        $this->setupFDB();
        $dir_path_arr = [];
        $new_parent_id = $id;
        while ($new_parent_id !== "0") {
            $dir = $this->FdbDirectory->find("first",
                array('conditions' =>
                    array(
                        'id' => $new_parent_id,
                    ),
                )
            );
            if (count($dir) == 0) {
                return null;
            }

            $new_parent_id = $dir['FdbDirectory']['parent_id'];
            $id = $dir['FdbDirectory']['id'];
            $dir_path_arr[] = $id;
        }
        $dir_path_arr = array_reverse($dir_path_arr);
        return $dir_path_arr;
    }
    /**
     * @param $id
     * @return array
     */
    public function getChildrenDirectories($id)
    {
        $this->setupFDB();
        $dirs = $this->FdbDirectory->find("first",
            array('conditions' =>
                array(
                    'parent_id' => $id,
                ),
                "order" => array("name asc")
            )
        );
        $_dirs = [];
        if (count($dirs) == 0) {
            return null;
        }
        foreach ($dirs as $dir) {
            $found_dir = (key_exists("FdbDirectory", $dir)) ? $dir['FdbDirectory'] : $dir;
            $_dirs[] = [
                'type' => "dir",
                'id' => $found_dir['id'],
                'name' => $found_dir['name'],
                'created' => $found_dir['created'],
                //'modified' => $found_dir['modified'],
            ];
        }
        return $_dirs;

    }

    /**
     * @param int $id
     * @return int
     */
    public function getParentDirectory($id)
    {
        if ($id == 0) {
            return null;
        }
        $this->setupFDB();
        $dir = $this->FdbDirectory->find("first",
            array('conditions' =>
                array(
                    'id' => $id,
                ),
            )
        );
        return $dir['FdbDirectory']['parent_id'];
    }


    /**
     * @param string $directory
     * @param string $selected_type
     * @param string $sort_field
     * @param string $sort_dir
     * @param int $page
     * @return mixed
     */
    public function getDirectoryList($directory, $selected_type, $sort_field, $sort_dir, $page)
    {
        $this->setupFDB();
        if (!is_int($directory)) {
            //$directory = $this->getDirectoryIdByName($directory);
        }
        if ($directory == null) {
            //корневая директория по умолчанию
            $directory = 0;
        }
//        if ($directory <= 0) {
//            return ["status" => false, "error" => $this->INCORRECT_DIR_NAME_ERROR];
//        }
        $show_count = 20;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        $default_sorter = "file_name";
        $valid_sort = [
            "file_name",
            "created",
            "modified",
            "file_size"
        ];
        if ($sort_field == null) {
            $sort = $default_sorter;
        }
        if (!in_array($sort_field, $valid_sort)) {
            $sort = $default_sorter;
        }
        if ($sort_dir !== "ASC" && $sort_dir !== "DESC") {
            $sort_dir = "ASC";
        }

        $found_files = $this->FdbFile->find("all",
            array('conditions' =>
                array(
                    'dir_id' => $directory,
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                "order" => [$sort . " " . $sort_dir]
            )
        );
        $_files = [];
        foreach ($found_files as $found_file) {
            $found_file = $found_file['FdbFile'];
            $_files[] = [
                'type' => $this->setFileType(ext($found_file['file_name'])),
                'id' => $found_file['id'],
                'file_name' => $found_file['file_name'],
                'file_size' => $this->formatSizeUnits($found_file['file_size']),
                'hash' => $found_file['checksum'],
                'created' => $found_file['created'],
                'modified' => $found_file['modified'],
                'ext' => ext($found_file['file_name']),
            ];
        }
        return $_files;
    }

    /**
     * @return int
     */
    public function getCurrentStorageSize()
    {
        $this->setupFDB();
        $all_files = $this->FdbFile->find("all",
            array('conditions' =>
                array(),
                "fields" => 'SUM(file_size) as total_sum'
            )
        );
        if (isset($all_files[0][0]['total_sum'])) {
            return $all_files[0][0]['total_sum'];
        }
        return 0;
    }

    /**
     * @param int $bytes
     * @return string
     */
    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    private function allFileTypes()
    {
        return [
            "archive",
            "html",
            "video",
            "text",
            "doc",
            "image",
            "music",
            "program",
        ];
    }

    /**
     * @param $file_type
     * @return string
     */
    private function setFileType($file_type)
    {
        $found_file_type = "unknown";
        foreach ($this->allFileTypes() as $type) {
            $array_name = $type . "_file_type";
            $item_array = $this->$array_name;
            if (in_array($file_type, $item_array)) {
                $found_file_type = $type;
                break;
            }
        }
        return $found_file_type;
    }

    /**
     * @param $dir_list
     * @param $found_dir
     * @return bool
     */
    private function isDirectoryHasSubdirectory($dir_list, $found_dir)
    {
        foreach ($dir_list as $dir_list) {
            if ($dir_list["name"] == $found_dir) {
                return true;
            }
        }
        return false;
    }

    public function getDirectoryStat($directory)
    {

    }

    /**
     * @param $path
     * @param $dir_name
     * @return array
     */
    public function createDirectory($path, $dir_name)
    {
        if (!$this->isValidDirName($dir_name)) {
            return ["status" => false, "error" => $this->INCORRECT_DIR_NAME_ERROR];
        }
        $parent_dir_id = $this->getDirectoryIdByName($path);
        if ($parent_dir_id <= 0) {
            return ["status" => false, "error" => $this->DIRECTORY_NOT_EXISTS_ERROR];
        }
        if ($this->isDirectoryHasSubdirectory($path, $dir_name)) {
            return ["status" => false, "error" => $this->SUB_DIR_THIS_NAME_ALREADY_EXISTS_ERROR];

        }
        $create = $this->internalCreateDirectory($path, $parent_dir_id, $dir_name);
        if ($create) {
            return ["success" => true, "error" => "", "id" => $create];
        } else {
            return ["success" => false, "error" => "Не удалось создать директорию"];
        }


    }

    /**
     * @param $path
     * @param $parent_dir_id
     * @param $dir_name
     * @return bool
     */
    public function internalCreateDirectory($path, $parent_dir_id, $dir_name)
    {
        $abs_dir_name = $path . UDS . $dir_name;
        $saving = mkdir($abs_dir_name);
        if (!$saving) {
            return false;
        }
        $data = [
            'name' => $dir_name,
            'parent_id' => $parent_dir_id,
        ];
        $this->FdbDirectory->create();
        $this->FdbDirectory->save($data);
        $dir_id = $this->FdbDirectory->id;
        return $dir_id;
    }

    public function deleteDirectory($dir)
    {

    }

    public function forceDeleteDirectory($dir)
    {

    }

    /**
     * @param $dir_name
     * @return bool
     */
    private function isValidDirName($dir_name)
    {
        $dir_name = trim($dir_name);
        if ($dir_name == $this->getInitDirectory()) {
            return false;
        }
        if (mb_strlen($dir_name) == 0) {
            return false;
        }

        if (!$this->Validator->valid_directory($dir_name)) {
            return false;
        }
        return true;
    }

    /**
     * @param $dir_id
     * @return bool
     */
    private function checkDirectoryExists($dir_id)
    {
        return false;
    }


//----FILE----


    /**
     * @param string $file
     * @return string
     */
    private function fileCheckSum($file){
        return md5_file($file);
    }

    /**
     * @param $file_id
     * @return array|null|string
     */
    public function getRemoteFile($file_id)
    {
        $this->setupFDB();
        $file = $this->getFileById($file_id);
        if (!$file) {
            return ["error" => $this->FILE_NOT_EXISTS_ERROR, "status" => false];
        }
        $dir_id = $file['FdbFile']['dir_id'];
        $file_name = $file['FdbFile']['file_name'];
        $dir_path = $this->getPathByDirectoryId($dir_id);
        $remote_file = $this->remote_base_user_dir . $dir_path . $file_name;
        $upload_dir = Configure::read('FILE_TEMP_DIR');
        $local_file = $upload_dir . DS . $file_name;
        $this->synchroRemoteToLocal($local_file, $remote_file);
        if(file_exists($local_file)){
            return $file_name;
        } else {
            return null;
        }
    }

    /**
     * @param $file_id
     * @return null|string
     */
    public function getRemoteFileCached($file_id)
    {
        $this->setupFDB();
        $file_data = $this->getFileById($file_id);
        $file_name = $file_data['FdbFile']['file_name'];
        $file_hash = $file_data['FdbFile']['checksum'];
        $upload_dir = Configure::read('FILE_TEMP_DIR');
        $local_file = $upload_dir . DS . $file_name;
        if(file_exists($local_file) AND $file_hash == $this->fileCheckSum($local_file)){
            return basename($local_file);
        }
        return null;
    }

    public function getFileById($file_id)
    {
        $file = $this->FdbFile->find("first",
            array('conditions' =>
                array(
                    'id' => $file_id,
                )
            )
        );
        if (count($file) > 0) {
            return $file;
        }
        return null;
    }

// методы для работы с файлами
    public function deleteFile($file_id)
    {
        $this->setupFDB();
        $file = $this->getFileById($file_id);
        if ($file == null) {
            return false;
        }
        $dir_id = $file['FdbFile']['dir_id'];
        $dir_path = $this->getPathByDirectoryId($dir_id);
        $remote_file = $this->remote_base_user_dir . $dir_path . $file['FdbFile']['file_name'];
        $this->synchroRemoteDelete($remote_file);
    }

    public function moveFileToDirectory($file, $dir_name)
    {

    }


    /**
     * @param $dir
     * @param $file
     * @return bool
     */
    public function checkFileExists($dir, $file)
    {
        $this->setupFDB();
        $dir_id = $this->getDirectoryIdByName($dir);
        $check_file = $this->FdbFile->find("count",
            array('conditions' =>
                array(
                    'dir_id' => $dir_id,
                    'filename' => $file,
                )
            )
        );
        if ($check_file > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @param $dir
     * @param $file
     * @return array|bool
     */
    public function saveFile($data, $dir, $file)
    {
        $this->setupFDB();
        if ($this->checkDirectoryExists($dir)) {
            return ["status" => false, "error" => $this->DIRECTORY_NOT_EXISTS_ERROR];
        }
        if ($this->checkFileExists($dir, $file)) {
            return ["status" => false, "error" => $this->FILE_THIS_NAME_ALREADY_EXISTS_ERROR];
        }
        $dir_id = $this->getDirectoryIdByName($dir);
        $user_id = $this->Account->getUserId();
        return $this->internalSaveFile($data, $dir_id, $dir, $file, $user_id);
    }

    /**
     * @param mixed $data
     * @param int $dir_id
     * @param string $dir_name
     * @param string $file_name
     * @param int $user_id
     * @return bool
     */
    public function internalSaveFile($data, $dir_id, $dir_name, $file_name, $user_id)
    {
        $abs_file_name = $dir_name . UDS . $file_name;
        $saving = file_put_contents($abs_file_name, $data);
        if (!$saving) {
            return false;
        }
        $hash = $this->fileCheckSum($abs_file_name);
        $this->saveFileToDB($file_name, $dir_id, $hash, filesize($abs_file_name));
        //--saving

    }

    /**
     * @param $file_name
     * @param $dir_id
     * @param $hash
     */
    private function saveFileToDB($file_name, $dir_id, $hash, $size)
    {
        $data = [
            'file_name' => $file_name,
            'dir_id' => $dir_id,
            'checksum' => $hash,
            'file_size' => $size,
        ];
        $this->setupFDB();
        $this->FdbFile->create();
        $this->FdbFile->save($data);
        $file_id = $this->FdbFile->id;

        $session_id = $this->Account->getUserSessionId();
        $this->fileSession($file_id, $session_id);
        return $file_id;
    }

    /**
     * @param $file_id
     * @param $session_id
     */
    private function fileSession($file_id, $session_id)
    {
        $this->setupFDB();
        $this->FdbFileSession->create();
        $this->FdbFileSession->save(
            [
                "file_id" => $file_id,
                "session_id" => $session_id,
                "action" => "create"
            ]
        );
        return $this->FdbFileSession->id;
    }

    private function isValidFileName($filename)
    {

    }

    public function limitFilesSizeTotal()
    {
        //TODO HARDCODE
        //800 mb
        return 1024 * 1024 * 800;
    }

    public function limitMaxFileSize()
    {
        //TODO HARDCODE
        return 2;
    }

    public function test_synchro()
    {
        $local_file = "C:/server/test.test";
        $remote_file = "/var/mytemp/test.test";
        $this->synchroRemoteToLocal($local_file, $remote_file);
    }

    /**
     * @param $new_file_name
     * @param $file_name
     * @param $dir_id
     * @param $size
     */
    public function saveUploadedFile($new_file_name, $file_name, $dir_id, $size)
    {
        $hash = $this->fileCheckSum($new_file_name);
        $dir_exists = true;
        //сохранение
        $this->saveFileToDB($file_name, $dir_id, $hash, $size);
        if (!$this->Validator->valid_int($dir_id)) {
            $dir_exists = false;
        }
        if (!$this->checkDirectoryExists($dir_id)) {
            $dir_exists = false;
        }
        if ($dir_exists) {
            $dir_path = $this->getPathByDirectoryId($dir_id);
            $final_path = $this->remote_base_user_dir . $dir_path . UDS;
        } else {
            $final_path = $this->remote_base_user_dir;
        }
        // отправка файла в файловое хранилище
        $this->synchroLocalToRemote($new_file_name, $final_path . $file_name);
    }

    /**
     * @param $local_file
     * @param $remote_file
     * @return bool
     */
    public function synchroLocalToRemote($local_file, $remote_file)
    {
        $session = $this->Account->sshConnection();
        $sending = ssh2_scp_send($session, $local_file, $remote_file);
        if (!$sending) {
            return false;
        }
        return true;
    }

    /**
     * @param $local_file
     * @param $remote_file
     * @return bool
     */
    public function synchroRemoteToLocal($local_file, $remote_file)
    {
        $session = $this->Account->sshConnection();
        $receiving = ssh2_scp_recv($session, $remote_file, $local_file);
        if (!$receiving) {
            return false;
        }
        return true;
    }

    public function synchroRemoteDelete($remote_file)
    {
        $session = $this->Account->sshConnection();
        if (substr_count($remote_file, $this->remote_base_user_dir) == 0) {
            return false;
        }
        $check_file_exists_com = "ls -al " . $remote_file;
        $checking = ssh2_exec($session, $check_file_exists_com);
        stream_set_blocking($checking, true);
        $stream_out = ssh2_fetch_stream($checking, SSH2_STREAM_STDIO);
        $rl = stream_get_contents($stream_out);
        if (empty($rl)) {
            // файл не найден, что-то пошло не так
            return false;
        }
        $deleting_com = "rm -rf " . $remote_file;
        $deleting = ssh2_exec($session, $deleting_com);
        if (!$deleting) {
            return false;
        }
        return true;
    }

    public function downloadFileFromRemoteToHiveFtp()
    {

    }

    public
    function downloadFileFromRemoteToHiveHttp()
    {

    }

    public
    function uploadFileFromHiveToRemoteFtp()
    {

    }

    public
    function uploadFileFromHiveToRemoteHttp()
    {

    }

//-- помойка --
}