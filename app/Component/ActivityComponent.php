<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Активность
 */
class ActivityComponent extends Component
{
    public $components = array(
        'Session',
        'Error',
        'Log'
    );

    public $controller;
    public $user_data;
    public $user_id;

    public function getComponents()
    {
        return [
            'auth' => 'Авторизации',
            'admin' => 'Администраторы',
            'actions' => 'Акции',
            'locations' => 'Локации',
            'settings' => 'Настройки и управление',
            'news' => 'Новости',
        ];
    }

    public $uses = array('Error');

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    private function formatData($comment, $data){
        if(count($data)>0){
            foreach ($data as $k => $v){
                $comment = str_replace("#". $k, $v, $comment);
            }
        }
        return $comment;
    }

     /**
     * @param $component_name
     * @param $comment
     * @param $manager_id
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function add($component_name, $manager_id, $comment, $data)
    {
        $comment = $this->formatData($comment, $data);
        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activity = array(
            'name' => $component_name,
            'comment' => $comment,
            'data' => serialize($data),
            'manager_id' => $manager_id,
        );
        if ($this->Activities->save($activity)) {
            return true;
        } else {
            return false;
        }
    }

    public function getActivityList($page = null, $sort = null, $manager_id = null, $activity_name = null, $date = null){
        $show_count = 20;
        $page = (is_numeric($page)) ? $page : 0;
        if ($page <= 0 OR !is_numeric($page)) $page = 1;
        $limit_page = $show_count * ($page - 1);

        if($sort == null){
            $sort = "created";
        }

        if($manager_id!=null){
            $manager_query = array("manager_id" => $manager_id);
        } else {
            $manager_query = [];
        }

        if($date!=null){
            $date_query = array('DATE(Activities.created)' => $date);
        } else {
            $date_query = [];
        }

        $activity_string_search = "";
        if (!empty($activity_name)) {
            $activity_string_search = array("name" => $activity_name);
        }

        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activities =  $this->Activities->find('all',
            array(
                'conditions' => array(
                    $activity_string_search,
                    $manager_query,
                    $date_query
                ),
                'joins' => array(
                    array(
                        'table' => 'managers',
                        'alias' => 'Manager',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Activities.manager_id = Manager.id'
                        )
                    ),
                ),
                'limit' => $show_count,
                'offset' => $limit_page,
                'fields' => array('Activities.*', 'Manager.*'),
                'order' => array("Activities.$sort DESC")
            )
        );
        foreach ($activities as &$activity){
            $created = $activity['Activities']['created'];
            $activity['Activities']['created_ago'] = days_later(time() - strtotime($created));
        }
        return $activities;
    }

    public function getActivityCount($manager = null, $activity_name = null){
        if($manager!=null){
            $manager_query = array("user_id" => $manager);
        } else {
            $manager_query = [];
        }

        $activity_string_search = "";
        if (!empty($activity_name)) {
            $activity_string_search = array("activity" => $activity_name);
        }

        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activities =  $this->Activities->find('count',
            array(
                'conditions' => array(
                    $activity_string_search,
                    $manager_query,
                ),
            )
        );
        return $activities;
    }

    public function get_activity_list($activity_name = "", $manager_id = "", $data_1 = "")
    {
        $activity_string_search = "";
        if (!empty($activity_name)) {
            $activity_string_search = array("activity" => $activity_name);
        }
        $manager_string_search = "";
        if (!empty($manager_id)) {
            $manager_string_search = array("manager_id" => $manager_id);
        }
        $data_string_search = "";
        if (!empty($data_1)) {
            $data_string_search = array("data_1" => $data_1);
        }

        $modelName = "Activities";
        $this->Activities = ClassRegistry::init($modelName);
        $activities =  $this->Activities->find('all',
            array(
                'conditions' => array(
                    $activity_string_search,
                    $manager_string_search,
                    $data_string_search
                )
            )
        );
        return $activities;
    }

}