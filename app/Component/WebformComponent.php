<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Веб-форма
 */
class WebformComponent extends Component
{
    public $components   = [
        'Log',
        'DataProvider',
    ];

    public $controller;

    public $field_errors = [
        'required_numeric_field'  => 'Ожидается тип данных Число',
        'required_numeric_string' => 'Ожидается тип данных Строка',
        'value_min_length'        => 'Переданное значение меньше (короче) минимально возможного',
        'value_max_length'        => 'Переданное значение больше (длиннее) максимально возможного',
        'required_field_is_empty' => 'Значение обязательного поля пусто или отсутствует',
    ];

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function beforeFilter()
    {
    }

    public $errors                = [];

    public $error_text            = [
        'no_fields'            => 'в форме нет полей',
        'no_field_name'        => 'не заполнено поле Название',
        'no_field_type'        => 'не указан Тип поля',
        'no_field_code'        => 'не указан Код поля',
        'incorrect_field_name' => 'некорректно заполнено поле Название',
        'incorrect_field_code' => 'некорректно заполнено поле Код',
        'incorrect_field_type' => 'Неопределенный Тип поля',
        "form_id_is_null"      => 'Некорректный или пустой идентификатор формы',
        "user_id_is_null"      => 'Некорректный или пустой идентификатор пользователя',
        "manager_id_is_null"   => 'Некорректный или пустой идентификатор администратора',
        'form_data_is_null'    => 'Веб-форма не содержит требуемых данных',
        'form_is_not_found'    => 'Форма не найдена',
    ];

    public $valid_report_statuses = [
        'new',
        'closed',
        'inwork',
    ];

    public $api_methods           = [
        "fill" => "fillFormApi",
        "get"  => "getFormByIdApi",
    ];

    public function setup()
    {
        $modelName                    = "Webform_Field";
        $this->Webform_Field          = ClassRegistry::init($modelName);
        $modelName                    = "Webform";
        $this->Webform                = ClassRegistry::init($modelName);
        $modelName                    = "Webform_User_Data";
        $this->Webform_User_Data      = ClassRegistry::init($modelName);
        $modelName                    = "Webform_User_Data_List";
        $this->Webform_User_Data_List = ClassRegistry::init($modelName);

        $modelName                   = "Data_Radio_Element";
        $this->Data_Radio_Element    = ClassRegistry::init($modelName);
        $modelName                   = "Data_Checkbox_Element";
        $this->Data_Checkbox_Element = ClassRegistry::init($modelName);

    }

    public $valid_field_types = [
        "-"              => "Нет значения",
        "textarea"       => "Многострочное текстовое поле",
        "input_text"     => "Однострочное текстовое поле",
        "input_radio"    => "Элемент Radio",
        "input_checkbox" => "Элемент Checkbox",
        "select"         => "Выпадающий список",
        "input_date"     => "Тип Дата",
        "input_name"     => "Тип Фио/Название",
        "input_phone"    => "Тип Номер телефона",
        "input_email"    => "Тип Email",
    ];

    /* методы:
    render form
    add form field
    delete form field
    update form field
    validate webform

    form_reports
    fill_user_form
    form view
    form change status

     *
     * */

    private function validCode($code)
    {
        $regexp = "/^[A-Za-z0-9._-]{2,32}$/u";

        return preg_match($regexp, $code);
    }

    private function validName($code)
    {
        $regexp = "/^[ a-zA-Zа-яёА-ЯЁ0-9_-]{2,256}$/ui";

        return preg_match($regexp, $code);
    }

    private function validType($type)
    {
        if (!in_array($type, array_keys($this->valid_field_types))) {
            return false;
        }

        return true;
    }

    /**
     * @param $webform
     *
     * @return bool
     */
    public function validateWebform($webform)
    {
        if (count($webform) == 0) {
            $this->errors[] = $this->error_text['no_fields'];
        }

        foreach ($webform['name'] as $elem) {

            if (!$this->validName($elem)) {
                $this->errors[] = $this->error_text['incorrect_field_name'];
            }

            if (empty($elem)) {
                $this->errors[] = $this->error_text['no_field_name'];
            }
        }
        foreach ($webform['code'] as $elem) {

            if (!$this->validCode($elem)) {
                $this->errors[] = $this->error_text['incorrect_field_code'];
            }
            if (empty($elem)) {
                $this->errors[] = $this->error_text['no_field_code'];
            }
        }
        foreach ($webform['type'] as $elem) {

            if (!$this->validType($elem)) {
                $this->errors[] = $this->error_text['incorrect_field_type'];
            }
            if (empty($elem)) {
                $this->errors[] = $this->error_text['no_field_type'];
            }

        }
        if (count($this->errors) > 0) {
            return false;
        }

        return true;
    }

    /**
     * @param $form_id
     * @param $name
     * @param $type
     * @param $data_provider_id
     * @param $code
     * @param $placeholder
     * @param $order
     *
     * @return bool
     */
    public function addWebformField($form_id, $name, $type, $data_provider_id, $code, $placeholder, $order)
    {
        $this->setup();
        $data = [
            'form_id'             => $form_id,
            'field_name'          => $name,
            'field_type'          => $type,
            'field_value_list_id' => $data_provider_id,
            'field_code'          => $code,
            'placeholder'         => $placeholder,
            'field_order'         => $order,
        ];
        $this->Webform_Field->create();
        $this->Webform_Field->save($data);

        return $this->Webform_Field->id;
    }

    /**
     * @param $field_id
     * @param $value
     *
     * @return bool
     */
    public function addRadioValue($field_id, $value)
    {
        $this->setup();
        $data = [
            'field_id' => $field_id,
            'value'    => $value,
        ];
        $this->Data_Radio_Element->create();
        $this->Data_Radio_Element->save($data);

        return true;
    }

    /**
     * @param $field_id
     * @param $value
     *
     * @return bool
     */
    public function addCheckboxValue($field_id, $value)
    {
        $this->setup();
        $data = [
            'field_id' => $field_id,
            'value'    => $value,
        ];
        $this->Data_Checkbox_Element->create();
        $this->Data_Checkbox_Element->save($data);

        return true;
    }

    /**
     * @param $value_id
     * @param $value
     *
     * @return bool
     */
    public function updateRadioValue($value_id, $value)
    {
        $this->setup();
        $data                         = [
            'value' => $value,
        ];
        $this->Data_Radio_Element->id = $value_id;
        $this->Data_Radio_Element->save($data);

        return true;
    }

    /**
     * @param $value_id
     * @param $value
     *
     * @return bool
     */
    public function updateCheckboxValue($value_id, $value)
    {
        $this->setup();
        $data                            = [
            'value' => $value,
        ];
        $this->Data_Checkbox_Element->id = $value_id;
        $this->Data_Checkbox_Element->save($data);

        return true;
    }

    /**
     * @param $value_id
     *
     * @return bool
     */
    public function deleteCheckboxValue($value_id)
    {
        $this->setup();
        $this->Data_Checkbox_Element->id = $value_id;
        $this->Data_Checkbox_Element->delete();

        return true;
    }

    /**
     * @param $value_id
     *
     * @return bool
     */
    public function deleteRadioValue($value_id)
    {
        $this->setup();
        $this->Data_Radio_Element->id = $value_id;
        $this->Data_Radio_Element->delete();

        return true;
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getRadioValuesByField($id)
    {
        $this->setup();
        $rd_values = $this->Data_Radio_Element->find(
            "all",
            [
                'conditions' =>
                    [
                        'field_id' => $id,
                    ],
                'order'      => ['id ASC'],
            ]
        );
        $rds       = [];
        foreach ($rd_values as $rd_value) {
            $rd_value = $rd_value['Data_Radio_Element'];
            $rds[]    = ['id' => $rd_value['id'], 'value' => $rd_value['value']];
        }

        return $rds;
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getCheckboxValuesByField($id)
    {
        $this->setup();
        $cb_values = $this->Data_Checkbox_Element->find(
            "all",
            [
                'conditions' =>
                    [
                        'field_id' => $id,
                    ],
                'order'      => ['id ASC'],
            ]
        );
        $cbs       = [];
        foreach ($cb_values as $cb_value) {
            $cb_value = $cb_value['Data_Checkbox_Element'];
            $cbs[]    = ['id' => $cb_value['id'], 'value' => $cb_value['value']];
        }

        return $cbs;
    }

    /**
     * @param $form_id
     * @param $id
     * @param $name
     * @param $type
     * @param $data_provider_id
     * @param $code
     * @param $placeholder
     * @param $order
     *
     * @return bool
     */
    public function updateWebformField($form_id, $id, $name, $type, $data_provider_id, $code, $placeholder, $order)
    {
        $this->setup();
        $data                    = [
            'form_id'             => $form_id,
            'field_name'          => $name,
            'field_type'          => $type,
            'field_value_list_id' => $data_provider_id,
            'field_code'          => $code,
            'placeholder'         => $placeholder,
            'field_order'         => $order,
        ];
        $this->Webform_Field->id = $id;
        $this->Webform_Field->save($data);

        return true;
    }

    public function getWebformFields()
    {
        return $this->valid_field_types;
    }

    //FILL FORM
    public function fillWebform($form_id, $user_id, $manager_id, $form_data)
    {

        if ($form_id == null) {
            $this->errors[] = $this->error_text['form_id_is_null'];

            return false;
        }
        if ($manager_id == null) {
            $this->errors[] = $this->error_text['manager_id_is_null'];

            return false;
        }
        if (!is_array($form_data) or count($form_data) == 0) {
            $this->errors[] = $this->error_text['form_data_is_null'];

            return false;
        }
        $webform = $this->getWebformById($form_id);
        if ($webform == null) {
            $errors[] = $this->error_text['form_is_not_found'];

            return false;
        }
        $this->fillFormInternal($form_id, null, $manager_id, $form_data);

        return true;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    private function getFormElementTypeById($id)
    {
        $this->setup();
        $field = $this->Webform_Field->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );

        return $field['Webform_Field'];
    }

    /**
     * @param $form_id
     * @param $user_id
     * @param $manager_id
     * @param $form_data
     *
     * @return mixed
     */
    private function fillFormInternal($form_id, $user_id, $manager_id, $form_data)
    {
        $this->setup();
        $data = [
            'webform_id' => $form_id,
            'user_id'    => $user_id,
            'manager_id' => $manager_id,
            'status'     => 'new',
            'result'     => "-",
        ];
        $this->Webform_User_Data->create();
        $this->Webform_User_Data->save($data);
        $user_form_id = $this->Webform_User_Data->id;
        foreach ($form_data as $key => $form_data_item) {

            $field_type = $this->getFormElementTypeById($key);
            if ($field_type['field_type'] == "select") {
                $value_field = 'field_value_id';
            } else {
                if (['field_type'] == "checkbox") {
                    $value_field = 'field_value';
                } else {
                    if (['field_type'] == "radio") {
                        $value_field = 'radio_id';
                    } else {
                        if (['field_type'] == "radio") {
                            $value_field = 'checkbox_ids';
                        }
                    }
                }
            }
            $data = [
                'webform_id'          => $form_id,
                'webform_userdata_id' => $user_form_id,
                'field_id'            => $key,
                $value_field          => $form_data_item,
            ];
            $this->Webform_User_Data_List->create();
            $this->Webform_User_Data_List->save($data);
        }

        return $user_form_id;
    }

    public function fillFormApi($id, $params, $data)
    {
        $errors = [];
        if (empty($data['user_id']) or !is_numeric($data['user_id'])) {
            $errors[] = "empty user_id";
            $this->Api->response_api($errors, "error");
            exit;
        }

        if (empty($data['form_data']) or !is_numeric($data['form_data'])) {
            $errors[] = "no form data";
            $this->Api->response_api($errors, "error");
            exit;
        }
        if (empty($data['webform_id']) or !is_numeric($data['webform_id'])) {
            $errors[] = "no form identifier found";
            $this->Api->response_api($errors, "error");
            exit;
        }
        $webform = $this->getWebformById($data['webform_id']);
        if ($webform == null) {
            $errors[] = "form is not found";
            $this->Api->response_api($errors, "error");
            exit;
        }
        $form_id = $data['webform_id'];
        $user_id = $data['user_id'];
        $this->fillFormInternal($form_id, $user_id, null, $data['form_data']);
        // форма успешно заполнена

        $this->Api->response_api(null, "success");
        exit;
    }

    public function getFormData($id)
    {
        $this->setup();
        $fields = $this->Webform_Field->find(
            "all",
            [
                'conditions' =>
                    [
                        'form_id' => $id,
                    ],
                'order'      => ['field_order ASC'],
            ]
        );
        // получение источников данных для полей
        foreach ($fields as &$field) {
            $field_data = $field['Webform_Field'];
            $field_code = $field_data['code'];
            $type       = $field_data['field_type'];
            if ($type == "select") {
                $provider_list = $this->DataProvider->getProviderList($id);
                $arr           = [];
                foreach ($provider_list as $provider_list_elem) {
                    $arr[] = $provider_list_elem['Data_List_Item'];
                }
                $field['provider_list'] = $arr;
            } else {
                if ($type == "input_checkbox") {
                    if (!is_array($field[$field_code]['value_list'])) {
                        $field[$field_code]['value_list'] = $this->getCheckboxValuesByField($id);
                    }
                } else {
                    if ($type == "input_radio") {
                        if (!is_array($field[$field_code]['value_list'])) {
                            $field[$field_code]['value_list'] = $this->getRadioValuesByField($id);
                        }
                    }
                }
            }

        }

        return $fields;

    }

    /**
     * @param $form_id
     *
     * @return array
     */
    public function getFormFieldIds($form_id)
    {
        $this->setup();
        $fields = $this->Webform_Field->find(
            "all",
            [
                'conditions' =>
                    [
                        'form_id' => $form_id,
                    ],
                'order'      => ['field_order ASC'],
            ]
        );
        // получение источников данных для полей
        $ids = [];
        foreach ($fields as &$field) {
            $field_data = $field['Webform_Field'];
            $id         = $field_data['id'];
            $ids[]      = $id;
        }

        return $ids;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getWebformById($id)
    {
        $this->setup();

        return $this->Webform->find(
            "first",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    public function renderForm($id)
    {
        $this->set("form_data", $this->getFormData($id));
    }

    /**
     * @param $find_by_name
     * @param $enabled
     *
     * @return mixed
     */
    public function totalWebformCount($find_by_name, $enabled)
    {
        $this->setup();
        $enabled_filter = [];
        $find_filter    = [];
        if ($enabled != null) {
            $enabled_filter = $enabled;
        }
        if ($find_by_name != null) {
            $find_filter = ['name LIKE ' => "%$find_by_name%"];
        }

        return $this->Webform->find(
            "count",
            [
                'conditions' =>
                    [
                        $enabled_filter,
                        $find_filter,
                    ],
            ]
        );
    }

    /**
     * @param $find_by_name
     * @param $enabled
     *
     * @return mixed
     */
    public function totalWebformReportCount($find_by_name, $enabled)
    {
        $this->setup();
        $enabled_filter = [];
        $find_filter    = [];
        if ($enabled != null) {
            $enabled_filter = $enabled;
        }
        if ($find_by_name != null) {
            $find_filter = ['name LIKE ' => "%$find_by_name%"];
        }

        return $this->Webform_User_Data->find(
            "count",
            [
                'conditions' =>
                    [
                        $enabled_filter,
                        $find_filter,
                    ],
            ]
        );
    }

    /**
     * @param $show_count
     * @param $limit_page
     * @param $sort
     * @param $sort_dir
     * @param $enabled
     * @param $find_by_name
     *
     * @return mixed
     */
    public function webformReportList($show_count, $limit_page, $sort, $sort_dir, $enabled, $find_by_name)
    {
        $this->setup();
        $enabled_filter = [];
        $find_filter    = [];
        if ($enabled != null) {
            $enabled_filter = $enabled;
        }
        if ($find_by_name != null) {
            $find_filter = ['name LIKE ' => "%$find_by_name%"];
        }

        return $this->Webform_User_Data->find(
            "all",
            [
                'conditions' =>
                    [
                        $enabled_filter,
                        $find_filter,
                    ],
                'joins'      => [
                    [
                        'table'      => 'webforms',
                        'alias'      => 'Webform',
                        'type'       => 'INNER',
                        'conditions' => [
                            'Webform_User_Data.webform_id = Webform.id',
                        ],
                    ],
                    [
                        'table'      => 'users',
                        'alias'      => 'User',
                        'type'       => 'Left',
                        'conditions' => [
                            'User.id = Webform_User_Data.user_id',
                        ],
                    ],
                    [
                        'table'      => 'managers',
                        'alias'      => 'Manager',
                        'type'       => 'Left',
                        'conditions' => [
                            'Manager.id = Webform_User_Data.manager_id',
                        ],
                    ],
                ],
                'fields'     => ['Webform_User_Data.*', 'Webform.*', 'User.*', 'Manager.*'],
                'limit'      => $show_count,
                'offset'     => $limit_page,
                "order"      => ["Webform.$sort $sort_dir"],
            ]
        );
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function webformReportById($id)
    {
        $this->setup();

        return $this->Webform_User_Data->find(
            "all",
            [
                'conditions' =>
                    [
                        'Webform_User_Data.id' => $id,
                    ],
                'joins'      => [
                    [
                        'table'      => 'webforms',
                        'alias'      => 'Webform',
                        'type'       => 'INNER',
                        'conditions' => [
                            'Webform_User_Data.webform_id = Webform.id',
                        ],
                    ],
                    [
                        'table'      => 'users',
                        'alias'      => 'User',
                        'type'       => 'Left',
                        'conditions' => [
                            'User.id = Webform_User_Data.user_id',
                        ],
                    ],
                    [
                        'table'      => 'managers',
                        'alias'      => 'Manager',
                        'type'       => 'Left',
                        'conditions' => [
                            'Manager.id = Webform_User_Data.manager_id',
                        ],
                    ],
                ],
                'fields'     => ['Webform_User_Data.*', 'Webform.*', 'User.*', 'Manager.*'],
            ]
        );
    }

    public function webformReportDataById($id)
    {
        $this->setup();
        $form_fields = $this->Webform_User_Data_List->find(
            "all",
            [
                'conditions' =>
                    [
                        'Webform_User_Data_List.webform_userdata_id' => $id,
                    ],
                'joins'      => [
                    [
                        'table'      => 'webform_fields',
                        'alias'      => 'Webform_Field',
                        'type'       => 'Left',
                        'conditions' => [
                            'Webform_User_Data_List.field_id = Webform_Field.id',
                        ],
                    ],
                    [
                        'table'      => 'form_data_lists',
                        'alias'      => 'Form_Data_List',
                        'type'       => 'Left',
                        'conditions' => [
                            'Webform_User_Data_List.field_value_id = Form_Data_List.id',
                        ],
                    ],
                    [
                        'table'      => 'form_data_checkbox_elements',
                        'alias'      => 'Form_Data_Checkbox_Element',
                        'type'       => 'Left',
                        'conditions' => [
                            'Webform_User_Data_List.field_value_id = Form_Data_List.id',
                        ],
                    ],
                ],
                'order'      => ['Webform_User_Data_List.id ASC'],
                'fields'     => ['Webform_User_Data_List.*', 'Webform_Field.*', 'Form_Data_List.*'],
            ]
        );

        return $form_fields;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function hasWebformOrders($id)
    {
        $this->setup();

        return $this->Webform_User_Data->find(
            "count",
            [
                'conditions' =>
                    [
                        'id' => $id,
                    ],
            ]
        );
    }

    /**
     * @param $webform_id
     * @param $id
     *
     * @return bool
     */
    public function deleteWebformField($webform_id, $id)
    {
        $this->setup();
        $field = $this->Webform_Field->find(
            "first",
            [
                'conditions' =>
                    [
                        'id'      => $id,
                        'form_id' => $webform_id,
                    ],
            ]
        );
        if (count($field) > 0) {
            $this->Webform_Field->id = $id;
            $this->Webform_Field->delete();

            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     */
    public function updateWebformReportStatus($id, $status)
    {
        $this->setup();
        $data                        = [
            'status' => $status,

        ];
        $this->Webform_User_Data->id = $id;
        $this->Webform_User_Data->save($data);

        return true;
    }

}