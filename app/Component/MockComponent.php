<?php
App::uses('L10n', 'L10n');
App::uses('Component', 'Controller');

/**
 * Компонент Заглушка
 */
class MockComponent extends Component
{
    public $components = array(
        'Session',
        'Error'
    );

    public $controller;

    const BOOL = true;

    const ARR = array();

    const INT = 1;

    const STRING = "";

    function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

}