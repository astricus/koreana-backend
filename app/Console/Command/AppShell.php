<?php
App::uses('ComponentCollection', 'Controller');
App::uses('View', 'View');

error_reporting(E_ALL ^ E_WARNING);
set_time_limit(0);
ob_implicit_flush();

//SHELL APP

class AppShell extends Shell
{
	public $uses = array();

	public $components = array();

    public function main()
    {}

}