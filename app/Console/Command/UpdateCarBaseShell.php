<?php
App::uses('CarApiComponent', 'Component');
App::uses('ComponentCollection', 'Controller');
App::uses('View', 'View');

error_reporting(E_ALL ^ E_WARNING);
set_time_limit(0);
ob_implicit_flush();

//SHELL APP

class UpdateCarBaseShell extends AppShell
{
	public $uses = array();

	public $components = array('Task');

    public $task_name = "обновление базы автомобилей";

    public function main()
    {
        $this->out($this->task_name . ' началось');
        $this->updateBase();
    }

    public function updateBase()
    {
        $collection = new ComponentCollection();
        $this->start_time = microtime(true);
        $this->CarApiComponent = $collection->load('CarApi');
        $this->TaskComponent = $collection->load('Task');
        $this->start_time = microtime(true);
        $update_result = $this->CarApiComponent->updateBase();
        $this->out($update_result);
        $task_timer = (microtime(true) - $this->start_time);
        $this->TaskComponent->logTask($this->task_name, "update_car_base","success", $update_result, $task_timer);
        $this->out(" задание выполнено");
    }


}