<?php
App::uses('TaskComponent', 'Component');
App::uses('ComponentCollection', 'Controller');
App::uses('View', 'View');

error_reporting(E_ALL ^ E_WARNING);
set_time_limit(0);
ob_implicit_flush();

//SHELL APP

class TaskShell extends AppShell
{
	public $uses = array();

	public $components = array('Task');

    public function main()
    {
        $this->customTask();
    }

    public function customTask()
    {
        $collection = new ComponentCollection();
        $this->TaskComponent = $collection->load('Task');

        $tasks = $this->TaskComponent->taskToDo();
        if(count($tasks)==0){
            $this->out("Задачи для запуска отсутствуют");
            exit;
        }
        $this->out("Запускается " . count($tasks) . " фоновых задач");
        foreach ($tasks as $task_item){
            $task = $task_item['Task'];
            $task_id = $task['id'];
            $task_name = $task['name'];
            $task_code = $task['code'];
            $task_args = $task['args'];

            if(!key_exists($task_code, $this->TaskComponent->task_table)){
                $this->TaskComponent->startTask($task_id);
                $this->out("Код задачи $task_code не найден");
                $this->TaskComponent->resultTask($task_id, 'failure', "Код задачи $task_code не найден", 0);
                $this->TaskComponent->stopTask($task_id);
                continue;
            }
            $task_table = $this->TaskComponent->task_table[$task_code];
            $this->start_time = microtime(true);
            $this->TaskComponent->startTask($task_id);
            $this->out($task_name . ' начата');
            //$task_class = $task_table['class'] . "Component";
            $task_method = $task_table['action'];
            $result_task = [];
            $task_class = $collection->load( $task_table['class']);
            if(method_exists($task_class, $task_method)){

                $args_str = [];
                if(!empty($task_args)){
                    $args_str = explode(",", $task_args);
                }

                $this->out("Метод $task_method класса " . $task_table['class'] . "Component запущен с параметрами $task_args");

                // запуск фоновой задачи
                $result_task = $task_class->$task_method($task_args);

            } else {
                $result_error= "Метод $task_method или класс " . $task_table['class'] . "Component не найдены, ошибка";
                $this->out($result_error);
                $result_task['payload'] = $result_error;
                $result_task['status'] = 'failure';
            }
            $task_timer = (microtime(true) - $this->start_time);

            $this->TaskComponent->resultTask($task_id, $result_task['status'], $result_task['payload'], $task_timer);
            $this->TaskComponent->stopTask($task_id);
        }

    }

}