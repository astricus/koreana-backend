<?php
App::uses('TaskComponent', 'Component');
App::uses('ComponentCollection', 'Controller');
App::uses('View', 'View');

error_reporting(E_ALL ^ E_WARNING);
set_time_limit(0);
ob_implicit_flush();

//SHELL APP

class DumpShell extends AppShell
{
	public $uses = array();

	public $components = array('Task');

    public $task_name = "создание дампа базы данных";

    public function main()
    {
        $this->out($this->task_name . ' началось');
        $this->createDump();
    }

    public function createDump()
    {
        $collection = new ComponentCollection();
        $this->TaskComponent = $collection->load('Task');
        $this->start_time = microtime(true);
        $dump_name = date("Y-m-d") . "--.sql";
        $path = TMP;
        $final_file = $path . $dump_name;
        exec("mysqldump --user=root --password=Cosmos_888 --host=127.0.0.1 --ignore-table=koreana.api_log koreana > $final_file");
        $task_result = 'failure';
        if(file_exists($final_file)){
            $task_result = 'success';
        }
        $task_timer = (microtime(true) - $this->start_time);
        $this->TaskComponent->logTask($this->task_name, "create_dump", $task_result, $final_file, $task_timer);
        $this->out(" задание выполнено");
    }


}