<?php
App::uses('TaskComponent', 'Component');
App::uses('PushComponent', 'Component');
App::uses('ComponentCollection', 'Controller');
App::uses('View', 'View');

error_reporting(E_ALL ^ E_WARNING);
set_time_limit(0);
ob_implicit_flush();

//SHELL APP

class PushShell extends AppShell
{
	public $uses = array();

	public $components = array('Push');

    public $task_name = "Очистка старых пуш уведомлений";

    public function main()
    {
        $this->out($this->task_name . ' начата');
        $this->clearOldPush();
    }

    public function clearOldPush()
    {
        $collection = new ComponentCollection();
        $this->PushComponent = $collection->load('Push');
        $this->TaskComponent = $collection->load('Task');
        $this->start_time = microtime(true);
        $pushes = $this->PushComponent->clearOldPush();
        $this->out($pushes);
        $task_timer = (microtime(true) - $this->start_time);
        $this->TaskComponent->logTask($this->task_name, "clear_old_pushes",true, null, $task_timer);
        $this->out(" задание выполнено");
    }

}