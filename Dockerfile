FROM php:7.4-fpm

RUN apt-get update && apt-get install -y \
mc \
curl \
wget \
git \
ant \
libfreetype6-dev \
libjpeg62-turbo-dev \
libpng-dev \
&& docker-php-ext-configure gd --with-jpeg --with-freetype \
&& docker-php-ext-install -j$(nproc) iconv mysqli pdo_mysql gd \
&& docker-php-ext-enable gd

#RUN docker-php-ext-install mbstring

ADD php-fpm/php-ini-overrides.ini /usr/local/etc/php/conf.d/40-custom.ini

WORKDIR /koreana

CMD ["php-fpm"]