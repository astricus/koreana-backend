#!/bin/bash
#set production directory
prod_dir=/opt/koreana/src_test
mkdir -p $prod_dir

#copy app & docker files
cp -R app $prod_dir
cp -R certs $prod_dir
cp -R lib $prod_dir
cp -R nginx $prod_dir
cp -R php-fpm $prod_dir
cp -R .htaccess $prod_dir
cp -R docker-compose.yml $prod_dir
cp -R Dockerfile $prod_dir
cp -R mysql.env $prod_dir
cp -R index.php $prod_dir

#make directories && log files
mkdir -p $prod_dir/logs
touch $prod_dir/logs/api.log
touch $prod_dir/logs/nginx.log
touch $prod_dir/logs/access.log

mkdir -p $prod_dir/app/tmp
mkdir -p $prod_dir/app/tmp/logs
touch $prod_dir/app/tmp/logs/api.log
touch $prod_dir/app/tmp/logs/error.log

#clear not required dir
rm -rf $prod_dir/app/_Component
rm -rf $prod_dir/app/SITE

#setup app config
rm -f $prod_dir/app/Config/database.php
mv $prod_dir/app/Config/database_docker.php $prod_dir/app/Config/database.php
rm -f $prod_dir/app/Config/main_config.php
mv $prod_dir/app/Config/main_config_production.php $prod_dir/app/Config/main_config.php

#set required rights
chmod -R 755 $prod_dir/app
chmod -R 755 $prod_dir/lib
chmod -R 755 $prod_dir/.htaccess
chmod -R 755 $prod_dir/logs
chmod -R 755 $prod_dir/index.php
chmod -R 777 $prod_dir/app/tmp
chmod u+x $prod_dir/app/Console/cake

echo 'Production build is success!'